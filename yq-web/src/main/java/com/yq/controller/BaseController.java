package com.yq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by xvdong on 2017-10-16.
 */
@Controller
public class BaseController {
    public String index(){
        return "/index";
    }
}
