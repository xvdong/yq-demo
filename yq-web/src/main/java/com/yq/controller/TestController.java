package com.yq.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.yq.service.YqTestService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by xvdong on 2017-10-16.
 */
@Controller
@RequestMapping("/test")
public class TestController {


    @Reference
    private YqTestService service;

    @RequestMapping("/1")
    @ResponseBody
    public String testMethod(){
        return "测试内容";
    }

    @RequestMapping("/2")
    @ResponseBody
    public String testDubbo(){
        return service.test();
    }

    @RequestMapping("/3")
    @ResponseBody
    public List<Map> testMybatis(){
        String sql="SELECT * FROM rfq_request LIMIT 100";
        return service.test2(sql);
    }
}
