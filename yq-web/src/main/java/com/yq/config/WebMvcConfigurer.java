package com.yq.config;

import com.yq.interceptor.RightsHandlerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by xvdong on 2017-10-16.
 */
@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {
    @Bean
    public RightsHandlerInterceptor rightsHandlerInterceptor() {
        return new RightsHandlerInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(rightsHandlerInterceptor()).addPathPatterns("/rfqRequest/init","/rfqRequest/toEditRfqRequest","/rfqQuotation/init","/rfqRequest/auditInit","/rfqPreauditSupplier/init","/rfqTermsController/init","rfqFinancialApprovalList/init","/rfqTermsController/testuup");

        super.addInterceptors(registry);
    }

}
