package com.yq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages = { "com.yq"})
public class ApplicationBootstrap extends SpringBootServletInitializer {

	protected SpringApplicationBuilder confugure(SpringApplicationBuilder application){
		return application.sources(ApplicationBootstrap.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(ApplicationBootstrap.class, args);
	}
}
