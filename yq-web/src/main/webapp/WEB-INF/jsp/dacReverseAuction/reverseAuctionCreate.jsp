<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>发布反向竞价单</title>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>

    <c:import url="../common/jqGridBootstrap.jsp"/>
    <script src="${pageContext.request.contextPath}/js/rfq.datatable.edit.js?v=${version}"></script>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.js"></script>
    <script src="${pageContext.request.contextPath}/js/dacReverseAuction/dacReverseAuction4item.js?v=${version}"></script>

    <script src="${pageContext.request.contextPath}/js/dacReverseAuction/reverseAuctionCreate.js?v=${version}"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/verify.js?v=${version}"></script>
    <!-- 上传JS -->
    <script src="${resource}/lib/plupload-2.1.9/js/plupload.full.min.js"></script>
    <script src="${resource}/js/upload/uploadFile.js?v=${version}"></script>
    <style>
        .datatable {
            margin-bottom: 10px;
        }

        .flexarea .table th,
        td {
            text-align: center;
            vertical-align: middle;
        }

        .fixed-left .table th,
        td {
            text-align: center;
            vertical-align: middle;
        }

        .table .dropdown-menu {
            left: 120px !important;
            top: -28px !important;
        }
    </style>
</head>

<body id="validate-form">
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp"/>
        <div class="rightbar clearfix">
            <form id="dataform" >
                <input type="hidden" name="requestBo.id" id="requestId">
                <input type="hidden" name="requestBo.unifiedRfqNum" id="requestRfqNum">
            <div class="container">
                <div class="breadcrumbs wstyle">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">发布反向竞价单</li>
                                                    <button type="button" class="btn btn-primary pull-right" id="changeViewBtn" data-toggle="modal"
                                                            data-target="#changeViewModal"><i class="icon icon-columns"></i></button>
                    </ol>
                </div>
                <div class="page-content wstyle">
                    <!-- uh start  -->
                    <div id="stepBar" class="ui-stepBar-wrap nav-rightin" style="display: block;">
                        <div class="ui-stepBar">
                            <div class="ui-stepProcess" style="width: 0%;"></div>
                        </div>
                        <div class="ui-stepInfo-wrap">
                            <table class="ui-stepLayout" border="0" cellpadding="0" cellspacing="0" style="width: 1089px; margin-left: -67px;">
                                <tbody>
                                <tr>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-pre judge-stepSequence-pre-change" style="padding: 6px 12px;">1</a>
                                        <p class="ui-stepName">竞价规则</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change" style="padding: 4px 10px;">2</a>
                                        <p class="ui-stepName">竞价物料</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">3</a>
                                        <p class="ui-stepName">供应商设置</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">4</a>
                                        <p class="ui-stepName">商务条款与技术条款</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">5</a>
                                        <p class="ui-stepName">采购角色设置</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">6</a>
                                        <p class="ui-stepName">采购联系方式</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">7</a>
                                        <p class="ui-stepName">竞价单附件</p>
                                    </td>
                                    <td class="ui-stepInfo" style="width: 155px;">
                                        <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">8</a>
                                        <p class="ui-stepName">竞价单备注</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- uh end  -->
                    <div class="panel panel-block ke-toolbar" id="topBarInfo">
                        <div class="panel-heading clearfix"><span class="mg-margin-right">基本信息</span></div>
                        <div class="col-md-12 with-padding">
                            <div class="row mt12">
                                <div class="re" style="color:#666;">
                                    <div class="so-form">
                                        <input name="requestBo.rfqMethod"  type="hidden" value="DAC">
                                        <input name="requestBo.type"  type="hidden" value="0">
                                        <input name="requestBo.ouId"  type="hidden" value="177736">
                                        <input name="requestBo.ouName"  type="hidden" value="欧冶云商股份有限公司">
                                        <div class="ml12 left mr12 line-32">计划编号：</div>
                                        <div class="so-form-20 padding1">
                                            <input name="requestBo.planNo" class="form-control input"
                                                   placeholder="内部管理用计划编号" type="text">
                                        </div>
                                        <div class="ml12 left mr12 line-32">竞价标题：</div>
                                        <div class="so-form-20 padding1">
                                            <input name="requestBo.title" class="form-control input"
                                                   placeholder="如计算机采购" type="text">
                                        </div>
                                        <div class="right mr12 line-32">
                                            <a class="btn  btn-primary" data-toggle="modal" data-target="#myModa1"> <i
                                                    class="icon icon-plus-sign"></i>&nbsp;从历史竞价单创建</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix"><span class="mg-margin-right">竞价规则</span></div>
                        <div class="panel-body" style="padding-left:25px;">
                            <div class="row">
                                <ul>
                                    <li class="clearfix col-lg-4 mb12">
                                        <div class="left line-32">竞价开始时间：　</div>
                                        <div class="left" style="width:200px;">
                                            <input name="rulesBo.quotationStartDate" placeholder=""
                                                   class="form-control form-datetime" type="text">
                                        </div>
                                    </li>
                                    <li class="clearfix col-lg-4 mb12">
                                        <div class="left line-32">竞价截止时间：　</div>
                                        <div class="left" style="width:200px;">
                                            <input name="rulesBo.quotationEndDate" placeholder=""
                                                   class="form-control form-datetime" type="text">
                                        </div>
                                    </li>
                                    <li class="clearfix"></li>
                                    <li class="clearfix col-lg-12 ">
                                        <span class="left line-32">竞价方式：　　</span>
                                        <div class="col-lg-12 hidec999" id="checkedDiv3">
                                            <label class=" line-32">
                                                <input type="radio" name="rulesBo.biddingMethod" value="1"
                                                       checked="true">
                                                <span> 按总价竞价　</span>
                                            </label>
                                            <label class="ml12  line-32">
                                                <input type="radio" name="rulesBo.biddingMethod" value="0">
                                                <span> 按物料行竞价</span>
                                            </label>
                                            <span class="ml35 c999">按行竞价时，竞价次数限制无效，供应商可分项报价，不能对部分数量报价。</span>
                                        </div>
                                        <div class="col-lg-12 hidec999" style="margin-left:108px;" id="checkPublic">
                                            <input type="hidden" name="rulesBo.viewPriceBillboardFlag" value="1">
                                            <input type="hidden" name="rulesBo.viewLowerestPriceFlag" >
                                            <label class=" line-32">
                                                <input type="radio" name="radioOptions2" data-type="viewPriceBillboardFlag" checked="true" value="1">
                                                <span> 公开竞价排名</span>
                                            </label>
                                            <label class="ml12 line-32">
                                                <input type="radio" name="radioOptions2" data-type="viewLowerestPriceFlag"value="1">
                                                <span> 公开竞价价格</span>
                                            </label>
                                            <span class="ml35 c999">供应商在竞价过程中可见自己的排名或者当前的最低价。</span>
                                        </div>
                                    </li>
                                    <li class="clearfix col-lg-12 bjCheck">
                                        <div id="setRadio">
                                            <div class="hidec999">
                                                <label class="radio-inline">
                                                    <input checked="true" type="radio" name="radioOptions3" value="0"> 系统自动延时：
                                                </label>
                                                <span class="mr12 softText ">
                                                    <input id="quotationUpperDate" name="requestBo.quotationUpperDate" min="1" placeholder="" class="form-control quotationUpperDate" type="text">
                                                </span>
                                                <span class="line-32 mr12"> 分钟，延时总时长不大于</span>
                                                <span class="mr12 softText">
                                                    <input id="maxExtendTime" name="requestBo.maxExtendTime" min="1" placeholder="" class="form-control maxExtendTime" type="text">
                                                </span>
                                                <span class="line-32"> 分钟。</span>
                                                <span class="c999 line-32 ml12">竞价结束前5分钟，仍有人出价时，系统自动延时，留空或0则不延时。</span>
                                            </div>
                                            <div class="mt12 hidec999">
                                                <label class="radio-inline mr12">
                                                    <input type="radio" name="radioOptions3" value="1"> 竞价次数限制：结束前
                                                </label>
                                                <span class="mr12 softText">
                                                    <input id="lastTime" name="requestBo.lastTime" min="1" placeholder="" class="form-control" type="text" disabled>
                                                </span>
                                                <span class="line-32 mr12"> 分钟，可竞价次数不大于</span>
                                                <span class="mr12 softText">
                                                    <input id="totalNum" name="requestBo.totalNum" min="1" placeholder="" class="form-control" type="text" disabled>
                                                </span>
                                                <span class="line-32"> 次。</span>
                                                <span class="c999 line-32 ml12">不能与系统自动延时并用。</span>
                                            </div>
                                        </div>
                                        <div class="mt12">
                                            <span class="left line-32">竟价货币：　　　</span>
                                            <span style=" display:inline-block;width:150px;">
                                                  <select id="currency" name="requestBo.currency" class="form-control">
                                                    <option value="CNY">人民币CNY</option>
                                                    <option value="USD">美元USD</option>
                                                    <option value="GBP">英镑GBP</option>
                                                    <option value="JPY">日元JPY</option>
                                                    <option value="EUR">欧元EUR</option>
                                                    <option value="HKD">港币HKD</option>
                                                    <option value="CHF">瑞士法郎CHF</option>
                                                  </select>
                                        </span>
                                        </div>
                                        <div class="mt12 hidec999">
                                            <span class="mr12">起拍价：　　　
                                            </span>
                                            <span class="mr12 softText">
                                                   <input name="requestBo.startPrice" min="1" max="100" placeholder="" class="form-control" type="text">
                                            </span>
                                            <span class="line-32 mr12"> 元</span>
                                            <span class="c999 line-32 ml12">反向竞价起步的最大限价，供应商可见，留空则不限。</span>
                                        </div>
                                        <div class="mt12 hidec999">
                                            <span class="mr12">最小降价幅度（梯度）：</span>
                                            <span class="mr12 softText">
                                               <input name="requestBo.priceGrad" min="1" placeholder="" class="form-control" type="text">
                                            </span>
                                            <span class="line-32 mr12"> 元</span>
                                            <span class="c999 line-32 ml12">每次降价额的最小幅度，设置后则按幅度的N（正整数）倍向下竞价，留空则不限幅度。</span>
                                        </div>
                                    </li>
                                    <li class="clearfix col-lg-12 bjCheck hide">
                                        <div>
                                            <span>系统自动延时：　</span>
                                            <span class="mr12 softText ">
                                                  <input id="quotationUpperDate2" name="requestBo.quotationUpperDate" placeholder="" class="form-control quotationUpperDate" type="text" disabled>
                                            </span>
                                            <span class="line-32 mr12"> 分钟，延时总时长不大于</span>
                                            <span class="mr12 softText " >
                                                 <input id="maxExtendTime2" name="requestBo.maxExtendTime" placeholder="" class="form-control maxExtendTime" type="text" disabled>
                                            </span>
                                            <span class="line-32"> 分钟。</span>
                                            <span class="c999 line-32 ml12">竞价结束前5分钟，仍有人出价时，系统自动延时，留空或0则不延时。</span>
                                        </div>
                                        <div class="mt12">
                                            <span class="left line-32">报价货币：　　　</span>
                                            <span style=" display:inline-block;width:150px;">
                                                  <select id="currency2" name="requestBo.currency" class="form-control" disabled>
                                                    <option value="CNY">人民币CNY</option>
                                                    <option value="USD">美元USD</option>
                                                    <option value="GBP">英镑GBP</option>
                                                    <option value="JPY">日元JPY</option>
                                                    <option value="EUR">欧元EUR</option>
                                                    <option value="HKD">港币HKD</option>
                                                    <option value="CHF">瑞士法郎CHF</option>
                                                  </select>
                                            </span>
                                        </div>
                                    </li>
                                    <li class="clearfix col-lg-6 mt12" style="clear:both">
                                        <span class="line-32">是否需要保证金：　　</span>
                                        <span id="checkedDiv2">
                                               <label class="radio-inline">
                                                 <input checked="true" type="radio" name="rulesBo.needAssureMoney" value="1" onclick="showMoney();"> 是
                                               </label>
                                               <label class="radio-inline">
                                                 <input type="radio"  name="rulesBo.needAssureMoney" value="0" onclick="hideMoney();"> 否
                                               </label>
                                        </span>
                                        <span class="ml12 softText" id="money_div">
                                              <input id="assureMoney" name="rulesBo.assureMoney" placeholder="" class="form-control" type="text" value = "0.00"/>
                                        </span>
                                        <span class="ml12 softText">
                        元
                      </span>
                                    </li>
                                    <li class="col-lg-6 mt12" style="clear:both">
                                        <span class="left line-32">竞价公开程度：　　</span>
                                        <div class="col-lg-12 padding1">
                                            <label class=" line-32">
                                                <input data-name="rulesBo.isPublicOnlineMarket" type="checkbox" class="js-check" value="1" checked disabled="disabled">
                                                <span>公开竞价公告</span>
                                            </label>
                                            <label class="ml12  line-32">
                                                <input data-name="rulesBo.isPublicPub" type="checkbox" value="1" class="js-check" checked disabled="disabled">
                                                <span>公开中标供应商</span>
                                            </label>
                                            <label class="ml12  line-32">
                                                <input data-name="rulesBo.isPublicBidMoney" type="checkbox" value="1">
                                                <span>公开中标价格</span>
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel  panel-block ke-toolbar" id="requestItemData">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价物料</span>
                            <span class="right font-16px">参考总价：<strong class="red"><em id="totalPrice">0.00</em>元</strong></span>
                        </div>

                        <table class="table table-bordered table-hove align-md datatable" id="requestItemTable">
                            <thead>
                            <tr id="itemTable">
                                <th data-width="50">序号</th>
                                <th data-width="70">操作</th>
                                <th data-width="120" class="flex-col"><i class="r">*</i>物料代码</th>
                                <th data-width="180" class="flex-col"><i class="r">*</i>物料名称</th>
                                <th data-width="90" class="flex-col"><i class="r">*</i>数量</th>
                                <th data-width="90" class="flex-col"><i class="r">*</i>单位</th>
                                <th data-width="180" class="flex-col"><i class="r">*</i>型规</th>
                                <th data-width="180" class="flex-col">品牌</th>
                                <th data-width="120" class="flex-col">物料描述</th>
                                <th data-width="150" class="flex-col">
                                    <i class="r">*</i>预算价
                                    <div class="btn-group">
                                        <button class="btn btn-hui dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                            上一次成交价<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">上一次成交价</a></li>
                                            <li><a href="#">历史最低价</a></li>
                                        </ul>
                                    </div>
                                </th>
                                <th data-width="100" class="flex-col"><span><i class="r">*</i>交货期</span>
                                    <div class="input-group date form-date requestDeliveryDate" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                        <input id="requestDeliveryDate2" class="form-control requestDeliveryDate" size="16" type="text"
                                               style="display: none;">
                                        <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                    </div>
                                </th>
                                <th data-width="100" class="flex-col">备注</th>
                                <!--  自定义物料字段 -->
                                <c:forEach var="ext" items="${requestItemExtraVo}">
                                    <th data-width="100" class="flex-col ext-col" data-extname="${ext.itemName}" data-extcode="${ext.itemCode}" data-extid="${ext.id}">${ext.itemName}</th>
                                </c:forEach>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                       <div  class="panel-body">
                            <!--  按钮组 -->
                            <div class="left">
                                <div class="so-form-40 mt12">
                                    <button class="btn btn-primary btn-sm" type="button" id="addRow"><i class="icon icon-plus"></i>
                                        新增一行
                                    </button>
                                    <%--<button class="btn btn-primary btn-sm" type="button" id="addItem" data-toggle="modal" data-target="#itemsModal"><i class="icon icon-plus" ></i>
                                        从库中选择
                                    </button>--%>
                                    <button class="btn btn-success btn-sm" type="button" onClick="excelClicke()" data-position="center" data-toggle="modal" data-target="#cancelPrice33">
                                        <i class="icon icon-signout"></i> excel模版
                                    </button>
                                    <button class="btn btn-danger btn-sm" type="button" id="clearData"><i
                                            class="icon icon-warning-sign"></i> 清空物料
                                    </button>
                                </div>
                            </div>
                        <!--  分页 -->
                        <div class="so-form-60 right mt20">
                            <div class="pull-right" style="margin-top:-18px;" id="d_pager"></div>
                        </div>
                        <%--</div>--%>
                       </div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix"><span class="mg-margin-right">供应商设置</span></div>
                        <div class="panel-body" style="padding-left:25px;">
                            <div class="row">
                                <ul>
                                    <li class="clearfix col-lg-12">
                                        <span class="left line-32">寻源范围：　　</span>
                                        <div class="col-lg-12 hidec999" id="checkedDiv">
                                            <label class=" line-32">
                                                <input type="radio" name="rulesBo.publicBiddingFlag" value="1" checked="true" >
                                                <span> 公开寻源　</span>
                                            </label>
                                            <label class="ml12  line-32">
                                                <input type="radio" name="rulesBo.publicBiddingFlag" value="0" >
                                                <span> 定向寻源 </span>
                                            </label>
                                            <span class="ml35 c999">定向寻源仅限于特邀供应商范围，特邀供应商无须报名。</span>
                                        </div>
                                    </li>
                                    <li class="clearfix col-lg-12 bjCheck2 fb-zz">
                                        <h4>报名要求</h4>
                                        <div class="hidec999">
                                            <span class="line-32">报名截止时间：　　</span>
                                            <span style=" display:inline-block">
                          <input name="rulesBo.registrationEndDate" placeholder="" class="form-control form-datetime" type="text">
                        </span>
                                            <span class="ml35 c999">不能晚于竞价开始时间，留空则在竞价开始之前均可报名。</span>
                                        </div>
                                        <div class="mt12">
                                            <span class="line-32">注册资本要求大于：</span>
                                            <span style=" display:inline-block">
                          <input name="preauditBo.regcapital" placeholder="" class="form-control" type="text">
                        </span>
                                            <span class="ml12">万元</span>
                                        </div>
                                        <div class="mt12">
                                            <span class="left line-32">资质要求：　　　　</span>
                                            <div class="col-lg-12 padding1">
                                                <label class=" line-32">
                                                    <input name="preauditBo.qualifications" type="checkbox" value="00">
                                                    <span> ISO9001质量体系认证 </span>
                                                </label>
                                                <label class="ml12 line-32">
                                                    <input name="preauditBo.qualifications" type="checkbox" value="10">
                                                    <span> ISO14001环境管理质量体系认证</span>
                                                </label>
                                                <label class="ml12 line-32">
                                                    <input name="preauditBo.qualifications" type="checkbox" value="40">
                                                    <span>职业健康安全管理体系认证 </span>
                                                </label>
                                                <a id="more" class="ml12 blue" href="javascript:;"
                                                   data-toggle="collapse" data-target="#collapseButton">更多资质要求 <i
                                                        class="icon icon-double-angle-down" style="font-size:16px"></i></a>
                                                <div class="clearfix"></div>
                                                <div id="collapseButton" class="collapse" style="margin-left:94px;">
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="70"> ISO/TS16949汽车工业管理体系认证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="150"> ISO10012测量管理体系认证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="255"> 特种设备制造许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="256"> 特种设备设计许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="257"> 特种设备安装改造维修许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="258"> 化学危险物品经营许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="259"> 安全生产许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="260"> 防爆合格证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="261"> 非药品类易制毒化学品经营备案证明</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="262"> 全国工业产品生产许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="263"> 道路运输经营许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="264"> 制造计量器具许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="265"> 中国国家强制性产品认证证书</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="266"> 辐射安全许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="267"> 冶金标准样品认可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="268"> 气瓶充装许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="269"> 食品批发（流通、卫生）许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="270"> 民用核承压设备设计资格许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="271"> 民用核承压设备制造资格许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="272"> 民用核安全设备制造许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="273"> 民用核安全设备设计许可证</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="274"> 防雷工程专业设计资质证书</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="275"> 防雷工程专业施工资质证书</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="276"> 印刷经营许可</label>
                                                    <label><input name="preauditBo.qualifications" type="checkbox" value="277"> 成品油批发经营许可</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt12">其它要求</div>
                                        <div class="mt5">
                                            <textarea id="contentSimple" name="content"
                                                      class="form-control kindeditorSimple col-md-10"
                                                      style=" width:100%;height:150px;"></textarea>
                                        </div>
                                        <div class="mt12">
                                            报名要求附件
                                            <button class="btn right btn-warning btn-sm attachmentBtn"  type="button"
                                                    data-toggle="modal" data-target="#myModa2" data-type="P"><i
                                                    class="icon icon-plus"></i> 新增附件
                                            </button>
                                        </div>
                                        <table class="table table-bordered table-hove  align-md mt12">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>文件名称</th>
                                                <th>文件大小</th>
                                                <th>说明</th>
                                                <th width="150px">操作</th>
                                            </tr>
                                            </thead>
                                            <tbody id="attachmentList1"></tbody>
                                        </table>
                                        <span class="c999 font12">注：附件内容可以是询比价文件和详细说明，询比价文件需通过报名审核后才能下载。每个不超过10M，支持jpg、jpeg、gif、png、doc、docx、xls、xlsx、pdf、dwg格式。</span>
                                    </li>
                                    <li class="clearfix col-lg-12">
                                        <div class="mt12">
                                            <div class="clearfix line-32">
                                                <span class="left">特邀供应商：</span>
                                                <div class="fb-zz left bjCheck2">
                                                    <input data-name="rulesBo.isSupplierRequirement" value="1" type="checkbox" style="margin:0 2px 0 12px; " >
                                                    <span class="c999">特邀供应商仍需走报名审核流程。</span>
                                                </div>
                                                <button class="btn btn-warning btn-sm right" type="button"
                                                        data-toggle="modal" data-target="#supplistModal"><i
                                                        class="icon icon-plus" id="superlist"></i> 邀请供应商
                                                </button>
                                            </div>
                                            <div>
                                                <table class="table table-bordered table-hove  align-md" style="margin-bottom:5px;"
                                                       id="supplierListJqGrid"></table>
                                                <div class="col-md-12" id="supplierListjqGridPager"></div>
                                            </div>
                                            <%--<table class="table table-bordered table-hove  align-md" id="supplierListTable">
                                                <thead>
                                                <tr>
                                                    <th width="50px">序号</th>
                                                    <th>供应商U代码</th>
                                                    <th>特邀供应商名称</th>
                                                    <th>联系人</th>
                                                    <th>联系电话</th>
                                                    <th>微信状态</th>
                                                    <th width="100px">操作</th>
                                                </tr>
                                                </thead>
                                                <tbody> </tbody>
                                            </table>--%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">商务条款</span>
                            <button class="btn  btn-warning btn-sm right" type="button" data-toggle="modal"
                                    data-target="#myModaBL" id="attachment"><i class="icon icon-plus"></i> 我的常用商务条款
                            </button>
                        </div>

                        <div class="panel-body">
                            <div class="row" style="padding:10px 20px 10px 25px;">
                                <textarea id="contentSimple3" name="content"
                                          class="form-control kindeditorSimple col-md-10"
                                          style=" width:100%;height:150px;"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">技术条款</span>
                            <button class="btn  btn-warning btn-sm right" type="button" data-toggle="modal"
                                    data-target="#myModaTL" id="attachment2"><i class="icon icon-plus"></i> 我的常用技术条款
                            </button>
                        </div>

                        <div class="panel-body">
                            <div class="row" style="padding:10px 20px 10px 25px;">
                                <textarea id="contentSimple2"
                                          class="form-control kindeditorSimple col-md-10"
                                          style=" width:100%;height:150px;"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">角色设置</span>
                            <button class="btn right btn-warning btn-sm" type="button" data-toggle="modal"
                                    data-target="#myModaphone"><i class="icon icon-plus" id="prouser"></i> 新增角色
                            </button>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32 mr12 ml12">
                                    <table class="table table-bordered table-hove  align-md" id="roleTable">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>工号</th>
                                            <th>姓名</th>
                                            <th>部门</th>
                                            <th>职位</th>
                                            <th width="80">查看</th>
                                            <th width="80">操作</th>
                                            <th width="80">财务管理</th>
                                            <th>操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr id="puser_${sysUserVo.usrId}">
                                            <td data-name="userId" data-value="${sysUserVo.usrId}">${status.index+1}</td>
                                            <td data-name="jobCode" data-value="${rfqProjectUserNewVo.jobCode}">${rfqProjectUserNewVo.jobCode}</td>
                                            <td data-name="userName" data-value="${sysUserVo.usrFullName}">${sysUserVo.usrFullName}</td>
                                            <td data-name="userOffice" data-value="${sysUserVo.orgName}">${sysUserVo.orgName}</td>
                                            <td data-name="userTitle" data-value="${rfqProjectUserNewVo.userTitle}">${rfqProjectUserNewVo.userTitle}</td>
                                            <td>
                                                <input checked type="checkbox" disabled="disabled"/>
                                            </td>
                                            <td>
                                                <input checked type="checkbox" disabled="disabled"/>
                                            </td>
                                            <td>
                                                <input type="checkbox" disabled="disabled">
                                            </td>
                                            <td><c:if test="${status.index>0}"><a class="order-del-btn" data-id="${item.userId}"><i class="icon icon-trash red"></i>&nbsp; 删除</a></c:if></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">联系方式</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ml12 font12">
                                <ul>
                                    <li class="clearfix col-lg-10 mb12">
                                        <span class="left line-32">联系人：　</span>
                                        <div class=" col-lg-6 padding1">
                                            <input name="contactsBo.linkmanName" placeholder="联系人" class="form-control" type="text" value="${sysUserVo.usrFullName}">
                                        </div>
                                        <span class="line-32 c999 ml12">默认为寻源业务发起人，也可填写项目联络人或者收货人</span>
                                    </li>
                                    <li class="clearfix col-lg-10">
                                        <span class="left line-32">联系电话：</span>
                                        <div class="col-lg-6 padding1">
                                            <input name="contactsBo.linkmanTelphone" placeholder="联系电话" class="form-control" type="text">
                                        </div>
                                    </li>
                                    <li class="clearfix col-lg-10 mt12">
                                        <span class="left line-32">交货地址：</span>
                                        <div class="col-lg-2 padding1">
                                            <select  name="contactsBo.deliveryProvince" class="form-control" id="deliveryProvince">

                                            </select>
                                        </div>
                                        <div class="col-lg-2 padding1">
                                            <select name="contactsBo.deliveryCity"  class="form-control"  id="deliveryCity" >
                                            </select>
                                        </div>
                                        <div class="col-lg-2 padding1">
                                            <select name="contactsBo.deliveryArea"  class="form-control"  id="deliveryArea" >
                                            </select>
                                        </div>
                                        <div class="col-lg-5 padding1">
                                            <input name="contactsBo.deliveryAddress" id="deliveryAddress" placeholder="具体地址" class="form-control left" type="text">
                                        </div>
                                    </li>
                                    <div class="left line-32 mt12">
                                        <button   class="btn  btn-warning btn-sm" type="button" data-toggle="modal"
                                                data-target="#myModa8"><i class="icon icon-plus"></i>&nbsp常用地址
                                        </button>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel  panel-block  ke-toolbar">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价单附件</span>
                            <button class="btn right btn-warning btn-sm attachmentBtn" type="button" data-toggle="modal"
                                    data-target="#myModa2" data-type="R"><i class="icon icon-plus"></i> 新增附件
                            </button>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="re line-32 ml12 mr12">
                                    <table class="table table-bordered align-md">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>文件名称</th>
                                            <th width="100px">文件大小</th>
                                            <th>说明</th>
                                            <th width="150px">操作</th>
                                        </tr>
                                        </thead>
                                        <tbody id="attachmentList2"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--竞价单备注--%>
                    <div title="竞价单备注" class="panel panel-block ke-toolbar">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价单备注</span>
                        </div>
                        <div class="panel-body">
                            <div class="row" style="padding:10px 20px 10px 25px;">
                                <textarea id="memo" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- uh start  -->
                    <div class="panel2 hide" id="topBarInfo2" style="position:fixed; top:0; width:1038px; z-index:99;">

                        <div class="col-md-12 with-padding" style="padding:10px 12px">
                            <div class="row">
                                <div class="re line-32" style="color:#666;">
                                    <div class="so-form mt12">
                                        <%--<div class="left mr12 line-32">竞价单号：23123123213</div>--%>
                                        <div class="ml12 left mr12 line-32">计划编号：<span id="_planNo"></span></div>
                                        <div class="ml12 left mr12 line-32">询价单标题：<span id="_title"></span></div>
                                    </div>
                                </div>
                                <div class="text-right mr12" style="margin-top:-12px">
                                    <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                                    <%--  <a href="" class="btn btn-lg btn-danger js-form-btn" data-type="2" type="button">删除</a>--%>
                                    <a href="javascript:;" class="btn btn-lg btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                                    <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right ke-toolbar" id="btn-r-b">

                        <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                        <%--<a href="javascript:;" class="btn btn-lg btn-danger" type="button">删除</a>--%>
                        <a href="javascript:;" class="btn btn-lg btn-success js-form-btn js-block"  data-type="3" type="button">预览</a>
                        <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn js-block"  data-type="4" type="button">发布</a>
                    </div>

                    <div id="sideRightBar" class="ke-toolbar">
                        <div class="re">
                            <div class="ui-rightbar-middle"></div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="竞价物料">竞价规则</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="竞价规则">竞价物料</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="供应商设置">供应商设置</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="询单规则">商务条款与<br>技术条款</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="商务条款与技术条款">采购角色设<br>置</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="联系方式">采购联系方<br>式</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="询单附件">竞价单附件</a>
                                </div>
                            </div>
                            <div class="ui-rightbar-list">
                                <div class="ui-rightbar-point"></div>
                                <div class="ui-rightbar-text">
                                    <a href="javascript:;" class="ui-load-button" title="竞价单备注">竞价单备注</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- uh end  -->
                    <div class="text-right bcontrol">
                        <div class="row pagebtn text-center pdiv"><a class="btn btn-primary top ke-toolbar" style="margin:5px;">上一页</a><a
                                class="btn btn-primary bottom" style="margin:5px;">下一页</a></div>
                        <a href="javascript:;" class="btn btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                        <a href="javascript:;" class="btn btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                        <a href="javascript:;" class="btn btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                    </div>
                </div>
            </div>
         </form>
        </div>
    </div>
</div>




<!-- ///////////////////////新增附件2/////////////// -->
<div class="modal fade"  id="myModa2">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                           <form>
                               <input type="text"  id="fileName" placeholder="请上传,附件大小≤100M"  class="form-control no-border" />
                               <input type="button" id="upload" value="上传">
                               <span  id="msg">0%</span>
                           </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration" class="form-control" maxlength="1000"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary js-confirm" data-dismiss="modal" id="sure" disabled="disabled">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- id2结束 -->

<!-- ///////////////////////特邀供应商/////////////// -->
<div class="modal fade"  id="supplistModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">特邀供应商</h4>
            </div>
            <div class="modal-body">
                <form id="searchForm">
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">

                        <div class="so-form-20 mr12" style="width:15%;">
                            <input class="form-control col-lg-8" placeholder="供应商名称" type="text" name="supplierName">
                        </div>

                        <div class="so-form-20 mr12" style="width:15%;">
                            <select class="form-control" name="deviceDealerGrade">
                                <option value="">供应商等级</option>
                                <option value="已认证">已认证</option>
                                <option value="未认证" disabled="disabled">未认证</option>
                            </select>
                        </div>
                        <div class="so-form-20 mr12" style="width:20%;">
                            <input class="form-control" placeholder="所属类别" type="text" name="type">
                        </div>
                        <div class="so-form-20" style="width:20%;">
                            <input class="form-control" placeholder="供应商代码" type="text" name="supplierCode">
                        </div>
                        <div class="so-form-20 ml12">
                            <button class="btn btn-primary" type="button" id="btnSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>

                    </div>
                </form>
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGridsup" ></table>
                    <div class="col-md-12" id="jqGridPagersup"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->



<!-- ///////////////////////添加物料/////////////// -->
<div class="modal fade"  id="itemsModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">选择物料</h4>
            </div>
            <div class="modal-body">
                <form id="itemSearchForm">
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">

                        <div class="so-form-20 mr12" style="width:15%;">
                            <input class="form-control col-lg-8" placeholder="所属类目" type="text" name="">
                        </div>
                        <div class="so-form-20 mr12" style="width:20%;">
                            <input class="form-control" placeholder="物料名称" type="text" name="type">
                        </div>
                        <div class="so-form-20" style="width:20%;">
                            <input class="form-control" placeholder="物料代码" type="text" name="supplierCode">
                        </div>
                        <div class="so-form-20 ml12">
                            <button class="btn btn-primary" type="button" id="itembtnSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>

                    </div>
                </form>
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGriditem" ></table>
                    <div class="col-md-12" id="jqGridPageritem"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->


<!-- ///////////////////////我常用的商务及技术条款/////////////// -->
<div class="modal fade" id="myModaBL">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的常用商务条款</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGrid">

                    </table>
                </div>
                <div class="col-md-12 clearfix" style="padding-right:0px;" id="jqGridPager">

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm" >确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<div class="modal fade" id="myModaTL">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的常用技术条款</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGrid1">

                    </table>
                </div>
                <div class="col-md-12 clearfix" style="padding-right:0px;" id="jqGridPager1">

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->

<!-- ///////////////////////从历史询单创建/////////////// -->
<div class="modal fade" id="myModa1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">从历史竞价单创建</h4>
            </div>
            <div class="modal-body">
                <form id="searchFormHis" method="post" onsubmit="return false;">
                <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                    <div class="so-form-8">竞价单号：</div>
                    <div class="so-form-15 padding1">
                        <input name="unifiedRfqNum" id="unifiedRfqNum" class="form-control" placeholder="竞价单号" type="text">
                    </div>
                    <div class="so-form-8 ml12">竞价标题：</div>
                    <div class="so-form-30 padding1">
                        <input name="title" id="title" class="form-control" placeholder="竞价标题" type="text">
                    </div>
                    <div class="so-form-7 ml12">状态：</div>
                    <div class="so-form-15 padding1">
                        <select class="form-control" id="type" name="type">
                            <option value="">待选择</option>
                            <option value="0">草稿</option>
                            <option value="1">创建待审批</option>
                            <option value="2">待发布</option>
                            <option value="3">待报价</option>
                            <option value="5">报名中</option>
                            <option value="6">报价中</option>
                            <option value="7">待开标</option>
                            <option value="8">待核价</option>
                            <option value="9">结果待审批</option>
                            <option value="10">已结束</option>
                            <option value="11">已作废</option>
                            <option value="12">已流标</option>
                        </select>
                    </div>
                </div>
                <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                    <div class="so-form-8">创建人：</div>
                    <div class="so-form-15 padding1">
                        <input name="recCreatorUsername" id="recCreatorUsername" class="form-control" placeholder="请填写创建人" type="text">
                    </div>
                    <div class="so-form-8 ml12">创建日期：</div>
                    <div class="so-form-15">
                        <input name="recCreateTime" id="recCreateTime" placeholder="开始日期" class="form-control form-datetime" type="text">
                    </div>
                    <div class="so-form-3 height32">-</div>
                    <div class="so-form-15 padding1">
                        <input name="recCreateTimeEnd" id="recCreateTimeEnd" placeholder="截止日期" class="form-control form-datetime" type="text">
                    </div>
                    <div class="so-form-20 ml12">
                        <button class="btn btn-primary" type="button" id="btnSearchHis"><i class="icon icon-search"></i> 搜索</button>
                        <input class="btn" value="重置" type="reset">
                    </div>
                </div>
                    </form>
                <div class="content">
                    <table  class="table table-bordered align-md" id="jqGridHis">

                    </table>
                </div>
                <div class="col-md-12 clearfix" style="padding-right:0px;" id="jqGridHisPager">

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="rid"/>
            <button type="button" class="btn btn-primary" onclick="addHis()">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
<div class="modal fade" id="myModa8">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">常用地址</h4>
            </div>
            <div class="modal-body">
                <div class="so-form clearfix" style="margin:0px;">
                    <div class="so-form-20">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalAddress">新增</button>
                    </div>
                </div>
                <table class="table table-bordered table-hove align-md" id="AddreJqGrid"></table>
                <div class="col-md-12" id="AddreJqGridPager"></div>
            </div>
            <div class="modal-footer">
                <%--<a type="button" class="btn btn-primary btn-sm"  href="javascript:;"><i class="icon icon-plus"></i>&nbsp新增一行</a>--%>
                <a type="button" class="btn btn-success btn-sm"  href="javascript:addAddress();">确定</a>
            </div>
        </div>
    </div>
</div>
<!-- ///////////////////////新增角色/////////////// -->
<div class="modal fade"  id="myModaphone">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">角色设置</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">姓名： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-8 ml12 text-right">工号： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="jobCode">
                        </div>
                        <div class="so-form-8 ml12 text-right">部门： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userOffice">
                        </div>
                        <div class="so-form-15 ml12">
                            <button class="btn btn-primary" type="button" id="roleSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridphone">
                        <div class="col-md-12" id="jqGridPagerphone"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->

<!-- 发布 -->
<div class="modal fade" id="myModa6">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">提交确认</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t ml35">
                    <i class="icon icon-question-sign text-primary"></i>
                    <div class="content ml12">
                        <h4>你确定要提交本询价单吗?</h4>
                        <p>竞价标题：　　<span data-mp="title">物位计等备件公开采购</span></p>
                        <p>竞价单号：　　<span data-mp="requestNo">2014061820</span></p>
                        <%--<p>计划编号：　　2014061820</p>--%>
                        <p>
                            <span class="left">选择审批流程：</span>
                            <span class="col-lg-6 padding1">
                                <select id="appro" style="width:110px" class="form-control input-sm">
                                </select>
                            </span>
                        </p>

                    </div>
                </div>
            </div>
            <div class="modal-footer"><a data-type="1" type="button" class="btn btn-primary" href="javascript:;">确定提交</a>
                <a data-type="2" type="button" class="btn" href="javascript:;">返回修改</a></div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalAddress">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增常用地址</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li class="clearfix col-lg-10 mt12">
                        <%-- <span class="left line-32">：</span>--%>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryProvince" class="form-control"  id="deliveryProvince2" >
                            </select>
                        </div>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryCity"  class="form-control"  id="deliveryCity2" >
                            </select>
                        </div>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryArea"  class="form-control"  id="deliveryArea2" >
                            </select>
                        </div>
                        <div class="col-lg-5 padding1">
                            <input name="contactsBo.deliveryAddress" id="deliveryAddress2" placeholder="具体地址" class="form-control left" type="text">
                        </div>
                    </li>
                    <%--<li class="clearfix col-lg-10 mt12">
                        <span class="left line-32">选择市：</span>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryCity"  class="form-control"  id="deliveryCity2" >
                            </select>
                        </div>
                    </li>
                    <li class="clearfix col-lg-10 mt12">
                        <span class="left line-32">选择区（县）：</span>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryArea"  class="form-control"  id="deliveryArea2" >
                            </select>
                        </div>
                    </li>--%>
                    <%-- <li class="clearfix col-lg-10 mt12">
                         <span class="left line-32">详细地址（街道）：</span>
                         <div class="col-lg-5 padding1">
                             <input name="contactsBo.deliveryAddress" id="deliveryAddress2" placeholder="具体地址" class="form-control left" type="text">
                         </div>
                     </li>--%>
                </ul>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-success btn-sm"  href="javascript:ajaxToAddData();">确定</a>
            </div>
        </div>
    </div>
</div>
<!-- excel导入开始 -->
<div class="modal fade" id="cancelPrice33">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">excel导入</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row">
                    <span class="left-cont2">1、第一步请先下载excel模版。</span>
                    <div>
                        <span class="left-cont1 ml12"><a href="javascript:exportExcelModel();" class="blue">点此下载excel模版</a></span>
                    </div>
                </div>
                <div class="mg-row clearfix" style="margin-bottom:8px;">
                    <span class="left-cont2">2、第二步请上传根据模版编辑好的文件。</span>
                    <form action="${pageContext.request.contextPath}/rfqRequestItemTmp/importRecord2" method="post" class="form-horizontal" id="excelUpload" onsubmit="return false;">

                        <input type="file" name="file" id="file">
                    </form>

                    </span>
                </div>
                <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px; height:66px;">
                    <i class="icon icon-warning-sign sign-warn1"></i><span class="sign-1 ml12">

注意：Excel批量导入将覆盖询单内现有物料；<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上传附件文件类型仅限Excel文件。</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="saveExcel()" class="btn btn-primary">确定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!-- uh start -->
<div class="modal fade in" id="changeViewModal" aria-hidden="false">
    <div class="modal-dialog" style="margin-top: 33.3333px;">
        <div class="modal-content">
            <div class="modal-header view-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">你想用哪种布局展示页面？</h4>
            </div>
            <div class="modal-body body-view">
                <div class="view-options">
                    <a class="view-option view-option-single active row" data-dismiss="modal">
                        <div class="view-shape pull-left">
                            <div class="s-1">
                                <img src="${resource}/images/cross.png">
                            </div>
                        </div>
                        <div class="title">点击翻页</div>
                        <p class="text-muted">点击上部的小圆点进入相应的页面</p>
                    </a>
                    <a class="view-option view-option-double row" data-dismiss="modal">
                        <div class="view-shape pull-left">
                            <div class="s-1">
                                <img src="${resource}/images/longitudinal.png">
                            </div>
                        </div>
                        <div class="title strong">滚动翻页</div>
                        <p class="text-muted">根据鼠标滚动位置自动切换到相应位置</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- uh end -->

<script src="${resource}/js/jquery.easing.1.3.js"></script>
<script src="${pageContext.request.contextPath}/js/stepBar.js"></script>
<script src="${pageContext.request.contextPath}/js/dacReverseAuction/dacReverseAuctionEdit4uh.js?v=${version}"></script>
<script>



    /*删除行*/
    $('.order-del-btn').delBtn();


    //日期
    $(".form-datetime").datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        format: "yyyy-mm-dd hh:ii"
    });

    $('#checkedDiv2').find('label input').click(function () {
        if ($(this).parent().index() == 0) {
            $(this).parent().parent().parent().find('.softText').show();
        } else {
            $(this).parent().parent().parent().find('.softText').hide();
        }
    });
    $('#checkedDiv3').find('label input').click(function () {
        $('.bjCheck').show();
        $('.jj-table').show();
        if ($(this).parent().index() == 0) {
            $('.bjCheck').eq(1).hide();
            $('.jj-table').eq(1).hide();
        } else {
            $('.bjCheck').eq(0).hide();
            $('.jj-table').eq(0).hide();
        }
    });
    $('#checkedDiv').find('label input').click(function () {
        if ($(this).parent().index() == 0) {
            $('.bjCheck2').slideDown();
        } else {
            $('.bjCheck2').slideUp();
        }
    });

    /* 初始化数据表格 */
    $('table.datatable').datatable({
        fixedLeftWidth:'120px',
        fixedHeader: false,
        ready: function () {
            $('.btn-group').selectLisr();
        }
    });


    $('#setRadio>div label').click(function () {
        $('#setRadio input').attr("disabled", "disabled");
        $(this).parent().find('input').removeAttr("disabled");

    })
    function getAddressData() {
        return null;
    }

    function showMoney(){
        $('#money_div').show();
        $('#assureMoney').val("0.00");
    }

    function hideMoney(){
        $('#money_div').hide();
        $('#assureMoney').val("0.00");
    }
    $("#assureMoney").on('input propertychange', function () {
        var valTmp = this.value;
        var num = +valTmp;
        if (num != 0 && !num) {
            RFQ.error('请输入数字');
            this.value = '';
        } else if (~valTmp.indexOf('.') && valTmp.split('\.')[1].length > 2) {
            RFQ.error('小数部分输入太长，应该小于等于2位');
            this.value = num.toFixed(2);
        }
    });
    //起拍价,最小降价梯度,注册资本
    $('input[name="requestBo.startPrice"],[name="requestBo.priceGrad"],[name="preauditBo.regcapital"]').on('input propertychange', function () {
        var valTmp = this.value;
        var num = +valTmp;
        if (num != 0 && !num) {
            RFQ.error('请输入数字');
            this.value = '';
        }
    });

    $('body').append('<div class="dangzhu1"></div>');
    $('body').append('<div class="dangzhu2"></div>');
</script>

<!--[if lt IE 9]>
<script type="text/javascript">
    jQuery(function($){
        $('#appro').css('width','350px');
    });
</script>
<![endif]-->
</body>

</html>

