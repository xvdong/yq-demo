<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>竞价单详情</title>
    <%@include file="../common/headBase.jsp" %>
    <%@include file="../common/jqGridBootstrap.jsp" %>
    <c:import url="../common/jqGridBootstrap.jsp"/>

    <script src="${pageContext.request.contextPath}/js/dacReverseAuction/reverseAuctionDetail.js?v=${version}"></script>


</head>
<body>
<c:import url="../common/top.jsp"/>
<div class="wrapper">
    <c:if test="${sysUserVo.loginOrgType != '2'}">
        <input type="hidden" id="gys_login" value="1"/>
    </c:if>
    <div class="container container_main">
        <c:import url="../common/menu.jsp"/>
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="index.html"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">竞价单详情</li>
                    </ol>
                </div>
                <input type="hidden" id="type" value="${DACRfqRequestComplexVo.requestVo.type=='3'}"/>
                <div class="page-content">

                    <div class="panel">
                        <div class="panel-heading clearfix">

                            <div class=" pull-right">
                                <c:if test="${rfqSupplierListVo.status=='0'}">
                                    <span class="costom" onclick="freshQuotation('${DACRfqRequestComplexVo.requestVo.id}','0')"><img
                                            src="${pageContext.request.contextPath}/images/freshaddon.png"
                                            style="width:40px; height:40px; margin-right: 5px; margin-top: -25px;">试竞价</span>
                                </c:if>
                                <span class="btn btn-danger font12" onclick="requestPrint()">打印竟价单</span>
                                <span class="btn btn-warning font12" onclick="requestDownload()">下载竟价单</span>
                                <c:if test="${sysUserVo.loginOrgType == '2'&&(rfqSupplierListVo.status=='0'||rfqSupplierListVo.status=='1')}">
                                    <span class="btn btn-warning font12" onclick="dacDownload('${DACRfqRequestComplexVo.requestVo.id}','${DACRfqRequestComplexVo.rulesVo.biddingMethod}','${rfqSupplierListVo.status}')">下载物料明细</span>
                                </c:if>
                            </div>
                        </div>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>竞价单号：${DACRfqRequestComplexVo.requestVo.unifiedRfqNum}</li>
                                        <li style="width:40%;">竞价标题：${DACRfqRequestComplexVo.requestVo.title}</li>
                                        <li>计划编号：${DACRfqRequestComplexVo.requestVo.planNo}</li>

                                    </ul>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div>
                        <span class="left line-32" style="font-size:16px; display:inline-block">竞价物料</span>
                        <rc:roleCheck roleType="100">
                            <c:if test="${ not empty DACRfqRequestComplexVo.requestVo.totalPrice}">
                                <span class="red right line-32"
                                      style=" display:inline-block">参考总价：${DACRfqRequestComplexVo.requestVo.totalPrice}元</span>
                            </c:if>
                        </rc:roleCheck>
                    </div>
                    <table class="table table-bordered table-hove align-md" id="jqGrid">

                    </table>
                    <div class="col-md-12 clearfix" id="jqGridPager">

                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价规则</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <span class="col-xs-5">创建人：${DACRfqRequestComplexVo.requestVo.recCreatorUsername}</span>
                                        <span class="col-xs-5">创建时间：${DACRfqRequestComplexVo.requestVo.recCreateTime}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">发布时间：${DACRfqRequestComplexVo.requestVo.issueDate}</span>
                                        <span class="col-xs-5">报名截止时间：${DACRfqRequestComplexVo.rulesVo.registrationEndDate}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">竞价开始时间：${DACRfqRequestComplexVo.rulesVo.quotationStartDate}</span>
                                        <span class="col-xs-5">竞价截止时间: ${DACRfqRequestComplexVo.rulesVo.quotationEndDate}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">
                                        <c:if test="${DACRfqRequestComplexVo.rulesVo.needAssureMoney=='1'}">
                                            保证金金额：<span class="red">${DACRfqRequestComplexVo.rulesVo.assureMoney}元</span>
                                        </c:if>
                                        <c:if test="${DACRfqRequestComplexVo.rulesVo.needAssureMoney!='1'}">
                                            不缴纳保证金
                                        </c:if>
                                        </span>
                                        <span class="col-xs-5">竞价货币：${DACRfqRequestComplexVo.requestVo.currency}</span>
                                    </div>
                                    <c:if test="${DACRfqRequestComplexVo.rulesVo.biddingMethod=='1'}">

                                        <div class="mt12 clearfix">
                                        <span class="col-xs-5">起拍价：
                                        <c:if test="${not empty DACRfqRequestComplexVo.requestVo.startPrice}">
                                            <span class="red"
                                                  id="startPrice">${DACRfqRequestComplexVo.requestVo.startPrice}元</span>
                                        </c:if>
                                        <c:if test="${empty DACRfqRequestComplexVo.requestVo.startPrice}">
                                            <span class="red" id="startPrice">无限制</span>
                                        </c:if>
                                        </span>
                                            <span class="col-xs-5">最小降价幅度：
                                        <c:if test="${not empty DACRfqRequestComplexVo.requestVo.priceGrad}">
                                            <span class="red"
                                                  id="priceGrad">${DACRfqRequestComplexVo.requestVo.priceGrad}元</span>
                                        </c:if>
                                        <c:if test="${empty DACRfqRequestComplexVo.requestVo.priceGrad}">
                                            <span class="red" id="startPrice">无限制</span>
                                        </c:if>
                                        </span>
                                        </div>
                                    </c:if>
                                    <div class="mt12 clearfix">
                                        <input type="hidden" value="${DACRfqRequestComplexVo.rulesVo.biddingMethod}"
                                               id="biddingMethod">
                                        <span class="col-xs-5">竞价方式：

                                          <c:if test="${DACRfqRequestComplexVo.rulesVo.biddingMethod=='1'}">
                                              按总价竞价
                                          </c:if>
                                          <c:if test="${DACRfqRequestComplexVo.rulesVo.biddingMethod=='0'}">
                                              按物料行竞价
                                          </c:if>
                                            &nbsp;
                                           <c:if test="${DACRfqRequestComplexVo.rulesVo.viewLowerestPriceFlag=='1'}">
                                               公开竞价价格
                                           </c:if>
                                            <c:if test="${DACRfqRequestComplexVo.rulesVo.viewPriceBillboardFlag=='1'}">
                                                公开竞价排名
                                            </c:if>
                                            </span>
                                        <span class="col-xs-5">竞价公开程度：
                                         <c:if test="${DACRfqRequestComplexVo.rulesVo.isPublicOnlineMarket=='1'}">
                                             公开询价公告
                                         </c:if>
                                      <c:if test="${DACRfqRequestComplexVo.rulesVo.isPublicPub=='1'}">
                                          &nbsp; 公开中标供应商
                                      </c:if>
                                     <c:if test="${DACRfqRequestComplexVo.rulesVo.isPublicBidMoney=='1'}">
                                         &nbsp; 公开中标价格
                                     </c:if>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">供应商设置</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">邀请范围：</label><span class="bler blu-85">
                                         <c:if test="${DACRfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                             公开寻源（面向平台注册供应商公开寻源，供应商需先报名，待采购组织审核通过后再报价。）
                                         </c:if>
                                            <c:if test="${DACRfqRequestComplexVo.rulesVo.publicBiddingFlag=='0'}">
                                                定向寻源 (将询价单信息向特邀供应商公布，供应商无须报名可直接报价。)
                                            </c:if>
                                    </span>
                                    </div>
                                    <c:if test="${DACRfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                    <div class="clearfix"><label class="bler blu-8">供应商要求：</label>
                                        <span class="bler blu-85">
                                            1、注册资本要求大于${DACRfqRequestComplexVo.preauditVo.regcapital}万元
                                            <br/>
                                            2、资质要求${DACRfqRequestComplexVo.preauditVo.qualifications}
                                        </span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">报名要求：</label>
                                        <span class="bler blu-85">
                                            ${DACRfqRequestComplexVo.preauditVo.requirementDesc}
                                        </span>
                                    </div>
                                    </c:if>
                                    <c:if test="${DACRfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                        <div>
                                            <span class="left line-32" style="font-weight:bold; color:#333;">附件清单：</span>
                                        </div>
                                        <table class="table table-bordered align-md" style="margin-bottom:5px; width:98%;">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>文件名称</th>
                                                <th width="100px">文件大小</th>
                                                <th>说明</th>
                                                <th width="100px">操作</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:set var="sum" value="1"/>
                                            <c:forEach var="item" items="${DACRfqRequestComplexVo.attachmentsList}"
                                                       varStatus="status">
                                                <c:if test="${item.type == 'P'}">
                                                    <tr data-clone="orginal">
                                                        <td data-order="true">${sum}</td>
                                                        <td>${item.originalFilename}</td>
                                                        <td>${item.fileSize}KB</td>
                                                        <td>${item.fileDeclaration}</td>
                                                        <td>
                                                            <a href="${downloadAction}?id=${item.attachmentId}&file=${item.downloadFilename}"><i
                                                                    class="icon icon-download-alt green "></i>&nbsp;下载</a>
                                                        </td>
                                                    </tr>
                                                    <c:set var="sum" value="${sum + 1}"/>
                                                </c:if>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <div class="red" style="font-size:12px;">
                                            注：附件类型不支持exe格式，文件大小不超过100M。
                                        </div>
                                    </c:if>
                                    <c:if test="${sysUserVo.loginOrgType != '2'}">
                                        <div>
                                            <div class="line-32 mt12">
                                            <span class="left"
                                                  style="font-weight:bold; color:#333; display:inline-block">供应商信息：</span>
                                                <c:if test="${DACRfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                                    <span class="bler" style="color:#666;">特邀供应商仍需走报名审核流程。</span>
                                                </c:if>
                                            </div>
                                        </div>
                                        <%--<table class="table table-bordered align-md" style="width:98%;">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>供应商U代码</th>
                                                <th>特邀供应商名称</th>
                                                <th width="80px">联系人</th>
                                                <th>联系电话</th>
                                                <th width="80px">微信状态</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:if test="${DACRfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">
                                                <c:forEach var="supplierList"
                                                           items="${DACRfqRequestComplexVo.supplierList}"
                                                           varStatus="status">
                                                    <tr>
                                                        <td>${status.index+1}</td>
                                                        <td>${supplierList.supplierCode}</td>
                                                        <td>${supplierList.supplierName}</td>
                                                        <td>${supplierList.linkmanName}</td>
                                                        <td>${supplierList.linkmanTelphone}</td>
                                                        <td>${supplierList.isBindWechat}</td>
                                                    </tr>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test="${DACRfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                                <c:forEach var="preauditSupplier"
                                                           items="${DACRfqRequestComplexVo.preauditSupplierList}"
                                                           varStatus="status">
                                                    <tr>
                                                        <td>${status.index+1}</td>
                                                        <td>${preauditSupplier.supplierCode}</td>
                                                        <td>${preauditSupplier.supplierName}</td>
                                                        <td>${preauditSupplier.answerUsername}</td>
                                                        <td>${preauditSupplier.linkmanTelphone}</td>
                                                        <td>${supplierList.isBindWechat}</td>
                                                    </tr>
                                                </c:forEach>
                                            </c:if>
                                            </tbody>
                                        </table>--%>

                                        <div>
                                            <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="supplierListJqGrid"></table>
                                            <div class="col-md-12" id="supplierListjqGridPager"></div>
                                        </div>

                                        <input id="isSupplierRequirement" value="${DACRfqRequestComplexVo.rulesVo.isSupplierRequirement }" type="hidden">
                                        <c:if test="${DACRfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">
                                            <input id="supplierList" value='${supplierList}' type="hidden">
                                        </c:if>
                                        <c:if test="${DACRfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                            <input id="preauditSupplierList" value='${preauditSupplierList}' type="hidden">
                                        </c:if>
                                    </c:if>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">商务与技术条款</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-1">商务条款：</label>
                                        <span class="bler blu-88-nobg">
                                            ${DACRfqRequestComplexVo.requestVo.requestBusiTerms}
                                        </span>

                                    </div>
                                    <div class="clearfix"><label class="bler blu-1">技术条款：</label>
                                        <span class="bler blu-88-nobg">
                                            ${DACRfqRequestComplexVo.requestVo.requestTecTerms}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <c:if test="${DACRfqRequestComplexVo.requestVo.type !='6'}">
                        <div>
                            <div class="line-32 mt12">
                                <span class="left" style="font-size:16px; display:inline-block">角色设置</span>
                            </div>
                            <table class="table table-bordered align-md">
                                <thead>
                                <tr>
                                    <th width="50px">序号</th>
                                    <th>工号</th>
                                    <th>姓名</th>
                                    <th>部门</th>
                                    <th>职位</th>
                                    <th>查看</th>
                                    <th>管理</th>
                                    <th>财务管理</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${DACRfqRequestComplexVo.projectUserList}" varStatus="status">
                                    <tr data-clone="orginal">
                                        <td>${status.index+1}</td>
                                        <td>${item.jobCode}</td>
                                        <td>${item.userName}</td>
                                        <td>${item.userOffice}</td>
                                        <td>${item.userTitle}</td>
                                        <td>
                                            <input
                                                    <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if>
                                                    type="checkbox" disabled="disabled">
                                        </td>
                                        <td>
                                            <input
                                                    <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if>
                                                    type="checkbox" disabled="disabled">
                                        </td>
                                        <td>
                                            <input
                                                    <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if>
                                                    type="checkbox" disabled="disabled">
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </c:if>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">联系方式</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="mg-pacne clearfix" style="margin:0 15px;">
                                    <ul>
                                        <li>联系人：${DACRfqRequestComplexVo.contactsVo.linkmanName}</li>
                                        <li>联系电话：${DACRfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                        <li style="width:40%">
                                            交货地址： ${DACRfqRequestComplexVo.contactsVo.deliveryProvince}
                                            ${DACRfqRequestComplexVo.contactsVo.deliveryCity}
                                            ${DACRfqRequestComplexVo.contactsVo.deliveryArea}
                                            ${DACRfqRequestComplexVo.contactsVo.deliveryAddress}</li>
                                    </ul>

                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="mg-header5">
                        竞价单附件
                    </div>
                    <table class="table table-bordered align-md" style="margin-bottom:5px;">
                        <thead>
                        <tr>
                            <th width="50px">序号</th>
                            <th>文件名称</th>
                            <th width="100px">文件大小</th>
                            <th>说明</th>
                            <th width="100px">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="sum" value="1"/>
                        <c:forEach var="item" items="${DACRfqRequestComplexVo.attachmentsList}" varStatus="status">
                            <c:if test="${item.type == 'R'}">
                                <tr data-clone="orginal">
                                    <td data-order="true">${sum}</td>
                                    <td>${item.originalFilename}</td>
                                    <td>${item.fileSize}KB</td>
                                    <td>${item.fileDeclaration}</td>
                                    <td>
                                        <a href="${downloadAction}?id=${item.attachmentId}&file=${item.downloadFilename}"><i
                                                class="icon icon-download-alt green "></i>&nbsp;下载</a>
                                    </td>
                                </tr>
                                <c:set var="sum" value="${sum + 1}"/>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="red line-32" style="font-size:12px;">
                        注：附件内容可以是询比价文件和详细说明，询比价文件需通过报名审核后才能下载。每个不超过10M，支持jpg、jpeg、gif、png、doc、docx、xls、xlsx、pdf、dwg格式。
                    </div>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价单备注</span>
                        </div>
                        <div class="panel-body">
                            <div class="mg-pacne clearfix" style="margin:0 15px;">
                                ${DACRfqRequestComplexVo.requestVo.memo}
                            </div>
                        </div>
                    </div>
                    <c:if test="${rfqSupplierListVo.status=='1'||rfqSupplierListVo.status=='2'}">
                        <div class="text-right" id="hidd">
                            <c:if test="${DACRfqRequestComplexVo.rulesVo.needAssureMoney==1 and rfqSupplierListVo.assureFlag==0}">
                                <a href="#"><span class="btn btn-info font12" data-toggle="modal" data-target="#cancelMoney">缴纳保证金</span></a>
                            </c:if>
                            <a id="jjdtBtn">
                                <span class="btn btn-warning font12" onclick="startPriceformat()">
                                    <img src="${pageContext.request.contextPath}/images/freshaddon.png" style="width:40px; height:40px; margin-right: 5px; margin-top: -25px;">竞价大厅
                                </span>
                            </a>
                            <span class="btn btn-danger font12" data-toggle="modal" data-target="#cancelPrice">放弃报价</span>
                        </div>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="requestId" value="${DACRfqRequestComplexVo.requestVo.id}"/>
<input type="hidden" id="rules" value="${DACRfqRequestComplexVo.rulesVo.biddingMethod}"/>
<input type="hidden" id="needAssureMoney" value="${DACRfqRequestComplexVo.rulesVo.needAssureMoney}"/>
<input type="hidden" id="assureFlag" value="${rfqSupplierListVo.assureFlag}"/>
<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">放弃寻源单</h4>
            </div>
            <form id="giveUpQuo">
                <div class="modal-body">
                    <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px;">
                        <i class="icon icon-question-sign icon-la"></i>&nbsp;&nbsp;您确定要放弃该寻源单吗？
                    </div>
                    <div class="mg-row">
                        <span class="left-title">寻源类型：</span>
                        <span class="left-cont1">
                            竞价
                        </span>
                        <div>
                            <span class="left-title">寻源单号：</span>
                            <span class="left-cont1">
                                ${DACRfqRequestComplexVo.requestVo.unifiedRfqNum}
                            </span>
                        </div>
                    </div>
                    <div class="mg-row clearfix" style="margin-bottom:8px;">
                        <span class="left-title">寻源标题：</span>
                        <span class="left-cont1">
                            ${DACRfqRequestComplexVo.requestVo.title}
                        </span>
                        <div>
                            <span class="left-title">采购商：</span>
                            <span class="left-cont1">
                                ${DACRfqRequestComplexVo.requestVo.ouName}
                            </span>
                        </div>
                    </div>
                    <input name="requestId" type="hidden" value="${DACRfqRequestComplexVo.requestVo.id}">
                    <div class="mg-row">
                        <span class="left-title"><i class="r">*</i>放弃原因：</span>
                        <div class="right-paner">
                            <select name="giveUpReason" class="form-control">
                                <option value="品类无法供应">品类无法供应</option>
                                <option value="供应时间无法满足">供应时间无法满足</option>
                                <option value="太忙，无暇顾及">太忙，无暇顾及</option>
                                <option value="不想报了">不想报了</option>
                                <option value="EUR">其他原因，请输入</option>
                            </select>
                        </div>
                    </div>
                    <div class="mg-row" style="margin-bottom:0px;">
                        <span class="left-title"><i class="r">*</i>放弃说明：</span>
                        <div class="right-paner">
                            <textarea class="form-control" name="giveUpMemo" rows="6" placeholder="" maxlength="2000"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary give-up">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<%--缴纳保证金--%>
<div class="modal fade" id="cancelMoney">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">缴纳保证金</h4>
            </div>
            <form id="payCashForm" action="${pageContext.request.contextPath}/rfqSupplierList/updateRfqSupplierList" method="post">
                <div class="modal-body">
                    <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px;">
                        <i class="icon icon-question-sign icon-la"></i>&nbsp;&nbsp;确认缴纳保证金？
                    </div>

                    <input name="requestId" type="hidden" value="${DACRfqRequestComplexVo.requestVo.id}">
                    <input name="supplierCode" type="hidden" value="${rfqSupplierListVo.supplierCode}">
                    <div class="mg-row" style="margin-bottom:0px;">
                        <span class="left-title">保证金额：</span>
                        <div class="right-paner">
                            <input type="text" readonly="readonly" value="${rfqRulesVo.assureMoney}" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary give-up" id="payCash" data-dismiss="modal">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<c:if test="${rfqSupplierListVo.status=='1'||rfqSupplierListVo.status=='2'}">
    <script type="text/javascript">
        $('html,body').animate({scrollTop: $('#jjdtBtn').offset().top}, 300);
    </script>
</c:if>
</body>
</html>
