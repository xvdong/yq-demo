<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RfqApproval管理</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqApproval/rfqApproval.js?v=${version}"></script>
</head>
<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <div class="sidebar clearfix">
            <div class="container">
                <c:import url="../common/menu.jsp">
                    <c:param name="menuNo" value="1" />
                </c:import>
            </div>
        </div>
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading">
                            审批流详情
                        </div>
                        <ul class="list-group">
                            <label class="control-label">审批流编号</label>
                            <input type="text" name='approvalCode' id='approvalCode' class='form-control' value="${rfqApprovalVo.approvalCode}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">审批流类型</label>
                            <input type="text" name='approvalType' id='approvalType' class='form-control' value="${rfqApprovalVo.approvalType}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">组织</label>
                            <input type="text" name='organization' id='organization' class='form-control' value="${rfqApprovalVo.organization}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">审批流名称</label>
                            <input type="text" name='approvalName' id='approvalName' class='form-control' value="${rfqApprovalVo.approvalName}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">审批流条件</label>
                            <input type="text" name='approvalCondition' id='approvalCondition' class='form-control' value="${rfqApprovalVo.approvalCondition}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">创建人</label>
                            <input type="text" name='founder' id='founder' class='form-control' value="${rfqApprovalVo.founder}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">创建日期</label>
                            <input type="text" name='createTime' id='createTime' class='form-control' value="${rfqApprovalVo.createTime}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">修改人</label>
                            <input type="text" name='updateFounder' id='updateFounder' class='form-control' value="${rfqApprovalVo.updateFounder}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">修改时间</label>
                            <input type="text" name='updateTime' id='updateTime' class='form-control' value="${rfqApprovalVo.updateTime}">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
