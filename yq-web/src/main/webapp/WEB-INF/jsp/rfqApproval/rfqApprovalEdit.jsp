<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>审批流设置表管理</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqApproval/rfqApproval.js?v=${version}"></script>
</head>
<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <div class="sidebar clearfix">
            <div class="container">
                <c:import url="../common/menu.jsp">
                    <c:param name="menuNo" value="1" />
                </c:import>
            </div>
        </div>
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">

                    <div class='example'>
                        <form class="form-horizontal" id="saveApproval" role="form" method='post' action="editRfqApproval" onsubmit="return false;">
                            <legend>
                                <c:choose>
                                    <c:when test="${userBo.oper == 'add'}">新增审批流</c:when>
                                    <c:otherwise>编辑审批流</c:otherwise>
                                </c:choose>
                            </legend>
                            <input type="hidden" id="oper" name="oper" value="${rfqApprovalBo.oper}"/>
                            <input type="hidden" id="approvalCode" name="approvalCode" value="${rfqApprovalVo.approvalCode}"/>
                            <div class="form-group">
                                <label class="col-md-2 control-label">审批流名称</label>
                                <div class="col-md-4">
                                    <input type='text' name='approvalName' id='approvalName' class='form-control' value="${rfqApprovalVo.approvalName}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">审批流类型</label>
                                <div class="col-md-4">
                                    <select name="approvalType" id='approvalType' class="form-control">
                                        <option value="1">创建审批</option>
                                        <option value="2" <c:if test="${'2'.equals(rfqApprovalVo.approvalType)}"> selected="selected"</c:if>>多轮报价审批</option>
                                        <option value="3" <c:if test="${'3'.equals(rfqApprovalVo.approvalType)}"> selected="selected"</c:if>>结果审批</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">审批流条件</label>
                                <div class="col-md-4">
                                    <input type='text' name='approvalCondition' id='approvalCondition' class='form-control' value="${rfqApprovalVo.approvalCondition}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <input type='submit' id='save' class='btn btn-primary' value='保存' data-loading='稍候...' />
                                    <input type='hidden' name='type' id='type' value='article' />
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
