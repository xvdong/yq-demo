<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>审批流列表</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />

    <script src="${pageContext.request.contextPath}/js/rfqApproval/rfqApproval.js?v=${version}"></script>

</head>

<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />

<div class="wrapper">

    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">

            <div class="container">

                <div class="breadcrumbs">

                    <ol class="breadcrumb">

                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>

                        <li class="active">审批流列表</li>

                    </ol>

                </div>

                <div class="page-content">

                    <div class="row">

                        <div class="col-md-12">

                            <div>
                                <div class="panel-body">
                                    <form id="searchForm" method="post" onsubmit="return false;">
                                        <div class="col-md-12">
                                            <div class="col-xs-2">
                                                <input name="approvalName" id="approvalName" type="text" class="form-control" placeholder="审批流名称">
                                            </div>
                                            <div class="col-xs-2">
                                                <input name="approvalCondition" id="approvalCondition" type="text" class="form-control" placeholder="审批流条件">
                                            </div>
                                            <%--<div class="col-xs-2">
                                                <input name="createTime" id="createTime" type="text" class="form-control" placeholder="创建日期">
                                            </div>--%>
                                            <div class="col-xs-2">
                                                <input name="founder" id="founder" type="text" class="form-control" placeholder="创建人">
                                            </div>
                                            <div class="col-xs-2">
                                                <input name="updateFounder" id="updateFounder" type="text" class="form-control" placeholder="修改人">
                                            </div>
                                            <%--<div class="col-xs-2">
                                                <input name="updateTime" id="updateTime" type="text" class="form-control" placeholder="修改时间">
                                            </div>--%>
                                            <div class="so-form-15">
                                                <button class="btn btn-primary btnSearch" type="button" id="btnSearch">
                                                    <i class="icon icon-search"></i> 搜索
                                                </button>
                                                <input class="btn ml12" type="reset" value="重置"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-content"></div>

                                <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                                <div class="col-md-12" id="jqGridPager"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

<script>

    $(function () {

        $('.more-so').click(function () {

            $('.hiderow').stop().slideToggle('fast');

        });


        //日期

        $(".form-datetime").datetimepicker(
                {

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    forceParse: 0,

                    showMeridian: 1,

                    format: "yyyy-mm-dd hh:ii"

                });


        $(".form-date").datetimepicker(
                {

                    language: "zh-CN",

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    minView: 2,

                    forceParse: 0,

                    format: "yyyy-mm-dd"

                });


        $(".form-time").datetimepicker({

            language: "zh-CN",

            weekStart: 1,

            todayBtn: 1,

            autoclose: 1,

            todayHighlight: 1,

            startView: 1,

            minView: 0,

            maxView: 1,

            forceParse: 0,

            format: 'hh:ii'

        });


    });

</script>

</body>

</html>
