<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>竞价大厅</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${pageContext.request.contextPath}/js/dacQuotation/dacQuotationToallDetail.js?v=${version}"></script>
    <link href="${pageContext.request.contextPath}/css/auctionroomtotall.css?v=${version}" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/flipclock.css?v=${version}" rel="stylesheet">
</head>
<body>
<div class="top_nav h70">
    <div class="container">
        <div class="row">
            <div class="bj-logo">
                <a href="#"></a>
            </div>
            <div class="bj-chuizi">
                <img draggable="false" src="${pageContext.request.contextPath}/images/bj-chuizi.png">
            </div>
            <c:choose>
                <c:when test="${quotationBaseVo.type =='3'}">
                    <div class="pull-right headtime" id="clock">
                        <span class="left">距离试竞价结束时间还有</span>
                        <div class="clock"></div>
                    </div>
                </c:when>
            </c:choose>
        </div>
    </div>
</div>
<div class="bj-head bj-headmt">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pull-left">
                <ul class="topmenu bj-zhuangtai t20">
                    <li class="grayblue">当前状态</li>
                    <li>
                        <c:choose>
                           <c:when test="${quotationBaseVo.type =='3'}">试竞价</c:when>
                           <c:when test="${quotationType =='4'}">已放弃</c:when>
                           <c:when test="${quotationType =='7'}">已作废</c:when>
                           <c:otherwise>结果待发布</c:otherwise>
                        </c:choose>
                    </li>
                    <li><a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${quotationBaseVo.id}" target="caigoufang-jingjiadanxiangqing" class="grayblue border-text">竞价单详情</a></li>
                </ul>
            </div>
            <div class="pull-right mr12">
                <ul class="topmenu t20-right">
                    <li class="font14">
                        <span>采购单位:</span>&nbsp&nbsp<span>${quotationBaseVo.ouName}</span>
                    </li>
                    <li class="font14">
                        <span>竞价标题:</span>&nbsp&nbsp<span>${quotationBaseVo.title}</span>
                    </li>
                </ul>
            </div>
            <c:choose>
            <c:when test="${quotationBaseVo.type =='3'}">
                <div class="pull-right mr12">
                    <ul class="topmenu t20-right">
                        <li  class="font14">
                            <span>系统时间:</span>
                        </li>
                        <li class="font14">
                            <span class="serverTime" style="display: inline-block;"></span>
                        </li>
                    </ul>
                </div>
            </c:when>
            </c:choose>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container">
        <c:choose>
            <c:when test="${quotationBaseVo.type =='3'}">
                <div class="alert alert-danger clearfix mg-alert-danger">
                    <i class="icon icon-exclamation-sign mt3 ml5"></i>
                    <div class="pull-left ml12">
                        <p>当前为试竞价模式！</p>
                        <p>本数据不作为最终生效数据！</p>
                    </div>
                    <a href="#" class="close" style="font-size:26px; height:45px; line-height:45px;" data-dismiss="alert">×</a>
                </div>
            </c:when>
        </c:choose>
        <div class="row mt10r">
            <div class="panel chujia-left">
                <div class="panel-heading clearfix font-16px">
                    竞价规则
                </div>
                <div class="panel-body font14">
                    <div class="alert alert-warning chujia-left-head">
                        <i class="icon-warning-sign"></i>
                        竞价规则，请务必阅读！
                        <a href="javascript:;" style="color: #75BDED;">关于竞价规则的帮助 &nbsp; </a>
                        <c:choose>
                        <c:when test="${quotationBaseVo.type =='3'}">
                            本次竞价为含税竞价
                        </c:when>
                    </c:choose>
                    </div>
                    <div class="clearfix cj-quot c666">
                        <ul>
                            <li>竞价开始时间：<span>${fn:substring(quotationBaseVo.startDate,0,16)}</span></li>
                            <li>竞价结束时间：<span>${fn:substring(quotationBaseVo.quotationEndDate,0,16)}</span></li>
                            <%--<c:choose>
                                <c:when test="${ not empty quotationBaseVo.lastTime && not empty quotationBaseVo.totalNum}">
                                    <li>最后${quotationBaseVo.lastTime}分钟: 可竞价<span>${quotationBaseVo.totalNum}次</span></li>
                                </c:when>
                                <c:otherwise>--%>
                                    <li>超时竞价延时：<span>${quotationBaseVo.quotationUpperDate}分钟</span></li>
                               <%-- </c:otherwise>
                            </c:choose>--%>
                            <li>起拍价：
                                <c:if test="${ not empty quotationBaseVo.startPrice}">
                                    <span id="startPrice">${quotationBaseVo.startPrice}</span>元
                                </c:if>
                            </li>
                            <li>最小降价幅度（梯度）：
                                <c:if test="${ not empty quotationBaseVo.priceGrad}">
                                    <span>${quotationBaseVo.priceGrad}</span>元
                                </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel bj-right chujia-right">
                <div class="panel-heading clearfix font-16px">
                    竞价详情
                </div>
                <div class="panel-body">
                    <c:choose>
                        <c:when test="${quotationBaseVo.type =='3'}">
                            <div class="cj-rr" style="z-index:19;">
                                <button class="btn btn-info" data-toggle="modal" data-target="#chuPrice" id="jingjia" onclick="quotation()"><span>¥</span>竞价</button>
                                <button class="btn btn-info" id="reload">刷新</button>
                            </div>
                        </c:when>
                    </c:choose>
                    <div class="h20m">
                        <c:choose>
                            <c:when test="${myQuotationVo.viewLowerestPriceFlag == '1'}">
                                <strong class="col-md-3 min-180">当前最低价：</strong>
                                <span class="col-md-3 min-180 pricelowSign">
                                    <c:if test="${ not empty myQuotationVo.lowerestPrice}">
                                        <span id="pricelow">${myQuotationVo.lowerestPrice}</span>元
                                    </c:if>
                                </span>
                                <span id="pricelow-d" class="red"></span>
                            </c:when>
                            <c:otherwise>
                                <strong class="col-md-3 min-180">我的排名：</strong>
                                <span class="col-md-3 rankSign">
                                    <c:if test="${ not empty myQuotationVo.lowerestPrice}">
                                        第<span id="rank">${myQuotationVo.lowerestPrice}</span>名
                                    </c:if>
                                </span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="h20m">
                        <strong class="col-md-3 min-180">梯度：</strong>
                        <span class="col-md-3">
                            <c:if test="${ not empty quotationBaseVo.priceGrad}">
                                <span id="step">${quotationBaseVo.priceGrad}</span>元
                            </c:if>
                        </span>
                    </div>
                    <div class="h20m">
                        <strong class="col-md-3 min-180">我的上一次出价：</strong>
                        <span class="col-md-3 min-180 pricepreSign">
                            <c:if test="${ not empty myQuotationVo.lastPrice}">
                                <span id="pricepre">${myQuotationVo.lastPrice}</span>元
                            </c:if>
                        </span>
                        <span id="pricepre-d" class="red"></span>
                    </div>
                    <c:choose>
                        <c:when test="${quotationBaseVo.type =='3'}">
                            <div class="h40">
                                <div class="row">
                                    <strong class="col-md-3 min-180">快速降N倍：</strong>
                                    <c:if test="${ not empty quotationBaseVo.priceGrad}">
                                        <div class="col-md-9">
                                            <i class="icon icon-minus-sign cj-jian jianp"></i>
                                            <input class="form-control" type="text" min="0" placeholder="" value="0" id="stepn">
                                            <i class="icon icon-plus-sign cj-jia jiap"></i><span class="text1">倍</span>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="h40">
                                <div class="row">
                                    <strong class="col-md-3 min-180">本次拟出价：</strong>
                                    <div class="col-md-7">
                                        <c:choose>
                                            <c:when test="${ not empty quotationBaseVo.priceGrad}">
                                                <input class="form-control ml28" type="number" min="1" max="10" placeholder="" value="${myQuotationVo.lowerestPrice}" id="pricenow" disabled="true">
                                            </c:when>
                                            <c:otherwise>
                                                <input class="form-control ml28" type="number" min="1" max="10" placeholder="" value="${myQuotationVo.lowerestPrice}" id="pricenow">
                                            </c:otherwise>
                                        </c:choose>
                                        <span class="text1">元</span>
                                        <span id="pricenow-d" class="red ml12"></span>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
                </div>
            </div>
        </div>
        <input type="hidden" id="requestId" value="${quotationBaseVo.id}" />
        <input type="hidden" id="viewLowerestPriceFlag" value="${myQuotationVo.viewLowerestPriceFlag}" />
        <div class="row mt10r">
            <c:if test="${sysUserVo.loginOrgType == '2'&& quotationType != '0'}">
                <div class="panel-heading clearfix">
                    <div class=" pull-right">
                        <span class="btn btn-warning font12"  onclick="dacDownload('${quotationBaseVo.id}','1','${quotationType}')">下载物料明细</span>
                    </div>
                </div>
            </c:if>
            <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
            <div class="col-md-12" id="jqGridPager"></div>
        </div>

    </div>
</div>
<div class="modal fade" id="chuPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">竞价提示</h4>
            </div>
            <input type="hidden" id="isDeleted" value="1"/>
            <div class="modal-body c999">
                <div class="mg-row c666">
                    <div class="font-18px text-center">竞价提示</div>
                </div>
                <dl class="row">
                    <dt class="mg-row warn-big col-md-2">
                        <i class="icon-warning-sign"></i>
                    </dt>
                    <dd class="col-md-offset-2">
                        <div class="h20m row pricelowSign">
                            <strong class="col-md-3 min-180">当前最低价：</strong>
                            <span class="col-md-3"><span id="pricelow-t"></span></span>
                            <span class="red pricelow-td col-md-4"></span>
                        </div>
                        <div class="h20m row rankSign">
                            <strong class="col-md-3 min-180">我的排名：</strong>
                            <span class="col-md-3"><span id="rank-t"></span></span>
                        </div>
                        <div class="h20m row">
                            <strong class="col-md-3 min-180">我的上一次出价：</strong>
                            <span class="col-md-3"><span id="pricepre-t"></span></span>
                            <span class="red pricepre-td col-md-4">1</span>
                        </div>
                        <div class="h20m row">
                            <strong class="col-md-3 min-180 red">本次拟出价：</strong>
                            <strong class="col-md-3 min-160 red"><span id="pricenow-t"></span></strong>
                            <strong class="red pricenow-td col-md-4">1</strong>
                        </div>
                        <div class="h40">
                            <strong class="col-md-3 min-180">税率：</strong>
							<span class="col-md-3">
								<select id="extendField4" class="form-control" name="" style="width:100px;">
                                    <option value="" selected></option>
                                    <option value="6%">6%</option>
                                    <option value="7%">7%</option>
                                    <option value="10%">10%</option>
                                    <option value="11%">11%</option>
                                    <option value="13%">13%</option>
                                    <option value="17%">17%</option>
                                </select>
							</span>
                        </div>
                    </dd>
                </dl>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="confirmPrice()">确定</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="poiterModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">请仔细阅读竞拍协议</h4>
            </div>
            <div class="modal-body">
                <div class="alert with-icon pl20" style='margin-bottom: 0;'>
                    <!-- <i class="icon-warning-sign"></i> -->
                    <div class="content" style='text-indent: 25px; height: 200px; overflow-y: auto;'>
                        ${quotationBaseVo.memo}
                    </div>
                </div>
            </div>
            <div class="modal-footer" style='text-align: center; padding-top: 0;'>
                <button type="button" class="btn with-icon" data-dismiss="modal" disabled><span>请仔细阅读条款，同意请点击 “确定”，等待<strong class="timr"
                                                                                                                             style='color: red; margin: 0 5px;'>7</strong>秒</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/js/flipclock.js?v=${version}"></script>
<script>
    function quotation(){
        if ($("#viewLowerestPriceFlag").val() == '1') {
            $('.rankSign').hide();
            if ($("#pricelow").html() !== undefined) {
                $("#pricelow-t").html($("#pricelow").html()+'元');
            }
        } else {
            $('.pricelowSign').hide();
            if ($("#rank").html() !== undefined) {
                $("#rank-t").html('第'+$("#rank").html()+'名');
            }
        }
        if($("#pricepre").html()!== undefined){
            $("#pricepre-t").html($("#pricepre").html()+'元');
        }else{
            $("#pricepre-t").html($("#pricepre").html());
        }
        if($("#pricenow").val()!== undefined){
            $("#pricenow-t").html($("#pricenow").val()+'元');
        }else{
            $("#pricenow-t").html($("#pricenow").val());
        }
        $(".pricelow-td").html(jqNumToMoney($("#pricelow-t").html().replace('元','')));
        $(".pricepre-td").html(jqNumToMoney($("#pricepre-t").html().replace('元','')));
        $(".pricenow-td").html(jqNumToMoney($("#pricenow-t").html().replace('元','')));
    }
    $("#reload").click(function(){
        reloadePrice();
    });

    var arr1 = [];
    var inip = 0.0;
    var stepmax = 0;
    $(function(){
        priceArr();
        pricenowState();
        var time = null;
        var time2 = null;
        $("#pricelow-d").html(jqNumToMoney($("#pricelow").html()));
        $("#pricepre-d").html(jqNumToMoney($("#pricepre").html()));
        $("#pricenow-d").html(jqNumToMoney($("#pricenow").val()));

        function assignment(){
            if (inip !== Infinity) {
                $("#pricenow").val((inip-$('#stepn').val()*$("#step").html()).toFixed(4));
                $("#pricelow-d").html(jqNumToMoney($("#pricelow").html()));
                $("#pricepre-d").html(jqNumToMoney($("#pricepre").html()));
                $("#pricenow-d").html(jqNumToMoney($("#pricenow").val()));
            }
        }
        function addp(){
            if($('#stepn').val()< stepmax){
                $('#stepn').val(parseInt($('#stepn').val())+1);
                assignment();
            }
        }
        function delp(){
            $('#stepn').val(parseInt($('#stepn').val())-1);
            assignment();
        }

        //倍数+++
        $('.jiap').click(function(){
            clearInterval(time);
            addp();
        });
        $('.jiap').mousedown(function(){
            clearInterval(time);
            time = setInterval(function(){
                addp();
            },150)
        });
        $('.jiap').mouseup,$('.jiap').mouseout(function(){
            clearInterval(time);
        });

        //倍数---
        $('.jianp').click(function(){
            clearInterval(time);
            if($('#stepn').val()>0){
                delp();
            }
        });
        $('.jianp').mousedown(function(){
            clearInterval(time);
            time = setInterval(function(){
                if($('#stepn').val()>0){
                    delp();
                }
            },150)
        });
        $('.jianp').mouseup,$('.jianp').mouseout(function(){
            clearInterval(time);
        });

        //输入检测
//		$("#stepn").keydown(function(){
//			if($("#stepn").val() == "0"){
//				$("#stepn").val("");
//			}
//		})
        $("#stepn").keyup(function(){
            clearTimeout(time2);
            assignment();
            if($('#stepn').val()> stepmax){
                $('#stepn').val(stepmax);
                assignment();
                $.zui.messager.show('最大倍数'+stepmax, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
            time2 = setTimeout(function(){
                $("#stepn").val(parseFloat($("#stepn").val()))
            },1000);
        });
        $("#stepn").focus(function(){
            this.style.imeMode = 'disabled';
            if($("#stepn").val() == "0"){
                $("#stepn").val("");
            }
        });
        $("#stepn").blur(function(){
            if($("#stepn").val() != ""){
                $('#stepn').val(parseFloat($('#stepn').val()));
            }
            if($('#stepn').val() == ""){
                $('#stepn').val(0);
            }
        });
        //本次拟出价校验(梯度不存在时本次拟出价手动输入)
        if ($("#step").val() === undefined) {
            $("#pricenow").attr("onkeyup","checkPriceNow ()");
            $("#pricenow").attr("onafterpaste","checkPriceNow ()");
        }
        $.ajax({
            type: "POST",
            url: ctx + '/common/getNowTime',
            success: function (data) {
                setInterval(function () {
                    data++;
                    $('.serverTime').html(getLocalTime(data));
                }, 1000)
            }
        });
    });

    //出价校验
    function checkPriceNow () {
        var pricenow = $("#pricenow").val();
        var flag = true;
        if ($("#startPrice").html() !== undefined && Number(pricenow) > Number($("#startPrice").html())) {
            $.zui.messager.show('本次拟出价不可大于起拍价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
        }
        if ($("#pricelow").html() !== undefined && Number(pricenow) > Number($("#pricelow").html())) {
            $.zui.messager.show('本次拟出价不可大于当前最低价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
            flag = false;
        }
        if ($("#pricepre").html() !== undefined && Number(pricenow) > Number($("#pricepre").html())) {
            $.zui.messager.show('本次拟出价不可大于我的上一次出价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
            flag = false;
        }
        if (pricenow == '' || Number(pricenow) < 0) {
            $.zui.messager.show('本次拟出价不可小于0！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
            flag = false;
        }
        if (flag === false) {
            $("#jingjia").replaceWith('<button class="btn btn-info errbackground"id="jingjia"><span>￥</span>竞价</button>');
        } else {
            $("#jingjia").replaceWith('<button class="btn btn-info" data-toggle="modal" data-target="#chuPrice" id="jingjia" onclick="quotation()"><span>￥</span>竞价</button>');
            $("#pricenow-d").html(jqNumToMoney(pricenow));
        }

    }
    //输入判断-只能输入数字
    var num =  $("#stepn").val();

    //调用方法
    $("#stepn").keyup(function(){
        var reg=/^([1-9]\d{0,}|[0])$/;
        var num2 =  $("#stepn").val();
        console.log(reg.test(num))
        this.style.imeMode = 'disabled';
        if(reg.test(num2)){
            $("#stepn").val(num2);
            num =  $("#stepn").val();
        }else{
            $("#stepn").val(num);
        }
    });

    //数字大写转换
    function jqNumToMoney(n) {
        if (n !== undefined) {
            var fraction = ['角','分'];
            var digit = ['零','壹','贰','叁','肆','伍','陆','柒','捌','玖'];
            var unit = [['元','万','亿'],['','拾','百','千']];
            var head = n < 0?'欠':'';
            n = Math.abs(n);
            var s = '';
            for (var i = 0; i < fraction.length; i++) {
                s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
            }
            s = s || '';
            n = Math.floor(n);
            for (var i = 0; i < unit[0].length && n > 0; i++) {
                var p = '';
                for (var j = 0; j < unit[1].length && n > 0; j++) {
                    p = digit[n % 10] + unit[1][j] + p;
                    n = Math.floor(n / 10);
                }
                s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
            }
            return head + s.replace(/(零.)*零元/,'元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整');
        }
    }

    function getLocalTime(St) {
        var dt = new Date(parseInt(St) * 1000);
        var date=dt.getFullYear() + "-" + dt.getMonth() + "-" + dt.getDate() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        return date;
    }

    var clock = $('.clock').FlipClock({
        autoStart: false,
        countdown:true,
        callbacks: {
            stop: function() {
                //到0回调函数
                $.ajax({
                   type:'POST',
                    url:"deleteQuotation",
                    data:{
                        requestId:$("#requestId").val(),
                         },
                    success: function (data) {
                        if (data.status == 'success') {
                            $(".wrapper").html("<center><div  style='padding:15px 60px;border:1px ; text-align:center; margin:5% auto 5%;font-size:34px;font-family:楷体'>"
                                    +"<div id='ShowDiv'></div>" +
                                    "<div><p><span style='font-size: 25px'>如不能跳转请点击</span><a href=${pageContext.request.contextPath}'/rfqQuotation/init' style='color: red; text-decoration:none' target='_self'>报价列表</a></p></div>"
                                    +"<div><strong>进行操作</strong></div>"+
                                    "</div></center>");
                            Load();
                        }else {
                            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                        }
                    }
                });
            }
        }
    });
    //倒计时的秒数
    var secs=5;
    function Load(){
        for(var i=secs;i>=0;i--)
        {
            window.setTimeout('doUpdate(' + i + ')', (secs-i) * 1000);
        }
    }
    function doUpdate(num) {
        document.getElementById('ShowDiv').innerHTML = "试竞价已结束，系统将在<span style='color: red;'>"+ num +"</span>秒后自动跳转至报价列表" ;
        if(num == 0) {
            location.href=ctx+"/rfqQuotation/init";
        }
    };
    clock.setTime(${quotationBaseVo.countDown});
    clock.start();
    $('.minutes').html('时');
    $('.seconds').html('分');
    $('.clock').append('<span class="flip-clock-divider">秒</span>')
    $('.flip:nth-last-child(6)').after('<span class="flip-clock-divider">时</span>');
    $('.flip:nth-last-child(4)').after('<span class="flip-clock-divider">分</span>');

    $('#clock').data("_flipclock", clock);

    /**
     * 竞价单备注
     */
    $('#poiterModal').on('shown.zui.modal', function () {
        var t = 6;
        var timerpoiter = setInterval(function () {
            if (t < 0) {
                $('.timr').parent().parent().prop("disabled", false);
                $('.timr').parent().parent().removeClass('with-icon');
                $('.timr').parent().parent().addClass('btn-primary');
                $('.timr').parent().parent().text('确　定');
                clearInterval(timerpoiter);
            } else {
                $('.timr').text(t);
                t -= 1;
            }
        }, 1000)
    });
    $('#poiterModal').modal({show: true, backdrop: 'static', keyboard: false});
</script>
</body>
</html>