<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>竞价大厅</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${pageContext.request.contextPath}/js/dacQuotation/dacQuotationToallDetail.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/lib/plupload-2.1.9/js/plupload.full.min.js" type="text/javascript"></script>
    <script src="${resource}/js/upload/uploadFile.js?v=${version}" type="text/javascript" ></script>
    <link href="${pageContext.request.contextPath}/css/auctionroomtotall.css?v=${version}" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/flipclock.css?v=${version}" rel="stylesheet">
</head>
<body>
<div class="top_nav h70">
    <div class="container">
        <div class="row">
            <div class="bj-logo">
                <a href="#"></a>
            </div>
            <div class="bj-chuizi">
                <img draggable="false" src="${pageContext.request.contextPath}/images/bj-chuizi.png">
            </div>
            <div class="pull-right headtime" id="clock">
                <span class="left">距离竞价结束时间还有</span>
                <div class="clock"></div>
            </div>
        </div>
    </div>
</div>
<div class="bj-head bj-headmt">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pull-left">
                <ul class="topmenu bj-zhuangtai t20">
                    <li class="grayblue">当前状态</li>
                    <li>竞价中</li>
                    <li><a onclick="javascript:show('${quotationBaseVo.id}');" target="caigoufang-jingjiadanxiangqing" class="grayblue border-text">竞价单详情</a></li>
                </ul>
            </div>
            <div class="pull-right mr12">
                <ul class="topmenu t20-right">
                    <li class="font14">
                        <span>采购单位:</span>&nbsp&nbsp<span>${quotationBaseVo.ouName}</span>
                    </li>
                    <li class="font14">
                        <span>竞价标题:</span>&nbsp&nbsp<span>${quotationBaseVo.title}</span>
                    </li>
                </ul>
            </div>
            <div class="pull-right mr12">
                <ul class="topmenu t20-right">
                    <li  class="font14">
                        <span>系统时间:</span>
                    </li>
                    <li class="font14">
                        <span class="serverTime" style="display: inline-block;"></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container">
        <div class="row mt10r">
            <div class="panel chujia-left">
                <div class="panel-heading clearfix font-16px">
                    竞价规则
                </div>
                <div class="panel-body font14">
                    <div class="alert alert-warning chujia-left-head">
                        <i class="icon-warning-sign"></i>
                        竞价规则，请务必阅读！
                        <a href="javascript:;" style="color: #75BDED;">关于竞价规则的帮助 &nbsp; </a>本次竞价为含税竞价
                    </div>
                    <div class="clearfix cj-quot c666">
                        <ul>
                            <li>竞价开始时间：<span>${fn:substring(quotationBaseVo.startDate,0,16)}</span></li>
                            <li>竞价结束时间：<span>${fn:substring(quotationBaseVo.quotationEndDate,0,16)}</span></li>
                            <c:choose>
                                <c:when test="${ not empty quotationBaseVo.lastTime && not empty quotationBaseVo.totalNum}">
                                    <li>最后${quotationBaseVo.lastTime}分钟: 可竞价<span>${quotationBaseVo.totalNum}次</span></li>
                                </c:when>
                                <c:otherwise>
                                    <li>超时竞价延时：<span>${quotationBaseVo.quotationUpperDate}分钟</span></li>
                                </c:otherwise>
                            </c:choose>
                            <li>起拍价：
                            <c:if test="${ not empty quotationBaseVo.startPrice}">
                                <span id="startPrice">${quotationBaseVo.startPrice}</span>元
                            </c:if>
                            </li>
                            <li>最小降价幅度（梯度）：
                                <c:if test="${ not empty quotationBaseVo.priceGrad}">
                                    <span>${quotationBaseVo.priceGrad}</span>元
                                </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel bj-right chujia-right">
                <div class="panel-heading clearfix font-16px">
                    竞价详情
                </div>
                <div class="panel-body">
                    <div class="cj-rr" style="z-index:19;">
                        <button class="btn btn-info" data-toggle="modal" data-target="#chuPrice" id="jingjia" onclick="quotation()"><span>¥</span>竞价</button>
                        <button class="btn btn-info" id="reload">刷新</button>
                    </div>
                    <div class="h20m">
                        <c:choose>
                            <c:when test="${myQuotationVo.viewLowerestPriceFlag == '1'}">
                                <strong class="col-md-3 min-180">当前最低价：</strong>
                                <span class="col-md-3 min-180 pricelowSign">
                                    <c:if test="${ not empty myQuotationVo.lowerestPrice}">
                                        <span id="pricelow">${myQuotationVo.lowerestPrice}</span>元
                                    </c:if>
                                </span>
                                <span id="pricelow-d" class="red"></span>
                            </c:when>
                            <c:otherwise>
                                <strong class="col-md-3 min-180">我的排名：</strong>
                                <span class="col-md-3 rankSign">
                                    <c:if test="${ not empty myQuotationVo.lowerestPrice}">
                                        第<span id="rank">${myQuotationVo.lowerestPrice}</span>名
                                    </c:if>
                                </span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="h20m">
                        <strong class="col-md-3 min-180">梯度：</strong>
                        <span class="col-md-3">
                            <c:if test="${ not empty quotationBaseVo.priceGrad}">
                                <span id="step">${quotationBaseVo.priceGrad}</span>元
                            </c:if>
                        </span>
                    </div>
                    <div class="h20m">
                        <strong class="col-md-3 min-180">我的上一次出价：</strong>
                        <span class="col-md-3 min-180 pricepreSign">
                            <c:if test="${ not empty myQuotationVo.lastPrice}">
                                <span id="pricepre">${myQuotationVo.lastPrice}</span>元
                            </c:if>
                        </span>
                        <span id="pricepre-d" class="red"></span>
                    </div>
                    <div class="h40">
                        <div class="row">
                            <strong class="col-md-3 min-180">快速降N倍：</strong>
                            <c:if test="${ not empty quotationBaseVo.priceGrad}">
                                <div class="col-md-9">
                                    <i class="icon icon-minus-sign cj-jian jianp"></i>
                                    <input class="form-control" type="text" min="0" placeholder="" value="0" id="stepn">
                                    <i class="icon icon-plus-sign cj-jia jiap"></i><span class="text1">倍</span>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="h40">
                        <div class="row">
                            <strong class="col-md-3 min-180">本次拟出价：</strong>
                            <div class="col-md-7">
                                <c:choose>
                                    <c:when test="${ not empty quotationBaseVo.priceGrad}">
                                        <input class="form-control ml28" type="number" min="1" max="10" placeholder="" value="${myQuotationVo.lowerestPrice}" id="pricenow" disabled="true">
                                    </c:when>
                                    <c:otherwise>
                                        <input class="form-control ml28" type="number" min="1" max="10" placeholder="" value="${myQuotationVo.lowerestPrice}" id="pricenow">
                                    </c:otherwise>
                                </c:choose>
                                <span class="text1">元</span>
                                <span id="pricenow-d" class="red ml12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="requestId" value="${quotationBaseVo.id}" />
        <input type="hidden" id="viewLowerestPriceFlag" value="${myQuotationVo.viewLowerestPriceFlag}" />
        <div class="row mt10r">
            <c:if test="${sysUserVo.loginOrgType == '2'&& quotationType == '2'}">
                <div class="panel-heading clearfix">
                    <div class=" pull-right">
                        <span class="btn btn-warning font12"  onclick="dacDownload('${quotationBaseVo.id}','1','${quotationType}')">下载物料明细</span>
                    </div>
                </div>
            </c:if>
            <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
            <div class="col-md-12" id="jqGridPager"></div>
        </div>
        <form id="formId" method="post" style="margin-bottom: 30px;" class="row mt10r" action="${ctx}/dacQuotationTotal/dacQuotationTotaltext">
            <div class="panel mt20">
                <div class="panel-heading clearfix">
                    <span class="mg-header5 pull-left">报价附件</span>
                                <span class="pull-right">
                                 <a class="blue attachmentBtn" href="#" data-toggle="modal" data-target="#myModa2"
                                    data-type="Q">
                                     <label class="btn btn-primary btn-sm"><i
                                             class="icon icon-plus"></i>&nbsp;新增附件</label>
                                 </a>
                            </span>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <table class="table table-bordered align-md fixed">
                            <thead>
                            <tr>
                                <th width="20%">序号</th>
                                <th width="20%">文件名称</th>
                                <th width="20%">文件大小</th>
                                <th width="20%">说明</th>
                                <th width="20%">操作</th>
                            </tr>
                            </thead>
                            <tbody id="attachmentList1">
                            <c:set var="sum" value="1"/>
                            <c:forEach var="item" items="${rfqQuotationComplexVo.attachmentsList}"
                                       varStatus="status">
                                <c:if test="${item.type == 'Q'}">
                                    <tr data-clone="orginal">
                                        <td data-name="downloadUrl" data-value="${item.downloadUrl}"
                                            data-order="true"> ${sum}
                                            <input type="hidden" data-name="originalFileType"
                                                   data-value="${item.originalFileType}"/>
                                            <input type="hidden" data-name="originalFilename"
                                                   data-value="${item.originalFilename}"/>
                                            <input type="hidden" data-name="type" data-value="Q"/>
                                        </td>
                                        <td data-name="originalFilename"
                                            data-value="${item.originalFilename}">${item.originalFilename}</td>
                                        <td data-name="fileSize"
                                            data-value="${item.fileSize}">${item.fileSize}KB
                                        </td>
                                        <td data-name="fileDeclaration"
                                            data-value="${item.fileDeclaration}">${item.fileDeclaration}</td>
                                        <td data-name="downloadFilename"
                                            data-value="${item.downloadFilename}">
                                            <a href="javascript:;" class="text-danger order-del-btn"
                                               data-key="${item.downloadFilename}_Q"><i
                                                    class="icon-trash"></i> 删除</a>
                                        </td>
                                    </tr>
                                    <c:set var="sum" value="${sum + 1}"/>
                                </c:if>
                            </c:forEach>
                            </tbody>
                        </table>
                        <span class="c999"  style="color: red;"> 附件类型不支持exe格式，文件大小不超过100M。              </span>
                    </div>

                </div>
            </div>
            <a href="javascript:savetexts();" id="achanger1" class="btn btn-lg btn-primary js-form-btn js-block pull-right" data-type="2" type="button">保存</a>
        </form>
    </div>
</div>
<div class="modal fade" id="chuPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">竞价提示</h4>
            </div>
            <input type="hidden" id="isDeleted" value=""/>
            <div class="modal-body c999">
                <div class="mg-row c666">
                    <div class="font-18px text-center">竞价提示</div>
                </div>
                <dl class="row">
                    <dt class="mg-row warn-big col-md-2">
                        <i class="icon-warning-sign"></i>
                    </dt>
                    <dd class="col-md-offset-2">
                        <div class="h20m row pricelowSign">
                            <strong class="col-md-3 min-180">当前最低价：</strong>
                            <span class="col-md-3"><span id="pricelow-t"></span></span>
                            <span class="red pricelow-td col-md-4"></span>
                        </div>
                        <div class="h20m row rankSign">
                            <strong class="col-md-3 min-180">我的排名：</strong>
                            <span class="col-md-3"><span id="rank-t"></span></span>
                        </div>
                        <div class="h20m row">
                            <strong class="col-md-3 min-180">我的上一次出价：</strong>
                            <span class="col-md-3"><span id="pricepre-t"></span></span>
                            <span class="red pricepre-td col-md-4">1</span>
                        </div>
                        <div class="h20m row">
                            <strong class="col-md-3 min-180 red">本次拟出价：</strong>
                            <strong class="col-md-3 min-160 red"><span id="pricenow-t"></span></strong>
                            <strong class="red pricenow-td col-md-4">1</strong>
                        </div>
                        <div class="h40">
                            <strong class="col-md-3 min-180">税率：</strong>
							<span class="col-md-3">
								<select id="extendField4" class="form-control" name="" style="width:100px;">
                                    <option value="" selected></option>
                                    <option value="6%">6%</option>
                                    <option value="7%">7%</option>
                                    <option value="10%">10%</option>
                                    <option value="11%">11%</option>
                                    <option value="13%">13%</option>
                                    <option value="17%">17%</option>
                                </select>
							</span>
                        </div>
                    </dd>
                </dl>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="confirmPrice()">确定</button>
            </div>
        </div>
    </div>
</div>
<!-- ///////////////////////新增附件2/////////////// -->
<div class="modal fade" id="myModa2" data-type="Q">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                          <form>
                              <input type="text"  id="fileName" placeholder="请上传,附件大小≤100M"  class="form-control no-border" />
                              <input type="button" id="upload" value="上传">
                              <span  id="msg">0%</span>
                          </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration" class="form-control" maxlength="1000"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary js-confirm" data-dismiss="modal" id="sure" disabled="disabled">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="poiterModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">请仔细阅读竞拍协议</h4>
            </div>
            <div class="modal-body">
                <div class="alert with-icon pl20" style='margin-bottom: 0;'>
                    <!-- <i class="icon-warning-sign"></i> -->
                    <div class="content" style='text-indent: 25px; height: 200px; overflow-y: auto;'>
                        ${quotationBaseVo.memo}
                    </div>
                </div>
            </div>
            <div class="modal-footer" style='text-align: center; padding-top: 0;'>
                <button type="button" class="btn with-icon" data-dismiss="modal" disabled><span>请仔细阅读条款，同意请点击 “确定”，等待<strong class="timr"
                                                                                                                             style='color: red; margin: 0 5px;'>7</strong>秒</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/js/flipclock.js?v=${version}"></script>
<script>
    $(document).ready(function () {

        //附件
        var types = [".jpg", ".jpeg", ".gif", ".png", ".doc", ".docx", ".xls", ".xlsx", ".pdf"];
        var maxSize = 104857600;//100M，单位:B

        if (navigator.appName == "Microsoft Internet Explorer") {
            $("#myModa2").on('show.zui.modal', function () {
                //初始化flash插件
                initPlupload2(maxSize, types);
                //关闭时清理页面
                initEvent();
            });
        } else {
            //初始化flash插件
            initPlupload(maxSize, types);
            //关闭时清理页面
            initEvent();
        }
    });

    var attachmentList = {}; //附件
    //附件上传
    //BEGIN
    (function () {
        var $myModa2 = $('#myModa2');
        var $attachmentList1 = $('#attachmentList1');
        var rowTmp = '<tr data-clone="orginal"> <td data-order="true">1</td> <td>{originalFilename}</td> <td>{fileSize}KB</td> <td>{fileDeclaration}</td> <td><a href="javascript:;" data-key="{downloadFilename}_{type}" class="text-danger order-del-btn"><i class="icon-trash"></i> 删除</a></td> </tr>';

        /*var uploadComponent = new UploadComponent({
         divId: 'container_doc',
         code: 'AAAA',
         documentId: '1111',
         containerId: 'container_doc_upload_form',
         fileType: 'txt,doc,docx,xls,xlsx,pdf,png,jpg,jpeg,gif,bmp',
         inputDiv: 'fileup',
         styleType: "normal",
         uploadUrl: ctx + "/common/uploadFile",
         success: function (data) {
         if(data.status == 'success'){
         var result = JSON.parse(data.attachmentsVo);
         $("#fileName").val(result.originalFilename);
         RFQ.d(result);
         $myModa2.data('_result', result);
         $myModa2.find('.js-confirm').attr('disabled',false);
         }else{
         $.zui.messager.show(data.message, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
         }
         }
         });*/

        /*$('a.attachmentBtn').on('click', function () {
         /!*uploadComponent.reset();*!/
         var fileEl = $myModa2.find(':file')[0];
         RFQ.clearFileInput(fileEl);
         $myModa2.find('.percent').html('0%');
         $myModa2.find('.js-confirm').attr('disabled',true);
         $myModa2.find('textarea').val('');
         $myModa2.data('_type', $(this).data('type'));
         $myModa2.data('_result','');
         });*/

        //当前存在的数据加入缓存
        $('#attachmentList1 tr').each(function () {
            var tmp = {};
            $(this).find('[data-name]').each(function () {
                var $this = $(this);
                var name = $this.data('name');
                if (!name) return true;
                tmp[name] = $this.data('value');
            });
            attachmentList[tmp.downloadFilename + '_' + tmp.type] = tmp;
        });

        $myModa2.on('click', '.js-confirm', function () {
            var type = $myModa2.data('type');
            var data = $myModa2.data('_result');
            /*var fileEl = $myModa2.find(':file');
             if(!fileEl.value){
             RFQ.warn('未选择上传文件');
             return false;
             }*/
            if(data==undefined || data==''){
                showErrorMsg("您还没选择文件！");
            }
            if(!data.fileSize){
                RFQ.warn('文件未上传完成，请重新上传');
                return false;
            }
            data.type = type;
            data.fileDeclaration = $myModa2.find('textarea').val();
            var $container = $attachmentList1;
            attachmentList[data.downloadFilename + '_' + data.type] = data;
            $container.append(rowTmp.format(data));
            $myModa2.modal('hide');
            resetSeq($container);
        });

        //删除
        $attachmentList1.on('click', '.order-del-btn', deleteHandle);

        function deleteHandle() {
            var $this = $(this);
            var key = $(this).data("key");
            $this.parent().parent("tr").remove();
            delete attachmentList[key];
            resetSeq($attachmentList1);
            RFQ.d(attachmentList);
        }

        //重新设置序号
        function resetSeq($tbody) {
            var $tds = $tbody.find('tr td:first-child');
            $tds.each(function (i, el) {
                $(this).html(i + 1);
            });
        }
    })();
    //END

    //表单提交
    function savetexts(){
        //alert("jinlaile-------------");
        var $this = $("#achanger1");
        var type = $this.data('type');
        var requestId = $("#requestId").val();
        var extras = {};
        //组装附件信息
        if (!$.isEmptyObject(attachmentList)) {
            var index = 0, tmp;
            for (var key in attachmentList) {
                tmp = attachmentList[key];
                extras['attachmentsList[' + index + '].requestId'] = requestId;
                extras['attachmentsList[' + index + '].seq'] = index + 1;
                extras['attachmentsList[' + index + '].type'] = tmp.type;
                extras['attachmentsList[' + index + '].downloadFilename'] = tmp.downloadFilename;
                extras['attachmentsList[' + index + '].fileSize'] = tmp.fileSize;
                extras['attachmentsList[' + index + '].originalFileType'] = tmp.originalFileType;
                extras['attachmentsList[' + index + '].originalFilename'] = tmp.originalFilename;
                extras['attachmentsList[' + index + '].downloadUrl'] = tmp.downloadUrl;
                extras['attachmentsList[' + index + '].fileDeclaration'] = tmp.fileDeclaration;
                index++;
            }
        }
        RFQ.form('#formId').ajaxSubmit(ctx + '/dacQuotationTotal/dacQuotationTotaltext?requestId='+requestId+'&type=' + type, extras, function () {
                RFQ.info('保存成功');
                location.href = ctx + "/dacQuotationTotal/init?requestId=" + requestId;
         });


    }

    function quotation(){
        if ($("#viewLowerestPriceFlag").val() == '1') {
            $('.rankSign').hide();
            if ($("#pricelow").html() !== undefined) {
                $("#pricelow-t").html($("#pricelow").html()+'元');
            }
        } else {
            $('.pricelowSign').hide();
            if ($("#rank").html() !== undefined) {
                $("#rank-t").html('第'+$("#rank").html()+'名');
            }
        }
        if($("#pricepre").html()!== undefined){
            $("#pricepre-t").html($("#pricepre").html()+'元');
        }else{
            $("#pricepre-t").html($("#pricepre").html());
        }
        if($("#pricenow").val()!== undefined){
            $("#pricenow-t").html($("#pricenow").val()+'元');
        }else{
            $("#pricenow-t").html($("#pricenow").val());
        }
        $(".pricelow-td").html(jqNumToMoney($("#pricelow-t").html().replace('元','')));
        $(".pricepre-td").html(jqNumToMoney($("#pricepre-t").html().replace('元','')));
        $(".pricenow-td").html(jqNumToMoney($("#pricenow-t").html().replace('元','')));
    }
    $("#reload").click(function(){
        reloadePrice();
    });

    var arr1 = [];
    var inip = 0.0;
    var stepmax = 0;
    $(function(){
        priceArr();
        pricenowState();
        var time = null;
        var time2 = null;
        $("#pricelow-d").html(jqNumToMoney($("#pricelow").html()));
        $("#pricepre-d").html(jqNumToMoney($("#pricepre").html()));
        $("#pricenow-d").html(jqNumToMoney($("#pricenow").val()));

        function assignment(){
            if (inip !== Infinity) {
                $("#pricenow").val((inip-$('#stepn').val()*$("#step").html()).toFixed(4));
                $("#pricelow-d").html(jqNumToMoney($("#pricelow").html()));
                $("#pricepre-d").html(jqNumToMoney($("#pricepre").html()));
                $("#pricenow-d").html(jqNumToMoney($("#pricenow").val()));
            }
        }
        function addp(){
            if($('#stepn').val()< stepmax){
                $('#stepn').val(parseInt($('#stepn').val())+1);
                assignment();
            }
        }
        function delp(){
            $('#stepn').val(parseInt($('#stepn').val())-1);
            assignment();
        }

        //倍数+++
        $('.jiap').click(function(){
            clearInterval(time);
            addp();
        });
        $('.jiap').mousedown(function(){
            clearInterval(time);
            time = setInterval(function(){
                addp();
            },150)
        });
        $('.jiap').mouseup,$('.jiap').mouseout(function(){
            clearInterval(time);
        });

        //倍数---
        $('.jianp').click(function(){
            clearInterval(time);
            if($('#stepn').val()>0){
                delp();
            }
        });
        $('.jianp').mousedown(function(){
            clearInterval(time);
            time = setInterval(function(){
                if($('#stepn').val()>0){
                    delp();
                }
            },150)
        });
        $('.jianp').mouseup,$('.jianp').mouseout(function(){
            clearInterval(time);
        });

        //输入检测
//		$("#stepn").keydown(function(){
//			if($("#stepn").val() == "0"){
//				$("#stepn").val("");
//			}
//		})
        $("#stepn").keyup(function(){
            clearTimeout(time2);
            assignment();
            if($('#stepn').val()> stepmax){
                $('#stepn').val(stepmax);
                assignment();
                $.zui.messager.show('最大倍数'+stepmax, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
            time2 = setTimeout(function(){
                $("#stepn").val(parseFloat($("#stepn").val()))
            },1000);
        });
        $("#stepn").focus(function(){
            this.style.imeMode = 'disabled';
            if($("#stepn").val() == "0"){
                $("#stepn").val("");
            }
        });
        $("#stepn").blur(function(){

            if($("#stepn").val() != ""){
                $('#stepn').val(parseFloat($('#stepn').val()));
            }
            if($('#stepn').val() == ""){
                $('#stepn').val(0);
            }
            clearTimeout(time2);
        });
        //本次拟出价校验(梯度不存在时本次拟出价手动输入)
        if ($("#step").val() === undefined) {
            $("#pricenow").attr("onkeyup","checkPriceNow ()");
            $("#pricenow").attr("onafterpaste","checkPriceNow ()");
        }
        $.ajax({
            type: "POST",
            url: ctx + '/common/getNowTime',
            success: function (data) {
                setInterval(function () {
                    data++;
                    $('.serverTime').html(getLocalTime(data));
                }, 1000)
            }
        });
    });

    //出价校验
    function checkPriceNow () {
        var pricenow = $("#pricenow").val();
        var flag = true;
        if ($("#startPrice").html() !== undefined && Number(pricenow) > Number($("#startPrice").html())) {
            $.zui.messager.show('本次拟出价不可大于起拍价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
        }
        if ($("#pricelow").html() !== undefined && Number(pricenow) > Number($("#pricelow").html())) {
            $.zui.messager.show('本次拟出价不可大于当前最低价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
            flag = false;
        }
        if ($("#pricepre").html() !== undefined && Number(pricenow) > Number($("#pricepre").html())) {
            $.zui.messager.show('本次拟出价不可大于我的上一次出价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
            flag = false;
        }
        if (pricenow == '' || Number(pricenow) < 0) {
            $.zui.messager.show('本次拟出价不可小于0！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pricenow").focus();
            flag = false;
        }
        if (flag === false) {
            $("#jingjia").replaceWith('<button class="btn btn-info errbackground"id="jingjia"><span>￥</span>竞价</button>');
        } else {
            $("#jingjia").replaceWith('<button class="btn btn-info" data-toggle="modal" data-target="#chuPrice" id="jingjia" onclick="quotation()"><span>￥</span>竞价</button>');
            $("#pricenow-d").html(jqNumToMoney(pricenow));
        }

    }
    //输入判断-只能输入数字
    var num =  $("#stepn").val();

    //调用方法
    $("#stepn").keyup(function(){
        var reg=/^([1-9]\d{0,}|[0])$/;
        var num2 =  $("#stepn").val();
        //console.log(reg.test(num))
        this.style.imeMode = 'disabled';
        if(reg.test(num2)){
            $("#stepn").val(num2);
            num =  $("#stepn").val();
        }else{
            $("#stepn").val(num);
        }
    });

    //数字大写转换
    function jqNumToMoney(n) {
        if (n !== undefined) {
            var fraction = ['角','分'];
            var digit = ['零','壹','贰','叁','肆','伍','陆','柒','捌','玖'];
            var unit = [['元','万','亿'],['','拾','百','千']];
            var head = n < 0?'欠':'';
            n = Math.abs(n);
            var s = '';
            for (var i = 0; i < fraction.length; i++) {
                s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
            }
            s = s || '';
            n = Math.floor(n);
            for (var i = 0; i < unit[0].length && n > 0; i++) {
                var p = '';
                for (var j = 0; j < unit[1].length && n > 0; j++) {
                    p = digit[n % 10] + unit[1][j] + p;
                    n = Math.floor(n / 10);
                }
                s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
            }
            return head + s.replace(/(零.)*零元/,'元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整');
        }
    }

    function getLocalTime(St) {
        var dt = new Date(parseInt(St) * 1000);
        var date=dt.getFullYear() + "-" + dt.getMonth() + "-" + dt.getDate() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        return date;
    }

    var clock = $('.clock').FlipClock({
        autoStart: false,
        countdown:true,
        callbacks: {
            stop: function() {
                reloadePrice();
                if(Number(clock.getTime().time)<=0){
                    $(".wrapper").html("<center><div  style='padding:15px 60px;border:1px ; text-align:center; margin:5% auto 5%;font-size:34px;font-family:楷体'>"
                            +"<div id='ShowDiv'></div>" +
                            "<div><p><span style='font-size: 25px'>如不能跳转请点击</span><a href=${pageContext.request.contextPath}'/rfqQuotation/init' style='color: red; text-decoration:none' target='_self'>报价列表</a></p></div>"
                            +"<div><strong>进行操作</strong></div>"+
                            "</div></center>");
                    Load();
                }
            }
        }
    });
    //倒计时的秒数
    var secs=5;
    function Load(){
        for(var i=secs;i>=0;i--)
        {
            window.setTimeout('doUpdate(' + i + ')', (secs-i) * 1000);
        }
    }
    function doUpdate(num) {
        document.getElementById('ShowDiv').innerHTML = "试竞价已结束，系统将在<span style='color: red;'>"+ num +"</span>秒后自动跳转至报价列表" ;
        if(num == 0) {
            location.href=ctx+"/rfqQuotation/init";
        }
    };
    clock.setTime(${quotationBaseVo.countDown});
    clock.start();
    $('.minutes').html('时');
    $('.seconds').html('分');
    $('.clock').append('<span class="flip-clock-divider">秒</span>')
    $('.flip:nth-last-child(6)').after('<span class="flip-clock-divider">时</span>');
    $('.flip:nth-last-child(4)').after('<span class="flip-clock-divider">分</span>');

    $('#clock').data("_flipclock",clock);

    /**
     * 竞价单备注
     */
    $('#poiterModal').on('shown.zui.modal', function () {
        var t = 6;
        var timerpoiter = setInterval(function () {
            if (t < 0) {
                $('.timr').parent().parent().prop("disabled", false);
                $('.timr').parent().parent().removeClass('with-icon');
                $('.timr').parent().parent().addClass('btn-primary');
                $('.timr').parent().parent().text('确　定');
                clearInterval(timerpoiter);
            } else {
                $('.timr').text(t);
                t -= 1;
            }
        }, 1000)
    });
    $('#poiterModal').modal({show: true, backdrop: 'static', keyboard: false});
</script>
</body>
