<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>自定义物料表管理</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqRequestItemExtra/rfqRequestItemExtra.js?v=${version}"></script>
</head>
<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <div class="sidebar clearfix">
            <div class="container">
                <c:import url="../common/menu.jsp">
                    <c:param name="menuNo" value="1" />
                </c:import>
            </div>
        </div>
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">

                    <div class='example'>
                        <form class="form-horizontal" id="saveItem" role="form" method='post' action="editRfqRequestItemExtra" onsubmit="return false;">
                            <legend>
                                <c:choose>
                                    <c:when test="${rfqRequestItemExtraBo.oper == 'add'}">新增自定义物料</c:when>
                                    <c:otherwise>编辑自定义物料</c:otherwise>
                                </c:choose>
                            </legend>
                            <input type="hidden" name="oper" value="${rfqRequestItemExtraBo.oper}"/>
                            <input type="hidden" name="id" value="${rfqRequestItemExtraBo.id}"/>
                        <%--   <div class="form-group">
                            <label class="col-md-2 control-label">采购组织ID</label>
                            <div class="col-md-4">
                                <input type='text' name='ouId' id='ouId' class='form-control' value="${rfqRequestItemExtraVo.ouId}"/>
                            </div>
                              </div>--%>
                           <%-- <div class="form-group">
                                <label class="col-md-2 control-label">采购组织名称</label>
                                <div class="col-md-4">
                                    <input type='text' name='ouName' id='ouName' class='form-control' value="${rfqRequestItemExtraVo.ouName}"/>
                                </div>
                            </div>--%>
                            <div class="form-group">
                                <label class="col-md-2 control-label">编码</label>
                                <div class="col-md-4">
                                    <input type='text' name='itemCode' id='itemCode' class='form-control' value="${rfqRequestItemExtraVo.itemCode}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">名称</label>
                                <div class="col-md-4">
                                    <input type='text' name='itemName' id='itemName' class='form-control' value="${rfqRequestItemExtraVo.itemName}"/>
                                </div>
                            </div>
                           <%-- <div class="form-group">
                                <label class="col-md-2 control-label">费用科目标识</label>
                                <div class="col-md-4">
                                    <input type='text' name='seq' id='seq' class='form-control' value="${rfqRequestItemExtraVo.seq}"/>
                                </div>
                            </div>--%>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <input type='submit' id='save' class='btn btn-primary' value='保存' data-loading='稍候...' />
                                    <input type='hidden' name='type' id='type' value='article' />
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
