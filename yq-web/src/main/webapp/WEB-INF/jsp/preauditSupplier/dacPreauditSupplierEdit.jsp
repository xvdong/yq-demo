<%--
  Created by IntelliJ IDEA.
  User: yinchuanbao
  Date: 2016/9/12
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>供应商报名</title>
    <%--<link href="${pageContext.request.contextPath}/css/page.css" rel="stylesheet">--%>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>

    <c:import url="../common/jqGridBootstrap.jsp"/>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <!-- 上传JS -->
    <%--  <script type="text/javascript" src="${resource}/js/upload/jquery-1.10.2.min.js"></script>--%>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/upload/jquery.form.min.js"></script>


    <script src="${pageContext.request.contextPath}/lib/plupload-2.1.9/js/plupload.full.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/upload/uploadFile.js?v=${version}" type="text/javascript" ></script>

</head>
<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">供应商报名</li>
                    </ol>
                </div>
                <div class="page-content">
                    <c:if test="${rfqPreauditSupplier.status == '2'}">
                        <div class="alert alert-danger clearfix mg-alert-danger">
                            <i class="icon icon-exclamation-sign mt3 ml5"></i>
                            <div class="pull-left ml12">
                                <p>审批结果：已驳回</p>
                                <p>驳回理由：${rfqPreauditSupplier.pasDesc}</p>
                            </div>
                            <a href="#" class="close" style="font-size:26px; height:45px; line-height:45px;"
                               data-dismiss="alert">×</a>
                        </div>
                    </c:if>
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价单摘要</span>
                            <div class=" pull-right">
                                <span onclick="rfqRequestDetail('${basevo.id}')" > <span class="btn btn-info font12">竞价单详情</span></span>

                            </div>
                        </div>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>竞价价单号：${basevo.unifiedRfqNum}</li>
                                        <li>竞价标题：${basevo.title}</li>
                                        <li>采购单位：${basevo.ouName}</li>
                                        <li>计划编号：${basevo.planNo}</li>
                                        <c:choose>
                                            <c:when test="${rfqPreauditSupplier.status == '0'}">
                                                <li>当前状态：<span class="red">待审核</span></li>
                                            </c:when>
                                            <c:when test="${rfqPreauditSupplier.status == '1'}">
                                                <li>当前状态：<span class="red">已通过</span></li>
                                            </c:when>
                                            <c:when test="${rfqPreauditSupplier.status == '2'}">
                                                <li>当前状态：<span class="red">已驳回</span></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li>当前状态：<span class="red">未报名</span></li>
                                            </c:otherwise>
                                        </c:choose>

                                    </ul>
                                    <c:choose>
                                        <c:when test="${rfqPreauditSupplier.status == '0'}">
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/dsh.png"></span>
                                        </c:when>
                                        <c:when test="${rfqPreauditSupplier.status == '1'}">
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/ytg.png"></span>
                                        </c:when>
                                        <c:when test="${rfqPreauditSupplier.status == '2'}">
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/ybh.png"></span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/wbm.png"></span>
                                        </c:otherwise>
                                    </c:choose>

                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span
                                                class="red">${basevo.assureMoney}元</span></span></li>
                                        <li>竞价币种：${basevo.currency}</li>


                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：${basevo.issueDate}</li>
                                        <li>竟价开始时间：${basevo.startDate}</li>
                                        <li>竞价截止时间：${basevo.quotationEndDate}</li>
                                        <li>报名截止时间：${basevo.registrationEndTime}</li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${basevo.linkmanName}</li>
                                        <li>联系电话：${basevo.linkmanTelphone}</li>
                                        <li>交货地址：${basevo.deliveryProvince}${basevo.deliveryCity}${basevo.deliveryArea}${basevo.deliveryAddress}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名要求</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:30px; color:#666; font-size:12px;">
                                    <div class="clearfix"><label class="bler blu-1">注册资本：</label><span
                                            class="bler blu-2">${rfqPreaudit.regcapital}万元</span></div>
                                    <div class="clearfix"><label class="bler blu-1">资质要求：</label><span
                                            class="bler blu-2">${rfqPreaudit.qualifications}</span></div>
                                    <div class="clearfix"><label class="bler blu-1">报名要求：</label><span
                                            class="bler blu-2">${rfqPreaudit.requirementDesc}</span></div>


                                    <div class="clearfix">
                                        <span>附件清单：</span>
                                        <table class="table table-bordered align-md" style="width:98%;">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>附件名称</th>
                                                <th width="100px">文件大小</th>
                                                <th>说明</th>
                                                <th width="100px">操作</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${rfqAttachmentsPreaudit}" var="item">
                                                <tr data-clone="orginal">
                                                    <td>${item.seq}</td>
                                                    <td data-order="true">${item.originalFilename}</td>
                                                    <td>${item.fileSize}B</td>
                                                    <td>${item.fileDeclaration}</td>
                                                    <td><a href="${downloadUrl}/${item.downloadFilename}"><i
                                                            class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名信息</span>
                        </div>
                        <div class="row mt12 ml12">
                            <form class="form-horizontal mt12" role="form" method="post">
                                <div class="form-group">
                                    <div><span class="required"></span></div><label class="col-md-1 control-label">联系人:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="" value="${rfqPreauditSupplier.linkmanName}"
                                               id="linkmanName" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-1 control-label">固定电话:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="" value="${rfqPreauditSupplier.linkmanTel}" id="Tel"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><span class="required"></span></div><label class="col-md-1 control-label">移动电话:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="" value="${rfqPreauditSupplier.linkmanTelphone}"
                                               id="linkmanTelphone" class="form-control" onkeyup="changeTelPhone()">
                                    </div>
                                    <div>
                                        <span class="left"><a id="sendCode" class="btn btn-default" href="javascript:;" onclick="sendvalidateCode(this)">发送验证码</a></span>
                                        <div class="clearfix"></div>
                                        <div><span class="required" id="validateCodeRequired"></span></div>
                                        <div class="col-md-4" style="margin-left:83px">

                                            <input id="validateCode" onblur="checkValidateCode()" type="text" placeholder="请输入验证码" class="form-control mt12 ml88 required">
                                        </div>
                                        <br>
                                        <c:if test="${environment != 'run'}">
                                            <span class="left">  因短信尚未接通，验证码请统一录入：123456</span>
                                        </c:if>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div><span class="required"></span></div><label class="col-md-1 control-label">报名阐述:</label>
                                    <div class="col-md-10">
                                        <span class="right-titl-y">公司简介、业绩、优势等，2000字以内</span>
                                        <textarea id="responseDesc" name="responseDesc"
                                                  class="form-control kindeditorSimple col-md-10"
                                                  style=" width:100%;height:150px;">${rfqPreauditSupplier.responseDesc}</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名附件</span><span class="right"><a class="attachmentBtn" href="#" data-toggle="modal"
                                                                                            data-target="#myModa2"><label
                                class="btn btn-primary btn-sm"><i
                                class=" icon icon-plus-sign"></i> 新增附件</label></a></span>
                        </div>


                        <!-- 了，快乐模块模块里面看了 -->
                        <div class="mt12 clearfix" style="padding:0px 20px">
                            <table class="table table-bordered align-md" id="jqGrid">
                                <thead>
                                <tr>
                                    <th width="50px">序号</th>
                                    <th>附件名称</th>
                                    <th width="100px">文件大小</th>
                                    <th>说明</th>
                                    <th width="100px">操作</th>
                                </tr>
                                </thead>
                                <tbody id="filelsit">
                                <c:if test="${not empty rfqAttachmentsSupplier}">
                                    <c:forEach items="${rfqAttachmentsSupplier}" var="item">
                                        <tr data-clone="orginal">
                                            <td>${item.seq}</td>
                                            <td data-order="true">${item.originalFilename}</td>
                                            <td>${item.fileSize}B</td>
                                            <td>${item.fileDeclaration}</td>
                                            <td><a href="javascript:deleteFile(${item.attachmentId});"><i class="red icon icon-trash"></i>&nbsp;删除</a></td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                </tbody>
                            </table>
                            <span class="c999"  style="color: red;"> 附件类型不支持exe格式，文件大小不超过100M。              </span>
                        </div>
                        <!-- <div class="col-xs-12'"> <span class="mr12 order-add-btn"><a href="javascript:void(0);" id="addtr"><i class=" icon icon-plus-sign text-success"></i> 新增附件</a></span>   </div> -->

                    </div>
                    <div class="text-right">
                        <a class="left blue" href="#" data-toggle="modal" data-target="#myModa3">我的报名记录</a>
                        <a id="enroll" class="btn btn-lg btn-primary" type="button">提交报名</a>
                        <a href="javascript:void(0);" onclick="closeWindow()" class="btn btn-lg btn-primary" type="button">取消</a>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>


<!-- ///////////////////////新增附件2/////////////// -->
<div class="modal fade" id="myModa2">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span  aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div  class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                            <form>
                                <input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
                                <input type="text"  id="fileName" placeholder="请上传,附件大小≤100M"  class="form-control no-border" />
                                <input type="button" id="upload" value="上传">
                                <span  id="msg">0%</span>
                            </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration"  class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" <%--onclick="saveUpload()"--%> class="btn btn-primary js-confirm" data-dismiss="modal" id="sure" disabled="disabled">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<input id="uploadData" type="hidden" value="">
<!-- id2结束 -->


<div class="modal fade" id="myModa3">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的报名记录</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="4%">序号</th>
                            <th width="12%">询价单号</th>
                            <th width="10%">询价标题</th>
                            <th width="8%">报名部门</th>
                            <th width="6%">报名人</th>
                            <th width="11%">报名时间</th>
                            <th width="6%">报名状态</th>
                            <th width="12%">报名审核意见</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="rfqPreauditSupplierHistory" items="${rfqPreauditSupplierHistorylist}">

                            <tr>
                                <td align="center">${rfqPreauditSupplierHistory.rowno}</td>
                                <td align="center"> <span class="blue" onclick="rfqRequestDetail(${basevo.id})">${rfqPreauditSupplierHistory.unifiedRfqNum}</span>
                                </td>
                                <td align="center">${rfqPreauditSupplierHistory.title}</td>
                                <td></td>

                                <td align="center">${rfqPreauditSupplierHistory.answerUsername}</td>
                                <td align="center">${rfqPreauditSupplierHistory.answerTime}</td>
                                <c:choose>
                                    <c:when test="${rfqPreauditSupplierHistory.status == '0'}">
                                        <td align="center">待审核</td>
                                    </c:when>
                                    <c:when test="${rfqPreauditSupplierHistory.status == '1'}">
                                        <td align="center">已通过</td>
                                    </c:when>
                                    <c:when test="${rfqPreauditSupplierHistory.status == '2'}">
                                        <td align="center">已驳回</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td align="center">未报名</td>
                                    </c:otherwise>
                                </c:choose>
                                <td align="center"> ${rfqPreauditSupplierHistory.pasDesc}</td>
                            </tr>

                        </c:forEach>

                        <%--<tr>
                            <td align="center">6</td>
                            <td><a class="blue" href="10-ggmk-xunjiadanxiangqing.html"  target="10-ggmk-xunjiadanxiangqing.html">20150709001</a></td>
                            <td>计算机采购</td>
                            <td>销售部</td>
                            <td>张三</td>
                            <td>2016/07/18 9：00</td>
                            <td>报名已退回</td>
                            <td>请在报名时补充业绩</td>
                        </tr>--%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"><a type="button" class="btn btn-primary" data-dismiss="modal">确定</a></div>
    </div>
</div>
</div>


<div class="modal fade" id="myModa7">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="alertMsg">

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModa4">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body" style="padding:20px;">
                采购方要求三证、ISO9001证书齐全才允许报名，您的证书不全请补全信息。 <br><a class="blue" href="javascript:;">补全企业简历</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">返回</button>
            </div>
        </div>
    </div>
</div>

<script>
    var editor;
    KindEditor.ready(function (K) {
        editor = K.create('textarea#responseDesc', {
            resizeType: 1,
            allowPreviewEmoticons: false,
            allowImageUpload: false,
            items: []
        });
    });

    /*
     上传
     */
    $(function () {
        var $myModa2 = $('#myModa2');

        $myModa2.on('click', '.js-confirm', function () {
            var data = $myModa2.data('_result');
            if(data==undefined || data==''){
                showErrorMsg("您还没选择文件！");
            }
            if(!data.fileSize){
                showErrorMsg('文件未上传完成，请重新上传');
                return false;
            }
            var requestid = '${basevo.id}';
            var fileDeclaration = $("#fileDeclaration").val();
            if(checkLen(fileDeclaration)){
                showErrorMsg("文件说明长度超过2000字数限制！")
            }else{
                data["fileDeclaration"]=fileDeclaration;
                data["requestId"]='${basevo.id}';

                data["preauditId"]='${rfqPreaudit.paId}';
                data["supplierCode"]='${sysUserVo.supplierCode}';
                var url=ctx+"/dacPreauditSupplier/saveUpload";
                $.ajax({
                    url:url,
                    type: 'post',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if(data.rspmsg=='true'){
                            showSuccessMsg("文件保存成功");
                            updateFileList(requestid);

                        }else{
                            showErrorMsg("文件保存失败");
                        }
                    },
                    error:function (data) {
                        showErrorMsg("文件保存失败");
                    }
                });

            }
        });

    })

    /*
     刷新文件列表
     */
    function updateFileList(id){
        var url=ctx+"/dacPreauditSupplier/updateFileList";
        var data={id : id};
        $.ajax({
            url:url,// 跳转到 action
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                var s = '';
                $.each(data,function(i,item){
                    s+='<tr data-clone="orginal">';
                    if(item.seq!=undefined && item.seq!='null'){
                        s+="<td>"+item.seq+"</td>";
                    }else{
                        s+="<td></td>";
                    }
                    if(item.originalFilename != undefined && item.originalFilename != 'null') {
                        s += '<td data-order="true">' + item.originalFilename + '</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileSize != undefined && item.fileSize != 'null') {
                        s+='<td>'+item.fileSize+'KB</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileDeclaration != undefined && item.fileDeclaration != 'null') {
                        s+='<td>'+item.fileDeclaration+'</td>';
                    }else{
                        s+="<td></td>";
                    }
                    s+='<td><a href="javascript:deleteFile('+item.attachmentId+');"><i class="red icon icon-trash"></i>&nbsp;删除</a></td>';
                    s+='</tr>';
                });
                $("#filelsit").html("");
                $("#filelsit").html(s);
            }
        });
    }
    /*
     删除附件
     */
    function deleteFile(attachmentId){
        var url=ctx+"/dacPreauditSupplier/deleteUpload";
        var data = [{attachmentId: attachmentId}];
        var requestid = '${basevo.id}';
        $.ajax({
            url: url,
            type: 'post',
            data: JSON.stringify(data),
            cache: false,
            contentType: 'application/json;charset=utf-8',
            async: false,
            dataType: 'json',
            success: function (data) {
                if(data.rspmsg=='true'){
                    showSuccessMsg("文件删除成功");
                    updateFileList(requestid);
                }else{
                    showErrorMsg("文件删除失败");
                }
            },
            error:function (data) {
                showErrorMsg("文件删除失败");
            }
        });
    }
    /*
     竞价单号跳转
     */

    function rfqRequestDetail(id) {
        var aa = window.open();
        var url = ctx+"/dacReverseAuction/reverseAuctionDetail?id=" + id;
        aa.location.href = url ;

    }
    function preauditSupplierInit(){
        location.href= ctx+"/dacPreauditSupplier/init";
    }
    var $myModa7 = $('#myModa7');
    //报名成功提示
    var alertMsgSuccess = '<div class="modal-header"><h4 class="modal-title">{title}</h4></div><div class="modal-body"><div class="alert-icon-t"> <i class="icon-check text-primary"></i> <div class="content"> <h4>{msg}</h4><p>询单标题：${basevo.title}</p> <p>询单编号：${basevo.unifiedRfqNum}</p> </div> </div> </div> <div class="modal-footer"><a type="button" class="btn btn-primary" href="javascript:preauditSupplierInit();">确定</a></div>';
    //报名失败提示
    var alertMsgError = '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button> <h4 class="modal-title">{title}</h4> </div> <div class="modal-body"> <div class="alert-icon-t"><i class="icon icon-times red"></i> <div class="content"> <h4>{msg}</h4><p>询单标题：${basevo.title}</p> <p>询单编号：${basevo.unifiedRfqNum}</p> <p class="red">报价开始时间：${basevo.startDate}</p> </div> ';
    //报名表单验证提示
    var alertMsg2 = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button><h4 class="modal-title">{title}</h4> </div> <div class="modal-body"><div class="alert-icon-t"> <i class="icon icon-times red"></i><div class="content"><p>{msg}</p></div></div> </div><div class="modal-footer"> <a type="button" class="btn btn-primary"  data-dismiss="modal">确定</a> </div>';
    /*报名*/
    $("#enroll").click(function () {

        //表单验证
        if(chenkform()){
            $("#alertMsg").html("");
            var data = {
                status: '${rfqPreauditSupplier.status}',
                linkmanName: $('#linkmanName').val(),
                responseDesc: editor.html(),
                linkmanTelphone: $('#linkmanTelphone').val(),
                linkmanTel: $('#Tel').val(),
                unifiedRfqNum: '${basevo.unifiedRfqNum}',
                id: '${basevo.id}'
            };
            var regcapital = '${sysUserVo.regcapital}';
            var requestRegcapital = '${rfqPreaudit.regcapital}';
            regcapital =  Number(regcapital);
            requestRegcapital = Number(requestRegcapital);
            if(isNaN(requestRegcapital)||((regcapital / 10000) - requestRegcapital)>=0){
                $.ajax({
                    url:ctx+'/dacPreauditSupplier/preauditSupplierAdd',// 跳转到 action
                    data: data,
                    type: 'post',
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.rspcod == "200") {
                            // view("修改成功！");


                            $("#alertMsg").html(alertMsgSuccess.format({msg:"报名提交成功！", title:"提交成功"}));
                            $myModa7.modal('show');
                        } else if (data.rspcod == "500") {


                            $("#alertMsg").html(alertMsgError.format({msg:"报名提交失败！",title:"提交失败"}));
                            $myModa7.modal('show');
                        } else if (data.rspcod == "501") {
                            showErrorMsg(data.rspmsg);
                        }
                    },
                    error: function () {
                        $("#alertMsg").html(alertMsgError.format({msg: "报名提交失败！", title: "提交失败"}));
                        $myModa7.modal('show');
                    }
                });
            }else{
                $("#alertMsg").html(alertMsg2.format({msg: "供应商注册资本不符合报名要求", title: ""}));
                $myModa7.modal('show');
            }
        }
    });


    var linkmanTelphoneCopy='${rfqPreauditSupplier.linkmanTelphone}';
    var isTelphoneAvailability='false';
    var linkmanTelphoneCopy = '${rfqPreauditSupplier.linkmanTelphone}';
    var isTelphoneAvailability = 'false';
    var varifrCode = '';
    var linkTelphone = $("#linkmanTelphone").val();
    if (linkmanTelphoneCopy != undefined && linkmanTelphoneCopy != '' && linkTelphone == linkmanTelphoneCopy) {
        $("#sendCode").attr('disabled', true);
        $("#validateCode").attr('disabled', true);
    }
    function changeTelPhone() {
        var linkTelphone = $("#linkmanTelphone").val();
        if (linkmanTelphoneCopy != undefined && linkmanTelphoneCopy != '') {
            if (linkTelphone != linkmanTelphoneCopy) {
                $("#sendCode").removeAttr('disabled');
                $("#validateCode").removeAttr('disabled');
            } else {
                $("#sendCode").attr('disabled', true);
                $("#validateCode").attr('disabled', true);
            }
        }
    }
    /*
     表单验证
     */
    function chenkform() {
        var linkname = $("#linkmanName").val();
        var tel = $("#Tel").val();
        var linkTelphone = $("#linkmanTelphone").val();
        var validate = $("#validateCode").val();
        var responseDesc = editor.text();
        if (linkTelphone != undefined && linkTelphone != '' && linkmanTelphoneCopy != linkTelphone) {
            checkValidateCode();
            if (varifrCode != '0') {
                showErrorMsg("输入验证码错误，请重新输入");
                isTelphoneAvailability = 'false';
                return false;
            }
        }
        if (linkname == undefined || linkname == '') {
            $("#alertMsg").html(alertMsg2.format({msg: "联系人姓名不可为空,请重新填写", title: "信息填写不完整"}));
            $myModa7.modal('show');
            return false;
        } else if (linkname.length > 50) {
            $("#alertMsg").html(alertMsg2.format({msg: "联系人姓名超过50字长度,请重新填写", title: "信息填写不完整"}));
            $myModa7.modal('show');
            return false;
        } else if (tel.length > 50) {
            $("#alertMsg").html(alertMsg2.format({msg: "固定电话长度超过50字限制,请重新填写", title: "信息填写不完整"}));
            $myModa7.modal('show');
            return false;
        } else if (linkTelphone == undefined || linkTelphone == '') {
            $("#alertMsg").html(alertMsg2.format({msg: "移动电话不可为空,请重新填写", title: "信息填写不完整"}));
            $myModa7.modal('show');
            return false;
        } else if (checkForm(linkTelphone)) {
            $("#alertMsg").html(alertMsg2.format({msg: "移动电话格式不对,请重新填写", title: "信息填写不完整"}));
            $myModa7.modal('show');
            return false;
        } else if (linkTelphone != undefined && linkTelphone != '' && linkmanTelphoneCopy != linkTelphone && isTelphoneAvailability == 'false') {
            $("#alertMsg").html(alertMsg2.format({msg: "移动电话验证失败，请重新验证", title: "信息填写不完整"}));
            $myModa7.modal('show');
            return false;
        } else if (responseDesc == undefined || responseDesc == '') {
            $("#alertMsg").html(alertMsg2.format({msg: "报名阐述不可为空,请重新填写", title: "信息填写不完整"}));
            $myModa7.modal('show');
        } else if (checkLen(responseDesc)) {
            $("#alertMsg").html(alertMsg2.format({msg: "报名阐述长度超过2000字数限制", title: "信息填写不完整"}));
            $myModa7.modal('show');
//        $.zui.messager.show('报名阐述字数限2000字以内', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
            return false;
        } else {
            return true;
        }
    }

    /*
     验证码倒计时
     */
    var wait=60;
    function time(o) {
        if (wait == 0) {
            o.removeAttribute("disabled");
            o.innerHTML="发送验证码";
            wait = 60;
            failure = '1';
        } else {
            o.setAttribute("disabled", true);
            o.innerHTML="重新发送(" + wait + ")";
            wait--;
            setTimeout(function() {time(o) },1000);
        }
    }
    /*
     点击发送验证码
     */
    var validateCode="";
    /* 标记验证码是否有效 0 有效 1 无效*/
    var failure = '1';
    var businessId = "";
    function sendvalidateCode(obj) {
        var linkTelphone = $("#linkmanTelphone").val();
        var id = '${basevo.id}';
        if (linkTelphone != undefined && linkTelphone != '') {
            if (!checkForm(linkTelphone)) {
                time(obj);

                var url = ctx + "/dacPreauditSupplier/sendvalidateCode";
                $.ajax({
                    url: url,
                    type: 'post',
                    cache: false,
                    data: {
                        linkmanTelphone: $('#linkmanTelphone').val()
                    },
                    dataType: 'json',
                    success: function (data) {
                        businessId = data.rspmsg;
                        failure = '0';
                    }
                });
            } else {
                showErrorMsg("移动电话格式不正确，请重新填写！");
            }
        } else {
            showErrorMsg("移动电话不可 为空！");
        }
    }

    /*
     校验验证码
     */
    function checkValidateCode() {
        if (varifrCode != '0') {
            var inputValidateCode = $("#validateCode").val();
            if (businessId != "") {
                var url = ctx + "/dacPreauditSupplier/verifyCode";
                $.ajax({
                    url: url,
                    type: 'post',
                    cache: false,
                    async: false,
                    data: {
                        linkmanTelphone: $('#linkmanTelphone').val(),
                        inputValidateCode: inputValidateCode,
                        businessId: businessId
                    },
                    dataType: 'json',
                    success: function (data) {
                        //  validateCode=data.rspmsg;
                        if (data.rspmsg == '0') {
                            isTelphoneAvailability = 'true';
                            varifrCode = '0';
                            return true;
                        } else {
                            showErrorMsg("输入验证码错误，请重新输入");
                            isTelphoneAvailability = 'false';
                            varifrCode = '1';
                            return false;
                        }
                    }
                });
            } else {
                showErrorMsg("输入验证码错误，请重新输入");
                isTelphoneAvailability = 'false';
                return false;
            }
        } else {
            return true;
        }
    }
//验证是否是手机号
        function checkForm(linkTelphone) {
            var myreg = /^(((13[0-9])|(15[0-9])|(18[0-9])|(14[0-9])|(17[0-9]))+\d{8})$/;
            // test() 方法用于检测一个字符串是否匹配某个模式
            if (!myreg.test(linkTelphone)) {
                return true;
            } else {
                return false;
            }
        };
    /* 字符串长度验证*/
    function checkLen(str){
        var newStr =  str.split('&lt;').join('<').split('&gt;').join('>').replace(/\s+/g, "");
        var myLen = newStr.length;
        if (myLen > 2000 ) {
            return true;
        }
        else {
            return false;
        }
    };


    function showSuccessMsg(message) {
        new $.zui.Messager(message, {
            type: 'success' // 定义颜色主题
        }).show()
    }
    function showErrorMsg(message){
        new $.zui.Messager(message, {
            type: 'danger' // 定义颜色主题
        }).show()
    }

    $(document).ready(function (){
        var types=[".jpg",".jpeg",".gif",".png",".doc",".docx",".xls",".xlsx",".pdf"];
        var maxSize=104857600;//100M，单位:B
        if(navigator.appName == "Microsoft Internet Explorer"){
            $("#myModa2").on('show.zui.modal', function () {
                //初始化flash插件
                initPlupload2(maxSize,types);
                //关闭时清理页面
                initEvent();
            });
        }else {
            //初始化flash插件
            initPlupload(maxSize,types);
            //关闭时清理页面
            initEvent();
        }
    });
    function closeWindow() {
        var attachmentIds = [];
        $("#filelsit").find('input[name="attachmentId"]').each(function () {
            attachmentIds.push({attachmentId: '' + this.value});
        });
        var url = ctx + "/rfqPreauditSupplier/deleteUpload";
        var data = attachmentIds;
        var requestid = '${basevo.id}';
        $.ajax({
            url: url,
            type: 'post',
            contentType: 'application/json;charset=utf-8',
            async: false,
            cache: false,
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (data) {
                if (data.rspmsg == 'true') {
                }
            },
            error: function (data) {
            }
        });
        location.href= ctx+"/rfqPreauditSupplier/init";
    };
</script>
</body>
</html>

