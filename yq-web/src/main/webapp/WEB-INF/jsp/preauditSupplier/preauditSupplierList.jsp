<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>报名列表</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>

    <script src="${pageContext.request.contextPath}/js/preauditSupplier/preauditSupplierList.js?v=${version}"></script>
    <script src="${resource}/lib/datetimepicker/datetimepicker.js?v=${version}"></script>

</head>
        <input type="hidden" id="ocntextPath" value="${pageContext.request.contextPath}">
<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs pore">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">报名列表</li>
                    </ol>
                    <div class="abrl" ><a href="${ctx}/show/supplier.html" target='_blank'>新版询比价报名，报价操作演示</a></div>
                </div>
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <ul id="myTab" class="nav nav-tabs">
                                    <li> <a href="#tab1" data-toggle="tab">全部报名</a> </li>
                                    <li class="active"> <a href="#tab2" data-toggle="tab" data-method="RAQ">询比价</a> </li>

                                    <%--<li> <a href="#tab3" data-toggle="tab" data-method="DAC">反向竞价</a> </li>--%>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane  example" id="tab1">
                                        <div class="row mt12">
                                            <div class="so-form">
                                              <form  method="post" class="searchFormTab" id="searchForm1" >
                                                <div class="so-form-15 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="title"         class="form-control" placeholder="询价标题"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="ouName"        class="form-control" placeholder="采购单位"></div>
                                                <div class="so-form-15">
                                                    <button id="btnSearch1" class="btn btn-primary" type="button"><i class="icon icon-search"></i> 搜索</button>
                                                    <input class="btn" type="reset" value="重置" />
                                                </div>
                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                <div class='clearfix'></div>
                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="issueDateStart"  placeholder="发布时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input type="text" name="issueDateEnd"  placeholder="发布时间(止)" class="form-control form-datetime"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationEndDateStart"  placeholder="截止时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDateEnd"  placeholder="截止时间(止)" class="form-control form-datetime"></div>
                                                </div>
                                              </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane active  example" id="tab2">
                                        <div class="col-lg-12 ">
                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                                <ul class="nav navbar-nav">
                                                    <li class="active"><a href="javascript:void(0)" data-status="3" data-type="allStatus">全部状态<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="4" data-type="notEnroll">未报名<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="0" data-type="audit">审核中<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="1" data-type="pass">已通过<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="2" data-type="back">已退回<strong class="red2">(0)</strong></a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                                <ul class="nav navbar-nav">
                                                    <li class="active"><a href="javascript:void(0)" data-status="3"  data-type="allTime">全部时间<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="1" data-type="notStop">未截止<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="0" data-type="arreadyStop">已截止<strong class="red2">(0)</strong></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row mt12">
                                            <div class="so-form">
                                    <form  method="post" id="searchForm2" class="searchFormTab" onsubmit="return false;">
                                                <div class="so-form-15 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="title"         class="form-control" placeholder="询价标题"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="ouName"        class="form-control" placeholder="采购单位"></div>
                                                <div class="so-form-15">
                                                    <button id="btnSearch2" class="btn btn-primary" type="button"><i class="icon icon-search"></i> 搜索</button>
                                                    <input class="btn" type="reset" value="重置" />
                                                </div>
                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                <div class='clearfix'></div>
                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="issueDateStart"  placeholder="发布时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input type="text" name="issueDateEnd" placeholder="发布时间(止)" class="form-control form-datetime"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationEndDateStart"  placeholder="截止时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDateEnd"  placeholder="截止时间(止)" class="form-control form-datetime"></div>
                                                </div>
                                    </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane  example" id="tab3">
                                        <div class="col-lg-12 ">
                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                                <ul class="nav navbar-nav">
                                                    <li class="active"><a href="javascript:void(0)" data-status="3" data-type="allStatus">全部状态<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="4" data-type="notEnroll">未报名<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="0" data-type="audit">审核中<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="1" data-type="pass">已通过<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="2" data-type="back">已退回<strong class="red2">(0)</strong></a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                                <ul class="nav navbar-nav">
                                                    <li class="active"><a href="javascript:void(0)" data-status="3"  data-type="allTime">全部时间<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="1" data-type="notStop">未截止<strong class="red2">(0)</strong></a></li>
                                                    <li><a href="javascript:void(0)" data-status="0" data-type="arreadyStop">已截止<strong class="red2">(0)</strong></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row mt12">
                                            <div class="so-form">
                                                <form  method="post" id="searchForm3" class="searchFormTab" onsubmit="return false;">
                                                    <div class="so-form-15 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                    <div class="so-form-15 padding1"><input type="text" name="title"         class="form-control" placeholder="询价标题"></div>
                                                    <div class="so-form-15 padding1"><input type="text" name="ouName"        class="form-control" placeholder="采购单位"></div>
                                                    <div class="so-form-15">
                                                        <button id="btnSearch3" class="btn btn-primary" type="button"><i class="icon icon-search"></i> 搜索</button>
                                                        <input class="btn" type="reset" value="重置" />
                                                    </div>
                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                    <div class='clearfix'></div>
                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="issueDateStart"  placeholder="发布时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input type="text" name="issueDateEnd" placeholder="发布时间(止)" class="form-control form-datetime"></div>
                                                        <div class="so-form-15"><input type="text" name="registrationEndDateStart"  placeholder="截止时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="registrationEndDateEnd"  placeholder="截止时间(止)" class="form-control form-datetime"></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-bordered table-hove align-md" id="jqGrid">



                                    </table>
                                    <div class="col-md-12" id="jqGridPager">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
    <%--<script src="dist/js/jquery.js"></script>--%>
    <!-- ZUI Javascript组件 -->
    <%--<script src="dist/js/zui.min.js"></script>--%>
    <!--选择时间和日期-->
   <%-- <script src="dist/lib/datetimepicker/datetimepicker.min.js"></script>--%>
    <script>
        $(function(){

            $('.more-so').click(function(){
                $(this).parent().siblings('.hiderow').stop().slideToggle('fast');
                //  日期
                $(".tab-content").children('.active').find(".form-datetime").datetimepicker(
                    {
                        weekStart: 1,
                        todayBtn: 1,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        forceParse: 0,
                        showMeridian: 1,
                        format: "yyyy-mm-dd hh:ii"
                    });
//            $.placeholder();
            });

            $(".form-date").datetimepicker(
                    {
                        language:  "zh-CN",
                        weekStart: 1,
                        todayBtn:  1,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        minView: 2,
                        forceParse: 0,
                        format: "yyyy-mm-dd"
                    });

            $(".form-time").datetimepicker({
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 1,
                minView: 0,
                maxView: 1,
                forceParse: 0,
                format: 'hh:ii'
            });

        });



        $("#btnSearch1").click(function () {
            deletejqGridJson();
            $("#jqGrid").jqGrid('setGridParam', {
                postData: form2Json("searchForm1"),
                page: 1
            }).trigger("reloadGrid");
        });
        $("#btnSearch2").click(function () {
            deletejqGridJson();
            $("#jqGrid").jqGrid('setGridParam', {
                postData: form2Json("searchForm2"),
                page: 1
            }).trigger("reloadGrid");
        });
        function PreauditSupplierDetail(unifiedRfqNum, rfqMethod){
            var aa = window.open();
            var url = null;
            if(rfqMethod == 'RAQ'){
                url = ctx+"/rfqPreauditSupplier/preauditSupplierDetail?unifiedRfqNum=" + unifiedRfqNum;
            }else if(rfqMethod == 'DAC'){
                url = ctx+"/dacPreauditSupplier/preauditSupplierDetail?unifiedRfqNum=" + unifiedRfqNum;
            }
            aa.location.href = url ;
        }
        function PreauditSupplierEdit(unifiedRfqNum, rfqMethod){
            var url = null ;
            if(rfqMethod == 'RAQ'){
                url =ctx+ "/rfqPreauditSupplier/preauditSupplierEdit?unifiedRfqNum=" + unifiedRfqNum;
            }else if(rfqMethod == 'DAC'){
                url =ctx+ "/dacPreauditSupplier/preauditSupplierEdit?unifiedRfqNum=" + unifiedRfqNum;
            }

            window.location.href=url;
        }

        function deletejqGridJson(){
            var postData = $("#jqGrid").jqGrid("getGridParam", "postData");
            if("isStop" in postData){

                delete postData["isStop"];
            }
            if("preauditSupplierStatus" in postData){

                delete postData["preauditSupplierStatus"];
            }
            if("unifiedRfqNum" in postData ){
                delete postData["unifiedRfqNum"];
            }
            if("title" in postData ){
                delete postData["title"];
            }
            if("ouName" in postData ){
                delete postData["ouName"];
            }
            if("issueDateStart" in postData ){
                delete postData["issueDateStart"];
            }
            if("issueDateEnd" in postData ){
                delete postData["issueDateEnd"];
            }
            if("registrationEndDateStart" in postData ){
                delete postData["registrationEndDateStart"];
            }
            if("registrationEndDateEnd" in postData ){
                delete postData["registrationEndDateEnd"];
            }
        }
    </script>
</body>
</html>
