<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>供应商报名</title>
    <%--<link href="${pageContext.request.contextPath}/css/page.css" rel="stylesheet">--%>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>

    <c:import url="../common/jqGridBootstrap.jsp"/>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>

</head>
<input type="hidden" id="ocntextPath" value="${pageContext.request.contextPath}">
<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">供应商报名</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竟价单摘要</span>
                            <div class=" pull-right">
                                <a onclick="rfqRequestDetail('${basevo.id}')" target="_blank"> <span class="btn btn-info font12">竞价单详情</span></a>

                            </div>
                        </div>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>竞价单号：${basevo.unifiedRfqNum}</li>
                                        <li>竞价标题：${basevo.title}</li>
                                        <li>采购单位：${basevo.ouName}</li>
                                        <li>计划编号：${basevo.planNo}</li>
                                        <c:choose>
                                            <c:when test="${rfqPreauditSupplier.status == '0'}">
                                                <li>当前状态：<span class="red">待审核</span></li>
                                            </c:when>
                                            <c:when test="${rfqPreauditSupplier.status == '1'}">
                                                <li>当前状态：<span class="red">已通过</span></li>
                                            </c:when>
                                            <c:when test="${rfqPreauditSupplier.status == '2'}">
                                                <li>当前状态：<span class="red">已驳回</span></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li>当前状态：<span class="red">未报名</span></li>
                                            </c:otherwise>
                                        </c:choose>

                                    </ul>
                                    <c:choose>
                                        <c:when test="${rfqPreauditSupplier.status == '0'}">
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/dsh.png"></span>
                                        </c:when>
                                        <c:when test="${rfqPreauditSupplier.status == '1'}">
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/ytg.png"></span>
                                        </c:when>
                                        <c:when test="${rfqPreauditSupplier.status == '2'}">
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/ybh.png"></span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="zt-ico-w zt-gz"><img
                                                    src="${pageContext.request.contextPath}/images/wbm.png"></span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span
                                                class="red">${basevo.assureMoney}元</span></span></li>
                                        <li>竞价币种：${basevo.currency}</li>


                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：${basevo.issueDate}</li>
                                        <li>竟价开始时间：${basevo.startDate}</li>
                                        <li>竞价截止时间：${basevo.quotationEndDate}</li>
                                        <li>报名截止时间：${basevo.registrationEndTime}</li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>联系人：${basevo.linkmanName}</li>
                                            <li>联系电话：${basevo.linkmanTelphone}</li>
                                            <li>交货地址：${basevo.deliveryProvince}${basevo.deliveryCity}${basevo.deliveryArea}${basevo.deliveryAddress}</li>
                                        </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名要求</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:30px; color:#666; font-size:12px;">
                                    <div class="clearfix"><label class="bler blu-1">注册资本：</label><span
                                            class="bler blu-2">${rfqPreaudit.regcapital}万元</span></div>
                                    <div class="clearfix"><label class="bler blu-1">资质要求：</label><span
                                            class="bler blu-2">${rfqPreaudit.qualifications}</span></div>
                                    <div class="clearfix"><label class="bler blu-1">报名要求：</label><span
                                            class="bler blu-2">${rfqPreaudit.requirementDesc}</span>
                                    </div>


                                    <div class="clearfix">
                                        <span>附件清单：</span>
                                        <table class="table table-bordered align-md" style="width:98%;">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>附件名称</th>
                                                <th width="100px">文件大小</th>
                                                <th>说明</th>
                                                <th width="100px">操作</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${rfqAttachmentsPreaudit}" var="item">
                                                <tr data-clone="orginal">
                                                    <td>${item.seq}</td>
                                                    <td data-order="true">${item.originalFilename}</td>
                                                    <td>${item.fileSize}B</td>
                                                    <td>${item.fileDeclaration}</td>
                                                    <td><a href="${downloadUrl}/${item.downloadFilename}"><i
                                                            class="icon icon-download-alt green"></i>&nbsp;下载</a></td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel">

                        <div class="panel-heading clearfix">

                            <span class="mg-margin-right">报名信息</span>

                        </div>

                        <div class="panel-body">

                            <div class="row" style="padding:20px">


                                <div class="re line-32" style="margin-left:30px;color:#666; font-size:12px;">

                                    <div class="clearfix">

                                        <label class="bler blu-1">联系人：</label><span class="bler blu-2">${rfqPreauditSupplier.linkmanName}</span>

                                    </div>
                                    <div class="clearfix">

                                        <label class="bler blu-1">固定电话：</label><span
                                            class="bler blu-2">${rfqPreauditSupplier.linkmanTel}</span>

                                    </div>
                                    <div class="clearfix">

                                        <label class="bler blu-1">移动电话：</label><span
                                            class="bler blu-2">${rfqPreauditSupplier.linkmanTelphone}</span>

                                    </div>

                                    <div class="clearfix"><label class="bler blu-1">报名阐述：</label>
                                        <span class="bler blu-2">
                                            ${rfqPreauditSupplier.responseDesc}
                                     </span>

                                    </div>


                                </div>

                            </div>


                        </div>

                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名附件</span>
                        </div>



                        <div class="mt12 clearfix" style="padding:0px 20px">
                            <table class="table table-bordered align-md">
                                <thead>
                                <tr>
                                    <th width="50px">序号</th>
                                    <th>附件名称</th>
                                    <th width="100px">文件大小</th>
                                    <th>说明</th>
                                    <th width="100px">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:if test="${not empty rfqAttachmentsSupplier}">
                                <c:forEach items="${rfqAttachmentsSupplier}" var="item">
                                    <tr data-clone="orginal">
                                        <td>${item.seq}</td>
                                        <td data-order="true">${item.originalFilename}</td>
                                        <td>${item.fileSize}B</td>
                                        <td>${item.fileDeclaration}</td>
                                        <td><a href="${downloadUrl}/${item.downloadFilename}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                    </tr>
                                </c:forEach>
                                </c:if>
                                </tbody>
                            </table>
                        </div>
                        <!-- <div class="col-xs-12'"> <span class="mr12 order-add-btn"><a href="javascript:void(0);" id="addtr"><i class=" icon icon-plus-sign text-success"></i> 新增附件</a></span>   </div> -->
                    </div>
                    <%--审批意见--%>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">审批意见</span>
                            <span class="pull-right">
                                   <span onclick="auditHistory()" class="btn btn-sm btn-info">审批历史</span>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    ${rfqPreauditSupplier.pasDesc}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- 了，快乐模块模块里面看了 -->


            <!-- <div class="col-xs-12'"> <span class="mr12 order-add-btn"><a href="javascript:void(0);" id="addtr"><i class=" icon icon-plus-sign text-success"></i> 新增附件</a></span>   </div> -->
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<%--<script src="dist/js/jquery.js"></script>--%>
<!-- ZUI Javascript组件 -->
<%--<script src="dist/js/zui.min.js"></script>--%>
<!--富文本JS-->
<%--<script src="dist/lib/kindeditor/kindeditor-all-min.js"></script>
<script src="dist/lib/kindeditor/lang/zh_CN.js"></script>
<script src="dist/js/base.js"></script>--%>
<script>
    var editor;
    KindEditor.ready(function (K) {
        editor = K.create('textarea#contentSimple', {
            resizeType: 1,
            allowPreviewEmoticons: false,
            allowImageUpload: false,
            items: []
        });
    });

    //删除

    $('.order-del-btn').delBtn();
    var bClick = false;

    if (bClick) {
        $('#myModa4').modal({
            keyboard: false,
            show: true,
            backdrop: 'static'
        })
    }
    /*
     删除附件
     */
    function deleteFile(attachmentId){
        var url=ctx+"/rfqPreauditSupplier/deleteUpload";
        var data={attachmentId : attachmentId};
        var requestid = '${basevo.id}';
        $.ajax({
            url:url,
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                if(data.rspmsg=='true'){
                    showSuccessMsg("文件删除成功");
                    updateFileList(requestid);
                }else{
                    showErrorMsg("文件删除失败");
                }
            },
            error:function (data) {
                showErrorMsg("文件删除失败");
            }
        });
    }
    /*
     刷新文件列表
     */
    function updateFileList(id){
        var url=ctx+"/rfqPreauditSupplier/updateFileList";
        var data={id : id};
        $.ajax({
            url:url,// 跳转到 action
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                var s = '';
                $.each(data,function(i,item){
                    s+='<tr data-clone="orginal">';
                    if(item.seq!=undefined && item.seq!='null'){
                        s+="<td>"+item.seq+"</td>";
                    }else{
                        s+="<td></td>";
                    }
                    if(item.originalFilename != undefined && item.originalFilename != 'null') {
                        s += '<td data-order="true">' + item.originalFilename + '</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileSize != undefined && item.fileSize != 'null') {
                        s+='<td>'+item.fileSize+'B</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileDeclaration != undefined && item.fileDeclaration != 'null') {
                        s+='<td>'+item.fileDeclaration+'</td>';
                    }else{
                        s+="<td></td>";
                    }
                    s+='<td><a href="javascript:deleteFile('+item.attachmentId+');"><i class="red icon icon-trash"></i>&nbsp;删除</a></td>';
                    s+='</tr>';
                });
                $("#filelsit").html("");
                $("#filelsit").html(s);
            }
        });
    }

    function rfqRequestDetail(id) {
        var aa = window.open();
        var url = ctx + "/dacReverseAuction/reverseAuctionDetail?id=" + id;
        aa.location.href = url;
    }
    //审批历史
    function auditHistory(){
        var aa = window.open();
        var url=ctx+"/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${basevo.unifiedRfqNum}"+"&supplierCode=${rfqPreauditSupplier.supplierCode}";
        aa.location.href = url;
    }
   //光标离开这个textField就验证该文本框是否为空
   function check(){
       var forumPost = document.getElementById("subject");
       var forumPostName = forumPost.value;
       var subjectchkRule = document.getElementById("subjectchk");
       var postNameRule = document.getElementById("postNameRule");
       postNameRule.style.color="#FF0000";
       if(forumPostName == ""){
           subjectchkRule.style.display="none";
           postNameRule.style.display="none";
           postNameRule.innerHTML="*帖子标题不能为空";
           forumPost.focus();
       } else{
           postNameRule.innerHTML="";
       }
   }
   // 只要键盘一抬起就验证编辑框中的文字长度，最大字符长度可以根据需要设定
   function checkLength(obj) {
       var maxChars = 80;
    //最多字符数
    var curr = maxChars - obj.value.length;
       if( curr > 0 ){
           document.getElementById("checklen").innerHTML = curr.toString();
       }else{
           document.getElementById("checklen").innerHTML = '0';
           document.getElementById("subject").readOnly=true;
       }
   }
</script>
</body>
</html>
