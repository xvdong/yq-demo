<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>供应商报价管理</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqQuotationLock/quotationLockList.js?v=${version}"></script>
</head>

<body>
<c:import url="../common/top.jsp"/>
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
            	<div class="breadcrumbs">
                    <ol class="breadcrumb">
                                              <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                      <li class="active">供应商报价管理</li>
                    </ol>
                </div>
                <input type="hidden" id="rfqMethod"/>
                <div class="page-content">
                  	<div class="row">
			            <div class="col-md-12"> 
			              <div>
			                <ul id="myTab" class="nav nav-tabs">
			                    <li class="active"> <a href="#tab1" data-toggle="tab" onclick="typeList('')">全部报价</a> </li>
			                    <li> <a href="#tab2" data-toggle="tab" data-method="RAQ" onclick="typeList('RAQ')">询比价</a> </li>
                                <%--<li> <a href="#tab4" data-toggle="tab" data-method="DAC" onclick="typeList('DAC')">反向竞价</a> </li>--%>
                                <%--<li> <a href="#tab3" data-toggle="tab" data-method="EXPIRED">已过期</a> </li>--%>
			                </ul>
			                <div class="tab-content">
                                <div class="tab-pane active  example" id="tab1">
			                        <div class="row mt12">
                                        <form  method="post" id="searchFormTab" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">
                                                <div class="so-form-12 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="title" class="form-control" placeholder="询价标题"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="ouName" class="form-control" placeholder="采购单位"></div>
                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button">
                                                        <i class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn" type="reset" value="重置">
                                                </div>
                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                <div class="clearfix"></div>
                                                <div class="hiderow">
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="startDateStart" placeholder="报价起始时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="startDateEnd" placeholder="报价起始时间(止)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="quotationStartDate" placeholder="报价截止时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="quotationEndDate" placeholder="报价截止时间(止)" class="form-control form-datetime">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
			                        </div>
                                </div>
                                <div class="tab-pane  example" id="tab2">
                                    <div class="col-lg-12 ">
                                        <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                            <ul class="nav navbar-nav">
                                                  <li class="active"><a href="#" data-type="100">全部状态<strong class="red2">(5)</strong></a></li>
                                                  <li><a href="#" data-type="0">未开始<strong class="red2">(1)</strong></a></li>
                                                  <li><a href="#" data-type="1">待报价<strong class="red2">(1)</strong></a></li>
                                                  <li><a href="#" data-type="2">已报价<strong class="red2">(1)</strong></a></li>
                                                  <li><a href="#" data-type="3">结果待发布<strong class="red2">(1)</strong></a></li>
                                                  <li><a href="#" data-type="4">已放弃<strong class="red2">(1)</strong></a></li>
                                                  <li><a href="#" data-type="5">已中标<strong class="red2">(2)</strong></a></li>
                                                  <li><a href="#" data-type="6">未中标<strong class="red2">(2)</strong></a></li>
                                                  <li><a href="#" data-type="7">已作废<strong class="red2">(2)</strong></a></li>
                                            </ul>
                                         </div>
                                    </div>

                                    <div class="row mt12">
                                        <form  method="post" id="searchFormTab2" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">
                                                <div class="so-form-12 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="title" class="form-control" placeholder="询价标题"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="ouName" class="form-control" placeholder="采购单位"></div>
                                              <div class="so-form-15">
                                                  <button class="btn btn-primary btnSearch" type="button">
                                                      <i class="icon icon-search"></i> 搜索
                                                  </button>
                                                  <input class="btn" type="reset" value="重置">
                                              </div>
                                              <div class="so-form-10">
                                                  <a href="javascript:;" class="more-so height32">高级搜索</a>
                                              </div>
                                              <div class="clearfix"></div>
                                              <div class="hiderow">
                                                  <div class="so-form-15 mr30">
                                                      <input type="text" name="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime">
                                                  </div>
                                                  <div class="so-form-15 mr30">
                                                      <input type="text" name="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime">
                                                  </div>
                                                  <div class="so-form-15 mr30">
                                                      <input type="text" name="startDateStart" placeholder="报价起始时间(起)" class="form-control form-datetime">
                                                  </div>
                                                  <div class="so-form-15 mr30">
                                                      <input type="text" name="startDateEnd" placeholder="报价起始时间(止)" class="form-control form-datetime">
                                                  </div>
                                                  <div class="clearfix"></div>
                                                  <div class="so-form-15 mr30">
                                                      <input type="text" name="quotationStartDate" placeholder="报价截止时间(起)" class="form-control form-datetime">
                                                  </div>
                                                  <div class="so-form-15 mr30">
                                                      <input type="text" name="quotationEndDate" placeholder="报价截止时间(止)" class="form-control form-datetime">
                                                  </div>
                                              </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane  example" id="tab4">
                                    <div class="col-lg-12 ">
                                        <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="#" data-type="100">全部状态<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="0">未开始<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="1">待竞价<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="2">已竞价<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="3">结果待发布<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="4">已放弃<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="5">已中标<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="6">未中标<strong class="red2">(0)</strong></a></li>
                                                <li><a href="#" data-type="7">已作废<strong class="red2">(0)</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="row mt12">
                                        <form  method="post" id="searchFormTab4" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">
                                                <div class="so-form-12 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="竞价单号"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="title" class="form-control" placeholder="竞价标题"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="ouName" class="form-control" placeholder="采购单位"></div>
                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button">
                                                        <i class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn" type="reset" value="重置">
                                                </div>
                                                <div class="so-form-10">
                                                    <a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="hiderow">
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="startDateStart" placeholder="竞价起始时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="startDateEnd" placeholder="竞价起始时间(止)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="quotationStartDate" placeholder="竞价截止时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="quotationEndDate" placeholder="竞价截止时间(止)" class="form-control form-datetime">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane  example" id="tab3">
                                    <div class="col-lg-12" style="display: none;">
                                        <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="#" data-type="8">已过期</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row mt12">
                                        <form  method="post" id="searchFormTab3" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">
                                                <div class="so-form-12 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="title" class="form-control" placeholder="询价标题"></div>
                                                <div class="so-form-15 padding1"><input type="text" name="ouName" class="form-control" placeholder="采购单位"></div>
                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button">
                                                        <i class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn" type="reset" value="重置">
                                                </div>
                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                <div class="clearfix"></div>
                                                <div class="hiderow">
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="startDateStart" placeholder="报价起始时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="startDateEnd" placeholder="报价起始时间(止)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="quotationStartDate" placeholder="报价截止时间(起)" class="form-control form-datetime">
                                                    </div>
                                                    <div class="so-form-15 mr30">
                                                        <input type="text" name="quotationEndDate" placeholder="报价截止时间(止)" class="form-control form-datetime">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                            <div class="col-md-12" id="jqGridPager"></div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--锁定业务员历史情况--%>
<div class="modal fade" id="myModa1" style="z-index:1060;width:900px; margin:0 auto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">锁定业务员历史情况</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <div class="panel">
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li><span></span><span id="unifiedRfqNum"></span></li>
                                        <li style="width:35%">采购方：<span id="ouName"></span></li>
                                        <li style="margin-left:40px">锁定业务员姓名：<span id="recLockUsername"></span> </li>
                                        <li>锁定业务员登录名：<span id="recLockLoginName"></span></li>
                                        <li>锁定时间：<span id="recLockTime"></span></li>
                                        <li style="margin-left:88px">锁定状态：<span class="isLocked" id="isLocked"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="lockHistoryJqGrid" ></table>
                        <div class="col-md-12" id="lockHistoryJqGridPager"></div>
                    </div>
                </div>
                <div class="mt12" style="margin-left:408px"><a type="button" class="btn btn-primary"  href="javascript:;" class="close" data-dismiss="modal">关闭</a></div>
            </div>
        </div>
    </div>
</div>

<%--解锁--%>
<div class="modal fade" id="myModa2" style="width:900px; margin:0 auto; overflow: scroll;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">锁定业务员情况</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <div class="panel">
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li><span></span><span id="unifiedRfqNumLock"></span></li>
                                        <li style="width:35%">采购方：<span id="ouNameLock"></span></li>
                                        <li style="margin-left:40px">锁定业务员姓名：<span id="recLockUsernameLock"></span> </li>
                                        <li>锁定业务员登录名：<span id="recLockLoginNameLock"></span></li>
                                        <li>锁定时间：<span id="recLockTimeLock"></span></li>
                                        <li style="margin-left:88px">锁定状态：<span class="isLocked" id="isLockedLock"></span></li>
                                    </ul>
                                </div>
                                <input type="hidden" id="requestId" value="" />
                                <input type="hidden" id="recLockUseridLock" value="" />
                                <input type="hidden" id="idLock" value="" />
                                <input type="hidden" id="rMethod" value="" />
                                <div class="mt12" style="margin-left:335px;" ><a class="order-del-btn btn btn-sm btn-primary" data-toggle="modal" data-target="#myModa1" id="lookLockHistory">查看锁定历史情况</a></div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <table class="table table-bordered align-md" style="margin-bottom:5px;" id="lockJqGrid" ></table>
                        <div class="col-md-12" id="lockJqGridPager"></div>
                    </div>
                </div>
                <div class="mt12" style="margin-left:408px"><a type="button" class="btn btn-primary"  href="javascript:;" class="close" data-dismiss="modal">关闭</a></div>
            </div>
        </div>
    </div>
</div>

<script>


$(function(){

    $('.more-so').click(function(){
		$('.hiderow').stop().slideToggle('fast');

	});

    $('.wuliao-show').click(function(){
		$('.hiderow2').stop().show('fast');
	});


	 //日期
  $(".form-datetime").datetimepicker(
  {
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      showMeridian: 1,
      format: "yyyy-mm-dd hh:ii"
  });

  $(".form-date").datetimepicker(
  {
      language:  "zh-CN",
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
      format: "yyyy-mm-dd"
  });

  $(".form-time").datetimepicker({
      language:  "zh-CN",
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 1,
      minView: 0,
      maxView: 1,
      forceParse: 0,
      format: 'hh:ii'
  });

});


</script>
</body>
</html>
