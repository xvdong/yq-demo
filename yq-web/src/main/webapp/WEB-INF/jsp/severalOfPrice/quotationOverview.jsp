<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

<%--    <title><c:choose><c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}">待报价</c:when><c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}">待竞价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='RAQ'}">报价中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}">竞价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></title>--%>
    <title>报价总览</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <link href="${pageContext.request.contextPath}/css/zui.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/doc.min.css" rel="stylesheet">
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>

    <link href="${pageContext.request.contextPath}/css/common.css" rel="stylesheet"/>

    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
    <style>
        .bjzl-w{ width:650px !important;}
        .bjzl-l{ left:650px !important;}
        .bjzl-h{ height:82px !important;}
        .table.align-md th{ border-top:0 !important; border-left:0 !important;}
        .hj-tb .hjmx-tbody td{  word-break:break-all;}
        .bjzl-w2{ width:250px !important;}
        .bjzl-w3{ width:251px !important;}
    </style>

    <script>
        var ctx = '${ctx}';
        var resource = '${resource}';
    </script>
    <!--获取核价类型-->
    <script>
        var price_Type;
        $(document).ready(function () {
            $.ajax({
                url:ctx+"/rfqRequest/priceType",
                data:{requestId:"${vo.id}"},
                type:'POST',
                dataType:'json',
                async:false,
                success:function(data){
                    price_Type = data.toString();
                },
            })
        })
    </script>
</head>

<body>
<c:import url="../common/top.jsp"/>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="isFlag" value="0">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>  <li><a><i class="icon icon-home"></i> 工作台</a></li>
                    </ol>
                </div>
                <div class="page-content">
                    <c:import url="../common/rfqSummary.jsp">
                        <c:param name="requestId" value="AAA" />
                        <c:param name="flag" value="bjzl" />
                    </c:import>
                    <div class="hj-tb" style="padding-top:132px;">
                        <div class="hj-tb-xd" style="z-index:2;">
                            <div class="panel xx-title-panel mb12">
                                <div class="panel-body clearfix">
                                    <span class="mg-title-16">报价明细：</span>
                                    <span class="mg-sand">（总计 <a  href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?id=${vo.id}" target="hjsp-quotation_history_list" class="red">3</a> 家供应商报价）</span>

                                </div>
                            </div>
                            <div class="hjmx-th">
                                <div class="hjmx-th-gd bjzl-w bjzl-h">

                                    <div class="bjzl-h">
                                        <table class="table table-bordered align-md bjzl-h" style="border:0">
                                            <tr>
                                                <th width="80">序号</th>
                                                <th>物料代码 </th>
                                                <th width="80">物料名称</th>
                                                <th width="100">型号规格</th>
                                                <th width="100">采购数量<br>计量单位</th>
                                                <th width="80">参考单价</th>
                                                <th width="90">要求交货期</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="hjmx-th-fd bjzl-l  bjzl-h">
                                    <div class="re" id="div3">
                                        <div class="scrollDiv">
                                            <%--<div class="bjzl-h bjzl-w2">
                                                <div class="t">
                                                    <h4>上海欧冶国际股份有限公司</h4>
                                                    <span class="label label-warning">报价详情</span> 2000.00元

                                                </div>

                                            </div>--%>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hjmx-tbody">
                            <div class="hjmx-tbody-gd bjzl-w">

                                <table class="table table-bordered table-striped align-md">
                                    <tbody>
                                    <%--<tr>
                                        <td width="80">1</td>
                                        <td><span>c001 c001c001c001c001c001c001</span></td>
                                        <td width="80"><span>物料名物料名称物料名物料名物料名称物料名称称称</span></td>
                                        <td width="100"><span>X111</span></td>
                                        <td width="100"><span>7 个</span></td>
                                        <td width="80"><span>17</span></td>
                                        <td width="90"><span>2017-03-21</span></td>
                                    </tr>
--%>
                                    </tbody>
                                </table>
                            </div>

                            <div class="hjmx-tbody-fd bjzl-l">
                                <div class="re" id="div2">
                                    <div class="scrollDiv " id="scrollDiv">
                                        <%--<table class="table table-bordered table-striped align-md bjzl-w3" >

                                            &lt;%&ndash;<tbody>
                                            &lt;%&ndash;<tr>
                                                <td><span>供应商针对该条物料的报价供应商针对该条物料的报价供应商针对该条物料的报价供应商针对该条物料的报价</span></td>
                                            </tr>&ndash;%&gt;

                                            </tbody>&ndash;%&gt;
                                        </table>--%>
                                    </div>
                                </div>

                            </div>

                            <div id="bar-row" class="bjzl-l">
                                <div id="bar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        <!--  分页 -->
                        <div class="so-form-60 right mt20">
                            <div class="pull-right" style="margin-top:-45px;">
                                <ul class="pager pager-loose pagers">
                                    <li><span>共 <strong class="text-danger" id="total"></strong> 条记录 </span></li>
                                    <li><span><strong class="text-danger" id="pageNum"></strong></strong><strong class="text-danger">/</strong><strong id="pages" class="text-danger"></strong> 页</span></li>
                                    <li><span>每页显示
                           <select name="" class="form-control input-sm" id="pageSize">
                               <option selected="selected">20</option>
                               <option>50</option>
                               <option>100</option>
                           </select>
                           条</span></li>
                                    <li id="homePage"><a class="gray">首页</a></li>
                                    <li id="lastPage"><a class="gray">上一页</a></li>
                                    <li id="nextPage"><a class="gray">下一页</a></li>
                                    <li id="endPage"><a class="gray">尾页</a></li>
                                    <li><span>转到
                           <input type="text" class="form-control input-sm" size="3" id="pageNo" onkeyup="this.value=this.value.replace(/^0|\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
                           页</span> <span class="ml12"><a class="btn btn-sm" id="toPage">确定</a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 撤销询价原因模态框-->
<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">撤销询价原因</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因类别：</span>

                        <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                            <option value="不需要采购">不需要采购</option>
                            <option value="供应商退出">供应商退出</option>
                            <option value="其他原因">其他原因</option>
                        </select>
                    </p>
                    <div class="clearfix"></div>
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因说明：</span>

                        <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="requestId" id="requestId" value="${vo.id}">

<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<!-- ZUI Javascript组件 -->
<script src="${pageContext.request.contextPath}/js/zui.min.js"></script>

<!-- 公共JS组件 -->
<script src="${pageContext.request.contextPath}/js/base.js"></script>
<script type="text/javascript">
    $(function(){
        var panelThead=$("#panel-thead").height()+$("#panel-thead").offset().top-70;

        /*吸顶*/
        $('.hj-tb-xd').setScroll({
            defaultTop:panelThead,
            startTop:panelThead
        });


        /*function changeBar(){
            alert("============================================================");
            $('.hjmx-th-fd .scrollDiv>div:odd').css('background','#fff1d5');
            $('.hjmx-th-fd .scrollDiv>div:even').css('background','#ddf3f5');
            $('.scrollDiv').css('width',$('.scrollDiv>div').length*($('.scrollDiv>div').width()));
            $('.hjmx-tbody .hjmx-tbody-fd').height($('.hjmx-tbody-gd').height());

            $('.hj-tb').height($('.hjmx-tbody-gd').height()+10);
            $('#bar-row').css({
                'top':$('.hjmx-tbody-gd').height(),
                'width':$('.hjmx-tbody-fd').width()
            });
            $('.hjmx-th').width($('.hjmx-tbody').width()-2);
            $('.hjmx-tbody-gd tr').each(function(){
                var sda=$(this).index();
                var sheight=$(this).height();
                $('#scrollDiv table').each(function(){
                    $(this).find('tr').eq(sda).height(sheight);
                })
            })
        }*/


        $(window).resize(function(){
//            changeBar();

        });
//        changeBar();
        $('.zkBtn').click(function(){
            var This=$(this);
            $(this).next().toggleClass('hidden');
//            changeBar();
        });




    })



    /*自定义滚动*/
    var myWeb={};
//    window.onload=function(){
//        myWeb.scrollLeft("bar-row","div2","div3");
//    }

    function scroll(k){
        myWeb.scrollLeft("bar-row","div2","div3",k);
    }

</script>
<script src="${pageContext.request.contextPath}/js/addMouseWheel.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/upload/jquery.form.min.js"></script>
<script src="${pageContext.request.contextPath}/lib/kindeditor/kindeditor-all-min.js"></script>
<%--<!-- ZUI Javascript组件 -->
<script src="${pageContext.request.contextPath}/js/zui.min.js"></script>
<!--富文本JS-->
<script src="${pageContext.request.contextPath}/lib/kindeditor/lang/zh_CN.js"></script>&ndash;%&gt;
<!-- 公共JS组件 -->
<script src="${pageContext.request.contextPath}/js/base.js"></script>
<script src="${pageContext.request.contextPath}/js/addMouseWheel.js?v=${version}"></script>
<script src="${pageContext.request.contextPath}/js/designateResult/quotationOverview.js?v=${version}"></script>
--%>
<script src="${pageContext.request.contextPath}/js/designateResult/quotationOverview.js?v=${version}"></script>
</body>
</html>
