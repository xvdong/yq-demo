<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>多轮报价设置</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqLaunchedSeveralPrice/rfqLaunchedSeveralPrice.js?v=${version}"></script>
    <!-- json 转换工具 -->
    <script src="${pageContext.request.contextPath}/js/rfqLaunchedSeveralPrice/json2.js?v=${version}"></script>
    <!--选择时间和日期-->
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <!--日期插件css-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
    <script src="${pageContext.request.contextPath}/lib/kindeditor/kindeditor-all-min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/kindeditor/lang/zh_CN.js"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
</head>

<body>
<input type="hidden" value="${unifiedRfqNum}" name="unifiedRfqNum1" id="unifiedRfqNum1">
<input type="hidden" value="${auditType}" name="auditType" id="auditType">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="index.html"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">多轮报价设置</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">时间设置</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="so-form">
                                    <span class="required"></span>
                                    <div class="so-form-12 ml12 line-32">报价截止时间：</div>
                                    <div class="so-form-20 padding1"><input name="date" id="date" placeholder="截止日期" class="form-control form-datetime" type="text"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mg-header" style="margin-bottom:0px; border-bottom:none; padding-right:15px;">
                        <span class="required"></span>
                        <div class="font-16-y16">供应商设置：</div>
                            <span class="pull-right">
                                <a id="selectSupplier" class="trage" data-toggle="modal" data-target="#myModa33">
                                    <i class="icon icon-plus-sign"></i> 选择供应商</a>
                                <a class="trage" onclick="cleanSup()">清空</a>
                            </span>
                    </div>

                    <table id="SelListTab" class="table table-bordered table-hove align-md">
                        <thead>
                        <tr>
                            <th>供应商代码</th>
                            <th>供应商名称</th>
                            <th>联系人</th>
                            <th>联系电话</th>
                            <%--<th>联系地址</th>--%>
                            <th>备注</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div class="row">


                        <form class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <div class="mg-header " style=" margin:10px 0 0 20px; border-bottom:none;">
                                    <span class="required"></span>
                                    <div class="font-16-y16">多次报价说明（备注）</div></div>
                                <div class="col-md-12" style="padding:0px 20px;">
                                    <textarea id="contentSimple2" name="content" class= "col-md-10" style=" width:100%;height:150px;"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="text-right">
                            <a href="#" class="btn btn-lg btn-primary" type="button"   id="release">发布</a>

                        <a href="${pageContext.request.contextPath}/designateResult/init?unifiedRfqNum=${unifiedRfqNum}" class="btn btn-lg" type="button">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- 即时聊天
 <a href="#" id="im"></a> -->

<%--//选择供应商的弹框--%>
<div class="modal fade"  id="myModa33">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">选择供应商</h4>
               <%-- <input type ="text" id="tips"/>--%>
            </div>
            <div class="modal-body">

                <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
              <form id="formid" method = 'post'  >
                        <input id="unifiedRfqNum" name="unifiedRfqNum"; hidden/>
                    <div class="so-form-20 mr12" style="width:15%;">
                        <input class="form-control col-lg-8" placeholder="供应商名称" id="supplierName" name="supplierName" type="text">
                    </div>

                    <div class="so-form-20 mr12" style="width:20%;">
                        <input class="form-control" placeholder="联系人" id="linkmanName" type="text">
                    </div>
                    <div class="so-form-20" style="width:20%;">
                        <input class="form-control" placeholder="供应商代码" id="supplierCode" name="supplierCode" type="text">
                    </div>
                   </form>
                    <div class="so-form-20 ml12">
                        <button class="btn btn-primary" type="button" onclick="findSupplier(unifiedRfqNo)"><i class="icon icon-search"></i> 搜索</button>
                        <input class="btn" value="重置" type="reset" onclick="reset()"/>
                    </div>

                  </div>
             <%--供应商列表--%>
                <div class="content" >
                    <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                    <div class="col-md-12" id="jqGridPager"></div>
                </div>

            </div>

        <div class="modal-footer">

            <button type="button" class="btn btn-primary" data-dismiss="modal" id="selectSup">确定</button>
            <button type="button" class="btn btn-default" id="quxiao" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
</div>
    <!-- 发布(多伦报价审批入口) -->
    <div class="modal fade" id="myModa99">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                    <h4 class="modal-title">提交确认</h4>
                </div>
                <div class="modal-body">
                    <div class="alert-icon-t ml35">
                        <i class="icon icon-question-sign text-primary"></i>
                        <div class="content ml12">
                            <h4>你确定要提交本询价单吗?</h4>

                            <p>询单编号：　　<span data-mp="requestNo" id="newUnifiedRfqNum" name="newUnifiedRfqNum">${newUnifiedRfqNum}</span></p>
                            <p>
                                <span class="left">选择审批流程：</span>
                             <span class="col-lg-6 padding1">
                                 <c:choose>
                                     <c:when test="${approvalList.size()>0}">
                                         <select id="approvalCode" class="form-control input-sm" style="width:110px">
                                             <option value="0">无需审批</option>
                                             <c:forEach var="approvalBo" items="${approvalList}">
                                                 <option value="${approvalBo.number},${approvalBo.type}"><c:if test="${approvalBo.type == 1}">标准:</c:if><c:if test="${approvalBo.type == 2}">灵活:</c:if>${approvalBo.wfDefineName}</option>
                                             </c:forEach>
                                         </select>
                                     </c:when>
                                     <c:otherwise>
                                         <select id="approvalCode" class="form-control input-sm" style="width:110px" disabled="true">
                                             <option value="0">无有效审批</option>
                                         </select>
                                     </c:otherwise>
                                 </c:choose>
                            </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a type="button" id="sub" class="btn btn-primary" data-type="1"  href="javascript:;">确定提交</a>
                    <a type="button" class="btn"  data-type="2" href="javascript:;" data-dismiss="modal">返回修改</a>
                </div>
            </div>
        </div>
    </div>

<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo">
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
<%--提交审批成功弹框--%>
<%--<div class="modal fade" id="myModa">--%>
    <%--<div class="modal-dialog modal-sm">--%>
        <%--<div class="modal-content">--%>
            <%--<div class="modal-body">--%>
                <%--<div class="alert-icon-t"> <i class="icon-check text-primary"></i>--%>
                    <%--<div class="content">--%>
                        <%--<h4>提交审批成功！</h4>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class="modal-footer">--%>
                <%--<a type="button" class="btn btn-primary"  href="javascript:;">确定</a>--%>
                <%--<a type="button" class="btn"   href="javascript:;">取消</a>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>
    <%--<div class="modal fade" id="myModa99">--%>
        <%--<div class="modal-dialog modal-sm">--%>
            <%--<div class="modal-content">--%>
                <%--<div class="modal-body">--%>
                    <%--<div class="alert-icon-t"> <i class="icon-check text-primary"></i>--%>
                        <%--<div class="content">--%>
                            <%--<h4>提交审批成功！</h4>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
                <%--<div class="modal-footer">--%>
                    <%--<a type="button" class="btn btn-primary"  href="javascript:;">确定</a>--%>
                    <%--<a type="button" class="btn"  href="javascript:;">取消</a>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>


<script>
    var editor_contentSimple2;
    KindEditor.ready(function(K) {
        editor_contentSimple2 = K.create('textarea#contentSimple2', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });
/*----------------------------------------------------------------------------------------------------------------*/
    var unifiedRfqNo="${unifiedRfqNum}";
    var selectedSupplier=[];//已选中的供应商
    var supplierList=[];

    $(function(){
        //选择时间设为当前时间以后
        $("#date").datetimepicker("setStartDate",dateTime());
        $("#unifiedRfqNum").val(unifiedRfqNo);

        //点击选择供应商
        $("#selectSupplier").click(function(){

           // reset();//清空供应商名称和供应商代码条件框
            $('#myModa33 input[type="text"]').not('.phcolor').val('');
            $.placeholder();
            //重新加载供应商列表
            $("#jqGrid").jqGrid('setGridParam', {
                postData: form2Json("formid")
            }).trigger("reloadGrid");
           findSupplier(unifiedRfqNo);
        });
        //点击提交审核
        $("#sub").click(function () {
            submitTo();
        });


        //已选择供应商列表
        $("#selectSup").click(function(){
            var selSupplier = [];
            var ids=$("#jqGrid").jqGrid("getGridParam",'selarrrow');
//            console.info(ids);
            var flag=true;
            var selectedSup = [];
            for(var i=0;i<ids.length;i++){
                var rowData = $("#jqGrid").jqGrid("getRowData",ids[i]);
               // console.info(rowData);
                    if(selectedSupplier.length===0){
                        selectedSupplier.push(rowData);
                    }else{
                        for(var j=0;j<selectedSupplier.length;j++){
                            if(selectedSupplier[j].id==rowData.id){
                                selSupplier.push(rowData.supplierName);
                                $("#selectSup").removeAttr("data-dismiss");
                                flag=false;
                            }
                        }
                        if (flag){
                            selectedSupplier.push(rowData);
                            selectedSup.push(rowData);
                            $("#selectSup").attr("data-dismiss","modal");
                        }
                    }
            }
            if(!flag){
                for(var j=0;j<selectedSup.length;j++){
                    selectedSupplier.shift(selectedSup[j]);
                }
            }
            if(!flag){
                $.zui.messager.show('sorry,'+selSupplier+'已选择,已选择的供应商不可再选！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
                return false;
            }else{
                setSelectedSupplierList();
            }
         });
        $("#release").click(function(){
            var area =editor_contentSimple2.text();
            var date = Date.parse($("#date").val());
            var now =  Date.parse(dateTime());
            if($("#date").val()==null||$("#date").val()==""){
                $.zui.messager.show('sorry,请填写截止日期！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
                return;
            }else if( date < now){
                $.zui.messager.show('截止日期不能早于当前时间！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
                return;
            }else if(area === null || area === ""){
                $.zui.messager.show('请填写二轮报价原因！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
                return ;
            }else if(area.length>2000){
                $.zui.messager.show('报价原因不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
                return;
            }else if(selectedSupplier.length === 0){
                $.zui.messager.show('请选择至少一位供应商！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
                return ;
            } else{
                $("#myModa99").modal("show");
            }
        });
            /*            $('input[name=sel_checkbox]:checked').each(function(){
                                if(selectedSupplier.length==0){
                                    selectedSupplier.push(supplierList[$(this).val()])
                                }else{
                                    var flag=true;
                                    for(var i=0;i<selectedSupplier.length;i++){
                                        if(selectedSupplier[i].id==supplierList[$(this).val()].id){
                                            alert("已选择过的供应商不可选！");
                                           // $(this).checked=false;
                                            //TODO
                                            $("#selectSup").removeAttr("data-dismiss");
                                            flag=false;
                                            return false;
                                        }
                                    }
                                    if(flag)
                                        selectedSupplier.push(supplierList[$(this).val()])
                                        $("#selectSup").attr("data-dismiss","modal");
                                }
                            setSelectedSupplierList();

                         });*/

    });
    //主页面根据已选择的追加列表
    function setSelectedSupplierList(){
        var v_html="";
        $.each(selectedSupplier,function(i,d){
          //  console.info(d);
           /* if(d.supplierCode==="48325892375920"){
                var linkAddress = "河北省石家庄市桥东区地址信息2321";
            }else if(d.supplierCode==="48325892375921"){
                var linkAddress = "湖南省永州市祁阳县地址seewewr";
            }else if(d.supplierCode==="48325892375922"){
                var linkAddress = "安徽省阜阳市太和县地址fewgfr";
            }else if(d.supplierCode==="48325892375923"){
                var linkAddress = "江苏省南京市下关区地址eee";
            }else if(d.supplierCode==="48325892375924"){
                var linkAddress = "辽宁省沈阳市沈河区地址0000";
            }*/

            var tr='<tr><td align="center">supplierCode</td><td>supplierName</td><td>linkmanName</td><td>linkmanTelphone</td><!--<td>linkAddress</td>--><td></td><td width="140"><span class="ml5"><a class="btn btn-sm btn-info" href="javascript:void(0)" onclick="cancel(this,{dataIndex})"><i class="icon icon-remove-circle"></i> 取消邀请</a></span></td></tr>';//v_html= v_html.replace("linkmanTelphone", d.linkmanTelphone)
            v_html+= tr.replace("linkmanTelphone", d.linkmanTelphone)
                    .replace("linkmanName", d.linkmanName)
                    .replace("deviceDealerGrade", d.deviceDealerGrade)
                    .replace("supplierName", d.supplierName)
                    .replace("supplierCode", d.supplierCode)
                   /* .replace("linkAddress", linkAddress)*/
                    .replace("{dataIndex}", i)
                    .replace("index", i+1);
        });
        $("#SelListTab>tbody").html(v_html);
    }
/*        //供应商弹框的列表
        function findSupplier(){
            $.post("/rfqLaunchedSeveralPrice/findSupplier",
                    {
                        unifiedRfqNum:unifiedRfqNo
                    },
                    function (data){
                        $("#supplierSelTab>tbody").empty();
                        supplierList=data.RfqSupplierListVoList;
                        $.each(data.RfqSupplierListVoList,function(i,d){
                            console.log(d)
                            var v_html="<tr><td><input type='checkbox' name='sel_checkbox'  value='"+i+"'></td> <td>index</td> <td>supplierCode</td> <td>supplierName</td> <td></td> <td>deviceDealerGrade</td> <td>linkmanName</td> <td>linkmanTelphone</td></tr>";
                            v_html= v_html.replace("linkmanTelphone", d.linkmanTelphone);
                            v_html= v_html.replace("linkmanName", d.linkmanName);
                            v_html= v_html.replace("deviceDealerGrade", d.deviceDealerGrade);
                            v_html= v_html.replace("supplierName", d.supplierName);
                            v_html= v_html.replace("supplierCode", d.supplierCode);
                            v_html= v_html.replace("index", i+1);
                            $("#supplierSelTab>tbody").append(v_html);
                        });
                    },"json");
        }*/

    //供应商清空事件
    function cleanSup(){
        //selectedSupplier.splice(0,selectedSupplier.length);
      //  console.log(selectedSupplier);
        selectedSupplier.splice(0,selectedSupplier.length);
        $("#SelListTab>tbody").html("");
      //  setSelectedSupplierList();
    };
    //取消邀请
    function cancel(d,index){
        selectedSupplier.splice(index,1);
        //console.log(selectedSupplier);
        $("#SelListTab>tbody").html("");
        setSelectedSupplierList();
        //$("#SelListTab>tbody>tr:eq("+index+")").remove();
       // $("#SelListTab>tbody").html("")
       // $("#SelListTab>tbody>tr:eq(1)")
    }
    function reset(){
        $("#supplierName").val("");
        $("#supplierCode").val("");
        $("#linkmanName").val("");
    }

//传值到后台提交页面
    function submitTo(){
        var selAudit = $('#approvalCode').val();
        var procDefType = selAudit.split(",")[1];
        if(selAudit != 0) {   //选择审批
            if (procDefType == 2) {   //灵活流程
                $('#myModa99').modal('hide');
                $('#myRole').modal('show');
                return;
            } else {
                var $confirmBtn = $('#myModa99').find('.modal-footer .btn-primary');
                $confirmBtn.attr('disabled', true);
                postforData([]);
            }
        }else {
            var $confirmBtn = $('#myModa99').find('.modal-footer .btn-primary');
            $confirmBtn.attr('disabled', true);
            postforData([]);
        }
    }

    //ajax提交方法
    function postforData(rowDataLists){
        var area =editor_contentSimple2.html();
        $.ajax({
            type:'post',
            url:ctx + "/rfqLaunchedSeveralPrice/saveSupplierTime",
            dataType: 'json',
            async:false,
            data:{
                bo: JSON.stringify({
                    quotationEndDate: $("#date").val(),
                    roundEndDate: $("#date").val(),
                    unifiedRfqNum: unifiedRfqNo
                }),
                unifiedRfqNum: $("#unifiedRfqNum1").val(),
                voList: JSON.stringify(selectedSupplier),
                content: area,
                approvalCode: $('#approvalCode').val(),
                rowDataLists: JSON.stringify(rowDataLists)
            },
            success: function (data){
                if (data > 0){
                    $.zui.messager.show('提交成功！', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                    window.location.href="${pageContext.request.contextPath}/rfqRequest/init";
                }else {
                    $.zui.messager.show('sorry,提交失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        })
    }

    function dateTime (){
        var date = "";
        $.ajax({
            type: "POST",
            async:false,
            url:ctx + '/common/getNowTime',
            data: {
            },
            success: function (data) {
                date =  new Date(data*1000);
            }
        });
        var seperator1 = "-";
        var seperator2 = ":";
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        if (hours >= 0 && hours <= 9) {
            hours = "0" + hours;
        }
        if (minutes >= 0 && minutes <= 9) {
            minutes = "0" + minutes;
        }
        var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                + " " + hours + seperator2 + minutes ;
        return currentdate;
    }




    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });


</script>

<%--<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });
</script>--%>
<!--[if lt IE 9]>
<script type="text/javascript">
    jQuery(function($){
        $('#approvalCode').css('width','350px');
    });
</script>
<![endif]-->

</body>
</html>
