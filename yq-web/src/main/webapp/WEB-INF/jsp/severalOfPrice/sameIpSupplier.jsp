<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>多轮报价设置</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqLaunchedSeveralPrice/sameIpList.js?v=${version}"></script>
    <script>
        //日期
        $(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });

        $(".form-date").datetimepicker(
                {
                    language:  "zh-CN",
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

        $(".form-time").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            format: 'hh:ii'
        });
    </script>
</head>

<body>

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="index.html"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">IP地址相同供应商列表</li>
                    </ol>
                </div>
                <div class="page-content">
                     <input id="unifiedRfqNum" hidden value="${unifiedRfqNum}"/>
                    <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                    <div class="col-md-12" id="jqGridPager"></div>


                </div>


            </div>
        </div>
    </div>
</div>

<%--

<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<!-- ZUI Javascript组件 -->
<script src="${pageContext.request.contextPath}/js/zui.min.js"></script>
<!--富文本JS-->
<script src="${pageContext.request.contextPath}/lib/kindeditor/kindeditor-all-min.js"></script>
<script src="${pageContext.request.contextPath}/lib/kindeditor/lang/zh_CN.js"></script>
<script src="${pageContext.request.contextPath}/js/base.js"></script>
<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });

    //删除

    $('.order-del-btn').delBtn();
    var bClick=false;

    if(bClick){
        $('#myModa4').modal({
            keyboard : false,
            show     : true,
            backdrop :'static'
        })
    }
</script>--%>
</body>
</html>
