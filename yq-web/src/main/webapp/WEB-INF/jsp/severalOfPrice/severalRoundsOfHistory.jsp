<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>报价历史</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqLaunchedSeveralPrice/severalRoundsOfHistory.js?v=${version}"></script>
    <script>
        //日期
        $(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });

        $(".form-date").datetimepicker(
                {
                    language:  "zh-CN",
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

        $(".form-time").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            format: 'hh:ii'
        });
    </script>
</head>

<body>

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="index.html"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">报价历史</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="content">
                        <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                        <div class="col-md-12" id="jqGridPager"></div>
                    </div>
                    <input type="hidden" id="unifiedRfqNum" name="unifiedRfqNum" value="${unifiedRfqNum}">
                    <input type="hidden" id="ouRfqNum" name="ouRfqNum" value="${ouRfqNum}">
                    <input type="hidden" id="quotationRound" name="quotationRound" value="${quotationRound}">
                    <input type="hidden" id="requestId" name="id" value="${id}">
<%--                  <table class="table table-bordered table-hove align-md">
                        <thead>
                        <tr>
                            <th>询单号</th>
                            <th>询单标题</th>
                            <th>创建人</th>
                            <th>创建时间</th>
                            <th>截止时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                       <c:forEach items="${pageInfo.list}" var="z" varStatus="l">
                        <tr>
                            <td>${z.unifiedRfqNum}</td>
                            <td>${z.title}</td>
                            <td>${z.recRoundUsername}</td>
                            <td>${z.roundStartDate}</td>
                            <td>${z.roundEndDate}</td>
                            <td width="140"><span class="ml5"><a class="btn btn-sm btn-info" href="javascript:void(0);" target="9-hj-supply_chain_quotation_list" onclick="seeDetail('${z.quotationRound}')"><i class="icon icon-zoom-in"></i> 查看详情</a></span></td>
                        </tr>
                       </c:forEach>
                        </tbody>
                    </table>--%>
<%--                    <div class="col-md-12 clearfix">
                        <div class=" pull-right" style="margin-top: -18px;">
                            <ul class="pager pager-loose pagers">
                                <li><span>共 <strong class="text-danger">100</strong> 条记录</span></li>
                                <li><span><strong class="text-danger">1/5</strong> 页</span></li>
                                <li><span>每页显示
			                        <select name="" class="form-control input-sm">
                                        <option>20</option>
                                        <option>50</option>
                                        <option>100</option>
                                    </select>
			                        条</span></li>
                                <li> <a href="#">首页</a> </li>
                                <li> <a href="#">上一页</a> </li>
                                <li> <a href="#">下一页</a> </li>
                                <li> <a href="#">尾页</a> </li>
                                <li><span>转到
			                        <input type="text" class="form-control input-sm" size="3">
			                        页</span> <span class="ml12"><a class="btn btn-sm">确定</a></span></li>
                            </ul>
                        </div>
                    </div>--%>

                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
