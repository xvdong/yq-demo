<%--
  Created by IntelliJ IDEA.
  User: cbyin
  Date: 2016/10/10
  Time: 16:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>

<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>多轮报价发布</title>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>

    <c:import url="../common/jqGridBootstrap.jsp"/>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestDetail.js?v=${version}"></script>

</head>
<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">创建多轮报价审批</li>
                    </ol>
                </div>
                <div class="page-content">

                    <div class="panel">
                        <div class="panel-heading clearfix">

                            <div class=" pull-right">
                                <%--<a href="hejiafabu-shenpilishiyemiannew.html">
                                    <span class="label mg-btn label-info">审批流程跟踪</span>
                                </a>--%>
                                    <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                        <a class="btn btn-warning font12" data-position="center" data-toggle="modal" data-target="#adjustTime">调整时间</a>
                                        <span class="btn btn-danger font12"  onclick="requestPrint()">打印询价单</span>
                                        <span class="btn btn-warning font12" onclick="requestDownload()">下载询价单</span>
                                    </p:permission>
                            </div>
                        </div>

                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li style="width:40%;">询价标题：${rfqRequestComplexVo.requestVo.title}</li>
                                        <li>计划编号：${rfqRequestComplexVo.requestVo.planNo}</li>

                                    </ul>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div>
                        <span class="left line-32" style="font-size:16px; display:inline-block">物料信息</span>
                        <span class="red right line-32"
                              style=" display:inline-block">预算总价：${rfqRequestComplexVo.requestVo.totalPrice}元</span>
                    </div>
                    <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;"
                           id="jqGrid"></table>
                    <div class="col-md-12 clearfix" id="jqGridPager">

                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">供应商设置</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix line-32">
                                        <span class="left" style="font-weight:bold; color:#333; display:inline-block">供应商信息：</span>
                                    </div>
                                    <table class="table table-bordered align-md" style="width:98%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>供应商U代码</th>
                                            <th>特邀供应商名称</th>
                                            <th width="80px">联系人</th>
                                            <th>联系电话</th>
                                            <th width="80px">微信状态</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="supplierList" items="${rfqRequestComplexVo.supplierList}">
                                                <tr>
                                                    <td>${supplierList.rowno}</td>
                                                    <td>${supplierList.supplierCode}</td>
                                                    <td>${supplierList.supplierName}</td>
                                                    <td>${supplierList.linkmanName}</td>
                                                    <td>${supplierList.linkmanTelphone}</td>
                                                    <td>${supplierList.isBindWechat}</td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                </div>


                            </div>

                        </div>
                    </div>


                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">商务与技术条款</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-1">商务条款：</label>
                                        <span class="bler blu-88-nobg">${rfqRequestComplexVo.requestVo.requestBusiTerms}</span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-1">技术条款：</label>
                                        <span class="bler blu-88-nobg">${rfqRequestComplexVo.requestVo.requestTecTerms}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">询价规则</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <span class="col-xs-5">创建人：${rfqRequestComplexVo.requestVo.recCreatorUsername}</span>
                                        <span class="col-xs-5">创建时间：${rfqRequestComplexVo.requestVo.recCreateTime}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">发布时间：${rfqRequestComplexVo.requestVo.issueDate}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">报价开始时间：${rfqRequestRoundHistoryVo.roundStartDate}</span>
                                        <span class="col-xs-5">报价截止时间：${rfqRequestRoundHistoryVo.roundEndDate}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">保证金金额：<span
                                                class="red">${rfqRequestComplexVo.rulesVo.assureMoney}元</span></span>
                                        <span class="col-xs-5">报价货币：${rfqRequestComplexVo.requestVo.currency}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">分项分量报价：
                                            <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag=='1'}">
                                                允许对部分产品报价
                                            </c:if>
                                            <c:if test="${rfqRequestComplexVo.rulesVo.partialQuantityFlag=='1'}">
                                                &nbsp; 允许对部分数量报
                                            </c:if>
                                        </span>
                                        <span class="col-xs-5">询价公开程度：
                                            <c:if test="${rfqRequestComplexVo.rulesVo.isPublicOnlineMarket=='1'}">
                                                公开询价公告
                                            </c:if>
                                            <c:if test="${rfqRequestComplexVo.rulesVo.isPublicPub=='1'}">
                                                &nbsp; 公开中标供应商
                                            </c:if>
                                            <c:if test="${rfqRequestComplexVo.rulesVo.isPublicBidMoney=='1'}">
                                                &nbsp; 公开中标价格
                                            </c:if>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="line-32 mt12">
                            <span class="left" style="font-size:16px; display:inline-block">角色设置</span>
                        </div>
                        <table class="table table-bordered align-md">
                            <thead>
                            <tr>
                                <th width="50px">序号</th>
                                <th>工号</th>
                                <th>姓名</th>
                                <th>部门</th>
                                <th>职位</th>
                                <th>查看</th>
                                <th>管理</th>
                                <th>财务管理</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${rfqRequestComplexVo.projectUserList}">
                                <tr data-clone="orginal">
                                    <td>${item.seq}</td>
                                    <td>${item.jobCode}</td>
                                    <td>${item.userName}</td>
                                    <td>${item.userOffice}</td>
                                    <td>${item.userTitle}</td>
                                    <td>
                                        <input
                                                <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if>
                                                type="checkbox" disabled>
                                    </td>
                                    <td>
                                        <input
                                                <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if>
                                                type="checkbox" disabled>
                                    </td>
                                    <td>
                                        <input
                                                <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if>
                                                type="checkbox" disabled>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">联系方式</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="mg-pacne clearfix" style="margin:0 15px;">
                                    <ul>
                                        <li>联系人：${rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                        <li>联系电话：${rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                        <li style="width:40%">交货地址：
                                            ${rfqRequestComplexVo.contactsVo.deliveryProvince}
                                            ${rfqRequestComplexVo.contactsVo.deliveryCity}
                                            ${rfqRequestComplexVo.contactsVo.deliveryArea}
                                            ${rfqRequestComplexVo.contactsVo.deliveryAddress}
                                        </li>
                                    </ul>

                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="mg-header5">
                        询单附件
                    </div>
                    <table class="table table-bordered align-md" style="margin-bottom:5px;">
                        <thead>
                        <tr>
                            <th width="50px">序号</th>
                            <th>文件名称</th>
                            <th width="100px">文件大小</th>
                            <th>说明</th>
                            <th width="100px">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}">
                            <tr data-clone="orginal">
                                <td data-order="true">${item.seq}</td>
                                <td>${item.originalFilename}</td>
                                <td>${item.fileSize}KB</td>
                                <td></td>
                                <td><a href="${pageContext.request.contextPath}/rfqAttachments/fileDownload?id=${item.attachmentId}&file=${item.downloadFilename}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="red line-32" style="font-size:12px;">
                        注：附件类型不支持exe格式，文件大小不超过100M。
                    </div>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">多轮报价说明</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                            ${generalAuditingVo.applicationDesc}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="text-right">
                            <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                <a href="" class="btn btn-lg btn-warning" type="button" onclick="release()" data-toggle="modal" data-target="#myModa6">发布</a>
                            </p:permission>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div



<!-- 调整时间弹窗开始 -->
<div class="modal fade" id="adjustTime">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">调整时间</h4>
            </div>
            <div class="modal-body modal-body2">

                <div class="mg-row">
                    <span class="block-title">报价截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                       <%-- <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                            <li><a href="#" id="overtime">立即结束</a></li>
                            <li><a href="#" id="starttime">指定时间</a></li>
                        </ul>--%>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date" placeholder="${rfqRequestComplexVo.requestVo.quotationEndDate}" class="form-control form-datetime">
                    </div>
                </div>
                <p>调整原因：</p>
                <div class="">
                    <textarea id="remark" class="form-control" rows="6" placeholder=""></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateTime()">保存</button>
            </div>
        </div>
    </div>
</div>
<%--<c:set var="ctx" value="${rfqRequestComplexVo.preauditSupplierList}"></c:set>--%>
<!-- 调整时间弹窗结束 -->
<%--<a href="#" id="im"></a>--%>

<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->

<%--<script src="dist/js/jquery.js"></script>--%>

<!-- ZUI Javascript组件 -->

<%--<script src="dist/js/zui.min.js"></script>--%>

<!--选择时间和日期-->

<%--<script src="dist/lib/datetimepicker/datetimepicker.min.js"></script>--%>

<script>

    $('#xzjs').on('click', function () {
        $('#myModa5').modal()
    })
</script>

<script>


    //日期

    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });


    $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });


    $(".form-time").datetimepicker({
        language: "zh-CN",
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });

</script>

<!--富文本JS-->

<%--<script src="dist/lib/kindeditor/kindeditor-all-min.js"></script>

<script src="dist/lib/kindeditor/lang/zh_CN.js"></script>--%>

<script>

/*    var editor;

    KindEditor.ready(function (K) {
        editor = K.create('textarea#remark', {
            resizeType: 1,
            allowPreviewEmoticons: false,
            allowImageUpload: false,
            items: []
        });
    });*/


</script>
<script>
    function release() {
        var now = new Date().getTime();
        var quotationEndDate = Date.parse('${rfqRequestRoundHistoryVo.roundEndDate}'.replace(/-/g,'/'));
        var endDate = '${rfqRequestRoundHistoryVo.roundEndDate}';
        if((quotationEndDate-now)>0){


        var data = {
            unifiedRfqNum: '${rfqRequestComplexVo.requestVo.unifiedRfqNum}',
            quotationEndDate:endDate
        };
        $.ajax({
            url:ctx+'/rfqLaunchedSeveralPrice/launchedServeralRelease',
            data: data,
            type: 'post',
            cache: false,
            dataType: 'json',
            success: function (data) {
                if (data.rspcod == "success") {
                    showSuccessMsg("发布成功");
                    setTimeout(function () {window.location.href=ctx+"/rfqRequest/init";},1000);

                } else {
                    showErrorMsg("发布失败");
                }
            },
            error: function () {
                showErrorMsg("发布失败");
            }
        });
        }else{
            showWarnMsg("报价截止时间已过期，请调整报价截止时间");
        }
    }
    /*
    更新报价截止时间
     */
    function updateTime() {
        var remark = $("#remark").val();
       var  data= {
                    roundEndDate:$("#date").val(),
                    requestId: '${rfqRequestComplexVo.requestVo.id}',
                    remarks: remark,
                    unifiedRfqNum: '${rfqRequestComplexVo.requestVo.unifiedRfqNum}'
        };
        if(remark==undefined||remark ==''){
            showWarnMsg("调整原因不可为空，请重新填写！");
        }else {
            $.ajax({
                url: ctx + '/rfqLaunchedSeveralPrice/updateTime',
                data: data,
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.rspcod == 'success') {
                        window.location.reload();
                    } else {
                        $.zui.messager.show('时间修改失败！', {
                            placement: 'center',
                            type: 'danger',
                            icon: 'icon-exclamation-sign'
                        });
                    }
                },
                error: function () {
                    $.zui.messager.show('时间修改失败！', {
                        placement: 'center',
                        type: 'danger',
                        icon: 'icon-exclamation-sign'
                    });
                }
            });
        }

    }
    function showSuccessMsg(message) {
        new $.zui.Messager(message, {
            type: 'success' // 定义颜色主题
        }).show()
    }
    function showErrorMsg(message){
        new $.zui.Messager(message, {
            type: 'danger' // 定义颜色主题
        }).show()
    }
    function showWarnMsg(message){
        new $.zui.Messager(message, {
            type: 'warning' // 定义颜色主题
        }).show()
    }
</script>
<input type="hidden" id="requestId" value="${rfqRequestComplexVo.requestVo.id}" />
</body>
</html>
