<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>未中标信息</title>
    <%@include file="../common/tagDeclare.jsp" %>
    <%@include file="../common/headBase.jsp" %>
    <c:import url="../common/kingEditor.jsp"/>
    <c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
    <%@include file="../common/jqGridBootstrap.jsp" %>
    <link href="${ctx}/css/auctionroomtotall.css?v=${version}" rel="stylesheet">
    <script src="${ctx}/js/designateResult/pricingCommonality.js?v=${version}"></script>
    <script type="text/javascript" src="${ctx}/js/weizhongbiao/weizhongbiaoDetail.js?v=${version}"></script>
    <script type="text/ecmascript" src="${ctx}/js/grid.inlinedit.js?v=${version}"></script>

    <script src="${ctx}/lib/plupload-2.1.9/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="${ctx}/js/upload/uploadFile.js?v=${version}"></script>

    <!--富文本JS-->
    <script src="${ctx}/lib/kindeditor/kindeditor-all-min.js"></script>
    <script src="${ctx}/lib/kindeditor/lang/zh_CN.js"></script>

    <!-- 引入 echarts.js -->
    <script src="${ctx}/js/echarts.common.min.js"></script>

    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>

<body>
<c:import url="../common/top.jsp"/>
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp"/>
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="index.jsp"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">未中标信息</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix" style="padding:0 15px; line-height:36px;">
                            <span class="mg-margin-right">基本信息</span>
                            <div class=" pull-right">
                                <a href="${ctx}/dacReverseAuction/reverseAuctionDetail?id=${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank">
                                    <span class="btn btn-primary font12">竞价单详情</span>
                                </a>
                                <span class="btn btn-warning font12"  onclick="dacDownload('${rfqQuotationComplexVo.rfqQuotationVo.requestId}','${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.biddingMethod}','${quotationType==null?"6":"30"}')">下载物料明细</span>
                                <c:if test="${quotationType=='3'}">
                                    <span class="btn btn-warning font12" onClick="excelClicke()"
                                          data-toggle="modal" data-target="#cancelPrice33">上传物料明细</span>
                                </c:if>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="color:#666;">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li style="width:40%">竞价单号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}</li>
                                            <li style="width:40%">计划编号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.planNo}</li>
                                            <li style="width:40%">竞价标题：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.title}</li>
                                        </ul>
                                        <span class="zt-ico-w" style="right:150px"><img src="${ctx}/images/wzb.png"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <input type="hidden" id="requestId" value="${rfqQuotationComplexVo.rfqQuotationVo.requestId}"/>
                            <input type="hidden" id="subtotalTaxed" value="${rfqQuotationComplexVo.rfqQuotationVo.subtotalTaxed}"/>
                            <input type="hidden" id="memo" value="${rfqQuotationComplexVo.rfqQuotationItemVo[0].memo}"/>
                            <input type="hidden" id="quotationId" value="${rfqQuotationComplexVo.rfqQuotationVo.id}"/>
                            <%--竞价方式--%>
                            <input type="hidden" id="biddingMethod" value="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.biddingMethod}"/>
                            <%--状态--%>
                            <input type="hidden" id="rtype" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type}"/>
                            <input type="hidden" id="qstatus" value="${rfqQuotationComplexVo.status}"/>

                            <span class="mg-margin-right">竞价物料</span>
                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.biddingMethod==1}">
                                <span class="right font-16px">竞价金额：<a class="blue" onclick="showMap2()"><strong class="red">${rfqQuotationComplexVo.rfqQuotationVo.subtotalTaxed}元</strong></a></span>
                            </c:if>

                        </div>
                        <div class="panel-body padding1 class1">
                            <table class="table table-bordered table-hove mt5 align-md" style="font-size:14px" id="jqGrid">
                            </table>
                            <!--  分页 -->
                            <div class="col-md-12" id="jqGridPager"></div>
                        </div>
                    </div>

                    <div class="panel" id = "desc">
                        <div class="panel-heading clearfix"> <span class="mg-margin-right">未中标说明</span> </div>
                            <div class="panel-body padding1">
                            <textarea id="contentSimple" name="content" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">
                                ${rfqQuotationComplexVo.memoDesc}
                            </textarea>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix"> <span class="mg-margin-right">补填明细附件</span></div>
                        <div class="panel-body padding1">
                            <table class="table table-bordered table-hove mt5 align-md" style="font-size:14px">
                                <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>文件名称</th>
                                    <th>文件大小</th>
                                    <th>说明</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody id="filelsit">
                                <c:set var="sum" value="1"/>
                                <c:forEach items="${rfqQuotationComplexVo.rfqRequestComplexVo.attachmentsList}" var="attachment" varStatus="status">
                                    <c:if test="${attachment.type == 'Q'and attachment.supplierCode == rfqQuotationComplexVo.rfqQuotationVo.supplierNum}">
                                        <tr data-clone="orginal">
                                            <td>${sum}</td>
                                            <td>${attachment.originalFilename}</td>
                                            <td>${attachment.fileSize}KB</td>
                                            <td>${attachment.fileDeclaration}</td>
                                            <td>
                                                <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type=='10'}">
                                                    <a href="${downloadAction}?id=${attachment.attachmentId}"><i
                                                            class="icon icon-download-alt green "></i>&nbsp;下载</a>
                                                </c:if>

                                                <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type !='10'}">
                                                    <a href="javascript:deleteFile(${attachment.attachmentId});"><i class="red icon icon-trash"></i>&nbsp;删除</a>
                                                </c:if>
                                            </td>
                                        </tr>
                                        <c:set var="sum" value="${sum + 1}"/>
                                    </c:if>
                                </c:forEach>
                                </tbody>
                            </table>
                            <span class="c999"  style="color: red;"> 附件类型不支持exe格式，文件大小不超过100M。              </span>
                            <div class="btn-group left">
                                <div class="so-form-40">
                                    <button id="addAttachments" class="btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#myModa2"><i class="icon icon-plus"></i>&nbsp;&nbsp;新增附件</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right" id="buttonDIV"><a href="javascript:submitSave();"  class="btn btn-lg btn-warning" data-type="1" type="button" >提交</a><a href= ${ctx}+"/rfqQuotation/init" class="btn btn-lg btn-danger ml5" data-type="2" type="button">关闭</a> </div>

                </div>

            </div>
        </div>
    </div>
</div>


<%--<div class="modal fade" id="myModa6">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">提交确认</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t ml35"> <i class="icon icon-question-sign text-primary"></i>
                    <div class="content ml12">
                        <h4>你确定要提交本竞价单吗?</h4>
                        <p>竞价标题：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.title}</p>
                        <p>竞价单号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}</p>
                        <p>计划编号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.planNo}</p>
                        <p>
                            <span class="left">选择审批流程：</span>
                            <span class="col-lg-6 padding1">
                                 <select name="" class="form-control input-sm">
                                    <option>无需审批</option>
                                    <option>采购500万以上审批流程</option>
                                </select>
                            </span>
                        </p>
                        <div class="clearfix"></div>
                        <p class="mt12"> <span class="left">审批人：</span>
                            <span class="col-lg-6 padding1">
                                <select name="" class="form-control input-sm">
                                    <option>张三</option>
                                </select>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:submitSave();">确定提交</a> <a type="button" class="btn"  href="javascript:;">返回修改</a></div>
        </div>
    </div>
</div>--%>


<!-- ///////////////////////新增附件2/////////////// -->
<div class="modal fade"  id="myModa2">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                           <form>
                               <input type="text"  id="fileName" placeholder="请上传,附件大小≤100M"  class="form-control no-border" />
                               <input type="button" id="upload" value="上传">
                               <span  id="msg">0%</span>
                           </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration" class="form-control" maxlength="1000"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary js-confirm" data-dismiss="modal" id="sure" disabled="disabled">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- id2结束 -->
<!-- excel导入开始 -->
<div class="modal fade" id="cancelPrice33">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">excel导入</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row clearfix" style="margin-bottom:8px;">
                    <span class="left-cont2">请上传根据模版编辑好的文件。</span>
                    <form action="${resource}/rfqRequestItemTmp/importRecord2" method="post" class="form-horizontal" id="excelUpload" onsubmit="return false;">
                        <input id="requestIdURL" name="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                        <input type="file" name="file" id="excelFile">
                        <input type="text"  id="fileNamess" placeholder="请上传,附件大小≤100M" disabled /><span  id="msgss"></span>
                    </form>

                    </span>
                </div>
                <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px; height:66px;">
                    <i class="icon icon-warning-sign sign-warn1"></i><span class="sign-1 ml12">
                        注意：Excel批量导入将覆盖询单内现有物料；
                    <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上传附件文件类型仅限Excel文件。</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"  onclick="upstart()" class="btn btn-primary js-confirm" data-dismiss="modal" id="suress" disabled="disabled">确定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- excel导入结束 -->
<div class="modal fade" id="myModal19">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title" id="materialName"></h4>
            </div>
            <div class="modal-body">
                <div id="mainChart" style="width:870px;height:400px;"></div>
            </div>
            <div class="modal-footer"> <a type="button" data-dismiss="modal" class="btn btn-primary" href="javascript:;">确定</a> </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal20">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的出价</h4>
            </div>
            <div class="modal-body">
                <div id="mainChart1" style="width:870px;height:400px;"></div>
            </div>
            <div class="modal-footer"> <a type="button" data-dismiss="modal" class="btn btn-primary" href="javascript:;">确定</a> </div>
        </div>
    </div>
</div>


<script>
    //日期
    $(".form-datetime").datetimepicker({
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd"
    });

    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple', {
            resizeType: 1,
            allowPreviewEmoticons: false,
            allowImageUpload: false,
            items: [],
            readonlyMode : true
        });
    });

</script>


<script>
    /* 初始化数据表格 */
    $('table.datatable').datatable({
        fixedLeftWidth:'50px',
        fixedHeader:false,
        ready: function()
        {
            $('.btn-group').selectLisr();
        }
    });

    /*
     上传
     */
    $(function (){
        var $myModa2 = $('#myModa2');
        $myModa2.on('click', '.js-confirm', function () {
            var data = $myModa2.data('_result');
            if(data==undefined || data==''){
                showErrorMsg("您还没选择文件！");
            }
            if(!data.fileSize){
                showErrorMsg('文件未上传完成，请重新上传');
                return false;
            }
            var requestid = $("#requestId").val();
            var fileDeclaration = $("#fileDeclaration").val();
            if(checkLen(fileDeclaration)){
                showErrorMsg("文件说明长度超过2000字数限制！")
            }else{
                data["fileDeclaration"]=fileDeclaration;
                data["requestId"]=requestid;
                /*dataObj["type"]='P';*/
                if('${rfqPreaudit.paId}'!= null&&'${rfqPreaudit.paId}'!=''){
                    data["preauditId"]='${rfqPreaudit.paId}';
                }
                data["supplierCode"]='${sysUserVo.supplierCode}';
                var url=ctx+"/weizhongbiao/saveUpload";
                $.ajax({
                    url:url,// 跳转到 action
                    type: 'post',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if(data.rspmsg=='true'){
                            showSuccessMsg("文件上传成功");
                            updateFileList(requestid);

                        }else{
                            showErrorMsg("文件上传失败");
                        }
                    },
                    error:function (data) {
                        showErrorMsg("文件保存失败");
                    }
                });

            }
        })
    })
    /*
     刷新文件列表
     */
    function updateFileList(id){
        var url=ctx+"/weizhongbiao/updateFileList";
        var data={id : id};
        $.ajax({
            url:url,// 跳转到 action
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                var s = '';
                $.each(data,function(i,item){
                    s+='<tr data-clone="orginal">';
                    if(item.seq!=undefined && item.seq!='null'){
                        s+="<td>"+item.seq+"</td>";
                    }else{
                        s+="<td></td>";
                    }
                    if(item.originalFilename != undefined && item.originalFilename != 'null') {
                        s += '<td data-order="true">' + item.originalFilename + '</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileSize != undefined && item.fileSize != 'null') {
                        s+='<td>'+item.fileSize+'KB</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileDeclaration != undefined && item.fileDeclaration != 'null') {
                        s+='<td>'+item.fileDeclaration+'</td>';
                    }else{
                        s+="<td></td>";
                    }
                    s+='<td><a href="javascript:deleteFile('+item.attachmentId+');"><i class="red icon icon-trash"></i>&nbsp;删除</a></td>';
                    s+='</tr>';
                });
                $("#filelsit").html("");
                $("#filelsit").html(s);
            }
        });
    }

    /* 字符串长度验证*/
    function checkLen(str){
        var newStr =  str.split('&lt;').join('<').split('&gt;').join('>').replace(/\s+/g, "");
        var myLen = newStr.length;
        if (myLen > 2000 ) {
            return true;
        }
        else {
            return false;
        };
    };
        /*
         删除附件
         */
        function deleteFile(attachmentId){
            var url=ctx+"/weizhongbiao/deleteUpload";
            var data={attachmentId : attachmentId};
            var requestid = $("#requestId").val();
            $.ajax({
                url:url,
                type: 'post',
                data: data,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if(data.rspmsg=='true'){
                        showSuccessMsg("文件删除成功");
                        updateFileList(requestid);
                    }else{
                        showErrorMsg("文件删除失败");
                    }
                },
                error:function (data) {
                    showErrorMsg("文件删除失败");
                }
            });
        }

        $(document).ready(function (){
            var types=[".jpg",".jpeg",".gif",".png",".doc",".docx",".xls",".xlsx",".pdf"];
            var maxSize=104857600;//100M，单位:B
            if(navigator.appName == "Microsoft Internet Explorer"){
                $("#myModa2").on('show.zui.modal', function () {
                    //初始化flash插件
                    initPlupload2(maxSize,types);
                    //关闭时清理页面
                    initEvent();
                });
            }else {
                //初始化flash插件
                initPlupload(maxSize,types);
                //关闭时清理页面
                initEvent();
            }
        })

</script>
<script>
    //明细清单Excel flash上传
    $(document).ready(function (){
        var typesss=[".xls",".xlsx"];
        var maxSize=104857600;//100M，单位:B
        var requestId=$("#requestId").val();
//        alert("================"+requestId);
        var money='${rfqQuotationComplexVo.rfqQuotationVo.subtotalTaxed}';
//        alert("=========#####"+money);
        if(navigator.appName == "Microsoft Internet Explorer"){
            $("#cancelPrice33").on('show.zui.modal', function () {
                //初始化flash插件
                initPlupload2mx(maxSize,typesss,requestId,money);
                //关闭时清理页面
                initEventse();
            });
        }else {
            //初始化flash插件
            initPluploadmx(maxSize,typesss,requestId,money);
            //关闭时清理页面
            initEventse();
        }
    });
</script>
</body>
</html>

