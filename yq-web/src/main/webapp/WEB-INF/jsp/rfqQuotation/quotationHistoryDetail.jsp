<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>历史报价单</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfq.datatable.edit.js?v=${version}"></script>
    <style>

        .flex-col{  width:120px !important; text-align:center; vertical-align:middle;}

        .flexarea td{  width:120px !important;text-align:center; vertical-align:middle;}

        .fixed-left td,th{text-align:center; vertical-align:middle;}

        .fixed-left tr td:nth-of-type(2){text-align:left; padding-bottom:20px;}
        .hide{
            display: none;
        }
    </style>

</head>

<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">历史报价单</li>
                        <li class="right">
                            <span class="right-titl-y">
                                （报价时间：${rfqQuotationComplexVo.rfqQuotationHisotryVo.quotationDate}）
                            </span></li>
                        <c:if test="${isBuyer}">
                            <div style="float: right;margin-top: 10px;">
                                <strong class="red">
                                    报价供应商名称：${rfqQuotationComplexVo.rfqQuotationHisotryVo.supplierName}
                                    供应商代码：${rfqQuotationComplexVo.rfqQuotationHisotryVo.supplierNum}
                                </strong>
                            </div>
                        </c:if>
                    </ol>
                </div>

                <div class="page-content">
                    <div class="page-content">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价单摘要</span>
                            </div>
                            <input id="dataSource" type="hidden" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.dataSource}"/>
                            <div class="panel-body font12">
                                <div class="row">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>询价单号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}</li>
                                            <li>询价标题：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.title}</li>
                                            <li>采购单位：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouName}</li>
                                            <li>计划编号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.planNo}</li>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>保证金金额：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.assureMoney}元</li>
                                            <li>报价货币：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.currency}元</li>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                            <%--<li>预算总价：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.budgetPrice}元</li>--%>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>发布时间：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.issueDate}</li>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationRound == null || rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationRound == 0}">
                                                <li>报价开始时间：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.startDate}</li>
                                                <li>报价截止时间：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                            </c:if>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationRound > 0}">
                                                <li>报价开始时间：${rfqQuotationComplexVo.rfqQuotationHisotryVo.roundStartDate}</li>
                                                <li>报价截止时间：${rfqQuotationComplexVo.rfqQuotationHisotryVo.roundEndDate}</li>
                                            </c:if>

                                            <li>报名截止时间：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>联系人：${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                            <li>联系电话：${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                            <li>收货地址：
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryProvince}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryCity}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryArea}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryAddress}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价单条款协议</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                        <div class="clearfix">
                                            <label class="bler blu-1">商务条款：</label>
                                            <span class="bler blu-88-nobg">
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.requestBusiTerms}
                                            </span>
                                        </div>
                                        <div class="clearfix">
                                            <label class="bler blu-1">技术条款：</label>
                                            <span class="bler blu-88-nobg">
                                              ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.requestTecTerms}
                                            </span>
                                        </div>
                                        <div class="mt12 clearfix">
                                            <span class="col-xs-12">附件清单：</span>
                                            <table class="table table-bordered mt12 align-md fixed" style="width:97%;">
                                                <thead>
                                                <tr>
                                                    <th width="50px">序号</th>
                                                    <th>文件名称</th>
                                                    <th width="100px">文件大小</th>
                                                    <th>说明</th>
<%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>   <th width="100px">操作</th><%--</p:permission>--%>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:set var="sum" value="1"/>
                                                    <c:forEach items="${rfqQuotationComplexVo.rfqRequestComplexVo.attachmentsList}" var="attachment" varStatus="status">
                                                        <c:if test="${attachment.type == 'R'}">
                                                        <tr data-clone="orginal">
                                                            <td>${sum}</td>
                                                            <td>${attachment.originalFilename}</td>
                                                            <td>${attachment.fileSize}KB</td>
                                                            <td>${attachment.fileDeclaration}</td>
                                                           <%-- <p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}"> --%><td><a href="${downloadAction}?id=${attachment.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                                        </tr>
                                                            <c:set var="sum" value="${sum + 1}"/>
                                                        </c:if>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <div class="left font14">报价历史物料</div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <table class="table datatable table-bordered align-md" id="requestItemData">
                                        <thead>
                                        <tr>
                                            <th data-width="80">序号</th>
                                            <th>物料信息
                                                <a class="btn btn-danger btn-mini" id="all-zk" href="javascript:allZk();">
                                                    <span class="zk_span">全部展开</span>
                                                    <span class="zk_span hide">全部隐藏</span>
                                                </a>
                                            </th>
                                            <th data-width="80">采购数量</th>
                                            <th class="flex-col">可供量</th>
                                            <th class="flex-col">*未税单价(元)</th>
                                            <th class="flex-col">*税率</th>
                                            <th class="flex-col">含税单价</th>
                                            <th class="flex-col">未税总价</th>
                                            <th class="flex-col">含税总价</th>
                                            <th class="flex-col">*承诺交货期</th>
                                            <th class="flex-col">替代型规</th>
                                            <th class="flex-col">制造商</th>
                                            <th class="flex-col">产地</th>
                                            <th class="flex-col">上一级供应渠道</th>
                                            <th class="flex-col">运输方式</th>
                                            <th class="flex-col">发货地点</th>
                                            <th class="flex-col">材质</th>
                                            <th class="flex-col">单重</th>
                                            <th class="flex-col">质保期</th>
                                            <th class="flex-col">报价备注</th>
                                            <c:forEach var="ext" items="${rfqQuotationItemExtra}">
                                                <th data-width="100" class="flex-col ext-col" data-extname="${ext.name}" data-extcode="${ext.code}" ext="${ext.code}">${ext.name}</th>
                                            </c:forEach>
                                        </tr>
                                        </thead>
                                        <tbody>
                                       <%-- <c:forEach var="itemVo" items="${rfqQuotationComplexVo.rfqQuotationItemHistoryList}" varStatus="status">
                                            <tr data-clone="orginal">
                                                <td data-order="true">${status.index+1}</td>
                                                <td>
                                                    代码：${itemVo.materialNo} 物料名称：${itemVo.materialName} 规格型号：${itemVo.character}
                                                    <a href="javascript:;" class="zkBtn icon icon-chevron-down"></a>
                                                    <div class="hidden">
                                                    描述：${itemVo.specification} 计量单位：${itemVo.unit} 生产厂家(品牌)：${itemVo.producer}
                                                    </div>
                                                </td>
                                                <td> ${itemVo.requestAmount} </td>
                                                <td>${itemVo.availableCount}</td>
                                                <td>${itemVo.unitPrice}</td>
                                                <td>${itemVo.tax}</td>
                                                <td>${itemVo.unitPriceTaxed}</td>
                                                <td>${itemVo.subtotal}</td>
                                                <td>${itemVo.subtotalTaxed}</td>
                                                <td>${itemVo.availableDeliveryDate}</td>
                                                <td>${itemVo.substitutableSpec}</td>
                                                <td>${itemVo.manufactory}</td>
                                                <td>${itemVo.productPlace}</td>
                                                <td>${itemVo.upstreamSuppliers}</td>
                                                <td>${itemVo.transType}</td>
                                                <td>${itemVo.deliveryLocation}</td>
                                                <td>${itemVo.material}</td>
                                                <td>${itemVo.singleWeight}</td>
                                                <td>${itemVo.timeOfWarranty}</td>
                                                <td>${itemVo.memo}</td>
                                            </tr>
                                        </c:forEach>--%>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="so-form-40 mt12">
                                    <%--供应商查看报价历史没有导出报价Excel按钮,去掉权限认证--%>
<%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}" quotationRound="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationRound}">--%>
                                    <button class="btn btn-primary btn-sm" type="button" onclick="exportExcel();">
                                        <i class="icon icon-upload-alt"></i>&nbsp;导出报价Excel
                                    </button>
<%--</p:permission>--%>
                                    <div class="pull-right" style="margin-top:-18px;" id="d_pager"></div>
                                </div>
                            </div>
                        </div>
                    <div class="panel mt24">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报价说明</span>
                        </div>

                        <div class="panel-body">
                            <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                <div class="clearfix">
                                    <label class="bler blu-8">报价有效期：</label>
                                    <span class="bler blu-2">${rfqQuotationComplexVo.rfqQuotationHisotryVo.quotationValidDate}</span>
                                </div>

                                <div class="clearfix">
                                    <label class="bler blu-8">返点说明：</label>
                                    <span class="bler blu-2">${rfqQuotationComplexVo.rfqQuotationHisotryVo.pointDescription}</span></div>
                                <div class="clearfix">
                                    <label class="bler blu-8">其它说明：</label>
                                    <span class="bler blu-2">${rfqQuotationComplexVo.rfqQuotationHisotryVo.quotationMemo}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">报价附件</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <table class="table table-bordered align-md fixed">
                                        <thead>
                                        <tr>
                                            <th width="20%">序号</th>
                                            <th width="20%">文件名称</th>
                                            <th width="20%">文件大小</th>
                                            <th width="20%">说明</th>
<%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><th width="20%">操作</th><%--</p:permission>--%>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="sum" value="1"/>
                                        <c:forEach var="item" items="${rfqQuotationComplexVo.attachmentsList}" varStatus="status">
                                            <c:if test="${item.type == 'Q'}">
                                            <tr data-clone="orginal">
                                                <td data-order="true">${sum}</td>
                                                <td>${item.originalFilename}</td>
                                                <td>${item.fileSize}KB</td>
                                                <td>${item.fileDeclaration}</td>
                                               <%-- <p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                            </tr>
                                                <c:set var="sum" value="${sum + 1}"/>
                                            </c:if>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="requestId" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}">
</div>

</body>
<script>
    var requestId = '${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}';
    var status = '${rfqQuotationComplexVo.status}';
    var supplierNum = "${rfqQuotationComplexVo.rfqQuotationHisotryVo.supplierNum}";
    var id = "${param['id']}"; //报价单id 标识区分采购组织查看 或者供应商报价
    /* 初始化数据表格 */
    $('table.datatable').datatable();


    function setHeight() {
        $('.datatable-rows .fixed-left tr').each(function () {
            var sda = $(this).index();
            var sheight = $(this).find('td').height();
            $('.datatable-rows .flexarea tr').eq(sda).find('td').height(sheight);
        })
    }
    setHeight();
    $('.zt-ico-w').cachet();

    function allZk(){
        $('.zk_span').toggleClass('hide');
        var index = $('.zk_span.hide').index();
        $('.zkBtn').each(function(){
            if(index == 0){
                $(this).next().removeClass('hidden');
            }else{
                $(this).next().addClass('hidden');
            }
        });
        setHeight();
        if(index == 0){
            $('.zkBtn').addClass('cur');
        }else{
            $('.zkBtn').removeClass('cur');
        }
    }




    $(function(){
        $('#datatable-requestItemData').on('click','.zkBtn',function(){
            $(this).next().toggleClass('hidden');
            setHeight();
            $(this).toggleClass('cur');
        });

        //物料分页加载
        var url = ctx+'/rfqQuotationItem/rfqQuotationItemList?requestId='+requestId+'&qtype='+status+'&supplierNum='+supplierNum;
        if(id){
            url = ctx+'/rfqQuotation/getQuotationHistoryDetail?id='+id+'&type=0';
        }
        var _mappingNames_ext=[];
        var display_div = '<div style="position: relative;">{value}</div>';//输入框
        $('#requestItemData').find('th.ext-col').each(function (i) {
            var $this = $(this);
            var extCode = $this.data('extcode');
            var valName = 'itemValue_'+(i+1);
            // extColsMapping[valName] = extCode;
            _mappingNames_ext.push(valName);
        });
        var _mappingNames = ['items','requestAmount','availableCount','unitPrice','tax','unitPriceTaxed','subtotal','subtotalTaxed','availableDeliveryDate','substitutableSpec','manufactory','productPlace','upstreamSuppliers','transType','deliveryLocation','material','singleWeight','timeOfWarranty','memo'];
        $.each(_mappingNames_ext,function(i,d){
            _mappingNames.push(d);
        });
        var options = {
            datatype: 'json',
            mtype:'POST',
            renderDel:false,
            url: url,
            mappingNames: _mappingNames,
            cellFormat: function (name,val,row,index) {
                row.purchaseNo = row.purchaseNo == undefined ? "" : row.purchaseNo;
                row.purchasePerson = row.purchasePerson == undefined ? "" : row.purchasePerson;
                row.drawingNo = row.drawingNo == undefined ? "" : row.drawingNo;
                if(name == 'items'){
                    var extendItem = "" ;
                    //有自定义物料时解析自定义物料
                    if(row.requestExtendItemData != undefined &&　row.requestExtendItemData　!= ''　){
                        //json串转换成数组
                        var requestExtendItemData = JSON.parse(row.requestExtendItemData);
                        //遍历数组
                        $.each(requestExtendItemData,function (i,d) {
                            var itemName = d.itemName;
                            var itemValue = d.itemValue;
                            extendItem += (" " + itemName + " : " + itemValue + "<br/>");//拼接自定义物料信息
                        });
                    }
                    if($("#dataSource").val()=='JFE'){
                        return ('<div style="position: relative;">代码：{materialNo} <br/> 物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/> 生产厂家(品牌)：{producer} <br/> 交货期：{requestDeliveryDate}<br/>申购单号：{purchaseNo}<br/>申购人：{purchasePerson}<br/>图号：{drawingNo}<br/> 备注：{requestMemo} <br/>'+extendItem+' </div></div>').format(row);
                    }else if($("#dataSource").val()=='NBBX'){
                        return ('<div style="position: relative;">代码：{materialNo} <br/> 物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/> 生产厂家(品牌)：{producer} <br/> 交货期：{requestDeliveryDate}<br/>材质/图号：{drawingNo}<br/> 备注：{requestMemo} <br/>'+extendItem+' </div></div>').format(row);
                    }else{
                        return ('<div style="position: relative;">代码：{materialNo} <br/> 物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/> 生产厂家(品牌)：{producer} <br/> 交货期：{requestDeliveryDate}<br/> 备注：{requestMemo} <br/>'+extendItem+' </div></div>').format(row);
                    }
                }
                if(name == 'tax'&& val !== ''){
                    val = val + '%';
                }
                if ("availableCount" == name || "subtotalTaxed" == name || "requestAmount" == name || "singleWeight" == name || "unitPriceTaxed" == name) {
                    val =  val === "" ? "" : val.toFixed(4);
                } else if ("unitPrice" == name || "subtotal" == name) {
                    val = val === "" ? "" : val.toFixed(2);
                }
                return val||'';
            }
        };
        RFQ.dataTable('#requestItemData').init(options).load();
    });



    //excel导出
    function exportExcel(){
        $.ajax({
            type: "POST",
            url: ctx+"/rfqQuotationItem/exportRecord",
            data: {
                requestId: requestId,
                qtype:status,
                supplierNum:supplierNum,
                quotationId:id
            },
            success: function(data) {
                if ("success"==data.status) {
                    location.href=data.message;
                } else {
                    RFQ.error('sorry,出错了！');
                }
            }
        });
    }
</script>
</html>

