<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>供应商报价物料列表</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/myquotation/myquotationItem.js?v=${version}"></script>
</head>

<body onload="rfqMethodType()">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
            	<div class="breadcrumbs">
                    <ol class="breadcrumb">
                                              <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                      <li class="active"><c:choose><c:when test="${rfqMethod=='PSCS'}">供应商报价物料列表</c:when><c:when test="${rfqMethod=='DAC'}">供应商竞价列表</c:when></c:choose></li>
                    </ol>
                </div>
                <input type="hidden" id="rfqMethod" value="${rfqMethod}"/>
                <div class="page-content">
                  	<div class="row">
			            <div class="col-md-12"> 
			              <div>   
			                <div class="tab-content">
                                <div class="tab-pane active  example" id="tab1">
                                    <div class="row mt12">
                                        <c:choose>
                                            <c:when test="${rfqMethod=='PSCS'}">
                                                <form  method="post" id="searchFormTab" class="searchFormTab" onsubmit="return false;">
                                                    <div class="so-form">
                                                        <div class="so-form-12 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="询价单号"></div>
                                                        <div class="so-form-15 padding1"><input type="text" name="title" class="form-control" placeholder="询价标题"></div>
                                                        <div class="so-form-15 padding1"><input type="text" name="ouName" class="form-control" placeholder="采购单位"></div>
                                                        <div class="so-form-15">
                                                            <button class="btn btn-primary btnSearch" type="button">
                                                                <i class="icon icon-search"></i> 搜索
                                                            </button>
                                                            <input class="btn" type="reset" value="重置">
                                                        </div>
                                                        <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                        <div class="clearfix"></div>
                                                        <div class="hiderow">
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="startDateStart" placeholder="报价起始时间(起)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="startDateEnd" placeholder="报价起始时间(止)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="quotationStartDate" placeholder="报价截止时间(起)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="quotationEndDate" placeholder="报价截止时间(止)" class="form-control form-datetime">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </c:when>
                                            <c:when test="${rfqMethod=='DAC'}">
                                                <form  method="post" id="searchFormTab" class="searchFormTab" onsubmit="return false;">
                                                    <div class="so-form">
                                                        <div class="so-form-12 padding1"><input type="text" name="unifiedRfqNum" class="form-control" placeholder="竞价单号"></div>
                                                        <div class="so-form-15 padding1"><input type="text" name="title" class="form-control" placeholder="竞价标题"></div>
                                                        <div class="so-form-15 padding1"><input type="text" name="ouName" class="form-control" placeholder="采购单位"></div>
                                                        <div class="so-form-15">
                                                            <button class="btn btn-primary btnSearch" type="button">
                                                                <i class="icon icon-search"></i> 搜索
                                                            </button>
                                                            <input class="btn" type="reset" value="重置">
                                                        </div>
                                                        <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                        <div class="clearfix"></div>
                                                        <div class="hiderow">
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="startDateStart" placeholder="竞价起始时间(起)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="startDateEnd" placeholder="竞价起始时间(止)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="quotationStartDate" placeholder="竞价截止时间(起)" class="form-control form-datetime">
                                                            </div>
                                                            <div class="so-form-15 mr30">
                                                                <input type="text" name="quotationEndDate" placeholder="竞价截止时间(止)" class="form-control form-datetime">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </c:when>
                                        </c:choose>
                                    </div>
                                </div>
			                </div>
                              <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                              <div class="col-md-12" id="jqGridPager"></div>
                          </div>
			            </div>
			          </div>		
                    <div class="text-right"> 
                        <a href="${ctx}/rfqQuotation/init" class="btn btn-lg btn-primary" type="button">返回</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


$(function(){

    $('.more-so').click(function(){
        $('.hiderow').stop().slideToggle('fast');
    });
	
	 //日期
  $(".form-datetime").datetimepicker(
  {
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      showMeridian: 1,
      format: "yyyy-mm-dd hh:ii"
  });
  
  $(".form-date").datetimepicker(
  {
      language:  "zh-CN",
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
      format: "yyyy-mm-dd"
  });
  
  $(".form-time").datetimepicker({
      language:  "zh-CN",
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 1,
      minView: 0,
      maxView: 1,
      forceParse: 0,
      format: 'hh:ii'
  });	
	
});


</script>
</body>
</html>
