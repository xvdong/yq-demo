<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>报价单</title>
    <%@include file="../common/headBase.jsp" %>
    <%@include file="../common/jqGridBootstrap.jsp" %>
    <!--富文本JS-->
    <c:import url="../common/kingEditor.jsp"/>
    <script src="${resource}/js/rfq.datatable.edit.js?v=${version}"></script>
    <script src="${resource}/js/myquotation/rfqQuotationEdit4item2.js?v=${version}"></script>
    <script src="${resource}/js/myquotation/rfqQuotationEdit.js?v=${version}"></script>
    <script src="${resource}/js/myquotation/calculateDetail.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/lib/plupload-2.1.9/js/plupload.full.min.js" type="text/javascript"></script>
    <script src="${resource}/js/upload/uploadFile.js?v=${version}" type="text/javascript" ></script>
    <style>
        .flex-col {

            text-align: center;
            vertical-align: middle;
        }

        .flexarea td {

            text-align: center;
            vertical-align: middle;
        }

        .fixed-left td,
        th {
            text-align: center;
            vertical-align: middle;
        }

        .fixed-left tr td:nth-of-type(2) {
            text-align: left;
            padding-bottom: 20px;
        }

        .table > thead > tr > th {
            vertical-align: middle
        }

        .flexarea .dropdown-menu {
            width: 300px;
            top: -27px;
            left: 70px;
        }

        .flexarea .dropdown-menu li {
            width: 25%;
            float: left;
        }
        .hide{display: none;}

        .dropdown-menunew{position: absolute; top: 0; right: -300px; width: 300px; z-index: 1000; min-width: 160px; padding: 5px 0; margin: 2px 0 0; font-size: 13px; list-style: none; background-color: #fff; background-clip: padding-box; border: 1px solid #cbcbcb; border: 1px solid rgba(0,0,0,.15); border-radius: 4px; box-shadow: 0 6px 12px rgba(0,0,0,.175);}
        .dropdown-menunew li{list-style: none;  width: 74px; float: left;}
        .dropdown-menunew li a{display: block; padding: 3px 20px;  font-weight: 400; line-height: 1.53846154; color: #353535; white-space: nowrap;}
        .dropdown-menunew li a:hover{ background-color: #3280fc;}
        .js-select{
            background-color: #ffffff !important;
        }
    </style>
</head>

<body id="s-body">

<c:import url="../common/top.jsp"/>
<input id="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
<input id="supplierCode" type="hidden" value="${rfqQuotationComplexVo.supplierCode}">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp"/>
        <div class="rightbar clearfix">
            <form id="formId" method="POST" action="${ctx}/rfqQuotation/saveRfqQuotation">
                <input name="rfqQuotationBO.requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                <input name="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                <div class="container" style="padding-bottom:20px;">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                                                    <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                            <li class="active">报价单</li>
                        </ol>
                    </div>
                    <div class="page-content">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价单摘要</span>
                                <div class=" pull-right">
                                    <div class=" pull-right">
                                        <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${rfqQuotationBO.requestId}" target="_blank"><span class="btn btn-primary font12">询单详情</span></a>
                                        <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.needAssureMoney==1 and rfqQuotationComplexVo.rfqSupplierListVo.assureFlag==0}">
                                            <a href="#"><span class="btn btn-info font12"  data-toggle="modal"
                                                              data-target="#cancelMoney">缴纳保证金</span></a>
                                        </c:if>
                                        <span class="btn btn-danger font12" data-toggle="modal"
                                              data-target="#cancelPrice">放弃报价</span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body font12">
                                <div class="row">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>
                                                询价单号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}</li>
                                            <li>询价标题：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.title}</li>
                                            <li>采购单位：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouName}</li>
                                            <li>计划编号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.planNo}</li>
                                            <li>当前状态：
                                                <span class="red">
                                                       <c:if test="${rfqQuotationComplexVo.status=='1'}">
                                                           待报价
                                                       </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='2'}">
                                                         已报价
                                                     </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='3'}">
                                                         结果待发布
                                                     </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='4'}">
                                                         已放弃
                                                     </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='5'}">
                                                         已中标
                                                     </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='6'}">
                                                         未中标
                                                     </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='7'}">
                                                         已作废
                                                     </c:if>
                                                     <c:if test="${rfqQuotationComplexVo.status=='8'}">
                                                         已过期
                                                     </c:if>
                                                </span>
                                            </li>
                                            <input id="currentStatus" hidden="hidden" value="${rfqQuotationComplexVo.status}"/>
                                            <%--   <li>邀请范围：
                                                   <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                                       公开询价（面向平台注册供应商公开寻源，供应商需先报名，待采购组织审核通过后再报价。）
                                                   </c:if>
                                                   <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.publicBiddingFlag=='0'}">
                                                       定向询价 (将询价单信息向特邀供应商公布，供应商无须报名可直接报价。)
                                                   </c:if>
                                               </li>--%>
                                        </ul>
                                        <span class="zt-ico-w">
                                            <c:if test="${rfqQuotationComplexVo.status=='1'}">
                                                <img src="${ctx}/images/dbj.png">
                                            </c:if>
                                            <c:if test="${rfqQuotationComplexVo.status=='2'}">
                                                <img src="${ctx}/images/ybj.png">
                                            </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='3'}">
                                                 <img src="${ctx}/images/jgdfb_03.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='4'}">
                                                 <img src="${ctx}/images/yfq.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='5'}">
                                                 <img src="${ctx}/images/yzb.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='6'}">
                                                 <img src="${ctx}/images/wzb.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='7'}">
                                                 <img src="${ctx}/images/yzf.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='8'}">
                                                 <img src="${ctx}/images/ygq.png">
                                             </c:if>
                                            </span>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>
                                                保证金金额：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.assureMoney}元
                                            </li>
                                            <li>报价货币：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.currency}元
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>
                                                发布时间：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.issueDate}</li>
                                            <li>
                                                报价开始时间：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.quotationStartDate}</li>
                                            <li>
                                                报价截止时间：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                            <li>
                                                报名截止时间：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>
                                                联系人：${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                            <li>
                                                联系电话：${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                            <li>收货地址：
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryProvince}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryCity}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryArea}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryAddress}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价单条款协议</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                        <div class="clearfix">
                                            <label class="bler blu-1">商务条款：</label><span
                                                class="bler blu-88-nobg">${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.requestBusiTerms}</span>
                                        </div>
                                        <div class="clearfix">
                                            <label class="bler blu-1">技术条款：</label><span
                                                class="bler blu-88-nobg">${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.requestTecTerms}</span>
                                        </div>
                                        <div class="mt12 clearfix">
                                            <span class="col-xs-12">附件清单：</span>
                                            <table class="table table-bordered mt12 align-md fixed" style="width:97%;">
                                                <thead>
                                                <tr>
                                                    <th width="50px">序号</th>
                                                    <th>文件名称</th>
                                                    <th width="100px">文件大小</th>
                                                    <th>说明</th>
                                                    <th width="100px">操作</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:set var="sum" value="1"/>
                                                <c:forEach
                                                        items="${rfqQuotationComplexVo.rfqRequestComplexVo.attachmentsList}"
                                                        var="attachment" varStatus="status">
                                                    <c:if test="${attachment.type == 'R'}">
                                                        <tr data-clone="orginal">
                                                            <td>${sum}</td>
                                                            <td>${attachment.originalFilename}</td>
                                                            <td>${attachment.fileSize}KB</td>
                                                            <td>${attachment.fileDeclaration}</td>
                                                            <td><a href="${downloadAction}?id=${attachment.attachmentId}"><i
                                                                    class="icon icon-download-alt green "></i>&nbsp;下载</a>
                                                            </td>
                                                        </tr>
                                                        <c:set var="sum" value="${sum + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel mt20">
                            <div class="panel-heading clearfix" id="searchForm2">
                                <span class="mg-header5 pull-left">报价物料</span>
                                <span class="pull-left ml12 c999">关键字搜索</span>
                                <span class="pull-left ml12"><input class="form-control input-sm" type="text" name="keyword"
                                                                    id="soText"></span>&nbsp;
                                <span class="label mg-btn label-primary" id="searchKey">查询</span>
                                <span class="pull-left soInfo ml12"><i
                                        class="icon icon-exclamation-sign"></i> 无搜索结果</span>
                                <span class="pull-right">以下报价仅对本次询价有效，含税总价<span class="red" id="totalPrice"><c:out
                                        value="${rfqQuotationComplexVo.rfqQuotationVo.subtotalTaxed}"
                                        default="0.00"/></span>元</span>
                            </div>
                            <div class="panel-body s-panel-body" id="panelBody">
                                <div class="row">
                                    <table class="table datatable table-bordered align-md" id="requestItemData">
                                        <thead>
                                        <tr>
                                            <!-- 以下两列左侧固定 -->
                                            <th data-width="80">序号</th>
                                            <th>物料信息
                                                <a class="btn btn-danger btn-mini" id="all-zk" href="javascript:allZk();">
                                                    <span class="zk_span">全部展开</span>
                                                    <span class="zk_span hide">全部隐藏</span>
                                                </a>
                                            </th>
                                            <th data-width="80">采购数量</th>
                                            <!-- 以下可滚动 -->
                                            <th class="flex-col" data-width="80">状态</th>
                                            <th class="flex-col" data-width="80"><i class="r">*</i>可供量</th>
                                            <th class="flex-col" data-width="120"><i class="r">*</i>未税单价(元)</th>
                                            <th class="flex-col" data-width="120">
                                                <div><i class="r">*</i>税率</div>
                                                <div class="btn-group">
                                                    <button class="btn btn-hui dropdown-toggle" type="button"
                                                            id="dropdownMenu1" data-toggle="dropdown">
                                                        6%<span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" id="tax" name="tax" role="menu"
                                                        aria-labelledby="dropdownMenu1">
                                                        <li><a data-value="6">6%</a></li>
                                                        <li><a data-value="7">7%</a></li>
                                                        <li><a data-value="10">10%</a></li>
                                                        <li><a data-value="11">11%</a></li>
                                                        <li><a data-value="13">13%</a></li>
                                                        <li><a data-value="17">17%</a></li>
                                                    </ul>
                                                </div>


                                            </th>
                                            <th class="flex-col" data-width="100">含税单价</th>
                                            <th class="flex-col" data-width="100">未税总价</th>
                                            <th class="flex-col" data-width="100">含税总价</th>
                                            <th class="flex-col" data-width="120"><span><i class="r">*</i>承诺交货期</span>
                                                <div class="input-group date form-date availableDeliveryDate" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                    <input id="availableDeliveryDate2"  class="form-control availableDeliveryDate" size="16" type="text" style="display: none;">
                                                    <span   class="input-group-addon"><span class="icon-calendar"></span></span>
                                                </div>
                                            </th>
                                            <th class="flex-col" data-width="100">替代型规</th>
                                            <th class="flex-col" data-width="100"><i class="r"></i>制造商</th>
                                            <th class="flex-col" data-width="100"><i class="r"></i>产地</th>
                                            <th class="flex-col" data-width="120"><i class="r"></i>上一级供应渠道</th>
                                            <th class="flex-col" data-width="100">运输方式</th>
                                            <th class="flex-col" data-width="100">发货地点</th>
                                            <th class="flex-col" data-width="100">材质</th>
                                            <th class="flex-col" data-width="100">单重</th>
                                            <th class="flex-col" data-width="120">质保期</th>
                                            <th class="flex-col" data-width="100">报价备注</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%--<c:forEach items="${rfqQuotationComplexVo.rfqQuotationItemVo}" var="itemVo"
                                                   varStatus="status">
                                            <tr>
                                                <td>${status.index+1}</td>
                                                <td>
                                                    代码：${itemVo.materialNo} 物料名称：${itemVo.materialName}
                                                    规格型号：${itemVo.character}
                                                    <a href="javascript:;" class="zkBtn icon icon-chevron-down"></a>
                                                    <div class="hidden">
                                                        描述：${itemVo.specification} 计量单位：${itemVo.unit}
                                                        生产厂家(品牌)：${itemVo.producer}
                                                    </div>
                                                </td>
                                                <td>${itemVo.requestAmount}
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].requestAmount"
                                                           value="${itemVo.requestAmount}" type="hidden" placeholder="">
                                                </td>
                                                <td>${empty itemVo.requestItemId?'未报价':'已报价'}
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].requestId"
                                                           value="${rfqQuotationBO.requestId}" type="hidden"
                                                           placeholder="">
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].requestItemId"
                                                           value="${itemVo.requestItemIdOrg}" type="hidden"
                                                           placeholder="">
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].seq"
                                                           value="${status.index+1}" type="hidden" placeholder="">
                                                </td>

                                                <td>
                                                    <input class="form-control js-change" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
                                                           data-name="rfqQuotationItemList[${status.index}].availableCount"
                                                           value="${itemVo.availableCount}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control js-change" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"
                                                           data-name="rfqQuotationItemList[${status.index}].unitPrice"
                                                           value="${itemVo.unitPrice}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control js-change" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"
                                                           data-name="rfqQuotationItemList[${status.index}].tax"
                                                           value="${itemVo.tax}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].unitPriceTaxed"
                                                           value="${itemVo.unitPriceTaxed}" readonly="readonly"
                                                           type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].subtotal"
                                                           value="${itemVo.subtotal}" type="text" readonly="readonly"
                                                           placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].subtotalTaxed"
                                                           value="${itemVo.subtotalTaxed}" type="text"
                                                           readonly="readonly" placeholder=""
                                                           onkeyup="countTotalPrice()">
                                                </td>
                                                <td>
                                                    <input class="form-control form-datetime"
                                                           data-name="rfqQuotationItemList[${status.index}].availableDeliveryDate"
                                                           value="${itemVo.availableDeliveryDate}" type="text"
                                                           placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].substitutableSpec"
                                                           value="${itemVo.substitutableSpec}" type="text"
                                                           placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].manufactory"
                                                           value="${itemVo.manufactory}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].productPlace"
                                                           value="${itemVo.productPlace}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].upstreamSuppliers"
                                                           value="${itemVo.upstreamSuppliers}" type="text"
                                                           placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].transType"
                                                           value="${itemVo.transType}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].deliveryLocation"
                                                           value="${itemVo.deliveryLocation}" type="text"
                                                           placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].material"
                                                           value="${itemVo.material}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].singleWeight"
                                                           value="${itemVo.singleWeight}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control form-datetime"
                                                           data-name="rfqQuotationItemList[${status.index}].timeOfWarranty"
                                                           value="${itemVo.timeOfWarranty}" type="text" placeholder="">
                                                </td>
                                                <td>
                                                    <input class="form-control"
                                                           data-name="rfqQuotationItemList[${status.index}].memo"
                                                           value="${itemVo.memo}" type="text" placeholder="">
                                                </td>
                                            </tr>
                                        </c:forEach>--%>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12 clearfix">
                                    <div class="ml12 left">
                                        <span class="blue" style="cursor:pointer;" data-position="center" onClick="excelClicke()"
                                              data-toggle="modal" data-target="#cancelPrice33">
                                            <i class="icon icon-download-alt"></i>&nbsp;&nbsp;Excel模板报价
                                        </span>
                                    </div>
                                    <div class="pull-right" style="margin-top:-18px;" id="d_pager"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel mt24">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">报价说明</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="so-form">
                                        <div class="so-form-9 ml12"><i class="r">*</i>报价有效期：</div>
                                        <div class="so-form-20 padding1" style=" margin-right:40px;">
                                            <input name="rfqQuotationBO.quotationValidDate" id="date" placeholder="截止日期"
                                                   value="${rfqQuotationComplexVo.rfqQuotationVo.quotationValidDate}"
                                                   class="form-control form-date" type="text">
                                            <input name="rfqQuotationBO.subtotalTaxed" id="subtotalTaxed" placeholder=""
                                                   value="<c:out value="${rfqQuotationComplexVo.rfqQuotationVo.subtotalTaxed}" default="0.00"/>"
                                                   class="form-control" type="hidden">
                                            <input name="rfqQuotationBO.subtotal" id="subtotal" placeholder=""
                                                   value="<c:out value="${rfqQuotationComplexVo.rfqQuotationVo.subtotal}" default="0.00"/>"
                                                   class="form-control" type="hidden">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt12">
                                    <div class="so-form">
                                        <div class="so-form-8 ml12">返点说明：</div>
                                        <div class="so-form-90 padding1">
                                            <textarea id="contentSimple" name="rfqQuotationBO.pointDescription"
                                                      class="form-control kindeditorSimple col-md-10"
                                                      style=" width:100%;height:150px;">${rfqQuotationComplexVo.rfqQuotationVo.pointDescription}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt12">
                                    <div class="so-form">
                                        <div class="so-form-8 ml12">其它说明：</div>
                                        <div class="so-form-90 padding1">
                                            <textarea id="contentSimple2" name="rfqQuotationBO.quotationMemo"
                                                      class="form-control kindeditorSimple col-md-10"
                                                      style=" width:100%;height:150px;">${rfqQuotationComplexVo.rfqQuotationVo.quotationMemo}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <span class="mg-header5 pull-left">报价附件</span>
                                <span class="pull-right">
                                 <a class="blue attachmentBtn" href="#" data-toggle="modal" data-target="#myModa2"
                                    data-type="Q">
                                     <label class="btn btn-primary btn-sm"><i
                                             class="icon icon-plus"></i>&nbsp;新增附件</label>
                                 </a>
                            </span>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <table class="table table-bordered align-md fixed">
                                        <thead>
                                        <tr>
                                            <th width="20%">序号</th>
                                            <th width="20%">文件名称</th>
                                            <th width="20%">文件大小</th>
                                            <th width="20%">说明</th>
                                            <th width="20%">操作</th>
                                        </tr>
                                        </thead>
                                        <tbody id="attachmentList1">
                                        <c:set var="sum" value="1"/>
                                        <c:forEach var="item" items="${rfqQuotationComplexVo.attachmentsList}"
                                                   varStatus="status">
                                            <c:if test="${item.type == 'Q'}">
                                                <tr data-clone="orginal">
                                                    <td data-name="downloadUrl" data-value="${item.downloadUrl}"
                                                        data-order="true"> ${sum}
                                                        <input type="hidden" data-name="originalFileType"
                                                               data-value="${item.originalFileType}"/>
                                                        <input type="hidden" data-name="originalFilename"
                                                               data-value="${item.originalFilename}"/>
                                                        <input type="hidden" data-name="type" data-value="Q"/>
                                                    </td>
                                                    <td data-name="originalFilename"
                                                        data-value="${item.originalFilename}">${item.originalFilename}</td>
                                                    <td data-name="fileSize"
                                                        data-value="${item.fileSize}">${item.fileSize}KB
                                                    </td>
                                                    <td data-name="fileDeclaration"
                                                        data-value="${item.originalFilename}">${item.fileDeclaration}</td>
                                                    <td data-name="downloadFilename"
                                                        data-value="${item.downloadFilename}">
                                                        <a href="javascript:;" class="text-danger order-del-btn"
                                                           data-key="${item.downloadFilename}_P"><i
                                                                class="icon-trash"></i> 删除</a>
                                                    </td>
                                                </tr>
                                                <c:set var="sum" value="${sum + 1}"/>
                                            </c:if>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="text-right" style="padding:0 10px 30px 10px;">
                    <a class="left blue" href="#" data-toggle="modal" data-target="#myModa3" id="myHistory">我的报价历史</a>
                    <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="2" type="button">保存报价</a>
                    <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1"
                       type="button" <%--data-toggle="modal"--%> <%--data-target="#myModa6"--%>>提交报价</a>
                    <a href="${ctx}/rfqQuotation/init" class="btn btn-lg btn-primary" type="button">取消</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">放弃寻源单</h4>
            </div>
            <form id="giveUpQuo">
                <div class="modal-body">
                    <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px;">
                        <i class="icon icon-question-sign icon-la"></i>&nbsp;&nbsp;您确定要放弃该寻源单吗？
                    </div>
                    <div class="mg-row">
                        <span class="left-title">寻源类型：</span>
                        <span class="left-cont1">
                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod == 'RAQ'}">询价 </c:if>
                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod == 'RFQ'}">招标 </c:if>
                        </span>
                        <div>
                            <span class="left-title">寻源单号：</span>
                            <span class="left-cont1">
                                ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}
                            </span>
                        </div>
                    </div>
                    <div class="mg-row clearfix" style="margin-bottom:8px;">
                        <span class="left-title">寻源标题：</span>
                        <span class="left-cont1">
                            ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.title}
                        </span>
                        <div>
                            <span class="left-title">采购商：</span>
                            <span class="left-cont1">
                                ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouName}
                            </span>
                        </div>
                    </div>
                    <input name="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                    <div class="mg-row">
                        <span class="left-title"><i class="r">*</i>放弃原因：</span>
                        <div class="right-paner">
                            <select name="giveUpReason" class="form-control">
                                <option value="品类无法供应">品类无法供应</option>
                                <option value="供应时间无法满足">供应时间无法满足</option>
                                <option value="太忙，无暇顾及">太忙，无暇顾及</option>
                                <option value="不想报了">不想报了</option>
                                <option value="EUR">其他原因，请输入</option>
                            </select>
                        </div>
                    </div>
                    <div class="mg-row" style="margin-bottom:0px;">
                        <span class="left-title"><i class="r">*</i>放弃说明：</span>
                        <div class="right-paner">
                            <textarea class="form-control" name="giveUpMemo" rows="6" placeholder="" maxlength="2000"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary give-up">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="cancelMoney">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">缴纳保证金</h4>
            </div>
            <form id="payCashForm" action="${pageContext.request.contextPath}/rfqSupplierList/updateRfqSupplierList" method="post">
                <div class="modal-body">
                    <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px;">
                        <i class="icon icon-question-sign icon-la"></i>&nbsp;&nbsp;确认缴纳保证金？
                    </div>

                    <input name="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                    <input name="supplierCode" type="hidden" value="${rfqQuotationComplexVo.supplierCode}">
                    <div class="mg-row" style="margin-bottom:0px;">
                        <span class="left-title">保证金额：</span>
                        <div class="right-paner">
                            <input type="text" readonly="readonly" value="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.assureMoney}" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary give-up" id="payCash" data-dismiss="modal">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="cancelPrice33">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">excel导入</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row">
                    <span class="left-cont2">1、第一步请先下载excel模版。</span>
                    <div>
                        <span class="left-cont1 ml12"><a href="javascript:exportExcel();" class="blue">点此下载excel模版</a></span>
                    </div>
                </div>
                <div class="mg-row clearfix" style="margin-bottom:8px;">
                    <span class="left-cont2">2、第二步请上传根据模版编辑好的文件。</span>
                    <form action="${resource}/rfqQuotationItemTemp/importRecord" method="post" class="form-horizontal" id="excelUpload" onsubmit="return false;">
                        <input name="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                        <input type="file" name="file" id="file">
                    </form>

                    </span>
                </div>
                <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px; height:66px;">
                    <i class="icon icon-warning-sign sign-warn1"></i><span class="sign-1 ml12">
                        注意：Excel批量导入将覆盖询单内现有物料；
                    <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上传附件文件类型仅限Excel文件。</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="saveExcel()" class="btn btn-primary">确定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!-- ///////////////////////新增附件2/////////////// -->
<div class="modal fade" id="myModa2" data-type="Q">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                          <form>
                              <input type="text"  id="fileName" placeholder="请上传,附件大小≤100M"  class="form-control no-border" />
                              <input type="button" id="upload" value="上传">
                              <span  id="msg">0%</span>
                          </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration" class="form-control" maxlength="1000"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary js-confirm" data-dismiss="modal" id="sure" disabled="disabled">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- id2结束 -->
<div class="modal fade" id="myModa3">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的报价历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                    <div class="col-md-12" id="jqGridPager"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer" data-dismiss="modal"><a type="button" class="btn btn-primary" href="javascript:;">确定</a></div>
    </div>
</div>
<!-- 非标报弹窗 -->
<div class="modal fade" id="myModafei">
    <div class="modal-dialog modal-fullscreen"  style="overflow:auto;">
        <div class="modal-content" style=" width:2280px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="position:fixed; top:15px; right:15px;">
                    <span aria-hidden="true">×</span><span class="sr-only">关闭</span>
                </button>
                <h4 class="modal-title">国内非标备件详细报价页面</h4>
            </div>
            <form id="detailForm" >
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveDetail()">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<script type="text/javascript">

    /* 初始化数据表格 */
    $('table.datatable').datatable();
    //END

    var editor;
    var editor2;
    KindEditor.ready(function (K) {
        editor = K.create('textarea#contentSimple', {
            resizeType: 1, allowPreviewEmoticons: false, allowImageUpload: false, items: [], afterBlur: function () {
                this.sync();
            }
        });
        editor2 = K.create('textarea#contentSimple2', {
            resizeType: 1, allowPreviewEmoticons: false, allowImageUpload: false, items: [], afterBlur: function () {
                this.sync();
            }
        });
    });

    var $datatable = $('#datatable-requestItemData');

    function setHeight() {
        $('.datatable-rows .fixed-left tr').each(function () {
            var sda = $(this).index();
            var sheight = $(this).find('td').height();
            $('.datatable-rows .flexarea tr').eq(sda).find('td').height(sheight);
        })
    }
    setHeight();
    $('.zt-ico-w').cachet();

    function allZk(){
        $('.zk_span').toggleClass('hide');
        var index = $('.zk_span.hide').index();
        $('.zkBtn').each(function(){
            if(index == 0){
                $(this).next().removeClass('hidden');
            }else{
                $(this).next().addClass('hidden');
            }
        });
        setHeight();
        if(index == 0){
            $('.zkBtn').addClass('cur');
        }else{
            $('.zkBtn').removeClass('cur');
        }
    }

    $(function(){
        $datatable.on('click','.zkBtn',function(){
            $(this).next().toggleClass('hidden');
            setHeight();
            $(this).toggleClass('cur');
        });
    });

    var attachmentList = {}; //附件

    //附件上传
    //BEGIN
    (function () {
        var $myModa2 = $('#myModa2');
        var $attachmentList1 = $('#attachmentList1');
        var rowTmp = '<tr data-clone="orginal"> <td data-order="true">1</td> <td>{originalFilename}</td> <td>{fileSize}KB</td> <td>{fileDeclaration}</td> <td><a href="javascript:;" data-key="{downloadFilename}_{type}" class="text-danger order-del-btn"><i class="icon-trash"></i> 删除</a></td> </tr>';

        /*var uploadComponent = new UploadComponent({
         divId: 'container_doc',
         code: 'AAAA',
         documentId: '1111',
         containerId: 'container_doc_upload_form',
         fileType: 'txt,doc,docx,xls,xlsx,pdf,png,jpg,jpeg,gif,bmp',
         inputDiv: 'fileup',
         styleType: "normal",
         uploadUrl: ctx + "/common/uploadFile",
         success: function (data) {
         if(data.status == 'success'){
         var result = JSON.parse(data.attachmentsVo);
         $("#fileName").val(result.originalFilename);
         RFQ.d(result);
         $myModa2.data('_result', result);
         $myModa2.find('.js-confirm').attr('disabled',false);
         }else{
         $.zui.messager.show(data.message, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
         }
         }
         });*/

        /*$('a.attachmentBtn').on('click', function () {
         /!*uploadComponent.reset();*!/
         var fileEl = $myModa2.find(':file')[0];
         RFQ.clearFileInput(fileEl);
         $myModa2.find('.percent').html('0%');
         $myModa2.find('.js-confirm').attr('disabled',true);
         $myModa2.find('textarea').val('');
         $myModa2.data('_type', $(this).data('type'));
         $myModa2.data('_result','');
         });*/

        //当前存在的数据加入缓存
        $('#attachmentList1 tr').each(function () {
            var tmp = {};
            $(this).find('[data-name]').each(function () {
                var $this = $(this);
                var name = $this.data('name');
                if (!name) return true;
                tmp[name] = $this.data('value');
            });
            attachmentList[tmp.downloadFilename + '_' + tmp.type] = tmp;
        });

        $myModa2.on('click', '.js-confirm', function () {
            var type = $myModa2.data('type');
            var data = $myModa2.data('_result');
            /*var fileEl = $myModa2.find(':file');
             if(!fileEl.value){
             RFQ.warn('未选择上传文件');
             return false;
             }*/
            if(data==undefined || data==''){
                showErrorMsg("您还没选择文件！");
            }
            if(!data.fileSize){
                RFQ.warn('文件未上传完成，请重新上传');
                return false;
            }
            data.type = type;
            data.fileDeclaration = $myModa2.find('textarea').val();
            var $container = $attachmentList1;
            attachmentList[data.downloadFilename + '_' + data.type] = data;
            $container.append(rowTmp.format(data));
            $myModa2.modal('hide');
            resetSeq($container);
        });

        //删除
        $attachmentList1.on('click', '.order-del-btn', deleteHandle);

        function deleteHandle() {
            var $this = $(this);
            var key = $(this).data("key");
            $this.parent().parent("tr").remove();
            delete attachmentList[key];
            resetSeq($this.closest('tbody'));
            RFQ.d(attachmentList);
        }

        //重新设置序号
        function resetSeq($tbody) {
            var $tds = $tbody.find('tr td:first-child');
            $tds.each(function (i, el) {
                $(this).html(i + 1);
            });
        }
    })();
    //END


    //表单提交
    (function () {
        //表单操作按钮
        $('#formId').on('click', '.js-form-btn', function () {
            var $this = $(this);
            var type = $this.data('type');
            if (type == 1||type == 2) {
                if(!validate2000(editor)){
                    RFQ.error("返点说明最多输入2000字符！");
                    return;
                }
                if(!validate2000(editor2)){
                    RFQ.error("其他说明最多输入2000字符！");
                    return;
                }

                var rfqDTable = $datatable.data('_rfqDTable');
                if(rfqDTable.options.beforePagination.call(rfqDTable)) { //临时数据保存成功
                    rfqDTable.load();
                    var extras = fecthExtraParams();
                    //  var url = $this.attr('action');
                    RFQ.form('#formId').ajaxSubmit(ctx + '/rfqQuotation/saveRfqQuotation?type='+type, extras, function () {
                        if (type == 1) {
                            RFQ.info('提交成功');
                            location.href = ctx + '/rfqQuotation/init';
                        }
                        if(type == 2){ //保存成功
                            RFQ.info('保存成功!');
                            window.location.reload();
                        }
                    });
                }
            }else { //删除
                RFQ.info('删除！');
            }
        });

        var $myModa6 = $('#myModa6');
        //发布摸态框按钮事件绑定
        $myModa6.on('click', '.modal-footer a', function () {
            var type = $(this).data('type');
            var id = $myModa6.data('id');
            if (type == 1) { //确认提交
//                location.href='init';
            }
        });

        //抓取额外参数
        function fecthExtraParams() {
            //组装额外参数 选取的供应商以及角色
            var extras = {};
            //富文本e
//            extras['rfqQuotationBO.pointDescription'] = editor.html();

            //组装物料行信息
            var rfqDTable = $datatable.data('_rfqDTable');

            RFQ.d(rfqDTable);
            $.extend(extras, rfqDTable.fetchChanges('rfqQuotationItemList'));

//            $('#datatable-requestItemData').find('.table-datatable:visible input').each(function () {
//                extras[$(this).data('name')] = this.value;
//            });

            //组装附件信息
            if (!$.isEmptyObject(attachmentList)) {
                var index = 0, tmp;
                for (var key in attachmentList) {
                    tmp = attachmentList[key];
                    extras['attachmentsList[' + index + '].seq'] = index + 1;
                    extras['attachmentsList[' + index + '].type'] = tmp.type;
                    extras['attachmentsList[' + index + '].downloadFilename'] = tmp.downloadFilename;
                    extras['attachmentsList[' + index + '].fileSize'] = tmp.fileSize;
                    extras['attachmentsList[' + index + '].originalFileType'] = tmp.originalFileType;
                    extras['attachmentsList[' + index + '].originalFilename'] = tmp.originalFilename;
                    extras['attachmentsList[' + index + '].downloadUrl'] = tmp.downloadUrl;
                    extras['attachmentsList[' + index + '].fileDeclaration'] = tmp.fileDeclaration;
                    index++;
                }
            }
            if(!$.isEmptyObject(itemDetailList)){
                var index = 0,tmp;
                extras['itemDetailList[' + index + '].requestItemId'] = rfqRequestItemId;
                for(var key in itemDetailList){
                    tmp = itemDetailList[key];
                    extras['itemDetailList[' + index + '].managementFeeRate'] = tmp['managementFeeRate'];
                    extras['itemDetailList[' + index + '].profitsRate'] = tmp['profitsRate'];
                    extras['itemDetailList[' + index + '].packingFeeRate'] = tmp['packingFeeRate']   ;
                    extras['itemDetailList[' + index + '].transportFeeRate'] = tmp['transportFeeRate'];
                    extras['itemDetailList[' + index + '].compositionFeeRate'] = tmp['compositionFeeRate'];
                    extras['itemDetailList[' + index + '].designFeeRate'] = tmp['designFeeRate'];
                    extras['itemDetailList[' + index + '].kitFee'] = tmp['kitFee'];
                    extras['itemDetailList[' + index + '].singleKitTotalPrice'] = tmp['singleKitTotalPrice'];
                    extras['itemDetailList[' + index + '].totalPrice'] = tmp['totalPrice'];
                    for(var i=1;i<12;i++) {
                        extras['itemDetailList[' + index + '].netWeight'+i] = tmp['netWeight'+i];
                        extras['itemDetailList[' + index + '].grossWeight'+i] =  tmp['grossWeight'+i];
                        extras['itemDetailList[' + index + '].materialUnitPrice'+i] = tmp['materialUnitPrice'+i];
                        extras['itemDetailList[' + index + '].materialFee'+i] = tmp['materialFee'+i];
                        extras['itemDetailList[' + index + '].processTotalFee'+i] = tmp['processTotalFee'+i];
                        extras['itemDetailList[' + index + '].unitTotalPrice'+i] = tmp['unitTotalPrice'+i];
                        for(var j=1;j<12;j++) {
                            if (i == 11 && j == 1) {
                                extras['itemDetailList[' + index + '].timeOfSingleton11_1'] = tmp['timeOfSingleton11_1'];
                                extras['itemDetailList[' + index + '].unitPriceOfSingleton11_1'] = tmp['unitPriceOfSingleton11_1'];
                            } else {
                                extras['itemDetailList[' + index + '].timeOfSingleton' + i + j] = tmp['timeOfSingleton'+i+j];
                                extras['itemDetailList[' + index + '].unitPriceOfSingleton' + i + j] = tmp['unitPriceOfSingleton'+i+j];
                            }
                        }

                    }
                    index++;
                }
            }
            return extras;
        }



        $('body').append('<div class="dangzhu1"></div>');
        $('body').append('<div class="dangzhu2"></div>');
        $('.s-panel-body').append('<div class="dangzhu3"></div>');
        $('.s-panel-body').append('<div class="dangzhu4"></div>');

//        $('.btn-group').selectLisr();
        $(".form-datetime").datetimepicker({
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        $(".form-date").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });
    })();

</script>
</body>

</html>
