<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">报价详情</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'&& rfqQuotationComplexVo.status=='4'}">放弃报价说明</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'&& rfqQuotationComplexVo.status=='7'}">已作废</c:when></c:choose></title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfq.datatable.edit.js?v=${version}"></script>
    <script src="${resource}/js/myquotation/rfqQuotationEdit.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/lib/plupload-2.1.9/js/plupload.full.min.js" type="text/javascript"></script>
    <script src="${resource}/js/upload/uploadFile.js?v=${version}" type="text/javascript" ></script>
    <style>

        .flex-col{  width:120px !important; text-align:center; vertical-align:middle;}

        .flexarea td{  width:120px !important;text-align:center; vertical-align:middle;}

        .fixed-left td,th{text-align:center; vertical-align:middle;}

        .fixed-left tr td:nth-of-type(2){text-align:left; padding-bottom:20px;}
        .hide{display: none;}

    </style>

</head>

<body>

<c:import url="../common/top.jsp"/>

<div class="wrapper">

    <div class="container container_main">
        <c:import url="../common/menu.jsp" />

        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active"><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">报价详情</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'&& rfqQuotationComplexVo.status=='4'}">放弃报价查看</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'&& rfqQuotationComplexVo.status=='7'}">已作废</c:when></c:choose></li>
                        <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'&& isBuyer}">
                            <div style="float: right;margin-top: 10px;">
                                <strong class="red">
                                    报价供应商名称：${rfqQuotationComplexVo.rfqQuotationVo.supplierName}
                                    供应商代码：${rfqQuotationComplexVo.rfqQuotationVo.supplierNum}
                                </strong>
                            </div>
                        </c:if>
                    </ol>
                </div>
                <input id="dataSource" type="hidden" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.dataSource}"/>
                <div class="page-content">
                    <div class="page-content">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right"><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">询价单摘要</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价单摘要</c:when></c:choose></span>
                                <div class=" pull-right">
                                    <input id="requestId" type="hidden" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}">
                                    <input type="hidden" id="ouRfqNum" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}" />
                                    <c:set var="ouRfqNum" value="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}"/>
                                    <c:if test="${sessionScope.chatUrl==null || sessionScope.chatUrl == ''}">
                                        <a id = 'im' href="javascript:RFQ.warn('提示：该功能仅在uat环境与生产环境可用！');"></a>
                                    </c:if>
                                    <c:if test="${sessionScope.chatUrl!=null && sessionScope.chatUrl != ''}">
                                        <a id = 'im' target="_blank"  href="${sessionScope.chatUrl}${ouRfqNum}"></a>
                                    </c:if>
                                    <c:if test="${sysUserVo.loginOrgType == '2'}">
                                        <c:choose>
                                            <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">
                                                <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank"> <span class="btn btn-info font12">询单详情</span></a>
                                                <c:if test="${rfqQuotationComplexVo.status=='5'}">
                                                    <span class="btn btn-danger font12" data-position="center"  <%--data-target="#myModa20"--%>onclick="getBill3()">导出成交通知单</span>
                                                </c:if>
                                            </c:when>
                                            <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">
                                                <a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank"><span class="btn btn-info font12">竞价单详情</span></a>
                                            </c:when>
                                        </c:choose>
                                    </c:if>
                                    <c:if test="${sysUserVo.loginOrgType!='2'}">
<p:permission  privilege="1" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <c:choose>
                                    <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">
                                    <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank"> <span class="btn btn-info font12">询单详情</span></a>
                                    </c:when>
                                    <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">
                                    <a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank"><span class="btn btn-info font12">竞价单详情</span></a>
                                    </c:when>
                                    </c:choose>
</p:permission>
                                    </c:if>
                                </div>
                            </div>
                            <div class="panel-body font12">
                                <div class="row">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ' || rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='PSCS'}">询价单号</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价单号</c:when></c:choose>：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}</li>
                                            <li><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ' || rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='PSCS'}">询价标题</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价标题</c:when></c:choose>：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.title}</li>
                                            <li>采购单位：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.ouName}</li>
                                            <li>计划编号：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.planNo}</li>
                                            <li>当前状态：
                                            <span class="red">
                                            <c:choose>
                                                <c:when test="${!isBuyer}">
                                                    <c:if test="${rfqQuotationComplexVo.status=='2'}">
                                                        已报价
                                                    </c:if>
                                                    <c:if test="${rfqQuotationComplexVo.status=='3'}">
                                                        结果待发布
                                                    </c:if>
                                                    <c:if test="${rfqQuotationComplexVo.status=='4'}">
                                                        已放弃
                                                    </c:if>
                                                    <c:if test="${rfqQuotationComplexVo.status=='5'}">
                                                        已中标
                                                    </c:if>
                                                    <c:if test="${rfqQuotationComplexVo.status=='6'}">
                                                        未中标
                                                    </c:if>
                                                    <c:if test="${rfqQuotationComplexVo.status=='7'}">
                                                        已作废
                                                    </c:if>
                                                    <c:if test="${rfqQuotationComplexVo.status=='8'}">
                                                        已过期
                                                    </c:if>
                                                </c:when>
                                                <c:otherwise>
                                                        <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '13'}">结果待发布</c:if>
                                                        <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '10'}">已结束</c:if>
                                                        <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '9'}">结果待审批</c:if>
                                                        <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '11'}">已作废</c:if>
                                                </c:otherwise>
                                            </c:choose>


                                                <%--${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type==1?'报价中':'已报价'}--%>
                                            </span>
                                            </li>
                                            <c:choose>
                                                <c:when test="${sysUserVo.loginOrgType!='2'}">
                                                    <c:choose>
                                                        <c:when test="${rfqQuotationComplexVo.rfqSupplierListVo.supplierName!=null}">
                                                            <c:choose>
                                                                <c:when test="${rfqQuotationComplexVo.rfqSupplierListVo.supplierName!=''}">

                                                                    <c:choose>
                                                                        <c:when test="${rfqQuotationComplexVo.status=='5'}">
                                                                            <li>中标单位：<span class="red">${rfqQuotationComplexVo.rfqSupplierListVo.supplierName}</span></li>
                                                                        </c:when>
                                                                    </c:choose>

                                                                </c:when>
                                                            </c:choose>
                                                        </c:when>


                                                    </c:choose>
                                                </c:when>
                                            </c:choose>

                                        </ul>
                                        <span class="zt-ico-w">
                                         <c:choose>
                                             <c:when test="${!isBuyer}">
                                             <c:if test="${rfqQuotationComplexVo.status=='2'}">
                                                 <img src="${ctx}/images/ybj.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='3'}">
                                                 <img src="${ctx}/images/jgdfb_03.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='4'}">
                                                 <img src="${ctx}/images/yfq.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='5'}">
                                                 <img src="${ctx}/images/yzb.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='6'}">
                                                 <img src="${ctx}/images/wzb.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='7'}">
                                                 <img src="${ctx}/images/yzf.png">
                                             </c:if>
                                             <c:if test="${rfqQuotationComplexVo.status=='8'}">
                                                 <img src="${ctx}/images/ygq.png">
                                             </c:if>
                                             </c:when>
                                             <c:otherwise>
                                                 <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '13'}">
                                                     <img src="${ctx}/images/jgdfb_03.png"></c:if>
                                                 <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '10'}">
                                                     <img src="${ctx}/images/jiesu.png"></c:if>
                                                 <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '9'}">
                                                     <img src="${ctx}/images/dsh.png"></c:if>
                                                 <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.type == '11'}">
                                                     <img src="${ctx}/images/yzf.png"></c:if>
                                             </c:otherwise>
                                         </c:choose>
                                        </span>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>保证金金额：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.assureMoney}元</li>
                                            <li><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ' || rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='PSCS'}">报价货币</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价币种</c:when></c:choose>：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.currency}元</li>
                                            <%--<li>预算总价：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.budgetPrice}元</li>--%>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <li>分项分量报价：允许对部分产品报价   允许对部分数量报价</li></c:if>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <li>分项分量报价：不允许对部分产品报价   允许对部分数量报价</li></c:if>

                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <li>分项分量报价：允许对部分产品报价   不允许对部分数量报价</li></c:if>
                                            <c:if test="${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <li>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</li></c:if>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>发布时间：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.issueDate}</li>
                                            <li><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ' || rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='PSCS'}">报价开始时间</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价开始时间</c:when></c:choose>：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.quotationStartDate}</li>
                                            <li><c:choose><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='RAQ' || rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='PSCS'}">报价截止时间</c:when><c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价截止时间</c:when></c:choose>：${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                            <li>报名截止时间：${rfqQuotationComplexVo.rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>联系人：${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                            <li>联系电话：${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                            <li>收货地址：
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryProvince}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryCity}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryArea}
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.contactsVo.deliveryAddress}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">寻源单条款协议</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                        <div class="clearfix">
                                            <label class="bler blu-1">商务条款：</label>
                                            <span class="bler blu-88-nobg">
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.requestBusiTerms}
                                            </span>
                                        </div>
                                        <div class="clearfix">
                                            <label class="bler blu-1">技术条款：</label>
                                            <span class="bler blu-88-nobg">
                                                ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.requestTecTerms}
                                            </span>
                                        </div>
                                        <div class="mt12 clearfix">
                                            <span class="col-xs-12">附件清单：</span>
                                            <table class="table table-bordered mt12 align-md fixed" style="width:97%;">
                                                <thead>
                                                <tr>
                                                    <th width="50px">序号</th>
                                                    <th>文件名称</th>
                                                    <th width="100px">文件大小</th>
                                                    <th>说明</th>
                                                    <c:if test="${isSupplier == '1'}">
                                                        <th width="100px">操作</th>
                                                    </c:if>
                                                    <c:if test="${isSupplier != '1'}">
                    <%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><th width="100px">操作</th><%--</p:permission>--%>
                                                    </c:if>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:set var="sum" value="1"/>
                                                <c:forEach items="${rfqQuotationComplexVo.rfqRequestComplexVo.attachmentsList}" var="attachment" varStatus="status">
                                                    <c:if test="${attachment.type == 'R'}">
                                                        <tr data-clone="orginal">
                                                            <td>${sum}</td>
                                                            <td>${attachment.originalFilename}</td>
                                                            <td>${attachment.fileSize}KB</td>
                                                            <td>${attachment.fileDeclaration}</td>
                                                            <c:if test="${isSupplier == '1'}">
                                                                <td><a href="${downloadAction}?id=${attachment.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                                            </c:if>
                                                            <c:if test="${isSupplier != '1'}">
                                                            <%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><td><a href="${downloadAction}?id=${attachment.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                                            </c:if>
                                                        </tr>
                                                        <c:set var="sum" value="${sum + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:if test="${rfqQuotationComplexVo.status=='2'||rfqQuotationComplexVo.status=='3'||rfqQuotationComplexVo.status=='5'}">
                        <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right" id="totalPriceType">${rfqQuotationComplexVo.status=='5' ? '中标物料':'报价物料'}</span>
                                <div class="right mr12">
                                    <span class="font12">${rfqQuotationComplexVo.status=='5' ? '中标总价':'以下报价仅对本次询价有效，含税总价'}
                                        <span class="red" id="totalPrice">
                                        </span>元
                                    </span>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <table class="table datatable table-bordered align-md" id="requestItemData">
                                        <thead>
                                        <tr>
                                            <th data-width="80">序号</th>
                                            <th>物料信息
                                                <a class="btn btn-danger btn-mini" id="all-zk" onclick="allZk()">
                                                    <span class="zk_span">全部展开</span>
                                                    <span class="zk_span hide">全部隐藏</span>
                                                </a>
                                            </th>
                                            <th data-width="80">采购数量</th>
                                            <th class="flex-col">*可供量</th>
                                            <c:choose>
                                                <%--1含税,0未税--%>
                                                <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.priceType !=null}">
                                                    <c:choose>
                                                        <c:when test="${rfqQuotationComplexVo.status=='5'}">
                                                            <th class="flex-col">拟签数量</th>
                                                            <th class="flex-col">*税率</th>
                                                        </c:when>
                                                    </c:choose>
                                                </c:when>
                                            </c:choose>
                                            <c:choose>
                                                <%--1含税,0未税--%>
                                                <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.priceType == '1'}">
                                                    <c:choose>
                                                        <c:when test="${rfqQuotationComplexVo.status=='5'}">
                                                            <th class="flex-col">拟签含税单价</th>
                                                            <th class="flex-col">拟签含税总价</th>
                                                        </c:when>
                                                    </c:choose>
                                                </c:when>
                                            </c:choose>

                                            <c:choose>
                                                <%--1含税,0未税--%>
                                                <c:when test="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.priceType == '0'}">
                                                    <c:choose>
                                                        <c:when test="${rfqQuotationComplexVo.status=='5'}">
                                                            <th class="flex-col">拟签未税单价</th>
                                                            <th class="flex-col">拟签未税总价</th>
                                                        </c:when>
                                                    </c:choose>
                                                </c:when>
                                            </c:choose>
                                            <c:if test="${rfqQuotationComplexVo.status!='5'}">
                                            <th class="flex-col">*未税单价(元)</th>
                                            <th class="flex-col">*税率</th>
                                            <th class="flex-col">含税单价</th>
                                            <th class="flex-col">未税总价</th>
                                            <th class="flex-col">含税总价</th>
                                            <th class="flex-col">*承诺交货期</th>
                                            <th class="flex-col">替代型规</th>
                                            <th class="flex-col">制造商</th>
                                            <th class="flex-col">产地</th>
                                            <th class="flex-col">上一级供应渠道</th>
                                            <th class="flex-col">运输方式</th>
                                            <th class="flex-col">发货地点</th>
                                            <th class="flex-col">材质</th>
                                            <th class="flex-col">单重</th>
                                            <th class="flex-col">质保期</th>
                                            <th class="flex-col">报价备注</th>
                                                <c:forEach var="ext" items="${rfqQuotationItemExtra}">
                                                    <th data-width="100" class="flex-col ext-col" data-extname="${ext.name}" data-extcode="${ext.code}" ext="${ext.code}">${ext.name}</th>
                                                </c:forEach>
                                            </c:if>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <%--  <c:forEach var="itemVo" items="${rfqQuotationComplexVo.rfqQuotationItemVo}" varStatus="status">
                                                  <tr data-clone="orginal">
                                                      <td data-order="true">${status.index+1}</td>
                                                      <td>
                                                          代码：${itemVo.materialNo} 物料名称：${itemVo.materialName} 规格型号：${itemVo.character}
                                                          <a href="javascript:;" class="zkBtn icon icon-chevron-down"></a>
                                                          <div class="hidden">
                                                             描述：${itemVo.specification} 计量单位：${itemVo.unit} 生产厂家(品牌)：${itemVo.producer}
                                                          </div>
                                                      </td>
                                                      <td>${itemVo.requestAmount}</td>
                                                      <td>${itemVo.availableCount}</td>
                                                      <td>${itemVo.unitPrice}</td>
                                                      <td>${itemVo.tax}%</td>
                                                      <td>${itemVo.unitPriceTaxed}</td>
                                                      <td>${itemVo.subtotal}</td>
                                                      <td>${itemVo.subtotalTaxed}</td>
                                                      <td>${itemVo.availableDeliveryDate}</td>
                                                      <td>${itemVo.substitutableSpec}</td>
                                                      <td>${itemVo.manufactory}</td>
                                                      <td>${itemVo.productPlace}</td>
                                                      <td>${itemVo.upstreamSuppliers}</td>
                                                      <td>${itemVo.transType}</td>
                                                      <td>${itemVo.deliveryLocation}</td>
                                                      <td>${itemVo.material}</td>
                                                      <td>${itemVo.singleWeight}</td>
                                                      <td>${itemVo.timeOfWarranty}</td>
                                                      <td>${itemVo.memo}</td>
                                                  </tr>
                                              </c:forEach>--%>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="so-form-40 mt12">
                                    <c:if test="${sysUserVo.loginOrgType=='2'}">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="exportExcel();">
                                                <i class="icon icon-upload-alt"></i>&nbsp;导出报价Excel
                                            </button>
                                    </c:if>
                                    <c:if test="${sysUserVo.loginOrgType!='2'}">
                                    <p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <button class="btn btn-primary btn-sm" type="button" onclick="exportExcel();">
                                        <i class="icon icon-upload-alt"></i>&nbsp;导出报价Excel
                                    </button>
                                    </p:permission>
                                    </c:if>
                                    <div class="pull-right" style="margin-top:-18px;" id="d_pager"></div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${rfqQuotationComplexVo.status=='2'||rfqQuotationComplexVo.status=='3'}">
                        <div class="panel mt24">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">报价说明</span>
                            </div>

                            <div class="panel-body">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">报价有效期：</label>
                                        <span class="bler blu-2">${rfqQuotationComplexVo.rfqQuotationVo.quotationValidDate}</span>
                                    </div>

                                    <div class="clearfix">
                                        <label class="bler blu-8">返点说明：</label>
                                        <span class="bler blu-2">${rfqQuotationComplexVo.rfqQuotationVo.pointDescription}</span></div>
                                    <div class="clearfix">
                                        <label class="bler blu-8">其它说明：</label>
                                        <span class="bler blu-2">${rfqQuotationComplexVo.rfqQuotationVo.quotationMemo}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">报价附件</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <table class="table table-bordered align-md fixed">
                                        <thead>
                                        <tr>
                                            <th width="20%">序号</th>
                                            <th width="20%">文件名称</th>
                                            <th width="20%">文件大小</th>
                                            <th width="20%">说明</th>
                                            <c:if test="${isSupplier == '1'}">
                                                <th width="20%">操作</th>
                                            </c:if>
                                            <c:if test="${isSupplier != '1'}">
                                                <%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><th width="20%">操作</th><%--</p:permission>--%>
                                            </c:if>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="sum" value="1"/>
                                        <c:forEach var="item" items="${rfqQuotationComplexVo.attachmentsList}" varStatus="status">
                                            <c:if test="${item.type == 'Q'}">
                                                <tr data-clone="orginal">
                                                    <td data-order="true">${sum}</td>
                                                    <td>${item.originalFilename}</td>
                                                    <td>${item.fileSize}KB</td>
                                                    <td>${item.fileDeclaration}</td>
                                                    <c:if test="${isSupplier == '1'}">
                                                        <td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                                    </c:if>
                                                    <c:if test="${isSupplier !='1'}">
                                                        <%--<p:permission  privilege="2" requestNo="${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                                    </c:if>
                                                </tr>
                                                <c:set var="sum" value="${sum + 1}"/>
                                            </c:if>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${rfqQuotationComplexVo.status=='4'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">放弃说明：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                            ${rfqQuotationComplexVo.rfqSupplierListVo.giveUpMemo}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${rfqQuotationComplexVo.status=='5'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">中标说明：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                            ${rfqQuotationComplexVo.memoDesc}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${rfqQuotationComplexVo.status=='6'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">未中标说明：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                            ${rfqQuotationComplexVo.memoDesc}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${rfqQuotationComplexVo.status=='7'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">作废原因：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                            ${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.pubEndMemo}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <rc:roleCheck roleType="010">
                        <c:if test="${rfqQuotationComplexVo.status=='2' && empty param['type']}">
                                    <div class="text-right" id="foot">
                                        <a href="${ctx}/rfqQuotation/getRfqQuotation?requestId=${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}&quotationType=reQuotation" class="btn btn-lg btn-primary" type="button">重新报价</a>
                                        <a href="${ctx}/rfqQuotation/init" class="btn btn-lg btn-primary" type="button">返回</a>
                                    </div>
                        </c:if>
                    </rc:roleCheck>
                    <c:if test="${!isBuyer}">
                        <div class="text-right" style="padding:0 10px 0 10px;">
                            <a class="left blue" href="#" data-toggle="modal" data-target="#myModa3" id="myHistory">我的报价历史</a>
                        </div>

                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 非标报弹窗 -->
<div class="modal fade" id="myModafei">
    <div class="modal-dialog modal-fullscreen"  style="overflow:auto;">
        <div class="modal-content" style=" width:2280px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="position:fixed; top:15px; right:15px;">
                    <span aria-hidden="true">×</span><span class="sr-only">关闭</span>
                </button>
                <h4 class="modal-title">国内非标备件详细报价页面</h4>
            </div>
            <form id="detailForm" >
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
        </div>
    </div>
</div>
<div class="modal fade" id="myModa3">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的报价历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                    <div class="col-md-12" id="jqGridPager"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer" data-dismiss="modal"><a type="button" class="btn btn-primary" href="javascript:;">确定</a></div>
    </div>
</div>

<script>
    var confiremedPrice='${confiremedPrice}';//中标含税总价
    var confiremedNoTaxPrice='${confiremedNoTaxPrice}';//中标非税总价
    var priceTaxed='${rfqQuotationComplexVo.rfqQuotationVo.subtotalTaxed}';//含税总价
    var requestId = '${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}';
    var priceType = '${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.priceType}';////核价方式 1 含税/0 未税
    var status = '${rfqQuotationComplexVo.status}';//5就是中标物料页面
    var supplierNum = "${param['supplierNum']}";
    if (supplierNum =="" || supplierNum ==null){
        supplierNum="${supplierNum}"
    }
    var id = "${param['id']}"; //报价单id 标识区分采购组织查看 或者供应商报价
    if(id != ''){
        var supplierNum = '${rfqQuotationComplexVo.rfqQuotationVo.supplierNum}';
    }
    var status='${rfqQuotationComplexVo.status}';//5是中标物料
    //报价截止时间
    var stopTime=(Date.parse('${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.quotationEndDate}'));
    //当前时间
    var currentTime = new Date().getTime();
    if (stopTime<=currentTime){
        $("#foot").hide();
    }else {
        $("#foot").show();
    }

    /* 初始化数据表格 */
    $('table.datatable').datatable();

    function setHeight() {
        $('.datatable-rows .fixed-left tr').each(function () {
            var sda = $(this).index();
            var sheight = $(this).find('td').height();
            $('.datatable-rows .flexarea tr').eq(sda).find('td').height(sheight);
        })
    }

    function allZk(){
        $('.zk_span').toggleClass('hide');
        var index = $('.zk_span.hide').index();
        $('.zkBtn').each(function(){
            if(index == 0){
                $(this).next().removeClass('hidden');
            }else{
                $(this).next().addClass('hidden');
            }
        });
        setHeight();
        if(index == 0){
            $('.zkBtn').addClass('cur');
        }else{
            $('.zkBtn').removeClass('cur');
        }
    }

    $(function(){
        $('#datatable-requestItemData').on('click','.zkBtn',function(){
            $(this).next().toggleClass('hidden');
            setHeight();
            $(this).toggleClass('cur');
        });

        //物料分页加载
        var dataSource=$("#dataSource").val();
        var url = ctx+'/rfqQuotationItem/rfqQuotationItemList?requestId='+requestId+'&qtype='+status+'&supplierNum='+supplierNum;
        if(id){
            url = ctx+'/rfqQuotation/getQuotationHistoryDetail?id='+id+'&type=1' + '&qtype=' + status;
        }
        var _mappingNames_ext=[];
        var display_div = '<div style="position: relative;">{value}</div>';//输入框
        $('#requestItemData').find('th.ext-col').each(function (i) {
        			var $this = $(this);
        			var extCode = $this.data('extcode');
        			var valName = 'itemValue_'+(i+1);
        			 // extColsMapping[valName] = extCode;
        			_mappingNames_ext.push(valName);
        });
        //------------------------跳转到次页面后table加载方式start
        if (priceType=='1'&&status=='5') {//含税核价  , 'unitPrice', 'tax', 'unitPriceTaxed', 'subtotal', 'subtotalTaxed', 'availableDeliveryDate', 'substitutableSpec', 'manufactory', 'productPlace', 'upstreamSuppliers', 'transType', 'deliveryLocation', 'material', 'singleWeight', 'timeOfWarranty', 'memo'
            $("#totalPriceType").text("中标含税总价：");
            $("#totalPrice").text(confiremedPrice)
            var _mappingNames=['items', 'requestAmount', 'availableCount', 'confirmedAmout','tax', 'confirmedPriceTaxed', 'rsubtotalTaxed'];
//            $.each(_mappingNames_ext,function(i,d){
//                _mappingNames.push(d);
//            });
//            console.log(1);
//            console.log(_mappingNames);
            var options = {
                datatype: 'json',
                mtype: 'post',
                renderDel: false,
                url: url,
                mappingNames: _mappingNames,
                cellFormat: function (name, val, row, index) {
                    row.purchaseNo = row.purchaseNo == undefined ? "" : row.purchaseNo;
                    row.purchasePerson = row.purchasePerson == undefined ? "" : row.purchasePerson;
                    row.drawingNo = row.drawingNo == undefined ? "" : row.drawingNo;
                    if (name == 'items') {
                        var extendItem = "" ;
//                        debugger;
                        //有自定义物料时解析自定义物料
                        if(row.requestExtendItemData != undefined &&　row.requestExtendItemData　!= ''　){
                            //json串转换成数组对象
                            var requestExtendItemData = JSON.parse(row.requestExtendItemData);
                            //遍历数组
                            $.each(requestExtendItemData,function (i,d) {
                                var itemName = d.itemName;
                                var itemValue = d.itemValue;
                                extendItem += (" " + itemName + " : " + itemValue + "<br/>");//拼接自定义物料信息
                            });
                        }
                        if(dataSource=='JFE'){
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/>型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>申购单号：{purchaseNo}<br/>申购人：{purchasePerson}<br/>图号：{drawingNo}<br/>备注：{requestMemo}<br/>'+extendItem+'</div></div>').format(row);
                        }else  if(dataSource=='NBBX'){
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/>型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>材质/图号：{drawingNo}<br/>备注：{requestMemo}<br/>'+extendItem+'</div></div>').format(row);
                        }else{
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/>型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>备注：{requestMemo}<br/>'+extendItem+'</div></div>').format(row);
                        }
                    }
                    if(name == 'tax'&& val !== ''){
                        val = val + '%';
                    }
                    if ("availableCount" == name || "subtotalTaxed" == name || "requestAmount" == name || "singleWeight" == name || "unitPriceTaxed" == name) {
                        val =  val === "" ? "" : val.toFixed(4);
                    } else if ("unitPrice" == name || "subtotal" == name) {
                        val =  val === "" ? "" : val.toFixed(2);
                    }
                    return val || '';
                }
            };
        } else if(priceType=='0'&&status=='5') {//未税核价  ,'unitPrice', 'tax', 'unitPriceTaxed', 'subtotal', 'subtotalTaxed', 'availableDeliveryDate', 'substitutableSpec', 'manufactory', 'productPlace', 'upstreamSuppliers', 'transType', 'deliveryLocation', 'material', 'singleWeight', 'timeOfWarranty', 'memo'
            $("#totalPriceType").text("中标未税总价：");
            $("#totalPrice").text(confiremedNoTaxPrice)
            var _mappingNames=['items', 'requestAmount', 'availableCount', 'confirmedAmout', 'tax','confirmedUnitPrice', 'rconfirmedSubtotal'];
//            $.each(_mappingNames_ext,function(i,d){
//                _mappingNames.push(d);
//            });
//            console.log(2);
//            console.log(_mappingNames);
            var options = {
                datatype: 'json',
                mtype: 'post',
                renderDel: false,
                url: url,
                mappingNames: _mappingNames,
                cellFormat: function (name, val, row, index) {
                    row.purchaseNo = row.purchaseNo == undefined ? "" : row.purchaseNo;
                    row.purchasePerson = row.purchasePerson == undefined ? "" : row.purchasePerson;
                    row.drawingNo = row.drawingNo == undefined ? "" : row.drawingNo;
                    if (name == 'items') {
                        var extendItem = "" ;
                        //有自定义物料时解析自定义物料
                        if(row.requestExtendItemData != undefined &&　row.requestExtendItemData　!= ''　){
                            //json串转换成数组
                            var requestExtendItemData = JSON.parse(row.requestExtendItemData);
                            //遍历数组
                            $.each(requestExtendItemData,function (i,d) {
                                var itemName = d.itemName;
                                var itemValue = d.itemValue;
                                extendItem += (" " + itemName + " : " + itemValue + "<br/>");//拼接自定义物料信息
                            });
                        }
                        if(dataSource=='JFE'){
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>申购单号：{purchaseNo}<br/>申购人：{purchasePerson}<br/>图号：{drawingNo}<br/>备注：{requestMemo}<br/>'+extendItem+'<br/></div></div>').format(row);
                        }else if(dataSource=='NBBX'){
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>材质/图号：{drawingNo}<br/>备注：{requestMemo}<br/>'+extendItem+'<br/></div></div>').format(row);
                        }else{
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>备注：{requestMemo}<br/>'+extendItem+'<br/></div></div>').format(row);
                        }
                    }
                    if(name == 'tax'&& val !== ''){
                        val = val + '%';
                    }
                    if ("availableCount" == name || "subtotalTaxed" == name || "requestAmount" == name || "singleWeight" == name || "unitPriceTaxed" == name) {
                        val =  val === "" ? "" : val.toFixed(4);
                    } else if ("unitPrice" == name || "subtotal" == name) {
                        val =  val === "" ? "" : val.toFixed(2);
                    }
                    return val || '';
                }
            };
        }else {//非中标情况页面
            $("#totalPrice").text(priceTaxed)
             var _mappingNames=['items', 'requestAmount', 'availableCount','unitPrice', 'tax', 'unitPriceTaxed', 'subtotal', 'subtotalTaxed', 'availableDeliveryDate', 'substitutableSpec', 'manufactory', 'productPlace', 'upstreamSuppliers', 'transType', 'deliveryLocation', 'material', 'singleWeight', 'timeOfWarranty', 'memo'];
            $.each(_mappingNames_ext,function(i,d){
                _mappingNames.push(d);
            });
//            console.log(3);
//            console.log(_mappingNames);
            var options = {
                datatype: 'json',
                mtype: 'post',
                renderDel: false,
                url: url,
                mappingNames: _mappingNames,
                cellFormat: function (name, val, row, index) {
                    row.purchaseNo = row.purchaseNo == undefined ? "" : row.purchaseNo;
                    row.purchasePerson = row.purchasePerson == undefined ? "" : row.purchasePerson;
                    row.drawingNo = row.drawingNo == undefined ? "" : row.drawingNo;
                    if (name == 'items') {
                        var extendItem = "" ;
                        //有自定义物料时解析自定义物料
                        if(row.requestExtendItemData != undefined &&　row.requestExtendItemData　!= ''　){
                            //json串转换成对象数组
                            var requestExtendItemData = JSON.parse(row.requestExtendItemData);
                            //遍历数组
                            $.each(requestExtendItemData,function (i,d) {
                                var itemName = d.itemName;
                                var itemValue = d.itemValue;
                                extendItem += (" " + itemName + " : " + itemValue + "<br/>");//拼接自定义物料信息
                            });
                        }

                        if(dataSource=='JFE'){
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>申购单号：{purchaseNo}<br/>申购人：{purchasePerson}<br/>图号：{drawingNo}<br/>备注：{requestMemo}<br/>'+extendItem+'<br/></div></div>').format(row);
                        }else if(dataSource=='NBBX'){
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/> 型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>材质/图号：{drawingNo}<br/>备注：{requestMemo}<br/>'+extendItem+'<br/></div></div>').format(row);
                        }else{
                            return ('<div style="position: relative;">代码：{materialNo} <br/>物料名称：{materialName} <br/>型号规格：{character} <br/><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden">计量单位：{unit} <br/>生产厂家(品牌)：{producer} <br/>交货期：{requestDeliveryDate} <br/>备注：{requestMemo}<br/>'+extendItem+'<br/></div></div>').format(row);
                        }
                    }

                    if(name == 'tax'&& val !== ''){
                        val = val + '%';
                    }
                    if ("availableCount" == name || "subtotalTaxed" == name || "requestAmount" == name || "singleWeight" == name || "unitPriceTaxed" == name) {
                        val = val === "" ? "" : val.toFixed(4);
                    } else if ("unitPrice" == name || "subtotal" == name) {
                        val = val === "" ? "" : val.toFixed(2);
                    }
//                    console.log(display_div.format({value:val||''}));
//                    console.log(name);
                    return display_div.format({value:val||''});
                    //return val || '';
                }
            };
        }
        //------------------------跳转到次页面后table加载方式end
        RFQ.dataTable('#requestItemData').init(options).load();
    });

    //excel导出
    function exportExcel(){
        $.ajax({
            type: "POST",
            /*url: ctx+"/rfqRequestItem/exportRecord",*/
            url: ctx+"/rfqQuotationItem/exportRecord",
            data: {
                requestId: requestId,
                qtype:status,
                supplierNum:supplierNum
            },
            success: function(data) {
                if ("success"==data.status) {
                    location.href=data.message;
                } else {
                    $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                };
            }
        });
    }

    function getDetail(id,quotationItemId) {
        $.ajax({
            url: ctx + "/rfqQuotation/getQuotationItemDetail",
            type: "POST",
            async: false, //设置成为 同步的
            data: {
                requestItemId: id,
                quotationItemId: quotationItemId
            },
            success: function (data) {
                var htmlTex = '<div class="modal-body"><div class="content">' +
                        '<div class="so-form">' +
                        '<span class="so-form-20">编号：' + data.seriNo + '</span>' +
                        '<span class="so-form-20">金额单位：不含增值税元</span>' +
                        '</div>' +
                        '<div class="clearfix"></div>' +
                        '<div class="so-form">' +
                        '<span class="so-form-20">中文品名：高速矫机剪切模</span>' +
                        '<span class="so-form-20">物品代码：' + data.materialNo + '</span>' +
                        '<span class="so-form-15">申请单行编号：' + data.singleLineNo + '</span>' +
                        '<span class="so-form-15">图号：' + data.picNo + '</span>' +
                        '<span class="so-form-15">要求交货期：2010/12/15</span>' +
                        '<span class="so-form-15">数量：1(套）</span>' +
                        '</div>' +
                        '<table class="table table-bordered align-md change_1" id="datatable-requestItemdetailTable">' +
                        '<thead>' +
                        '<tr>' +
                        '<th rowspan="2">序号</th>' +
                        '<th rowspan="2">零件名称</th>' +
                        '<th rowspan="2" width="90"></th>' +
                        '<th rowspan="2">数量</th>' +
                        '<th colspan="4" width="">报价单位</th>' +
                        '<th colspan="2">' + data.processName11 + '</th>' +
                        '<th colspan="2">' + data.processName12 + '</th>' +
                        '<th colspan="2">' + data.processName13 + '</th>' +
                        '<th colspan="2">' + data.processName14 + '</th>' +
                        '<th colspan="2">' + data.processName15 + '</th>' +
                        '<th colspan="2">' + data.processName16 + '</th>' +
                        '<th colspan="2">' + data.processName17 + '</th>' +
                        '<th colspan="2">' + data.processName18 + '</th>' +
                        '<th colspan="2">' + data.processName19 + '</th>' +
                        '<th colspan="2">' + data.processName110 + '</th>' +
                        '<th colspan="2">' + data.processName111 + '</th>' +
                        '<th rowspan="2" width="90">加工费合计</th>' +
                        '<th rowspan="2" width="90">单价合计</th>' +
                        '</tr>' +
                        '<tr>' +
                        '<th width="70">净重</th>' +
                        '<th width="70">毛重</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">材料费</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '<th width="70">单件工时</th>' +
                        '<th width="70">单价</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                for (var i = 1; i < 12; i++) {
                    htmlTex += '<tr>' +
                            '<td rowspan="3">' + i + '</td>' +
                            '<td rowspan="3">' + data["partName" + i] + '</td>' +
                            '<td rowspan="2">供应商报价</td>' +
                            '<td rowspan="2">' + data["quantity" + i] + '</td>' +
                            '<td colspan="4" width="">9Cr18MoV</td>' +
                            '<td colspan="2" width="">锻造</td>' +
                            '<td colspan="2" width="">车床加工</td>' +
                            '<td colspan="2" width="">热处理</td>' +
                            '<td colspan="2" width="">线切割孔</td>' +
                            '<td colspan="2" width="">磨床加工</td>' +
                            '<td colspan="2" width="">锻造</td>' +
                            '<td colspan="2" width="">车床加工</td>' +
                            '<td colspan="2" width="">热处理</td>' +
                            '<td colspan="2" width="">线切割孔</td>' +
                            '<td colspan="2" width="">磨床加工</td>' +
                            '<td colspan="2" width="">其它加工</td>' +
                            '<td width="">&nbsp;</td>' +
                            '<td width="">&nbsp;</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td>' + data["netWeight" + i] + '</td>' +
                            '<td>' + data["grossWeight" + i] + '</td>' +
                            '<td>' + data["materialUnitPrice" + i] + '</td>' +
                            '<td>' + data["materialFee" + i] + '</td>';
                    if (i == 11) {
                        htmlTex += '<td>' + data["timeOfSingleton11_1"] + '</td>' +
                                '<td>' + data["unitPriceOfSingleton11_1"] + '</td>';
                    } else {
                        htmlTex += '<td>' + data["timeOfSingleton" + i + 1] + '</td>' +
                                '<td>' + data["unitPriceOfSingleton" + i + 1] + '</td>';
                    }
                    htmlTex += '<td>' + data["timeOfSingleton" + i + 2] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 2] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 3] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 3] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 4] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 4] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 5] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 5] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 6] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 6] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 7] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 7] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 8] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 8] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 9] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 9] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 10] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 10] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 11] + '</td>' +
                            '<td>' + data["timeOfSingleton" + i + 11] + '</td>' +
                            '<td>' + data["processTotalFee" + i] + '</td>' +
                            '<td>' + data["unitTotalPrice" + i] + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td>业务员核价</td>' +
                            '<td>1</td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '<td></td>' +
                            '</tr>';
                }

                htmlTex += '<tr>' +
                        '<td rowspan="3" colspan="2">其它费用</td>' +
                        '<td rowspan="2">供应商报价</td>' +
                        '<td colspan="3">管理费</td>' +
                        '<td colspan="2">利润</td>' +
                        '<td colspan="2">包装费</td>' +
                        '<td colspan="2">运输费</td>' +
                        '<td colspan="2">合成费</td>' +
                        '<td colspan="2">设计费</td>' +
                        '<td colspan="3">配套件</td>' +
                        '<td colspan="4">单套(件)总价</td>' +
                        '<td colspan="4">总价</td>' +
                        '<td colspan="5"></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td colspan="2">' + data.managementFeeRate + '</td>' +
                        '<td>%</td>' +
                        '<td>' + data.profitsRate + '</td>' +
                        '<td>%</td>' +
                        '<td>' + data.packingFeeRate + '</td>' +
                        '<td>%</td>' +
                        '<td>' + data.transportFeeRate + '</td>' +
                        '<td>%</td>' +
                        '<td>' + data.compositionFeeRate + '</td>' +
                        '<td>%</td>' +
                        '<td>' + data.designFeeRate + '</td>' +
                        '<td>%</td>' +
                        '<td colspan="2">' + data.kitFee + '"></td>' +
                        '<td>元</td>' +
                        '<td colspan="3">' + data.singleKitTotalPrice + '</td>' +
                        '<td>元</td>' +
                        '<td colspan="3">' + data.totalPrice + '</td>' +
                        '<td>元</td>' +
                        '<td colspan="2">承诺交货期</td>' +
                        '<td colspan="3">2010/12/22</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>业务员核价</td>' +
                        '<td colspan="2"></td><td>%</td>' +
                        '<td></td><td>%</td>' +
                        '<td></td><td>%</td>' +
                        '<td></td><td>%</td>' +
                        '<td></td><td>%</td>' +
                        '<td></td><td>%</td>' +
                        '<td colspan="2"></td><td>元</td>' +
                        '<td colspan="3"></td><td>元</td>' +
                        '<td colspan="3"></td><td>元</td>' +
                        '<td colspan="5">工装模具用此单另报</td>' +
                        '</tr>' +
                        '</tbody>' +
                        '</table>' +
                        '<div class="panel mt12">' +
                        '<div class="panel-heading clearfix">' +
                        '<span class="mg-margin-right">报价说明：</span>' +
                        '</div>' +
                        '<div class="panel-body">' +
                        '<div class="row">' +
                        '<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">' +
                        '报价说明：每套剪切模由剪切上模和剪切下模各一件组成，孔由线切割完成，确保精度。<br/>' +
                        '报价人：张 ××     日期：2010年12月*日    联系电话： <br/>' +
                        '报价单位：<br/>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="panel">' +
                        '<div class="panel-heading clearfix">' +
                        '<span class="mg-margin-right">核价说明：</span>' +
                        '</div>' +
                        '<div class="panel-body">' +
                        '<div class="row">' +
                        '<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">' +
                        '核价说明原因描述描述描述描述描述描述描述描述描述描述。<br/>' +
                        '采购员：' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="panel">' +
                        '<div class="panel-heading clearfix">' +
                        '<span class="mg-margin-right">备注：</span>' +
                        '</div>' +
                        '<div class="panel-body">' +
                        '<div class="row">' +
                        '<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">' +
                        '1、计量单位：费用：元；重量：公斤；工时：小时。<br/>' +
                        '2、本核价表由供应商按此格式填写，有底纹的格子由采购员填写。<br/>' +
                        '3、当某种零件加工工序超过10道时，列出主要的10道工序分别报价，其余工序合并在第11道工序内报价。<br/>' +
                        '4、组件的零件超过10种时，列出主要的10种零件并允许简化报价，工序名称、单件工时、工序单价省略，其它内容必须填写；其余零件归在第11种（其它零件）内；当采用简化报价时，加工费合计一栏的程序计算公式不起作用，其报价、核价必须手工录入。' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="panel">' +
                        '<div class="panel-heading clearfix">' +
                        '<span class="mg-margin-right">核价方式说明：</span>' +
                        '</div>' +
                        '<div class="panel-body">' +
                        '<div class="row">' +
                        '<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">' +
                        '1、全部认可：全部认可供应商报价。 <br/>' +
                        '2、局部修改：个别数据认为有误，手工输入数据修改（该单元格计算公式就删除了）。  <br/>' +
                        '3、总体下浮：材料费、工时费按输入的比例总体下浮。   <br/>' +
                        '4、局部下浮：可多选或单选下浮全部材料重量、下浮全部材料单价、下浮全部工时小时、下浮全部工时单价，输入下浮比例。 <br/>' +
                        '5、总价修改：仅对总价手工输入修改。 <br/>' +
                        '6、其它费用的核价，数据量小，手工输入管理费率、利润率、包装费率、运输费率等。' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                $('#detailForm').html(htmlTex);
            }
        });
    }

//--------供应商导出成交通知单
    function getBill3() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/supplierExport",
            data: {id: '${rfqQuotationComplexVo.rfqRequestComplexVo.requestVo.id}',supplierCode:'${rfqQuotationComplexVo.rfqQuotationVo.supplierNum}'},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
</script>

</body>

</html>

