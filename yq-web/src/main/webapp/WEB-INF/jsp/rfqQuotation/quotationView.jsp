<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>供应商报价历史列表</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/myquotation/rfqQuotationHistory.js?v=${version}"></script>
</head>

<body>
<c:import url="../common/top.jsp" />
<input id="requestId" type="hidden" value="${RfqQuotationHisotryBo.requestId}">
<input id="supplierNum" type="hidden" value="${RfqQuotationHisotryBo.supplierNum}">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">供应商报价历史列表</li>
                    </ol>
                </div>
                <div class="page-content">

                    <div class="tab-content">

                        <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                        <div class="col-md-12" id="jqGridPager"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>
