<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/24
  Time: 9:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>中标信息</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%@include file="../common/tagDeclare.jsp" %>
    <%@include file="../common/headBase.jsp" %>
    <c:import url="../common/kingEditor.jsp"/>
    <%@include file="../common/jqGridBootstrap.jsp" %>

    <script src="${pageContext.request.contextPath}/js/win/myauctionwin.js?v=${version}" type="text/javascript"></script>
    <%--附件上传--%>
    <script src="${pageContext.request.contextPath}/lib/plupload-2.1.9/js/plupload.full.min.js" type="text/javascript"></script>
    <script src="${resource}/js/upload/uploadFile.js?v=${version}" type="text/javascript" ></script>
    <script src="${pageContext.request.contextPath}/js/echarts.common.min.js"></script>

    <script src="${pageContext.request.contextPath}/js/designateResult/pricingCommonality.js?v=${version}"></script>
</head>
<body>
<c:import url="../common/top.jsp"/>
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp"/>
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">中标信息</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="alert alert-warning clearfix" id="redWrite"> 根据采购方要求，请在中标后补填报价明细，明细总额须与中标金额一致，否则无法提交。 <a href="#"
                                                                                                           class="close"
                                                                                                           font-size="22px"
                                                                                                           data-dismiss="alert">
                        × </a></div>
                    <div class="panel">
                        <div class="panel-heading clearfix" style="padding:0 15px; line-height:36px;">
                            <span class="mg-margin-right">基本信息</span>
                            <div class=" pull-right">
                                <a href="${ctx}/dacReverseAuction/reverseAuctionDetail?id=${rfqRequestVo.id}" target="_blank">
                                    <span class="btn btn-primary font12">竞价单详情</span>
                                </a>
                                <span class="btn btn-warning font12"  onclick="dacDownload('${rfqRequestVo.id}','${biddingMethod}','${quotationType==null?"5":"30"}')">下载物料明细</span>
                                <c:if test="${quotationType=='3'}">
                                    <span class="btn btn-warning font12" onClick="excelClicke()"
                                          data-toggle="modal" data-target="#cancelPrice33">上传物料明细</span>
                                </c:if>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="color:#666;">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li style="width:40%">竞价单号：${rfqRequestVo.unifiedRfqNum}</li>
                                            <li style="width:40%">计划编号：${rfqRequestVo.planNo}</li>
                                            <li style="width:40%">竞价标题：${rfqRequestVo.title}</li>
                                        </ul>
                                        <span class="zt-ico-w" style="right:150px"><img
                                                src="${pageContext.request.contextPath}/images/yzb.png"></span>
                                        <%--<span class="info-btn-right right mt20 mr12"><a class="btn btn-primary"
                                                                                        href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${rfqRequestVo.id}"
                                                                                        target="gysbj-jingjiadanxiangqing">竞价单详情</a></span>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">

                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">中标物料</span>
                            <c:if test="${biddingMethod == '1'}">
                            <span class="right font-16px" id="winMoney">中标金额：<span style="color:red" id="redMoney">${designeVo.subtotalTaxed}</span>元
                                <%--<a class="blue" herf="javascript:;" data-toggle="modal"--%>
                                                                  <%--data-target="#myModal20"><strong class="red">100000.00元</strong></a></span>--%>
                           </c:if>
                        </div>


                        <div class="panel-body padding1">
                            <div class="jqGridPage">
                                <table id="jqGrid"></table>
                                <div id="jqGridPager" style="width: 980px"></div>
                            </div>
                        </div>

                    </div>


                    <div class="panel" id="desc">
                        <div class="panel-heading clearfix"> <span class="mg-margin-right">中标说明</span> </div>
                        <div class="panel-body padding1">
                            <%--<textarea name="content" readonly="readonly" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">aaa</textarea>--%>
                                <%--<div class="tab-pane active" id="tabgs1">--%>
                                <div class="tab-pane active">
                                    <textarea id="contentSimple1"  name="contentSimple1" class= "" style=" width:100%;height:150px;color:#000;"> </textarea>
                                </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading clearfix"><span class="mg-margin-right">补填明细附件</span></div>
                        <div class="panel-body padding1">
                            <table class="table table-bordered table-hove mt5 align-md" style="font-size:14px">
                                <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>文件名称</th>
                                    <th>文件大小</th>
                                    <th>说明</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody id="filelsit">
                                </tbody>
                            </table>
                            <span class="c999"  style="color: red;"> 附件类型不支持exe格式，文件大小不超过100M。              </span>
                            <div class="btn-group left">
                                <div class="so-form-40">
                                    <button id="addFile" class="btn btn-primary btn-sm" href="#" data-toggle="modal"
                                            data-target="#myModa2"><i class="icon icon-plus"></i>&nbsp;&nbsp;新增附件
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right" id="submit"><a onclick="submitButtonEvent()" class="btn btn-lg btn-warning" type="button"
                                               >提交</a><a onclick="closeButtonEvent()" class="btn btn-lg btn-danger ml5"
                                                                               type="button">关闭</a></div>
                </div>

            </div>
        </div>
    </div>
</div>

<%--文件上传 start--%>
<div class="modal fade" id="myModa2">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                            <form>
                                <input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
                                <input type="hidden" id="requestID" value="${rfqRequestVo.id}">
                                <input type="text" id="fileName" placeholder="请上传,附件大小≤100M"
                                       class="form-control no-border"/>
                                <input type="button" id="upload" value="上传">
                                <span id="msg">0%</span>
                            </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" <%--onclick="saveUpload()"--%> class="btn btn-primary js-confirm"
                        data-dismiss="modal" id="sure" disabled="disabled">确定
                </button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<%--文件上传 end--%>
<!-- excel导入开始 -->
<div class="modal fade" id="cancelPrice33">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">excel导入</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row clearfix" style="margin-bottom:8px;">
                    <span class="left-cont2">请上传根据模版编辑好的文件。</span>
                    <form action="${resource}/rfqRequestItemTmp/importRecord2" method="post" class="form-horizontal" id="excelUpload" onsubmit="return false;">
                        <input id="requestIdURL" name="requestId" type="hidden" value="${rfqQuotationBO.requestId}">
                        <input type="file" name="file" id="excelFile">
                        <input type="text"  id="fileNamess" placeholder="请上传,附件大小≤100M" disabled /><span  id="msgss"></span>
                    </form>

                    </span>
                </div>
                <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px; height:66px;">
                    <i class="icon icon-warning-sign sign-warn1"></i><span class="sign-1 ml12">
                        注意：Excel批量导入将覆盖询单内现有物料；
                    <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上传附件文件类型仅限Excel文件。</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"  onclick="upstart()" class="btn btn-primary js-confirm" data-dismiss="modal" id="suress" disabled="disabled">确定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- excel导入结束 -->
<div class="modal fade" id="myModal19">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title" id="thing">XX物料</h4>
            </div>
            <div class="modal-body">
                <div id="mainChart" style="width:870px;height:400px;"></div>
            </div>
            <div class="modal-footer"><a type="button" data-dismiss="modal" class="btn btn-primary" href="javascript:;">确定</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal20">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的出价</h4>
            </div>
            <div class="modal-body">
                <div id="mainChart1" style="width:870px;height:400px;"></div>
            </div>
            <div class="modal-footer"><a type="button" data-dismiss="modal" class="btn btn-primary" href="javascript:;">确定</a>
            </div>
        </div>
    </div>
</div>


</body>
</html>

<script>
    var supplierNumt="${sysUserVo.supplierCode}"
    var downloadUrl='${downloadUrl}'//sftp路径
    var subtotalTaxed='${designeVo.subtotalTaxed}'
    var requestId='${rfqRequestVo.id}'
    var biddingMethod='${biddingMethod}'//是按物料还是按总价
    var quotationId='${designeVo.quotationId}';
    var type='${rfqRequestVo.type}'//询单状态,10为结束
    var i = '${basevo.id}';        //


    //插件初始化代码在js文件
    /*
     上传
     */
    $(function () {
        var $myModa2 = $('#myModa2');

        $myModa2.on('click', '.js-confirm', function () {


            var data = $myModa2.data('_result');
            downloadUrl = data.downloadUrl;
            if (data == undefined || data == '') {
                showErrorMsg("您还没选择文件！");
            }
            if (!data.fileSize) {
                showErrorMsg('文件未上传完成，请重新上传');
                return false;
            }

            var requestid = '${basevo.id}';
            var fileDeclaration = $("#fileDeclaration").val();
            if (checkLen(fileDeclaration)) {
                showErrorMsg("文件说明长度超过2000字数限制！")
            } else {
                data["fileDeclaration"] = fileDeclaration;
                data["requestId"] = '${basevo.id}';
                /*dataObj["type"]='P';*/

                //如果rfqPreaudit=null会导致后台错误,必须doNothing,原因未知
                if ('${rfqPreaudit.paId}'!= null&&'${rfqPreaudit.paId}'!="") {
                    data["preauditId"] = '${rfqPreaudit.paId}';
                } else {
                    //doNothing
                }
                data["supplierCode"] = '${sysUserVo.supplierCode}'
                var url = ctx + "/win/saveUpload";
                $.ajax({
                    url: url,// 跳转到 action
                    type: 'post',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.rspmsg == 'true') {
                            showSuccessMsg("文件保存成功");
                            updateFileList(requestid);

                        } else {
                            showErrorMsg("文件保存失败");
                        }
                    },
                    error: function (data) {
                        showErrorMsg("文件保存失败");
                    }
                });

            }
        });
    });

    /*
     刷新文件列表
     */
    function updateFileList(id) {
        var url = ctx + "/win/updateFileList";
        var data = {id: id};
        $.ajax({
            url: url,// 跳转到 action
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                var s = '';
                $.each(data, function (i, item) {
                    s += '<tr data-clone="orginal">';
                    if (item.seq != undefined && item.seq != 'null') {
                        s += "<td>" + item.seq + "</td>";
                    } else {
                        s += "<td></td>";
                    }
                    if (item.originalFilename != undefined && item.originalFilename != 'null') {
                        s += '<td data-order="true">' + item.originalFilename + '</td>';
                    } else {
                        s += "<td></td>";
                    }
                    if (item.fileSize != undefined && item.fileSize != 'null') {
                        s += '<td>' + item.fileSize + 'KB</td>';
                    } else {
                        s += "<td></td>";
                    }
                    if (item.fileDeclaration != undefined && item.fileDeclaration != 'null') {
                        s += '<td>' + item.fileDeclaration + '</td>';
                    } else {
                        s += "<td></td>";
                    }
                    if (type!=10){
                        s += ' <td><i class="icon icon-trash red ml12"></i>&nbsp; <a id="delete" class="order-del-btn"  href="javascript:deleteFile(' + item.attachmentId + ');">删除</a></td>';
                    }else {
                        s += ' <td><i class="icon icon-download-alt green"></i>&nbsp;<a id="downLoad" href=  \''+ downloadUrl+ item.downloadFilename + '\'>下载</a></td>';
                 }
//                    s += ' <td><i class="icon icon-download-alt green"></i>&nbsp;<a id="downLoad" href="' + item.downloadUrl + '">下载</a><i class="icon icon-trash red ml12"></i>&nbsp; <a id="delete" class="order-del-btn"  href="javascript:deleteFile(' + item.attachmentId + ');">删除</a></td>';
                    s += '</tr>';

                });
                $("#filelsit").html("");
                $("#filelsit").html(s);
            }
        });
    }
    /*
     删除附件
     */
    function deleteFile(attachmentId) {
        var url = ctx + "/rfqPreauditSupplier/deleteUpload";
        var data = {attachmentId: attachmentId};
        var requestid = '${basevo.id}';
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                if (data.rspmsg == 'true') {
                    showSuccessMsg("文件删除成功");
                    updateFileList(requestid);
                } else {
                    showErrorMsg("文件删除失败");
                }
            },
            error: function (data) {
                showErrorMsg("文件删除失败");
            }
        });
    }

    function downLoad(item) {

        //alert(item)
        $.zui.messager.show(item, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});


    }

    function showSuccessMsg(message) {
        new $.zui.Messager(message, {
            type: 'success' // 定义颜色主题
        }).show()
    }
    function showErrorMsg(message) {
        new $.zui.Messager(message, {
            type: 'danger' // 定义颜色主题
        }).show()
    }

    /* 字符串长度验证*/
    function checkLen(str) {
        var newStr = str.split('&lt;').join('<').split('&gt;').join('>').replace(/\s+/g, "");
        var myLen = newStr.length;
        if (myLen > 2000) {
            return true;
        }
        else {
            return false;
        }
        ;
    }
    ;
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/myquotation/myquotationList.js?v=${version}"></script>
<script>
    //明细清单Excel flash上传
    $(document).ready(function (){
        var typesss=[".xls",".xlsx"];
        var maxSize=104857600;//100M，单位:B
        var requestId='${rfqRequestVo.id}';
        var money='${designeVo.subtotalTaxed}';
        if(navigator.appName == "Microsoft Internet Explorer"){
            $("#cancelPrice33").on('show.zui.modal', function () {
                //初始化flash插件
                initPlupload2mx(maxSize,typesss,requestId,money);
                //关闭时清理页面
                initEventse();
            });
        }else {
            //初始化flash插件
            initPluploadmx(maxSize,typesss,requestId,money);
            //关闭时清理页面
            initEventse();
        }
    });
</script>