<%--
  Created by IntelliJ IDEA.
  User: yinchuanbao
  Date: 2016/9/23
  Time: 10:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>报名审批历史</title>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>
    <c:import url="../common/jqGridBootstrap.jsp"/>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/preauditSupplierAudit/preauditSupplierAuditHistory.js?v=${version}"></script>
</head>

<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">报名审批历史</li>
                    </ol>
                </div>
                <div class="page-content">



                    <div class="tab-content">

                        <table class="table table-bordered align-md" id="jqGrid">

                        </table>
                        <div class="col-md-12" id="jqGridPager">
                        <%--<div class=" pull-right" style="margin-top: -18px;">

                        </div>--%>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<input type="hidden" value="${unifiedRfqNum}" id="unifiedRfqNum">
<input type="hidden" value="${supplierCode}" id="supplierCode">



<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<%--<script src="dist/js/jquery.js"></script>--%>
<!-- ZUI Javascript组件 -->
<%--<script src="dist/js/zui.min.js"></script>--%>
<!--选择时间和日期-->
<%--<script src="dist/lib/datetimepicker/datetimepicker.min.js"></script>--%>
<script>

    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });
</script>
</body>

</html>

