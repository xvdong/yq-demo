<%--
  Created by IntelliJ IDEA.
  User: cbyin
  Date: 2016/9/23
  Time: 14:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${preauditSupplierhistory.supplierName}报名详情</title>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>
    <c:import url="../common/jqGridBootstrap.jsp"/>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
</head>

<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">${preauditSupplierhistory.supplierName}报名详情</li>

                    </ol>
                </div>
                <div class="page-content">


                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名要求</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">注册资本：</label><span class="bler blu-85">${preaudit.regcapital}万元</span>
                                        <span class="zt-ico-w">

                                            <c:choose>
                                                <c:when test="${preauditSupplierhistory.status == '0'}">

                                                      <img src="${pageContext.request.contextPath}/images/dsh.png">
                                                </c:when>
                                                <c:when test="${preauditSupplierhistory.status == '1'}">
                                                      <img src="${pageContext.request.contextPath}/images/ytg.png">
                                                </c:when>
                                                <c:when test="${preauditSupplierhistory.status == '2'}">
                                                      <img src="${pageContext.request.contextPath}/images/ybh.png">
                                                </c:when>
                                                <c:otherwise>
                                                      <img src="${pageContext.request.contextPath}/images/wbm.png">
                                                </c:otherwise>
                                            </c:choose>



                                        </span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">资质要求：</label>
                                        <span class="bler blu-85">${preaudit.qualifications}</span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">报名要求：</label><span class="bler blu-85">
                                        ${preaudit.requirementDesc}
                                     </span>
                                    </div>
                                    <div>
                                        <span class="left line-32" style="font-weight:bold; color:#333;">附件清单：</span>
                                    </div>
                                    <table class="table table-bordered align-md" style="margin-bottom:5px; width:98%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>文件名称</th>
                                            <th width="100px">文件大小</th>
                                            <th>说明</th>
                                            <th width="100px">操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${rfqAttachmentsPreaudit}" var="item">
                                            <tr data-clone="orginal">
                                                <td>${item.seq}</td>
                                                <td data-order="true">${item.originalFilename}</td>
                                                <td>${item.fileSize}B</td>
                                                <td>${item.fileDeclaration}</td>
                                                <td><a href="${pageContext.request.contextPath}/rfqAttachments/fileDownload?id=${item.attachmentId}&file=${item.downloadFilename}"><i
                                                        class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>


                            </div>

                        </div>
                    </div>



                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">供应商信息：</span>

                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">注册资本：</label><span class="bler blu-85">${preauditSupplierhistory.regcapital}万元</span>
                                        <span class="zt-ico-w">
                                             <c:if test="${preauditSupplierhistory.status == '1'}">
                                                 <img src="${pageContext.request.contextPath}/images/hggys.png">
                                             </c:if>
                                        </span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">拥有资质：</label>
                                        <span class="bler blu-85">${preauditSupplierhistory.qualifications}</span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">报名阐述：</label><span class="bler blu-85">
                                        ${preauditSupplierhistory.responseDesc}
                                     </span>
                                    </div>
                                    <div>
                                        <span class="left line-32" style="font-weight:bold; color:#333;">附件清单：</span>
                                    </div>
                                    <table class="table table-bordered align-md" style="margin-bottom:30px; width:98%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>文件名称</th>
                                            <th width="100px">文件大小</th>
                                            <th>说明</th>
                                            <th width="100px">操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${not empty rfqAttachmentsSupplier}">
                                        <c:forEach items="${rfqAttachmentsSupplier}" var="item">
                                            <tr data-clone="orginal">
                                                <td>${item.seq}</td>
                                                <td data-order="true">${item.originalFilename}</td>
                                                <td>${item.fileSize}B</td>
                                                <td>${item.fileDeclaration}</td>
                                                <td><a href="${pageContext.request.contextPath}/rfqAttachments/fileDownload?id=${item.attachmentId}&file=${item.downloadFilename}"><i
                                                        class="icon icon-download-alt green"></i>&nbsp;下载</a></td>
                                            </tr>
                                        </c:forEach>
                                        </c:if>
                                        </tbody>
                                    </table>
                                    <div class="clearfix">
                                        <label class="bler blu-8" style="color:#333;">联系方式：</label>
                                        <span class="bler blu-85">


                                        <span class="col-xs-3">联系人：${preauditSupplierhistory.linkmanName}</span>
                                        <span class="col-xs-3">固定电话：${preauditSupplierhistory.linkmanTel}</span>
                                        <span class="col-xs-3">移动电话：${preauditSupplierhistory.linkmanTelphone}</span>
                                        <span class="col-xs-3">报名时间：${preauditSupplierhistory.answerTime}</span>


                                        </span>
                                        <span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/hggys.png"></span>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>


                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">审批意见</span>
                            <span class="pull-right">
                                   <span onclick="auditHistory()" class="btn btn-sm btn-info">审批历史</span>
                                </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    ${preauditSupplierhistory.pasDesc}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
</div>









<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<%--<script src="dist/js/jquery.js"></script>--%>
<!-- ZUI Javascript组件 -->
<%--<script src="dist/js/zui.min.js"></script>--%>
<!--选择时间和日期-->
<%--<script src="dist/lib/datetimepicker/datetimepicker.min.js"></script>--%>
<script>

    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });
</script>
<!--富文本JS-->
<%--<script src="dist/lib/kindeditor/kindeditor-all-min.js"></script>
<script src="dist/lib/kindeditor/lang/zh_CN.js"></script>--%>
<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });
</script>
<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple2', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });


    function auditHistory(){
        var aa = window.open();
        var url=ctx+"/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${rfqRequestVo.unifiedRfqNum}";
        aa.location.href = url ;
    }


    /*
     刷新文件列表
     */
    function updateFileList(id){
        var url=ctx+"/rfqPreauditSupplier/updateFileList";
        var data={id : id};
        $.ajax({
            url:url,// 跳转到 action
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                var s = '';
                $.each(data,function(i,item){
                    s+='<tr data-clone="orginal">';
                    if(item.seq!=undefined && item.seq!='null'){
                        s+="<td>"+item.seq+"</td>";
                    }else{
                        s+="<td></td>";
                    }
                    if(item.originalFilename != undefined && item.originalFilename != 'null') {
                        s += '<td data-order="true">' + item.originalFilename + '</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileSize != undefined && item.fileSize != 'null') {
                        s+='<td>'+item.fileSize+'B</td>';
                    }else{
                        s+="<td></td>";
                    }
                    if(item.fileDeclaration != undefined && item.fileDeclaration != 'null') {
                        s+='<td>'+item.fileDeclaration+'</td>';
                    }else{
                        s+="<td></td>";
                    }
                    s+='<td><a href="javascript:deleteFile('+item.attachmentId+');"><i class="red icon icon-trash"></i>&nbsp;删除</a></td>';
                    s+='</tr>';
                });
                $("#filelsit").html("");
                $("#filelsit").html(s);
            }
        });
    }
    /*
     删除附件
     */
    function deleteFile(attachmentId){
        var url=ctx+"/rfqPreauditSupplier/deleteUpload";
        var data={attachmentId : attachmentId};
        var requestid = '${basevo.id}';
        $.ajax({
            url:url,
            type: 'post',
            data: data,
            cache: false,
            dataType: 'json',
            success: function (data) {
                if(data.rspmsg=='true'){
                    showSuccessMsg("文件删除成功");
                    updateFileList(requestid);
                }else{
                    showErrorMsg("文件删除失败");
                }
            },
            error:function (data) {
                showErrorMsg("文件删除失败");
            }
        });
    }
</script>
<!-- 公共JS组件 -->
<%--<script src="dist/js/base.js"></script>--%>
</body>

</html>

