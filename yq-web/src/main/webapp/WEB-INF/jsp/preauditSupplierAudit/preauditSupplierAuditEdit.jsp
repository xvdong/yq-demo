<%--
  Created by IntelliJ IDEA.
  User: yinchuanbao
  Date: 2016/9/18
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${preauditSupplier.supplierName}报名详情</title>
    <c:import url="../common/headBase.jsp"/>
    <c:import url="../common/kingEditor.jsp"/>
    <c:import url="../common/jqGridBootstrap.jsp"/>
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>

</head>

<body>
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">${preauditSupplier.supplierName}报名详情</li>

                        <li class="right mr12"><span style="color:#666;">报名时间：${preauditSupplier.answerTime}</span></li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">报名要求</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">注册资本：</label><span class="bler blu-85">${preaudit.regcapital}万元</span>
                                        <span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/wsh.png"></span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">资质要求：</label>
                                        <span class="bler blu-85">${preaudit.qualifications}</span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">报名要求：</label><span class="bler blu-85">
                                        ${preaudit.requirementDesc}
                                     </span>
                                    </div>
                                    <div>
                                        <span class="left line-32" style="font-weight:bold; color:#666;">附件清单：</span>
                                    </div>
                                    <table class="table table-bordered align-md" style="margin-bottom:5px; width:98%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>文件名称</th>
                                            <th width="100px">文件大小</th>
                                            <th>说明</th>
<%--<p:permission requestNo="${rfqRequestVo.unifiedRfqNum}" privilege="2" >--%><th width="100px">操作</th><%--</p:permission>--%>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${rfqAttachmentsPreaudit}" var="item">
                                            <tr data-clone="orginal">
                                                <td>${item.seq}</td>
                                                <td data-order="true">${item.originalFilename}</td>
                                                <td>${item.fileSize}B</td>
                                                <td>${item.fileDeclaration}</td>
                                                <%--<p:permission requestNo="${rfqRequestVo.unifiedRfqNum}" privilege="2" >--%><td><a href="${pageContext.request.contextPath}/rfqAttachments/fileDownload?id=${item.attachmentId}&file=${item.downloadFilename}"><i
                                                        class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">供应商信息</span>

                        </div>
                        <div class="panel-body">
                            <div class="row">

                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">注册资本：</label><span class="bler blu-85">${preauditSupplier.regcapital}万元</span>
                                        <span class="zt-ico-w">
                                            <c:if test="${preauditSupplier.status == '1'}">
                                                <img src="${pageContext.request.contextPath}/images/hggys.png">
                                            </c:if>
                                        </span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">拥有资质：</label>
                                        <span class="bler blu-85">${preauditSupplier.qualifications}</span>
                                    </div>
                                    <div class="clearfix"><label class="bler blu-8">报名阐述：</label><span class="bler blu-85">
                                               ${preauditSupplier.responseDesc}
                                     </span>
                                    </div>
                                    <div>
                                        <span class="left line-32" style="font-weight:bold; color:#333;">附件清单：</span>
                                    </div>
                                    <table class="table table-bordered align-md" style="margin-bottom:30px; width:98%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>文件名称</th>
                                            <th width="100px">文件大小</th>
                                            <th>说明</th>
<%--<p:permission requestNo="${rfqRequestVo.unifiedRfqNum}" privilege="2" >--%><th width="100px">操作</th><%--</p:permission>--%>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${not empty rfqAttachmentsSupplier}">
                                        <c:forEach items="${rfqAttachmentsSupplier}" var="item">
                                            <tr data-clone="orginal">
                                                <td>${item.seq}</td>
                                                <td data-order="true">${item.originalFilename}</td>
                                                <td>${item.fileSize}B</td>
                                                <td>${item.fileDeclaration}</td>
                                                <%--<p:permission requestNo="${rfqRequestVo.unifiedRfqNum}" privilege="2" >--%><td><a href="${pageContext.request.contextPath}/rfqAttachments/fileDownload?id=${item.attachmentId}&file=${item.downloadFilename}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                            </tr>
                                        </c:forEach>
                                        </c:if>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">联系方式</span>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:30px; color:#666; font-size:12px;">
                                    <span class="col-xs-3">联系人：${preauditSupplier.linkmanName}</span>
                                    <span class="col-xs-4">固定电话：${preauditSupplier.linkmanTel}</span>
                                    <span class="col-xs-3">移动电话：${preauditSupplier.linkmanTelphone}</span>

                                </div>

                            </div>

                        </div>

                    </div>
                    <div class=" " style="padding-bottom:20px;">
                        <input type="hidden" id="requestNum" name="requestNum" value="${requestNum}"/>
                        <%--<div class="required"></div>
                        <span class="line-32 font-16-y16" style="margin-left: 7px">审批意见</span>
                        <div class="form-group" >

                            <textarea  id="contentSimple" name="content" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">${preauditSupplier.pasDesc}</textarea>

                        </div>--%>
                        <hr>
                        <div class="text-right">
<p:permission requestNo="${rfqRequestVo.unifiedRfqNum}" privilege="1" >
                                <span class="left blue"  onclick="auditHistory()" >报名审批历史</span>
</p:permission>
                            <p:permission requestNo="${rfqRequestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:submitAudit('1');<%--validate('${preauditSupplier.supplierCode}');--%>" class="btn btn-md btn-primary" type="button" >审批通过</a>
                                <a href="javascript:submitAudit('2');/*audit('2');*/" class="btn btn-md btn-warning" type="button" data-dismiss="modal">审批驳回</a>
                            </p:permission>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModa7">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">保存成功</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t"> <i class="icon-check text-primary"></i>
                    <div class="content">
                        <h4>报名提交成功！</h4>
                        <p>询单标题：物位计等备件公开采购</p>
                        <p>询单编号：2014061820</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:;">确定</a> </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModa2">
    <div class="modal-dialog " style="margin-top: 210.667px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批通过</h4>
            </div>
            <div  class="modal-body" style="overflow: hidden;">
            <div class="required"></div>
            <span class="line-32 font-16-y16" style="margin-left: 7px">审批意见</span>
            <div class="form-group" >

                <textarea  id="contentSimple" name="content" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">${preauditSupplier.pasDesc}</textarea>

            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModa3">
    <div class="modal-dialog " style="margin-top: 210.667px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">请填写供应商U代码</h4>
            </div>
            <div  class="modal-body" style="overflow: hidden;">
                <div class="required"></div>
                <span class="line-32 font-16-y16" style="margin-left: 7px">供应商U代码</span>
                <input type="text" id="qualifiedCode" value="" placeholder="例如: U65362"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary qualified" data-dismiss="modal">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<%--<script src="dist/js/jquery.js"></script>--%>
<!-- ZUI Javascript组件 -->
<%--<script src="dist/js/zui.min.js"></script>--%>
<!--富文本JS-->
<%--<script src="dist/lib/kindeditor/kindeditor-all-min.js"></script>
<script src="dist/lib/kindeditor/lang/zh_CN.js"></script>--%>
<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });
    function validate(supplierCode){
        var url=ctx+"/rfqRequestState/validate";
        $.ajax({
            url: url,// 跳转到 action
            data:"id="+supplierCode+"&requestNum=${requestNum}",
            type: 'post',
            cache: false,
            async:false,
            dataType: 'json',
            success: function (data) {
                 if(data.status=="failed"){
                     audit('1')
                 }else if(data.status=="success"){
                     $.zui.messager.show("该供应商已通过审核", {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                 }
            },
            error: function () {
                var alertMsg = ''
                $("#alertMsg").html(alertMsg);
            }
        });
    }
function audit(auditstatus){
    var url = ctx+"/rfqPreauditSupplier/audit";
    var contentSimple = editor.text();
    if(contentSimple==undefined||contentSimple==''){
        $.zui.messager.show('审批意见不可为空', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
    }else if(checkLen(contentSimple)){
        $.zui.messager.show('审批意见长度超过2000字数限制！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
    }else{
        $.ajax({
            url: url,// 跳转到 action
            data: "status="+auditstatus+"&pasId=${preauditSupplier.pasId}&pasDesc="+contentSimple+"&supplierCode=${preauditSupplier.supplierCode}",
            type: 'post',
            cache: false,
            async:false,
            dataType: 'json',
            success: function (data) {
                if (data.rspcod == "0000") {
                    $.zui.messager.show('审批成功', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                    setTimeout(function () {
                        window.opener.location.href=window.opener.location.href
                        window.close();
                    },1000)

                } else if (data.rspcod == "0001"){
                    $.zui.messager.show(data.rspmsg, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                } else if (data.rspcod == "0002") {
                    $("#myModa3").modal('show');
                    $("#qualifiedCode").val("");
                }
            },
            error: function () {
                var alertMsg = ''
                $("#alertMsg").html(alertMsg);
            }
        });
    }
}

    /* 字符串长度验证*/
    function checkLen(str){
        /* 最长2000个字符 */
        len = 2000;
        var myLen = str.length;
        if (myLen > len ) {
            return true;
        }else {
            return false;
        }
    }
   function auditHistory(){
        var aa = window.open();
        var url=ctx+"/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${rfqRequestVo.unifiedRfqNum}";
        aa.location.href = url;
    }
    var $myModa2 = $('#myModa2');
    var supplierCode = '${preauditSupplier.supplierCode}';
    $myModa2.on('click','.btn-primary',function() {
        var auditstatus = $myModa2.data("_auditstatus");
        if(auditstatus=='1'){
            validate(supplierCode);
        }else if(auditstatus=='2'){
            audit('2');
        }
    });

    function submitAudit(auditstatus) {
        $myModa2.data("_auditstatus",auditstatus);
        var supplierCode = '${preauditSupplier.supplierCode}';
        var desc = '${preauditSupplier.pasDesc}';
        if(auditstatus=='1'){
            editor.text('欢迎参加，请积极报价！');
            $myModa2.find('.modal-title').html('审批通过');
        }else if(auditstatus=='2'){
            editor.text('条件不符，驳回！');
            $myModa2.find('.modal-title').html('审批驳回');
        }



        $myModa2.modal('show');
//    $myModa2.modal.show();
    }

    $(".qualified").bind("click",function(){
        var url = ctx+"/rfqPreauditSupplier/qualified";
        var qualifiedCode = $("#qualifiedCode").val();
        if (qualifiedCode == undefined|| qualifiedCode == '') {
            $.zui.messager.show('供应商U代码不可为空', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        } else {
            $.ajax({
                url: url,// 跳转到 action
                data: "bspCompanyCode="+qualifiedCode+"&supplierCode=${preauditSupplier.supplierCode}",
                type: 'post',
                cache: false,
                async:false,
                dataType: 'json',
                success: function (data) {
                    if (data.rspcod == "1") {
                        $.zui.messager.show('合格供应商添加成功', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                        audit("1");
                    } else {
                        $.zui.messager.show("合格供应商添加失败", {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                    }
                },
                error: function () {
                    $.zui.messager.show("合格供应商添加失败", {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            });

        }
    });

</script>
<!-- 公共JS组件 -->
<%--<script src="dist/js/base.js"></script>--%>
</body>
</html>
