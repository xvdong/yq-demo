<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/10/8
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>条款库维护</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js?v=${version}"></script>
</head>

<body>

<c:import url="../common/top.jsp" />
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
<div class="wrapper">

    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">

            <div class="container">

                <div class="breadcrumbs">

                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">条款库维护</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <form  method="post" id="searchFormTab1" onsubmit="return false;">
                                        <div class="so-form">
                                            <div class="left mr12 line-32">条款名称：</div>
                                              <div class="so-form-20 padding1">
                                                        <input class="form-control so-form-25 input" name="title" id="title" placeholder="请输入条款名称" type="text">
                                              </div>
                                                    <div class="ml12 left ml12 line-32">条款内容：</div>
                                                    <div class="so-form-20 padding1">
                                                        <input class="form-control input" name="content" id="content" placeholder="请填写条款内容" type="text">
                                                    </div>
                                                    <div class="so-form-20 padding1 ml12">
                                                        <select name="type" id="type" class="form-control input-md">
                                                            <option style="align: center" value="">--请选择--</option>
                                                            <option value="BL">商务条款</option>
                                                            <option value="TL">技术条款</option>
                                                        </select>
                                                    </div>
                                                    <div class="so-form-15 ml35">
                                                        <button class="btn btn-primary" id="btnSearch" type="button"><i class="icon icon-search"></i> 搜索</button>
                                                        <input class="btn ml5" type="reset" value="重置">
                                                    </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <table id="jqGrid"></table>
                            <div id="jqGridPager"></div>
                            <div class="title_main" style="padding-top: 17px">
                                <button class="btn btn-primary btn-md" id="addRfqTrems" onclick="add()" type="button"  data-toggle="modal" data-target="#myModa2"><i class="icon icon-plus"></i> 新增</button>
                                <input type="hidden" id="hid"/>
                                <button class="btn btn-danger btn-md" type="button" id="delCtmAgree" onclick="deleteData()"><i class="icon icon-warning-sign"></i>批量删除</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <!-- ///////////////////////条款新增/修改/////////////// -->
            <div class="modal fade"  id="myModa2" >
                <div class="modal-dialog modal-md">
                    <form  method="post" class="form-horizontal" id="modalFrom" role="form" onsubmit="return false;">
                     <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                            <h4 class="modal-title" id="fromTitle"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row clearfix" style="margin:5px 0px 10px 20px">
                                <div class="so-form-8"><strong>条款名称：</strong></div>
                                <div class="left">
                                    <input class="form-control" id="termsTitle" name="title"  placeholder="条款名称" type="text" value="" style="width:200px">
                                </div>
                                <div class="clearfix" id="remain">
                                    <input type='hidden'  id='termId' name='termId' class="form-control" value="0"/>
                             </div>
                                <div class="so-form-8"><strong>条款类型：</strong></div>
                                <div class="left">
                                    <select name="type" id="type-test" class="form-control input-md" style="width:200px">
                                        <option>请选择</option>
                                        <option value="BL">商务条款</option>
                                        <option value="TL">技术条款</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="so-form-8"><strong>条款内容：</strong></div>
                                <div class="so-form-15 padding1">
                                    <textarea id="contentSimple" name="content"   class= "form-control kindeditorSimple col-md-10"  style=" width:70%;height:150px;">${termsVo.content}</textarea>
                                </div>
                                <div class="clearfix"></div>
                                <div class="so-form-8" style="margin-top: 10px"><strong>&nbsp;备&nbsp;&nbsp;&nbsp;注：</strong></div>
                                <div class="so-form-15 padding1" style="margin-top: 10px">
                                    <textarea id="contentRemark" name="remark"   class= "form-control kindeditorSimple col-md-10"  style=" width:70%;height:100px;">${termsVo.remark}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="enter">确定</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                    </form>
                </div>
            </div>
            <!-- id2结束 -->
</body>
<script src="${pageContext.request.contextPath}/lib/kindeditor/kindeditor-all-min.js"></script>
<script src="${pageContext.request.contextPath}/lib/kindeditor/lang/zh_CN.js"></script>
<%--<script>
    var editor;
    KindEditor.ready(function (K) {
        editor = K.create('textarea#contentSimple', {
            resizeType: 1,
            allowPreviewEmoticons: false,
            allowImageUpload: false,
            items: []
        });
    });
</script>--%>
<script src="${pageContext.request.contextPath}/js/rfqTerms/rfqTremsList.js?v=${version}"></script>
</html>

