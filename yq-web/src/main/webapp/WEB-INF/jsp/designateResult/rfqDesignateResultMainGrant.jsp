<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>待授标</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqLaunchedSeveralPrice/rfqLaunchedSeveralPrice.js?v=${version}"></script>
    <!-- json 转换工具 -->
    <script src="${pageContext.request.contextPath}/js/rfqLaunchedSeveralPrice/json2.js?v=${version}"></script>
    <!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
    <script src="${pageContext.request.contextPath}/lib/kindeditor/kindeditor-all-min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/kindeditor/lang/zh_CN.js"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
</head>

<body>

<c:import url="../common/top.jsp" />

<script>
    var ctx = '${ctx}';
    var resource = '${resource}';
</script>

<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">授标</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">询价单摘要</span>
                            <div class=" pull-right">
<p:permission requestNo="${vo.unifiedRfqNum}" privilege="1"><a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">询单详情</span></a></p:permission>
                                <!--当前判断可能有误-->
                                <c:choose>
                                   <c:when test="${vo.publicBiddingFlag == '0' &&  vo.type == '6' }"> <p:permission requestNo="${vo.unifiedRfqNum}" privilege="2"><span class="btn btn-warning font12" data-position="center" data-toggle="modal" data-target="#supplistModal" id="superlist">追加供应商</span></p:permission></c:when>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${vo.type != '11' && vo.type != '12' }"><p:permission requestNo="${vo.unifiedRfqNum}" privilege="2" ><span class="btn btn-danger font12" data-position="center" data-toggle="modal" data-target="#cancelPrice" >撤销询价</span>        <span class="btn btn-danger font12" data-position="center"  <%--data-target="#myModa20"--%>onclick="getBill()">导出报价单</span></p:permission></c:when>
                                </c:choose>
                            </div>
                        </div>
                        <input id="dataSource" type="hidden" value="${vo.dataSource}"/>
                    <%--<input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}">--%>
                        <input type="hidden" name="id" id="id" value="${vo.id}">
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>询价单号：${vo.ouRfqNum}</li>
                                        <li>询价标题：${vo.title}</li>
                                        <li>采购单位：${vo.ouName}</li>
                                        <li>计划编号：${vo.planNo}</li>
                                        <li>当前状态：<span class="red" id="nowState" ><c:choose><c:when test="${vo.type == '3'}">待报价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6'}">报价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '8'}">待核价</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></span></li>
                                        <li>邀请范围：<span id="publicBiddingFlag"><c:choose><c:when test="${vo.publicBiddingFlag == '1'}">公开寻源</c:when><c:otherwise>定向寻源</c:otherwise></c:choose></span></li>
                                    </ul>
                                    <c:choose>
                                        <c:when test="${vo.type == '3'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbj.png"></span></c:when>
                                        <c:when test="${vo.type == '5'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bmz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bjz.png"></span></c:when>
                                        <c:when test="${vo.type == '7'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dkb.png"></span></c:when>
                                        <c:when test="${vo.type == '8'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dhj.png"></span></c:when>
                                        <c:when test="${vo.type == '11'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/yzf.png"></span></c:when>
                                        <c:when test="${vo.type == '12'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/ylb.png"></span></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span class="red"><c:choose><c:when test="${vo.assureMoney >= '0'}"><fmt:formatNumber type="number" value="${vo.assureMoney} " maxFractionDigits="0"/>元</c:when></c:choose></span></span></li>
                                        <li>报价货币：<c:choose><c:when test="${vo.currency=='CNY'}">人民币CNY</c:when><c:when test="${vo.currency=='USD'}">美元USD</c:when><c:when test="${vo.currency=='GBP'}">英镑GBP</c:when><c:when test="${vo.currency=='JPY'}">日元JPY</c:when><c:when test="${vo.currency=='EUR'}">欧元EUR</c:when><c:when test="${vo.currency=='HKD'}">港元HKD</c:when><c:when test="${vo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                                        <li>预算总价：<span class="red"><fmt:formatNumber type="number" value="${vo.totalBudget}" maxFractionDigits="0"/>元</span></li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                        <ul>
                                            <c:if test="${rfqRulesVo.partialProductFlag == '1' && rfqRulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                            <c:if test="${rfqRulesVo.partialProductFlag != '1' && rfqRulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                            <c:if test="${rfqRulesVo.partialProductFlag == '1' && rfqRulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                            <c:if test="${rfqRulesVo.partialProductFlag != '1' && rfqRulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                        </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：<c:choose><c:when test="${fn:length(vo.issueDate)>'16'}">${fn:substring(vo.issueDate,0,16)}</c:when> <c:otherwise>${vo.issueDate}</c:otherwise></c:choose></li>
                                        <li>报价开始时间：<span id ="startDate"><c:choose><c:when test="${fn:length(vo.startDate)>'16'}">${fn:substring(vo.startDate,0,16)}</c:when> <c:otherwise>${vo.startDate}</c:otherwise></c:choose></span></li>
                                        <li>报价截止时间：<span id ="quotationEndDate"><c:choose><c:when test="${fn:length(vo.quotationEndDate)>'16'}">${fn:substring(vo.quotationEndDate,0,16)}</c:when> <c:otherwise>${vo.quotationEndDate}</c:otherwise></c:choose></span></li>
                                        <c:choose>
                                            <c:when test="${vo.publicBiddingFlag == '1'}">
                                                <li>报名截止时间：<span id ="registrationEndTime"><c:choose><c:when test="${fn:length(vo.registrationEndTime)>'16'}">${fn:substring(vo.registrationEndTime,0,16)}</c:when><c:otherwise>${vo.registrationEndTime}</c:otherwise></c:choose></span></li>
                                            </c:when>
                                        </c:choose>
                                    </ul>
                                    <%--<input type="hidden" name="registrationEndTime" id="registrationEndTime" value="${vo.registrationEndTime}">--%>
                                    <%--<input type="hidden" name="startDate" id="startDate" value="${vo.startDate}">--%>
                                    <%--<input type="hidden" name="quotationEndDate" id="quotationEndDate" value="${vo.quotationEndDate}">--%>
                                    <c:choose>
                                        <c:when test="${vo.type == '3' || vo.type == '5'  || vo.type == '6' }"><p:permission  privilege="2" requestNo="${vo.unifiedRfqNum}"><a class="btn btn-info tz-time-btn" data-position="center" data-toggle="modal" data-target="#adjustTime" onclick="checkTime ();"><i class="icon icon-time"></i>&nbsp;调整时间</a></p:permission></c:when>
                                    </c:choose>
                                    <c:choose>
                                        <%--//${pageContext.request.contextPath}/designateResult/init?unifiedRfqNum=${vo.unifiedRfqNum}--%>
                                        <%--location.href = $("#contextPath").val() + "/designateResult/init?unifiedRfqNum="+$("#unifiedRfqNum").val();--%>
                                        <%--<c:when test="${vo.type == '7'}"><a href="javascript:void(0)" onclick="bidOpening(${vo.unifiedRfqNum});"><span class="btn btn-warning mt12"><i class="icon icon-circle-arrow-right"></i>&nbsp;开标</span></a></c:when>--%>
                                        <c:when test="${vo.type == '7'}"><a data-toggle="modal" data-target="#myModal2"><span class="btn btn-warning mt12"><i class="icon icon-circle-arrow-right"></i>&nbsp;开标</span></a></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${vo.linkmanName}</li>
                                        <li>联系电话：${vo.linkmanTelphone}</li>
                                        <li style="width:40%;">交货地址：${vo.deliveryProvince}${vo.deliveryCity}${vo.deliveryArea}${vo.deliveryAddress}</li>
                                    </ul>
                                </div>
                                <div class="bh-panel-footer" style="margin-bottom:-15px;">
                                    <div class="mg-links">
                                        <p:permission  privilege="1" requestNo="${vo.unifiedRfqNum}">
                                        <a href="${pageContext.request.contextPath}/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${vo.unifiedRfqNum}" target="hejiafabu-baomingshenpilishi">报名历史</a>
                                        <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${vo.id}" target="gysbj-baomingshenpilishi">审批记录</a>
                                        <a href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?id=${vo.id}" target="hjsp-quotation_history_list">报价历史</a>
                                        <a href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${vo.unifiedRfqNum}" target="gysbj-caozuorizhi">操作日志</a>
                                        </p:permission>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="hj-tb">
                        <div class="hj-tb-xd" style="z-index:2;">
                            <div class="panel mb18">
                                <div class="panel-body clearfix">
                                    <span class="mg-title-16">核价明细：</span>
                                    <c:choose>
                                        <c:when test="${supplierCount > 0 }">
                                            <span class="mg-sand mr12">未报价供应商：<a href="${pageContext.request.contextPath}/designateResult/querySupplierNotQuotedPricePage?type=1&unifiedRfqNum=${vo.unifiedRfqNum}" class="red" target="_blank">${supplierCount}</a>家</span>
                                        </c:when>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${identicalCount > 0}">
                                            <span class="mg-sand">IP地址相同供应商：<a href="${pageContext.request.contextPath}/designateResult/queryIPIdenticalPage?type=2&unifiedRfqNum=${vo.unifiedRfqNum}" class="red" target="_blank">${identicalCount}</a>家</span>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </div>
                            <div class="hjmx-th">
                                <div class="hjmx-th-gd">
                                    <span>采购信息</span><span style="font-weight:normal;">（拟签总价：<strong class="red">0.00</strong>元）</span>

                                    <div style="margin-left:-1px;">
                                        <table class="table table-bordered align-md" style="border:0">
                                            <tr>
                                                <th width="60">序号</th>
                                                <th>物料信息 <a class="btn btn-danger btn-mini" id="all-zk" href="javascript:;">全部展开</a></th>
                                                <th width="80">采购数量</th>
                                                <th width="100">最低报价<br>参考单价</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="hjmx-th-fd">
                                    <div class="re" id="div3">
                                        <div class="scrollDiv">
                                            <!-- <div>
                                                <div class="t">
                                                    <%--<h4><a href="javascript:;" class="blue"><img src="dist/images/load.gif"></a></h4>--%>
                                                    <span class="label label-warning">报</span> <a class="red" href="myquotationView.html">0.00</a> 元
                                                    <span class="green">附</span> <span class="red">注</span>
                                                    <span class="ml12">拟签小计：<strong class="red">0.00</strong>元</span>
                                                </div>
                                                <table class="table table-bordered align-md ">
                                                    <tr>
                                                        <th width="">可供数量<br>拟签数量</th>
                                                        <th width="135">含税报价单价(元)<br>含税拟签单价(元)</th>
                                                        <th  width="80">税率</th>
                                                    </tr>
                                                </table>
                                            </div> -->


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hjmx-tbody">
                            <div class="hjmx-tbody-gd">

                                <table class="table table-bordered table-striped align-md">
                                    <tbody>
                                    <!-- <tr>
                                        <td width="35">1</td>
                                        <td>
                                            <div class="zkDiv">
                                                XXX
                                                <a href="#" class="btn btn-mini hjBtn btn-warning" data-toggle="modal" data-target="#myModa3">按行核价</a>
                                                <a href="javascript:;" class="zkBtn icon icon-chevron-down"></a>

                                                <div class="hidden">
                                                    更多说明更多说明更多说明更多说明更多说明更多说明更多说明
                                                </div>

                                            </div>

                                        </td>
                                        <td width="80">0个</td>
                                        <td width="100">0.00</td>
                                    </tr> -->



                                    </tbody>
                                </table>
                            </div>

                            <div class="hjmx-tbody-fd">
                                <div class="re" id="div2">
                                    <div class="scrollDiv" id="scrollDiv">
                                        <!-- <table class="table table-bordered table-striped align-md" >

                                            <tbody>
                                                <tr>
                                                    <td>1.5<div class="col-xs-12"><input type="text" class="form-control input-sm"></div></td>
                                                    <td width="135">0.00<div class="col-xs-12"><input type="text" class="form-control input-sm"></div></td>
                                                    <td width="80">0%</td>
                                                </tr>
                                            </tbody>
                                        </table> -->



                                    </div>
                                </div>

                            </div>

                            <div id="bar-row">
                                <div id="bar"></div>
                            </div>
                        </div>
                    </div>
                    <!--  分页 -->
                    <div class="so-form-60 right mt20">
                        <div class="pull-right" style="margin-top:-18px;">
                            <ul class="pager pager-loose pagers">
                                <li><span>共 <strong class="text-danger" id="total"></strong> 条记录 </span></li>
                                <li><span><strong class="text-danger" id="pageNum"></strong></strong><strong class="text-danger">/</strong><strong id="pages" class="text-danger"></strong> 页</span></li>
                                <li><span>每页显示
                                  <select name="" class="form-control input-sm" id="pageSize">
                                      <option selected="selected">20</option>
                                      <option>50</option>
                                      <option>100</option>
                                  </select>
                                  条</span></li>
                                <li id="homePage"><a class="gray">首页</a></li>
                                <li id="lastPage"><a class="gray">上一页</a></li>
                                <li id="nextPage"><a class="gray">下一页</a></li>
                                <li id="endPage"><a class="gray">尾页</a></li>
                                <li><span>转到
                                  <input type="text" class="form-control input-sm" size="3" id="pageNo" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
                                  页</span> <span class="ml12"><a class="btn btn-sm" id="toPage">确定</a></span></li>
                            </ul>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="mt12">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <div class="required"></div>
                                <span class="mg-margin-right" style="margin-left: 10px">核价说明</span>
                            </div>
                            <div class="panel-body">
                                <textarea id="resultMemo" name="content" class="form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">${vo.resultMemo}</textarea>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="text-right">
                        <%--<a class="blue mr12" href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/init?unifiedRfqNum=${vo.unifiedRfqNum}">发起多轮报价</a>--%>
                      <p:permission requestNo="${vo.unifiedRfqNum}" privilege="2" >
                          <a href="javascript:;" class="btn btn-lg btn-success sbBtn" type="button" id="resultMemoSave">保存</a>
                          <a href="javascript:void(0)" class="btn btn-lg btn-primary" type="button" onclick="submit_modal_func()"   >提交</a>
                      </p:permission>
                        <a href="${pageContext.request.contextPath}/designateResult/init?unifiedRfqNum=${vo.unifiedRfqNum}" class="btn btn-lg " type="button">返回</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- 即时聊天
 <a href="#" id="im">
     <i class="icon icon-comments-alt"></i>即<br>时<br>聊<br>天
 </a>      -->

<!-- 供应商筛选提示框-->
<div class="hidentable" style="display:none">
    <table class="table table-bordered align-md" id="supplierList" style="width:480px;">
        <thead>
        <tr>
            <th><input type="checkbox" id="supplierSelTh"></th>
            <th>供应商</th>
            <th>报价总价</th>
            <th>报价条数</th>
            <th>最低价条数</th>
            <th>附件</th>
            <th>备注</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th><input type="checkbox"></th>
            <td>北京科技有限公司北京科技有限公司</td>
            <td><a class="red" href="myquotationView.html">50000</a></td>
            <td>100</td>
            <td>120</td>
            <td><a class="icon icon-check green"></a></td>
            <td><a class="icon icon-times red2"></a></td>
        </tr>
        <tr>
            <th><input type="checkbox"></th>
            <td>北京科技有限公司北京科技有限公司</td>
            <td><a class="red" href="myquotationView.html">50000</a></td>
            <td>100</td>
            <td>120</td>
            <td><a class="icon icon-check green"></a></td>
            <td><a class="icon icon-times red2"></a></td>
        </tr>
        </tbody>
    </table>
    <div class="text-right mt12">
        <a href="javascript:supplierSubmitFunc();" id="supplierSubmit" class="btn btn-primary" type="button">确定</a>
        <a href="javascript:$('#gyssx').trigger('click');" class="btn" type="button">取消</a>
    </div>
</div>

<!-- 价格筛选工具-->
<div class="hidentable2" style="display:none">
    <div class="hidentable2Div">
        <ul class="checkList">
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <h4>1、按行最低价核价</h4>
                <p>说明：该模式下系统自动算出每行物料报价最低的供应商，可支持将每行物料报价最低供应商以红色标记显示。</p>
            </li>
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <h4>2、按行最低价核价</h4>
                <p>说明：该模式下系统自动算出每行物料报价最低报价，并可将所有物料的最低报价指定于任何一家供应商。</p>
            </li>
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <h4>3、按行报价总价最低价核价</h4>
                <p>说明：该模式下系统自动算出物料报价总价最低的供应商。</p>
            </li>
        </ul>
        <div class="text-right mt12">
            <a href="javascript:;" class="btn btn-primary" type="button" onClick="javascipt:soubiao($(this))">确定</a>
            <a href="javascript:$('#jgsx').trigger('click');" class="btn" type="button">取消</a>
        </div>
    </div>
</div>

<!-- 导出核价模板-->
<div class="hidentable3" style="display:none; margin-right:30px;">
    <div class="hidentable3Div">
        <ul class="checkList">
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <p>导出物料核价模板</p>
            </li>
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <p>导出供应商核价模板</p>
            </li>
        </ul>
        <div class="mt12 text-center">
            <a href="javascript:$('#mbhj').trigger('click');" class="btn btn-primary btn-mini" type="button">确定</a>
            <a href="javascript:$('#mbhj').trigger('click');" class="btn btn-mini" type="button">取消</a>
        </div>
    </div>
</div>


<!-- 导入核价模板-->
<div class="hidentable4" style="display:none; margin-right:30px;">
    <div class="hidentable3Div">
        <ul class="checkList">
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <p>导入物料核价模板</p>
            </li>
            <li onClick="selectLi($(this))">
                <i class="icon icon-check-circle"></i>
                <p>导入供应商核价模板</p>
            </li>
        </ul>
        <div class="mt12 text-center">
            <a href="javascript:$('#drmbhj').trigger('click');" class="btn btn-primary btn-mini" type="button">确定</a>
            <a href="javascript:$('#drmbhj').trigger('click');" class="btn btn-mini" type="button">取消</a>
        </div>
    </div>
</div>

<!-- 授标模态框-->
<div class="modal fade" id="myModa18">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">授标</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <h4>系统已检测到您使用了"按行最低价核价"工具，是否需要将物料授标给一家供应商？</h4>
                    <p>
                        <span class="pull-left line-32">指定授标供应商：</span>

                        <select class="form-control pull-left" id="targetSupplier" style="width:300px;">
                        </select>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:setPriceToolsByTargetSupplier();">确定</a>
                <a href="javascript:;" class="btn"  data-dismiss="modal" type="button">取消</a>
            </div>
        </div>
    </div>
</div>



<!-- 审批历史弹窗-->
<div class="modal fade" id="myModa1">
    <div class="modal-dialog modal-lg" style="width:1000px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>询单号</th>
                            <th>询单名称</th>
                            <th>创建人</th>
                            <th>审批人信息</th>
                            <th>提交时间</th>
                            <th>审批类型</th>
                            <th>审批时间</th>
                            <th>状态</th>
                            <th>审批等级</th>
                            <th>审批理由</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td>同意</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:;">确定</a> </div>
    </div>
</div>

<!-- 按单个物料行核价页面-->
<div class="modal fade" id="myModa3">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">按单个物料行核价页面</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered align-md" id="itemTable">
                    <thead>
                    <tr>
                        <th colspan="2">采购信息</th>
                        <th width="500" colspan="5">供应商报价信息</th>
                        <th colspan="2">核价结果</th>
                    </tr>
                    <tr>
                        <td width="150">物料信息</td>
                        <td width="60">采购数量</td>
                        <td width="120">供应商</td>
                        <td width="60">可供数量</td>
                        <td>含税报价单价(元)</td>
                        <td>税率</td>
                        <td>报价总价</td>
                        <td width="80">拟签数量</td>
                        <td width="80">拟签含税单价(元)</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td rowspan="4" style="vertical-align:top">
                            <div class="text-left">
                                代码：11111
                                物料名称：轴承
                                规格型号：ksksjhsdj
                                品牌：moumou
                                要求交货期：2016-7-6
                                物料长描述：描述1、描述2、描述3.
                            </div>
                        </td>
                        <td rowspan="4">12个</td>
                        <td>上海某某公司</td>
                        <td>12</td>
                        <td>166.00</td>
                        <td>17%</td>
                        <td>200.00</td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                    </tr>
                    <tr>
                        <td>上海某某公司</td>
                        <td>12</td>
                        <td>166.00</td>
                        <td>17%</td>
                        <td>200.00</td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                    </tr>
                    <tr>
                        <td>上海某某公司</td>
                        <td>12</td>
                        <td>166.00</td>
                        <td>17%</td>
                        <td>200.00</td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                    </tr>
                    <tr>
                        <td>上海某某公司</td>
                        <td>12</td>
                        <td>166.00</td>
                        <td>17%</td>
                        <td>200.00</td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                        <td><input class="form-control input-sm" type="text" placeholder=""></td>
                    </tr>
                    </tbody>
                </table>
                <div class="clearfix text-right">拟签数量总计：<span class="red" id="myModa3_v_amout_sum">0</span>套，拟签含税（或未税）总价<span class="red" id="myModa3_unit_price_sum">0.00</span>元</div>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:func_saveItemWindow();">确定</a>
                <a href="javascript:;" class="btn" type="button"  data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>


<!-- 撤销询价原因模态框-->
<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">撤销询价原因</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <p>
                    <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因类别：</span>

                        <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                            <option value="不需要采购">不需要采购</option>
                            <option value="供应商退出">供应商退出</option>
                            <option value="其他原因">其他原因</option>
                        </select>
                    </p>
                    <div class="clearfix"></div>
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因说明：</span>

                        <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>


<!-- 是否切换-->
<div class="modal fade" id="myModa9">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                您是否确认切换核价方式
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:;">确定</a>
                <a href="javascript:;" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>
<!-- 发布(结果审批入口2) -->
<div class="modal fade" id="myModa6">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">提交确认</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t ml35">
                    <i class="icon icon-question-sign text-primary"></i>
                    <div class="content ml12">
                        <h4>你确定要提交本次核价结果吗?</h4>
                        <p>询单标题：　　${vo.title}</p>
                        <p>询单编号：　　${vo.ouRfqNum}</p>
                        <p>拟签总价：　　<strong class="red" id="confirmedPrice">0.00</strong><strong class="red">元</strong></p>
                        <p>
                            <span class="left">选择审批流程：</span>
                         <span class="col-lg-6 padding1">
                             <c:choose>
                                 <c:when test="${appList.size()>0}">
                                     <select id="approvalCode" class="form-control input-sm" style="width:110px">
                                         <c:forEach var="approvalBo" items="${appList}">
                                             <option value="${approvalBo.number},${approvalBo.type}"><c:if test="${approvalBo.type == 1}">标准:</c:if><c:if test="${approvalBo.type == 2}">灵活:</c:if>${approvalBo.wfDefineName}</option>
                                         </c:forEach>
                                     </select>
                                 </c:when>
                                 <c:otherwise>
                                     <select id="approvalCode" class="form-control input-sm" style="width:110px" disabled="true">
                                         <option value="0">无有效审批</option>
                                     </select>
                                 </c:otherwise>
                             </c:choose>
                        </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> <a type="button" class="btn btn-primary" onclick="submit_func()" href="javascript:void(0);">确定提交</a>  <a type="button" class="btn" data-dismiss="modal" href="javascript:void(0);">返回修改</a></div>
        </div>
    </div>
</div>

<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo" >
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->

<input id="contextPath" type="hidden" value="${pageContext.request.contextPath}"/>
<input type="hidden" name="requestId" id="requestId" value="${vo.id}">
<input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}">

<script type="text/javascript">
    var editor_contentSimple;
    KindEditor.ready(function(K) {
        editor_contentSimple = K.create('textarea#contentSimple', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : []
        });
    });

    $(function(){
        $('#gyssx').popover({
            animation:true,
            html:true,
            tipClass:'mg-popover2',
            placement:'bottom',
            content:$('.hidentable').html(),
            title:'筛选供应商'
        });
        $('#jgsx').popover({
            animation:true,
            html:true,
            tipClass:'mg-popover1',
            placement:'bottom',
            content:function(){return $('.hidentable2').html()}
        });
        $('#mbhj').popover({
            animation:true,
            html:true,
            tipClass:'mg-popover1',
            placement:'bottom',
            content:function(){return $('.hidentable3').html()}
        });
        $('#drmbhj').popover({
            animation:true,
            html:true,
            tipClass:'mg-popover1',
            placement:'bottom',
            content:function(){return $('.hidentable4').html()}
        });


        /*吸顶*/
        $('.hj-tb-xd').setScroll({
            defaultTop:$('.page-content').find('.panel').eq(0).height()+75,
            startTop:$('.page-content').find('.panel').eq(0).height()+75
        });


    })

    function changeBar(){
        $('.hjmx-th-fd .scrollDiv>div:odd').css('background','#fff1d5');
        $('.hjmx-th-fd .scrollDiv>div:even').css('background','#ddf3f5');
        $('.scrollDiv').css('width',$('.scrollDiv>div').length*($('.scrollDiv>div').width()));
        $('.hjmx-tbody .hjmx-tbody-fd').height($('.hjmx-tbody-gd').height());

        $('.hj-tb').height($('.hjmx-tbody-gd').height()+10);
        $('#bar-row').css({
            'top':$('.hjmx-tbody-gd').height(),
            'width':$('.hjmx-tbody-fd').width()
        });
        $('.hjmx-th').width($('.hjmx-tbody').width()-2);
        $('.hjmx-tbody-gd tr').each(function(){
            var sda=$(this).index();
            var sheight=$(this).height();
            $('#scrollDiv table').each(function(){
                $(this).find('tr').eq(sda).height(sheight);
            })
        })
    }


    $('#all-zk').click(function(){
        if($(this).html()=="全部展开"){
            $('.zkBtn').each(function(){
                $(this).next().removeClass('hidden');
                $('.zkBtn').addClass('cur');
            });
            $(this).html('全部收缩');
        }else{
            $('.zkBtn').each(function(){
                $(this).next().addClass('hidden');
                $('.zkBtn').removeClass('cur');
            });
            $(this).html('全部展开');
        }
        changeBar();
    });

    function selectLi(This){
        This.hasClass('active')?This.removeClass('active').siblings().removeClass('active'):This.addClass('active').siblings().removeClass('active');
    }

    function soubiao(This){
        var v_index=This.parent().prev().find('li.active').index();
        if(v_index==1){
            setSelSupplier();
            $('#myModa18').modal();
        }else{
            var type=1;
            if(v_index==0){
                type=1;//按行核价
            }else{
                type=2;//按总价核价
            }
            setPriceTools(type);
        }
    }



    /*自定义滚动*/
    var myWeb={};
    function scroll(k){
        myWeb.scrollLeft("bar-row","div2","div3",k);
    }

    /*撤销询单*/
    function revoke(){
        if( $("#pubEndMemo").val().length == 0){
            $.zui.messager.show('请填写撤销原因！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
            return;
        }
        if($("#pubEndMemo").val().length > 2000){
            $.zui.messager.show('撤销原因不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
            return;
        }
        $.ajax({
            type: "POST",
            url:$("#contextPath").val()+'/rfqRequestState/revocationOfRequest',
            data: {
                extendField4:$("#extendField4  option:selected").val(),
                pubEndMemo:$("#pubEndMemo").val(),
                unifiedRfqNum:$("#unifiedRfqNum").val(),
                id:$("#id").val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    location.href = $("#contextPath").val() + "/rfqRequest/init";
                } else {
                    $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    }

    function getBill() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd",
            data: {id:"${vo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
</script>
<script src="${pageContext.request.contextPath}/js/addMouseWheel.js?v=${version}"></script>
<script src="${pageContext.request.contextPath}/js/designateResult/rfqDesignateResultMainGrant.js?v=${version}"></script>

<!--[if lt IE 9]>
<script type="text/javascript">
    jQuery(function($){
        $('#approvalCode').css('width','350px');
    });
</script>
<![endif]-->
</body>

</html>
