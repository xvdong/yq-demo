<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><c:choose><c:when test="${vo.type == '9'}">结果待审批</c:when><c:when test="${vo.type == '8'}">授标</c:when></c:choose></title>
    <%@include file="../common/headBase.jsp"%>
    <c:import url="../common/kingEditor.jsp"/>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${pageContext.request.contextPath}/js/echarts.common.min.js?v=${version}"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
</head>

<body>
<c:import url="../common/top.jsp"/>
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <c:choose>
                            <c:when test="${vo.type == '8'}">
                                <li class="active">待授标</li>
                            </c:when>
                            <c:when test="${vo.type == '9'}">
                                <li class="active">结果待审批</li>
                            </c:when>
                        </c:choose>
                    </ol>
                </div>
                <div class="page-content">
                    <c:if test="${auditing.applicationDesc =='结果审批'}">
                        <c:if test="${auditing.auditingDesc != ''}">
                            <div class="alert alert-danger clearfix mg-alert-danger">
                                <i class="icon icon-exclamation-sign mt3 ml5"></i>
                                <div class="pull-left ml12">
                                    <p>审批结果：已驳回</p>
                                    <p>驳回理由：${auditing.auditingDesc}</p>
                                </div>
                                <a href="#" class="close" style="font-size:26px; height:45px; line-height:45px;"
                                   data-dismiss="alert" onclick="changeHigeht(this)">×</a>
                            </div>
                        </c:if>
                    </c:if>
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价单摘要</span>
                            <div class=" pull-right">
                                <a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">竞价单详情</span></a>
                                <!--当前判断可能有误-->
                                <c:choose>
                                    <c:when test="${vo.type != '11' && vo.type != '12' }"><span class="btn btn-danger font12" data-position="center" data-toggle="modal" data-target="#cancelPrice" >撤销竞价</span></c:when>
                                </c:choose>
                            </div>
                        </div>
                        <input type="hidden" name="rfqMethod" id="rfqMethod" value="DAC"/>
                        <input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}">
                        <input type="hidden" name="requestId" id="requestId" value="${vo.id}">
                        <input type="hidden" name="supplierName" id="supplierName" value="${designeVo.supplierName}">
                        <input type="hidden" name="supplierNum" id="supplierNum" value="${designeVo.supplierNum}">
                        <input type="hidden" name="type" id="type" value="${vo.type}">
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>竞价单号：${vo.unifiedRfqNum}</li>
                                        <li>竞价标题：${vo.title}</li>
                                        <li>采购单位：${vo.ouName}</li>
                                        <li>计划编号：${vo.planNo}</li>
                                        <li>当前状态：<span class="red" id="nowState" ><c:choose><c:when test="${vo.type == '3'}">待报价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6'}">报价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '9'}">结果待审批</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '8'}">待授标</c:when><c:when test="${vo.type == '12'}">已流标</c:when><c:when test="${vo.type == '13'}">结果待发布</c:when><c:when test="${vo.type == '10'}">已结束</c:when></c:choose></span></li>
                                        <li>邀请范围：<span id="publicBiddingFlag"><c:choose><c:when test="${vo.publicBiddingFlag == '1'}">公开寻源</c:when><c:otherwise>定向寻源</c:otherwise></c:choose></span></li>
                                    </ul>
                                    <c:choose>
                                        <c:when test="${vo.type == '3'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbj.png"></span></c:when>
                                        <c:when test="${vo.type == '5'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bmz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bjz.png"></span></c:when>
                                        <c:when test="${vo.type == '7'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dkb.png"></span></c:when>
                                        <c:when test="${vo.type == '8'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dsb.png"></span></c:when>
                                        <c:when test="${vo.type == '10'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/jiesu.png"></span></c:when>
                                        <c:when test="${vo.type == '9'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dsp.png"></span></c:when>
                                        <c:when test="${vo.type == '11'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/yzf.png"></span></c:when>
                                        <c:when test="${vo.type == '12'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/ylb.png"></span></c:when>
                                        <c:when test="${vo.type == '13'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/jgdfb_03.png"></span></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span class="red"><c:choose><c:when test="${vo.assureMoney >= '1'}"><fmt:formatNumber type="number" value="${vo.assureMoney} " maxFractionDigits="0"/>元</c:when><c:otherwise>0.00元</c:otherwise></c:choose></span></span></li>
                                        <li>报价货币：<c:choose><c:when test="${vo.currency=='CNY'}">人民币CNY</c:when><c:when test="${vo.currency=='USD'}">美元USD</c:when><c:when test="${vo.currency=='GBP'}">英镑GBP</c:when><c:when test="${vo.currency=='JPY'}">日元JPY</c:when><c:when test="${vo.currency=='EUR'}">欧元EUR</c:when><c:when test="${vo.currency=='HKD'}">港元HKD</c:when><c:when test="${vo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                                        <li>参考总价：<span class="red"><fmt:formatNumber type="number" value="${vo.totalBudget}" maxFractionDigits="0"/>元</span></li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：<c:choose><c:when test="${fn:length(vo.issueDate)>'16'}">${fn:substring(vo.issueDate,0,16)}</c:when> <c:otherwise>${vo.issueDate}</c:otherwise></c:choose></li>
                                        <li>竞价开始时间：<span id ="startDate"><c:choose><c:when test="${fn:length(vo.startDate)>'16'}">${fn:substring(vo.startDate,0,16)}</c:when> <c:otherwise>${vo.startDate}</c:otherwise></c:choose></span></li>
                                        <li>竞价截止时间：<span id ="quotationEndDate"><c:choose><c:when test="${fn:length(vo.quotationEndDate)>'16'}">${fn:substring(vo.quotationEndDate,0,16)}</c:when> <c:otherwise>${vo.quotationEndDate}</c:otherwise></c:choose></span></li>
                                        <c:choose>
                                            <c:when test="${vo.publicBiddingFlag == '1'}">
                                                <li>报名截止时间：<span id ="registrationEndTime"><c:choose><c:when test="${fn:length(vo.registrationEndTime)>'16'}">${fn:substring(vo.registrationEndTime,0,16)}</c:when><c:otherwise>${vo.registrationEndTime}</c:otherwise></c:choose></span></li>
                                            </c:when>
                                        </c:choose>
                                    </ul>
                                    <c:if test="${vo.type=='13'}">
                                        <a href="javascript:void(0);" onclick="completeRelease();"><span class="btn btn-danger mt12">结果发布</span></a>
                                    </c:if>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${vo.linkmanName}</li>
                                        <li>联系电话：${vo.linkmanTelphone}</li>
                                        <li style="width:40%;">交货地址：${vo.deliveryProvince}${vo.deliveryCity}${vo.deliveryArea}${vo.deliveryAddress}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>         <div class="panel" <%--id = "completeRelease"--%>>
                                    <div class="panel-heading clearfix">
                                        <span class="mg-margin-right">${vo.title}竞价单出价</span>

                                    </div>
                                    <div class="panel-body font12">
                                        <div id="mainChart" style="width:100%;height:400px;"></div>
                                    </div>
                                </div>

                                <c:if test="${vo.type == '13'}">
                        <form id="completeRelease">
                            <div class="">
                                <input type="hidden" name="requestBo.id" value="${rfqRequestComplexVo.requestVo.id}" />
                                <div class="form-group" style="margin:0;">
                                    <div class="mg-header">中标说明：</div>
                                    <ul id="myTabZB" class="nav nav-tabs mt12">

                                    </ul>
                                    <div class="tab-content" id="tabContent">

                                    </div>
                                </div>
                            </div>
                            <div class="mg-header">未中标说明：</div>
                            <div>
                                <textarea id="contentSimple9"  name="memoDesc" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">很遗憾，您未中标！</textarea>
                            </div>
                        </form>
                    </c:if>

                                    <div class="mg-header mt20">
                                        中标情况：（中标供应商：<span style="color:red">${designeVo.supplierName}</span>&nbsp;&nbsp;&nbsp; 中标金额：<span style="color:red">${designeVo.subtotalTaxed}</span>元）
              							<span class="pull-right">
                                        <a class="trage" href="${pageContext.request.contextPath}/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${vo.unifiedRfqNum}" target="hejiafabu-baomingshenpilishi">报名历史</a>
                                        <a class="trage" href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?unifiedRfqNum=${vo.unifiedRfqNum}" target="9-hj-supply_chain_quotation_list">报价记录</a>
                                        <a class="trage" href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${vo.id}" target="gysbj-shenpilishiyemiannew">审批记录</a>
                                        <a class="trage" href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${vo.unifiedRfqNum}" target="gysbj-caozuorizhi">操作日志</a>
                                        </span>
                                    </div>
                                <div class="jqGridPage">
                                    <table id="jqGrid"></table>
                                    <div id="jqGridPager"></div>
                                </div>

                        <div class="clearfix"></div>
                    <c:choose>
                        <c:when test="${vo.type == '8'}">
                            <div class="mt12">
                                <div class="panel clearfix">
                                    <div class="panel-heading clearfix">
                                        <span class="required" style="margin-left: -10px"></span>
                                        <span class="mg-margin-right">授标说明</span>
                                    </div>
                                    <div class="panel-body">
                                        <textarea id="contentSimple" name="content" class="form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;"></textarea>
                                    </div>
                                </div>
                            </div>
                    <hr>
                    <div class="text-right">
                        <a href="javascript:void(0)" class="btn btn-lg btn-primary" type="button" onclick="submit_modal_func()">提交</a>
                        <a href="javascript:window.history.go(-1)" class="btn btn-lg " type="button">返回</a>
                    </div>
                </div>
                        </c:when>
                    </c:choose>

            </div>
        </div>
    </div>
</div>

<%--提交按钮(结果审批入口1)--%>
<div class="modal fade" id="myModa6">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">提交确认</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t ml35">
                    <i class="icon icon-question-sign text-primary"></i>
                    <div class="content ml12">
                        <h4>你确定要提交本报价单吗?</h4>
                        <p>竞价标题：　　${vo.title}</p>
                        <p>竞价编号：　　${vo.unifiedRfqNum}</p>
                        <p>
                            <span class="left">选择审批流程：</span>
                     <span class="col-lg-6 padding1">
                             <c:choose>
                                 <c:when test="${appList.size()>0}">
                                     <select id="approvalCode" class="form-control input-sm" style="width:110px">
                                         <c:forEach var="approvalBo" items="${appList}">
                                             <option value="${approvalBo.number},${approvalBo.type}"><c:if test="${approvalBo.type == 1}">标准:</c:if><c:if test="${approvalBo.type == 2}">灵活:</c:if>${approvalBo.wfDefineName}</option>
                                         </c:forEach>
                                     </select>
                                 </c:when>
                                 <c:otherwise>
                                     <select id="approvalCode" class="form-control input-sm" style="width:110px" disabled="true">
                                         <option value="0">无有效审批</option>
                                     </select>
                                 </c:otherwise>
                             </c:choose>
                        </span>
                        </p>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> <a type="button" class="btn btn-primary" onclick="submit_func()"  href="javascript:void(0);">确定提交</a>  <a type="button" class="btn" data-dismiss="modal"  href="javascript:void(0);">返回修改</a></div>
        </div>
    </div>
</div>


<!-- 撤销竞价原因模态框-->
<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">撤销竞价原因</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因类别：</span>

                        <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                            <option value="不需要采购">不需要采购</option>
                            <option value="供应商退出">供应商退出</option>
                            <option value="其他原因">其他原因</option>
                        </select>
                    </p>
                    <div class="clearfix"></div>
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因说明：</span>

                        <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>

<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo">
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
<input id="contextPath" type="hidden" value="${pageContext.request.contextPath}"/>
<script src="${pageContext.request.contextPath}/js/designateResult/pricingCommonality.js?v=${version}"></script>
<script src="${pageContext.request.contextPath}/js/designateResult/rfqDesignateMoney.js?v=${version}"></script>
<!--[if lt IE 9]>
<script type="text/javascript">
    jQuery(function($){
        $('#approvalCode').css('width','350px');
    });
</script>
<![endif]-->
</body>

</html>
