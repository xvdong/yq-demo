<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><c:choose><c:when test="${type == '1'}">未报价供应商</c:when><c:when test="${type == '2'}">IP地址相同供应商</c:when></c:choose></title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/designateResult/sameIpList.js?v=${version}"></script>
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>
<body>

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${unifiedRfqNum}">
                <input type="hidden" name="ouRfqNum" id="ouRfqNum" value="${ouRfqNum}">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="index.html"><i class="icon icon-home"></i> 工作台</a></li>
                        <c:choose>
                            <c:when test="${type == '1'}"><li class="active">未报价供应商</li></c:when>
                            <c:when test="${type == '2'}"><li class="active">IP地址相同供应商</li></c:when>
                        </c:choose>
                    </ol>
                </div>
                <c:choose>
                    <c:when test="${type == '1'}"><table class="table table-bordered table-hove align-md" id="jqGrid1"></table></c:when>
                    <c:when test="${type == '2'}"><table class="table table-bordered table-hove align-md" id="jqGrid2"></table></c:when>
                </c:choose>
            </div>
        </div>
    </div>
</div>
</body>
</html>
