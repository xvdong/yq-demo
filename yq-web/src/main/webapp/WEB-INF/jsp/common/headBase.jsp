<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<c:set var="resource" value="${pageContext.request.contextPath}"></c:set>
<c:set var="downloadAction" value="${ctx}/rfqAttachments/fileDownload"></c:set>
<!-- base import -->
<link href="${resource}/css/zui.min.css" rel="stylesheet">
<link href="${resource}/css/doc.min.css" rel="stylesheet">
<link href="${resource}/css/common.css?v=${version}" rel="stylesheet">
<!--届满微调css-->
<link href="${resource}/css/rfq.extra.css?v=${version}" rel="stylesheet"/>
<!--数据表格css-->
<link href="${resource}/lib/datatable/zui.datatable.min.css" rel="stylesheet"/>
<!--日期插件css-->
<link href="${resource}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
<script src="${resource}/js/jquery.js?v=${version}"></script>
<script src="${resource}/js/common.js?v=${version}"></script>
<!-- jQuery (表单提交控件) -->
<script src="${resource}/js/jquery.form.js?v=${version}"></script>
<!-- ZUI Javascript组件 -->
<script src="${resource}/js/zui.min.js?v=${version}"></script>
<!--选择时间和日期-->
<script src="${resource}/lib/datetimepicker/datetimepicker.min.js?v=${version}"></script>
<!--数据表格-->
<script src="${resource}/lib/datatable/zui.datatable.min.js?v=${version}"></script>
<!--基础JS-->
<script src="${resource}/js/base.js?v=${version}"></script>
<!-- 字典转换文件 -->
<script src="${resource}/js/rfq.formatter.js?v=${version}"></script>
<!-- RFQ 工具文件 -->
<script src="${resource}/js/rfq.js?v=${version}"></script>
<!--[if lt IE 8]>
<div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
<![endif]-->
<!--[if lt IE 8]>
<script src="${resource}/lib/ieonly/html5shiv.js?v=${version}"></script>
<script src="${resource}/lib/ieonly/respond.js?v=${version}"></script>
<script src="${resource}/lib/ieonly/excanvas.js?v=${version}"></script>
<![endif]-->
<script>
    var ctx = '${ctx}';
    var resource = '${resource}';
</script>





