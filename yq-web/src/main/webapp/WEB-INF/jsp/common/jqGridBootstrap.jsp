<%@ page contentType="text/html;charset=UTF-8"%>
<!-- jqgrid and bootstrap import -->
<!-- This is the localization file of the grid controlling messages, labels, etc.
<!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrapfont.css">
<!-- The link to the CSS that the grid needs -->
<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/css/ui.jqgrid-bootstrap.css" />
<link href="${pageContext.request.contextPath}/css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
<!-- We support more than 40 localizations -->
<script type="text/ecmascript" src="${pageContext.request.contextPath}/js/grid.locale-cn.js?v=${version}"></script>
<!-- This is the Javascript file of jqGrid -->
<script src="${pageContext.request.contextPath}/js/jquery.jqGrid.min.js?v=${version}"></script>




