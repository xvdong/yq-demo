<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>提示消息</title>
    <c:import url="../common/headBase.jsp" />
    <script src="${pageContext.request.contextPath}/js/jquery.form.js?v=${version}" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/ctmContract/finishMainBody.js?v=${version}"></script>
    <script type="text/javascript">
        function countDown(secs,surl){
            //alert(surl);
            var jumpTo = document.getElementById('jumpTo');
            jumpTo.innerHTML=secs;
            if(--secs>0){
                setTimeout("countDown("+secs+",'"+surl+"')",1000);
            }
            else{
                location.href=surl;
            }
        }
    </script>
    <style>
        body{ background: #fff;}
        .tzDiv{ width:640px;  height:220px; background: url("../img/images/tz.jpg") no-repeat left top; position: absolute; left: 50%; top: 50%; margin-left: -275px; margin-top: -110px;}
        .tz-info{ padding-left: 210px; padding-top: 10px;color:#bcbcbc; font-size: 16px; }
        #jumpTo{color: red;}
        .tz-info h3{color:#53a3ed; font-size: 22px;}
        .tz-btn{ margin-top: 15px; display: block; width: 142px; height: 41px; line-height: 39px; color: #fff; text-align: center; background: url("../img/images/tzbtn.jpg") no-repeat;}
        .tz-btn:hover{ color: #fff !important; opacity: 0.9;}
    </style>
</head>
<body>
<!-- nav -->
<c:import url="/common/top" />


<div class="tzDiv">
    <div class="tz-info">
        <h3>正在跳转中...</h3>
        <%= request.getAttribute("msg")%>
        <a class="tz-btn" href="<%= request.getAttribute("url")%>">直接跳转</a>
    </div>
</div>
<script type="text/javascript">countDown(5,'<%= request.getAttribute("url")%>');</script>
</body>
</html>
