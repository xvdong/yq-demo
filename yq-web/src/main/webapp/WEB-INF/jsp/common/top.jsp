<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- top -->
<div class="top_nav">
    <div class="container">
        <div class="row">
            <div class="col-xs-8">
                <ul class="head_l list-unstyled">
                    <li class="welcome">${sessionUser.usrAccount}您好，欢迎使用欧冶采购！</li>
                    <%--<li>消息 <a href="#"><span class="label label-danger">8</span></a></li>--%>
                    <%--<li>角色切换<a href="http://localhost:8082/logout"><i class="icon-exchange"></i></a></li>--%>
                    <li>服务器当前时间：<span class="serverTime" style="display: inline-block;"></span></li>
                </ul>
            </div>
            <div class="col-xs-4">
                <div class="head_r_box">
                    <ul class="head_r list-unstyled">
                        <li><a href="http://www.ouyeelbuy.com/ouyeelbuy-web/">网站首页</a></li>
                        <%--<li class="nav-xl"><a href="###">我的采购宝 <span class="caret"></span></a>
                            <div class="nav-content">
                                <dl>
                                    <dd><a href="#">我的信息</a></dd>
                                    <dd><a href="#">我的采购</a></dd>
                                    <dd><a href="#">我的审批</a></dd>
                                    <dd><a href="#">安全退出</a></dd>
                                </dl>
                            </div>
                        </li>--%>
                        <li><a href="http://www.ouyeelbuy.com/ouyeelbuy-web/serviceCenter">服务中心</a></li>
                        <%--<li class="nav-xl"><a href="#">导航菜单 <span class="caret"></span></a>
                            <div class="nav-content nav-sitemap">
                                <dl>
                                    <dt>请购管理</dt>
                                    <dd class="sitemap">
                                        <span><a href="#">发布询价单</a></span><span><a href="#">发布招募书</a></span><span><a href="#">发布自主招标</a></span><span><a
                                            href="#">发布反向竞拍</a></span><span><a href="#">发布组合竞价</a></span>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>请购管理</dt>
                                    <dd class="sitemap">
                                        <span><a href="#">发布询价单</a></span><span><a href="#">发布招募书</a></span><span><a href="#">发布自主招标</a></span><span><a
                                            href="#">发布反向竞拍</a></span><span><a href="#">发布组合竞价</a></span>
                                    </dd>
                                </dl>
                            </div>
                        </li>--%>
                        <li><a href="http://www.ouyeelbuy.com/ouyeelbuy-web/">安全退出</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top_header">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="logo"><a href="${sessionScope.workHref}"> <img src="../images/logo.png"></a></div>
            </div>
            <div class="col-md-10">
                <ul class="topmenu">
                    <%--工作台和工作台首页暂不跳转--%>
                    <%--<li><a href="http://eps.baosteel.net.cn/eps_shp_mem/memberNew/default.jsp">工作台首页</a></li>--%>
                    <li><a href="#">工作台首页</a></li>
                    <%--<li><a href="#">请购管理</a></li>
                    <li><a href="#">采购管理</a></li>--%>
                    <li><a class="active" href="<c:url value="/ctmAdultRecord/init" />">询比价</a></li>
                    <%--<li><a href="#">财务管理</a></li>
                    <li><a href="#">报表管理</a></li>
                    <li><a href="#">系统设置</a></li>--%>
                </ul>
                <div class="nav-search" id="searchForm">
                    <input type="text" class="form-control input-xs" id="searchInput" placeholder="">
                    <!--<i class="icon icon-search"></i>-->
                    <button id="searchHelpBtn" type="button" class="btn btn-link"><i class="icon icon-search"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: ctx + '/common/getNowTime',
            success: function (data) {
                setInterval(function () {
                    data++;
                    $('.serverTime').html(getLocalTime(data));
//                 console.log(data);
                }, 1000)
            }
        });
    });

    function getLocalTime(St) {
//        return new Date(parseInt(St) * 1000).toLocaleString();
        var dt = new Date(parseInt(St) * 1000);
        var date=dt.getFullYear() + "年" + (dt.getMonth()+1) + "月" + dt.getDate() + "日 " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        return date;

    }
</script>
