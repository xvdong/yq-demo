<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="top_nav">

    <div class="container">

        <div class="row">

            <div class="col-xs-7">

                <ul class="head_l list-unstyled">

                    <li class="welcome">您好，欢迎使用欧冶采购！</li>

                    <li>消息 <a href="#"><span class="label label-danger">8</span></a></li>

                    <li>角色切换<a href="javascript:;"><i class="icon-exchange"></i></a></li>

                    <li>服务器当前时间：<span class="serverTime" style="display: inline-block;"></span></li>
                </ul>

            </div>

            <div class="col-xs-5">

                <div class="head_r_box">

                    <ul class="head_r list-unstyled">

                        <li><a href="#">网站首页</a></li>

                        <li class="nav-xl"><a href="###">我的采购宝 <span class="caret"></span></a>

                            <div class="nav-content">

                                <dl>

                                    <dd><a href="#">我的信息</a></dd>

                                    <dd><a href="#">我的采购</a></dd>

                                    <dd><a href="#">我的审批</a></dd>

                                    <dd><a href="#">安全退出</a></dd>

                                </dl>

                            </div>

                        </li>

                        <li><a href="#">客服中心</a></li>

                        <li class="nav-xl"><a href="#">网站导航 <span class="caret"></span></a>

                            <div class="nav-content nav-sitemap">

                                <dl>

                                    <dt>请购管理</dt>

                                    <dd class="sitemap">

                                        <span><a href="#">发布询价单</a></span><span><a href="#">发布招募书</a></span><span><a
                                            href="#">发布自主招标</a></span><span><a href="#">发布反向竞拍</a></span><span><a
                                            href="#">发布组合竞价</a></span>

                                    </dd>

                                </dl>

                                <dl>

                                    <dt>请购管理</dt>

                                    <dd class="sitemap">

                                        <span><a href="#">发布询价单</a></span><span><a href="#">发布招募书</a></span><span><a
                                            href="#">发布自主招标</a></span><span><a href="#">发布反向竞拍</a></span><span><a
                                            href="#">发布组合竞价</a></span>

                                    </dd>

                                </dl>

                            </div>

                        </li>

                    </ul>

                </div>

            </div>

        </div>

    </div>

</div>

<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: ctx + '/common/getNowTime',
            success: function (data) {
                setInterval(function () {
                    data++;
                    $('.serverTime').html(getLocalTime(data));
//                 console.log(data);
                }, 1000)
            }
        });
    });

    function getLocalTime(St) {
        return new Date(parseInt(St) * 1000).toLocaleString();
    }
</script>

