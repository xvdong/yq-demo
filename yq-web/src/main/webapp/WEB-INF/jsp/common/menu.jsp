<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="sidebar clearfix">

    <div class="container">

        <div class="menu" data-toggle="menu">

            <ul class="nav nav-primary">

                <li class="show">

                    <a href="#"><i class="icon-list-ul"></i> 常用菜单</a>

                    <ul class="nav">
                        <c:choose>
                            <c:when test="${environment == 'jq' || environment == 'run'}">
                                <c:forEach items="${userMenus}" var="item">
                                    <li><a href="${item.url}"><i class="icon-angle-right"></i>${item.menuName}</a>
                                    </li>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <li><a href="${pageContext.request.contextPath}/rfqRequest/init"><i
                                        class="icon-angle-right"></i>寻源项目管理</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqRequest/toEditRfqRequest"><i
                                        class="icon-angle-right"></i>发布询价单</a></li>
                                <li><a href="${pageContext.request.contextPath}/dacReverseAuction/toreleaseReverseAuction">
                                    <i class="icon-angle-right"></i>发布反向竞价</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqPreauditSupplier/init"><i
                                        class="icon-angle-right"></i>供应商报名列表</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqQuotation/init"><i
                                        class="icon-angle-right"></i>供应商报价列表</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqQuotationLock/init"><i
                                        class="icon-angle-right"></i>供应商报价管理</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqApproval/init"><i
                                        class="icon-angle-right"></i>审批流管理</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqRequest/auditInit"><i
                                        class="icon-angle-right"></i>审批列表</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqFinancialControlList/init"><i
                                        class="icon-angle-right"></i>财务控制</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqFinancialApprovalList/init"><i
                                        class="icon-angle-right"></i>缴纳审批列表</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqTermsController/init"><i
                                        class="icon-angle-right"></i>条款库维护</a></li>
                                <li><a href="${pageContext.request.contextPath}/rfqRequest/permission"><i
                                        class="icon-angle-right"></i>权限查看</a></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>

                </li>


            </ul>

        </div>

    </div>

</div>































