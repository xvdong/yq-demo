<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="../common/tagDeclare.jsp"%>
<div class="panel" id="panel-thead">
    <input id="requestId" value="${vo.id}" type="hidden">
    <div class="panel-heading clearfix">
        <span class="mg-margin-right"><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">询价单摘要</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价单摘要</c:when></c:choose></span>
        <div class=" pull-right">
            <c:if test="${vo.rfqMethod=='RAQ' && param.flag!='bjzl'}">
                <c:if test="${sessionScope.chatUrl==null || sessionScope.chatUrl == ''}">
                    <a id = 'im' href="javascript:RFQ.warn('提示：该功能仅在uat环境与生产环境可用！');"></a>
                </c:if>
                <c:if test="${sessionScope.chatUrl!=null && sessionScope.chatUrl != ''}">
                    <a id = 'im' target="_blank"  href="${sessionScope.chatUrl}${vo.ouRfqNum}"></a>
                </c:if>
            </c:if>

            <c:choose>
                <c:when test="${vo.rfqMethod=='DAC'&&vo.type == '6'}">
                    <a href="${pageContext.request.contextPath}/biddingHall/init?requestId=${vo.id}" onclick="momentMethod()"><span class="costom"><img src="${pageContext.request.contextPath}/images/freshaddon.png" style="width:40px; height:40px; margin-right: 5px; margin-top: -25px;">竞价大厅</span></a>
                </c:when>
            </c:choose>
            <p:permission  privilege="1" requestNo="${vo.unifiedRfqNum}">
                <c:choose>
                    <c:when test="${vo.rfqMethod=='RAQ' && param.flag!='bjzl'}"><a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">询单详情</span></a></c:when>
                    <c:when test="${vo.rfqMethod=='DAC'}"><a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">竞价单详情</span></a></c:when>
                </c:choose>
            </p:permission>

            <c:if  test="${ vo.quotationRound == '0' && vo.specilSupplier == '1'}">
                <c:if test="${ vo.type == '3' || vo.type == '4'  || vo.type == '5'  || vo.type == '6' }">
                    <p:permission requestNo="${vo.unifiedRfqNum}" privilege="2" >
                        <span class="btn btn-warning font12" data-position="center" id="superlist">追加供应商</span>
                    </p:permission>
                </c:if>
            </c:if>
            <c:choose>
                <c:when test="${(vo.type != '11' && vo.type != '12') && param.flag!= 'bjzl' }">
                    <p:permission requestNo="${vo.unifiedRfqNum}" privilege="2" >
                        <span class="btn btn-danger font12" data-position="center" data-toggle="modal" data-target="#cancelPrice" ><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">撤销询价</c:when><c:when test="${vo.rfqMethod=='DAC'}">撤销竞价</c:when></c:choose></span>
                    </p:permission>
                </c:when>
            </c:choose>
        </div>
    </div>
    <input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}">
    <input type="hidden" name="quotationRound" id="quotationRound" value="${vo.quotationRound}">
    <input type="hidden" name="id" id="id" value="${vo.id}">
    <div class="panel-body font12">
        <div class="row">
            <div class="mg-pacne clearfix">
                <ul>
                    <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">询价单号</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价单号</c:when></c:choose>：${vo.ouRfqNum}</li>
                    <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">询价标题</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价标题</c:when></c:choose>：${vo.title}</li>
                    <li>采购单位：${vo.ouName}</li>
                    <li>计划编号：${vo.planNo}</li>
                    <li>当前状态：<span class="red" id="nowState" ><c:choose><c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}">待报价</c:when><c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}">待竞价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6' && vo.rfqMethod=='RAQ'}">报价中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}">竞价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when><c:when test="${vo.type == '9'}">结果待审批</c:when></c:choose></span></li>
                    <li>邀请范围：<span id="publicBiddingFlag"><c:choose><c:when test="${vo.publicBiddingFlag == '1'}">公开寻源</c:when><c:otherwise>定向寻源</c:otherwise></c:choose></span></li>
                </ul>
                <c:choose>
                    <c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbj.png"></span></c:when>
                    <c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/djj.png"></span></c:when>
                    <c:when test="${vo.type == '5'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bmz.png"></span></c:when>
                    <c:when test="${vo.type == '6'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bjz.png"></span></c:when>
                    <c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/jjz.png"></span></c:when>
                    <c:when test="${vo.type == '7'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dkb.png"></span></c:when>
                    <c:when test="${vo.type == '11'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/yzf.png"></span></c:when>
                    <c:when test="${vo.type == '12'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/ylb.png"></span></c:when>
                    <c:when test="${vo.type == '9'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dsh.png"></span></c:when>
                </c:choose>
            </div>
            <div class="mg-pacne clearfix">
                <ul>
                    <li>保证金金额：<span class="red"><span class="red"><c:choose><c:when test="${vo.assureMoney >= '0'}"><fmt:formatNumber type="number" value="${vo.assureMoney} " maxFractionDigits="4"/>元</c:when></c:choose></span></span></li>
                    <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">报价货币</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价币种</c:when></c:choose>：<c:choose><c:when test="${vo.currency=='CNY'}">人民币CNY</c:when><c:when test="${vo.currency=='USD'}">美元USD</c:when><c:when test="${vo.currency=='GBP'}">英镑GBP</c:when><c:when test="${vo.currency=='JPY'}">日元JPY</c:when><c:when test="${vo.currency=='EUR'}">欧元EUR</c:when><c:when test="${vo.currency=='HKD'}">港元HKD</c:when><c:when test="${vo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                    <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">预算总价</c:when><c:when test="${vo.rfqMethod=='DAC'}">参考总价</c:when></c:choose>：<span class="red"><fmt:formatNumber type="number" value="${vo.totalBudget}" maxFractionDigits="0"/>元</span></li>
                </ul>
            </div>
            <div class="mg-pacne clearfix">
                <ul>
                        <span>分项分量报价：
                            <c:choose>
                                <c:when test="${rfqRulesVo.partialProductFlag == '1'}">允许对部分产品报价 </c:when>
                                <c:otherwise>不允许对部分产品报价 </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${rfqRulesVo.partialQuantityFlag == '1'}">允许对部分数量报价 </c:when>
                                <c:otherwise>不允许对部分数量报价 </c:otherwise>
                            </c:choose>
                        </span>
                </ul>
            </div>
            <div class="mg-pacne clearfix">
                <ul>
                    <li>发布时间：<c:choose><c:when test="${fn:length(vo.issueDate)>'16'}">${fn:substring(vo.issueDate,0,16)}</c:when> <c:otherwise>${vo.issueDate}</c:otherwise></c:choose></li>
                    <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">报价开始时间</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价开始时间</c:when></c:choose>：<span id ="startDate"><c:choose><c:when test="${fn:length(vo.startDate)>'16'}">${fn:substring(vo.startDate,0,16)}</c:when><c:otherwise>${vo.startDate}</c:otherwise></c:choose></span></li>
                    <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">报价截止时间</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价截止时间</c:when></c:choose>：<span id ="quotationEndDate"><c:choose><c:when test="${fn:length(vo.quotationEndDate)>'16'}">${fn:substring(vo.quotationEndDate,0,16)}</c:when><c:otherwise>${vo.quotationEndDate}</c:otherwise></c:choose></span></li>
                    <c:choose>
                        <c:when test="${vo.publicBiddingFlag == '1'}">
                            <li>报名截止时间：<span id ="registrationEndTime"><c:choose><c:when test="${fn:length(vo.registrationEndTime)>'16'}">${fn:substring(vo.registrationEndTime,0,16)}</c:when><c:otherwise>${vo.registrationEndTime}</c:otherwise></c:choose></span></li>
                        </c:when>
                    </c:choose>
                </ul>

                <c:choose>
                    <c:when test="${vo.type == '3' || vo.type == '5'  || vo.type == '6' }"><p:permission requestNo="${vo.unifiedRfqNum}" privilege="2" ><a class="btn btn-info tz-time-btn" data-position="center" data-toggle="modal" data-target="#adjustTime" onclick="checkTime ();"><i class="icon icon-time"></i>&nbsp;调整时间</a></p:permission></c:when>
                </c:choose>
                <c:choose>
                    <c:when test="${vo.type == '7'}"><p:permission requestNo="${vo.unifiedRfqNum}" privilege="2" ><a data-toggle="modal" data-target="#myModal2"><span class="btn btn-warning mt12"><i class="icon icon-circle-arrow-right"></i>&nbsp;开标</span></a></p:permission></c:when>
                </c:choose>
            </div>
            <div class="mg-pacne clearfix">
                <ul>
                    <li>联系人：${vo.linkmanName}</li>
                    <li>联系电话：${vo.linkmanTelphone}</li>
                    <li style="width:40%;">交货地址：${vo.deliveryProvince}${vo.deliveryCity}${vo.deliveryArea}${vo.deliveryAddress}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="ouRfqNum" value="${vo.ouRfqNum}" />
<script>
    function momentMethod(){
        window.sessionStorage.unifiedRfqNum = $("#unifiedRfqNum").val();
    }
</script>
