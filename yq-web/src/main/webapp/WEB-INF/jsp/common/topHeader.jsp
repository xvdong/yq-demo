<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="top_header">

    <div class="container">

        <div class="row">

            <div class="col-md-2">

                <div class="logo"><a href="${sessionScope.workHref}"> <img src="${pageContext.request.contextPath}/images/logo.png"></a></div>

            </div>

            <div class="col-md-10">

                <ul class="topmenu">

                    <li><a href="index.html">工作台首页</a></li>

                    <li><a class="active" href="#">请购管理</a></li>

                    <li><a href="#">采购管理</a></li>

                    <li><a href="#">工作量审批</a></li>

                    <li><a href="#">电子合同</a></li>

                    <li><a href="#">财务管理</a></li>

                    <li><a href="#">报表管理</a></li>

                    <li><a href="#">我的门户</a></li>

                    <li><a href="#">系统设置</a></li>

                </ul>



            </div>

        </div>

    </div>

</div>
