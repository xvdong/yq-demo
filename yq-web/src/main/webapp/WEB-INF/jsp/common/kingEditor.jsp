<%@ page contentType="text/html;charset=UTF-8"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/lib/kindeditor/themes/default/default.css">
<script src="${pageContext.request.contextPath}/lib/kindeditor/kindeditor-all-min.js?v=${version}"></script>
<script src="${pageContext.request.contextPath}/lib/kindeditor/lang/zh_CN.js?v=${version}"></script>