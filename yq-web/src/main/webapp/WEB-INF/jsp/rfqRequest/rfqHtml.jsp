<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>结果待审批</title>
    <style type="text/css">
        body{padding-bottom:20px;background:#fff; color:#333; font-family:SimSun;}
        ul,li{list-style:none; margin:0; padding:0;}
        .v-align{vertical-align:middle !important;}
        .container {max-width:1000px;margin-top:12px;margin:0 auto;}
        .container_main{padding:0 0 10px 0; margin-top:0;}
        h1{
            text-align: center; margin-bottom: 50px; color: #666;
        }
        .rightbar{ height:100%; }
        .table.align-md td,.table.align-md th{ text-align:center; vertical-align:middle;}
        .mg-a-text{color:#ea644a;margin-right:8px}
        .mg-margin-right{margin-right:40px;font-size:16px}
        .mg-header{line-height:16px;position:relative;font-size:16px;padding-bottom:10px;margin-bottom:15px;clear:both}

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border:1px solid #666;
            color:#333
        }
        .table  th ,td{
            border:1px solid #666;
            line-height:16px;
            text-align: center;
            font-size:11px;
            color: #333;
            padding:6px 0;
            border-spacing:0;
            vertical-align:middle;
        }
        .table tr td:first-child{
            border-left: 1px solid #666;
        }
        .table  th{
            border-top:1px solid #666;
        }
        .table  th:first-child{
            border-left: 1px solid #666;
        }
        .table1{
            border:1px solid #666;
        }
        .table1 th,.table1 td{
            height:46px; text-align: left; border:0;
        }
        .table1 th {
            background-color: #f5f5f5;
        }
        td label{
            color: #333;
            font-weight: bold;
        }
        .panel .table1 th,.panel .table1 td{
            height: 30px;
            line-height: 30px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container container_main">
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">
                    <h1>欧冶采购报价单</h1>
                    <div class="panel">
                        <table class="table table1">
                            <tr>
                                <th colspan="3" style="padding-left: 10px;">基本情况</th>
                            </tr>

                            <tbody>
                            <tr>
                                <td colspan="2" style="padding-left: 10px;">采购组织：${ouName}</td>
                                <td>计划单号：${planNo}</td>
                            </tr>
                            <tr>
                                <td style="padding-left: 10px;">询价单号：${ouRfqNum}</td>
                                <td>询价单标题：${title}</td>
                                <td>创建日期：${recCreateTime}</td>
                            </tr>
                            <tr>
                                <td style="padding-left: 10px;">邀请供应商数量：${numberOfSupplier}</td>
                                <td>报价供应商数量：${numberOfPrice}</td>
                                <td>币种：${currency}</td>
                            </tr>
                            <tr>
                                <td style="padding-left: 10px;">询价项数：${number}</td>
                                <td colspan="2">拟签项数：${countOfWangTo}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="mg-header">
                        物料明细：
                    </div>
                    <div class="tab-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>序号</th>
                                <th>物料代码</th>
                                <th>物料名称</th>
                                <th>型号规格</th>
                                <th>生产厂家</th>
                                <th>采购数量</th>
                                <th>参考价格</th>
                                <th>计量单位</th>
                                <th>要求交货期</th>
                            </tr>
                            </thead>


                            <tbody>
                            <c:set var="sum" value="1"/>
                            <c:forEach items="${thingsList}" var="item" varStatus="status">
                                <tr>
                                    <td>${sum}</td>
                                    <td>${item.materialNo}</td>
                                    <td>${item.materialName}</td>
                                    <td>${item.character}</td>
                                    <td>${item.producer}</td>
                                    <td>${item.requestAmount}</td>
                                    <td>${item.salePrice}</td>
                                    <td>${item.unit}</td>
                                    <td>${item.requestDeliveryDate}</td>
                                </tr>
                                <c:set var="sum" value="${sum + 1}"/>
                            </c:forEach>
                            </tbody>
                        </table>

                    </div>
                    <div class="mg-header">
                        报价情况：
                    </div>
                    <div class="tab-content">
                        <table class="table table-bordered align-md">
                            <thead>
                            <tr>
                                <th>报价单位</th>
                                <th>报价时间</th>
                                <th>报价次数</th>
                                <th width="52"  style="line-height:18px;padding:3px 0">报价物料行数</th>
                                <th>报价有效期</th>
                                <th style="line-height:18px;padding:3px 0">有效报价总价</th>
                                <th width="52" style="line-height:18px;padding:3px 0" >拟签物料行数</th>
                                <c:if test="${priceType =='0'}">
                                    <th width="52"  style="line-height:18px;padding:3px 0">拟签项原报价未税总价</th>
                                    <th>拟签未税总价</th>
                                </c:if>
                                <c:if test="${priceType =='1'}">
                                    <th width="52"  style="line-height:18px;padding:3px 0">拟签项原报价含税总价</th>
                                    <th>拟签含税总价</th>
                                </c:if>
                                <c:if test="${priceType =='N'}">
                                    <th width="52"  style="line-height:18px;padding:3px 0">拟签项原报价总价</th>
                                    <th>拟签总价</th>
                                </c:if>
                            </tr>
                            </thead>
                            <tbody>

                            <c:forEach items="${rfqHtmlQuotationDetailList}" var="item" varStatus="status">
                                <tr>
                                    <td>${item.supplierName}</td>
                                    <td>${item.quotationDate}</td>
                                    <td>${item.quotationTimes}</td>
                                    <td>${item.quotaionNubers}</td>
                                    <td>${item.quotationValidDate}</td>
                                    <td>${item.total}</td>
                                    <td>${item.wantToNumer}</td>
                                    <td>${item.wantToOrigTotal}</td>
                                    <td>${item.wantToToal}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>


                    <div class="mg-header" style="border-bottom: none">
                        供应商详细报价：
                    </div>
                    <div class="tab-content">
                        <c:forEach items="${winSupplier}" var="item" varStatus="status">
                            <table class="table table-bordered align-md">
                                <thead>
                                <tr>
                                    <th>物料代码</th>
                                    <th>物料名称</th>
                                    <th style="width:68px">型号和规格</th>
                                    <th style="width:64px" >承诺交货期</th>
                                    <th>可供数量</th>
                                    <c:if test="${priceType =='0'}">
                                        <th>未税单价</th>
                                    </c:if>
                                    <c:if test="${priceType =='1'}">
                                        <th>含税单价</th>
                                    </c:if>
                                    <c:if test="${priceType =='N'}">
                                        <th>单价</th>
                                    </c:if>
                                    <th>制造商</th>
                                    <th>拟签数量</th>
                                    <c:if test="${priceType =='0'}">
                                        <th>拟签未税单价</th>
                                    </c:if>
                                    <c:if test="${priceType =='1'}">
                                        <th>拟签含税单价</th>
                                    </c:if>
                                    <c:if test="${priceType =='N'}">
                                        <th>拟签单价</th>
                                    </c:if>
                                    <th >询价单备注</th>
                                    <th>代替型规</th>
                                    <th  width="56"  style="line-height:18px;padding:3px 0">报价物料明细备注</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="12" style="text-align: left"><span
                                            style="margin-right: 26px; padding-left: 4px">供应商:${item.supplierName}</span>
                                        <span>报价人:${item.quotationPeple}</span></td>
                                </tr>

                                <c:set var="supplierCode" value="${item.supplierCode}"/>
                                <c:forEach items="${thirdList}" var="item2" varStatus="status">
                                    <c:if test="${supplierCode == item2.supplierNum}">
                                        <tr>
                                            <td>${item2.materialNo}</td>
                                            <td>${item2.materialName}</td>
                                            <td>${item2.character}</td>
                                            <td>${item2.availableDeliveryDate}</td>
                                            <td>${item2.availableCount}</td>
                                            <td>${item2.price}</td>
                                            <td>${item2.manufactory}</td>
                                            <td>${item2.confirmedAmout}</td>
                                            <td>${item2.wantToOnePrice}</td>
                                            <td>${item2.requestMemo}</td>
                                            <td>${item2.substitutableSpec}</td>
                                            <td>${item2.quotationMemo}</td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                                <tr>
                                    <td colspan="12" style="text-align: left; padding-left: 14px">报价说明:${item.quotationMemo}</td>
                                </tr>
                                </tbody>
                            </table>
                        </c:forEach>

                    </div>


                    <div class="panel mt24">
                        <table class="table">
                            <tr>
                                <th colspan="3" style="padding-left: 10px; background-color: #f5f5f5; text-align: left">采购员核价说明</th>
                            </tr>

                            <tbody>
                            <tr>
                                <td colspan="3" style="padding:10px; height:52px;line-height: 24px; text-align: left; border-right: 1">
                                    <c:if test="${requestStatus != '8'}">
                                        ${resultMemo}
                                    </c:if>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div  style="padding-bottom: 20px; float:right;color:#666"><strong>日期：${date}</strong></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
