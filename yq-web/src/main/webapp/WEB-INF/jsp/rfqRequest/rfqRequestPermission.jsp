<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>权限查看</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqRequestPermission.js?v=${version}"></script>
</head>

<body>

<c:import url="../common/top.jsp" />

<div class="wrapper">

    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">

            <div class="container">

                <div class="breadcrumbs">

                    <ol class="breadcrumb">

                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>

                        <li class="active">权限查看</li>

                    </ol>

                </div>

                <div class="page-content">

                    <div class="row">

                        <div class="col-md-12">

                            <div>

                                <ul id="myTab" class="nav nav-tabs">

                                    <li class="active"><a href="#tab1" data-toggle="tab">全部类型</a></li>

                                    <%--<li><a href="#tab2" data-toggle="tab" data-method="DAC">单一来源</a></li>--%>

                                    <li><a href="#tab3" data-toggle="tab" data-method="RAQ">询比价</a></li>

<%--
                                    <li><a href="#tab4" data-toggle="tab" data-method="DAC">反向竞价</a></li>
--%>

                                    <%--<li><a href="#tab5" data-toggle="tab" data-method="DAC">招投标</a></li>--%>

                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane active  example" id="tab1">

                                        <div class="col-lg-12 ">


                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">

                                            <form  method="post" id="searchFormTab1" class="searchFormTab" onsubmit="return false;">
                                                <div class="so-form">

                                                    <div class="so-form-20 padding1">
                                                        <input name="unifiedRfqNum" id="unifiedRfqNum" type="text" class="form-control" placeholder="询价单号">
                                                    </div>

                                                    <div class="so-form-20 padding1">
                                                        <input name="title" id="title" type="text" class="form-control" placeholder="询单标题">
                                                    </div>

                                                    <div class="so-form-20 padding1">
                                                        <input name="recCreatorUsername" id="recCreatorUsername" type="text" class="form-control" placeholder="创建人">
                                                    </div>
                                                   <c:if test="${isSuperMan == 1}">
                                                       <div class="so-form-15 padding1"><input name="ouName" id="ouName1" type="text" class="form-control search_c" placeholder="采购组织" autocomplete="off">
                                                       </div>
                                                       <div class="so-form-15 padding1" style="display: none;"><input name="uuCode" id="uuCode1" type="text" class="form-control search_c" >
                                                       </div>
                                                   </c:if>
                                                    <div class="so-form-15">
                                                        <button class="btn btn-primary btnSearch" type="button"><i
                                                                class="icon icon-search"></i> 搜索
                                                        </button>
                                                        <input class="btn ml12" type="reset" value="重置"/></div>

                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                       placeholder="创建时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                                type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                                class="form-control form-datetime"></div>
                                                        <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                       placeholder="报名时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                           placeholder="报名时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                       placeholder="报价时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                           placeholder="报价时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>


                                        </div>

                                    </div>

                                    <div class="tab-pane  example" id="tab2">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">

                                                <ul class="nav navbar-nav">

                                                    <li class="active"><a href="#">全部状态：<strong
                                                            class="red2">(0)</strong></a></li>

                                                    <li><a href="#">草稿<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">拟定中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">招募中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">招募结束<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">已作废<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">已签合同<strong class="red2">(0)</strong></a></li>

                                                </ul>

                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab2" class="searchFormTab" onsubmit="return false;">

                                                <div class="so-form">

                                                    <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                            placeholder="询价单号"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                            placeholder="询单标题"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                            placeholder="创建人"></div>

                                                    <c:if test="${isSuperMan == 1}">
                                                        <div class="so-form-15 padding1"><input type="text" name="ouName" id="ouName2"  class="form-control search_c" placeholder="采购组织" autocomplete="off">
                                                        </div>
                                                        <div class="so-form-15 padding1" style="display: none;"><input name="uuCode" id="uuCode2" type="text" class="form-control search_c" >
                                                        </div>
                                                    </c:if>
                                                    <div class="so-form-15">
                                                        <button class="btn btn-primary btnSearch" type="button"><i
                                                                class="icon icon-search"></i> 搜索
                                                        </button>
                                                        <input class="btn ml12" type="reset" value="重置"/></div>

                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="recCreateTime" id="recCreateTime"
                                                                                       placeholder="创建时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                                type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                                class="form-control form-datetime"></div>
                                                        <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                       placeholder="报名时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="registrationEndDate"
                                                                                           placeholder="报名时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                       placeholder="报价时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="quotationEndDate"
                                                                                           placeholder="报价时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                    </div>

                                                </div>

                                            </form>
                                        </div>

                                    </div>

                                    <div class="tab-pane  example" id="tab3">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">

                                                <ul class="nav navbar-nav">

                                                    <li class="active"><a href="#" data-type="100">全部状态：<strong
                                                            class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="0">草稿<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="102">创建待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="101">待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="3">待报价<strong class="red2">(0)</strong></a></li>

                                                    <%--<li><a href="#" data-type="4">待报名<strong class="red2">(0)</strong></a></li>--%>

                                                    <li><a href="#" data-type="5">报名中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="6">报价中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="7">待开标<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="8">待核价<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="9">结果待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="13">结果待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="11">已作废<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="10">已结束<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="12">已流标<strong class="red2">(0)</strong></a></li>

                                                </ul>


                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab3" class="searchFormTab" onsubmit="return false;">
                                                <div class="so-form">

                                                    <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                            placeholder="询价单号"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                            placeholder="询单标题"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                            placeholder="创建人"></div>
                                                    <c:if test="${isSuperMan == 1}">
                                                        <div class="so-form-15 padding1"><input type="text" name="ouName" id="ouName3"  class="form-control search_c" placeholder="采购组织" autocomplete="off">
                                                        </div>
                                                        <div class="so-form-15 padding1" style="display: none;"><input name="uuCode" id="uuCode3" type="text" class="form-control search_c" >
                                                        </div>
                                                    </c:if>


                                                    <div class="so-form-15">
                                                        <button class="btn btn-primary btnSearch" type="button"><i
                                                                class="icon icon-search"></i> 搜索
                                                        </button>
                                                        <input class="btn ml12" type="reset" value="重置"/></div>

                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                       placeholder="创建时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                                type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                                class="form-control form-datetime"></div>
                                                        <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                       placeholder="报名时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                           placeholder="报名时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                       placeholder="报价时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                           placeholder="报价时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>

                                        </div>

                                    </div>


                                    <div class="tab-pane  example" id="tab4">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">

                                                <ul class="nav navbar-nav">

                                                    <li class="active"><a href="#" data-type="100">全部状态：<strong
                                                            class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="0">草稿<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="102">创建待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="101">待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="3">待竞价<strong class="red2">(0)</strong></a></li>

                                                    <%--<li><a href="#" data-type="4">待报名<strong class="red2">(0)</strong></a></li>--%>

                                                    <li><a href="#" data-type="5">报名中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="6">竞价中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="7">待开标<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="8">待授标<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="9">结果待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="13">结果待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="11">已作废<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="10">已结束<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="12">已流标<strong class="red2">(0)</strong></a></li>

                                                </ul>

                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab4"  class="searchFormTab" onsubmit="return false;">
                                                <div class="so-form">

                                                    <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                            placeholder="竞价单号"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                            placeholder="竞价标题"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                            placeholder="创建人"></div>

                                                    <c:if test="${isSuperMan == 1}">
                                                        <div class="so-form-15 padding1"><input type="text" name="ouName" id="ouName4" class="form-control search_c" placeholder="采购组织" autocomplete="off">
                                                        </div>
                                                        <div class="so-form-15 padding1" style="display: none;"><input name="uuCode" id="uuCode4" type="text" class="form-control search_c" >
                                                        </div>
                                                    </c:if>
                                                    <div class="so-form-15">
                                                        <button class="btn btn-primary btnSearch" type="button"><i
                                                                class="icon icon-search"></i> 搜索
                                                        </button>
                                                        <input class="btn ml12" type="reset" value="重置"/></div>

                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                       placeholder="创建时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                                type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                                class="form-control form-datetime"></div>
                                                        <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                       placeholder="报名时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                           placeholder="报名时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                       placeholder="报价时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                           placeholder="报价时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                    <div class="tab-pane  example" id="tab5">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">


                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab5" class="searchFormTab" onsubmit="return false;">
                                                <div class="so-form">

                                                    <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                            placeholder="询价单号"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                            placeholder="询单标题"></div>

                                                    <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                            placeholder="创建人"></div>
                                                    <c:if test="${isSuperMan == 1}">
                                                        <div class="so-form-15 padding1"><input type="text" name="ouName" id="ouName5" class="form-control search_c" placeholder="采购组织2" autocomplete="off">
                                                        </div>
                                                        <div class="so-form-15 padding1" style="display: none;"><input name="uuCode" id="uuCode5" type="text" class="form-control search_c" >
                                                        </div>
                                                    </c:if>
                                                    <div class="so-form-15">
                                                        <button class="btn btn-primary btnSearch" type="button"><i
                                                                class="icon icon-search"></i> 搜索
                                                        </button>
                                                        <input class="btn ml12" type="reset" value="重置"/></div>

                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                       placeholder="创建时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                                type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                                class="form-control form-datetime"></div>
                                                        <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                       placeholder="报名时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                           placeholder="报名时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                       placeholder="报价时间(起)"
                                                                                       class="form-control form-datetime">
                                                        </div>
                                                        <div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                           placeholder="报价时间(止)"
                                                                                           class="form-control form-datetime">
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                                <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                                <div class="col-md-12" id="jqGridPager"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>
<%--       下拉搜索js start           --%>
<script>
    var chinese_fullname=['全部',
        '宝钢金属有限公司',
        '上海宝信软件股份有限公司采购商务管理室',
        '采购电子商务平台项目组（测试专用）',
        '江苏永钢集团有限公司',
        '上海宝钢新型建材科技有限公司',
        '宝钢股份黄石涂镀板有限公司',
        '东方钢铁电子商务有限公司',
        '宝钢发展工厂维护部',
        '上海梅山钢铁股份有限公司',
        '上海朝冶机电成套设备有限公司',
        '宝发展酒店物业管理有限公司',
        '宁波钢铁有限公司',
        '宝钢特钢有限公司',
        '宝钢不锈钢有限公司',
        '武汉钢铁重工集团冶金重工有限公司',
        '宝钢集团上海第一钢铁有限公司',
        '上海一钢企业开发有限公司',
        '特变电工山东鲁能泰山电缆有限公司',
        '上海昌新钢渣有限公司',
        '上海宝列冶金机电设备有限公司',
        '厦门宝钢精密钢材科技有限公司',
        '宝钢国际工厂管理部',
        '上海宝钢工业技术服务有限公司(旧)',
        '上海宝钢设备检修有限公司机械制造事业部',
        '上海十钢有限公司',
        '宝钢集团上海五钢有限公司',
        '上海宝顶能源有限公司',
        '上海宝钢物流有限公司',
        '上海宝钢工业公司塑料制品部',
        '常州宝菱重工机械有限公司',
        '上海宝华国际招标有限公司',
        '宝钢发展有限公司上海置业分公司',
        '宝钢发展制造服务事业部市场营销部',
        '上海江南轧辊有限公司',
        '宝钢苏冶重工有限公司',
        '上海宝钢铸造有限公司',
        '上海宝钢工程咨询有限公司',
        '郑州宝钢钢材加工配送有限公司',
        '宝武集团环境资源科技有限公司工业环境保障部',
        '上海宝钢工业公司各生产作业部',
        '上海宝钢高强钢加工配送有限公司',
        '上海宝江汽车贸易有限公司',
        '宝钢资源有限公司',
        '上海宝钢工贸有限公司',
        '上海宝钢磁业有限公司',
        '上海宝钢船板加工配送有限公司',
        '上海宝钢不锈钢贸易有限公司',
        '广州宝丰井汽车钢材加工有限公司',
        '天津宝钢钢材配送有限公司',
        '宝钢发展有限公司汽车通勤公司',
        '广州宝钢井昌钢材配送有限公司',
        '上海丰宝综合经营有限公司',
        '重庆宝井钢材加工配送有限公司',
        '福州宝井钢材有限公司',
        '上海宝钢工业有限公司',
        '济南宝钢钢材加工配送有限公司',
        '上海宝钢住商汽车贸易有限公司',
        '上海宝钢阿赛洛激光拼焊有限公司',
        '上海宝钢高新技术零部件有限公司',
        '上海宝钢液压成形零部件有限公司',
        '上海宝井钢材加工配送有限公司',
        '东莞宝钢特殊钢加工配送有限公司',
        '宝钢轧辊科技有限责任公司',
        '西安宝钢钢材加工配送有限公司',
        '长春一汽宝友钢材加工配送有限公司',
        '上海宝菱电气控制设备有限公司',
        '广州花都宝井汽车钢材部件有限公司',
        '安徽宝钢钢材配送有限公司',
        '烟台宝井钢材加工有限公司',
        '上海宝钢航运有限公司',
        '佛山宝钢不锈钢贸易有限公司',
        '宁波宝钢不锈钢加工有限公司',
        '上海宝钢包装钢带有限公司',
        '宝钢发展有限公司',
        '青岛宝井钢材加工配送有限公司',
        '宝钢集团上海二钢有限公司',
        '上海宝钢车轮有限公司',
        '沈阳宝钢钢材贸易有限公司',
        '南昌宝江钢材加工配送有限公司',
        '南京宝钢住商金属制品有限公司',
        '重庆宝钢汽车钢材部件有限公司',
        '柳州宝钢汽车钢材部件有限公司',
        '宝钢钢构有限公司',
        '天津宝钢储菱物资配送有限公司',
        '上海宝钢节能环保技术有限公司',
        '安徽皖宝矿业股份有限公司',
        '宝钢集团上海钢管有限公司',
        '长春宝钢钢材贸易有限公司',
        '宝钢工程技术集团有限公司',
        '特变电工新疆新能源股份有限公司',
        '武汉宝钢华中贸易有限公司',
        '宝钢建筑系统集成有限公司',
        '宝钢集团常州冶金机械厂',
        '广州宝钢南方贸易有限公司',
        '大连华锐重工集团股份有限公司',
        '苏州大方特种车股份有限公司',
        '安康市宝林矿业有限公司',
        '南京梅山冶金发展有限公司',
        '成都宝钢汽车钢材部件加工配送有限公司',
        '大连宝友金属制品有限公司',
        '上海宝钢商贸有限公司',
        '上海鹰峰电子科技股份有限公司',
        '烟台宝钢车轮有限公司',
        '上海宝松重型机械工程(集团)有限公司',
        '宁波宝井钢材加工配送有限公司',
        '大峘集团有限公司',
        '上海金艺检测技术有限公司',
        '天津宝井钢材加工配送有限公司',
        '上海申宝汽车服务有限公司',
        '长沙宝钢钢材加工配送有限公司',
        '宝钢德盛不锈钢有限公司',
        '吉林市一汽宝钢汽车钢材部件有限公司',
        '江苏宝钢精密钢丝有限公司',
        '宝钢发展有限公司职业健康公司',
        '宝钢发展有限公司物业管理部',
        '宝钢发展湛江有限公司',
        '宝钢物流(江苏)有限公司',
        '上海宝钢工业技术服务有限公司',
        '上海宝钢智能化停车库有限公司',
        '常熟宝升精冲材料有限公司',
        '佛山三水宝钢钢材部件有限公司',
        '襄阳宝林泓钢材加工配送有限公司',
        '上海宝钢生态绿化有限公司',
        '上海宝通运输实业有限公司',
        '河南森源电气股份有限公司',
        '东方付通信息技术有限公司',
        '上海开拓磁选金属有限公司',
        '重庆宝钢美威车轮有限公司',
        '上海宝钢运输有限公司',
        '诚融(上海)动产信息服务有限公司',
        '韶钢设备管理部',
        '欧冶云商股份有限公司',
        '湛江宝发赛迪转底炉技术有限公司',
        '北京天润新能投资有限公司西北分公司',
        '德阳立达机电设备有限公司',
        '上海欧冶数据技术有限责任公司',
        '重庆宝吉汽车零部件有限公司',
        '浙江联鑫板材科技有限公司',
        '东莞宝钢钢材部件有限公司',
        '上海矿石国际交易中心有限公司',
        '上海宝钢物流有限公司湛江分公司',
        '博创智能装备股份有限公司',
        '上海宝钢拆车有限公司',
        '上海欧冶材料技术有限责任公司',
        '上海欧冶采购信息科技有限责任公司',
        '重庆宝钢钢材加工配送有限公司',
        '江苏永联精筑建设集团有限公司',
        '江门市华津金属制品有限公司',
        '汶瑞机械(山东)有限公司',
        '凌源钢铁股份有限公司',
        '武汉钢铁重工集团有限公司',
        '佛山市高明基业冷轧钢板有限公司',
        '江苏宝京汽车部件有限公司',
        '上海宝钢磁业有限公司湛江分公司',
        '冀中能源股份有限公司',
        '上海宝钢浦东国际贸易有限公司',
        '银邦金属复合材料股份有限公司',
        '浙江迅定钢铁有限公司',
        '上海欧冶资源电子商务有限公司',
        '柳州宝钢汽车零部件有限公司',
        '上海宝钢汽车检测修复有限公司',
        '内蒙古大中矿业股份有限公司',
        '上海宝钢气体有限公司',
        '万华化学集团股份有限公司',
        '天津海钢板材有限公司',
        '杭州宝井钢材加工配送有限公司',
        '上海辉钢机电设备科技有限公司',
        '上海三冠钢铁有限公司',
        '江苏长强钢铁有限公司',
        '达州市善智商贸有限公司',
        '山东光明工模具制造有限公司',
        '山西聚鑫物云电子科技有限公司',
        '上海宝钢月新汽车零部件有限公司',
        '威远双宏贸易有限公司',
        '上海魏投实业有限公司',
        '河北宝钢制罐北方有限公司',
        '盐城市联鑫钢铁有限公司',
        '威海威高进出口有限公司',
        '磐石铸诚无缝钢管有限公司',
        '上海宝钢建筑工程设计研究院',
        '河南中原云工有限责任公司',
        '山东冠洲股份有限公司',
        '上海欧珏供应链管理有限公司',
        '宝钢集团宝山宾馆',
        '武汉宝钢印铁有限公司',
        '武汉宝钢制罐有限公司',
        '河南宝钢制罐有限公司',
        '哈尔滨宝钢制罐有限公司',
        '成都宝钢制罐有限公司',
        '佛山宝钢制罐有限公司',
        '易买服科采购服务有限公司',
        '宝钢集团宝山宾馆',
        '上海宝信软件股份有限公司深圳分公司',
        '北海诚德金属压延有限公司',
        '上海宝聚表面技术有限公司',
        '河北宝钢制罐北方有限公司',
        '朝阳金达集团实业有限公司',
        '上海宝钢制盖有限公司',
        '江苏江南创佳型材有限公司',
        '安阳钢铁股份有限公司',
        '宝钢集团南通线材制品有限公司',
        '武汉钢铁集团鄂城钢铁有限责任公司',
        '郑州点检企业管理咨询有限公司',
        '朝阳金达钛业股份有限公司'
    ];
    var bsp_company_code=['',
        'U57476',
        'U34515',
        'U34663',
        'U56362',
        'U35402',
        'U21284',
        'U09632',
        'U34499',
        'U34514',
        'U21643',
        'U51964',
        'U21092',
        'U34503',
        'U34504',
        'U33188',
        'U34511',
        'U34512',
        'U34128',
        'U34323',
        'U33325',
        'U37054',
        'U34519',
        'U50296',
        'U34517',
        'U34520',
        'U22239',
        'U51762',
        'U34523',
        'U34507',
        'U53228',
        'U21002',
        'U52182',
        'U54162',
        'U54689',
        'U53231',
        'U33174',
        'U53631',
        'U57421',
        'U57471',
        'U57512',
        'U32851',
        'U05633',
        'U55941',
        'U01759',
        'U56201',
        'U09328',
        'U00124',
        'U06568',
        'U20909',
        'U56116',
        'U21012',
        'U33983',
        'U20515',
        'U04755',
        'U27075',
        'U59226',
        'U21264',
        'U21184',
        'U59157',
        'U59153',
        'U28078',
        'U59640',
        'U60232',
        'U20973',
        'U10247',
        'U10401',
        'U20885',
        'U01505',
        'U07240',
        'U53485',
        'U00121',
        'U10267',
        'U07395',
        'U31843',
        'U04846',
        'U59073',
        'U58847',
        'U53541',
        'U21120',
        'U07226',
        'U21748',
        'U06358',
        'U07041',
        'U21178',
        'U67585',
        'U61427',
        'U23447',
        'U00126',
        'U33175',
        'U79675',
        'U02294',
        'U87113',
        'U02249',
        'U01378',
        'U34790',
        'U42365',
        'U42084',
        'U53384',
        'U53303',
        'U52544',
        'U00109',
        'U12937',
        'U37376',
        'U21687',
        'U13990',
        'U42615',
        'U42697',
        'U19642',
        'U04702',
        'U68706',
        'U43761',
        'U90593',
        'U11769',
        'U48957',
        'U57303',
        'UA4649',
        'U59058',
        'U55236',
        'U73589',
        'U61707',
        'U74689',
        'U56602',
        'U89656',
        'U07080',
        'U98082',
        'U72278',
        'U34518',
        'U43550',
        'U21835',
        'UB3459',
        'UB4926',
        'UA5722',
        'UB7831',
        'UB7992',
        'U33911',
        'UC4145',
        'U77105',
        'U16390',
        'UC4889',
        'UC5350',
        'UC7580',
        'UC8475',
        'UB6604',
        'UD1051',
        'UD2085',
        'U92037',
        'UD2196',
        'U56609',
        'UA6781',
        'UD4745',
        'U36616',
        'U05159',
        'UD5061',
        'UD5954',
        'UD6238',
        'U00113',
        'U64033',
        'U32441',
        'UD8438',
        'UD8221',
        'U21845',
        'UD9473',
        'U55922',
        'U60050',
        'UE0709',
        'U20991',
        'UE1601',
        'U25999',
        'U70641',
        'UE2166',
        'UE3113',
        'UE3582',
        'UE3814',
        'UE2010',
        'UE5173',
        'U06221',
        'U24656',
        'UE5167',
        'UE4738',
        'U21176',
        'UE6781',
        'U44898',
        'UE6863',
        'U60400',
        'U64598',
        'U36943',
        'U71585',
        'UD7391',
        'U07265',
        'U21161',
        'UF0057',
        'U60400',
        'U42541',
        'UE8489',
        'U34143',
        'U06221',
        'UE9030',
        'U61168',
        'UF0205',
        'U82128',
        'U60372',
        'U59361',
        'UG9158',
        'U64382'
    ];
//        $.ajax({
//            url:ctx+"/rfqRequest/getCompanyInfo",    //请求的url地址
//            dataType:"json",   //返回格式为json
//            async:true,//请求是否异步，默认为异步，这也是ajax重要特性
////                data:{"id":"value"},    //参数值,post才用
//            type:"GET",   //请求方式
//            beforeSend:function(){
//                //请求前的处理
//            },
//            success:function(data){
//                //请求成功时处理
//                for (var i=0;i<data.list.length;i++){
//                    bsp_company_code[i]=data.list[i].bsp_company_code;
//                    chinese_fullname[i]=data.list[i].chinese_fullname;
//                }
//            },
//            complete:function(){
//                //请求完成的处理
//            },
//            error:function(){
//                //请求出错处理
//            }
//        });



    $('.search_c').on("keyup",listShow);
    $('.search_c').on("focus",listShow);
    $('.search_c').on("click",function () {
        if (event.stopPropagation) {
            event.stopPropagation();
        }
        else if (window.event) {
            window.event.cancelBubble = true;
        }
    });
    function listShow() {
        var that = this;
        var bool = false;
            if($('#search_area').length>0){

        }else{
            $(this).parents('.tab-content').append("<ul id='search_area'></ul>");
        }
        $('#search_area').css("display", "block");
        $('#search_area').css("width","300px");
//        $('#search_area').css("width",$(this).parent().width()+"px");
        $('#search_area').css("top",$(this).position().top+30+"px");
        $('#search_area').css("left",$(this).position().left+"px");

        $('#search_area>li').remove();

        for (var i = 0; i < chinese_fullname.length; i++) {
            if(chinese_fullname[i].length>= $(this).val().length){
                for(var t=0; t< chinese_fullname[i].length- $(this).val().length+1; t++){
                    if ($(this).val() == chinese_fullname[i].slice(t, (t + $(this).val().length))) {
                        bool = true;
                        $('#search_area').append('<li>' + chinese_fullname[i] + "</li>");
                        break;
                    }
                }
                bool=false;
            }
        }
        $('#search_area>li').on("click", function () {
            $(that).val($(this).html());
            $('#search_area').css("display", "none");
        })
        $("#search_area>li").hover(function () {
            $(this).addClass("hover");
        }, function () {
            $(this).removeClass("hover");
        });
        bool = false;
    }

    $(document).on("click",function () {
        if ($('#search_area').length > 0) {
            $('#search_area').css("display", "none");
        }
    })
</script>
<%--       下拉搜索js end           --%>

<script>

    $(function () {

        $('.more-so').click(function () {

            $('.hiderow').stop().slideToggle('fast');

        });


        //日期

        $(".form-datetime").datetimepicker(
                {

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    forceParse: 0,

                    showMeridian: 1,

                    format: "yyyy-mm-dd hh:ii"

                });


        $(".form-date").datetimepicker(
                {

                    language: "zh-CN",

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    minView: 2,

                    forceParse: 0,

                    format: "yyyy-mm-dd"

                });


        $(".form-time").datetimepicker({

            language: "zh-CN",

            weekStart: 1,

            todayBtn: 1,

            autoclose: 1,

            todayHighlight: 1,

            startView: 1,

            minView: 0,

            maxView: 1,

            forceParse: 0,

            format: 'hh:ii'

        });


    });

</script>

</body>

</html>
