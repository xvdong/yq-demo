<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>欧冶采购成交通知单</title>
    <style type="text/css">
        body{padding-bottom:20px;background:#fff; color:#333; font-family:SimSun;}
        ul,li{list-style:none; margin:0; padding:0;}
        .v-align{vertical-align:middle !important;}
        .container {max-width:1000px;margin-top:12px;margin:0 auto;}
        .container_main{padding:0 0 10px 0; margin-top:0;}
        h1{
            text-align: center; margin-bottom: 50px; color: #666;
        }
        .rightbar{ height:100%; }
        .table.align-md td,.table.align-md th{ text-align:center; vertical-align:middle;}
        .mg-a-text{color:#ea644a;margin-right:8px}
        .mg-margin-right{margin-right:40px;font-size:16px}
        .mg-header{line-height:16px;position:relative;font-size:16px;padding-bottom:10px;margin-bottom:15px;clear:both}

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border:1px solid #666;

        }
        .table  th ,td{
            border:1px solid #666;
            line-height:46px;
            text-align: center;
            font-size:12px;
            color: #333;
            padding:6px 0 ;
            border-spacing:0;
            vertical-align:middle;
        }
        .table tr td:first-child{
            border-left: 1px solid #666;
        }
        .table  th{
            border-top:1px solid #666;
        }
        .table  th:first-child{
            border-left: 1px solid #666;
        }
        .table1{
            border:1px solid #666;
        }
        .table1 th,.table1 td{
            height:46px; text-align: left; border:0;
        }
        .table1 th {
            background-color: #f5f5f5;
        }
        td label{
            color: #333;
            font-weight: bold;
        }
        .panel .table1 th,.panel .table1 td{
            height: 16px;
            line-height: 16px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container container_main">
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">
                    <h1>欧冶采购成交通知单</h1>
                    <div class="panel">
                        <table class="table table1">
                            <tr>
                                <th colspan="3" style="padding-left: 10px;">基本情况</th>
                            </tr>

                            <tbody>
                            <tr>
                                <td style="padding-left: 10px;">采购组织:${ouName3}</td>
                                <td>供应商名称:${winSupplierName}</td>
                            </tr>
                            <tr>
                                <td style="padding-left: 10px;">询价单号:${ouRfqNum3}</td>
                                <td>询单标题:${title}</td>
                                <td>币种:${currency3}</td>
                            </tr>
                            <tr>
                                <td style="padding-left: 10px;">报价人:${quotationer}</td>
                                <c:if test="${priceType == '1'}">
                                    <td>中标含税总价:${winAllMoney}</td>
                                </c:if>
                                <c:if test="${priceType == '0'}">
                                    <td>中标未税总价:${winAllMoney}</td>
                                </c:if>
                                <%--出现中标总价时表明出bug了--%>
                                <c:if test="${priceType == 'N'}">
                                    <td>中标总价:${winAllMoney}</td>
                                </c:if>
                                <td>中标物料条数:${numberOfThings}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <%--<div class="mg-header">--%>
                        <%--物料明细：--%>
                    <%--</div>--%>
                    <%--<div class="tab-content">--%>
                        <%--<table class="table">--%>
                            <%--<thead>--%>
                            <%--<tr>--%>
                                <%--<th>物料代码</th>--%>
                                <%--<th>物料名称</th>--%>
                                <%--<th>型号规格</th>--%>
                                <%--<th>生产厂家</th>--%>
                                <%--<th>采购数量</th>--%>
                                <%--<th>计量单位</th>--%>
                                <%--<th>交货期</th>--%>
                            <%--</tr>--%>
                            <%--</thead>--%>
                            <%--<tbody>--%>
                            <%--<c:forEach items="${thingsList3}" var="item" varStatus="status">--%>
                                <%--<tr>--%>

                                    <%--<td>${item.materialNo}</td>--%>
                                    <%--<td>${item.materialName}</td>--%>
                                    <%--<td>${item.character}</td>--%>
                                    <%--<td>${item.producer}</td>--%>
                                    <%--<td>${item.requestAmount}</td>--%>
                                    <%--<td>${item.unit}</td>--%>
                                    <%--<td>${item.requestDeliveryDate}</td>--%>
                                <%--</tr>--%>
                            <%--</c:forEach>--%>
                            <%--</tbody>--%>
                        <%--</table>--%>
                    <%--</div>--%>


                    <div class="mg-header" style="border-bottom: none">
                        中标物料情况：
                    </div>
                    <div class="tab-content">
                        <table class="table table-bordered align-md">
                            <thead>
                            <%--<tr>--%>
                                <%--<th colspan="7" style="text-align: left; padding-left: 10px;">中标情况如下:</th>--%>
                            <%--</tr>--%>
                            </thead>
                            <tbody>


                                <%--<tr>--%>
                                    <%--<td>中标单位:${winSupplierName}</td>--%>
                                    <%--<td>中标总价:${winAllMoney}</td>--%>
                                    <%--<td colspan="5">报价备注:${quotationMemo}</td>--%>
                                <%--</tr>--%>
                                <tr>
                                    <th>物料代码</th>
                                    <th>物料名称</th>
                                    <th>型号规格</th>
                                    <th>生产厂家</th>
                                    <th>拟签数量</th>
                                    <c:if test="${priceType == '1'}">
                                        <th>拟签含税价格</th>
                                    </c:if>
                                    <c:if test="${priceType == '0'}">
                                        <th>拟签未税价格</th>
                                    </c:if>
                                    <c:if test="${priceType == 'N'}">
                                        <th>拟签价格</th>
                                    </c:if>
                                    <th>计量单位</th>
                                    <th>交货期</th>
                                </tr>

                                <c:forEach items="${thirdList3}" var="item2" varStatus="status">
                                        <tr>
                                            <td>${item2.materialNo}</td>
                                            <td>${item2.materialName}</td>
                                            <td>${item2.character}</td>
                                            <td>${item2.producer}</td>
                                            <td>${item2.confirmedAmout}</td>
                                            <td>${item2.wantToOnePrice}</td>
                                            <td>${item2.unit}</td>
                                            <td>${item2.giveDate}</td>
                                        </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div  style="padding-bottom: 20px; float:right;color:#666"><strong>日期：${date3}</strong></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
