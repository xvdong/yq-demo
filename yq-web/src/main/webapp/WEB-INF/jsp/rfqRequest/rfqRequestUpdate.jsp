<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>询单编辑</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <!--富文本JS-->
    <%@include file="../common/kingEditor.jsp"%>
    <script src="${resource}/js/rfq.datatable.edit.js?v=${version}"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestEdit.js?v=${version}"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestEdit4item.js?v=${version}"></script>
    <script src="${resource}/lib/plupload-2.1.9/js/plupload.full.min.js"></script>
    <script src="${resource}/js/upload/uploadFile.js?v=${version}"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
    <script src="${resource}/lib/datetimepicker/datetimepicker.js"></script>
    <style>
        .datatable{ margin-bottom:10px;}
        .flexarea .table th,td{  text-align:center; vertical-align:middle;}
        .fixed-left .table th,td{text-align:center; vertical-align:middle;}
        .table .dropdown-menu{left:120px !important; top:-28px !important;}
        em{font-style: normal}
    </style>
</head>

<body data-moudle="edit" data-id="${param.id}" id="s-body">
<input type="hidden" id="data-moudle" value="edit">
<input type="hidden" id="data-id" value="${param.id}">
<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <input id="dataSource" type="hidden" value="${rfqRequestComplexVo.requestVo.dataSource}"/>
        <div class="rightbar clearfix">
            <form id="dataform" >
                <input type="hidden" name="requestBo.id" value="${param.id}">
                <input type="hidden" name="requestBo.unifiedRfqNum" value="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                <div class="container">
                    <div class="breadcrumbs wstyle">
                        <ol class="breadcrumb">
                                                    <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                            <li class="active">发布询价单</li>
                            <button type="button" class="btn btn-primary pull-right" id="changeViewBtn" data-toggle="modal" data-target="#changeViewModal"><i class="icon icon-columns"></i></button>
                        </ol>
                    </div>
                    <div class="page-content wstyle">
                        <c:if test="${auditing.billTypeCode =='RFQ001'}">
                            <c:if test="${auditing.status == '70'}">
                                <c:if test="${auditing.auditAdvice != ''}">
                                    <div class="alert alert-danger clearfix mg-alert-danger">
                                        <i class="icon icon-exclamation-sign mt3 ml5"></i>
                                        <div class="pull-left ml12">
                                            <p>审批结果：已驳回</p>
                                            <p>驳回理由：${auditing.auditAdvice}</p>
                                        </div>
                                        <a href="#" class="close" style="font-size:26px; height:45px; line-height:45px;"
                                           data-dismiss="alert" onclick="changeHigeht(this)">×</a>
                                    </div>
                                </c:if>
                            </c:if>
                        </c:if>
                        <!-- uh start  -->
                        <div id="stepBar" class="ui-stepBar-wrap nav-rightin" style="display: block;">
                            <div class="ui-stepBar">
                                <div class="ui-stepProcess" style="width: 0%;"></div>
                            </div>
                            <div class="ui-stepInfo-wrap">
                                <table class="ui-stepLayout" border="0" cellpadding="0" cellspacing="0" style="width: 1089px; margin-left: -67px;">
                                    <tbody><tr>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-pre judge-stepSequence-pre-change" style="padding: 6px 12px;">1</a>
                                            <p class="ui-stepName">采购物料</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change" style="padding: 4px 10px;">2</a>
                                            <p class="ui-stepName">采购角色设置</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">3</a>
                                            <p class="ui-stepName">供应商设置</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">4</a>
                                            <p class="ui-stepName">询单规则</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">5</a>
                                            <p class="ui-stepName">商务条款与技术条款</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">6</a>
                                            <p class="ui-stepName">采购联系方式</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">7</a>
                                            <p class="ui-stepName">询单附件</p>
                                        </td>
                                        <td class="ui-stepInfo" style="width: 155px;">
                                            <a class="ui-stepSequence judge-stepSequence-hind judge-stepSequence-hind-change">8</a>
                                            <p class="ui-stepName">询单备注</p>
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>
                        <!-- uh end  -->


                        <div title="基本信息" class="panel panel-block ke-toolbar" id="topBarInfo">
                            <div class="col-md-12">
                                <div class="row mt12">
                                    <div class="re" style="color:#666;">
                                        <div class="so-form">
                                            <input name="requestBo.rfqMethod"  type="hidden" value="RAQ">
                                            <input name="requestBo.type"  type="hidden" value="0">
                                            <input name="requestBo.ouId"  type="hidden" value="">
                                            <input name="requestBo.ouName"  type="hidden" value="">
                                            <div class="left mr12 line-32"><i class="r">*</i>询价单标题：</div>
                                            <div class="so-form-30 padding1">
                                                <input name="requestBo.title" value="${rfqRequestComplexVo.requestVo.title} "  class="form-control so-form-25 input" placeholder="以物料名称命名，例如：计算机采购" type="text">
                                            </div>
                                            <div class="ml12 left mr12 line-32">计划编号：</div>
                                            <div class="so-form-20 padding1">
                                                <input name="requestBo.planNo" value="${rfqRequestComplexVo.requestVo.planNo}" class="form-control input" placeholder="请填写计划编号" type="text">
                                            </div>
                                            <div class="right mr12 line-32"> <a class="btn  btn-primary" data-toggle="modal" data-target="#myModa1"> <i class="icon icon-plus-sign"></i>&nbsp;从历史询单创建</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div title="采购物料" class="panel panel-block ke-toolbar">
                            <div class="clearfix" id="requestItemData">
                                <div class="mg-header5 mt12" style=" margin-bottom:15px;">
                                    <div class="left font-16-y16">物料信息</div>
                                    <span class="right line-32" style=" display:inline-block">预算总价：<strong class="red"><em id="totalPrice"><c:out value="${rfqRequestComplexVo.requestVo.totalPrice}" default="0.00"/></em>元</strong></span>
                                </div>


                                <table class="table table-bordered table-hove align-md datatable" id="requestItemTable">
                                    <thead>
                                    <tr>
                                        <th data-width="50">序号</th>
                                        <c:if test="${rfqRequestComplexVo.requestVo.dataSource !='JFE' && rfqRequestComplexVo.requestVo.dataSource !='ygck' && rfqRequestComplexVo.requestVo.dataSource !='ygzb' && rfqRequestComplexVo.requestVo.dataSource !='NBBX'}">
                                        <th data-width="70" id="operate">操作</th>
                                        </c:if>
                                        <th data-width="120" class="flex-col"><i class="r">*</i>物料代码</th>
                                        <th data-width="180" class="flex-col"><i class="r">*</i>物料名称</th>
                                        <th data-width="180" class="flex-col"><i class="r">*</i>型号规格</th>
                                        <th data-width="180" class="flex-col">生产厂家（品牌）</th>
                                        <th data-width="90" class="flex-col"><i class="r">*</i>采购数量</th>
                                        <th data-width="90" class="flex-col"><i class="r">*</i>计量单位</th>
                                        <c:if test="${rfqRequestComplexVo.requestVo.dataSource !='JFE'&& rfqRequestComplexVo.requestVo.dataSource !='ygck' && rfqRequestComplexVo.requestVo.dataSource !='ygzb' && rfqRequestComplexVo.requestVo.dataSource !='NBBX'}">
                                            <th data-width="150" class="flex-col">
                                                <i class="r">*</i> 参考单价（元）
                                                <div class="btn-group">
                                                    <button class="btn btn-hui dropdown-toggle" type="button"
                                                            id="dropdownMenu1" data-toggle="dropdown">
                                                        上一次成交单价<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu"
                                                        aria-labelledby="dropdownMenu1">
                                                        <li><a href="#">上一次成交单价</a></li>
                                                        <li><a href="#">历史最低单价</a></li>
                                                    </ul>
                                                </div>
                                            </th>
                                        </c:if>
                                        <c:if test="${rfqRequestComplexVo.requestVo.dataSource =='JFE' || rfqRequestComplexVo.requestVo.dataSource =='ygck' || rfqRequestComplexVo.requestVo.dataSource =='ygzb' || rfqRequestComplexVo.requestVo.dataSource =='NBBX'}">
                                            <th data-width="150" class="flex-col">
                                                <i class="r">*</i> 参考单价（元）
                                            </th>
                                        </c:if>
                                        <th data-width="100" class="flex-col"><span><i class="r">*</i>交货期</span>
                                            <c:if test="${rfqRequestComplexVo.requestVo.dataSource !='JFE' && rfqRequestComplexVo.requestVo.dataSource !='ygck' && rfqRequestComplexVo.requestVo.dataSource !='ygzb'}">
                                                <div class="input-group date form-date requestDeliveryDate" data-date=""
                                                     data-date-format="dd MM yyyy"
                                                     data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                    <input id="requestDeliveryDate2"
                                                           class="form-control requestDeliveryDate" size="16"
                                                           type="text"
                                                           style="display: none;">
                                                    <span class="input-group-addon"><span class="icon-calendar"></span></span>
                                                </div>
                                            </c:if>
                                        </th>
                                        <c:if test="${rfqRequestComplexVo.requestVo.dataSource =='JFE'}">
                                            <th data-width="150" class="flex-col">申购单号</th>
                                            <th data-width="250" class="flex-col">申购人</th>
                                            <th data-width="100" class="flex-col">图号</th>
                                        </c:if>
                                        <c:if test="${rfqRequestComplexVo.requestVo.dataSource =='NBBX'}">
                                            <th data-width="100" class="flex-col">材质/图号</th>
                                        </c:if>
                                        <th data-width="100" class="flex-col">备注</th>
                                        <!--  自定义物料字段 -->
                                        <c:forEach var="ext" items="${requestItemExtraVo}">
                                            <th data-width="100" class="flex-col ext-col" data-extname="${ext.itemName}" data-extcode="${ext.itemCode}" data-extid="${ext.id}">${ext.itemName}</th>
                                        </c:forEach>
                                    </tr>
                                    </thead>
                                    <tbody id="itemList">
                                    <%--<c:forEach var="requestItemList" items="${rfqRequestComplexVo.requestItemList}" varStatus="status">--%>
                                    <%--<tr>--%>
                                    <%--<td>${status.index+1}</td>--%>
                                    <%--<td><input data-name="materialNo" type="text" class="form-control input-sm" value="${requestItemList.materialNo}"></td>--%>
                                    <%--<td><input data-name="materialName" type="text" class="form-control input-sm" value="${requestItemList.materialName}"></td>--%>
                                    <%--<td><input data-name="requestAmount" type="text" class="form-control input-sm js-change" value="${requestItemList.requestAmount}"></td>--%>
                                    <%--<td><input data-name="unit" type="text" class="form-control input-sm" value="${requestItemList.unit}"></td>--%>
                                    <%--<td><input data-name="character" type="text" class="form-control input-sm" value="${requestItemList.character}"></td>--%>
                                    <%--<td><input data-name="producer" type="text" class="form-control input-sm" value="${requestItemList.producer}"></td>--%>
                                    <%--<td><input data-name="specification" type="text" class="form-control input-sm" value="${requestItemList.specification}"></td>--%>
                                    <%--<td><input data-name="salePrice" type="text" class="form-control input-sm js-change" value="${requestItemList.salePrice}"></td>--%>
                                    <%--<td><input data-name="requestDeliveryDate" type="text" class="form-control input-sm form-date" value="${requestItemList.requestDeliveryDate}"></td>--%>
                                    <%--<td><input data-name="memo" type="text" class="form-control input-sm" value="${requestItemList.memo}"></td>--%>
                                    <%--<td><a href="javascript:;" class="js-del"><i class="icon icon-trash red"></i>&nbsp;删除</a></td>--%>
                                    <%--</tr>--%>
                                    <%--</c:forEach>--%>
                                    </tbody>
                                </table>
                                <!--  按钮组 -->
                                <div class="left" id="buttonArray">
                                    <div class="so-form-40 mt12">
                                        <button class="btn btn-primary btn-sm" type="button" id="addRow"><i class="icon icon-plus"></i> 新增一行</button>

                                        <button class="btn btn-success btn-sm" type="button" onClick="excelClicke()" data-position="center" data-toggle="modal" data-target="#cancelPrice33">
                                            <i class="icon icon-signout"></i> excel模版
                                        </button>
                                        <button class="btn btn-danger btn-sm" type="button" id="clearData"><i class="icon icon-warning-sign"></i> 清空物料</button>
                                    </div>
                                </div>
                                <!--  分页 -->
                                <div class="so-form-60 right mt20">
                                    <div class="pull-right" style="margin-top:-18px;" id="d_pager">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div title="角色设置" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">角色设置</span>
                                <button class="btn right btn-warning btn-sm" type="button"  data-toggle="modal" data-target="#myModaphone" id="prouser"><i class="icon icon-plus"></i> 新增角色</button>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32 mr12 ml12">
                                        <table class="table table-bordered table-hove  align-md" id="roleTable">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>工号</th>
                                                <th>姓名</th>
                                                <th>部门</th>
                                                <th>职位</th>
                                                <th width="80">查看</th>
                                                <th width="80">操作</th>
                                                <th width="80">财务管理</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="item" items="${rfqRequestComplexVo.projectUserList}" varStatus="status">
                                                <tr id="puser_${item.userId}">
                                                    <td data-name="userId" data-value="${item.userId}">${status.index+1}</td>
                                                    <td data-name="jobCode" data-value="${item.jobCode}">${item.jobCode}</td>
                                                    <%--<td data-name="jobCode" data-value="${item.jobCode}">1205050319</td>--%>
                                                    <td data-name="userName" data-value="${item.userName}">${item.userName}</td>
                                                    <%--<td data-name="userOffice" data-value="${item.userOffice}">${item.userOffice}</td>--%>
                                                    <td data-name="userOffice" data-value="采购部">采购部</td>
                                                    <%--<td data-name="userTitle" data-value="${item.userTitle}">${item.userTitle}</td>--%>
                                                    <td data-name="userTitle" data-value="采购部门采购">采购部门采购</td>
                                                    <td>
                                                        <input <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if> type="checkbox" <c:if test="${status.index==0}">disabled="disabled"</c:if>/>
                                                    </td>
                                                    <td>
                                                        <input <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if> type="checkbox" <c:if test="${status.index==0}">disabled="disabled"</c:if>>
                                                    </td>
                                                    <td>
                                                        <input  <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if> type="checkbox" <c:if test="${status.index==0}">disabled="disabled"</c:if> <c:if test="${status.index>0}">class="js-money"</c:if>>
                                                    </td>
                                                    <td>
                                                        <c:if test="${status.index>0}"><a class="order-del-btn" data-id="${item.userId}"><i class="icon icon-trash red"></i>&nbsp; 删除</a></c:if>

                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div title="供应商设置" class="panel panel-block ke-toolbar">
                            <input id="publicBiddingFlag" type="hidden" value="${rfqRequestComplexVo.rulesVo.publicBiddingFlag}"/>
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">供应商设置</span>
                            </div>
                            <div class="panel-body font12" style="padding-left:30px; padding-right:25px;">
                                <strong class="left mr12">邀请范围：</strong>
                                <ul class="left" id="checkedDiv">
                                    <li id="publicBiddingFlagOne"><label><input type="radio" name="rulesBo.publicBiddingFlag" value="1"
                                                      <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">checked</c:if>> 公开询价</label><span
                                            class="ml12">面向平台注册供应商公开寻源，供应商需先报名，待采购组织审核通过后再报价。</span></li>
                                    <li id="publicBiddingFlagTwo"><label><input type="radio" name="rulesBo.publicBiddingFlag" value="0"
                                                      <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='0'}">checked</c:if>> 定向询价</label><span
                                            class="ml12">将询价单信息向特邀供应商公布，供应商无须报名可直接报价。</span></li>
                                </ul>
                                <div class="clearfix"></div>
                                <div class="mt12">

                                    <div class="fb-zz">
                                        <strong>供应商报名要求：</strong>
                                        <div class="mt12"><i class="r">*</i>注册资本：<input name="preauditBo.regcapital" type="text" class="form-control padding1"
                                                                                        style="width:150px; display:inline-block"
                                                                                        value="${rfqRequestComplexVo.preauditVo.regcapital}"><span
                                                class="red ml5">万元</span></div>
                                        <div class="mt12 ">
                                            资质要求：
                                            <div style="display:inline-block">
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'ISO9001质量体系认证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="00"> ISO9001质量体系认证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'ISO14001环境管理质量体系认证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="10"> ISO14001环境管理质量体系认证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'职业健康安全管理体系认证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="40"> 职业健康安全管理体系认证</label>
                                                <a id="more" href="javascript:;" class="blue" data-toggle="collapse" data-target="#collapseButton"><i
                                                        class="icon icon-double-angle-down"></i> 更多资质要求</a>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="collapseButton" class="
                                                    <c:choose>
                                                        <c:when test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书')}">
                                                            in
                                                        </c:when>
                                                        <c:otherwise>
                                                            collapse
                                                        </c:otherwise>
                                                    </c:choose>
                                                    "
                                                 style="margin-left:68px;">
                                                <%--<label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书1' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="资质证书1"> 资质证书1</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书2' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="资质证书2"> 资质证书2</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书3' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="资质证书3"> 资质证书3</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书4' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="资质证书4"> 资质证书4</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书5' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="资质证书5"> 资质证书5</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'资质证书6' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="资质证书6"> 资质证书6</label>--%>

                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'ISO/TS16949汽车工业管理体系认证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="70"> ISO/TS16949汽车工业管理体系认证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'ISO10012测量管理体系认证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="150"> ISO10012测量管理体系认证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'特种设备制造许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="255"> 特种设备制造许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'特种设备设计许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="256"> 特种设备设计许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'特种设备安装改造维修许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="257"> 特种设备安装改造维修许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'化学危险物品经营许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="258"> 化学危险物品经营许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'安全生产许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="259"> 安全生产许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'防爆合格证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="260"> 防爆合格证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'非药品类易制毒化学品经营备案证明' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="261"> 非药品类易制毒化学品经营备案证明</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'全国工业产品生产许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="262"> 全国工业产品生产许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'道路运输经营许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="263"> 道路运输经营许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'制造计量器具许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="264"> 制造计量器具许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'中国国家强制性产品认证证书' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="265"> 中国国家强制性产品认证证书</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'辐射安全许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="266"> 辐射安全许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'冶金标准样品认可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="267"> 冶金标准样品认可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'气瓶充装许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="268"> 气瓶充装许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'食品批发（流通、卫生）许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="269"> 食品批发（流通、卫生）许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'民用核承压设备设计资格许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="270"> 民用核承压设备设计资格许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'民用核承压设备制造资格许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="271"> 民用核承压设备制造资格许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'民用核安全设备制造许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="272"> 民用核安全设备制造许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'民用核安全设备设计许可证' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="273"> 民用核安全设备设计许可证</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'防雷工程专业设计资质证书' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="274"> 防雷工程专业设计资质证书</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'防雷工程专业施工资质证书' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="275"> 防雷工程专业施工资质证书</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'印刷经营许可' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="276"> 印刷经营许可</label>
                                                <label><input
                                                        <c:if test="${fn:contains(rfqRequestComplexVo.preauditVo.qualifications,'成品油批发经营许可' )}">checked</c:if>
                                                        name="preauditBo.qualifications" type="checkbox" value="277"> 成品油批发经营许可</label>
                                            </div>
                                        </div>


                                        <div class="mt12 ">
                                            报名要求：
                                            <textarea id="contentSimple" class="form-control kindeditorSimple col-md-10"
                                                      style=" width:100%;height:150px;">${rfqRequestComplexVo.preauditVo.requirementDesc}</textarea>
                                        </div>
                                        <div class="mt12">
                                            <div class="clearfix line-32">
                                                附件清单：
                                                <button class="btn right btn-warning btn-sm attachmentBtn" type="button" data-toggle="modal"
                                                        data-target="#myModa2" data-type="P"><i class="icon icon-plus"></i> 新增附件
                                                </button>
                                            </div>
                                            <table class="table table-bordered align-md fixed" style="margin-bottom:5px;">
                                                <thead>
                                                <tr>
                                                    <th width="50px">序号</th>
                                                    <th>文件名称</th>
                                                    <th width="100px">文件大小</th>
                                                    <th>说明</th>
                                                    <th width="100px">操作</th>
                                                </tr>
                                                </thead>
                                                <tbody id="attachmentList1">
                                                <c:set var="sum" value="1"/>
                                                <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}" varStatus="status">
                                                    <c:if test="${item.type == 'P'}">
                                                        <tr data-clone="orginal">
                                                            <td data-name="downloadUrl" data-value="${item.downloadUrl}" data-order="true"> ${sum}
                                                                <input type="hidden" data-name="originalFileType" data-value="${item.originalFileType}"/>
                                                                <input type="hidden" data-name="originalFilename" data-value="${item.originalFilename}"/>
                                                                <input type="hidden" data-name="type" data-value="P"/>
                                                            </td>
                                                            <td data-name="originalFilename" data-value="${item.originalFilename}">${item.originalFilename}</td>
                                                            <td data-name="fileSize" data-value="${item.fileSize}">${item.fileSize}KB</td>
                                                            <td data-name="fileDeclaration" data-value="${item.fileDeclaration}">${item.fileDeclaration}</td>
                                                            <td data-name="downloadFilename" data-value="${item.downloadFilename}">
                                                                <a href="javascript:;" class="text-danger order-del-btn"
                                                                   data-key="${item.downloadFilename}_P"><i class="icon-trash"></i> 删除</a>
                                                                <a href="${downloadAction}?id=${item.attachmentId}&file=${item.downloadFilename}"><i
                                                                        class="icon icon-download-alt green "></i>&nbsp;下载</a>
                                                            </td>
                                                        </tr>
                                                        <c:set var="sum" value="${sum + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                            <span class="c999"  style="color: red;"> 附件类型不支持exe格式，文件大小不超过100M。              </span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="mt12">
                                        <div class="clearfix line-32">
                                            <span class="left">供应商信息：</span>
                                            <div class="fb-zz left">
                                                <input data-name="rulesBo.isSupplierRequirement" type="checkbox" style="margin:0 2px 0 12px; " value="1"
                                                       <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">checked</c:if>>
                                                特邀供应商仍需走报名审核流程。
                                            </div>
                                            <button class="btn btn-warning btn-sm right" type="button" data-toggle="modal" data-target="#supplistModal"
                                                    id="superlist"><i class="icon icon-plus"></i> 邀请供应商
                                            </button>
                                        </div>
                                        <input id="isSupplierRequirement" value="${rfqRequestComplexVo.rulesVo.isSupplierRequirement}" type="hidden">
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">
                                            <input id="supplierList" value='${supplierList}' type="hidden">
                                        </c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                            <input id="preauditSupplierList" value='${preauditSupplierList}' type="hidden">
                                        </c:if>
                                        <div>
                                        <table class="table table-bordered table-hove  align-md" style="margin-bottom:5px;" id="supplierListJqGrid"></table>
                                        <div class="col-md-12" id="supplierListjqGridPager"></div>
                                        </div>
                                            <%--<thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>供应商U代码</th>
                                                <th>特邀供应商名称</th>
                                                <th>联系人</th>
                                                <th>联系电话</th>
                                                <th>微信状态</th>
                                                <th>操作</th>
                                            </tr>
                                            </thead>--%>


                                            <%--<c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">--%>
                                                <%--<c:forEach var="supplierList" items="${rfqRequestComplexVo.supplierList}" varStatus="status">--%>
                                                    <%--<tr>--%>
                                                        <%--<td data-name="companyId" data-value="${supplierList.companyId}">${status.index+1}</td>--%>
                                                        <%--<td data-name="supplierCode" data-value="${supplierList.supplierCode}">${supplierList.supplierCode}</td>--%>
                                                        <%--<td data-name="supplierName" data-value="${supplierList.supplierName}">${supplierList.supplierName}</td>--%>
                                                        <%--<td data-name="linkmanName" data-value="${supplierList.linkmanName}">${supplierList.linkmanName}</td>--%>
                                                        <%--<td data-name="linkmanTelphone"--%>
                                                            <%--data-value="${supplierList.linkmanTelphone}">${supplierList.linkmanTelphone}</td>--%>
                                                        <%--<td data-name="isBindWechat"--%>
                                                            <%--data-value="${supplierList.isBindWechat}">${supplierList.isBindWechat}</td>--%>
                                                        <%--<td><a class="order-del-btn" data-id="${supplierList.companyId}"><i class="red icon icon-trash"></i>&nbsp;--%>
                                                            <%--删除</a></td>--%>
                                                    <%--</tr>--%>
                                                <%--</c:forEach>--%>
                                            <%--</c:if>--%>
                                            <%--<c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">--%>
                                                <%--<c:forEach var="preauditSupplier" items="${rfqRequestComplexVo.preauditSupplierList}" varStatus="status">--%>
                                                    <%--<tr>--%>
                                                        <%--<td data-name="companyId" data-value="${preauditSupplier.companyId}">${status.index+1}</td>--%>
                                                        <%--<td data-name="supplierCode"--%>
                                                            <%--data-value="${preauditSupplier.supplierCode}">${preauditSupplier.supplierCode}</td>--%>
                                                        <%--<td data-name="supplierName"--%>
                                                            <%--data-value="${preauditSupplier.supplierName}">${preauditSupplier.supplierName}</td>--%>
                                                        <%--<td data-name="linkmanName"--%>
                                                            <%--data-value="${preauditSupplier.linkmanName}">${preauditSupplier.linkmanName}</td>--%>
                                                        <%--<td data-name="linkmanTelphone"--%>
                                                            <%--data-value="${preauditSupplier.linkmanTelphone}">${preauditSupplier.linkmanTelphone}</td>--%>
                                                        <%--<td data-name="isBindWechat" data-value="${preauditSupplier.isBindWechat}">${preauditSupplier.isBindWechat}</td>--%>
                                                        <%--<td><a class="order-del-btn" data-id="${preauditSupplier.companyId}"><i class="red icon icon-trash"></i>&nbsp;--%>
                                                            <%--删除</a></td>--%>
                                                    <%--</tr>--%>
                                                <%--</c:forEach>--%>
                                            <%--</c:if>--%>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div title="询价规则" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价规则</span>
                            </div>

                            <div class="panel-body">
                                <div class="row ml12">
                                    <ul>
                                        <li class="clearfix col-lg-4 fb-zz mb12">
                                            <span class="left line-32"><i class="r">*</i>报名截止时间：</span>
                                            <div class="col-lg-6 padding1">
                                                <input name="rulesBo.registrationEndDate" placeholder="" class="form-control form-datetime" type="text" value="${rfqRequestComplexVo.rulesVo.registrationEndDate}">
                                            </div>
                                        </li>
                                        <li class="clearfix col-lg-7 mb12">
                                            <span class="left line-32"><i class="r">*</i>报价开始时间：</span>
                                            <div class="col-lg-6 padding1">
                                                <input name="rulesBo.quotationStartDate" placeholder="" class="form-control form-datetime" type="text" value="${rfqRequestComplexVo.rulesVo.quotationStartDate}">
                                            </div>
                                        </li>
                                        <li class="clearfix col-lg-4 mb12">
                                            <span class="left line-32"><i class="r">*</i>报价截止时间：</span>
                                            <div class="col-lg-6 padding1">
                                                <input name="rulesBo.quotationEndDate" placeholder="" class="form-control form-datetime" type="text" value="${rfqRequestComplexVo.requestVo.quotationEndDate}">
                                            </div>
                                        </li>
                                        <li class="clearfix col-lg-8 mb12">
                                            <span class="left line-32">是否缴纳保证金：　</span>
                                            <div class="col-lg-3 padding1 line-32">
                                                <input type="radio" <c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney=='1'}">checked</c:if> name="rulesBo.needAssureMoney" value="1" onclick="showMoney();"/>是
                                                &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;
                                                <input type="radio" <c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney!='1'}">checked</c:if> name="rulesBo.needAssureMoney" value="0" onclick="hideMoney();"/>否
                                            </div>
                                            <div id="money_div" <c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney!='1'}">style="display: none;"</c:if>>
                                                <div class="col-lg-3 padding1">
                                                    <input name="rulesBo.assureMoney" placeholder=""  id="assureMoney" class="form-control" type="text" value="<fmt:formatNumber  value='${rfqRequestComplexVo.rulesVo.assureMoney}' pattern='0.00'/>">
                                                </div> <span class="line-32 red">元</span>
                                            </div>
                                        </li>
                                        <li class="clearfix col-lg-7 mb12 clearb">
                                            <span class="left line-32 w97">报价货币：　　</span>
                                            <div class="col-lg-3 padding1">
                                                <select name="requestBo.currency" class="form-control" aria-valuenow="${rfqRequestComplexVo.requestVo.currency}">
                                                    <option value="CNY">人民币CNY</option>
                                                    <option value="USD">美元USD</option>
                                                    <option value="GBP">英镑GBP</option>
                                                    <option value="JPY">日元JPY</option>
                                                    <option value="EUR">欧元EUR</option>
                                                    <option value="HKD">港币HKD</option>
                                                    <option value="CHF">瑞士法郎CHF</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li class="clearfix col-lg-6" style="clear:both">
                                            <span class="left line-32">分项分量报价：　　</span>
                                            <div class="col-lg-12 padding1">
                                                <label class=" line-32">
                                                    <input  <c:if test="${fn:contains(rfqRequestComplexVo.rulesVo.partialProductFlag,'1' )}">checked</c:if> data-name="rulesBo.partialProductFlag" type="checkbox" value="1">允许对部分产品报价
                                                </label>
                                                <label class="ml12  line-32">
                                                    <input  <c:if test="${fn:contains(rfqRequestComplexVo.rulesVo.partialQuantityFlag,'1' )}">checked</c:if> data-name="rulesBo.partialQuantityFlag" type="checkbox" value="1">允许对部分数量报价
                                                </label>
                                            </div>
                                        </li>
                                        <li class="col-lg-6" style="clear:both">
                                            <span class="left line-32">询价公开程度：　　</span>
                                            <div class="col-lg-12 padding1">
                                                <label class=" line-32">
                                                    <input  <c:if test="${fn:contains(rfqRequestComplexVo.rulesVo.isPublicOnlineMarket,'1' )}">checked</c:if> data-name="rulesBo.isPublicOnlineMarket" type="checkbox" value="1" class="js-check">公开询价公告
                                                </label>
                                                <label class="ml12  line-32">
                                                    <input  <c:if test="${fn:contains(rfqRequestComplexVo.rulesVo.isPublicPub,'1' )}">checked</c:if> data-name="rulesBo.isPublicPub" type="checkbox" value="1" class="js-check">公开中标供应商
                                                </label>
                                                <label class="ml12  line-32">
                                                    <input  <c:if test="${fn:contains(rfqRequestComplexVo.rulesVo.isPublicBidMoney,'1' )}">checked</c:if> data-name="rulesBo.isPublicBidMoney" type="checkbox" value="1">公开中标价格
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>



                        <div title="商务条款" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">商务条款</span>
                                <button class="btn  btn-warning btn-sm right" type="button"  data-toggle="modal" data-target="#myModaBL" id="attachment"><i class="icon icon-plus"></i>  我的常用商务条款</button>
                            </div>

                            <div class="panel-body">
                                <div class="row" style="padding:10px 20px 10px 25px;">
                                    <textarea id="contentSimple3" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">${rfqRequestComplexVo.requestVo.requestBusiTerms}</textarea>
                                </div>
                            </div>
                        </div>
                        <div title="技术条款" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">技术条款</span>
                                <button class="btn  btn-warning btn-sm right" type="button"  data-toggle="modal" data-target="#myModaTL" id="attachment2"><i class="icon icon-plus"></i>  我的常用技术条款</button>
                            </div>

                            <div class="panel-body">
                                <div class="row" style="padding:10px 20px 10px 25px;">
                                    <textarea id="contentSimple2" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">${rfqRequestComplexVo.requestVo.requestTecTerms}</textarea>
                                </div>
                            </div>
                        </div>

                        <div title="联系方式" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">联系方式</span>
                            </div>

                            <div class="panel-body">
                                <div class="row ml12 font12">
                                    <ul>
                                        <li class="clearfix col-lg-10 mb12">
                                            <span class="left line-32 w65"><i class="r">*</i>联系人：</span>
                                            <div class=" col-lg-6 padding1">
                                                <input name="contactsBo.linkmanName" placeholder="联系人" class="form-control" type="text" value="${rfqRequestComplexVo.contactsVo.linkmanName}">
                                            </div>
                                        </li>
                                        <li class="clearfix col-lg-10">
                                            <span class="left line-32"><i class="r">*</i>联系电话：</span>
                                            <div class="col-lg-6 padding1">
                                                <input name="contactsBo.linkmanTelphone" placeholder="移动电话或者区号-XXXXXXXX" class="form-control" type="text" value="${rfqRequestComplexVo.contactsVo.linkmanTelphone}">
                                            </div>
                                        </li>

                                        <li class="clearfix col-lg-10 mt12">
                                            <span class="left line-32"><i class="r">*</i>交货地址：</span>
                                            <div class="col-lg-2 padding1">
                                                <select name="contactsBo.deliveryProvince" class="form-control"  id="deliveryProvince" >
                                                </select>
                                            </div>
                                            <div class="col-lg-2 padding1">
                                                <select name="contactsBo.deliveryCity"  class="form-control"  id="deliveryCity" >
                                                </select>
                                            </div>
                                            <div class="col-lg-2 padding1">
                                                <select name="contactsBo.deliveryArea"  class="form-control"  id="deliveryArea" >
                                                </select>
                                            </div>
                                            <div class="col-lg-4 padding1 row-address">
                                                <input name="contactsBo.deliveryAddress" id="deliveryAddress" placeholder="具体地址" class="form-control left w270"  type="text" value="${rfqRequestComplexVo.contactsVo.deliveryAddress}" maxlength="200">
                                            </div>
                                        </li>
                                        <div class="left line-32 mt12 ml20 btn-address"><button class="btn  btn-warning btn-sm" type="button"  data-toggle="modal" data-target="#myModa8"><i class="icon icon-plus"></i>&nbsp常用地址</button></div>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div title="询单附件" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询单附件</span>
                                <button class="btn right btn-warning btn-sm attachmentBtn" type="button"  data-toggle="modal" data-target="#myModa2" data-type="R"><i class="icon icon-plus"></i> 新增附件</button>
                            </div>

                            <div class="panel-body">
                                <div class="row ">
                                    <div class="re line-32 ml12 mr12">
                                        <table class="table table-bordered align-md fixed">
                                            <thead>
                                            <tr>
                                                <th width="50px">序号</th>
                                                <th>文件名称</th>
                                                <th width="100px">文件大小</th>
                                                <th>说明</th>
                                                <th width="150px">操作</th>
                                            </tr>
                                            </thead>
                                            <tbody id="attachmentList2">
                                            <c:set var="sum" value="1"/>
                                            <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}" varStatus="status">
                                            <c:if test="${item.type == 'R'}">
                                            <tr data-clone="orginal">
                                                <td data-name="downloadUrl" data-value="${item.downloadUrl}" data-order="true"> ${sum}
                                                    <input type="hidden" data-name="originalFileType" data-value="${item.originalFileType}"/>
                                                    <input type="hidden" data-name="originalFilename" data-value="${item.originalFilename}"/>
                                                    <input type="hidden" data-name="type" data-value="R"/>
                                                </td>
                                                <td data-name="originalFilename" data-value="${item.originalFilename}">${item.originalFilename}</td>
                                                <td data-name="fileSize" data-value="${item.fileSize}">${item.fileSize}KB</td>
                                                <td data-name="fileDeclaration" data-value="${item.fileDeclaration}">${item.fileDeclaration}</td>
                                                <td data-name="downloadFilename" data-value="${item.downloadFilename}">
                                                    <a href="javascript:;" class="text-danger order-del-btn" data-key="${item.downloadFilename}_R"><i class="icon-trash"></i> 删除</a>
                                                    <a href="${downloadAction}?id=${item.attachmentId}&file=${item.downloadFilename}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a>
                                                </td>
                                            </tr>
                                                <c:set var="sum" value="${sum + 1}"/>
                                            </c:if>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <span class="c999"  style="color: red;"> 附件类型不支持exe格式，文件大小不超过100M。              </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--询单备注--%>
                        <div title="询单备注" class="panel panel-block ke-toolbar">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询单备注</span>
                            </div>
                            <div class="panel-body">
                                <div class="row" style="padding:10px 20px 10px 25px;">
                                    <textarea id="memo" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;">${rfqRequestComplexVo.requestVo.memo}</textarea>
                                </div>
                            </div>
                        </div>

                        <!-- uh start  -->
                        <div class="panel2 hide" id="topBarInfo2" style="position:fixed; top:0; width:1038px; z-index:99;">

                            <div class="col-md-12 with-padding" style="padding:10px 12px">
                                <div class="row">
                                    <div class="re line-32" style="color:#666;">
                                        <div class="so-form mt12">
                                            <%--   <div class="left mr12 line-32">竞价单号：23123123213</div>
                                               <div class="ml12 left mr12 line-32">计划编号：213213213213</div>
                                               <div class="ml12 left mr12 line-32">竞价标题：计算机采购</div>--%>
                                                <div class="ml12 left mr12 line-32">计划编号：<span id="_planNo">${rfqRequestComplexVo.requestVo.planNo}</span></div>
                                                <div class="ml12 left mr12 line-32">询价单标题：<span id="_title">${rfqRequestComplexVo.requestVo.title}</span></div>
                                        </div>
                                    </div>
                                    <div class="text-right mr12" style="margin-top:-12px">
                                        <c:if test="${!empty gFlag}">
                                            <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-lg btn-primary js-form-btn js-block" type="button" target="_blank">审批纪录</a>
                                        </c:if>
                                        <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                            <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                                        </p:permission>
                                        <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                            <a href="javascript:;" class="btn btn-lg btn-danger js-form-btn js-block" data-type="2" type="button">删除</a>
                                        </p:permission>
                                        <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="1" >
                                            <a href="javascript:;" class="btn btn-lg btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                                        </p:permission>
                                        <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                            <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                                        </p:permission>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-right ke-toolbar" id="btn-r-b">
                            <c:if test="${!empty gFlag}">
                                <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-lg btn-primary js-form-btn js-block" type="button" target="_blank">审批纪录</a>
                            </c:if>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-lg btn-danger js-form-btn js-block" data-type="2" type="button">删除</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="1" >
                                <a href="javascript:;" class="btn btn-lg btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                            </p:permission>
                        </div>



                        <%-- <div class="text-right">
                           <c:if test="${!empty gFlag}">
                               <a href="${pageContext.request.contextPath}/rfqGeneralAuditingHistory/init?objectBillId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-lg btn-primary js-block" type="button" target="_blank">审批纪录</a>
                           </c:if>
                           <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                               <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                           </p:permission>
                           <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                               <a href="javascript:;" class="btn btn-lg btn-danger js-form-btn js-block" data-type="2" type="button">删除</a>
                           </p:permission>
                           <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="1" >
                               <a href="javascript:;" class="btn btn-lg btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                           </p:permission>
                           <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                               <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                           </p:permission>
                       </div>--%>


                        <div id="sideRightBar" class="ke-toolbar">
                            <div class="re">
                                <div class="ui-rightbar-middle"></div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="采购物料">采购物料</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="采购角色设置">采购角色设<br>置</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="供应商设置">供应商设置</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="询价规则">询价规则</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="商务条款与技术条款">商务条款与<br>技术条款</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="采购联系方式">采购联系方<br>式</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="询单附件">询单附件</a>
                                    </div>
                                </div>
                                <div class="ui-rightbar-list">
                                    <div class="ui-rightbar-point"></div>
                                    <div class="ui-rightbar-text">
                                        <a href="javascript:;" class="ui-load-button" title="询单备注">询单备注</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- uh end  -->



                       <%-- <div class="text-right">
                            <c:if test="${!empty gFlag}">
                                <a href="${pageContext.request.contextPath}/rfqGeneralAuditingHistory/init?objectBillId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-lg btn-primary js-block" type="button" target="_blank">审批纪录</a>
                            </c:if>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-lg btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-lg btn-danger js-form-btn js-block" data-type="2" type="button">删除</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="1" >
                                <a href="javascript:;" class="btn btn-lg btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                            </p:permission>
                        </div>--%>

                        <div class="text-right bcontrol">
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-sm btn-danger pull-left bdel js-form-btn js-block" data-type="2" type="button"><i class="icon icon-trash"></i>  删除</a>
                            </p:permission>
                            <div class="row pagebtn text-center pdiv"><a class="btn btn-primary top ke-toolbar" style="margin:5px;">上一页</a><a class="btn btn-primary bottom" style="margin:5px;">下一页</a></div>
                            <c:if test="${!empty gFlag}">
                                <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-primary js-form-btn js-block" type="button" target="_blank">审批纪录</a>
                            </c:if>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-primary js-form-btn js-block" data-type="1" type="button">保存</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="1" >
                                <a href="javascript:;" class="btn btn-success js-form-btn js-block" data-type="3" type="button">预览</a>
                            </p:permission>
                            <p:permission requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}" privilege="2" >
                                <a href="javascript:;" class="btn btn-warning js-form-btn js-block" data-type="4" type="button">发布</a>
                            </p:permission>
                        </div>

                    </div>
                    <input type="hidden" id="requestId" value="${rfqRequestComplexVo.requestVo.id}" />
                </div>
        </form></div>
    </div>
</div>





<!-- ///////////////////////新增附件2/////////////// -->
<div class="modal fade"  id="myModa2">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增附件</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <p class="ml12">请单击“上传”按钮选择要上传的文件，完成后请单击确定添加附件（单个附件不超过100M）。</p>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 附件：</label>
                    <div class="col-sm-10" style="margin-left:-6px">
                        <span class="col-md-765 file_upload">
                          <form>
                              <input type="text"  id="fileName" placeholder="请上传,附件大小≤100M"  class="form-control no-border" />
                              <input type="button" id="upload" value="上传">
                              <span  id="msg">0%</span>
                          </form><br/>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right"> 说明：</label>
                    <div class="col-sm-8">
                        <textarea id="fileDeclaration" class="form-control" maxlength="1000"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary js-confirm" data-dismiss="modal" id="sure" disabled="disabled">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- id2结束 -->



<!-- ///////////////////////从历史询单创建/////////////// -->
<div class="modal fade"  id="myModa1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">从历史询价单创建</h4>
            </div>
            <div class="modal-body">
                <form id="searchFormHis" method="post" onsubmit="return false;">
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8">询价单号：</div>
                        <div class="so-form-15 padding1">
                            <input name="unifiedRfqNum" id="unifiedRfqNum" class="form-control" placeholder="询价单号" type="text">
                        </div>
                        <div class="so-form-8 ml12">询单标题：</div>
                        <div class="so-form-30 padding1">
                            <input name="title" id="title" class="form-control" placeholder="询单标题" type="text">
                        </div>
                        <div class="so-form-7 ml12">状态：</div>
                        <div class="so-form-15 padding1">
                            <select name="type" id="type" class="form-control" name="">
                                <option value="">待选择</option>
                                <option value="0">草稿</option>
                                <option value="1">创建待审批</option>
                                <option value="2">待发布</option>
                                <option value="3">待报价</option>
                                <option value="4">待报名</option>
                                <option value="5">报名中</option>
                                <option value="6">报价中</option>
                                <option value="7">待开标</option>
                                <option value="8">待核价</option>
                                <option value="9">结果待审批</option>
                                <option value="10">已结束</option>
                                <option value="11">已作废</option>
                                <option value="12">已流标</option>
                            </select>
                        </div>
                    </div>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8">创建人：</div>
                        <div class="so-form-15 padding1">
                            <input name="recCreatorUsername" id="recCreatorUsername" class="form-control" placeholder="请填写创建人" type="text">
                        </div>
                        <div class="so-form-8 ml12">创建日期：</div>
                        <div class="so-form-15">
                            <input name="recCreateTime" id="recCreateTime" placeholder="开始日期" class="form-control form-datetime" type="text">
                        </div>
                        <div class="so-form-3 height32">-</div>
                        <div class="so-form-15 padding1">
                            <input name="recCreateTimeEnd" id="recCreateTimeEnd" placeholder="截止日期" class="form-control form-datetime" type="text">
                        </div>
                        <div class="so-form-20 ml12">
                            <button class="btn btn-primary" type="button" id="btnSearchHis"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGridHis"></table>
                    <div class="col-md-12" id="jqGridHisPager"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="rid"/>
            <button type="button" class="btn btn-primary" onclick="addHis()">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->

<!-- ///////////////////////特邀供应商/////////////// -->
<div class="modal fade"  id="supplistModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">特邀供应商</h4>
            </div>
            <div class="modal-body">
                <form>
                <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">

                    <div class="so-form-20 mr12" style="width:15%;">
                        <input class="form-control col-lg-8" placeholder="供应商名称" type="text" name="supplierName">
                    </div>

                    <div class="so-form-20 mr12" style="width:15%;">
                        <select class="form-control" name="deviceDealerGrade">
                            <option value="">供应商等级</option>
                            <option value="已认证">已认证</option>
                            <option value="未认证">未认证</option>
                        </select>
                    </div>
                    <div class="so-form-20 mr12" style="width:20%;">
                        <input class="form-control" placeholder="所属类别" type="text" name="type">
                    </div>
                    <div class="so-form-20" style="width:20%;">
                        <input class="form-control" placeholder="供应商代码" type="text" name="supplierCode">
                    </div>
                    <div class="so-form-20 ml12">
                        <button class="btn btn-primary" type="button" id="btnSearch"><i class="icon icon-search"></i> 搜索</button>
                        <input class="btn" value="重置" type="reset">
                    </div>

                </div>
                </form>
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGridsup" ></table>
                    <div class="col-md-12" id="jqGridPagersup"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->


<!-- ///////////////////////我常用的商务及技术条款/////////////// -->
<div class="modal fade"  id="myModaBL">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的常用商务条款</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGrid"></table>
                    <div class="col-md-12" id="jqGridPager"></div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- ///////////////////////我常用的商务及技术条款/////////////// -->
<div class="modal fade"  id="myModaTL">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">我的常用技术条款</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md" id="jqGrid1"></table>
                    <div class="col-md-12" id="jqGridPager1"></div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->


<!-- ///////////////////////新增角色/////////////// -->
<div class="modal fade"  id="myModaphone">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">角色设置</h4>
            </div>
            <div class="modal-body">
                <form>
                <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                    <div class="so-form-8 text-right">姓名： </div>
                    <div class="so-form-15">
                        <input class="form-control" placeholder="" type="text" name="userName">
                    </div>
                    <div class="so-form-8 ml12 text-right">工号： </div>
                    <div class="so-form-15">
                        <input class="form-control" placeholder="" type="text" name="jobCode">
                    </div>
                    <div class="so-form-8 ml12 text-right">部门： </div>
                    <div class="so-form-15">
                        <input class="form-control" placeholder="" type="text" name="userOffice">
                    </div>
                    <div class="so-form-15 ml12">
                        <button class="btn btn-primary" type="button" id="roleSearch"><i class="icon icon-search"></i> 搜索</button>
                        <input class="btn" value="重置" type="reset">
                    </div>
                </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridphone">
                        <div class="col-md-12" id="jqGridPagerphone"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
<!-- excel导入开始 -->
<div class="modal fade" id="cancelPrice33">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">excel导入</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row">
                    <span class="left-cont2">1、第一步请先下载excel模版。</span>
                    <div>
                        <span class="left-cont1 ml12"><a href="javascript:exportExcelModel();" class="blue">点此下载excel模版</a></span>
                    </div>
                </div>
                <div class="mg-row clearfix" style="margin-bottom:8px;">
                    <span class="left-cont2">2、第二步请上传根据模版编辑好的文件。</span>
                    <form method="post">
                       <%-- &lt;%&ndash;<span class="file_upload" style="width:250px;">
                            <input type="file">
                            <input name="author" style="width:150px;" placeholder="请上传相关文档，附件≤10M。" id="author" class="form-control no-border" type="text">
                            <input value="上传" type="button" >
                        </span>&ndash;%&gt;--%>
                        <input type="file" name="file" id="excelFile">
                        <input type="text"  id="fileNamess" placeholder="请上传,附件大小≤100M" disabled /><span  id="msgss"></span>
                    </form>
                </div>
                <div class="alert-danger" style=" padding:10px 20px; line-height:22px; margin-bottom:10px; height:66px;">
                    <i class="icon icon-warning-sign sign-warn1"></i><span class="sign-1 ml12">

注意：Excel批量导入将覆盖询单内现有物料；<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上传附件文件类型仅限Excel文件。</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" <%--onclick="upstart()()"--%> onclick="upstart()" class="btn btn-primary js-confirm" data-dismiss="modal" id="suress" disabled="disabled">确定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- excel导入结束 -->

<!-- 发布 -->
<div class="modal fade" id="myModa6">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">提交确认</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t ml35">
                    <i class="icon icon-question-sign text-primary"></i>
                    <div class="content ml12">
                        <h4>你确定要提交本询价单吗?</h4>
                        <p>询单标题：　　<span data-mp="title">物位计等备件公开采购</span></p>
                        <p>询单编号：　　<span data-mp="requestNo">2014061820</span></p>
                        <p>
                            <span class="left">选择审批流程：</span>
                             <span class="col-lg-6 padding1">
                                <select id="appro" style="width:110px" class="form-control input-sm">
                                </select>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> <a type="button" class="btn btn-primary" data-type="1"  href="javascript:;">确定提交</a>  <a type="button" class="btn"  data-type="2" href="javascript:;">返回修改</a></div>
        </div>
    </div>
</div>



<!-- 保存 -->
<div class="modal fade" id="myModa9">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">保存成功</h4>
            </div>
            <div class="modal-body">
                <div class="alert-icon-t ml35">
                    <i class="icon-check text-primary"></i>
                    <div class="content ml12">
                        <h4>恭喜您，本询价单保存成功！</h4>
                        <p>询单标题：<span data-mp="title">物位计等备件公开采购</span></p>
                        <p>询单编号：<span data-mp="requestNo">2014061820</span></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer"><%-- <a type="button" class="btn btn-primary"  href="javascript:;">新增</a>  <a type="button" class="btn btn-warning"  href="javascript:;">维护</a> <a type="button" class="btn btn-success"  href="javascript:;">选择</a>--%>
                <a type="button" class="btn js-return"  href="init">确认</a></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModa8">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">常用地址</h4>
            </div>
            <div class="modal-body" style="height: 400px; overflow: scroll;">
                <div class="so-form clearfix" style="margin:0px;">
                    <div class="so-form-20">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalAddress">新增</button>
                    </div>
                </div>
                <table class="table table-bordered table-hove align-md" id="AddreJqGrid"></table>
                <div class="col-md-12" id="AddreJqGridPager"></div>
            </div>
            <div class="modal-footer">
                <%--<a type="button" class="btn btn-primary btn-sm"  href="javascript:;"><i class="icon icon-plus"></i>&nbsp新增一行</a>--%>
                <a type="button" class="btn btn-success btn-sm"  href="javascript:addAddress();">确定</a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalAddress">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">新增常用地址</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li class="clearfix col-lg-10 mt12">
                        <%-- <span class="left line-32">：</span>--%>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryProvince" class="form-control"  id="deliveryProvince2" >
                            </select>
                        </div>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryCity"  class="form-control"  id="deliveryCity2" >
                            </select>
                        </div>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryArea"  class="form-control"  id="deliveryArea2" >
                            </select>
                        </div>
                        <div class="col-lg-5 padding1">
                            <input name="contactsBo.deliveryAddress" id="deliveryAddress2" placeholder="具体地址" class="form-control left" type="text" maxlength="200">
                        </div>
                    </li>
                    <%--<li class="clearfix col-lg-10 mt12">
                        <span class="left line-32">选择市：</span>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryCity"  class="form-control"  id="deliveryCity2" >
                            </select>
                        </div>
                    </li>
                    <li class="clearfix col-lg-10 mt12">
                        <span class="left line-32">选择区（县）：</span>
                        <div class="col-lg-2 padding1">
                            <select name="contactsBo.deliveryArea"  class="form-control"  id="deliveryArea2" >
                            </select>
                        </div>
                    </li>--%>
                    <%-- <li class="clearfix col-lg-10 mt12">
                         <span class="left line-32">详细地址（街道）：</span>
                         <div class="col-lg-5 padding1">
                             <input name="contactsBo.deliveryAddress" id="deliveryAddress2" placeholder="具体地址" class="form-control left" type="text">
                         </div>
                     </li>--%>
                </ul>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-success btn-sm"  href="javascript:ajaxToAddData();">确定</a>
            </div>
        </div>
    </div>
</div>


<!-- uh start -->
<div class="modal fade in" id="changeViewModal" aria-hidden="false">
    <div class="modal-dialog" style="margin-top: 33.3333px;">
        <div class="modal-content">
            <div class="modal-header view-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">你想用哪种布局展示页面？</h4>
            </div>
            <div class="modal-body body-view">
                <div class="view-options">
                    <a class="view-option view-option-single active row" data-dismiss="modal">
                        <div class="view-shape pull-left">
                            <div class="s-1">
                                <img src="${resource}/images/cross.png">
                            </div>
                        </div>
                        <div class="title">点击翻页</div>
                        <p class="text-muted">点击上部的小圆点进入相应的页面</p>
                    </a>
                    <a class="view-option view-option-double row" data-dismiss="modal">
                        <div class="view-shape pull-left">
                            <div class="s-1">
                                <img src="${resource}/images/longitudinal.png">
                            </div>
                        </div>
                        <div class="title strong">滚动翻页</div>
                        <p class="text-muted">根据鼠标滚动位置自动切换到相应位置</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- uh end -->

<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo">
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary js-confirm">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
</div>
</div>
</div>
</div>

</div>
</div>
<script src="${resource}/js/jquery.easing.1.3.js"></script>
<script src="${resource}/js/stepBar.js"></script>
<script src="${resource}/js/rfqRequest/rfqRequestEdit4uh.js?v=${version}"></script>
<script>
    //物料Excel flash上传
    $(document).ready(function (){
        var typesss=[".xls",".xlsx"];
        var maxSize=104857600;//100M，单位:B
        if(navigator.appName == "Microsoft Internet Explorer"){
            $("#cancelPrice33").on('show.zui.modal', function () {
                //初始化flash插件
                initPlupload2ss(maxSize,typesss);
                //关闭时清理页面
                initEventss();
            });
        }else {
            //初始化flash插件
            initPluploadss(maxSize,typesss);
            //关闭时清理页面
            initEventss();
        }
    });
    /*$(function () {
        var $cancelPrice33 = $('#cancelPrice33');
        /!*
         上传确定按钮
         *!/
        $cancelPrice33.on('click', '.js-confirm', function () {
            upstart();
            //关闭时清理页面
           // initEventss();
        });
    });*/

    /**关闭提示后列表定位调整*/
    function changeHigeht(){
        /*吸顶*/
        $('.hj-tb-xd').setScroll({
            defaultTop:$('.page-content').find('.panel').eq(0).height()+75,
            startTop:$('.page-content').find('.panel').eq(0).height()+75
        });
    }
    function getAddressData(){
        var rowData = {
            deliveryProvince : '${rfqRequestComplexVo.contactsVo.deliveryProvince}',
            deliveryCity : '${rfqRequestComplexVo.contactsVo.deliveryCity}',
            deliveryArea:'${rfqRequestComplexVo.contactsVo.deliveryArea}',
            deliveryProvinceCode : '${rfqRequestComplexVo.contactsVo.deliveryProvinceCode}',
            deliveryCityCode : '${rfqRequestComplexVo.contactsVo.deliveryCityCode}',
            deliveryAreaCode:'${rfqRequestComplexVo.contactsVo.deliveryAreaCode}'
        }
        return rowData;
    }

    /* 初始化数据表格 */
    $('table.datatable').datatable({
        fixedLeftWidth:'120px',
        fixedHeader:false,
        ready: function()
        {
            $('.btn-group').selectLisr();
        }
    });

    /*删除行*/
    //$('.order-del-btn').delBtn();

    /*删除行*/
    $('#checkedDiv').checkeInfo();

    var publicBiddingFlag = '${rfqRequestComplexVo.rulesVo.publicBiddingFlag}';
    if(publicBiddingFlag == 0){ //定向
        $('.fb-zz').hide();
        $('.fb-zz input').attr('disabled',true);
    }else{
        $('.js-check').attr('disabled',true);
    }
    //日期
    $(".form-datetime").datetimepicker(
    {
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        format: "yyyy-mm-dd hh:ii"
    });

    function showMoney(){
        $('#money_div').show();
    }

    function hideMoney(){
        $('#money_div').hide();
        $('#assureMoney').val("0");
    }

    /*//保证金小数位校验
     $("#assureMoney").keyup(function(){
     var valTmp = this.value;
     var num = +valTmp;
     if(num != 0 && !num){
     RFQ.error('请输入数字');
     this.value = '';
     }else if(~valTmp.indexOf('.') && valTmp.split('\.')[1].length > 2){
     RFQ.error('小数部分输入太长，应该小于等于2位');
     this.value = num.toFixed(2);
     }
     })*/
    //保证金小数位校验
    $("#assureMoney").on('input propertychange' ,function () {
        var valTmp = this.value;
        var num = +valTmp;
        if (num != 0 && !num) {
            RFQ.error('请输入数字');
            this.value = '';
        } else if (~valTmp.indexOf('.') && valTmp.split('\.')[1].length > 2) {
            RFQ.error('小数部分输入太长，应该小于等于2位');
            this.value = num.toFixed(2);
        }
    });
    $('body').append('<div class="dangzhu1"></div>');
    $('body').append('<div class="dangzhu2"></div>');

    /*公共API个性化设置--邀请范围*/
    if($("#dataSource").val() =='JFE'||$("#dataSource").val() =='NBBX'){
        if($("#publicBiddingFlag").val() == '0'){
            document.getElementById("publicBiddingFlagOne").style.display="none";
            document.getElementById("superlist").style.display="none";
        };
        if($("#publicBiddingFlag").val() == '1'){
            document.getElementById("publicBiddingFlagTwo").style.display="none";
        };
        document.getElementById("buttonArray").style.display="none";
    }

    if($("#dataSource").val() =='ygck' || $("#dataSource").val() =='ygzb'){
        if($("#publicBiddingFlag").val() == '0'){
            document.getElementById("publicBiddingFlagOne").style.display="none";
        };
        document.getElementById("buttonArray").style.display="none";
    }
</script>
<!--[if lt IE 10]>
<script type="text/javascript">
    jQuery(function($){
        $('.row-address').css({'width': '50%', 'margin': '10px 0 0 66px'});
        $('.btn-address').css({'margin-top': '48px'});
        $('#deliveryAddress').css('width','100%');
    });
</script>
<![endif]-->
<!--[if lt IE 9]>
<script type="text/javascript">
    jQuery(function($){
        $('#appro').css('width','350px');
    });
</script>
<![endif]-->
</body>
</html>
