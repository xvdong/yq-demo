<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>审批人审批列表</title>

    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqRequestAuditList.js?v=${version}"></script>

</head>

<body>


<c:import url="../common/top.jsp" />


<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs pore">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">审批人审批列表</li>
                    </ol>
                    <div class="abrl" ><a href="${ctx}/show/caigoujieguoshenpi.html" target='_blank'>新版询比价审批操作演示</a></div>
                </div>
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <ul id="myTab" class="nav nav-tabs">
                                    <li class="active"> <a href="#tab1" data-toggle="tab" onclick="typeList('')">全部类型</a> </li>
                                    <li><a href="#tab2" data-toggle="tab" data-method="RAQ" onclick="typeList('RAQ')">询比价</a></li>
<%--
                                    <li><a href="#tab3" data-toggle="tab" data-method="DAC" onclick="typeList('DAC')">反向竞价</a></li>
--%>
                                </ul>
                                <input type="hidden" id="rfqMethod" name="rfqMethod"/>
                                <div class="tab-content">
                                    <div class="tab-pane active  example" id="tab1">
                                        <div class="col-lg-12 ">
                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                                <ul class="nav navbar-nav show-data">
                                                    <li><a href="#" data-type="2">待审批流程</a></li>
                                                    <li class="active"><a href="#" data-type="1">我发起的流程</a></li>
                                                    <li><a href="#" data-type="3">我审批过的流程</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="tab-pane  example" id="tab2">
                                        <div class="col-lg-12 ">
                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                                <ul class="nav navbar-nav show-data">
                                                    <li><a href="#" data-type="2">待审批流程</a></li>
                                                    <li class="active"><a href="#" data-type="1">我发起的流程</a></li>
                                                    <li><a href="#" data-type="3">我审批过的流程</a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab2" class="searchFormTab" onsubmit="return false;">

                                            <div class="so-form">
                                                <div class="so-form-20 padding1">
                                                    <input name="unifiedRfqNum"  type="text" class="form-control" placeholder="询价单号">
                                                </div>
                                                <div class="so-form-20 padding1">
                                                    <input name="title"  type="text" class="form-control" placeholder="询价标题">
                                                </div>
                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                            class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn" type="reset" value="重置" />
                                                </div>

                                                <%--<div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>--%>
                                                <%--</div>--%>

                                                <div class="clearfix"></div>

                                                <%--<div class="hiderow">--%>
                                                    <%--<div class="so-form-15"><input type="text" name="startTime" placeholder="提交日期起" class="form-control form-datetime"></div>--%>
                                                    <%--<div class="so-form-3 height32">-</div>--%>
                                                    <%--<div class="so-form-15 padding1"><input type="text" name="endTime" placeholder="提交日期止" class="form-control form-datetime"></div>--%>
                                                    <%--<div class="so-form-15"><input type="text" name="startDate" placeholder="截止日期起" class="form-control form-datetime"></div>--%>
                                                    <%--<div class="so-form-3 height32">-</div>--%>
                                                    <%--<div class="so-form-15 padding1"><input type="text" name="quotationEndDate" placeholder="截止日期止" class="form-control form-datetime"></div>--%>
                                                <%--</div>--%>

                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane  example" id="tab3">
                                        <div class="col-lg-12 ">
                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                                <ul class="nav navbar-nav show-data">
                                                    <li><a href="#" data-type="2">待审批流程</a></li>
                                                    <li class="active"><a href="#" data-type="1">我发起的流程</a></li>
                                                    <li><a href="#" data-type="3">我审批过的流程</a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab3" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">
                                                <div class="so-form-20 padding1">
                                                    <input name="unifiedRfqNum"  type="text" class="form-control" placeholder="竞价单号">
                                                </div>
                                                <div class="so-form-20 padding1">
                                                    <input name="title" type="text" class="form-control" placeholder="竞价标题">
                                                </div>
                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                            class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn" type="reset" value="重置" />
                                                </div>

                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="startTime" id="startTime" placeholder="提交日期起" class="form-control form-datetime"></div>
                                                    <div class="so-form-3 height32">-</div>
                                                    <div class="so-form-15 padding1"><input type="text" name="endTime" id="endTime" placeholder="提交日期止" class="form-control form-datetime"></div>
                                                    <div class="so-form-15"><input type="text" name="startDate" id="startDate" placeholder="截止日期起" class="form-control form-datetime"></div>
                                                    <div class="so-form-3 height32">-</div>
                                                    <div class="so-form-15 padding1"><input type="text" name="quotationEndDate" id="quotationEndDate" placeholder="截止日期止" class="form-control form-datetime"></div>
                                                </div>

                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                                <div class="col-md-12" id="jqGridPager">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<script>
    $(function(){


        $('.more-so').click(function () {

            $('.hiderow').stop().slideToggle('fast');

        });

        //日期
        $(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });

        $(".form-date").datetimepicker(
                {
                    language:  "zh-CN",
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

        $(".form-time").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            format: 'hh:ii'
        });

    });
</script>
</body>

</html>
