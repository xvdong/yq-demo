<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>已作废</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqAfterResult.js?v=${version}"></script>
</head>

<body>
<c:import url="../common/top.jsp" />
<input type="hidden" id="requestId" value="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">已作废</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">询价单摘要</span>
                            <div class=" pull-right">

                                <p:permission  privilege="1" requestNo="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank">
                                        <span class="btn btn-info font12">询单详情</span>
                                    </a>
                                </p:permission>
                                <p:permission  privilege="2" requestNo="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <span class="btn btn-danger font12" data-position="center"  <%--data-target="#myModa20"--%>onclick="getBill()">导出报价单</span>
                                </p:permission>

                                <%--<span class="btn btn-warning font12">打印报价单</span>--%>
                            </div>
                        </div>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>询价单号：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}</li>
                                        <li>询价标题：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.title}</li>
                                        <li>采购单位：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.ouName}</li>
                                        <li>计划编号：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.planNo}</li>
                                        <li>当前状态：
                                            <span class="red">
                                                已作废
                                            </span>
                                        </li>
                                        <li>邀请范围：${fanwei}</li>
                                    </ul>
                                    <span class="zt-ico-w zt-gz">
                                                     <img src="${ctx}/images/yzf.png">
                                            </span>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.assureMoney}元</li>
                                        <li>报价货币：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.currency}</li>
                                        <li>预算总价：<span class="red">${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.totalPrice}元</span></li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.issueDate}</li>
                                        <li>报价开始时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.quotationStartDate}</li>
                                        <li>报价截止时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                        <li>报名截止时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                        <li>联系电话：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                        <li>收货地址：
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryProvince}
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryCity}
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryArea}
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryAddress}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--核价说明--%>

                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">终止原因：</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    ${stopReason}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="mg-header mt20">
                        中标情况：（中标供应商：<span style="color:red" id="zhongbiaogys">0</span>家；采购总金额：<span style="color:red" id="totalMoney">0</span>元）
                        <span class="pull-right">

                                <p:permission  privilege="1" requestNo="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <a class="trage" href="${pageContext.request.contextPath}/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank">报名历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">审批历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?id=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">报价历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank" class="trage">操作日志</a>
                                </p:permission>
                            </span>
                    </div>


                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab" class="supplier_type">按供应商查看</a></li>
                        <li class=""> <a href="#tab2" data-toggle="tab" class="item_type">按物料查看</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane example active" id="tab1" style=" clear: both;">

                        </div>
                        <div class="tab-pane  example" id="tab2" style=" clear: both;">

                        </div>


                        <div class="so-form-60 right mt20" id="buttom">
                            <div class="pull-right" style="margin-top:-18px;">
                                <ul class="pager pager-loose pagers">
                                    <li><span>共 <strong class="text-danger" id="total"></strong> 条记录 </span></li>
                                    <li><span><strong class="text-danger" id="pageNum"></strong></strong><strong class="text-danger">/</strong><strong id="pages" class="text-danger"></strong> 页</span></li>
                                    <li><span>每页显示
                                  <select name="" class="form-control input-sm" id="pageSize">
                                      <option selected="selected">20</option>
                                      <option>30</option>
                                      <option>50</option>
                                      <option>100</option>
                                      <%--<option>100</option>--%>
                                  </select>
                                  条</span></li>
                                    <li id="homePage"><a href="javascript:;" class="blue"   id="ahomePage">首页</a></li>
                                    <li id="lastPage"><a href="javascript:;" class="blue"   id="alastPage">上一页</a></li>
                                    <li id="nextPage"><a href="javascript:;" class="blue"   id="anextPage">下一页</a></li>
                                    <li id="endPage"><a   href="javascript:;" class="blue"  id="aendPage"  >尾页</a></li>
                                    <li><span>转到
                                  <input type="text" class="form-control input-sm" size="3" id="pageNo" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" autocomplete="off">
                                  页</span> <span class="ml12"><a class="btn btn-sm" id="toPage">确定</a></span></li>
                                </ul>
                            </div>
                        </div>

                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>






<script>

    function getBill() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd",
            data: {id:"${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
</script>
</body>

</html>
