<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>结果待发布</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <c:import url="../common/kingEditor.jsp" />
    <script src="${resource}/js/rfqRequest/rfqRequestPendingRelease.js?v=${version}"></script>
    <style>
        /*.example{overflow:hidden}*/

        /*中标公司在一排显示的样式*/
        .com-list{overflow: hidden; height: 45px}
        .com-list ul li a{width: 94px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;}
        .com-list ul{width: auto; position: relative;}

    </style>
</head>
<body>
    <c:import url="../common/top.jsp" />
    <input type="hidden" id="requestId" value="${rfqRequestComplexVo.requestVo.id}">
    <c:set var="ouRfqNum" value="${rfqRequestComplexVo.requestVo.ouRfqNum}" />
    <div class="wrapper">
        <div class="container container_main">
            <c:import url="../common/menu.jsp" />
            <div class="rightbar clearfix">
                <div class="container">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                                                    <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                            <li class="active">结果待发布</li>
                        </ol>
                    </div>
                    <div class="page-content">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价单摘要</span>
                                <div class=" pull-right">
                                    <c:if test="${sessionScope.chatUrl==null || sessionScope.chatUrl == ''}">
                                        <a id = 'im' href="javascript:RFQ.warn('提示：该功能仅在uat环境与生产环境可用！');"></a>
                                    </c:if>
                                    <c:if test="${sessionScope.chatUrl!=null && sessionScope.chatUrl != ''}">
                                        <a id = 'im' target="_blank"  href="${sessionScope.chatUrl}${ouRfqNum}"></a>
                                    </c:if>
                                    <p:permission  privilege="1" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                        <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${rfqRequestComplexVo.requestVo.id}" target="_blank">
                                            <span class="btn btn-info font12">询单详情</span>
                                        </a>
                                    </p:permission>
                                    <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                          <span class="btn btn-warning font12" data-position="center"  onclick="getBill()">
                                        导出报价单
                                    </span>
                                        <%--<span class="btn btn-warning font12" data-position="center"  onclick="getBill2()">--%>
                                        <%--导出成交通知单--%>
                                    <%--</span>--%>
                                        <%--<span class="btn btn-warning font12">打印报价单</span>--%>
                                        <span class="btn btn-danger font12" data-position="center" data-toggle="modal" data-target="#cancelPrice" >撤销询价</span>
                                    </p:permission>

                                </div>
                            </div>
                            <input type="hidden" id="ouRfqNum" value="${rfqRequestComplexVo.requestVo.ouRfqNum}" />
                            <div class="panel-body font12">
                                <div class="row">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>询价单号：${rfqRequestComplexVo.requestVo.ouRfqNum}</li>
                                            <li>询价标题：${rfqRequestComplexVo.requestVo.title}</li>
                                            <li>采购单位：${rfqRequestComplexVo.requestVo.ouName}</li>
                                            <li>计划编号：${rfqRequestComplexVo.requestVo.planNo}</li>
                                            <li>当前状态：<span class="red">结果待发布</span></li>
                                            <li>邀请范围：
                                                <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                                    公开询价（面向平台注册供应商公开寻源，供应商需先报名，待采购组织审核通过后再报价。）
                                                </c:if>
                                                <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='0'}">
                                                    定向询价 (将询价单信息向特邀供应商公布，供应商无须报名可直接报价。)
                                                </c:if>
                                            </li>
                                        </ul>
                                        <span class="zt-ico-w zt-gz"><img src="${ctx}/images/jgdfb_03.png"></span>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>保证金金额：<span class="red">
                                                <span class="red">${rfqRequestComplexVo.rulesVo.assureMoney}元</span></span>
                                            </li>
                                            <li>报价货币：${rfqRequestComplexVo.requestVo.currency}</li>
                                            <li>预算总价：<span class="red">${rfqRequestComplexVo.requestVo.totalPrice}元</span></li>

                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                            <ul>
                                                <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                                <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                                <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                                <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                            </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>发布时间：${rfqRequestComplexVo.requestVo.issueDate}</li>
                                            <li>报价开始时间：${rfqRequestComplexVo.rulesVo.quotationStartDate}</li>
                                            <li>报价截止时间：${rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                            <li>报名截止时间：${rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                        </ul>
                                    </div>
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li>联系人：${rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                            <li>联系电话：${rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                            <li style="width:40%">收货地址：
                                                ${rfqRequestComplexVo.contactsVo.deliveryProvince}
                                                ${rfqRequestComplexVo.contactsVo.deliveryCity}
                                                ${rfqRequestComplexVo.contactsVo.deliveryArea}
                                                ${rfqRequestComplexVo.contactsVo.deliveryAddress}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--核价说明--%>
                        <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">核价说明</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                        ${rfqRequestComplexVo.requestVo.resultMemo}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form id="completeRelease">
                            <div class="">
                                <input type="hidden" name="requestBo.id" value="${rfqRequestComplexVo.requestVo.id}" />
                                <div class="form-group" style="margin:0;">
                                    <div class="mg-header">中标说明：</div>
                                    <ul id="myTab" class="nav nav-tabs mt12">
                                       <%-- <c:forEach var="supplierList" items="${rfqRequestComplexVo.supplierList}" varStatus="status">
                                            <li class="<c:if test="${status.index==0}">active</c:if>">
                                                <a href="#tabgs${status.index+1}" data-toggle="tab">${supplierList.supplierName}</a>
                                            </li>
                                        </c:forEach>--%>
                                    </ul>
                                    <div class="tab-content" id="tabContent">
                                       <%-- <c:forEach var="supplierList" items="${rfqRequestComplexVo.supplierList}" varStatus="status">
                                            <div class="tab-pane example <c:if test="${status.index==0}">active</c:if>" id="tabgs${status.index+1}">
                                                <textarea id="contentSimple${status.index+2}" name="content"
                                                class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">${status.index}</textarea>
                                            </div>
                                        </c:forEach>--%>
                                    </div>
                               </div>
                            </div>
                            <div class="mg-header">未中标说明：</div>
                            <div>
                               <textarea id="contentSimple9"  name="memoDesc" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">很遗憾，您未中标！</textarea>
                            </div>
                        </form>

                        <div class="mg-header mt20">
                            中标情况：（中标供应商：<span style="color:red" id="zhongbiaogys">0</span>家；采购总金额：
                            <span style="color:red" id="totalMoney">0</span>元）
                            <span class="pull-right">
                                <p:permission  privilege="1" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <a class="trage" href="${pageContext.request.contextPath}/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank">报名历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">审批历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?id=${rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">报价历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank" class="trage">操作日志</a>
                                </p:permission>
                            </span>
                        </div>
                        <ul id="myTab2" class="nav nav-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab" class="supplier_type" id="">按供应商查看</a></li>
                            <li class=""> <a href="#tab2" data-toggle="tab" class="item_type">按物料查看</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane example active" id="tab1">

                            </div>
                            <div class="tab-pane  example" id="tab2">

                            </div>

                            <div class="so-form-60 right mt20" id="buttom">
                                <div class="pull-right" style="margin-top:-18px;">
                                    <ul class="pager pager-loose pagers">
                                        <li><span>共 <strong class="text-danger" id="total"></strong> 条记录 </span></li>
                                        <li><span><strong class="text-danger" id="pageNum"></strong></strong><strong class="text-danger">/</strong><strong id="pages" class="text-danger"></strong> 页</span></li>
                                        <li><span>每页显示
                                  <select name="" class="form-control input-sm" id="pageSize">
                                      <option selected="selected">20</option>
                                      <option>30</option>
                                      <option>50</option>
                                      <option>100</option>
                                      <%--<option>100</option>--%>
                                  </select>
                                  条</span></li>
                                        <li id="homePage"><a  href="javascript:;" class="blue"   id="ahomePage">首页</a>   </li>
                                        <li id="lastPage"><a  href="javascript:;" class="blue"   id="alastPage">上一页</a>   </li>
                                        <li id="nextPage"><a  href="javascript:;" class="blue"   id="anextPage">下一页</a>   </li>
                                        <li id="endPage" ><a   href="javascript:;" class="blue"  id="aendPage" >尾页</a>   </li>

                                        <li><span>转到
                                  <input type="text" class="form-control input-sm" size="3" id="pageNo" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" autocomplete="off">
                                  页</span> <span class="ml12"><a class="btn btn-sm" id="toPage">确定</a></span></li>
                                    </ul>
                                </div>
                                <br>
                                <br>
                                <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <div class="text-right mt12">
                                        <a href="javascript:void(0);" onclick="completeRelease();" class="btn btn-lg btn-warning js-form-btn" type="button" id="pushResult1">结果发布</a>
                                    </div>
                                </p:permission>
                            </div>
                            <br>
                            <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                <div class="text-right mt12" id="firstButtom">
                                    <a href="javascript:void(0);" onclick="completeRelease();" class="btn btn-lg btn-warning js-form-btn" type="button" id="pushResult2">结果发布</a>
                                </div>
                            </p:permission>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModa20">
  <div class="modal-dialog">
      <form  method="post" class="form-horizontal" id="modalFrom" role="form" onsubmit="return false;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">导出报价单</h4>
      </div>
      <div class="modal-body">
          <div class="content">
            <p>
                <span class="pull-left line-32">指定授标供应商：</span>
                <select class="form-control pull-left" name="supplier" style="width:200px;">
                    <c:forEach var="supplierList" items="${priceList}" varStatus="status">
                        <option value="${supplierList.supplierCode}">${supplierList.supplierName}</option>
                   </c:forEach>
                </select>
            </p>
          </div>
      </div>
      <div class="clearfix mt12"></div>
      <div class="modal-footer"> 
         <a type="submit" class="btn btn-primary"  href="javascript:;" onclick="exportExcel()">确定</a>
         <a href="javascript:;" class="btn" type="button"  data-dismiss="modal">取消</a>
      </div>
    </div>
     </form>
  </div>
</div>
    <div class="modal fade" id="cancelPrice">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                    <h4 class="modal-title">撤销询价原因</h4>
                </div>
                <div class="modal-body">
                    <div class="content">
                        <p>
                            <span class="pull-left line-32">原因类别：</span>

                            <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                                <option value="不需要采购">不需要采购</option>
                                <option value="供应商退出">供应商退出</option>
                                <option value="其他原因">其他原因</option>
                            </select>
                        </p>
                        <div class="clearfix"></div>
                        <p>
                            <span class="pull-left line-32">原因说明：</span>

                            <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                        </p>
                    </div>
                </div>
                <div class="clearfix mt12"></div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                    <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal">取消</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<script>
    function getBill() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd",
            data: {id:"${rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
    function getBill2() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd2",
            data: {id:"${rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
</script>