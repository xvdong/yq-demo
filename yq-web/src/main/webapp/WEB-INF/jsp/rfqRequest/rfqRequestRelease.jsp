<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>询价单发布</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqRequestRelease.js?v=${version}"></script>
    <style>
        .datatable{ margin-bottom:10px;}
        .flexarea .table th,td{  text-align:center; vertical-align:middle;}
        .fixed-left .table th,td{text-align:center; vertical-align:middle;}
        .table .dropdown-menu{left:120px !important; top:-28px !important;}
        em{font-style: normal}
    </style>
</head>
<body>
    <c:import url="../common/top.jsp" />
    <div class="wrapper">
        <div class="container container_main">
            <c:import url="../common/menu.jsp" />
            <div class="rightbar clearfix">
                <form id="Myform">
                    <input type="hidden" name="rfqRequestNo" id="requestId" value="${rfqRequestComplexVo.requestVo.id}" />
                    <input type="hidden" name="publicBiddingFlag" value="${rfqRequestComplexVo.rulesVo.publicBiddingFlag}" />
                <div class="container" style="padding-bottom:20px;">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">
                                                    <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                            <li class="active">询价单发布详情</li>
                        </ol>
                    </div>

                    <div class="page-content">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <div class=" pull-right">
                                    <div class=" pull-right">
                                       <%-- <a href="hejiafabu-shenpilishiyemiannew.html">
                                            <span class="label mg-btn label-info">审批流程跟踪</span>
                                        </a>--%>
                                           <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                               <span class="btn btn-warning font12" data-toggle="modal" data-target="#myModa-jssz">角色设置</span>
                                               <span class="btn btn-warning font12" data-position="center" data-toggle="modal" data-target="#adjustTime">调整时间</span>
                                           </p:permission>
                                        <%--<span class="btn btn-danger font12" data-toggle="modal" data-target="#myModa1">打印询价单</span>--%>
                                        <%--<span class="btn btn-warning font12" data-toggle="modal" data-target="#myModa15">下载询价单</span>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body font12">
                                <div class="row">
                                    <div class="mg-pacne clearfix">
                                        <ul>
                                            <li style="width:40%;">询价标题：${rfqRequestComplexVo.requestVo.title}</li>
                                            <li>计划编号：${rfqRequestComplexVo.requestVo.planNo}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="line-32 mt12 ml12 mr12">
                                <span class="left" style="font-size:16px; display:inline-block">物料信息</span>
                                <c:if test="${ not empty rfqRequestComplexVo.requestVo.totalPrice}">
                                    <span class="red right line-32" style=" display:inline-block">预算总价：${rfqRequestComplexVo.requestVo.totalPrice}元</span>
                                </c:if>
                            </div>
                            <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                            <div class="col-md-12" id="jqGridPager"></div>
                        </div>

                        <div class="panel mt20">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">供应商设置</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                        <div class="clearfix">
                                            <label class="bler blu-8">邀请范围：</label>
                                            <span class="bler blu-85">
                                                 <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                                      公开询价（面向平台注册供应商公开寻源，供应商需先报名，待采购组织审核通过后再报价。）
                                                 </c:if>
                                                <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='0'}">
                                                      定向询价 (将询价单信息向特邀供应商公布，供应商无须报名可直接报价。)
                                                </c:if>
                                            </span>
                                        </div>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                            <div class="clearfix">
                                                <label class="bler blu-8">供应商要求：</label>
                                                <span class="bler blu-85">
                                                    1、注册资本要求大于${rfqRequestComplexVo.preauditVo.regcapital}万元
                                                    <br/>
                                                    2、资质要求${rfqRequestComplexVo.preauditVo.qualifications}
                                                </span>
                                            </div>
                                            <div class="clearfix">
                                                <label class="bler blu-8">报名要求：</label>
                                                <span class="bler blu-85">
                                                        ${rfqRequestComplexVo.preauditVo.requirementDesc}
                                                </span>
                                            </div>
                                        </c:if>
                                    <div>
                                        <span class="left line-32" style="font-weight:bold; color:#333;">附件清单：</span>
                                    </div>
                                    <table class="table table-bordered align-md fixed" style="margin-bottom:5px; width:96%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>文件名称</th>
                                            <th width="100px">文件大小</th>
                                            <th>说明</th>
                                            <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                                                <th width="100px">操作</th>
                                            <%--</p:permission>--%>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:set var="sum" value="1"/>
                                        <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}" varStatus="staus">
                                            <c:if test="${item.type == 'P'}">
                                            <tr data-clone="orginal">
                                                <td data-order="true">${sum}</td>
                                                <td>${item.originalFilename}</td>
                                                <td>${item.fileSize}KB</td>
                                                <td>${item.fileDeclaration}</td>
                                               <%-- <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                                                    <td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                               <%-- </p:permission>--%>
                                            </tr>
                                                <c:set var="sum" value="${sum + 1}"/>
                                            </c:if>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <div class="red" style="font-size:12px;">注：附件类型不支持exe格式，文件大小不超过100M。              </div>
                                    <div>
                                        <span class="left line-32" style="font-weight:bold; color:#333;">供应商信息：</span>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                             <span class="bler" style="color:#666;">特邀供应商仍需走报名审核流程。</span>
                                        </c:if>
                                    </div>
                                    <%--<table class="table table-bordered table-hove  align-md" style="margin-bottom:5px; width:96%;">
                                        <thead>
                                        <tr>
                                            <th width="50px">序号</th>
                                            <th>供应商U代码</th>
                                            <th>特邀供应商名称</th>
                                            <th>联系人</th>
                                            <th>联系电话</th>
                                            <th>微信状态</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">
                                            <c:forEach var="supplierList" items="${rfqRequestComplexVo.supplierList}">
                                                <tr>
                                                    <td>${supplierList.seq}</td>
                                                    <td>${supplierList.supplierCode}</td>
                                                    <td>${supplierList.supplierName}</td>
                                                    <td>${supplierList.linkmanName}</td>
                                                    <td>${supplierList.linkmanTelphone}</td>
                                                    <td>${supplierList.isBindWechat}</td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                            <c:forEach var="preauditSupplier" items="${rfqRequestComplexVo.preauditSupplierList}">
                                                <tr>
                                                    <td></td>
                                                    <td>${preauditSupplier.supplierCode}</td>
                                                    <td>${preauditSupplier.supplierName}</td>
                                                    <td>${preauditSupplier.answerUsername}</td>
                                                    <td>${preauditSupplier.linkmanTelphone}</td>
                                                    <td>${preauditSupplier.status}</td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        </tbody>
                                    </table>--%>
                                        <div>
                                            <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="supplierListJqGrid"></table>
                                            <div class="col-md-12" id="supplierListjqGridPager"></div>
                                        </div>
                                        <input id="isSupplierRequirement" value="${rfqRequestComplexVo.rulesVo.isSupplierRequirement }" type="hidden">
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">
                                            <input id="supplierList" value='${supplierList}' type="hidden">
                                        </c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                            <input id="preauditSupplierList" value='${preauditSupplierList}' type="hidden">
                                        </c:if>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">商务与技术条款</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-1">商务条款：</label>
                                        <span class="bler blu-88-nobg"> ${rfqRequestComplexVo.requestVo.requestBusiTerms}</span>
                                    </div>
                                    <div class="clearfix">
                                        <label class="bler blu-1">技术条款：</label>
                                        <span class="bler blu-88-nobg">${rfqRequestComplexVo.requestVo.requestTecTerms}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">询价规则</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <span class="col-xs-5">创建人：${rfqRequestComplexVo.requestVo.recCreatorUsername} </span>
                                        <span class="col-xs-5">创建时间：${rfqRequestComplexVo.requestVo.recCreateTime}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">发布时间：${rfqRequestComplexVo.requestVo.issueDate}</span>
                                        <span class="col-xs-5">报名截止时间：${rfqRequestComplexVo.rulesVo.registrationEndDate}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">报价开始时间：${rfqRequestComplexVo.rulesVo.quotationStartDate}</span>
                                        <span class="col-xs-5">报价截止时间：${rfqRequestComplexVo.requestVo.quotationEndDate}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">保证金金额：${rfqRequestComplexVo.rulesVo.assureMoney}元</span>
                                        <span class="col-xs-5">报价货币：${rfqRequestComplexVo.requestVo.currency}</span>
                                    </div>
                                    <div class="mt12 clearfix">
                                        <span class="col-xs-5">分项分量报价：
                                          <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag=='1'}">
                                              允许对部分产品报价
                                          </c:if>
                                           <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1'}">
                                               不允许对部分产品报价
                                           </c:if>
                                          <c:if test="${rfqRequestComplexVo.rulesVo.partialQuantityFlag=='1'}">
                                              &nbsp; 允许对部分数量报价
                                          </c:if>
                                            <c:if test="${rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }">
                                                不允许对部分数量报价
                                            </c:if>
                                        </span>
                                        <span class="col-xs-5">询价公开程度：
                                          <c:if test="${rfqRequestComplexVo.rulesVo.isPublicOnlineMarket=='1'}">
                                              公开询价公告
                                          </c:if>
                                          <c:if test="${rfqRequestComplexVo.rulesVo.isPublicPub=='1'}">
                                              &nbsp; 公开中标供应商
                                          </c:if>
                                         <c:if test="${rfqRequestComplexVo.rulesVo.isPublicBidMoney=='1'}">
                                             &nbsp; 公开中标价格
                                         </c:if>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                    <div>
                        <div class="line-32 mt12">
                            <span class="left" style="font-size:16px; display:inline-block">角色设置</span>
                        </div>
                        <table class="table table-bordered align-md">
                            <thead>
                            <tr>
                                <th width="50px">序号</th>
                                <th>工号</th>
                                <th>姓名</th>
                                <th>部门</th>
                                <th>职位</th>
                                <th>查看</th>
                                <th>管理</th>
                                <th>财务管理</th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="item" items="${rfqRequestComplexVo.projectUserList}">
                                    <tr data-clone="orginal">
                                        <td>${item.seq}</td>
                                        <td>${item.jobCode}</td>
                                        <td>${item.userName}</td>
                                        <td>${item.userOffice}</td>
                                        <td>${item.userTitle}</td>
                                        <td>
                                            <input disabled <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if> type="checkbox">
                                        </td>
                                        <td>
                                            <input disabled <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if> type="checkbox">
                                        </td>
                                        <td>
                                            <input disabled <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if> type="checkbox">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">联系方式</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="mg-pacne clearfix" style="margin:0 15px;">
                                    <ul>
                                        <li>联系人：${rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                        <li>联系电话：${rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                        <li style="width:40%">交货地址：
                                            ${rfqRequestComplexVo.contactsVo.deliveryProvince}
                                            ${rfqRequestComplexVo.contactsVo.deliveryCity}
                                            ${rfqRequestComplexVo.contactsVo.deliveryArea}
                                            ${rfqRequestComplexVo.contactsVo.deliveryAddress}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mg-header5">
                        附件
                    </div>
                    <table class="table table-bordered align-md" style="margin-bottom:5px;">
                        <thead>
                        <tr>
                            <th width="50px">序号</th>
                            <th>文件名称</th>
                            <th width="100px">文件大小</th>
                            <th>说明</th>
                            <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                                <th width="100px">操作</th>
                           <%-- </p:permission>--%>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="sum" value="1"/>
                            <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}" varStatus="status">
                                <c:if test="${item.type == 'R'}">
                                <tr data-clone="orginal">
                                    <td data-order="true">${sum}</td>
                                    <td>${item.originalFilename}</td>
                                    <td>${item.fileSize}KB</td>
                                    <td>${item.fileDeclaration}</td>
                                    <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                                        <td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td>
                                    <%--</p:permission>--%>
                                </tr>
                                    <c:set var="sum" value="${sum + 1}"/>
                                </c:if>
                          </c:forEach>
                        </tbody>
                    </table>
                    <div class="red line-32" style="font-size:12px;">
                        注：附件类型不支持exe格式，文件大小不超过100M。
                    </div>
                    <div class="text-right mt12">
                        <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                            <a href="javascript:;" class="btn btn-lg btn-warning js-form-btn" type="button">发布</a>
                        </p:permission>
                        <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-lg btn-primary" type="button" target="_blank">审批纪录</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- 调整时间弹窗开始 -->
    <div class="modal fade" id="adjustTime">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">关闭</span></button>
                    <h4 class="modal-title">调整时间</h4>
                </div>
                <form id="adjustTime_form">
                    <div class="modal-body modal-body2">
                        <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                            <div class="mg-row">
                                <span class="block-title">报名截止时间：</span>
                                 <div class="input-group">
                                    <input type="text" name="registrationEndDate" placeholder="${rfqRequestComplexVo.rulesVo.registrationEndDate}"
                                           value="${rfqRequestComplexVo.rulesVo.registrationEndDate}" class="form-control form-datetime">
                                </div>
                            </div>
                        </c:if>
                        <div class="mg-row">
                            <span class="block-title">报价开始时间：</span>
                            <div class="input-group">
                                <input type="text" id="quotationStartDate" name="quotationStartDate" placeholder="${rfqRequestComplexVo.rulesVo.quotationStartDate}"
                                       value="${rfqRequestComplexVo.rulesVo.quotationStartDate}" class="form-control form-datetime">
                                <input type="hidden" name="requestId" value="${rfqRequestComplexVo.rulesVo.requestId}" />
                                <input type="hidden" name="ruleId" value="${rfqRequestComplexVo.rulesVo.ruleId}" />
                                <input type="hidden" name="publicBiddingFlag" value="${rfqRequestComplexVo.rulesVo.publicBiddingFlag}" />
                            </div>
                        </div>
                        <div class="mg-row">
                            <span class="block-title">报价截止时间：</span>
                            <div class="input-group">
                                <input type="text" id="quotationEndDate" name="quotationEndDate" placeholder="${rfqRequestComplexVo.rulesVo.quotationEndDate}"
                                       value="${rfqRequestComplexVo.rulesVo.quotationEndDate}" class="form-control form-datetime">
                            </div>
                        </div>
                        <div><span class="required"></span></div>
                        <label class="col-md-2" style="line-height: 27px">调整原因：</label>
                        <div class="">
                            <textarea class="form-control" rows="6" placeholder="" name="reason"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-primary save_Time">保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 调整时间弹窗结束 -->

    <!-- 角色设置弹窗开始 -->
    <div class="modal fade" id="myModa-jssz">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                    <button class="btn right btn-warning btn-sm" type="button"  data-toggle="modal" data-target="#myModaphone" id="prouser"><i class="icon icon-plus"></i> 新增角色</button>
                    <h4 class="modal-title">角色设置</h4>
                </div>
                <form id="role_form">
                    <input type="hidden" name="requestBo.id" value="${rfqRequestComplexVo.rulesVo.requestId}" />
                    <div class="modal-body">
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">

                    </div>
                    <div class="content" style="padding:0 15px;">
                        <table class="table table-bordered table-hove  align-md" id="roleSetTable">
                            <thead>
                            <tr>
                                <th width="50px">序号</th>
                                <th>工号</th>
                                <th>姓名</th>
                                <th>部门</th>
                                <th>职位</th>
                                <th width="80">查看</th>
                                <th width="80">操作</th>
                                <th width="80">财务管理</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${rfqRequestComplexVo.projectUserList}" varStatus="status">
                                <tr id="puser_${item.userId}">
                                    <td data-name="userId" data-value="${item.userId}" >${item.seq}</td>
                                    <td data-name="jobCode" data-value="${item.jobCode}">${item.jobCode}</td>
                                    <td data-name="userName" data-value="${item.userName}" >${item.userName}</td>
                                    <td data-name="userOffice" data-value="${item.userOffice}">${item.userOffice}</td>
                                    <td data-name="userTitle" data-value="${item.userTitle}">${item.userTitle}</td>
                                    <c:choose>
                                        <c:when test="${item.userId == rfqRequestComplexVo.requestVo.recCreatorUserid}">
                                            <td>
                                                <input
                                                        <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if> disabled type="checkbox">
                                            </td>
                                            <td>
                                                <input
                                                        <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if> disabled type="checkbox">
                                            </td>
                                            <td>
                                                <input
                                                        <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if> disabled type="checkbox">
                                            </td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>
                                                <input
                                                        <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if> type="checkbox">
                                            </td>
                                            <td>
                                                <input
                                                        <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if> type="checkbox">
                                            </td>
                                            <td>
                                                <input
                                                        <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if> disabled type="checkbox">
                                            </td>
                                        </c:otherwise>

                                    </c:choose>
                                    <td>
                                       <%-- <c:if test="${status.index>0}">
                                            <a href="javascript:;" class=" text-danger order-del-btn" data-id="${item.userId}">
                                                <i class="icon icon-trash"></i>删除
                                            </a>
                                        </c:if>--%>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>

                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary save_role">确定</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 角色设置弹窗结束 -->

    <!-- ///////////////////////新增角色/////////////// -->
    <div class="modal fade"  id="myModaphone">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">关闭</span>
                    </button>
                    <h4 class="modal-title">角色设置</h4>
                </div>
                <div class="modal-body">
                <form>
                <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">姓名： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-8 ml12 text-right">工号： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="jobCode">
                        </div>
                        <div class="so-form-8 ml12 text-right">部门： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userOffice">
                        </div>
                        <div class="so-form-15 ml12">
                            <button class="btn btn-primary" type="button" id="roleSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form >
                <div class="content">
                    <table class="" id="jqGridphone">
                        <div class="col-md-12" id="jqGridPagerphone"></div>
                    </table>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save_role js-confirm">确定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" >取消</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.dropdown-trigger').dropdown();
        //日期

    $(".form-datetime").datetimepicker({
            weekStart: 1,
            startDate:new Date(), //开始时间，在这时间之前都不可选
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1,
            format: "yyyy-mm-dd hh:ii"
        });

    $(".form-date").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });

</script>

</body>
</html>