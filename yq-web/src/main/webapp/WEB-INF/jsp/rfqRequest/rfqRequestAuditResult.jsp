<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>结果待审批</title>
    <%@include file="../common/headBase.jsp" %>
    <%@include file="../common/jqGridBootstrap.jsp" %>
    <c:import url="../common/kingEditor.jsp" />
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequestState/rfqRequestState.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/js/designateResult/rfqDesignateMoney.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequest/rfqRequestEnd.js?v=${version}"></script>
</head>

<body>
<c:import url="../common/top.jsp"/>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="requestId" value="${param['id']}">
<input type="hidden" id="type" value="${rfqRequestComplexVo.requestVo.type}">
<input type="hidden" id="rfqMethod" value="${rfqRequestComplexVo.requestVo.rfqMethod}"/>
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">结果待审批</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right"><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">询价单摘要</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价单摘要</c:when></c:choose></span>
                            <div class=" pull-right">
                                <c:choose>
                                    <c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">
                                        <p:permission  privilege="1" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                            <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${param['id']}" target="_blank"> <span class="btn btn-info font12">询单详情</span></a>
                                        </p:permission>
                                    </c:when>
                                    <c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">
                                        <p:permission  privilege="1" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                            <a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${param['id']}" target="_blank"> <span class="btn btn-info font12">竞价单详情</span></a>
                                        </p:permission>
                                    </c:when>
                                </c:choose>
                                <p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <%--审批员角色结果审批页面--%>
                                    <span class="btn btn-danger font12" data-position="center" <%--data-target="#myModa20"--%>onclick="getBill()">导出报价单</span>
                                </p:permission>
                                <%--<span class="btn btn-warning font12">打印报价单</span>--%>

                            </div>
                        </div>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">询价单号</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价单号</c:when></c:choose>：${rfqRequestComplexVo.requestVo.ouRfqNum}</li>
                                        <li><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">询价标题</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价标题</c:when></c:choose>：${rfqRequestComplexVo.requestVo.title}</li>
                                        <li>采购单位：${rfqRequestComplexVo.requestVo.ouName}</li>
                                        <li>计划编号：${rfqRequestComplexVo.requestVo.planNo}</li>
                                        <li>当前状态：
                                                    <span class="red">
                                                             结果待审核
                                                    </span>
                                        </li>
                                        <li>邀请范围：<c:choose><c:when test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag == '1'}">公开寻源</c:when><c:otherwise>定向寻源</c:otherwise></c:choose>
                                        </li>
                                    </ul>
                                       <%--     <span class="zt-ico-w">
                                                     <img src="${ctx}/images/jieshu.png">
                                            </span>--%>
                                </div>
                                <div class="mg-pacne clearfix">
                                <ul>
                                    <li>保证金金额：${rfqRequestComplexVo.rulesVo.assureMoney}元</li>
                                    <li><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">报价货币</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价币种</c:when></c:choose>：<c:choose><c:when test="${rfqRequestComplexVo.requestVo.currency=='CNY'}">人民币CNY</c:when><c:when test="${rfqRequestComplexVo.requestVo.currency=='USD'}">美元USD</c:when><c:when test="${rfqRequestComplexVo.requestVo.currency=='GBP'}">英镑GBP</c:when><c:when test="${rfqRequestComplexVo.requestVo.currency=='JPY'}">日元JPY</c:when><c:when test="${rfqRequestComplexVo.requestVo.currency=='EUR'}">欧元EUR</c:when><c:when test="${rfqRequestComplexVo.requestVo.currency=='HKD'}">港元HKD</c:when><c:when test="${rfqRequestComplexVo.requestVo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                                    <li><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">预算总价</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">参考总价</c:when></c:choose>： <span class="red">${rfqRequestComplexVo.requestVo.totalPrice}元</span></li>
                                </ul>
                            </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：${rfqRequestComplexVo.requestVo.issueDate}</li>
                                        <li><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">报价开始时间</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价开始时间</c:when></c:choose>：${rfqRequestComplexVo.rulesVo.quotationStartDate}</li>
                                        <li><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">报价截止时间</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价截止时间</c:when></c:choose>：${rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                        <li>报名截止时间：${rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                        <li>联系电话：${rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                        <li>收货地址：
                                            ${rfqRequestComplexVo.contactsVo.deliveryProvince}
                                            ${rfqRequestComplexVo.contactsVo.deliveryCity}
                                            ${rfqRequestComplexVo.contactsVo.deliveryArea}
                                            ${rfqRequestComplexVo.contactsVo.deliveryAddress}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--核价说明--%>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">核价说明</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    ${rfqRequestComplexVo.requestVo.resultMemo}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-content">
                        <div class="mg-header mt20">
                            <c:choose>
                                <c:when test="${rfqRequestComplexVo.rulesVo.biddingMethod !='1'}">
                                    中标情况：（中标供应商：<span style="color:red" id="zhongbiaogys">0</span>家；采购总金额：<span style="color:red" id="totalMoney">0</span>元）
                                </c:when>
                                <c:when test="${rfqRequestComplexVo.rulesVo.biddingMethod =='1'}">
                                    中标情况：（中标供应商：<span style="color:red">${designeVo.supplierName}</span>&nbsp;&nbsp;&nbsp; 中标金额：<span style="color:red">${designeVo.subtotalTaxed}</span>元）
                                </c:when>
                            </c:choose>

                            <span class="pull-right">
                                <a href="${pageContext.request.contextPath}/rfqRequestState/quotationOverview?id=${rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">报价总览</a>
                                <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}"  class="trage" target="_blank">审批历史</a>
                                <%--<p:permission  privilege="1" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                                    <a class="trage" href="${pageContext.request.contextPath}/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank">报名历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?id=${rfqRequestComplexVo.requestVo.id}" target="hjsp-quotation_history_list.html" class="trage">报价历史</a>
                                    <a href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="gysbj-caozuorizhi.html" class="trage">操作日志</a>
                                <%--</p:permission>--%>
                            </span>
                        </div>
                        <c:choose>
                            <c:when test="${rfqRequestComplexVo.rulesVo.biddingMethod !='1'}">
                                <ul id="myTab" class="nav nav-tabs">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="supplier_type">按供应商查看</a></li>
                                    <li class=""> <a href="#tab2" data-toggle="tab" class="item_type">按物料查看</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane example active" id="tab1">

                                    </div>
                                    <div class="tab-pane  example" id="tab2">

                                    </div>
                                    <div class="so-form-60 right mt20" id="buttom">
                                        <div class="pull-right" style="margin-top:-18px;">
                                            <ul class="pager pager-loose pagers">
                                                <li><span>共 <strong class="text-danger" id="total"></strong> 条记录 </span></li>
                                                <li><span><strong class="text-danger" id="pageNum"></strong></strong><strong class="text-danger">/</strong><strong id="pages" class="text-danger"></strong> 页</span></li>
                                                <li><span>每页显示
                                  <select name="" class="form-control input-sm" id="pageSize">
                                      <option selected="selected">20</option>
                                      <option>30</option>
                                      <option>50</option>
                                      <option>100</option>
                                          <%--<option>100</option>--%>
                                  </select>
                                  条</span></li>
                                                <li id="homePage"><a  href="javascript:;" class="blue"   id="ahomePage">首页</a>   </li>
                                                <li id="lastPage"><a  href="javascript:;" class="blue"   id="alastPage">上一页</a>   </li>
                                                <li id="nextPage"><a  href="javascript:;" class="blue"   id="anextPage">下一页</a>   </li>
                                                <li id="endPage" ><a   href="javascript:;" class="blue"  id="aendPage" >尾页</a>   </li>

                                                <li><span>转到
                                  <input type="text" class="form-control input-sm" size="3" id="pageNo" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" autocomplete="off">
                                  页</span> <span class="ml12"><a class="btn btn-sm" id="toPage">确定</a></span></li>
                                            </ul>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${rfqRequestComplexVo.rulesVo.biddingMethod =='1'&& rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">
                                <div class="jqGridPage">
                                    <table id="jqGrid"></table>
                                    <div id="jqGridPager"></div>
                                </div>

                                <div class="clearfix"></div>
                            </c:when>
                        </c:choose>
                        <%--审核部分--%>
                        <form method="post" id="signAuditForm" action="signAudit" onsubmit="return false;">
                            <input type="hidden" name="requestId" value="${requestId}" />
                            <input type="hidden" name="auditType" value="${auditType}" />
                            <input type="hidden" name="eventNumber" value="${eventNumber}" />
                            <div class="form-group font-16px ">
                                审批意见<i class="r">*</i>：
                                <div class="mt12">
                                    <textarea id="auditingDesc" name="" maxlength="2000" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;"></textarea>
                                </div>
                            </div>
                            <div class="text-right">
                                <c:if test="${procDefType == 2}">
                                    <a href="javascript:;" class="btn btn-md btn-primary" data-toggle="modal" data-target="#myRole" type="button">流转</a>
                                </c:if>
                                <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                                <a href="javascript:subAudit('1');" class="btn btn-md btn-primary" type="button">审批通过</a>
                                <a href="javascript:subAudit('0');" class="btn btn-md btn-warning" type="button">审批驳回</a>
                                <%--<a href="javascript:subAudit('2');" class="btn btn-md btn-warning" type="button">审批失败</a>--%>
                                <%--</p:permission>--%>
                            </div>
                        </form>
                    </div>
                </div>
               </div>
            </div>
        </div>
    </div>
</div>


<!-- 即时聊天-->
<%--<a href="#" id="im"></a>--%>

<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">撤销询价原因</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <p>
                        <span class="pull-left line-32">原因类别：</span>

                        <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                            <option value="不需要采购">不需要采购</option>
                            <option value="供应商退出">供应商退出</option>
                            <option value="其他原因">其他原因</option>
                        </select>
                    </p>
                    <div class="clearfix"></div>
                    <p>
                        <span class="pull-left line-32">原因说明：</span>

                        <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="adjustTime">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">调整时间</h4>
            </div>
            <div class="modal-body modal-body2">
                <div class="mg-row">
                    <span class="block-title">报名截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                            立即结束<span class="caret"></span>
                        </button>
                        <ul id="select1" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            <li><a href="javascript:void(0)" onclick="nowTime(1);">立即结束</a></li>
                            <li><a href="#">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date1" placeholder="2016/02/29" class="form-control form-datetime" onchange="changeTime(1)">
                    </div>
                </div>
                <div class="mg-row">
                    <span class="block-title">报价开始时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                            立即开始<span class="caret"></span>
                        </button>
                        <ul id="select2" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                            <li><a href="javascript:void(0)" onclick="nowTime(2);">立即开始</a></li>
                            <li><a href="#">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date2" placeholder="2016/02/29" class="form-control form-datetime" onchange="changeTime(2)">
                    </div>
                </div>
                <div class="mg-row">
                    <span class="block-title">报价截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                        <ul id="select3" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                            <li><a href="javascript:void(0)" onclick="nowTime(3);">立即结束</a></li>
                            <li><a href="#">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date3" placeholder="2016/02/29" class="form-control form-datetime" onchange="changeTime(3)">
                    </div>
                </div>
                <div><span class="required"></span></div>
                <label class="col-md-2" style="line-height: 27px">调整原因：</label>
                <div class="">
                    <textarea class="form-control" rows="6" placeholder=""></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateTime()">保存</button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="myModa1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>询价单号</th>
                            <th>询单名称</th>
                            <th>创建人</th>
                            <th>提交时间</th>
                            <th>审批人信息</th>
                            <th>审批类型</th>
                            <th>审批时间</th>
                            <th>状态</th>
                            <th>审批等级</th>
                            <th>审批理由</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td>同意</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:;">确定</a> </div>
    </div>
</div>

<div class="modal fade" id="myModa20">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">导出报价单</h4>
            </div>
            <div class="modal-body">
                <div class="content">

                    <p>
                        <span class="pull-left line-32">指定授标供应商：</span>

                        <select class="form-control pull-left" name="" style="width:200px;">
                            <c:forEach var="supplierList" items="${priceList}" varStatus="status">
                                <option value="${supplierList.supplierCode}">${supplierList.supplierName}</option>
                            </c:forEach>
                        </select>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:;">确定</a>
                <a href="javascript:;" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>

<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo">
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="transfer">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->

<!-- 结束 -->
<script src="${pageContext.request.contextPath}/js/base.js?v=${version}"></script>
<script>

    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });

    function getBill() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd",
            data: {id:"${rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
</script>
</body>
</html>
