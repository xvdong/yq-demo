<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.type == '10'?'已结束':'结果待审批'}</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <c:import url="../common/kingEditor.jsp" />
    <script src="${resource}/js/rfqRequest/rfqRequestEnd.js?v=${version}"></script>
    <style>
        .example{overflow:hidden}

        /*中标公司在一排显示的样式*/
        .com-list{overflow: hidden; height: 45px}
        .com-list ul li a{width: 94px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;}
        .com-list ul{width: auto; position: relative;}

    </style>
</head>
<body>
<c:import url="../common/top.jsp" />
<c:set var="request" value="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo}"/>
<input type="hidden" id="requestId" value="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">${request.type == '10'?'已结束':'结果待审批'}</li>
                    </ol>
                </div>
                <input type="hidden" id="dataSource" value="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.dataSource}"/>
                <c:set var="ouRfqNum" value="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}"/>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">询价单摘要</span>
                            <div class=" pull-right">
                                <c:if test="${sessionScope.chatUrl==null || sessionScope.chatUrl == ''}">
                                    <a id = 'im' href="javascript:RFQ.warn('提示：该功能仅在uat环境与生产环境可用！');"></a>
                                </c:if>
                                <c:if test="${sessionScope.chatUrl!=null && sessionScope.chatUrl != ''}">
                                    <a id = 'im' target="_blank"  href="${sessionScope.chatUrl}${ouRfqNum}"></a>
                                </c:if>
                                <p:permission  privilege="1" requestNo="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank">
                                        <span class="btn btn-info font12">询单详情</span>
                                    </a>
                                </p:permission>
                                <p:permission  privilege="2" requestNo="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                    <span class="btn btn-danger font12" data-position="center"  <%--data-target="#myModa20"--%>onclick="getBill()">导出报价单</span>
                                    <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.type == '10'}">
                                        <span class="btn btn-danger font12" data-position="center"  <%--data-target="#myModa20"--%>onclick="getBill2()">导出成交通知单</span>
                                    </c:if>
                                    <span class="btn btn-danger font12" data-position="center"  <%--data-target="#myModa20"--%>onclick="getResult()">导出竞价结果</span>
                                </p:permission>

                                <%--<span class="btn btn-warning font12">打印报价单</span>--%>
                            </div>
                        </div>
                        <input type="hidden" id="ouRfqNum" value="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}"/>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>询价单号：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.ouRfqNum}</li>
                                        <li>询价标题：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.title}</li>
                                        <li>采购单位：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.ouName}</li>
                                        <li>计划编号：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.planNo}</li>
                                        <li>当前状态：
                                                    <span class="red">
                                                        ${request.type == '10'?'已结束':'结果待审批'}
                                                    </span>
                                        </li>
                                    </ul>
                                            <span class="zt-ico-w zt-gz">
                                                     <img src="${ctx}/images/${request.type == '10'?'jiesu':'dsh'}.png">
                                            </span>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.assureMoney}元</li>
                                        <li>报价货币：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.currency}</li>
                                        <li>预算总价：<span class="red">${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.totalPrice}元</span></li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.issueDate}</li>
                                        <li>报价开始时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.quotationStartDate}</li>
                                        <li>报价截止时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.quotationEndDate}</li>
                                        <li>报名截止时间：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.rulesVo.registrationEndDate}</li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                        <li>联系电话：${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                        <li>收货地址：
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryProvince}
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryCity}
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryArea}
                                            ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.contactsVo.deliveryAddress}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--核价说明--%>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">核价说明</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                    ${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.resultMemo}                                </div>
                            </div>
                        </div>
                    </div>
                    <div <c:if test="${request.type == '9'}">style="display: none"</c:if>>
                    <div class="">
                        <div class="form-group" style="margin:0;">
                            <div class="mg-header">中标说明：</div>
                            <div class="com-list">
                                <ul id="myTab" class="nav nav-tabs mt12">
                                    <c:forEach var="supplierList" items="${rfqQuotationNoticeComplexVo.rfqQuotationNoticeVo}" varStatus="status">
                                        <li class="<c:if test="${status.index==0}">active</c:if>">
                                            <a href="#tabgs${status.index+1}" data-toggle="tab" title="${supplierList.supplierName}">${supplierList.supplierName}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <c:forEach var="supplierList" items="${rfqQuotationNoticeComplexVo.rfqQuotationNoticeVo}" varStatus="status">
                                    <div class="tab-pane example <c:if test="${status.index==0}">active</c:if>" id="tabgs${status.index+1}">
                                        <textarea id="contentSimple${status.index+2}" readonly="readonly" name="content" class= "" style=" width:100%;height:150px;color:#666;">${supplierList.memoDesc}</textarea>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>

                    </div>

                    <div class="mg-header">未中标说明：</div>
                    <div>
                        <textarea id="contentSimple9" name="content" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">${rfqQuotationNoticeComplexVo.rfqQuotationNoticeVoN.memoDesc}</textarea>
                    </div>
                    </div>

                    <div class="mg-header mt20">
                        中标情况：（中标供应商：<span style="color:red" id="zhongbiaogys">0</span>家；采购总金额：<span style="color:red" id="totalMoney">0</span>元）
                            <span class="pull-right">

                                <p:permission  privilege="1" requestNo="${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}">
                                <a class="trage" href="${pageContext.request.contextPath}/rfqPreauditSupplier/auditHistory?unifiedRfqNum=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank">报名历史</a>
                                <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">审批历史</a>
                                <a href="${pageContext.request.contextPath}/rfqLaunchedSeveralPrice/inity?id=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}" target="_blank" class="trage">报价历史</a>
                                <a href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.unifiedRfqNum}" target="_blank" class="trage">操作日志</a>
                                </p:permission>
                            </span>
                    </div>
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab" class="supplier_type">按供应商查看</a></li>
                        <li class=""> <a href="#tab2" data-toggle="tab" class="item_type">按物料查看</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane example active" id="tab1" style=" clear: both;">

                        </div>
                        <div class="tab-pane  example" id="tab2" style=" clear: both;">

                        </div>


                        <div class="so-form-60 right mt20" id="buttom">
                            <div class="pull-right" style="margin-top:-18px;">
                                <ul class="pager pager-loose pagers">
                                    <li><span>共 <strong class="text-danger" id="total"></strong> 条记录 </span></li>
                                    <li><span><strong class="text-danger" id="pageNum"></strong></strong><strong class="text-danger">/</strong><strong id="pages" class="text-danger"></strong> 页</span></li>
                                    <li><span>每页显示
                                  <select name="" class="form-control input-sm" id="pageSize">
                                      <option selected="selected">20</option>
                                      <option>30</option>
                                      <option>50</option>
                                      <option>100</option>
                                      <%--<option>100</option>--%>
                                  </select>
                                  条</span></li>
                                    <li id="homePage"><a href="javascript:;" class="blue"   id="ahomePage">首页</a></li>
                                    <li id="lastPage"><a href="javascript:;" class="blue"   id="alastPage">上一页</a></li>
                                    <li id="nextPage"><a href="javascript:;" class="blue"   id="anextPage">下一页</a></li>
                                    <li id="endPage"><a   href="javascript:;" class="blue"  id="aendPage"  >尾页</a></li>
                                    <li><span>转到
                                  <input type="text" class="form-control input-sm" size="3" id="pageNo" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" autocomplete="off">
                                  页</span> <span class="ml12"><a class="btn btn-sm" id="toPage">确定</a></span></li>
                                </ul>
                            </div>
                        </div>

                        <hr>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- 即时聊天
 <a href="#" id="im"></a>
 -->



<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">竞价终止原因</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row">
                    <span class="left-title">原因类别：</span>
                    <div class="right-paner">
                        <div class="btn-group">
                            <button class="btn btn-hui dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                不需要采购<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">编辑</a></li>
                                <li><a href="#">删除</a></li>
                                <li><a href="#">修改</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mg-row">
                    <span class="left-title">原因说明：</span>
                    <div class="right-paner">
                        <textarea class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary">保存</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="adjustTime">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">调整时间</h4>
            </div>
            <div class="modal-body modal-body2">
                <div class="mg-row">
                    <span class="block-title">报名截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                            立即结束<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">立即结束</a></li>
                            <li><a href="#">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date" placeholder="2016/02/29" class="form-control form-date">
                    </div>
                </div>
                <div class="mg-row">
                    <span class="block-title">报名截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                            立即开始<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                            <li><a href="#">立即开始</a></li>
                            <li><a href="#">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date" placeholder="2016/02/29" class="form-control form-date">
                    </div>
                </div>
                <div class="mg-row">
                    <span class="block-title">报名截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                            <li><a href="#">立即结束</a></li>
                            <li><a href="#">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date" placeholder="2016/02/29" class="form-control form-date">
                    </div>
                </div>
                <div><span class="required"></span></div>
                <label class="col-md-2" style="line-height: 27px">调整原因：</label>
                <div class="">
                    <textarea class="form-control" rows="6" placeholder=""></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary">保存</button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="myModa1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">报名历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>供应商U代码</th>
                            <th>供应商名称</th>
                            <th>联系人</th>
                            <th>联系电话</th>
                            <th>报名时间</th>
                            <th>审批时间</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>687777777</td>
                            <td>中国联想 <a class="label label-warning pull-right" href="javascript:;">待考察</a></td>
                            <td>李四</td>
                            <td>15869784578</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>已通过</td>
                            <td><a class="btn btn-sm btn-success" href="baomingshenpichakan-yibohui.html" target="baomingshenpichakan-yibohui"><i class="icon icon-zoom-in"></i>&nbsp;查看</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>687777777</td>
                            <td>中国联想 <a class="label label-warning pull-right" href="javascript:;">待考察</a></td>
                            <td>李四</td>
                            <td>15869784578</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>已通过</td>
                            <td><a class="btn btn-sm btn-success" href="baomingshenpichakan-yibohui.html" target="baomingshenpichakan-yibohui"><i class="icon icon-zoom-in"></i>&nbsp;查看</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>687777777</td>
                            <td>中国联想 <a class="label label-warning pull-right" href="javascript:;">待考察</a></td>
                            <td>李四</td>
                            <td>15869784578</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>已通过</td>
                            <td><a class="btn btn-sm btn-success" href="baomingshenpichakan-yibohui.html" target="baomingshenpichakan-yibohui"><i class="icon icon-zoom-in"></i>&nbsp;查看</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>687777777</td>
                            <td>中国联想 <a class="label label-warning pull-right" href="javascript:;">待考察</a></td>
                            <td>李四</td>
                            <td>15869784578</td>
                            <td width="100">2011-01-02 06:06</td>
                            <td width="100">2011-01-02 06:06</td>
                            <td>已通过</td>
                            <td><a class="btn btn-sm btn-success" href="baomingshenpichakan-yibohui.html" target="baomingshenpichakan-yibohui"><i class="icon icon-zoom-in"></i>&nbsp;查看</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:;">确定</a> </div>
    </div>
</div>

<div class="modal fade" id="myModa20">
    <div class="modal-dialog">
        <form method="post" class="form-horizontal" id="modalFrom" role="form" onsubmit="return false;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only">关闭</span></button>
                    <h4 class="modal-title">导出报价单</h4>
                </div>
                <div class="modal-body">
                    <div class="content">

                        <p>
                            <span class="pull-left line-32">指定授标供应商：</span>

                            <select class="form-control pull-left" name="supplier" style="width:200px;">
                                <c:forEach var="supplierList" items="${priceList}" varStatus="status">
                                    <option value="${supplierList.supplierCode}">${supplierList.supplierName}</option>
                                </c:forEach>
                            </select>
                        </p>
                    </div>
                </div>
                <div class="clearfix mt12"></div>
                <div class="modal-footer">
                    <a type="submit" class="btn btn-primary" href="javascript:;" onclick="exportExcel()">确定</a>
                    <a href="javascript:;" class="btn" type="button" data-dismiss="modal">取消</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });
</script>

<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea#contentSimple', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
    });
</script>
<script>
    KindEditor.ready(function(K) {
        var editor2 = K.create('textarea#contentSimple2', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor3 = K.create('textarea#contentSimple3', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor4 = K.create('textarea#contentSimple4', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor5 = K.create('textarea#contentSimple5', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor5 = K.create('textarea#contentSimple6', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor5 = K.create('textarea#contentSimple7', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor5 = K.create('textarea#contentSimple8', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
        var editor5 = K.create('textarea#contentSimple9', {
            resizeType : 1,
            allowPreviewEmoticons : false,
            allowImageUpload : false,
            items : [],
            readonlyMode : true
        });
    });
</script>

<%--中标说明中公司后面上一个下一个按键控制的js--%>
<script>
    comList();
    function comList(){
        var l = $('.com-list ul li').length;
        if(l>10){
            var nspeed = 0;
            var m = 0;
            var lb = 9;
            var speed = 96;
            $('.com-list ul').append("<div class='lr-btn'><button class='abtn c-nallow btn btn-sm mr12'><i class='icon icon-angle-left'></i></button><button class='bbtn btn btn-sm btn-primary'><i class='icon icon-angle-right'></i></button></div>");
            $('.com-list .lr-btn').css({"position":"absolute","right":"0","top":"0px"});
            $('.com-list ul li').click(function(){
                //console.log($(this).index()+m);
                if($(this).index()-m==9){
                    $('.bbtn').trigger('click');
                }else if($(this).index()-m==0){
                    $('.abtn').trigger('click');
                }
            });
            $('.abtn').click(function(){
                if(m!=0){
                    nspeed+=speed;
                    m--;
                    $(this).parents('ul').css("margin-left",nspeed);
                    $('.bbtn').addClass('btn-primary');
                }
                if(m<=0){
                    $(this).removeClass('btn-primary');
                }
            });

            $('.bbtn').click(function(){
                if(m<l-lb-1){
                    nspeed-= speed;
                    m++;
                    $(this).parents('ul').css("margin-left",nspeed);
                    $('.abtn').addClass('btn-primary');
                }
                if(m>=l-lb-1){
                    $(this).removeClass('btn-primary');
                }
            })
        }
        else{
            if($('.lr-btn')){
                $('.com-list .lr-btn').remove();
            }
        }
    }

    function getBill() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd",
            data: {id:"${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                        location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }
    function getBill2() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqHtmlToPdf/rfqRequestEnd2",
            data: {id:"${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqHtmlToPdf/downLoadPdf",
                    data: {data:data},
                    success: function (data) {
                        var data;
                        try
                        {
                            data = eval('('+data+')');
                        }
                        catch (e)
                        {
                            data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                        }
                            location.href=data.downLoadUrl;
                    }
                });
            }
        });
    }

    //导出竞价结果
    function getResult() {
        $.ajax({
            type: "POST",
            url: ctx+"/rfqExportExcel/rfqExportResult",
            data: {requestId:"${rfqQuotationNoticeComplexVo.rfqRequestComplexVo.requestVo.id}"},
            success: function (data) {
                var data;
                try
                {
                    data = eval('('+data+')');
                }
                catch (e)
                {
                    data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
                }

                location.href=data.downLoadUrl;
            }
        });
    }
</script>


</body>

</html>
