<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><c:choose><c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}">待报价</c:when><c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}">待竞价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='RAQ'}">报价中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}">竞价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequestState/rfqRequestState.js?v=${version}"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>

<body>
<c:import url="../common/top.jsp"/>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="isFlag" value="1">

<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active"><c:choose><c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}">待报价</c:when><c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}">待竞价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6' && vo.rfqMethod=='RAQ'}">报价中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}">竞价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right"><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">询价单摘要</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价单摘要</c:when></c:choose></span>
                            <div class=" pull-right">
                                <c:choose>
                                    <c:when test="${vo.rfqMethod=='DAC'&&vo.type == '6'}">
                                        <a href="${pageContext.request.contextPath}/biddingHall/init?requestId=${vo.id}"><span class="costom"><img src="${pageContext.request.contextPath}/images/freshaddon.png" style="width:40px; height:40px; margin-right: 5px; margin-top: -25px;">竞价大厅</span></a>
                                    </c:when>
                                </c:choose>
                                <p:permission  privilege="1" requestNo="${vo.unifiedRfqNum}">
                                    <c:choose>
                                        <c:when test="${vo.rfqMethod=='RAQ'}"><a href="${pageContext.request.contextPath}/rfqRequest/rfqRequestDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">询单详情</span></a></c:when>
                                        <c:when test="${vo.rfqMethod=='DAC'}"><a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">竞价单详情</span></a></c:when>
                                    </c:choose>
                                </p:permission>
                            </div>
                        </div>
                        <input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}">
                        <input type="hidden" name="quotationRound" id="quotationRound" value="${vo.quotationRound}">
                        <input type="hidden" name="id" id="id" value="${vo.id}">
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">询价单号</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价单号</c:when></c:choose>：${vo.ouRfqNum}</li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">询价标题</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价标题</c:when></c:choose>：${vo.title}</li>
                                        <li>采购单位：${vo.ouName}</li>
                                        <li>计划编号：${vo.planNo}</li>
                                        <li>当前状态：<span class="red" id="nowState" ><c:choose><c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}">待报价</c:when><c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}">待竞价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6' && vo.rfqMethod=='RAQ'}">报价中</c:when><c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}">竞价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></span></li>
                                        <li>邀请范围：<span id="publicBiddingFlag"><c:choose><c:when test="${vo.publicBiddingFlag == '1'}">公开寻源</c:when><c:otherwise>定向寻源</c:otherwise></c:choose></span></li>
                                    </ul>
                                    <c:choose>
                                        <c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbj.png"></span></c:when>
                                        <c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/djj.png"></span></c:when>
                                        <c:when test="${vo.type == '5'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bmz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bjz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/jjz.png"></span></c:when>
                                        <c:when test="${vo.type == '7'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dkb.png"></span></c:when>
                                        <c:when test="${vo.type == '11'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/yzf.png"></span></c:when>
                                        <c:when test="${vo.type == '12'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/ylb.png"></span></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span class="red"><c:choose><c:when test="${vo.assureMoney >= '0'}"><fmt:formatNumber type="number" value="${vo.assureMoney} " maxFractionDigits="4"/>元</c:when></c:choose></span></span></li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">报价货币</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价币种</c:when></c:choose>：<c:choose><c:when test="${vo.currency=='CNY'}">人民币CNY</c:when><c:when test="${vo.currency=='USD'}">美元USD</c:when><c:when test="${vo.currency=='GBP'}">英镑GBP</c:when><c:when test="${vo.currency=='JPY'}">日元JPY</c:when><c:when test="${vo.currency=='EUR'}">欧元EUR</c:when><c:when test="${vo.currency=='HKD'}">港元HKD</c:when><c:when test="${vo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">预算总价</c:when><c:when test="${vo.rfqMethod=='DAC'}">参考总价</c:when></c:choose>：<span class="red"><fmt:formatNumber type="number" value="${vo.totalBudget}" maxFractionDigits="0"/>元</span></li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag == '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag != '1' && rfqRequestComplexVo.rulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：<c:choose><c:when test="${fn:length(vo.issueDate)>'16'}">${fn:substring(vo.issueDate,0,16)}</c:when> <c:otherwise>${vo.issueDate}</c:otherwise></c:choose></li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">报价开始时间</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价开始时间</c:when></c:choose>：<span id ="startDate"><c:choose><c:when test="${fn:length(vo.startDate)>'16'}">${fn:substring(vo.startDate,0,16)}</c:when><c:otherwise>${vo.startDate}</c:otherwise></c:choose></span></li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">报价截止时间</c:when><c:when test="${vo.rfqMethod=='DAC'}">竞价截止时间</c:when></c:choose>：<span id ="quotationEndDate"><c:choose><c:when test="${fn:length(vo.quotationEndDate)>'16'}">${fn:substring(vo.quotationEndDate,0,16)}</c:when><c:otherwise>${vo.quotationEndDate}</c:otherwise></c:choose></span></li>
                                        <c:choose>
                                            <c:when test="${vo.publicBiddingFlag == '1'}">
                                                <li>报名截止时间：<span id ="registrationEndTime"><c:choose><c:when test="${fn:length(vo.registrationEndTime)>'16'}">${fn:substring(vo.registrationEndTime,0,16)}</c:when><c:otherwise>${vo.registrationEndTime}</c:otherwise></c:choose></span></li>
                                            </c:when>
                                        </c:choose>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${vo.linkmanName}</li>
                                        <li>联系电话：${vo.linkmanTelphone}</li>
                                        <li style="width:40%;">交货地址：${vo.deliveryProvince}${vo.deliveryCity}${vo.deliveryArea}${vo.deliveryAddress}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:if test="${vo.type == '11'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">终止类别：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                            ${vo.extendField4}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">终止原因：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                        ${vo.pubEndMemo}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <div class="page-content">
                        <div class="panel">
                    <div class="panel-heading clearfix">
                        <span class="mg-margin-right">供应商情况</span>

                        <span class="pull-right">
                            <a class="trage text-info" href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${vo.id}" target="_blank" >审批历史</a>
                   <p:permission  privilege="1" requestNo="${vo.unifiedRfqNum}">
                       <a class="trage text-info" href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${vo.unifiedRfqNum}" target="_blank">操作日志</a>
                   </p:permission>
                            </span>

                    </div>
                    <ul id="myTab" class="nav nav-tabs mt12">
                        <li class="active"><a href="#tab1" data-toggle="tab">追加供应商<span id="b1"></span></a></li>
                           <%-- <c:if test="${vo.quotationRound == '0'&& vo.publicBiddingFlag == '1'}">
                                <li class=""> <a href="#tab2" data-toggle="tab">报名供应商<span id="b2"></span></a></li>
                            </c:if>--%>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane example active"  id="tab1">
                            <table class="table table-bordered table-hove align-md" id="jqGrid1"></table>
                        </div>
                        <%--<c:choose>
                            <c:when test="${vo.publicBiddingFlag == '1'}">--%>
                        <div class="tab-pane example" id="tab2">
                            <table class="table table-bordered table-hove align-md" id="jqGrid2"></table>
                          <%--  </c:when>
                        </c:choose>--%>
                        </div>
                    </div>
                  </div>
                </div>
                    <%--审核部分--%>
                    <form method="post" id="signAuditForm" action="signAudit" onsubmit="return false;">
                        <input type="hidden" name="requestId" value="${requestId}" />
                        <input type="hidden" name="auditType" value="${auditType}" />
                        <input type="hidden" name="eventNumber" value="${eventNumber}" id="eventNumber"/>
                        <div class="form-group font-16px ">
                            审批意见<i class="r">*</i>：
                            <div class="mt12">
                                <textarea id="auditingDesc" name="" maxlength="2000" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;"></textarea>
                            </div>
                        </div>
                        <div class="text-right">
                            <c:if test="${procDefType == 2}">
                                <a href="javascript:;" class="btn btn-md btn-primary" data-toggle="modal" data-target="#myRole" type="button">流转</a>
                            </c:if>
                            <%--<p:permission  privilege="2" requestNo="${vo.unifiedRfqNum}">--%>
                                <a href="javascript:subAudit('1');" class="btn btn-md btn-primary" type="button">审批通过</a>
                                <a href="javascript:subAudit('0');" class="btn btn-md btn-warning" type="button">审批驳回</a>
                                <%--<a href="javascript:subAudit('2');" class="btn btn-md btn-warning" type="button">审批失败</a>--%>
                            <%--</p:permission>--%>
                        </div>
                    </form>
               </div>
            </div>
        </div>
    </div>
</div>


<!-- 即时聊天-->
<%--<a href="#" id="im"></a>--%>



<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title"><c:choose><c:when test="${vo.rfqMethod=='RAQ'}">撤销询价原因</c:when><c:when test="${vo.rfqMethod=='DAC'}">撤销竞价原因</c:when></c:choose></h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <p>
                        <span class="required"></span>
                        <span class="pull-left line-32" style="margin-left: 12px">原因类别：</span>

                        <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                            <option value="不需要采购">不需要采购</option>
                            <option value="供应商退出">供应商退出</option>
                            <option value="其他原因">其他原因</option>
                        </select>
                    </p>
                    <div class="clearfix"></div>
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因说明：</span>

                        <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal" onclick="resetValues1()">取消</a>
            </div>
        </div>
    </div>
</div>

<%--开标确认弹窗--%>
<div class="modal modal-for-page fade in" id="myModal2" aria-hidden="false" style="display: none;">
    <div class="modal-dialog" style="margin-top: 106px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title"><strong>是否确定进行开标</strong></h4>
            </div>
            <div class="modal-body">

                <p>点击“确定”将进行开标；点击“取消”将取消开标。</p>
            </div>
            <div class="modal-footer">
                <%--<a type="button" class="btn btn-primary" href="${pageContext.request.contextPath}/designateResult/init?unifiedRfqNum=${vo.unifiedRfqNum}">确定</a>--%>
                    <a type="button" class="btn btn-primary" href="javascript:void(0)" onclick="bidOpening()">确定</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>

            </div>
        </div>
    </div>
</div>


<%--<div class="modal fade" id="cancelPrice">--%>
    <%--<div class="modal-dialog">--%>
        <%--<div class="modal-content">--%>
            <%--<div class="modal-header">--%>
                <%--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>--%>
                <%--<h4 class="modal-title">竞价终止原因</h4>--%>
            <%--</div>--%>
            <%--<div class="modal-body">--%>
                <%--<div class="mg-row">--%>
                    <%--<span class="left-title">原因类别：</span>--%>
                    <%--<div class="right-paner">--%>
                        <%--<div class="btn-group">--%>
                            <%--<button class="btn btn-hui dropdown-toggle" type="button" id="dropdownMenu11" data-toggle="dropdown">--%>
                                <%--不需要采购<span class="caret"></span>--%>
                            <%--</button>--%>
                            <%--<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">--%>
                                <%--<li><a href="#">编辑</a></li>--%>
                                <%--<li><a href="#">删除</a></li>--%>
                                <%--<li><a href="#">修改</a></li>--%>
                            <%--</ul>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
                <%--<div class="mg-row">--%>
                    <%--<span class="left-title">原因说明：</span>--%>
                    <%--<div class="right-paner">--%>
                        <%--<textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class="modal-footer">--%>
                <%--<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>--%>
                <%--<button type="button" class="btn btn-primary">保存</button>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>
<div class="modal fade" id="adjustTime">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">调整时间</h4>
            </div>
            <div class="modal-body modal-body2">
                <c:if test="${vo.publicBiddingFlag == '1'}">
                    <div class="mg-row">
                        <span class="block-title">报名截止时间：</span>
                        <div class="btn-group">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                指定时间<span class="caret"></span>
                            </button>
                            <ul id="select1" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li><a href="javascript:void(0)" onclick="nowTime(1);">立即结束</a></li>
                                <li><a href="javascript:void(0)" onclick="specifiedTime(1);">指定时间</a></li>
                            </ul>
                        </div>
                        <div class="input-group">
                            <input type="text" name="date" id="date1" placeholder="请选择时间" class="form-control form-datetime" >
                        </div>
                    </div>
                </c:if>
                <div class="mg-row">
                    <span class="block-title">报价开始时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                        <ul id="select2" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                            <li><a href="javascript:void(0)" onclick="nowTime(2);">立即开始</a></li>
                            <li><a href="javascript:void(0)" onclick="specifiedTime(2);">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date2" placeholder="请选择时间" class="form-control form-datetime" >
                    </div>
                </div>
                <div class="mg-row">
                    <span class="block-title">报价截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                        <ul id="select3" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                            <li><a href="javascript:void(0)" onclick="nowTime(3);">立即结束</a></li>
                            <li><a href="javascript:void(0)" onclick="specifiedTime(3);">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date3" placeholder="请选择时间" class="form-control form-datetime" >
                    </div>
                </div>
                <div><span class="required"></span></div>
                <label class="col-md-2" style="line-height: 27px">调整原因：</label>
                <div class="">
                    <textarea class="form-control" name="date" rows="6" placeholder="请填写修改原因" id="date4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateTime()">保存</button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="myModa1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>询价单号</th>
                            <th>询单名称</th>
                            <th>创建人</th>
                            <th>提交时间</th>
                            <th>审批人信息</th>
                            <th>审批类型</th>
                            <th>审批时间</th>
                            <th>状态</th>
                            <th>审批等级</th>
                            <th>审批理由</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td>同意</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:;">确定</a> </div>
    </div>
</div>

<!-- ///////////////////////追加供应商/////////////// -->
<div class="modal fade"  id="supplistModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">追加供应商</h4>
            </div>
            <div class="modal-body">
                    <form id="searchForm1" method="post" onsubmit="return false;">
                        <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">

                            <div class="so-form-20 mr12" style="width:15%;">
                                <input class="form-control col-lg-8" placeholder="供应商名称" name="supplierName" id="supplierName"   type="text">
                            </div>

                            <div class="so-form-20 mr12" style="width:15%;">
                                <select class="form-control" id="select" name="deviceDealerGrade">
                                    <option value="">供应商等级</option>
                                    <option value="已认证">已认证</option>
                                    <option value="未认证">未认证</option>
                                </select>
                            </div>
                            <div class="so-form-20 mr12" style="width:20%;">
                                <input class="form-control" placeholder="所属物料类目"  type="text">
                            </div>
                            <div class="so-form-20" style="width:20%;">
                                <input class="form-control" placeholder="供应商代码" name="supplierCode" id="supplierCode" type="text">
                            </div>
                            <div class="so-form-20 ml12">
                                <button class="btn btn-primary" type="button" id="btnSearch" ><i class="icon icon-search"></i> 搜索</button>
                                <input class="btn" value="重置" id="resetForm" type="reset">
                            </div>

                        </div>
                    </form>
                    <div class="content">
                        <table class="table table-bordered align-md" id="jqGridsup" ></table>
                        <div class="col-md-12" id="jqGridPagersup"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<button type="button" class="btn btn-primary js-confirm">确定</button>--%>
                <%--<span class="btn btn-warning font12" data-position="center" data-toggle="modal" data-target="#auditModa1">追加供应商</span>--%>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
</div>
<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo">
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="transfer">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
<script src="${pageContext.request.contextPath}/js/base.js?v=${version}"></script>
<script>

    var rType='${vo.type}';//'11'就是已作废,'12'已流标
    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });

    $("#select").change(function(){//供应商等级选择

        var selectVar=$(this).children('option:selected').val();
        if (selectVar == "已认证"){
            getAttData3("#jqGridsup", "#jqGridPagersup");

        }else if (selectVar == "未认证"){
            getAttData2("#jqGridsup", "#jqGridPagersup")
        }else {

        }

    });
</script>
</body>
</html>
