<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>寻源管理列表</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqRequest.js?v=${version}"></script>
    <script src="${resource}/lib/datetimepicker/datetimepicker.js?v=${version}"></script>
</head>

<body>

<c:import url="../common/top.jsp" />

<div class="wrapper">

    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">

            <div class="container">

                <div class="breadcrumbs pore">

                    <ol class="breadcrumb">

                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>

                        <li class="active">寻源管理列表</li>

                    </ol>
                    <div class="abrl" ><a href="${ctx}/show/caigoufangxundancaozuo.html" target='_blank'>新版询比价操作演示</a></div>
                </div>

                <div class="page-content">

                    <div class="row">

                        <div class="col-md-12">

                            <div>

                                <ul id="myTab" class="nav nav-tabs">

                                    <li class="active"><a href="#tab1" data-toggle="tab">全部类型</a></li>

                                    <%--<li><a href="#tab2" data-toggle="tab" data-method="DAC">单一来源</a></li>--%>

                                    <li><a href="#tab3" data-toggle="tab" data-method="RAQ">询比价</a></li>

<%--
                                    <li><a href="#tab4" data-toggle="tab" data-method="DAC">反向竞价</a></li>
--%>

                                    <%--<li><a href="#tab5" data-toggle="tab" data-method="DAC">招投标</a></li>--%>

                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane active  example" id="tab1">

                                        <div class="col-lg-12 ">


                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">

                                            <form  method="post" id="searchFormTab1" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">

                                                <div class="so-form-20 padding1">
                                                    <input name="unifiedRfqNum" id="unifiedRfqNum" type="text" class="form-control" placeholder="询价单号">
                                                </div>

                                                <div class="so-form-20 padding1">
                                                    <input name="title" id="title" type="text" class="form-control" placeholder="询单标题">
                                                </div>

                                                <div class="so-form-20 padding1">
                                                    <input name="recCreatorUsername" id="recCreatorUsername" type="text" class="form-control" placeholder="创建人">
                                                </div>

                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                            class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn ml12" type="reset" value="重置"/></div>

                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                   placeholder="创建时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                            type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                            class="form-control form-datetime timeipt"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                   placeholder="报名时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                       placeholder="报名时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                   placeholder="报价时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                       placeholder="报价时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                </div>

                                            </div>
                                            </form>


                                        </div>

                                    </div>

                                    <div class="tab-pane  example" id="tab2">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">

                                                <ul class="nav navbar-nav">

                                                    <li class="active"><a href="#">全部状态：<strong
                                                            class="red2">(0)</strong></a></li>

                                                    <li><a href="#">草稿<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">拟定中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">招募中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">招募结束<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">已作废<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#">已签合同<strong class="red2">(0)</strong></a></li>

                                                </ul>

                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab2" class="searchFormTab" onsubmit="return false;">

                                            <div class="so-form">

                                                <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                        placeholder="询价单号"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                        placeholder="询单标题"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                        placeholder="创建人"></div>

                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                        class="icon icon-search"></i> 搜索
                                                </button>
                                                    <input class="btn ml12" type="reset" value="重置"/></div>

                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="recCreateTime" id="recCreateTime"
                                                                                   placeholder="创建时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                            type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                            class="form-control form-datetime timeipt"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                   placeholder="报名时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="registrationEndDate"
                                                                                       placeholder="报名时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                   placeholder="报价时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="quotationEndDate"
                                                                                       placeholder="报价时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                </div>

                                            </div>

                                            </form>
                                        </div>

                                    </div>

                                    <div class="tab-pane  example" id="tab3">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">

                                                <ul class="nav navbar-nav">

                                                    <li class="active"><a href="#" data-type="100">全部状态：<strong
                                                            class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="0">草稿<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="102">创建待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="101">待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="3">待报价<strong class="red2">(0)</strong></a></li>

                                                    <%--<li><a href="#" data-type="4">待报名<strong class="red2">(0)</strong></a></li>--%>

                                                    <li><a href="#" data-type="5">报名中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="6">报价中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="7">待开标<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="8">待核价<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="9">结果待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="13">结果待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="11">已作废<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="10">已结束<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="12">已流标<strong class="red2">(0)</strong></a></li>

                                                </ul>


                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab3" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">

                                                <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                        placeholder="询价单号"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                        placeholder="询单标题"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                        placeholder="创建人"></div>

                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                            class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn ml12" type="reset" value="重置"/></div>

                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                   placeholder="创建时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                            type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                            class="form-control form-datetime timeipt"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                   placeholder="报名时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                       placeholder="报名时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                   placeholder="报价时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                       placeholder="报价时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                </div>

                                            </div>
                                            </form>

                                        </div>

                                    </div>


                                    <div class="tab-pane  example" id="tab4">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">

                                                <ul class="nav navbar-nav">

                                                    <li class="active"><a href="#" data-type="100">全部状态：<strong
                                                            class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="0">草稿<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="102">创建待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="101">待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="3">待竞价<strong class="red2">(0)</strong></a></li>

                                                    <%--<li><a href="#" data-type="4">待报名<strong class="red2">(0)</strong></a></li>--%>

                                                    <li><a href="#" data-type="5">报名中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="6">竞价中<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="7">待开标<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="8">待授标<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="9">结果待审批<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="13">结果待发布<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="11">已作废<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="10">已结束<strong class="red2">(0)</strong></a></li>

                                                    <li><a href="#" data-type="12">已流标<strong class="red2">(0)</strong></a></li>

                                                </ul>

                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab4"  class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">

                                                <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                        placeholder="竞价单号"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                        placeholder="竞价标题"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                        placeholder="创建人"></div>

                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                            class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn ml12" type="reset" value="重置"/></div>

                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                   placeholder="创建时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                            type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                            class="form-control form-datetime timeipt"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                   placeholder="报名时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                       placeholder="报名时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                   placeholder="报价时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                       placeholder="报价时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                </div>

                                            </div>
                                            </form>
                                        </div>

                                    </div>

                                    <div class="tab-pane  example" id="tab5">

                                        <div class="col-lg-12 ">

                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">


                                            </div>

                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="row mt12">
                                            <form  method="post" id="searchFormTab5" class="searchFormTab" onsubmit="return false;">
                                            <div class="so-form">

                                                <div class="so-form-20 padding1"><input type="text" name="unifiedRfqNum" class="form-control"
                                                                                        placeholder="询价单号"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="title" class="form-control"
                                                                                        placeholder="询单标题"></div>

                                                <div class="so-form-20 padding1"><input type="text" name="recCreatorUsername" class="form-control"
                                                                                        placeholder="创建人"></div>

                                                <div class="so-form-15">
                                                    <button class="btn btn-primary btnSearch" type="button"><i
                                                            class="icon icon-search"></i> 搜索
                                                    </button>
                                                    <input class="btn ml12" type="reset" value="重置"/></div>

                                                <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="hiderow">
                                                    <div class="so-form-15"><input type="text" name="recCreateTime" id="date"
                                                                                   placeholder="创建时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5" style="margin-right:75px"><input
                                                            type="text" name="recCreateTimeEnd" id="date" placeholder="创建时间(止)"
                                                            class="form-control form-datetime timeipt"></div>
                                                    <div class="so-form-15"><input type="text" name="registrationStartDate" id="date"
                                                                                   placeholder="报名时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="registrationEndDate" id="date"
                                                                                       placeholder="报名时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="so-form-15"><input type="text" name="quotationStartDate" id="date"
                                                                                   placeholder="报价时间(起)"
                                                                                   class="form-control form-datetime timeipt">
                                                    </div>
                                                    <div class="left ml5 mt5">—</div>
                                                    <div class="so-form-15 ml5"><input type="text" name="quotationEndDate" id="date"
                                                                                       placeholder="报价时间(止)"
                                                                                       class="form-control form-datetime timeipt">
                                                    </div>
                                                </div>

                                            </div>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                                <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                                <div class="col-md-12" id="jqGridPager"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

    </div>

</div>

<script>

    $(function () {
//        $('#myTab').on('shown.zui.tab', function(e) {
//            $.placeholder(true,$(".tab-content").children('.active').find(".form-datetime"));
//        })

        $('.more-so').click(function () {

            $(this).parent().siblings('.hiderow').stop().slideToggle('fast');
//  日期
            $(".tab-content").children('.active').find(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });
//            $.placeholder();
        });

        $(".form-date").datetimepicker(
                {

                    language: "zh-CN",

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    minView: 2,

                    forceParse: 0,

                    format: "yyyy-mm-dd"

                });


        $(".form-time").datetimepicker({

            language: "zh-CN",

            weekStart: 1,

            todayBtn: 1,

            autoclose: 1,

            todayHighlight: 1,

            startView: 1,

            minView: 0,

            maxView: 1,

            forceParse: 0,

            format: 'hh:ii'

        });


    });

</script>

</body>

</html>
