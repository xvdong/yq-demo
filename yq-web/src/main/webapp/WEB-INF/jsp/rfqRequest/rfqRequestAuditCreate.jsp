<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>创建待审批</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqRequestAudit.js?v=${version}"></script>
    <script src="${resource}/js/rfqRequest/rfqRequestDetail.js?v=${version}"></script>
</head>
<body>
<c:import url="../common/top.jsp"/>
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container" style="padding-bottom:20px;">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active"><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价单详情</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">询价单详情</c:when></c:choose></li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <div class=" pull-right">
                                <!--<a href="javascript:;"><span class="label mg-btn label-info">审批流程跟踪</span></a>-->
                                <%--<span class="btn btn-danger font12" data-toggle="modal" data-target="#myModa1">打印询价单</span>--%>
                                <%--<span class="btn btn-warning font12" data-toggle="modal" data-target="#myModa15">下载询价单</span>--%>
                            </div>
                        </div>
                        <input type="hidden" id="rfqMethod" name="rfqMethod" value="${rfqRequestComplexVo.requestVo.rfqMethod}"/>
                        <input type="hidden" id="biddingMethod" name="biddingMethod" value="${rfqRequestComplexVo.rulesVo.biddingMethod}"/>
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li style="width:30%;">询单号：${rfqRequestComplexVo.requestVo.unifiedRfqNum}</li>
                                        <li style="width:40%;"><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">竞价标题</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">询价标题</c:when></c:choose>：${rfqRequestComplexVo.requestVo.title}</li>
                                        <li>计划编号：${rfqRequestComplexVo.requestVo.planNo}</li>
                                        <c:choose>
                                            <c:when test="${rfqRequestComplexVo.requestVo.rfqMethod == 'DAC'}">
                                                <li>采购单位：${rfqRequestComplexVo.requestVo.ouName}</li>
                                            </c:when>
                                        </c:choose>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="line-32 mt12 ml12 mr12">
                            <span class="left" style="font-size:16px; display:inline-block">物料信息</span>
                            <c:if test="${ not empty rfqRequestComplexVo.requestVo.totalPrice}">
                                <span class="red right line-32" style=" display:inline-block">预算总价：${rfqRequestComplexVo.requestVo.totalPrice}元</span>
                            </c:if>
                        </div>
                        <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="jqGrid" ></table>
                        <div class="col-md-12" id="jqGridPager"></div>
                    </div>
                    <c:choose>
                        <c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='DAC'}">
                            <div class="panel">
                                <div class="panel-heading clearfix">
                                    <span class="mg-margin-right">竞价规则</span>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                            <div class="clearfix">
                                                <span class="col-xs-5">创建人：${rfqRequestComplexVo.requestVo.recCreatorUsername} </span>
                                                <span class="col-xs-5">创建时间：${rfqRequestComplexVo.requestVo.recCreateTime}</span>
                                            </div>
                                            <div class="mt12 clearfix">
                                                <span class="col-xs-5">发布时间：${rfqRequestComplexVo.requestVo.issueDate}</span>
                                                <span class="col-xs-5">报名截止时间：${rfqRequestComplexVo.rulesVo.registrationEndDate}</span>
                                            </div>
                                            <div class="mt12 clearfix">
                                                <span class="col-xs-5">竞价开始时间：${rfqRequestComplexVo.rulesVo.quotationStartDate}</span>
                                                <span class="col-xs-5">竞价截止时间：${rfqRequestComplexVo.requestVo.quotationEndDate}</span>
                                            </div>
                                            <div class="mt12 clearfix">
                                                <span class="col-xs-5">保证金金额：<c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney=='1'}">
                                                    <span class="red">${rfqRequestComplexVo.rulesVo.assureMoney}元</span>
                                                </c:if>
                                        <c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney!='1'}">
                                            不缴纳保证金
                                        </c:if></span>
                                                <span class="col-xs-5">竞价货币：${rfqRequestComplexVo.requestVo.currency}</span>
                                            </div>
                                            <c:if test="${rfqRequestComplexVo.rulesVo.biddingMethod=='1'}">
                                            <div class="mt12 clearfix">
                                                <span class="col-xs-5">起拍价：<c:if test="${not empty rfqRequestComplexVo.requestVo.startPrice}">
                                            <span class="red"
                                                  id="startPrice">${rfqRequestComplexVo.requestVo.startPrice}元</span>
                                                </c:if>
                                        <c:if test="${empty rfqRequestComplexVo.requestVo.startPrice}">
                                            <span class="red" id="startPrice">无限制</span>
                                        </c:if></span>
                                                <span class="col-xs-5">最小降价幅度：<c:if test="${not empty rfqRequestComplexVo.requestVo.priceGrad}">
                                            <span class="red"
                                                  id="priceGrad">${rfqRequestComplexVo.requestVo.priceGrad}元</span>
                                                </c:if>
                                        <c:if test="${empty rfqRequestComplexVo.requestVo.priceGrad}">
                                            <span class="red" id="startPrice">无限制</span>
                                        </c:if></span>
                                            </div>
                                            </c:if>
                                            <div class="mt12 clearfix">
                                    <span class="col-xs-5">竞价方式：
                                      <c:if test="${rfqRequestComplexVo.rulesVo.biddingMethod=='0'}">
                                          按物料行竞价
                                      </c:if>
                                      <c:if test="${rfqRequestComplexVo.rulesVo.biddingMethod=='1'}">
                                          按总价竞价
                                      </c:if>
                                      <c:if test="${rfqRequestComplexVo.rulesVo.viewPriceBillboardFlag=='1'}">
                                          &nbsp; 公开竞价排名
                                      </c:if>
                                      <c:if test="${rfqRequestComplexVo.rulesVo.viewLowerestPriceFlag=='1'}">
                                          &nbsp; 公开竞价价格
                                      </c:if>
                                    </span>
                                    <span class="col-xs-5">竞价公开程度：
                                      <c:if test="${rfqRequestComplexVo.rulesVo.isPublicOnlineMarket=='1'}">
                                          公开竞价公告
                                      </c:if>
                                      <c:if test="${rfqRequestComplexVo.rulesVo.isPublicPub=='1'}">
                                          &nbsp; 公开中标供应商
                                      </c:if>
                                     <c:if test="${rfqRequestComplexVo.rulesVo.isPublicBidMoney=='1'}">
                                         &nbsp; 公开中标价格
                                     </c:if>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
                    <div class="panel mt20">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">供应商设置</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                    <div class="clearfix">
                                        <label class="bler blu-8">邀请范围：</label>
                                        <span class="bler blu-85">
                                             <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                                  公开询价（面向平台注册供应商公开寻源，供应商需先报名，待采购组织审核通过后再报价。）
                                             </c:if>
                                            <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='0'}">
                                                  定向询价 (将询价单信息向特邀供应商公布，供应商无须报名可直接报价。)
                                            </c:if>
                                        </span>
                                    </div>
                                    <c:if test="${rfqRequestComplexVo.rulesVo.publicBiddingFlag=='1'}">
                                        <div class="clearfix">
                                            <label class="bler blu-8">供应商要求：</label>
                                            <span class="bler blu-85">
                                                1、注册资本要求大于${rfqRequestComplexVo.preauditVo.regcapital}万元
                                                <br/>
                                                2、资质要求${rfqRequestComplexVo.preauditVo.qualifications}
                                            </span>
                                        </div>
                                        <div class="clearfix">
                                            <label class="bler blu-8">报名要求：</label>
                                            <span class="bler blu-85">
                                                    ${rfqRequestComplexVo.preauditVo.requirementDesc}
                                            </span>
                                        </div>
                                    </c:if>
                                <div>
                                    <span class="left line-32" style="font-weight:bold; color:#333;">附件清单：</span>
                                </div>
                                <table class="table table-bordered align-md fixed" style="margin-bottom:5px; width:96%;">
                                    <thead>
                                    <tr>
                                        <th width="50px">序号</th>
                                        <th>文件名称</th>
                                        <th width="100px">文件大小</th>
                                        <th>说明</th>
                                        <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><th width="100px">操作</th><%--</p:permission>--%>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:set var="sum" value="1"/>
                                    <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}">
                                        <c:if test="${item.type == 'P'}">
                                        <tr data-clone="orginal">
                                            <td data-order="true">${sum}</td>
                                            <td>${item.originalFilename}</td>
                                            <td>${item.fileSize}KB</td>
                                            <td>${item.fileDeclaration}</td>
                                            <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                        </tr>
                                            <c:set var="sum" value="${sum + 1}"/>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="red" style="font-size:12px;">注：附件类型不支持exe格式，文件大小不超过100M。              </div>
                                <div>
                                    <span class="left line-32" style="font-weight:bold; color:#333;">供应商信息：</span>
                                    <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                         <span class="bler" style="color:#666;">特邀供应商仍需走报名审核流程。</span>
                                    </c:if>
                                </div>
                                <%--<table class="table table-bordered table-hove  align-md" style="margin-bottom:5px; width:96%;">--%>
                                    <%--<thead>--%>
                                    <%--<tr>--%>
                                        <%--<th width="50px">序号</th>--%>
                                        <%--<th>供应商U代码</th>--%>
                                        <%--<th>特邀供应商名称</th>--%>
                                        <%--<th>联系人</th>--%>
                                        <%--<th>联系电话</th>--%>
                                        <%--<th>微信状态</th>--%>
                                    <%--</tr>--%>
                                    <%--</thead>--%>
                                    <%--<tbody>--%>
                                    <%--<c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">--%>
                                        <%--<c:forEach var="supplierList" items="${rfqRequestComplexVo.supplierList}">--%>
                                            <%--<tr>--%>
                                                <%--<td>${supplierList.seq}</td>--%>
                                                <%--<td>${supplierList.supplierCode}</td>--%>
                                                <%--<td>${supplierList.supplierName}</td>--%>
                                                <%--<td>${supplierList.linkmanName}</td>--%>
                                                <%--<td>${supplierList.linkmanTelphone}</td>--%>
                                                <%--<td>${supplierList.isBindWechat}</td>--%>
                                            <%--</tr>--%>
                                        <%--</c:forEach>--%>
                                    <%--</c:if>--%>
                                    <%--<c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">--%>
                                        <%--<c:forEach var="preauditSupplier" items="${rfqRequestComplexVo.preauditSupplierList}" varStatus="status">--%>
                                            <%--<tr>--%>
                                                <%--<td>${status.index+1}</td>--%>
                                                <%--<td>${preauditSupplier.supplierCode}</td>--%>
                                                <%--<td>${preauditSupplier.supplierName}</td>--%>
                                                <%--<td>${preauditSupplier.answerUsername}</td>--%>
                                                <%--<td>${preauditSupplier.linkmanTelphone}</td>--%>
                                                <%--<td>${supplierList.isBindWechat}</td>--%>
                                            <%--</tr>--%>
                                        <%--</c:forEach>--%>
                                    <%--</c:if>--%>
                                    <%--</tbody>--%>
                                <%--</table>--%>
                                    <div>
                                        <table class="table table-bordered align-md" style="margin-bottom:5px; width:100%;" id="supplierListJqGrid" ></table>
                                        <div class="col-md-12" id="supplierListjqGridPager"></div>
                                    </div>

                                    <input id="isSupplierRequirement" value="${rfqRequestComplexVo.rulesVo.isSupplierRequirement }" type="hidden">
                                    <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='0'}">
                                        <input id="supplierList" value='${supplierList}' type="hidden">
                                    </c:if>
                                    <c:if test="${rfqRequestComplexVo.rulesVo.isSupplierRequirement=='1'}">
                                        <input id="preauditSupplierList" value='${preauditSupplierList}' type="hidden">
                                    </c:if>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel mt20">
                    <div class="panel-heading clearfix">
                        <span class="mg-margin-right">商务与技术条款</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                <div class="clearfix">
                                    <label class="bler blu-1">商务条款：</label>
                                    <span class="bler blu-88-nobg"> ${rfqRequestComplexVo.requestVo.requestBusiTerms}</span>
                                </div>
                                <div class="clearfix">
                                    <label class="bler blu-1">技术条款：</label>
                                    <span class="bler blu-88-nobg">${rfqRequestComplexVo.requestVo.requestTecTerms}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${rfqRequestComplexVo.requestVo.rfqMethod=='RAQ'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">询价规则</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re" style="margin-left:25px; color:#666; font-size:12px;">
                                        <div class="clearfix">
                                            <span class="col-xs-5">创建人：${rfqRequestComplexVo.requestVo.recCreatorUsername} </span>
                                            <span class="col-xs-5">创建时间：${rfqRequestComplexVo.requestVo.recCreateTime}</span>
                                        </div>
                                        <div class="mt12 clearfix">
                                            <span class="col-xs-5">发布时间：${rfqRequestComplexVo.requestVo.issueDate}</span>
                                            <span class="col-xs-5">报名截止时间：${rfqRequestComplexVo.rulesVo.registrationEndDate}</span>
                                        </div>
                                        <div class="mt12 clearfix">
                                            <span class="col-xs-5">报价开始时间：${rfqRequestComplexVo.rulesVo.quotationStartDate}</span>
                                            <span class="col-xs-5">报价截止时间：${rfqRequestComplexVo.requestVo.quotationEndDate}</span>
                                        </div>
                                        <div class="mt12 clearfix">
                                            <span class="col-xs-5">
                                                <c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney=='1'}">
                                                    保证金金额：${rfqRequestComplexVo.rulesVo.assureMoney}元
                                                </c:if>
                                                <c:if test="${rfqRequestComplexVo.rulesVo.needAssureMoney!='1'}">
                                                    不缴纳保证金
                                                </c:if>
                                            </span>
                                            <span class="col-xs-5">报价货币：${rfqRequestComplexVo.requestVo.currency}</span>
                                        </div>
                                        <div class="mt12 clearfix">
                                    <span class="col-xs-5">分项分量报价：
                                      <c:if test="${rfqRequestComplexVo.rulesVo.partialProductFlag=='1'}">
                                          允许对部分产品报价
                                      </c:if>
                                      <c:if test="${rfqRequestComplexVo.rulesVo.partialQuantityFlag=='1'}">
                                          &nbsp; 允许对部分数量报
                                      </c:if>
                                    </span>
                                    <span class="col-xs-5">询价公开程度：
                                      <c:if test="${rfqRequestComplexVo.rulesVo.isPublicOnlineMarket=='1'}">
                                          公开询价公告
                                      </c:if>
                                      <c:if test="${rfqRequestComplexVo.rulesVo.isPublicPub=='1'}">
                                          &nbsp; 公开中标供应商
                                      </c:if>
                                     <c:if test="${rfqRequestComplexVo.rulesVo.isPublicBidMoney=='1'}">
                                         &nbsp; 公开中标价格
                                     </c:if>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:when>
                </c:choose>
                <div class="panel mt20">
                    <div class="panel-heading clearfix">
                        <span class="mg-margin-right">角色设置</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-bordered align-md">
                                <thead>
                                <tr>
                                    <th width="50px">序号</th>
                                    <th>工号</th>
                                    <th>姓名</th>
                                    <th>部门</th>
                                    <th>职位</th>
                                    <th>查看</th>
                                    <th>操作</th>
                                    <th>财务管理</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${rfqRequestComplexVo.projectUserList}">
                                    <tr data-clone="orginal">
                                        <td>${item.seq}</td>
                                        <td>${item.jobCode}</td>
                                        <td>${item.userName}</td>
                                        <td>${item.userOffice}</td>
                                        <td>${item.userTitle}</td>
                                        <td>
                                            <input <c:if test="${fn:contains(item.codeList,'1' )}">checked</c:if> type="checkbox" disabled="disabled">
                                        </td>
                                        <td>
                                            <input <c:if test="${fn:contains(item.codeList,'2' )}">checked</c:if> type="checkbox" disabled="disabled">
                                        </td>
                                        <td>
                                            <input <c:if test="${fn:contains(item.codeList,'3' )}">checked</c:if> type="checkbox" disabled="disabled">
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel mt20">
                    <div class="panel-heading clearfix">
                        <span class="mg-margin-right">联系方式</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="mg-pacne clearfix" style="margin:0 15px;">
                                <ul>
                                    <li>联系人：${rfqRequestComplexVo.contactsVo.linkmanName}</li>
                                    <li>联系电话：${rfqRequestComplexVo.contactsVo.linkmanTelphone}</li>
                                    <li style="width:40%">交货地址：
                                        ${rfqRequestComplexVo.contactsVo.deliveryProvince}
                                        ${rfqRequestComplexVo.contactsVo.deliveryCity}
                                        ${rfqRequestComplexVo.contactsVo.deliveryArea}
                                        ${rfqRequestComplexVo.contactsVo.deliveryAddress}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel mt20">
                    <div class="panel-heading clearfix">
                        <span class="mg-margin-right"><c:choose><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod}=='DAC'">竞价单附件</c:when><c:when test="${rfqRequestComplexVo.requestVo.rfqMethod}=='RAQ'">询单附件</c:when></c:choose></span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-bordered align-md" style="margin-bottom:5px;">
                                <thead>
                                <tr>
                                    <th width="50px">序号</th>
                                    <th>文件名称</th>
                                    <th width="100px">文件大小</th>
                                    <th>说明</th>
                                    <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><th width="100px">操作</th><%--</p:permission>--%>
                                </tr>
                                </thead>
                                <tbody>
                                <c:set var="sum" value="1"/>
                                <c:forEach var="item" items="${rfqRequestComplexVo.attachmentsList}">
                                    <c:if test="${item.type == 'R'}">
                                    <tr data-clone="orginal">
                                        <td data-order="true">${sum}</td>
                                        <td>${item.originalFilename}</td>
                                        <td>${item.fileSize}KB</td>
                                        <td>${item.fileDeclaration}</td>
                                        <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%><td><a href="${downloadAction}?id=${item.attachmentId}"><i class="icon icon-download-alt green "></i>&nbsp;下载</a></td><%--</p:permission>--%>
                                    </tr>
                                        <c:set var="sum" value="${sum + 1}"/>
                                    </c:if>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="red line-32" style="font-size:12px;">
                        注：附件类型不支持exe格式，文件大小不超过100M。
                    </div>
                </div>
                <div class="panel mt20">
                    <div class="panel-heading clearfix">
                        <span class="mg-margin-right">询单备注</span>
                    </div>
                    <div class="panel-body">
                        <div class="mg-pacne clearfix" style="margin:0 15px;">
                            ${rfqRequestComplexVo.requestVo.memo}
                        </div>
                    </div>
                </div>
                <%--审核部分--%>
                <form method="post" id="signAuditForm" action="signAudit" onsubmit="return false;">
                    <%--<input type="hidden" id="gaId" name="gaId" value="${gaId}" />--%>
                    <input type="hidden" name="requestId" value="${requestId}" />
                    <input type="hidden" name="auditType" value="${auditType}" />
                    <input type="hidden" name="eventNumber" value="${eventNumber}" />
                    <%--<div class="form-group font-16px ">
                        审批意见<i class="r">*</i>：
                        <div class="mt12">
                            <textarea id="auditingDesc" name="auditingDesc" maxlength="2000" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;"></textarea>
                        </div>
                    </div>--%>
                    <div class="text-right">
                        <%--<p:permission  privilege="2" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                        <c:if test="${procDefType == 2}">
                            <a href="javascript:;" class="btn btn-md btn-primary" data-toggle="modal" data-target="#myRole" type="button" data-type="S" id="liu">流转</a>
                        </c:if>
                        <a href="javascript:submitAudit('1');/*subAudit('1');*/" class="btn btn-md btn-primary" type="button">审批通过</a>
                        <a href="javascript:submitAudit('0');/*subAudit('0');*/" class="btn btn-md btn-warning" type="button">审批驳回</a>
                       <%-- </p:permission>--%>
                        <%--<a href="javascript:subAudit('2');" class="btn btn-md btn-warning" type="button">审批失败</a>--%>
                        <%--<p:permission  privilege="1" requestNo="${rfqRequestComplexVo.requestVo.unifiedRfqNum}">--%>
                        <%--<a href="${pageContext.request.contextPath}/rfqGeneralAuditingHistory/init?objectBillId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-md btn-primary" type="button" target="_blank">审批纪录</a>--%>
                        <a href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${rfqRequestComplexVo.requestVo.id}" class="btn btn-md btn-primary" type="button" target="_blank">审批纪录</a>
                        <%--</p:permission>--%>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="requestId" value="${rfqRequestComplexVo.requestVo.id}" />
</div>
<div class="modal fade" id="myModa2">
    <div class="modal-dialog " style="margin-top: 210.667px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批通过</h4>
            </div>
            <div  class="modal-body" style="overflow: hidden;">
                <div class="form-group font-16px ">
                    <i class="r">*</i>审批意见
                    <div class="mt12">
                        <textarea id="auditingDesc" name="" maxlength="2000" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">确定</button>
                <button type="button" class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- ///////////////////////设置人员/////////////// -->
<div class="modal fade"  id="myRole">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批人设置</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="procinstId" value="${procinstId}" />
                <form>
                    <div class="so-form clearfix" style="margin:5px 0px 10px 0px;">
                        <div class="so-form-8 text-right">员工U代码： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userLoginNo">
                        </div>
                        <div class="so-form-8 ml12 text-right">员工名称： </div>
                        <div class="so-form-15">
                            <input class="form-control" placeholder="" type="text" name="userName">
                        </div>
                        <div class="so-form-15 ml12" style="width: 22%">
                            <button class="btn btn-primary" type="button" id="userSearch"><i class="icon icon-search"></i> 搜索</button>
                            <input class="btn" value="重置" type="reset">
                        </div>
                    </div>
                </form>
                <div class="content">
                    <table class="" id="jqGridRole">
                        <div class="col-md-12" id="jqGridPagerRole"></div>
                    </table>
                </div>
                <textarea  id="contentSimple" name="content" class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;" placeholder="请输入审批意见"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="transfer">确定</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </div>
    </div>
</div>
<!-- 结束 -->
</body>
<script>

    var $myModa2 = $('#myModa2');
    $myModa2.on('click','.btn-primary',function() {
        var auditstatus = $myModa2.data("_auditstatus");
        if(auditstatus=='1'){
            subAudit('1');
        }else if(auditstatus=='0'){
            subAudit('0');
        }
    });
    function submitAudit(auditstatus) {
        $myModa2.data("_auditstatus",auditstatus);

            if(auditstatus=='1'){
                $('#auditingDesc').val('同意');
                $myModa2.find('.modal-title').html('审批通过');
            }else if(auditstatus=='0'){
                $('#auditingDesc').val('请调整');
                $myModa2.find('.modal-title').html('审批驳回');
            }


        $myModa2.modal('show');
//    $myModa2.modal.show();
    }

    $("#myRole").on('show.zui.modal', function () {
        $('#contentSimple').val("");//打开的时候清除流转里面的记录
    });
    $("#myRole").on('hide.zui.modal', function () {
        $("#myRole").data('_type',"");//关闭时将标记置空
    });

    $("#liu").on('click', function () {
        $("#myRole").data('_type',$(this).data('type'));//创建审批页面流转标记一个类型
    });
</script>
</html>