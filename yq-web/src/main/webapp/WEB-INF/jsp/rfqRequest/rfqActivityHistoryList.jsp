<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>审批历史页面</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>
    <script src="${resource}/js/rfqRequest/rfqActivityHistory.js?v=${version}"></script>
</head>

<body>


<c:import url="../common/top.jsp" />


<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">查看审批记录</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <input type="hidden" id="requestId" value="${requestId}" />
                                <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                                <div class="col-md-12" id="jqGridPager"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--审批流程详情--%>
<div class="modal fade"  id="nodeView">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">流程详情</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <%--<table class="" id="nodeViewTable">

                    </table>--%>
                        <style>#gbox_jqNodeViewGrid{width: 568px !important;}</style>
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <table class="table table-bordered table-hove align-md" id="jqNodeViewGrid"></table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="transfer" data-dismiss="modal">确定</button>
        </div>
    </div>
</div>

<script>
    $(function(){

        //日期
        $(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });

        $(".form-date").datetimepicker(
                {
                    language:  "zh-CN",
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

        $(".form-time").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            format: 'hh:ii'
        });

    });
</script>
</body>

</html>
