<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp"%>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>欧冶采购成交通知单</title>
    <style type="text/css">
        body{padding-bottom:20px;background:#fff; color:#333; font-family:SimSun;}
        ul,li{list-style:none; margin:0; padding:0;}
        .v-align{vertical-align:middle !important;}
        .container {max-width:1000px;margin-top:12px;margin:0 auto;}
        .container_main{padding:0 0 10px 0; margin-top:0;}
        h1{
            text-align: center; margin-bottom: 50px; color: #666;
        }
        .rightbar{ height:100%; }
        .table.align-md td,.table.align-md th{ text-align:center; vertical-align:middle;}
        .mg-a-text{color:#ea644a;margin-right:8px}
        .mg-margin-right{margin-right:40px;font-size:16px}
        .mg-header{line-height:16px;position:relative;font-size:16px;padding-bottom:10px;margin-bottom:15px;clear:both}

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border:1px solid #666;

        }
        .table  th ,td{
            border:1px solid #666;
            line-height:46px;
            text-align: center;
            font-size:12px;
            color: #333;
            padding:6px 0 ;
            border-spacing:0;
            vertical-align:middle;
        }
        .table tr td:first-child{
            border-left: 1px solid #666;
        }
        .table  th{
            border-top:1px solid #666;
        }
        .table  th:first-child{
            border-left: 1px solid #666;
        }
        .table1{
            border:1px solid #666;
        }
        .table1 th,.table1 td{
            height:46px; text-align: left; border:0;
        }
        .table1 th {
            background-color: #f5f5f5;
        }
        td label{
            color: #333;
            font-weight: bold;
        }
        .panel .table1 th,.panel .table1 td{
            height: 16px;
            line-height: 16px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="container container_main">
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">
                    <h1>欧冶采购成交通知单</h1>
                    <div class="panel">
                        <table class="table table1">
                            <tr>
                                <th colspan="3" style="padding-left: 10px;">基本情况</th>
                            </tr>

                            <tbody>
                            <tr>
                                <td style="padding-left: 10px;">采购组织：${ouName2}</td>
                                <td>计划单号：${planNo2}</td>
                                <td>币种：${currency2}</td>
                            </tr>
                            <tr>
                                <td style="padding-left: 10px;">询价单号:${ouRfqNum2}</td>
                                <td>开始日期:${startDate}</td>
                                <td>截止日期:${quotationEndDate}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="mg-header">
                        物料明细：
                    </div>
                    <div class="tab-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>物料代码</th>
                                <th>物料名称</th>
                                <th>型号规格</th>
                                <th>生产厂家</th>
                                <th>采购数量</th>
                                <th>计量单位</th>
                                <th>交货期</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${thingsList2}" var="item" varStatus="status">
                                <tr>

                                    <td>${item.materialNo}</td>
                                    <td>${item.materialName}</td>
                                    <td>${item.character}</td>
                                    <td>${item.producer}</td>
                                    <td>${item.requestAmount}</td>
                                    <td>${item.unit}</td>
                                    <td>${item.requestDeliveryDate}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="mg-header">
                        报价情况：
                    </div>
                    <div class="tab-content">
                        <table class="table table-bordered align-md">
                            <thead>
                            <tr>
                                <th>报价单位</th>
                                <th>报价时间</th>
                                <th>报价次数</th>
                                <th style="line-height:18px;padding:3px 0">有效报价总价</th>
                            </tr>
                            </thead>
                            <tbody>

                            <c:forEach items="${rfqHtmlQuotationDetailPO2List}" var="item" varStatus="status">
                                <tr>
                                    <td>${item.supplierName}</td>
                                    <td>${item.quotationDate}</td>
                                    <td>${item.quotationTimes}</td>
                                    <td>${item.total}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>

                    <div class="mg-header" style="border-bottom: none">
                        中标情况：
                    </div>
                    <div class="tab-content">
                        <table class="table table-bordered align-md">
                            <thead>
                            <tr>
                                <th colspan="7" style="text-align: left; padding-left: 10px;">中标情况如下:</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${winSupplier2}" var="item" varStatus="status">

                                <tr>
                                    <td>中标单位:${item.supplierName}</td>
                                    <td>中标总价:${item.allMoney}</td>
                                    <td colspan="5">报价备注:${item.quotationMemo}</td>
                                </tr>
                                <tr>
                                    <td>物料代码</td>
                                    <td>物料名称</td>
                                    <td>型号和规格</td>
                                    <td>拟签数量</td>
                                    <c:if test="${priceType == '1'}">
                                        <td>拟签含税价格</td>
                                    </c:if>
                                    <c:if test="${priceType == '0'}">
                                        <td>拟签未税价格</td>
                                    </c:if>
                                    <c:if test="${priceType == 'N'}">
                                        <td>拟签价格</td>
                                    </c:if>
                                    <td>代替型规</td>
                                    <td>物料备注</td>
                                </tr>

                                <c:set var="supplierCode" value="${item.supplierCode}"/>
                                <c:forEach items="${thirdList2}" var="item2" varStatus="status">
                                    <c:if test="${item2.supplierNum == supplierCode}"   >
                                        <tr>
                                            <td>${item2.materialNo}</td>
                                            <td>${item2.materialName}</td>
                                            <td>${item2.character}</td>
                                            <td>${item2.confirmedAmout}</td>
                                            <td>${item2.wantToOnePrice}</td>
                                            <td>${item2.substitutableSpec}</td>
                                            <td>${item2.quotationMemo}</td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>


                    <div class="panel mt24">
                        <table class="table">
                            <tr>
                                <th colspan="3" style="padding-left: 10px; background-color: #f5f5f5; text-align: left">核价说明</th>
                            </tr>

                            <tbody>
                            <tr>
                                <td colspan="3" style="padding:10px; height:52px;line-height: 24px; text-align: left; border-right: 1">
                                    ${resultMemo2}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <%--<div class="mg-header">--%>
                        <%--中标说明--%>
                    <%--</div>--%>
                    <%--<div class="tab-content">--%>
                        <%--<table class="table table-bordered align-md">--%>
                            <%--<thead>--%>
                            <%--<tr>--%>
                                <%--<th>中标单位</th>--%>
                                <%--<th>中标说明</th>--%>
                            <%--</tr>--%>
                            <%--</thead>--%>
                            <%--<tbody>--%>

                            <%--<c:forEach items="${winSupplier2}" var="item" varStatus="status">--%>
                                <%--<tr>--%>
                                    <%--<td>${item.supplierName}</td>--%>
                                    <%--<td>${item.memoDesc}</td>--%>
                                <%--</tr>--%>
                            <%--</c:forEach>--%>
                            <%--</tbody>--%>
                        <%--</table>--%>
                    <%--</div>--%>


                    <div class="tab-content">
                        <div class="mg-header">
                            审批意见：
                        </div>
                        <div class="tab-content">
                            <table class="table table-bordered align-md">
                                <thead>
                                <tr>
                                    <th>审批类型</th>
                                    <th>审批人</th>
                                    <th>审批意见</th>
                                    <th>审批时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${auditList}" var="item3" varStatus="status">
                                        <tr>
                                            <td>询报价结果审批</td>
                                            <td>${item3.auditorName}</td>
                                            <td>${item3.auditAdvice}</td>
                                            <td>${fn:split(item3.auditTime," ")[0]}</td>
                                        </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>

                        <div  style="padding-bottom: 20px; float:right;color:#666"><strong>日期：${date2}</strong></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
