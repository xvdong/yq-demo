<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>设置登录用户</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>

<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="backurl" value="${pageContext.request.getParameter("backurl")}">

<div>
    用户账号:<input id="usrAccount" type="TEXT" MAXLENGTH="20"><br/><br/>
    用户全名:<input id="usrFullName" type="TEXT" MAXLENGTH="20"><br/><br/>
    权限类型：<input id="loginOrgType" type="TEXT" MAXLENGTH="20">采购角色:"1",供应商角色:"2",平台管理员:"3"<br/><br/>
    登陆类型:<input id="type" type="TEXT" MAXLENGTH="20">采购角色:"100",供应商角色:"010",平台管理员:"011"<br/><br/>
    用户权限id:<input id="usrPerId" type="TEXT" MAXLENGTH="20"><br/><br/>
    用户id:<input id="usrId" type="TEXT" MAXLENGTH="20"><br/><br/>
    组织机构id:<input id="usrOrgId" type="TEXT" MAXLENGTH="20"><br/><br/>
    组织名称:<input id="orgName" type="TEXT" MAXLENGTH="20"><br/><br/>
    组织机构代码:<input id="supplierCode" type="TEXT" MAXLENGTH="20"><br/><br/>
    注册资本:<input id="regcapital" type="TEXT" MAXLENGTH="20"><br/><br/>
    拥有资质:<input id="qualifications" type="TEXT" MAXLENGTH="20"><br/><br/>
    角色:<input id="roles" type="TEXT" MAXLENGTH="20"><br/><br/>
    ouId:<input id="ouId" type="TEXT" MAXLENGTH="20"><br/><br/>
    市场标识：<input type="text" id="domainId" value="20">
</div>
<button id="setSysUserInfoBtn">设置登录信息</button><br/>
当前用户:<pre id="userName"></pre>
<script type="application/javascript">
    var vUserObject={};
    $(document).ready(function(){
        $.ajax({
            url: $("#contextPath").val()+"/sysUser/getCurSysUserVo",
            dataType: "json",
            success: function(data){
                vUserObject=data;
                $("#userName").text(vUserObject.usrFullName);
                $("#usrAccount").val(vUserObject.usrAccount);
                $("#usrFullName").val(vUserObject.usrFullName);
                $("#type").val(vUserObject.type);
                $("#usrPerId").val(vUserObject.usrPerId);
                $("#usrId").val(vUserObject.usrId);
                $("#usrOrgId").val(vUserObject.usrOrgId);
                $("#orgName").val(vUserObject.orgName);
                $("#supplierCode").val(vUserObject.supplierCode);
                $("#regcapital").val(vUserObject.regcapital);
                $("#qualifications").val(vUserObject.qualifications);
                $("#roles").val(vUserObject.roles);
                $("#ouId").val(vUserObject.ouId);
                $("#domainId").val(vUserObject.domainId);
                $("#loginOrgType").val(vUserObject.loginOrgType);
            }});
        $("#setSysUserInfoBtn").bind("click",function(){
            vUserObject.usrAccount=$("#usrAccount").val();
            vUserObject.usrFullName=$("#usrFullName").val();
            vUserObject.type=$("#type").val();
            vUserObject.usrPerId=$("#usrPerId").val();
            vUserObject.usrId=$("#usrId").val();
            vUserObject.usrOrgId=$("#usrOrgId").val();
            vUserObject.orgName=$("#orgName").val();
            vUserObject.supplierCode=$("#supplierCode").val();
            vUserObject.regcapital=$("#regcapital").val();
            vUserObject.qualifications=$("#qualifications").val();
            vUserObject.roles=$("#roles").val();
            vUserObject.ouId=$("#ouId").val();
            vUserObject.domainId=$("#domainId").val();
            vUserObject.loginOrgType=$("#loginOrgType").val();
            //console.log(vUserObject);
            $.ajax({
                url: $("#contextPath").val()+"/sysUser/setCurSysUserVo",
                dataType: "json",
                data:vUserObject,
                success: function(data){
                    vUserObject=data;
                    $("#userName").text(vUserObject.usrFullName);
                    if($("#backurl").val()!=""){
                        window.location.href=$("#backurl").val();
                    }
                }});
        });
    });

</script>
</body>
</html>