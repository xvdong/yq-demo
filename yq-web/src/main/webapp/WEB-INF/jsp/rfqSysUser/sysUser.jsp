<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>设置登录用户</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>

<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="backurl" value="${pageContext.request.getParameter("backurl")}">
<select id="user">
<optgroup label="采购角色">
    <option value="a_1" type="100" loginOrgType="1" usrPerId="1" usrOrgId="1" orgName="公司1" supplierCode="48325892375925" ouId="0000001" regcapital="100000.00" qualifications="ISO90001认证" roles="1,2" domainId="20">测试帐号1(角色1，2)</option>
    <option value="a_2" type="100" loginOrgType="1" usrPerId="2" usrOrgId="1" orgName="公司1" supplierCode="48325892375925" ouId="0000002" regcapital="100000.00" qualifications="ISO90001认证" roles="1" domainId="20">测试帐号2(角色1)</option>
    <option value="a_3" type="100" loginOrgType="1" usrPerId="3" usrOrgId="1" orgName="公司1" supplierCode="48325892375925" ouId="0000003" regcapital="100000.00" qualifications="ISO90001认证" roles="2" domainId="20">测试帐号3(角色2)</option>
    <option value="a_4" type="100" loginOrgType="1" usrPerId="4" usrOrgId="1" orgName="公司1" supplierCode="48325892375925" ouId="0000004" regcapital="100000.00" qualifications="ISO90001认证" roles="3,4" domainId="20">测试帐号4(角色3,4)</option>
    <option value="a_5" type="100" loginOrgType="1" usrPerId="5" usrOrgId="1" orgName="公司1" supplierCode="48325892375925" ouId="0000005" regcapital="100000.00" qualifications="ISO90001认证" roles="4" domainId="20">测试帐号5(角色4)</option>
</optgroup>
<optgroup label="供应商角色">
    <option value="b_1" type="010" loginOrgType="2" usrPerId="6" usrOrgId="6" orgName="公司6" supplierCode="U33166" ouId="0000006" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">供应商6</option>
    <option value="b_2" type="010" loginOrgType="2" usrPerId="7" usrOrgId="7" orgName="公司7" supplierCode="48325892375921" ouId="0000007" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">供应商7</option>
    <option value="b_3" type="010" loginOrgType="2" usrPerId="8" usrOrgId="8" orgName="公司8" supplierCode="48325892375922" ouId="0000008" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">供应商8</option>
    <option value="b_4" type="010" loginOrgType="2" usrPerId="9" usrOrgId="9" orgName="公司9" supplierCode="48325892375923" ouId="0000009" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">供应商9</option>
    <option value="b_5" type="010" loginOrgType="2" usrPerId="10" usrOrgId="10" orgName="公司10" supplierCode="48325892375924" ouId="0000010" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">供应商10</option>
</optgroup>
<optgroup label="平台管理员">
    <option value="c_1" type="011" loginOrgType="3" usrPerId="11" usrOrgId="11" orgName="公司11" supplierCode="48325892375926" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">测试帐号1</option>
    <option value="c_2" type="011" loginOrgType="3" usrPerId="12" usrOrgId="12" orgName="公司12" supplierCode="48325892375927" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">测试帐号2</option>
    <option value="c_3" type="011" loginOrgType="3" usrPerId="13" usrOrgId="13" orgName="公司13" supplierCode="48325892375928" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">测试帐号3</option>
    <option value="c_4" type="011" loginOrgType="3" usrPerId="14" usrOrgId="14" orgName="公司14" supplierCode="48325892375929" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">测试帐号4</option>
    <option value="c_5" type="011" loginOrgType="3" usrPerId="15" usrOrgId="15" orgName="公司15" supplierCode="48325892375930" regcapital="100000.00" qualifications="ISO90001认证" domainId="20">测试帐号5</option>
</optgroup>
</select>
<button id="setSysUserInfoBtn">设置登录信息</button><br/>
当前用户:<pre id="userName"></pre>
<script>
    var vUserObject={};
$(document).ready(function(){
    $.ajax({
        url: $("#contextPath").val()+"/sysUser/getCurSysUserVo",
        dataType: "json",
        success: function(data){
            vUserObject=data;
            $("#userName").text(vUserObject.usrFullName);
        }});
    $("#setSysUserInfoBtn").bind("click",function(){
        var v_opt= $("#user").find("option:selected");
        vUserObject.usrAccount=$(v_opt).val();
        vUserObject.usrFullName=$(v_opt).html();
        vUserObject.type=$(v_opt).attr("type");
        vUserObject.loginOrgType=$(v_opt).attr("loginOrgType");
        vUserObject.usrPerId=$(v_opt).attr("usrPerId");
        vUserObject.usrId=$(v_opt).attr("usrPerId");
        vUserObject.usrOrgId=$(v_opt).attr("usrOrgId");
        vUserObject.orgName=$(v_opt).attr("orgName");
        vUserObject.supplierCode=$(v_opt).attr("supplierCode");
        vUserObject.regcapital=$(v_opt).attr("regcapital");
        vUserObject.qualifications=$(v_opt).attr("qualifications");
        vUserObject.roles=$(v_opt).attr("roles");
        vUserObject.ouId=$(v_opt).attr("ouId");
        vUserObject.domainId=$(v_opt).attr('domainId');
      //  console.log(vUserObject);
        $.ajax({
            url: $("#contextPath").val()+"/sysUser/setCurSysUserVo",
            type:'post',
            dataType: "json",
            data:vUserObject,
            success: function(data){
                vUserObject=data;
                $("#userName").text(vUserObject.usrFullName);
                if($("#backurl").val()!=""){

                    window.location.href=$("#backurl").val();
                    if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0"){
                        window.event.returnValue = false;
                    }
                }

            }});
    });
});

</script>
</body>
</html>
