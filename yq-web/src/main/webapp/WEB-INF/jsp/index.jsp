<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>欢迎使用RFQ开发测试页</title>
    <link href="${pageContext.request.contextPath}/css/zui.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/doc.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/common.css?v=${version}" rel="stylesheet">
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>
<body>

    <h3>寻源管理列表（采购方）：</h3><br/>
        <a href="${pageContext.request.contextPath}/rfqRequest/init?unifiedRfqNum=RZ16072800270">寻源管理列表</a><br/>
        <a href="${pageContext.request.contextPath}/rfqRequest/toEditRfqRequest">发布询单</a><br/>
    <h3>报名功能（供应商）：</h3><br/>
        <a href="${pageContext.request.contextPath}/rfqPreauditSupplier/init">供应商报名列表</a><br/>
    <h3>报价功能（供应商）：</h3><br/>
        <a href="${pageContext.request.contextPath}/rfqQuotation/init">供应商报价列表</a><br/>
    <br/><br/><h3>审批功能（采购审批）：</h3><br/>
        <a href="${pageContext.request.contextPath}/rfqRequest/auditInit">审批人审批列表</a><br/>
    <h3>财务审批（财务审批）：</h3><br/>

   <%-- <h3>用户登录：</h3><br/>
    <a href="${pageContext.request.contextPath}/sysUser/init">设置登录信息</a><br/>--%>
</body>
</html>
