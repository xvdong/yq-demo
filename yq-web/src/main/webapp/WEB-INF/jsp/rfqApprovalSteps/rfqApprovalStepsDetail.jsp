<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RfqApprovalSteps管理</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqApprovalSteps/rfqApprovalSteps.js?v=${version}"></script>
</head>
<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <div class="sidebar clearfix">
            <div class="container">
                <c:import url="../common/menu.jsp">
                    <c:param name="menuNo" value="1" />
                </c:import>
            </div>
        </div>
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading">
                            审批步骤表详情
                        </div>
                        <%--<ul class="list-group">
                            <label class="control-label">审批流编号</label>
                            <input type="text" name='approvalCode' id='approvalCode' class='form-control' value="${rfqApprovalStepsVo.approvalCode}">

                        </ul>--%>
                        <ul class="list-group">
                            <label class="control-label">审批步骤编号</label>
                            <input type="text" name='approvalStepCode' id='approvalStepCode' class='form-control' value="${rfqApprovalStepsVo.approvalStepCode}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">审批步骤名称</label>
                            <input type="text" name='approvalStepName' id='approvalStepName' class='form-control' value="${rfqApprovalStepsVo.approvalStepName}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">创建时间</label>
                            <input type="text" name='createTime' id='createTime' class='form-control' value="${rfqApprovalStepsVo.createTime}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">创建人</label>
                            <input type="text" name='founder' id='founder' class='form-control' value="${rfqApprovalStepsVo.founder}">

                        </ul>
                        <ul class="list-group">
                            <label class="control-label">是否有效(默认无效:0   有效:1)</label>
                            <input type="text" name='isValid' id='isValid' class='form-control' value="${rfqApprovalStepsVo.isValid}">

                        </ul>
                        <%--<ul class="list-group">
                            <label class="control-label">ID</label>
                            <input type="text" name='id' id='id' class='form-control' value="${rfqApprovalStepsVo.id}">

                        </ul>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
