<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>审批步骤表管理</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <script src="${pageContext.request.contextPath}/js/rfqApprovalSteps/rfqApprovalSteps.js?v=${version}"></script>
</head>
<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />
<div class="wrapper">
    <div class="container container_main">
        <div class="sidebar clearfix">
            <div class="container">
                <c:import url="../common/menu.jsp">
                    <c:param name="menuNo" value="1" />
                </c:import>
            </div>
        </div>
        <div class="rightbar clearfix">
            <div class="container">
                <div class="page-content">

                    <div class='example'>
                        <form class="form-horizontal" id="saveApprovalStep" role="form" method='post' action="editRfqApprovalSteps"  onsubmit="return false;">
                            <legend>
                                <c:choose>
                                    <c:when test="${userBo.oper == 'add'}">新增审批步骤</c:when>
                                    <c:otherwise>编辑审批步骤</c:otherwise>
                                </c:choose>
                            </legend>
                            <input type="hidden" id="oper" name="oper" value="${rfqApprovalStepsBo.oper}"/>
                            <input type="hidden" id="id" name="id" value="${rfqApprovalStepsVo.id}"/>
                            <div class="form-group">
                                <%--<label class="col-md-2 control-label">审批流编号</label>--%>
                                <div class="col-md-4">
                                    <input type='hidden' name='approvalCode' id='approvalCode' class='form-control' value="${rfqApprovalStepsVo.approvalCode}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">审批步骤编号</label>
                                <div class="col-md-4">
                                    <input type='text' name='approvalStepCode' id='approvalStepCode' readonly="readonly" class='form-control' value="${rfqApprovalStepsVo.approvalStepCode}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">审批步骤名称</label>
                                <div class="col-md-4">
                                    <input type='text' name='approvalStepName' id='approvalStepName' class='form-control' value="${rfqApprovalStepsVo.approvalStepName}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <input type='submit' id='save' class='btn btn-primary' value='保存' data-loading='稍候...' />
                                    <input type='hidden' name='type' id='type' value='article' />
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
