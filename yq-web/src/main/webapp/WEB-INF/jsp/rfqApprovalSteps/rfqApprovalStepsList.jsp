<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>审批步骤列表</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />

    <script src="${pageContext.request.contextPath}/js/rfqApprovalSteps/rfqApprovalSteps.js?v=${version}"></script>

</head>

<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<c:import url="../common/top.jsp" />

<div class="wrapper">

    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">

            <div class="container">

                <div class="breadcrumbs">

                    <ol class="breadcrumb">

                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>

                        <li class="active">审批流步骤列表</li>

                    </ol>

                </div>

                <div class="page-content">

                    <div class="row">

                        <div class="col-md-12">

                            <div>
                                <div class="panel-body">
                                    <form id="searchForm" method="post" onsubmit="return false;">
                                        <div class="col-md-12">
                                            <div>
                                                <input type="hidden" name="approvalCode" id="approvalCode" value="${bo.getApprovalCode()}" placeholder="审批流编号">
                                            </div>
                                            <div class="col-xs-2">
                                                <input name="approvalStepCode" id="approvalStepCode" type="text" class="form-control" placeholder="审批步骤编号">
                                            </div>
                                            <div class="col-xs-2">
                                                <input name="approvalStepName" id="approvalStepName" type="text" class="form-control" placeholder="审批步骤名称">
                                            </div>
                                            <%--<div class="col-xs-2">
                                                <input name="createTime" id="createTime" type="text" class="form-control" placeholder="创建时间">
                                            </div>--%>
                                            <div class="col-xs-2">
                                                <input name="founder" id="founder" type="text" class="form-control" placeholder="创建人">
                                            </div>
                                            <%--<div class="col-xs-2">
                                                <input name="isValid" id="isValid" type="text" class="form-control" placeholder="是否有效(默认无效:0   有效:1)">
                                            </div>--%>
                                            <div class="so-form-15">
                                                <button class="btn btn-primary btnSearch" type="button" id="btnSearch">
                                                    <i class="icon icon-search"></i> 搜索
                                                </button>
                                                <input class="btn ml12" type="reset" value="重置"/>
                                                <a class="right btn  btn-primary" href="${pageContext.request.contextPath}/rfqApproval/init">&nbsp;返回审批流列表</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-content"></div>

                                <table class="table table-bordered table-hove align-md" id="jqGrid"></table>
                                <div class="col-md-12" id="jqGridPager"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

<!--选择审核角色-->
<div class="modal fade" id="myModal-shr">
    <div class="modal-dialog modal-lg">
        <form id="shrForm" action="addPeople" method="post" onsubmit="return false;">
            <input id="stepId" name="id" type="hidden"/>
            <input id="roleids" name="roleids" type="hidden"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                    <h4 class="modal-title">选择审核角色</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="RolejqGrid"></table>
                            <div id="RolejqGridPager"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">取消</a>
                    <a class="btn btn-primary" href="javascript:addRole();">确定</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $(function () {

        $('.more-so').click(function () {

            $('.hiderow').stop().slideToggle('fast');

        });


        //日期

        $(".form-datetime").datetimepicker(
                {

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    forceParse: 0,

                    showMeridian: 1,

                    format: "yyyy-mm-dd hh:ii"

                });


        $(".form-date").datetimepicker(
                {

                    language: "zh-CN",

                    weekStart: 1,

                    todayBtn: 1,

                    autoclose: 1,

                    todayHighlight: 1,

                    startView: 2,

                    minView: 2,

                    forceParse: 0,

                    format: "yyyy-mm-dd"

                });


        $(".form-time").datetimepicker({

            language: "zh-CN",

            weekStart: 1,

            todayBtn: 1,

            autoclose: 1,

            todayHighlight: 1,

            startView: 1,

            minView: 0,

            maxView: 1,

            forceParse: 0,

            format: 'hh:ii'

        });


    });

</script>

</body>

</html>
