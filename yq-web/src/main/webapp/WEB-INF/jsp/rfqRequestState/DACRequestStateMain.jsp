<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><c:choose><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></title>
    <%@include file="../common/headBase.jsp"%>
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequestState/rfqRequestState.js?v=${version}"></script>
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>

<body>
<c:import url="../common/top.jsp"/>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">

<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active"><c:choose><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right">竞价单摘要</span>
                            <div class=" pull-right">
                                <a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${vo.id}" target="_blank"><span class="btn btn-info font12">竞价单详情</span></a>
                                <c:choose>
                                    <c:when test="${vo.type != '11' && vo.type != '12' }">
                                        <span class="btn btn-danger font12" data-position="center" data-toggle="modal" data-target="#cancelPrice" >撤销竞价</span>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                        <input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}">
                        <input type="hidden" name="quotationRound" id="quotationRound" value="${vo.quotationRound}">
                        <input type="hidden" name="id" id="id" value="${vo.id}">
                        <input type="hidden" name="type" id="type" value="${vo.type}">
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>竞价单号：${vo.unifiedRfqNum}</li>
                                        <li>竞价标题：${vo.title}</li>
                                        <li>采购单位：${vo.ouName}</li>
                                        <li>计划编号：${vo.planNo}</li>
                                        <li>当前状态：<span class="red" id="nowState" ><c:choose><c:when test="${vo.type == '3'}">待报价</c:when><c:when test="${vo.type == '5'}">报名中</c:when><c:when test="${vo.type == '6'}">报价中</c:when><c:when test="${vo.type == '7'}">待开标</c:when><c:when test="${vo.type == '11'}">已作废</c:when><c:when test="${vo.type == '12'}">已流标</c:when></c:choose></span></li>
                                        <li>邀请范围：<span id="publicBiddingFlag"><c:choose><c:when test="${vo.publicBiddingFlag == '1'}">公开寻源</c:when><c:otherwise>定向寻源</c:otherwise></c:choose></span></li>
                                    </ul>
                                    <c:choose>
                                        <c:when test="${vo.type == '3'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbj.png"></span></c:when>
                                        <c:when test="${vo.type == '5'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bmz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bjz.png"></span></c:when>
                                        <c:when test="${vo.type == '7'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dkb.png"></span></c:when>
                                        <c:when test="${vo.type == '11'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/yzf.png"></span></c:when>
                                        <c:when test="${vo.type == '12'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/ylb.png"></span></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span class="red"><c:choose><c:when test="${vo.assureMoney >= '1'}"><fmt:formatNumber type="number" value="${vo.assureMoney} " maxFractionDigits="4"/>元</c:when></c:choose></span></span></li>
                                        <li>报价货币：<c:choose><c:when test="${vo.currency=='CNY'}">人民币CNY</c:when><c:when test="${vo.currency=='USD'}">美元USD</c:when><c:when test="${vo.currency=='GBP'}">英镑GBP</c:when><c:when test="${vo.currency=='JPY'}">日元JPY</c:when><c:when test="${vo.currency=='EUR'}">欧元EUR</c:when><c:when test="${vo.currency=='HKD'}">港元HKD</c:when><c:when test="${vo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                                        <li>预算总价：<span class="red"><fmt:formatNumber type="number" value="${vo.totalBudget}" maxFractionDigits="0"/>元</span></li>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：<c:choose><c:when test="${fn:length(vo.issueDate)>'16'}">${fn:substring(vo.issueDate,0,16)}</c:when> <c:otherwise>${vo.issueDate}</c:otherwise></c:choose></li>
                                        <li>竞价开始时间：<span id ="startDate"><c:choose><c:when test="${fn:length(vo.startDate)>'16'}">${fn:substring(vo.startDate,0,16)}</c:when><c:otherwise>${vo.startDate}</c:otherwise></c:choose></span></li>
                                        <li>竞价截止时间：<span id ="quotationEndDate"><c:choose><c:when test="${fn:length(vo.quotationEndDate)>'16'}">${fn:substring(vo.quotationEndDate,0,16)}</c:when><c:otherwise>${vo.quotationEndDate}</c:otherwise></c:choose></span></li>
                                        <c:choose>
                                            <c:when test="${vo.publicBiddingFlag == '1'}">
                                                <li>报名截止时间：<span id ="registrationEndTime"><c:choose><c:when test="${fn:length(vo.registrationEndTime)>'16'}">${fn:substring(vo.registrationEndTime,0,16)}</c:when><c:otherwise>${vo.registrationEndTime}</c:otherwise></c:choose></span></li>
                                            </c:when>
                                        </c:choose>
                                    </ul>
                                    <c:choose>
                                        <c:when test="${vo.type == '7'}"><a href="javascript:void(0);" onclick="opening()"><span class="btn btn-warning mt12"><i class="icon icon-circle-arrow-right"></i>&nbsp;开标</span></a></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${vo.linkmanName}</li>
                                        <li>联系电话：${vo.linkmanTelphone}</li>
                                        <li style="width:40%;">交货地址：${vo.deliveryProvince}${vo.deliveryCity}${vo.deliveryArea}${vo.deliveryAddress}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:if test="${vo.type == '11'}">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">终止原因：</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">
                                            ${vo.pubEndMemo}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <div class="page-content">
                        <div class="panel">
                            <div class="panel-heading clearfix">
                                <span class="mg-margin-right">供应商情况</span>
                            <span class="pull-right">
                                 <a class="trage text-info" href="${pageContext.request.contextPath}/rfqRequest/listActivityHistory?requestId=${vo.id}" target="_blank" >审批历史</a>
                                <a class="trage text-info" href="${pageContext.request.contextPath}/rfqRequestLog/init?title=${vo.unifiedRfqNum}" target="_blank">操作日志</a>
                            </span>
                            </div>
                            <ul id="myTab" class="nav nav-tabs mt12">
                                <li class="active"><a href="#tab1" data-toggle="tab">报价供应商<span id="b1"></span></a></li>
                                <c:if test="${vo.quotationRound == '0'&& vo.publicBiddingFlag == '1'}">
                                    <li class=""> <a href="#tab2" data-toggle="tab">报名供应商<span id="b2"></span></a></li>
                                </c:if>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane example active"  id="tab1">
                                    <table class="table table-bordered table-hove align-md" id="jqGrid1"></table>
                                </div>
                                <div class="tab-pane example" id="tab2">
                                    <table class="table table-bordered table-hove align-md" id="jqGrid2"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- 即时聊天-->
<%--<a href="#" id="im"></a>--%>



<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">撤销竞价原因</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <p>
                        <span class="required"></span>
                        <span class="pull-left line-32" style="margin-left: 12px">原因类别：</span>

                        <select id="extendField4" class="form-control pull-left" name="" style="width:200px;">
                            <option value="不需要采购">不需要采购</option>
                            <option value="供应商退出">供应商退出</option>
                            <option value="其他原因">其他原因</option>
                        </select>
                    </p>
                    <div class="clearfix"></div>
                    <p>
                        <span class="required"></span><span class="pull-left line-32" style="margin-left: 12px">原因说明：</span>

                        <textarea id="pubEndMemo" class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </p>
                </div>
            </div>
            <div class="clearfix mt12"></div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary"  href="javascript:void(0)" onclick="revoke()">确定</a>
                <a href="javascript:void(0)" class="btn" type="button" data-dismiss="modal">取消</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModa1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">审批历史</h4>
            </div>
            <div class="modal-body">
                <div class="content">
                    <table class="table table-bordered align-md">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>询价单号</th>
                            <th>询单名称</th>
                            <th>创建人</th>
                            <th>提交时间</th>
                            <th>审批人信息</th>
                            <th>审批类型</th>
                            <th>审批时间</th>
                            <th>状态</th>
                            <th>审批等级</th>
                            <th>审批理由</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-02<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td>创建审批</td>
                            <td width="100">2011-01-03<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td>同意</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-04<br> 06:06</td>
                            <td></td>
                            <td>已驳回 </td>
                            <td></td>
                            <td>内容有误</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>687777777</td>
                            <td>计算机竞价采购</td>
                            <td>李四</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td>结果审批</td>
                            <td width="100">2011-01-05<br> 06:06</td>
                            <td></td>
                            <td>已通过 </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer"> <a type="button" class="btn btn-primary"  href="javascript:;">确定</a> </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/base.js?v=${version}"></script>
<script>

    //日期
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    $(".form-date").datetimepicker(
            {
                language:  "zh-CN",
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

    $(".form-time").datetimepicker({
        language:  "zh-CN",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        format: 'hh:ii'
    });
</script>
</body>
</html>
