<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>供应商缴纳审核</title>
    <c:import url="../common/headBase.jsp" />
    <c:import url="../common/jqGridBootstrap.jsp" />
    <!--选择时间和日期-->
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/rfqQuotationFee/rfqFinancialApprovalDetail.js?v=${version}"></script>
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a> 来提升用户体验.</div>
    <![endif]-->
</head>

<body>
<c:import url="../common/top.jsp"/>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">供应商缴纳审核</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="panel">
                        <div class="panel-heading clearfix">
                            <span class="mg-margin-right"><c:choose><c:when test="${vo.rfqMethod=='DAC'}">竞价单摘要</c:when><c:when test="${vo.rfqMethod=='RAQ'}">询价单摘要</c:when></c:choose></span>
                        </div>
                        <input type="hidden" name="unifiedRfqNum" id="unifiedRfqNum" value="${vo.unifiedRfqNum}" />
                        <input type="hidden" name="moneyOrFlagType" id="moneyOrFlagType" value="${vo.moneyOrFlagType}" />
                        <input type="hidden" name="requestId" id="requestId" value="${vo.requestId}" />
                        <input type="hidden" name="type" id="type" value="${vo.type}" />
                        <div class="panel-body font12">
                            <div class="row">
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='DAC'}">竞价单号</c:when><c:when test="${vo.rfqMethod=='RAQ'}">询价单号</c:when></c:choose>：${vo.ouRfqNum}</li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='DAC'}">竞价标题</c:when><c:when test="${vo.rfqMethod=='RAQ'}">询价标题</c:when></c:choose>：${vo.title}</li>
                                        <li>采购单位：${vo.ouName}</li>
                                        <li>计划编号：${vo.planNo}</li>
                                        <li>当前状态：<span class="red" id="nowState" value="${vo.type}" >
                                            <c:choose>
                                                <c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}">待报价</c:when>
                                                <c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}">待竞价</c:when>
                                                <c:when test="${vo.type == '4'}">待报名</c:when>
                                                <c:when test="${vo.type == '5'}">报名中</c:when>
                                                <c:when test="${vo.type == '6'&& vo.rfqMethod=='RAQ'}">报价中</c:when>
                                                <c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}">竞价中</c:when>
                                                <c:when test="${vo.type == '7'}">待开标</c:when>
                                                <c:when test="${vo.type == '8'&& vo.rfqMethod=='RAQ'}">待核价</c:when>
                                                <c:when test="${vo.type == '8'&& vo.rfqMethod=='DAC'}">待授标</c:when>
                                                <c:when test="${vo.type == '9'}">结果待审批</c:when>
                                                <c:when test="${vo.type == '10'}">已结束</c:when>
                                                <c:when test="${vo.type == '11'}">已作废</c:when>
                                                <c:when test="${vo.type == '12'}">已流标</c:when>
                                                <c:when test="${vo.type == '13'}">结果待发布</c:when>
                                                <c:when test="${vo.type == '14'}">多伦报价创建待审批</c:when>
                                                <c:when test="${vo.type == '15'}">多伦报价待发布</c:when>
                                            </c:choose></span>
                                        </li>
                                        <li>邀请范围：<c:choose><c:when test="${vo.publicBiddingFlag == '1'}">公开寻源</c:when><c:when test="${vo.publicBiddingFlag == '0'}">定向寻源</c:when></c:choose></li>
                                    </ul>
                                    <c:choose>
                                        <c:when test="${vo.type == '3'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbj.png"></span></c:when>
                                        <c:when test="${vo.type == '3'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/djj.png"></span></c:when>
                                        <c:when test="${vo.type == '4'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dbm.png"></span></c:when>
                                        <c:when test="${vo.type == '5'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bmz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/bjz.png"></span></c:when>
                                        <c:when test="${vo.type == '6'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/jjz.png"></span></c:when>
                                        <c:when test="${vo.type == '7'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dkb.png"></span></c:when>
                                        <c:when test="${vo.type == '8'&& vo.rfqMethod=='RAQ'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dhj.png"></span></c:when>
                                        <c:when test="${vo.type == '8'&& vo.rfqMethod=='DAC'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dsb.png"></span></c:when>
                                        <c:when test="${vo.type == '9'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dsh.png"></span></c:when>
                                        <c:when test="${vo.type == '10'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/jiesu.png"></span></c:when>
                                        <c:when test="${vo.type == '11'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/yzf.png"></span></c:when>
                                        <c:when test="${vo.type == '12'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/ylb.png"></span></c:when>
                                        <c:when test="${vo.type == '13'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dfb.png"></span></c:when>
                                        <c:when test="${vo.type == '14'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dsh.png"></span></c:when>
                                        <c:when test="${vo.type == '15'}"><span class="zt-ico-w"><img src="${pageContext.request.contextPath}/images/dfb.png"></span></c:when>
                                    </c:choose>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>保证金金额：<span class="red"><span class="red"><c:choose><c:when test="${vo.assureMoney >= '0'}"><fmt:formatNumber type="number" value="${vo.assureMoney} " maxFractionDigits="0"/>元</c:when></c:choose></span></span></li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='DAC'}">竞价币种</c:when><c:when test="${vo.rfqMethod=='RAQ'}">报价货币</c:when></c:choose>：<c:choose><c:when test="${vo.currency=='CNY'}">人民币CNY</c:when><c:when test="${vo.currency=='USD'}">美元USD</c:when><c:when test="${vo.currency=='GBP'}">英镑GBP</c:when><c:when test="${vo.currency=='JPY'}">日元JPY</c:when><c:when test="${vo.currency=='EUR'}">欧元EUR</c:when><c:when test="${vo.currency=='HKD'}">港元HKD</c:when><c:when test="${vo.currency=='CHF'}">瑞士法郎CHF</c:when></c:choose></li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='DAC'}">参考总价</c:when><c:when test="${vo.rfqMethod=='RAQ'}">预算总价</c:when></c:choose>：<span class="red"><fmt:formatNumber type="number" value="${vo.totalBudget}" maxFractionDigits="0"/>元</span></li>
                                        <%--<li>预算总价：<span class="red">${vo.totalPrice}元</span></li>--%>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <c:if test="${rfqRulesVo.partialProductFlag == '1' && rfqRulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：允许对部分产品报价   允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqRulesVo.partialProductFlag != '1' && rfqRulesVo.partialQuantityFlag == '1' }"> <span>分项分量报价：不允许对部分产品报价   允许对部分数量报价</span></c:if>

                                        <c:if test="${rfqRulesVo.partialProductFlag == '1' && rfqRulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                        <c:if test="${rfqRulesVo.partialProductFlag != '1' && rfqRulesVo.partialQuantityFlag != '1' }"> <span>分项分量报价：不允许对部分产品报价   不允许对部分数量报价</span></c:if>
                                    </ul>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>发布时间：${vo.issueDate}</li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='DAC'}">竞价开始时间</c:when><c:when test="${vo.rfqMethod=='RAQ'}">报价开始时间</c:when></c:choose>：${vo.startDate}</li>
                                        <li><c:choose><c:when test="${vo.rfqMethod=='DAC'}">竞价截止时间</c:when><c:when test="${vo.rfqMethod=='RAQ'}">报价截止时间</c:when></c:choose>：${vo.quotationEndDate}</li>
                                        <c:if test="${vo.publicBiddingFlag == '1'}">
                                            <li>报名截止时间：${vo.registrationEndTime}</li>
                                        </c:if>

                                    </ul>
                                    <input type="hidden" name="registrationEndTime" id="registrationEndTime" value="${vo.registrationEndTime}">
                                    <input type="hidden" name="startDate" id="startDate" value="${vo.startDate}">
                                    <input type="hidden" name="quotationEndDate" id="quotationEndDate" value="${vo.quotationEndDate}">
                                    <%--<c:choose>--%>
                                        <%--<c:when test="${vo.type == '5'|| vo.type == '6' }"><a class="btn btn-info tz-time-btn" data-position="center" data-toggle="modal" data-target="#adjustTime" onclick="checkTime ();"><i class="icon icon-time"></i>&nbsp;调整时间</a></c:when>--%>
                                    <%--</c:choose>--%>
                                    <%--<c:choose>--%>
                                        <%--<c:when test="${vo.type == '7'}"><a href="${pageContext.request.contextPath}/designateResult/init?unifiedRfqNum=${vo.unifiedRfqNum}"><span class="btn btn-warning mt12"><i class="icon icon-circle-arrow-right"></i>&nbsp;开标</span></a></c:when>--%>
                                    <%--</c:choose>--%>
                                </div>
                                <div class="mg-pacne clearfix">
                                    <ul>
                                        <li>联系人：${vo.linkmanName}</li>
                                        <li>联系电话：${vo.linkmanTelphone}</li>
                                        <li style="width:40%;">交货地址：${vo.deliveryProvince}${vo.deliveryCity}${vo.deliveryAddress}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-content">
                        <div class="panel">
                            <div class="tab-content">
                                <div class="tab-pane example active"  id="tab1">
                                    <table class="table table-bordered table-hove align-md" id="jqGrid1">
                                        <div class="col-md-12" id="jqGridPager1"></div>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 结束 -->
<script src="${pageContext.request.contextPath}/js/base.js?v=${version}"></script>
</body>
</html>
