<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引用tag标签库--%>
<%@include file="../common/tagDeclare.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>供应商缴纳审核列表</title>
    <%@include file="../common/headBase.jsp"%>
    <%@include file="../common/jqGridBootstrap.jsp"%>

    <!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
    <%--<script src="${pageContext.request.contextPath}/js/jquery.js"></script>--%>
    <!-- ZUI Javascript组件 -->
    <script src="${pageContext.request.contextPath}/js/zui.min.js"></script>
    <!--选择时间和日期-->
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <%--<script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>--%>
    <script src="${resource}/js/rfqQuotationFee/rfqFinancialApproval.js?v=${version}"></script>


</head>

<body>

<c:import url="../common/top.jsp" />
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<div class="wrapper">
    <div class="container container_main">
        <c:import url="../common/menu.jsp" />
        <div class="rightbar clearfix">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                                                <%--工作台暂不跳转--%>                         <%--<li><a href="${sessionScope.workHref}"><i class="icon icon-home"></i> 工作台</a></li>--%>                         <li><a href="#"><i class="icon icon-home"></i> 工作台</a></li>
                        <li class="active">供应商缴纳审核列表</li>
                    </ol>
                </div>
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <ul id="myTab" class="nav nav-tabs">
                                    <li class="active" > <a href="javascript:void(0)" data-toggle="tab" id="financialApprovalAllBtn">全部</a> </li>
                                    <li > <a href="javascript:void(0)" data-toggle="tab" id="financialAssureMoneyBtn" >保证金</a> </li>
                                    <li > <a href="javascript:void(0)" data-toggle="tab" id="financialTenderFlagBtn">标书费</a> </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active  example" >
                                        <div class="col-lg-12 ">
                                            <div class="collapse navbar-collapse navbar-collapse-example ml-12">
                                                <ul class="nav navbar-nav">
                                                    <li class="active" id="financialApprovalAll"><a href="javascript:void(0)" id="myTabl">全部状态<strong class="red2" id="t1"></strong></a></li>
                                                    <li><a href="javascript:void(0)" id="myTab2">未缴纳<strong class="red2"  id="t2"></strong></a></li>
                                                    <li ><a href="javascript:void(0)" id="myTab3">已缴纳<strong class="red2"  id="t3"></strong></a></li>
                                                    <li><a href="javascript:void(0)" id="myTab4">已退回<strong class="red2"  id="t4"></strong></a></li>
                                                </ul>
                                                <div class="clearfix"></div>

                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="row mt12">
                                            <form id="searchForm" name="searchForm"  method="post" onsubmit="return false;">
                                                <div class="so-form">
                                                    <div class="so-form-15 padding1">
                                                        <input name="unifiedRfqNum" id="unifiedRfqNum" type="text" class="form-control" placeholder="询价单号"></div>
                                                    <div class="so-form-15 padding1">
                                                        <input name="title" id="title" type="text" class="form-control" placeholder="询价标题"></div>
                                                    <div class="so-form-15 padding1">
                                                        <input name="recCreatorUsername" id="recCreatorUsername" type="text" class="form-control" placeholder="创建人"></div>
                                                    <div class="so-form-15 padding1">
                                                        <select name="rfqMethod" id="rfqMethod" class="form-control">
                                                           <option selected="selected" value="">请选择</option>
                                                            <option value ="RAQ">询比价</option>
                                                           <%-- <option value ="DAC">反向竞价</option>
                                                            <option value="反向竞拍">反向竞拍</option>--%>
                                                        </select>
                                                    </div>
                                                    <div class="so-form-15 ml12" >
                                                        <button class="btn btn-primary" type="button" id="btnSearch1"><i class="icon icon-search"></i> 搜索</button>
                                                        <button class="btn btn-primary" type="button" id="btnSearch2" style="display: none"><i class="icon icon-search"></i> 搜索</button>
                                                        <button class="btn btn-primary" type="button" id="btnSearch3" style="display: none"><i class="icon icon-search"></i> 搜索</button>
                                                        <button class="btn btn-primary" type="button" id="btnSearch4" style="display: none"><i class="icon icon-search"></i> 搜索</button>
                                                        <input id="resetForm"class="btn" type="reset" value="重置" />
                                                    </div>
                                                    <div class="so-form-10"><a href="javascript:;" class="more-so height32">高级搜索</a></div>
                                                    <div class="hiderow">
                                                        <div class="so-form-15"><input type="text" name="issueStartDate" id="issueStartDate" placeholder="发布时间(起)" class="form-control form-datetime"></div><div class="left ml5 mt5">—</div>
                                                        <div class="so-form-15 ml5" style="margin-right:75px"><input type="text" name="issueEndDate" id="issueEndDate" placeholder="发布时间(止)" class="form-control form-datetime"></div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="dataTable1">
                                <table class="table table-bordered table-hove align-md" id="jqGrid1"></table>
                                <div class="col-md-12" id="jqGridPager1"></div>
                            </div>

                            <div id="dataTable2" hidden="true">
                                <table class="table table-bordered table-hove align-md" id="jqGrid2" ></table>
                                <div class="col-md-12" id="jqGridPager2"></div>
                            </div>
                            <div id="dataTable3" hidden="true">
                                <table class="table table-bordered table-hove align-md" id="jqGrid3" ></table>
                                <div class="col-md-12" id="jqGridPager3"></div>
                            </div>
                            <div id="dataTable4" hidden="true">
                                <table class="table table-bordered table-hove align-md" id="jqGrid4" ></table>
                                <div class="col-md-12" id="jqGridPager4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<div class="col-md-12" id="jqGridPager"></div>--%>
</div>
<%--<!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->--%>
<%--<script src="${pageContext.request.contextPath}/js/jquery.js"></script>--%>
<%--<!-- ZUI Javascript组件 -->--%>
<%--<script src="${pageContext.request.contextPath}/js/zui.min.js"></script>--%>
<%--<!--选择时间和日期-->--%>
<%--<script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>--%>
<script>
    $(function(){

        $('.more-so').click(function(){
            $('.hiderow').stop().slideToggle('fast');
        });

        //日期
        $(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });

        $(".form-date").datetimepicker(
                {
                    language:  "zh-CN",
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

        $(".form-time").datetimepicker({
            language:  "zh-CN",
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            format: 'hh:ii'
        });

    });
</script>
</body>
</html>

