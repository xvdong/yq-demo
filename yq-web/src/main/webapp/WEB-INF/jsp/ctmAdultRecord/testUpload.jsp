<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>demo</title>
    <%@include file="../common/headBase.jsp"%>
    <%--上传相关引入开始--%>
    <%--<script type="text/javascript" src="${pageContext.request.contextPath}/js/upload/jquery-1.10.2.min.js"></script>--%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/upload/fileUpload4Xplat.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/upload/jquery.form.min.js"></script>
    <%--上传相关引入结束--%>

    <%--验证框架相关引入开始--%>
    <%--<script type="text/javascript" src="${pageContext.request.contextPath}/js/base_new.js"></script>--%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/verify.js?v=${version}"></script>
    <%--验证框架相关引入结束--%>
</head>
<body>
<script type="text/javascript">
    $(function () {
        new UploadComponent({
            divId: 'container_doc',
            code: 'AAAA',
            documentId: '1111',
            fileType: 'doc,xls,exe',
            inputDiv: 'fileup',
            styleType:"normal",
            uploadUrl:"../common/uploadFile"
        })
    })
</script>

<div id="container_doc">
</div>

<div class="validate-item" style="width:1000px; margin:0 auto">
    <form action="" method="post" id="validate-form">
        <ul>
            <li>
                <label>正负浮点数且小数位不超过4位</label>
                <div style="width:200px;">
                    <%--valtype为验证的类型即verity.js中的类型--%>
                    <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
            </li>
            <li>
                <label>正负浮点数且小数位不超过4位</label>
                <div style="width:200px;">
                    <%--valtype为验证的类型即verity.js中的类型--%>
                    <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
            </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li><li>
            <label>正负浮点数且小数位不超过4位</label>
            <div style="width:200px;">
                <%--valtype为验证的类型即verity.js中的类型--%>
                <input type="text" placeholder=""  class="v-text form-control input" valtype="floatDec4"/>
        </li>
        </ul>

        <input type="button"  id="validate-button" value="提 交" />
    </form>
</div>
<script>
    window.onload=function(){
        // 绑定提交按钮
        $('#validate-button').bind('click',function(){
            // 移除所有提示信息
            $('.tipInfo').remove();
            var flag=true;
            // 遍历表单中所有的text并校验
            $('.validate-item input.v-text').each(function(){
                var s=$(this).Bvalidate({
                    // 此处同java的override，可以覆盖默认定义的正则表达式
                    positiveFloatReg:/^([1-9]\d+|\d)(\.\d{1,2})?$/

                });
                flag=flag&&s;
            });
            if(flag){
                //验证通过执行
                alert('验证成功');
            }else{ // 校验不通过根据类型提示相应信息
                $.each($('#validate-form').find('input[type="text"]'), function(i, key) {
                    if($('#validate-form').find('input[type="text"]').eq(i).hasClass('error')){
                        $(this).after('<span class="tipInfo red"><i class="icon icon-remove-sign"></i> '+setTipInfo($(this).attr("valtype"))+'</span>')
                    }
                });
            };

            // 当文本框获得焦点时，移除提示信息
            $('#validate-form').find('input[type="text"]').focus(function() {
                $(this).removeClass('error').next('.tipInfo').remove();
            });

            $('html,body').animate({'scrollTop':$('.error').eq(0).offset().top},300);
        });
    };

</script>

</body>
</html>
