<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/tagDeclare.jsp" %>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>竞价大厅<c:choose><c:when test="${biddingHallVo.biddingMethod=='0'}">（按物料行竞价）</c:when><c:when test="${biddingHallVo.biddingMethod=='1'}">（按总价竞价）</c:when></c:choose></title>
    <c:import url="../common/headBase.jsp"/>
    <link href="${pageContext.request.contextPath}/css/auctionroomtotall.css?v=${version}" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/flipclock.css?v=${version}" rel="stylesheet">
    <c:import url="../common/jqGridBootstrap.jsp" />
    <link href="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/lib/datetimepicker/datetimepicker.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/validate/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/rfqRequestState/rfqRequestState.js?v=${version}"></script>
    <!--[if lt IE 8]>
    <div class="alert alert-danger">您正在使用 <strong>过时的</strong> 浏览器. 是时候 <a href="http://browsehappy.com/">更换一个更好的浏览器</a>
        来提升用户体验.
    </div>
    <![endif]-->

</head>
<body>
<div class="top_nav h70">
    <div class="container">
        <div class="row">
            <div class="bj-logo">
                <a href="#"></a>
            </div>
            <div class="bj-chuizi">
                <img draggable="false" src="${pageContext.request.contextPath}/images/bj-chuizi.png">
            </div>
            <div class="pull-right headtime">
                <span class="left">距离竞价结束时间还有</span>
                <div class="clock"></div>
            </div>
        </div>
    </div>
</div>
<div class="bj-head bj-headmt">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pull-left">
                <ul class="topmenu bj-zhuangtai t20">
                    <li class="grayblue">当前状态</li>
                    <li>${biddingHallVo.typeStr}</li>
                    <li><a href="${pageContext.request.contextPath}/dacReverseAuction/reverseAuctionDetail?id=${biddingHallVo.id}" target="caigoufang-jingjiadanxiangqing" class="grayblue border-text">竞价单详情</a></li>
                    <input type="hidden" value="${biddingHallVo.id}" id="requestId"/>
                    <input type="hidden" value="${biddingHallVo.biddingMethod}" id="biddingMethod"/>
                    <input type="hidden" value="window.sessionStorage.unifiedRfqNum" id="unifiedRfqNum"/>
                </ul>
            </div>
            <div class="pull-right mr12">
                <ul class="topmenu t20-right">
                    <li class="font14">
                        <span>采购单位:</span>&nbsp&nbsp<span>${biddingHallVo.ouName}</span>
                    </li>
                    <li class="font14">
                        <span>竞价标题:</span>&nbsp&nbsp<span>${biddingHallVo.title}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="container">
        <div class="row mt10r">
            <div class="panel bj-left">
                <ul class="nav nav-tabs clearfix font-16px xx-title-panel">
                    <li class="active"><a href="#tabgs1" data-toggle="tab">竞价人</a></li>
                    <li class=""><a href="#tabgs2" data-toggle="tab">未审批</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active panel-body0 font14" id="tabgs1">
                    </div>
                    <div class="tab-pane panel-body0 font14" id="tabgs2">
                    </div>
                </div>
            </div>
            <div class="panel bj-right">
                <div class="panel-heading clearfix font-16px">
                    竞价概要
                </div>
                <div class="panel-body font12 pd22">
                    <div class="list2 col-md-4 col-xs-4">
                        <dl class="font14">
                            <dt style="background:url(${pageContext.request.contextPath}/images/supply-num.jpg) no-repeat center center;"></dt>
                            <dd><strong>参与供应商数</strong></dd>
                            <dd class="bigh"><span id="supplierNum"></span><span id="supplier" style="font-size: 14px;color: #0d0d0d">家</span></dd>
                        </dl>
                    </div>
                    <div class="list2 col-md-4 col-xs-4">
                        <dl class="font14">
                            <dt style="background:url(${pageContext.request.contextPath}/images/Y-num.jpg) no-repeat center center;"></dt>
                            <dd><strong>供应商出价总次数</strong></dd>
                            <dd class="bigh"><span id="totalBid"></span><span id="total" style="font-size: 14px;color: #0d0d0d"><strong>次</strong></span></dd>
                        </dl>
                    </div>
                    <div class="list2 col-md-4 col-xs-4 nav-pri">
                        <dl class="font14">
                            <dt style="background:url(${pageContext.request.contextPath}/images/Y-low.jpg) no-repeat center center;"></dt>
                            <dd><strong>当前最低价</strong></dd>
                            <dd class="bigh"><span id="minPrice"></span><span id="Price" style="font-size: 14px;color: #0d0d0d">元</span></dd>
                        </dl>
                        <div class="nav-pripop">
                            <dl id="minMaterial">
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel bj-right bj-right2">
                <div class="panel-heading clearfix font-16px">
                    竞价规则
                </div>
                <div class="panel-body font14 pd25">
                    <div class="clearfix bj-quot c666">
                        <ul class="col-md-4">
                            <li>竞价开始时间：<span id ="startDate"><c:choose><c:when test="${fn:length(biddingHallVo.quotationStartDate)>'16'}">${fn:substring(biddingHallVo.quotationStartDate,0,16)}</c:when><c:otherwise>${biddingHallVo.quotationStartDate}</c:otherwise></c:choose></span></li>
                            <li>起拍价：<span>
                                <c:choose>
                                    <c:when test="${!empty biddingHallVo.startPrice}">
                                        ${biddingHallVo.startPrice}元
                                    </c:when>
                                    <c:otherwise>不限</c:otherwise>
                                </c:choose>
                                </span></li>
                            <li class="op-pri">公开竞价价格：<span>
                                <c:choose>
                                    <c:when test="${!empty biddingHallVo.viewLowerestPriceFlag && biddingHallVo.viewLowerestPriceFlag == '1'}">
                                        是
                                    </c:when>
                                    <c:otherwise>否</c:otherwise>
                                </c:choose>
                            </span></li>
                            <li>竞价截止时间：<span id ="quotationEndDate"><c:choose><c:when test="${fn:length(biddingHallVo.quotationEndDate)>'16'}">${fn:substring(biddingHallVo.quotationEndDate,0,16)}</c:when><c:otherwise>${biddingHallVo.quotationEndDate}</c:otherwise></c:choose></span></li>
                            <li>最小降价幅度：<span>

                                <c:choose>
                                    <c:when test="${!empty biddingHallVo.priceGrad}">
                                        ${biddingHallVo.priceGrad}元
                                    </c:when>
                                    <c:otherwise>不限</c:otherwise>
                                </c:choose>
                            </span></li>
                            <li class="op-rank">公开竞价排名：<span>
                            <c:choose>
                                <c:when test="${!empty biddingHallVo.viewPriceBillboardFlag && biddingHallVo.viewPriceBillboardFlag == '1'}">
                                    是
                                </c:when>
                                <c:otherwise>否</c:otherwise>
                            </c:choose>
                            </span></li>
                            <c:if test="${!empty biddingHallVo.quotationUpperDate}">
                                <li>超时竞价延时:<span>
                                    <span>${biddingHallVo.quotationUpperDate}分钟</span>
                            </span></li>
                            </c:if>
                            <li class="op-comname">公开竞价公司名称：<span>是</span></li>
                        </ul>
                        <div class="btngroup pull-left">
                            <a class="btn btn-info mr22" data-position="center" data-toggle="modal" data-target="#adjustTime" onclick="checkTime ();"><i class="icon icon-time"></i>&nbsp;调整时间</a>
                            <span class="btn btn-danger mr22" data-position="center" data-toggle="modal" data-target="#cancelPrice" >撤销竞价</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <table class="table table-bordered table-hove align-md" style="margin-bottom:5px; width:100%;"
                   id="jqGrid"></table>
            <div class="col-md-12" id="jqGridPager"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="adjustTime">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">调整时间</h4>
            </div>
            <div class="modal-body modal-body2">
                <c:if test="${vo.publicBiddingFlag == '1'}">
                    <div class="mg-row">
                        <span class="block-title">报名截止时间：</span>
                        <div class="btn-group">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                指定时间<span class="caret"></span>
                            </button>
                            <ul id="select1" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li><a href="javascript:void(0)" onclick="nowTime(1);">立即结束</a></li>
                                <li><a href="javascript:void(0)" onclick="specifiedTime(1);">指定时间</a></li>
                            </ul>
                        </div>
                        <div class="input-group">
                            <input type="text" name="date" id="date1" placeholder="请选择时间" class="form-control form-datetime" >
                        </div>
                    </div>
                </c:if>
                <div class="mg-row">
                    <span class="block-title">报价开始时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                        <ul id="select2" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                            <li><a href="javascript:void(0)" onclick="nowTime(2);">立即开始</a></li>
                            <li><a href="javascript:void(0)" onclick="specifiedTime(2);">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date2" placeholder="请选择时间" class="form-control form-datetime" >
                    </div>
                </div>
                <div class="mg-row">
                    <span class="block-title">报价截止时间：</span>
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown">
                            指定时间<span class="caret"></span>
                        </button>
                        <ul id="select3" class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                            <li><a href="javascript:void(0)" onclick="nowTime(3);">立即结束</a></li>
                            <li><a href="javascript:void(0)" onclick="specifiedTime(3);">指定时间</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" name="date" id="date3" placeholder="请选择时间" class="form-control form-datetime" >
                    </div>
                </div>
                <div><span class="required"></span></div>
                <label class="col-md-2" style="line-height: 27px">调整原因：</label>
                <div class="">
                    <textarea class="form-control" name="date" rows="6" placeholder="请填写修改原因" id="date4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateTime()">保存</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cancelPrice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">关闭</span></button>
                <h4 class="modal-title">终止竞价</h4>
            </div>
            <div class="modal-body">
                <div class="mg-row">
                    <span class="left-title">原因类别：</span>
                    <div class="right-paner">
                        <select class="form-control wid-35b">
                            <option value="供应商数量不符合要求">供应商数量不符合要求</option>
                            <option value="供应商出价有误">供应商出价有误</option>
                            <option value="不想采购了">不想采购了</option>
                        </select>
                    </div>
                </div>
                <div class="mg-row">
                    <span class="left-title">原因说明：</span>
                    <div class="right-paner">
                        <textarea class="form-control" rows="6" placeholder="可以输入多行文本"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary">确定</button>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/js/flipclock.js"></script>

<c:import url="../common/jqGridBootstrap.jsp"/>
<script src="${pageContext.request.contextPath}/js/commonPage.js?v=${version}"></script>
<script src="${pageContext.request.contextPath}/js/biddingHall/biddingHall.js?v=${version}"></script>

<script>
    $(".form-datetime").datetimepicker(
            {
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "yyyy-mm-dd hh:ii"
            });

    var clock = $('.clock').FlipClock({
        autoStart: false,
        countdown: true,
        callbacks: {
            stop: function () {
                //到0回调函数
            }
        }
    });

    var secondOver = '<c:out value="${secondOver}"></c:out> ';
    clock.setTime(secondOver);
    clock.start();
    $('.minutes').html('时');
    $('.seconds').html('分');
    $('.clock').append('<span class="flip-clock-divider">秒</span>')
    $('.flip:nth-last-child(6)').after('<span class="flip-clock-divider">时</span>');
    $('.flip:nth-last-child(4)').after('<span class="flip-clock-divider">分</span>');
    $("#unifiedRfqNum").val(window.sessionStorage.unifiedRfqNum);
</script>
</body>
</html>

