$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    $('#attachment').click(function(){
        getAttData("BL","#jqGrid","#jqGridPager");
    })
    $('#attachment2').click(function(){
        getAttData("TL","#jqGrid1","#jqGridPager1");
    })
    function getAttData(type,grid,page){
        $(grid).jqGrid({
            url: '/rfqLibTerms/rfqLibTermsList',
            datatype: 'json',
            mtype: 'POST',
            colModel: [
                {
                    label: '序号',
                    name: 'termId',
                    index: 'TERM_ID',
                    width: 40,
                    key: true
                },
                {
                    label: '条款名称',
                    name: 'title',
                    index: 'TITLE',
                    width: 80
                },
                {
                    label: '条款内容',
                    name: 'content',
                    index: 'CONTENT',
                    width: 110
                },
                {
                    label: '备注',
                    name: 'remark',
                    index: 'REMARK',
                    width: 110
                }
            ],
            jsonReader: { //映射server端返回的字段
                root: "list",
                rows: "list",
                page: "pageNum",
                total: "pages",
                records: "total"
            },
            postData:{type:type},
            prmNames: {
                id: 'termId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
            },
            viewrecords: true, // show the current page, data rang and total records on the toolbar
            multiselect: true,
            width: 810,
            height: "100%",
            rowNum: 10,
            rowList: [10, 20, 30],
            sortname: 'TERM_ID',
            sortorder: 'asc',
            pager:page,
            // caption: "询报价_条款定义库列表"
        });
    }

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqLibTermsDetail(\'' + rawObject.termId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqLibTerms(\'' + rawObject.termId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqLibTerms(\'' + rawObject.termId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqLibTerms").click(function () {
        location.href = "editRfqLibTerms?oper=add"
    });
});

function rfqLibTermsDetail(termId) {
    location.href = "rfqLibTermsDetail?termId=" + termId;
}

function editRfqLibTerms(termId) {
    location.href = "editRfqLibTerms?oper=edit&termId=" + termId;
}

function deleteRfqLibTerms(termId) {
    location.href = "delRfqLibTerms?termId=" + termId;
}