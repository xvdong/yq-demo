$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    var $jqGrid = $("#jqGrid");
    $("#jqGrid").jqGrid({
        url: ctx+'/rfqPreauditSupplier/preauditSupplierList',
        datatype: 'json',
        mtype: 'POST',
        autowidth:true,
        rownumbers: true,
        postData: {rfqMethod: $('#myTab .active a').data('method')},
        colModel: [
            /*{
                label: '序号',
                name: 'id',
                index: 'id',
                width: 20,
                key: true,
            },*/
            {
                label: '类型',
                name: 'rfqMethod',
                index: 'RFQ_METHOD',
                width: 56,
                formatter: rfqMethodFormatter
            },
            {
                label: '询价单号',
                name: 'unifiedRfqNum',
                index: 'unifiedRfqNum',
                width: 76,
                hidden:true,
                formatter: rfqNumFormatter
            },
            {
                label: '询价单号',
                name: 'ouRfqNum',
                index: 'OU_RFQ_NUM',
                width: 76,
                formatter: rfqNumFormatter
            },
            {
                label: '询价标题',
                name: 'title',
                index: 'title',
                width: 106,
            },
            {
                label: '采购单位',
                name: 'ouName',
                index: 'ouName',
                width: 142,
            },
            {
                label: '发布人',
                name: 'issueUsername',
                index: 'issueUsername',
                width: 56
            },
            {
                label: '发布时间',
                name: 'issueDate',
                index: 'issueDate',
                width: 136
            },
            {
                label: '截止时间',
                name: 'registrationEndDate',
                index: 'registrationEndDate',
                width: 139
            },

            {
                label: '状态',
                name: 'preauditSupplierStatus',
                index: 'preauditSupplierStatus',
                width: 57,
                formatter: statusFormatter
            },

            {
                width: 130,
                label: '操作',
                name: 'act',
                index: 'act',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true,
        width: 980,
        height: "100%",
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'issueDate',
        sortorder: 'desc',
        pager: "#jqGridPager",
        loadComplete: function () {
            var $activeTab = $('#myTab .active a');
            //表单重置
            var $activeContent = $($activeTab.attr('href'));
            $activeContent.find('.searchFormTab')[0].reset();
            $.placeholder();
            //状态重置
            var $lis = $activeContent.find('.navbar-nav li');
            $lis.removeClass('active');
            //默认为全部状态激活
            $lis.eq(0).addClass('active');
            var rfqMethod = $activeTab.data('method') || '';
            $.get('searchStatus', {rfqMethod: rfqMethod}, function (data) {

                if ($lis != undefined) {
                    $lis.each(function () {
                        var $a = $(this).find('a');
                        var type = $a.data('type');
                        var $stext = $a.find('strong');

                        $stext.text('(' + data[type] + ')');

                    });
                }

            }, 'json');
            if (rfqMethod == 'DAC') {
                $("#jqGrid").jqGrid('setLabel', 2, "竞价单号");
                $("#jqGrid").jqGrid('setLabel', 3, "竞价标题");
            } else if (rfqMethod == 'RAQ') {
                $("#jqGrid").jqGrid('setLabel', 2, "询价单号");
                $("#jqGrid").jqGrid('setLabel', 3, "询价标题");
            }
        }

    });

    $("#jqGrid").jqGrid('setLabel', 0,"序号");

    var $myTab = $('#myTab');

    //标签页切换
    $myTab.find('[data-toggle]').on('shown.zui.tab', function(e) {
        var $activeTab = $(e.target);
        //表单重置
        var $activeContent = $($activeTab.attr('href'));
        $activeContent.find('.searchFormTab')[0].reset();
        $.placeholder();
        //状态重置
        var $lis = $activeContent.find('.navbar-nav li');
        $lis.removeClass('active');
        //默认为全部状态激活
        $lis.eq(0).addClass('active');
        //数据重新加载

        var rfqMethod = $activeTab.data('method')||'';

        var params = {"rfqMethod":rfqMethod};

        //TODO 数据统计
        $.get('searchStatus',{rfqMethod:rfqMethod},function(data){

            if($lis!=undefined){
                $lis.each(function(){
                    var $a = $(this).find('a');
                    var type = $a.data('type');
                    var $stext = $a.find('strong');

                    $stext.text('('+data[type]+')');

                });
            }

        },'json');
        if(rfqMethod=='DAC'){
            $("#jqGrid").jqGrid('setLabel', 2,"竞价单号");
            $("#jqGrid").jqGrid('setLabel', 3,"竞价标题");
        }else if(rfqMethod=='RAQ'){
            $("#jqGrid").jqGrid('setLabel', 2,"询价单号");
            $("#jqGrid").jqGrid('setLabel', 3,"询价标题");
        }
        deletejqGridJson();
        $("#jqGrid").jqGrid('setGridParam', {
            postData: params,
            page: 1
        }).trigger("reloadGrid");



    });

    //状态页切换
    $('div.tab-content').on('click','.navbar-nav a',function(){
        var $this = $(this);
        var type = $this.data("type");
        var status = $this.data("status");
        var $activeTab = $myTab.find('.active a');
        var rfqMethod = $activeTab.data('method')||'';
        if(type=='allStatus'||type=='notEnroll'||type=='audit'||type=='pass'||type=='back'){
            searchByStatus(status,rfqMethod);
        }else if(type=='allTime'||type=='notStop'||type=='arreadyStop'){
            searchByTime(status,rfqMethod);
        }
    });
    //状态校验
    $jqGrid.on('click', '.js-check', function () {
        var $this = $(this);
        var id = $this.data('id');
        var type = $this.data('type');
        var action = $this.data('action');
        $.post(ctx + '/rfqPreauditSupplier/checkState', {id: id, type: type}, function (msg) {
            if (msg.rspcod != 200) {
                RFQ.error(msg.rspmsg || '网络异常！');
                window.location.reload();
            } else {
                eval(action);
            }

        });
    });
});
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rowObject) {
        var status = rowObject['preauditSupplierStatus'];
        var detailBtn = '<span class="ml5"><a class="js-check btn btn-info btn-sm" href="javascript:;" ' ;
        if(rowObject['rfqMethod']=='RAQ'){
            detailBtn += ' data-id="' + rowObject.id +'" data-type="0" data-action="PreauditSupplierDetail(\'' + rowObject['unifiedRfqNum'] + '\',\''+ rowObject['rfqMethod']+'\')"><i class="js-check icon icon-zoom-in"></i>查看</a></span>';
        }else if(rowObject['rfqMethod']=='DAC'){
            detailBtn += ' data-id="' + rowObject.id + '" data-type="0" data-action="PreauditSupplierDetail(\'' + rowObject['unifiedRfqNum'] + '\',\'' + rowObject['rfqMethod'] + '\')"><i class="js-check icon icon-zoom-in"></i>查看</a></span>';
        }


        var enrollBtn = '<span class="ml5"><a class="js-check btn btn-sm btn-success" href="javascript:;"' ;
        if(rowObject['rfqMethod']=='RAQ'){
            enrollBtn += 'data-id="' + rowObject.id + '" data-type="1" data-action="PreauditSupplierEdit(\'' + rowObject['unifiedRfqNum'] + '\',\'' + rowObject['rfqMethod'] + '\')"><i class="js-check icon icon-chevron-sign-right"></i> 报名</a></span>';
        }else if(rowObject['rfqMethod']=='DAC'){
            enrollBtn += 'data-id="' + rowObject.id + '" data-type="1" data-action="PreauditSupplierEdit(\'' + rowObject['unifiedRfqNum'] + '\',\'' + rowObject['rfqMethod'] + '\')"><i class="js-check icon icon-chevron-sign-right"></i> 报名</a></span>';
        }


        //--------------------------------------- todo 暂时修改
        var endDate = 0;
        if(rowObject.registrationEndDate != undefined && rowObject.registrationEndDate != null && rowObject.registrationEndDate != ""){
            endDate = Date.parse(rowObject['registrationEndDate'].replace(/-/g,'/'));
        }
        //----------------------------------------
        var now = new Date().getTime();

        if ((status == "" && (endDate-now)>=0 )||(status == "2" && (endDate-now)>=0 )||(status == "3"&& (endDate-now)>=0)) {
            return enrollBtn;
        } else {
            return detailBtn;
        }

    };
    function statusFormatter(cellvalue, options, rawObject) {
        if (cellvalue == "" || cellvalue == '3') {
            return "未报名"
        } else if (cellvalue == "0") {
            return "审核中";
        } else if (cellvalue == "1") {
            return "已通过";
        } else if (cellvalue == "2") {
            return "已退回";
        }
    }
    function rfqNumFormatter(cellvalue, options, rawObject){

        var numHtml='<span class="blue" onclick="rfqRequestDetail(\''+rawObject["id"]+'\')">'+cellvalue+'</span>';
        return numHtml;
    }

function rfqRequestDetail(id) {
    var aa = window.open();
    var url = ctx+"/rfqRequest/rfqRequestDetail?id=" + id;
    aa.location.href = url ;

}
    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchFormTab"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqRequest").click(function () {
        location.href = ctx+"/rfqRequest/editRfqRequest?oper=add"
    });



/**
 * 起止时间格式化
 */
function startEndTimeFormatter(cellvalue, options, rawObject) {
    var endDate = rawObject.quotationEndDate;
    return cellvalue + '~\n' + endDate;
}


<!-- 搜索-->
function searchByStatus(status,rfqMethod) {
    deletejqGridJson();
    $("#jqGrid").jqGrid('setGridParam', {
        postData: {"preauditSupplierStatus":status,"rfqMethod":rfqMethod},
        page: 1
    }).trigger("reloadGrid");
}

function searchByTime(isStop,rfqMethod){
    deletejqGridJson();
    $("#jqGrid").jqGrid('setGridParam', {
        postData: {"isStop":isStop,"rfqMethod":rfqMethod},
        page: 1
    }).trigger("reloadGrid");
}

/**
 * 类型格式化
 */
function rfqMethodFormatter(cellvalue, options, rawObject) {
    var result = '';
    if ('RAQ' == cellvalue) {
        result = '<span class="label label-warning">询价</span>';
    } else if ('RFQ' == cellvalue) {
        result = '<span class="label label-primary">招标</span>';
    } else if ('DAC' == cellvalue) {
        result = '<span class="label label-warning">竞价</span>';
    } else {
        result = '<span class="label label-danger">未知</span>';
    }
    return result;



}