$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqRulesList',
        datatype: 'json',
        mtype: 'POST',
        colModel: [
				{
					label: '主键',
					name: 'ruleId',
					index: 'RULE_ID',
					width: 20,
					key: true,
				},
				{
					label: '询单ID',
					name: 'requestId',
					index: 'REQUEST_ID',
					width: 20,
				},
				{
					label: '可提前结束此询价单(比价)',
					name: 'advanceEndFlag',
					index: 'ADVANCE_END_FLAG',
					width: 20,
				},
				{
					label: '可延长此询价单(比价)',
					name: 'extandEndFlag',
					index: 'EXTAND_END_FLAG',
					width: 20,
				},
				{
					label: '询价单审批(比价)',
					name: 'needAuditingFlag',
					index: 'NEED_AUDITING_FLAG',
					width: 20,
				},
				{
					label: '追加供应商审批(比价)',
					name: 'appendSupplierFlag',
					index: 'APPEND_SUPPLIER_FLAG',
					width: 20,
				},
				{
					label: '可对部分产品报价(比价)',
					name: 'partialProductFlag',
					index: 'PARTIAL_PRODUCT_FLAG',
					width: 20,
				},
				{
					label: '可对部分数量进行报价(比价)',
					name: 'partialQuantityFlag',
					index: 'PARTIAL_QUANTITY_FLAG',
					width: 20,
				},
				{
					label: '允许代供应商报价(比价)',
					name: 'substituteQuoteFlag',
					index: 'SUBSTITUTE_QUOTE_FLAG',
					width: 20,
				},
				{
					label: '竞价方式(值集:A44)0-单价/1-总价',
					name: 'biddingMethod',
					index: 'BIDDING_METHOD',
					width: 20,
				},
				{
					label: '需要资格预审',
					name: 'needPreauditFlag',
					index: 'NEED_PREAUDIT_FLAG',
					width: 20,
				},
				{
					label: '公开竞价',
					name: 'publicBiddingFlag',
					index: 'PUBLIC_BIDDING_FLAG',
					width: 20,
				},
				{
					label: '可查看排名',
					name: 'viewPriceBillboardFlag',
					index: 'VIEW_PRICE_BILLBOARD_FLAG',
					width: 20,
				},
				{
					label: '查看最低价',
					name: 'viewLowerestPriceFlag',
					index: 'VIEW_LOWEREST_PRICE_FLAG',
					width: 20,
				},
				{
					label: '不需要评议',
					name: 'noNnedScoringFlag',
					index: 'NO_NNED_SCORING_FLAG',
					width: 20,
				},
				{
					label: '变更需要审批',
					name: 'needAuditWhenModifiesFlag',
					index: 'NEED_AUDIT_WHEN_MODIFIES_FLAG',
					width: 20,
				},
				{
					label: '核价结果需要审批',
					name: 'needResultAduitFlag',
					index: 'NEED_RESULT_ADUIT_FLAG',
					width: 20,
				},
				{
					label: '可进行第二轮询价',
					name: 'needSecondRfqFlag',
					index: 'NEED_SECOND_RFQ_FLAG',
					width: 20,
				},
				{
					label: '允许代评议',
					name: 'substituteScoreFlag',
					index: 'SUBSTITUTE_SCORE_FLAG',
					width: 20,
				},
				{
					label: '需要密码开标',
					name: 'needOpeningPwdFlag',
					index: 'NEED_OPENING_PWD_FLAG',
					width: 20,
				},
				{
					label: '需要保证金',
					name: 'needAssureMoney',
					index: 'NEED_ASSURE_MONEY',
					width: 20,
				},
				{
					label: '需要购买标书',
					name: 'needTenderFlag',
					index: 'NEED_TENDER_FLAG',
					width: 20,
				},
				{
					label: '保证金额',
					name: 'assureMoney',
					index: 'ASSURE_MONEY',
					width: 20,
				},
				{
					label: '标书费',
					name: 'tenderMoney',
					index: 'TENDER_MONEY',
					width: 20,
				},
				{
					label: '是否显示报价数据',
					name: 'needShowQuotationData',
					index: 'NEED_SHOW_QUOTATION_DATA',
					width: 20,
				},
				{
					label: '可选择AVL供应商',
					name: 'avlSupplierFlag',
					index: 'AVL_SUPPLIER_FLAG',
					width: 20,
				},
				{
					label: '综合/价格优先 如果选择价格优先 则不需要专家进行评议',
					name: 'typeOfRusult',
					index: 'TYPE_OF_RUSULT',
					width: 20,
				},
				{
					label: '是否改变核价金额',
					name: 'needChangeResult',
					index: 'NEED_CHANGE_RESULT',
					width: 20,
				},
				{
					label: '询单是否可编辑',
					name: 'requestEdit',
					index: 'REQUEST_EDIT',
					width: 20,
				},
				{
					label: '询单是否可核价',
					name: 'requestResult',
					index: 'REQUEST_RESULT',
					width: 20,
				},
				{
					label: '询单是否可结果发布',
					name: 'requestResultPub',
					index: 'REQUEST_RESULT_PUB',
					width: 20,
				},
				{
					label: '竞价类型1反向/-1正向',
					name: 'biddingType',
					index: 'BIDDING_TYPE',
					width: 20,
				},
				{
					label: '是否公开中标结果',
					name: 'isPublicPub',
					index: 'IS_PUBLIC_PUB',
					width: 20,
				},
				{
					label: '是否公布中标结果（0：否；1：是）',
					name: 'isPublicBidResult',
					index: 'IS_PUBLIC_BID_RESULT',
					width: 20,
				},
				{
					label: '是否公布中标金额（0：否；1：是）',
					name: 'isPublicBidMoney',
					index: 'IS_PUBLIC_BID_MONEY',
					width: 20,
				},
				{
					label: '支付保证金类型0锁定1担保交易',
					name: 'isBailType',
					index: 'IS_BAIL_TYPE',
					width: 20,
				},
				{
					label: '报价澄清（0：否；1：是）',
					name: 'isClarification',
					index: 'IS_CLARIFICATION',
					width: 20,
				},
				{
					label: '是否公开网上超市',
					name: 'isPublicOnlineMarket',
					index: 'IS_PUBLIC_ONLINE_MARKET',
					width: 20,
				},
				{
					label: '价格评分基准分',
					name: 'priceScoringBenchmark',
					index: 'PRICE_SCORING_BENCHMARK',
					width: 20,
				},
				{
					label: '是否只能为含税报价0不是，1是',
					name: 'isTaxQuotation',
					index: 'IS_TAX_QUOTATION',
					width: 20,
				},
				{
					label: '参考单价对供应商可见0否，1是',
					name: 'baseTaxed',
					index: 'BASE_TAXED',
					width: 20,
				},
				{
					label: '缴纳保证金时选择东方付通支付0否，1是',
					name: 'dfftPay',
					index: 'DFFT_PAY',
					width: 20,
				},
				{
					label: '缴纳保证金时选择线下支付0否，1是',
					name: 'offlinePay',
					index: 'OFFLINE_PAY',
					width: 20,
				},
				{
					label: '特邀供应商是否需要报名A0不需要/1需要',
					name: 'isSupplierRequirement',
					index: 'IS_SUPPLIER_REQUIREMENT',
					width: 20,
				},
				{
					label: '报名截止时间A',
					name: 'registrationEndDate',
					index: 'REGISTRATION_END_DATE',
					width: 20,
				},
				{
					label: '报价开始时间A',
					name: 'quotationStartDate',
					index: 'QUOTATION_START_DATE',
					width: 20,
				},
				{
					label: '报价截止时间A',
					name: 'quotationEndDate',
					index: 'QUOTATION_END_DATE',
					width: 20,
				},

            {
                width: 100,
                label: '操作',
                name: 'ruleId',
                index: 'RULE_ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'ruleId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'RULE_ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_控制参数列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqRulesDetail(\'' + rawObject.ruleId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqRules(\'' + rawObject.ruleId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqRules(\'' + rawObject.ruleId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqRules").click(function () {
        location.href = "editRfqRules?oper=add"
    });
});

function rfqRulesDetail(ruleId) {
    location.href = "rfqRulesDetail?ruleId=" + ruleId;
}

function editRfqRules(ruleId) {
    location.href = "editRfqRules?oper=edit&ruleId=" + ruleId;
}

function deleteRfqRules(ruleId) {
    location.href = "delRfqRules?ruleId=" + ruleId;
}