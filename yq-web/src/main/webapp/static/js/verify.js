;(function(){
	$.extend($.fn,{
		Bvalidate:function(options){//表单验证
			var defaults = {
				noEmptyReg:/^\S*$/,  //非空
				acsiiReg:/^[\x00-\xFF]+$/,//仅ACSII字符
				numberReg:/^([+-]?)\d*\.?\d+$/,  //纯数字，不能包含任何非数字
				integerReg:/^-?[1-9]\d*$/,  //正负整数
				negativeIntReg:/^-[1-9]\d*$/,  //负整数
				positiveIntReg:/^[0-9]\d*$/,  //正整数
				floatReg:/^-?(([1-9]\d+|\d)(\.\d{1,})?)$/,  //正负浮点数
				negativeFloatReg:/^-(([1-9]\d+|\d)(\.\d{1,})?)$/,  //负浮点数
				positiveFloatReg:/^([1-9]\d+|\d)(\.\d{1,})?$/,  //正浮点数
				alphabetReg:/^[A-Za-z]+$/,//大小写字母
				LETTERReg:/^[A-Z]+$/,//大写字母
				letterReg:/^[a-z]+$/,//小写字母
				chineseReg:/^[\u4e00-\u9fa5]+$/,//中文
				colorReg:/^#[a-fA-F0-9]{6}$/,//16进制色值
				dateReg:/^\d{4}(\-|\/|.)\d{1,2}\1\d{1,2}$/,//日期
				usernameReg:/^[A-Za-z0-9_\-\u4e00-\u9fa5]+$/,//用户名
				passwordReg:/^[A-Za-z0-9_-]{6,18}$/,//密码
				trueNameReg:/^[A-Za-z0-9\u4e00-\u9fa5]+$/,//真实姓名
				tellphoneReg:/^1[3|4|5|8][0-9]\d{8}$/,//手机号码
				phoneNumberCodeReg:/^0\d{2,3}$/,//国内固定电话区号
				phoneNumberReg:/^\d{7,8}$/,//国内固定电话
				emailReg:/\w+((-w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+/,//邮箱
				qqReg:/^[1-9]*[1-9][0-9]*$/,//QQ
				IDcardReg_a:/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{3})$/,//验证身份证15位
				IDcardReg_b:/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})([0-9xX])$/,//验证身份证18位
				IPReg:/((?:(?:25[0-5]|2[0-4]\d|[01]?\d?\d)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d?\d))/,//IP地址
				postCodeReg:/^[1-9][0-9]{5}$/,//邮政编码
				imgReg:/(.*)\.(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)$/,//判断图片
				fileReg:/(.*)\.(rar|zip|7zip|tgz)$/,//判断压缩文件
				siteReg:/[a-zA-z]+:\/\/[^\s]+/,//网址,
				ftpReg:/ftp\:\/\/[^:]*:@([^\/]*)/, //FTP地址
				passportReg:/^[0-9]{9}$/,//验证护照号码
				floatDec4Reg:/^[\d]*(.[\d]{1,4})?$/,//正负浮点数且小数位不超过4位
				area:{ 11: "北京", 12: "天津", 13: "河北", 14: "山西",15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海",32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西",37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东",45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州",53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海",64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门",91: "国外"},
				success:function(){},
				error:function(){}
			};
			var options  = $.extend(true,defaults,options);	
			var self=this,
				value=$(self).val(),
				l=value.length,
				need=$(self).attr('valNeedCache'),
				type=$(self).attr('valtype');
			var S={
				init:function(){
					switch(type){
						case  'required'        : return this.noEmpty();break;//非空
						case  'ACSII'           : return this.needTest(options.acsiiReg);break;//仅ACSII字符
						case  'number'          : return this.needTest(options.numberReg);break;//纯数字，不能包含任何非数字
						case  'integer'         : return this.needTest(options.integerReg);break;//正负整数
						case  'negativeInteger' : return this.needTest(options.negativeIntReg);break;//负整数
						case  'positiveInteger' : return this.needTest(options.positiveIntReg);break;//正整数
						case  'float'           : return this.needTest(options.floatReg);break;//正负浮点数
						case  'negativeFloat'   : return this.needTest(options.negativeFloatReg);break;//负浮点数
						case  'positiveFloat'   : return this.needTest(options.positiveFloatReg);break;//正浮点数
						case  'alphabet'        : return this.needTest(options.alphabetReg);break;//大小写字母
						case  'LETTER'          : return this.needTest(options.LETTERReg);break;//大写字母
						case  'letter'          : return this.needTest(options.letterReg);break;//小写字母
						case  'chinese'         : return this.needTest(options.chineseReg);break;//中文
						case  'color'           : return this.needTest(options.colorReg);break;//16进制色值
						case  'date'            : return this.needTest(options.dateReg);break;//日期
						case  'username'        : return this.needTest(options.usernameReg);break;//用户名
						case  'password'        : return this.needTest(options.passwordReg);break;//密码
						case  'trueName'        : return this.needTest(options.trueNameReg);break;//真实姓名
						case  'tellphone'       : return this.needTest(options.tellphoneReg);break;//手机号码
						case  'phoneNumberCode' : return this.needTest(options.phoneNumberCodeReg);break;//国内固定电话区号
						case  'phoneNumber'     : return this.needTest(options.phoneNumberReg);break;//国内固定号码
						case  'email'           : return this.needTest(options.emailReg);break;//邮箱
						case  'QQ'              : return this.needTest(options.qqReg);break;//QQ
						case  'IDcard'          : return this.checkIDCard(options.IDcardReg_a,options.IDcardReg_b);break;//身份证
						case  'IP'              : return this.needTest(options.IPReg);break;//IP地址
						case  'postCode'        : return this.needTest(options.postCodeReg);break;//邮政编码
						case  'img'             : return this.needTest(options.imgReg);break;//判断图片
						case  'file'            : return this.needTest(options.fileReg);break;//判断压缩文件
						case  'site'            : return this.needTest(options.siteReg);break;//网址
						case  'ftp'             : return this.needTest(options.ftpReg);break;//ftp地址
						case  'passport'        : return this.needTest(options.passportReg);break;//护照号码
						case  'floatDec4'        : return this.needTest(options.floatDec4Reg);break;//正负浮点数且小数位不超过4位
						default                 : break;
					};	
				},
				needTest:function(reg){
					if(typeof(need)=='undefined'){
						return S.regTest(reg);
					}else{
						if(value==''){
							$(self).removeClass('error').addClass('true');
							return true; 	
						}else{
							return S.regTest(reg);	
						};	
					};	
				},
				noEmpty:function(){
					if(value!=''){
						$(self).removeClass('error').addClass('true');	
						return true;	
					}else{
						$(self).removeClass('true').addClass('error');
						return false;	
					};	
				},
				regTest:function(reg){
					if(reg.test(value)){
						$(self).removeClass('error').addClass('true');	
						return true;
					}else{
						$(self).removeClass('true').addClass('error');
						return false;
					};
				},
				checkIDCard:function(_a,_b){//验证中国居民身份证
					 var re;
					 if (l!=15&&l!=18){
						 $(self).removeClass('true').addClass('error').siblings('p').attr('class','error').html('<em class="plugin"></em>身份证号码位数不对').fadeIn(200);
						 return false;
					 }else if (l==15){
						 re = new RegExp(_a);
					 }else{
						 re = new RegExp(_b);
					 };
					 var idcard_array = new Array();
					 idcard_array = value.split("");
					 if(options.area[parseInt(value.substr(0, 2))] == null) { //地区检验
						 $(self).removeClass('true').addClass('error').siblings('p').attr('class','error').html('<em class="plugin"></em>身份证地区非法').fadeIn(200);
						 return false;
					 };
					 var a=value.match(re);
					 if (a!=null){//出生日期正确性检验
						 if(l==15){
							 var DD=new Date("19" + a[3] + "/" + a[4] + "/" + a[5]);
							 var flag=DD.getYear() == a[3] && (DD.getMonth() + 1) == a[4] && DD.getDate() == a[5];
						 }else if (l==18) {
							 var DD=new Date(a[3] + "/" + a[4] + "/" + a[5]);
							 var flag=DD.getFullYear() == a[3] && (DD.getMonth() + 1) == a[4] && DD.getDate() == a[5];
						 };
						 if(!flag){
							 $(self).removeClass('true').addClass('error').siblings('p').attr('class','error').html('<em class="plugin"></em>身份证出生日期不对').fadeIn(200);
							 return false; 
						 };
						 if(l==18){
							 S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7+ (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9+ (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10+ (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5+ (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8+ (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4+ (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2+ parseInt(idcard_array[7]) * 1+ parseInt(idcard_array[8]) * 6+ parseInt(idcard_array[9]) * 3;
							 Y = S % 11;
							 M = "F";
							 JYM = "10X98765432";
							 M = JYM.substr(Y, 1); //判断校验位
							 if (M == idcard_array[17]) {
								 $(self).removeClass('error').addClass('true').siblings('p').attr('class','true').html('<em class="plugin"></em>').fadeIn(200);	
								 return true;
							 }else{
								 $(self).removeClass('true').addClass('error').siblings('p').attr('class','error').html('<em class="plugin"></em>身份证号码校验错误').fadeIn(200);
								 return false;
							 };
						 };
					 }else{
						 $(self).addClass('error').addClass('true').siblings('p').attr('class','error').html('<em class="plugin"></em>身份证含有非法字符').fadeIn(200);
						 return false;
					 };
				}	
			};	
			return S.init();
		},
		
	
		
	});

})(jQuery);

var setTipInfo=function (type){
	switch(type){
		case  'required'        : return '不能为空';break;//非空
		case  'number'          : return '只能是纯数字';break;//仅ACSII字符
		case  'integer'         : return '只能是正负整数';break;
		case  'negativeInteger' : return '只能是负整数';break;
		case  'positiveInteger' : return '只能是正整数';break;
		case  'float'           : return '只能是正负浮点数';break;
		case  'negativeFloat'   : return '只能是负浮点数';break;
		case  'positiveFloat'   : return '只能是正浮点数';break;
		case  'alphabet'        : return '只能是大小写字母';break;
		case  'LETTER'          : return '只能是大写字母';break;
		case  'letter'          : return '只能是小写字母';break;
		case  'chinese'         : return '只能是中文';break;
		case  'color'           : return '只能是16进制色值';break;
		case  'date'            : return '只能是日期';break;
		case  'username'        : return '只能是用户名';break;
		case  'password'        : return '只能是密码';break;
		case  'trueName'        : return '只能是真实姓名';break;
		case  'tellphone'       : return '只能是手机号码';break;
		case  'phoneNumberCode' : return '只能是国内固定电话区号';break;
		case  'phoneNumber'     : return '只能是国内固定号码';break;
		case  'email'           : return '只能是邮箱';break;
		case  'QQ'              : return '只能是QQ';break;
		case  'IDcard'          : return '只能是身份证';break;
		case  'IP'              : return '只能是IP地址';break;
		case  'postCode'        : return '只能是邮政编码';break;
		case  'img'             : return '只能是图片';break;
		case  'file'            : return '只能是压缩文件';break;
		case  'site'            : return '只能是网址';break;
		case  'ftp'             : return '只能是ftp地址';break;
		case  'passport'        : return '只能是护照号码';break;
		case  'floatDec4'       : return '只能是正负浮点数且小数位不超过4位';break;
		default                 : return '请按标准格式填写';break;
	}
}