$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqProjectUserPrivilegeList',
        datatype: 'json',
        mtype: 'POST',
        colModel: [
				{
					label: '(SEQ_RFQ_PROJECT_USER_PRI_ID.NEXTVAL)',
					name: 'pupId',
					index: 'PUP_ID',
					width: 20,
					key: true,
                },
				{
					label: '小组成员ID',
					name: 'puId',
					index: 'PU_ID',
					width: 20,
                },
				{
					label: '业务对象(预留)',
					name: 'objectCode',
					index: 'OBJECT_CODE',
					width: 20,
                },
				{
					label: '操作权限(值集:A43)(1-操作/2-审批/3-查看/4-代报/5-代评/6-密码开标/7-评议)',
					name: 'actionCode',
					index: 'ACTION_CODE',
					width: 20,
                },

            {
                width: 100,
                label: '操作',
                name: 'pupId',
                index: 'PUP_ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'pupId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'PUP_ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_小组成员_权限定义列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqProjectUserPrivilegeDetail(\'' + rawObject.pupId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqProjectUserPrivilege(\'' + rawObject.pupId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqProjectUserPrivilege(\'' + rawObject.pupId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqProjectUserPrivilege").click(function () {
        location.href = "editRfqProjectUserPrivilege?oper=add"
    });
});

function rfqProjectUserPrivilegeDetail(pupId) {
    location.href = "rfqProjectUserPrivilegeDetail?pupId=" + pupId;
}

function editRfqProjectUserPrivilege(pupId) {
    location.href = "editRfqProjectUserPrivilege?oper=edit&pupId=" + pupId;
}

function deleteRfqProjectUserPrivilege(pupId) {
    location.href = "delRfqProjectUserPrivilege?pupId=" + pupId;
}