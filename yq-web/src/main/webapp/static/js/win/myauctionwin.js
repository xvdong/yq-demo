$(document).ready(function () {
    initStatus();    //初始化一些状态
    initMyColmodel();//根据状态来设置可编辑的列
    initGrid();      //初始化jqgrid
});

//初始化一些状态
function initStatus() {
    //初始化jqgrid样式
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';

    getDescValue();//ajax获取中标说明处的值
    //隐藏或显示 最上面带X红条,最下面提交和关闭,补充明细附件块,
    if (type != 10) {
        $("#desc").hide();
        $("#redWrite").show();
        $("#submit").show();

        $("#addFile").show();
        $("#delete").show();
        $("#downLoad").hide();
    } else {
        $("#desc").show();
        $("#redWrite").hide();
        $("#submit").hide();

        $("#addFile").hide();
        $("#delete").hide();
        $("#downLoad").show();
    }
    if (biddingMethod != 1) {
        //initMatterChart();
    } else {
        $("#redMoney").css("cursor", "pointer").click(function () {
            $("#myModal19").modal('show');
            $("#thing").html("物料")

            initMonChart();
        });
    }

    //更新文件下载列表
    updateFileList(i);
    //初始化plupload文件上传插件
    var types = [".jpg", ".jpeg", ".gif", ".png", ".doc", ".docx", ".xls", ".xlsx", ".pdf"];
    var maxSize = 104857600;//100M，单位:B
    if(navigator.appName == "Microsoft Internet Explorer" ){
        $("#myModa2").on('show.zui.modal', function () {
            //初始化flash插件
            initPlupload2(maxSize,types);
            //关闭时清理页面
            initEvent();
        });
    }else {
        //初始化flash插件
        initPlupload(maxSize,types);
        //关闭时清理页面
        initEvent();
    }
}

function initMonChart(){
    $.ajax({
        type:"POST",
        url:$("#contextPath").val()+"/designateContract/quateOffer",
        data:{requestId:$("#requestID").val()},
        datatype: 'json',
        success: function (data) {
            var dataJson=eval("("+data+")");
            setEchat(dataJson.moneyList);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
}

function initMatterChart(){
    $.ajax({
        url:"WLQuotation",
        type:"POST",
        data:{
            requestId:$("#requestId").val(),
            requestItemId:requestItemId
        },
        datatype:'json',
        success: function (data) {
            var dataJson=eval("("+data+")");
            $("#WLtitle").text(name);
            $("#myModal19").modal('show');
            setEchat(dataJson.moneyList);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
}

//获取中标说明
function getDescValue() {
    var bo = new Object();
    bo.quotationId = quotationId;
    bo.memoType = '1';//此页面一定是中标状态
    bo.requestId = requestId;
    $.ajax({
        url: $("#contextPath").val() + "/win/getDesc?biddingMethod="+biddingMethod,   //请求的url地址
        dataType: "json",   //返回格式为json
        async: true,//请求是否异步，默认为异步，这也是ajax重要特性
        data: bo,    //参数值,post才用
        type: "POST",   //请求方式
        beforeSend: function () {
            //请求前的处理
        },
        success: function (data) {
            //请求成功时处理
            $("#contentSimple1").val(data.desc);
            //document.getElementById("contentSimple1").innerHTML =data1.desc;
        },
        complete: function () {
            //请求完成的处理
        },
        error: function () {
            //请求出错处理
            //alert("error")
        }
    });
}

//初始化jqgrid
function initGrid() {
    $("#jqGrid").jqGrid({
        url: $("#contextPath").val() + "/win/test",
        cellurl: $("#contextPath").val() + "/win/edit?requestId="+requestId,
        datatype: 'json', //数据来源，本地数据（local，json,jsonp,xml等）
        height: "auto",//高度，表格高度。可为数值、百分比或'auto'
        colModel: myColmodel,
        mtype: 'POST',
        postData: {"requestId":requestId,"supplierNum": supplierNumt},
        jsonReader: { //映射server端返回的字段
            root: "list",//list就是传来的链表,把它作为根
            rows: "list",
            page: "pageNum",//这个是第几页
            total: "pages", //这个是有几页
            records: "total" //这个是总共有几条记录
        },
        viewrecords: true,//是否在浏览导航栏显示记录总数
        rowNum: 10,//每页显示记录数
        rowList: [10,20, 30, 50,100],//用于改变显示行数的下拉列表框的元素数组。
        pager: '#jqGridPager',//分页、按钮所在的浏览导航栏
        rownumbers: true,
        cellEdit: true,
        cellsubmit: 'remote',
        //toppager: true,//是否在上面显示浏览导航栏
        // subGrid : true,
        //sortname:'SUBTOTAL',//默认的排序列名
        sortorder: 'asc',//默认的排序方式（asc升序，desc降序）
//            caption: "采购退货单列表",//表名
        autowidth: true,//自动宽

        //width: '1000',
        afterSaveCell: function (rowid, cellname, value, iRow, iCol) {
            //编辑之后更新(因为更改总价后单价也要改，而且要显示出来)
            $("#jqGrid").trigger("reloadGrid");
        },
        ////jqgrid去掉滚轮,这个代码并不能保证最小化后再最大化去掉滚轮,但是可以在开始时去掉,通常jqgrid被封装后或者修改主题后会出现滚轮
        //gridComplete: function () {
        //    function removeHorizontalScrollBar() {
        //        $("div.ui-state-default.ui-jqgrid-hdiv.ui-corner-top").css("width", parseInt($("div.ui-state-default.ui-jqgrid-hdiv.ui-corner-top").css("width")) + 1 + "px");
        //        $("#jqGrid").closest(".ui-jqgrid-bdiv").css("width", parseInt($("#jqGrid").closest(".ui-jqgrid-bdiv").css("width")) + 1 + "px");
        //    }
        //    removeHorizontalScrollBar();
        //}

    });
    $("#jqGrid").jqGrid('setLabel', 0, "序号");//保证第一列是自动的序号

}
function actFormatter(cellvalue, options, rawObject) {
    var see = '<a class="blue"  herf="javascript:void(0);" onclick="showMap(\'' +rawObject.requestItemId+'\'' +'\,'+'\''+rawObject.materialName+ '\')"  data-toggle="modal" data-target="#myModal19" >查看</a>';

    return see;
};

showMap=function (requestItemId,name){
    //alert(requestItemId)
    $.ajax({
        url:$("#contextPath").val() + "/designateContract/WLQuotation",
        type:"POST",
        data:{
            requestId:requestId,
            requestItemId:requestItemId
        },
        datatype:'json',
        success: function (data) {
            var dataJson=eval("("+data+")");
            $("#thing").text(name);
            $("#myModal19").modal('show');
            setEchat(dataJson.moneyList);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
};

function setEchat(moneyList){
    var myChart;
    // 基于准备好的dom，初始化echarts图表
    if(biddingMethod == 0){
         myChart = echarts.init(document.getElementById('mainChart'));
    }else{
         myChart = echarts.init(document.getElementById('mainChart1'));
    }
    var option=optionTemptale;
    // 为echarts对象加载数据
    myChart.setOption(convertData(moneyList,option));
}

function fn(materialName) {
    $("#thing").html(materialName)
}

//提交,后台验证中标金额与小计是否相等
function submitButtonEvent(){
    var ids = $("#jqGrid").jqGrid('getDataIDs');
    for(var i=0;i<ids.length;i++){
        var datas = $("#jqGrid").jqGrid("getRowData", ids[i]);
        if ($("#"+(i+1)+"subtotal").val()!=undefined){
            $.ajax({
                url:$("#contextPath").val() + "/win/edit",
                async: false,//请求是否异步，默认为异步，这也是ajax重要特性
                data: {"requestId":requestId,"id":ids[i],"subtotal":$("#"+(i+1)+"subtotal").val()},    //参数值,post才用
                type: "POST",   //请求方式
                success:function(){
                    $("#jqGrid").trigger("reloadGrid");//刷新
                }
            });
        }
    };
    toCheck();
}
function toCheck() {
    $.ajax({
        url: $("#contextPath").val() + "/win/check",    //请求的url地址
        dataType: "json",   //返回格式为json
        async: false,//请求是否异步，默认为异步，这也是ajax重要特性
        data: {'requestId': $("#requestID").val(), 'subtotalTaxed': subtotalTaxed},    //参数值,post才用
        type: "POST",   //请求方式
        beforeSend: function () {
            //请求前的处理
        },
        success: function (data) {
            //请求成功时处理
            if (data.status == "success") {
                $.zui.messager.show('提交成功！', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                location.href = $("#contextPath").val() + "/rfqQuotation/init";
            } else {
                $.zui.messager.show('无法提交,明细总额与中标金额不一致！', {
                    placement: 'center',
                    type: 'danger',
                    icon: 'icon-exclamation-sign'
                });
            }
        },
        complete: function () {
            //请求完成的处理
        },
        error: function () {
            //请求出错处理
            $.zui.messager.show('网络异常', {
                placement: 'center',
                type: 'danger',
                icon: 'icon-exclamation-sign'
            });
        }
    });
}
function closeButtonEvent(){
    /*window.history.back(-1);*/
    location.href = $("#contextPath").val()+"/rfqQuotation/init"
}

//根据条件初始化colmodel
function initMyColmodel() {
    //if ((type!=10)&&(biddingMethod==1)){...}else{}; 未结束(两项可编辑)，按总价
    //if ((type==10)&&(biddingMethod==1)){...}else{}; 结束(两项不可编辑)，按总价
    //if ((type==10)&&(biddingMethod==0)){...}else{}; 两项不可编辑，按物料
    if ((type != 10) && (biddingMethod == 1)) {
        myColmodel = [{
            label: '物料代码',
            name: 'materialNo',//物料代码
            index: 'materialNo',
            align: 'center',
            editable: false,
        },
            {

                label: '物料名称',
                name: 'materialName',//物料名称
                index: 'materialName',
                align: 'center',
                editable: false,


            }, {

                label: '规型',
                name: 'character',//规型
                index: 'character',
                align: 'center',
                editable: false,
            }, {

                label: '数量',
                name: 'confirmedAmout',
                index: 'confirmedAmout',
                editable: false,
                align: 'center',
            },
            {
                label: '单位',
                name: 'unit',//单位
                index: 'unit',
                align: 'center',
                editable: false,

            }, {

                label: '要求交货期',
                name: 'requestDeliveryDate',//实体中注释为交货期,交付日期offerDate无法取到
                index: 'requestDeliveryDate',
                editable: false,
                align: 'center',
            }, {

                label: '单价 ',
                name: 'perPrice',
                index: 'perPrice',
                editable: false,
                align: 'center',
            }, {

                label: '小计',
                name: 'subtotal',
                index: 'subtotal',
                align: 'center',
                editable: true,
            },
            {
                //备注
                label: '备注',
                name: 'memo',
                index: 'memo',
                editable: true,
                align: 'center',
            },
            //{
            //    label: '我的出价',
            //    align: 'center',
            //    editable: false,
            //    formatter: actFormatter
            //},
            {//
                label: '备注aaaa',
                name: 'requestItemId',
                index: 'requestItemId',
                editable: false,
                align: 'center',
                hidden:true,
            },
        ];
    } else {
    }
    ;
    if (biddingMethod == 0) {
        myColmodel = [{
            label: '物料代码',
            name: 'materialNo',//物料代码
            index: 'materialNo',
            align: 'center',
            editable: false,
        },
            {

                label: '物料名称',
                name: 'materialName',//物料名称
                index: 'materialName',
                align: 'center',
                editable: false,


            }, {

                label: '规型',
                name: 'character',//规型
                index: 'character',
                align: 'center',
                editable: false,
            }, {

                label: '数量',
                name: 'confirmedAmout',
                index: 'confirmedAmout',
                editable: false,
                align: 'center',
            },
            {
                label: '单位',
                name: 'unit',//单位
                index: 'unit',
                align: 'center',
                editable: false,

            }, {

                label: '要求交货期',
                name: 'requestDeliveryDate',//实体中注释为交货期,交付日期offerDate无法取到
                index: 'requestDeliveryDate',
                editable: false,
                align: 'center',
            }, {

                label: '单价 ',
                name: 'confirmedPriceTaxed',
                index: 'confirmedPriceTaxed',
                editable: false,
                align: 'center',
            }, {

                label: '小计',
                name: 'subtotalTaxed',
                index: 'subtotalTaxed',
                align: 'center',
                editable: false,
            },
            {
                //备注
                label: '备注',
                name: 'memo',
                index: 'memo',
                editable: false,
                align: 'center',
            },
            {
                label: '我的出价',
                align: 'center',
                editable: false,
                formatter: actFormatter
            },
            {//
                name: 'requestItemId',
                index: 'requestItemId',
                editable: false,
                align: 'center',
                hidden:true,
            },
        ];
    } else {
    }
    ;
    if ((type == 10) && (biddingMethod == 1)) {
        myColmodel = [{
            label: '物料代码',
            name: 'materialNo',//物料代码
            index: 'materialNo',
            align: 'center',
            editable: false,
        },
            {

                label: '物料名称',
                name: 'materialName',//物料名称
                index: 'materialName',
                align: 'center',
                editable: false,


            }, {

                label: '规型',
                name: 'character',//规型
                index: 'character',
                align: 'center',
                editable: false,
            }, {

                label: '数量',
                name: 'confirmedAmout',
                index: 'confirmedAmout',
                editable: false,
                align: 'center',
            },
            {
                label: '单位',
                name: 'unit',//单位
                index: 'unit',
                align: 'center',
                editable: false,

            }, {

                label: '要求交货期',
                name: 'requestDeliveryDate',//实体中注释为交货期,交付日期offerDate无法取到
                index: 'requestDeliveryDate',
                editable: false,
                align: 'center',
            },{

                label: '单价 ',
                name: 'confirmedPriceTaxed',
                index: 'confirmedPriceTaxed',
                editable: false,
                align: 'center',
            }, {

                label: '小计',
                name: 'subtotalTaxed',
                index: 'subtotalTaxed',
                align: 'center',
                editable: false,
            },
            {
                //备注
                label: '备注',
                name: 'memo',
                index: 'memo',
                editable: false,
                align: 'center',
            },
            //{
            //    label: '我的出价',
            //    align: 'center',
            //    editable: false,
            //    formatter: actFormatter
            //},

            {//
                name: 'requestItemId',
                index: 'requestItemId',
                editable: false,
                align: 'center',
                hidden:true,
            },

        ];
    } else {
    }
    ;

}
