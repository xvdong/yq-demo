/**
 * 字典转换
 * Created by jake.xu on 2016-09-07
 */
function formatRfqRequestType(cellvalue, options, rawObject){
	var rval = '';
	switch (cellvalue) {
		case '100': rval = '全部'; break;
		case '0': rval = '草稿'; break;
		case '1': rval = "创建待审批"; break;
		case '2': rval = "待发布"; break;
		case '3':
			if('RAQ'==rawObject.rfqMethod){
				rval = "待报价";
			}else if('DAC'==rawObject.rfqMethod){
				rval = "待竞价";
			}
			 break;
		case '4': rval = "待报名"; break;
		case '5': rval = "报名中"; break;
		case '6':
			if('RAQ'==rawObject.rfqMethod){
				rval = "报价中";
			}else if('DAC'==rawObject.rfqMethod){
				rval = "竞价中";
			}
			 break;
		case '7': rval = "待开标"; break;
		case '8':
			if('RAQ'==rawObject.rfqMethod){
				rval = "待核价";
			}else if('DAC'==rawObject.rfqMethod){
				rval = "待授标";
			}
			 break;
		case '9': rval = "结果待审批"; break;
		case '10': rval = "已结束"; break;
		case '11': rval = "已作废"; break;
		case '12': rval = "已流标"; break;
		case '13': rval = "结果待发布"; break;
		case '14': rval = "多轮创建待审批"; break;
		case '15': rval = "待发布"; break;
		default:
			break;
	}
	return rval;
}
/**
 * 报价状态
 */
function formatRfqQuotationType(cellvalue, options, rawObject){
	var rval = '';
	switch (cellvalue) {
		case '100': rval = '全部'; break;
		case '0': rval = '未开始'; break;
		case '1':
			if('RAQ'==rawObject.rfqMethod){
				rval = "待报价";
			}else if('DAC'==rawObject.rfqMethod){
				rval = "待竞价";
			}else{
				rval = "待报价";
			}
			break;
		case '2':
			if('RAQ'==rawObject.rfqMethod){
				rval = "已报价";
			}else if('DAC'==rawObject.rfqMethod){
				rval = "已竞价";
			}else{
				rval = "已报价";
			}
			break;
		case '3': rval = "结果待发布"; break;
		case '4': rval = "已放弃"; break;
		case '5': rval = "已中标"; break;
		case '6': rval = "未中标"; break;
		case '7': rval = "已作废"; break;
		case '8': rval = "已过期"; break;
		default:
			break;
	}
	return rval;
}
/**
 * 审批状态
 * Created by jake.xu on 2016-09-20
 */
function formatRfqAuditStatus(cellvalue, options, rawObject){
	var rval = '';
	switch (cellvalue) {
		case 20: rval = '审核中'; break;
		case 40: rval = '挂起'; break;
		case 50: rval = "终止"; break;
		case 60: rval = "审批通过"; break;
		case 70: rval = "审批不通过"; break;
		default:
			break;
	}
	return rval;
}

/**
 * 审批类型
 */
function formatRfqAuditType(cellvalue, options, rawObject){
	var rval = '';
	switch (cellvalue) {
		case 'RFQ001': rval = '创建审批'; break;
		case 'RFQ002': rval = '多轮创建审批'; break;
		case 'RFQ003': rval = "追加供应商审批"; break;
		case 'RFQ004': rval = "结果审批"; break;
		default:
			break;
	}
	return rval;
}

/**
 * 供应商报价状态
 */
function formatRfqSupplyType(cellvalue, options, rawObject){
	var rval = '';
	switch (cellvalue) {
		case '10': rval = '未生成报价单'; break;
		case '40': rval = '已生成'; break;
		case '50': rval = "未审批供应商"; break;
		case '60': rval = "草稿"; break;
		case '70': rval = "驳回"; break;
		default:
			break;
	}
	return rval;
}

/**
 * 1、文字过长，多余部分显示。。。。
 * 2、鼠标悬停出现所有文字
 */
function formatCSS(cellvalue, options, rawObject){
	var rval = '<p class="ellipli_1" onmouseover="this.title=this.innerText">'+cellvalue+'</p>';
    if (rawObject.status == 20 ){
        return "";
    }else {
        return rval;
    }
}