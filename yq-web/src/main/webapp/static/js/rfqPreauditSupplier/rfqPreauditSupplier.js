$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqPreauditSupplierList',
        datatype: 'json',
        mtype: 'POST',
        colModel: [
			{
				label: '主键',
				name: 'pasId',
				index: 'PAS_ID',
				width: 20,
				key: true,
			},
			{
				label: '资格预审单ID(FK)',
				name: 'paId',
				index: 'PA_ID',
				width: 20,
			},
				{
					label: '注册公司ID',
					name: 'companyId',
					index: 'COMPANY_ID',
					width: 20,
				},
				{
					label: '供应商代码',
					name: 'supplierCode',
					index: 'SUPPLIER_CODE',
					width: 20,
				},
				{
					label: '供应商名称',
					name: 'supplierName',
					index: 'SUPPLIER_NAME',
					width: 20,
				},
				{
					label: '记录创建日期',
					name: 'recCreateTime',
					index: 'REC_CREATE_TIME',
					width: 20,
				},
				{
					label: '状态(值集:A32)0 待审核/1 通过/2 驳回',
					name: 'status',
					index: 'STATUS',
					width: 20,
				},
				{
					label: '报名详述',
					name: 'responseDesc',
					index: 'RESPONSE_DESC',
					width: 20,
				},
				{
					label: '报名时间',
					name: 'answerTime',
					index: 'ANSWER_TIME',
					width: 20,
				},
				{
					label: '报名人ID',
					name: 'answerUserid',
					index: 'ANSWER_USERID',
					width: 20,
				},
				{
					label: '报名人名称',
					name: 'answerUsername',
					index: 'ANSWER_USERNAME',
					width: 20,
				},
				{
					label: '审批意见',
					name: 'pasDesc',
					index: 'PAS_DESC',
					width: 20,
				},
				{
					label: '合格供应商对应的注册公司名称',
					name: 'companyName',
					index: 'COMPANY_NAME',
					width: 20,
				},
				{
					label: '联系人名称',
					name: 'linkmanName',
					index: 'LINKMAN_NAME',
					width: 20,
				},
				{
					label: '联系人电话',
					name: 'linkmanTelphone',
					index: 'LINKMAN_TELPHONE',
					width: 20,
				},
				{
					label: '审批时间',
					name: 'approvalTime',
					index: 'APPROVAL_TIME',
					width: 20,
				},
				{
					label: '提供给pscs 表示报名信息是否发送成功0：未成功/1：成功',
					name: 'isSuccess',
					index: 'IS_SUCCESS',
					width: 20,
				},
				{
					label: '是否为合格供应商（1：是 0：否）',
					name: 'isSupplier',
					index: 'IS_SUPPLIER',
					width: 20,
				},
				{
					label: '市场标识',
					name: 'martSign',
					index: 'MART_SIGN',
					width: 20,
				},
				{
					label: '微信状态A',
					name: 'isBindWechat',
					index: 'IS_BIND_WECHAT',
					width: 20,
				},
				{
					label: '是否特邀A',
					name: 'isSpecilInvite',
					index: 'IS_SPECIL_INVITE',
					width: 20,
				},

            {
                width: 100,
                label: '操作',
                name: 'pasId',
                index: 'PAS_ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'pasId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'PAS_ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_资格预审_供应商列表列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqPreauditSupplierDetail(\'' + rawObject.pasId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqPreauditSupplier(\'' + rawObject.pasId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqPreauditSupplier(\'' + rawObject.pasId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqPreauditSupplier").click(function () {
        location.href = "editRfqPreauditSupplier?oper=add"
    });
});

function rfqPreauditSupplierDetail(pasId) {
    location.href = "rfqPreauditSupplierDetail?pasId=" + pasId;
}

function editRfqPreauditSupplier(pasId) {
    location.href = "editRfqPreauditSupplier?oper=edit&pasId=" + pasId;
}

function deleteRfqPreauditSupplier(pasId) {
    location.href = "delRfqPreauditSupplier?pasId=" + pasId;
}