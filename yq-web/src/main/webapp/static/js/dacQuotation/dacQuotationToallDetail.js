$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var requestId= $('#requestId').val();

    var colModel = [{
            label: '物料代码',
            name: 'materialNo',
            index: 'MATERIAL_NO',
            width: 80,
        },
        {
            label: '物料名称',
            name: 'materialName',
            index: 'MATERIAL_NAME',
            width: 80,
        },
        {
            label: '型规',
            name: 'character',
            index: 'CHARACTER',
            width: 80
        },
        {
            label: '品牌',
            name: 'producer',
            index: 'PRODUCER',
            width: 80,
        },
        {
            label: '数量',
            name: 'requestAmount',
            index: 'REQUEST_AMOUNT',
            width: 80
        },
        {
            label: '单位',
            name: 'unit',
            index: 'UNIT',
            width: 80
        },
        {
            label: '交货期',
            name: 'requestDeliveryDate',
            index: 'REQUEST_DELIVERY_DATE',
            width: 80,
            formatter:'date',
            formatoptions:{srcformat: 'Y-m-d H:i:s', newformat: 'Y-m-d'}
        }
    ];
    $.post(ctx+'/rfqRequest/getCustomItemHeader',{"requestId":requestId}, function (data) {
            $.each(data.obj,function(i,item){
            var res={};
            res.label=item.itemName;
            res.name="itemValue_"+(i+1);
            res.width=80;
            colModel.push(res);
        });
        console.log(colModel);
        RFQ.jqGrid('#jqGrid').init({
            url: ctx+'/rfqRequest/rfqRequestItemList',
            colModel: colModel,
            postData:{"id":requestId},
            width: 500,
            sortname: 'SEQ',
            sortorder: 'ASC',
            pager: "#jqGridPager"
        });
    },'json');
    /**定时刷新竞价数据*/
    setInterval("reloadePrice()","60000");
});
/**刷新竞价数据*/
function reloadePrice() {
    $.ajax({
        type: "POST",
        async:false,
        url:ctx + '/dacQuotationTotal/loadPrice',
        data: {
            requestId:$("#requestId").val()
        },
        dataType: "json",
        success: function (data) {
            if ($('#viewLowerestPriceFlag').val() === '1') {
                if ($('#pricelow').val() === undefined && data.myQuotationVo.lowerestPrice !== null) {
                    $('.pricelowSign').append('<span id="pricelow">'+ data.myQuotationVo.lowerestPrice +'</span>元');
                } else {
                    $('#pricelow').html(data.myQuotationVo.lowerestPrice);
                }
            } else {
                if ($('#rank').val() === undefined && data.myQuotationVo.lowerestPrice !== null) {
                    $('.rankSign').append('第<span id="rank">'+ data.myQuotationVo.lowerestPrice +'</span>名');
                } else {
                    $('#rank').html(data.myQuotationVo.lowerestPrice);
                }
            }
            if ($('#pricepre').val() === undefined && data.myQuotationVo.lastPrice !== null) {
                $('.pricepreSign').append('<span id="pricepre">'+ data.myQuotationVo.lastPrice +'</span>元');
            } else {
                $('#pricepre').html(data.myQuotationVo.lastPrice);
            }
            $('#stepn').val(0);
            priceArr();
            pricenowState();
            $("#pricelow-d").html(jqNumToMoney($("#pricelow").html()));
            $("#pricepre-d").html(jqNumToMoney($("#pricepre").html()));
            $("#pricenow-d").html(jqNumToMoney($("#pricenow").val()));

            var clock = $('#clock').data("_flipclock");
            clock.setTime(data.quotationBaseVo.countDown);
        },
        error: function () {
            $.zui.messager.show('重新载入数据失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
}
/**提交竞价数据*/
function confirmPrice () {
    $.ajax({
        type: "POST",
        async:false,
        url:ctx + '/dacQuotationTotal/commintPrice',
        data: {
            requestId:$("#requestId").val(),
            subtotalTaxed:$("#pricenow").val(),
            extendField4:$("#extendField4  option:selected").val(),
            isDeleted:$("#isDeleted").val()
        },
        success: function (data) {
            if (data.rspmsg == '操作成功') {
                reloadePrice();
            } else {
                $.zui.messager.show('sorry,系统出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
}
/**本次拟出价是否可输入控制*/
function pricenowState () {
    if ($("#step").val() === undefined || inip === Infinity) {
        $("#pricenow").attr("disabled", false);
    } else {
        $("#pricenow").attr("disabled", true);
    }
}
/**每次操作需重新获取当前最低值*/
function priceArr () {
    arr1 = [];
    if (!isNaN($("#startPrice").html())) {
        arr1.push($("#startPrice").html());
    }
    if (!isNaN($("#pricelow").html())) {
        arr1.push($("#pricelow").html());
    }
    if (!isNaN($("#pricepre").html())) {
        arr1.push($("#pricepre").html());
    }
    inip = Math.min.apply(Math, arr1);
    if (inip !== Infinity ) {
        $("#pricenow").val(inip.toFixed(4));
        stepmax = parseInt(inip/$("#step").html());
    }
}
//竞价单详情
function show(data){
    window.sessionStorage.designateResult_tax=null;
    location.href=ctx+"/dacReverseAuction/reverseAuctionDetail?id="+data;
}