/**
 * 计算非标明细
 */
function calculate() {
    var $requestItemdetailTable = $('#datatable-requestItemdetailTable');
    $requestItemdetailTable.on('input propertychange', '.input-change', function () {
        //材料费计算
        var index = ($(this).parent().parent("tr").index() + 2) / 3;
        var grossWeight = $('input[name="grossWeight' + index + '"]').val();
        var materialUnitPrice = $('input[name="materialUnitPrice' + index + '"]').val();
        !grossWeight && (grossWeight = 0);
        !materialUnitPrice && (materialUnitPrice = 0);
        var materialFee = grossWeight * materialUnitPrice;
        $('input[name="materialFee' + index + '"]').val(materialFee.toFixed(4));

        //单件总价
        var tatal = 0;
        //index 行 i列
        for (var i =1; i < 12; i++) {
            var timeOfSingleton=0;
            var unitPriceOfSingleton=0;
            if(i==1&&index==11){
                 timeOfSingleton = $('input[name="timeOfSingleton11_1"]').val();
                 unitPriceOfSingleton = $('input[name="unitPriceOfSingleton11_1"]').val();
            }else{
                 timeOfSingleton = $('input[name="timeOfSingleton' + index + i + '"]').val();
                 unitPriceOfSingleton = $('input[name="unitPriceOfSingleton' + index + i + '"]').val();
            }
            !timeOfSingleton && (timeOfSingleton = 0);
            !unitPriceOfSingleton && (unitPriceOfSingleton = 0);
            tatal += timeOfSingleton * unitPriceOfSingleton;
        }
        var netWeight=$('input[name="netWeight' + index + '"]').val();

        $('input[name="processTotalFee' + index + '"]').val(tatal.toFixed(4));
        $('input[name="unitTotalPrice' + index + '"]').val((tatal + materialFee+netWeight).toFixed(4));

        //单套件总价
        var singleKitTotalPrice = 0;
        for (var i = 1; i < 12; i++) {
            var unitTotalPrice = $('input[name="unitTotalPrice' + i + '"]').val() * 1;//零件数量目前给1
            !unitTotalPrice && (unitTotalPrice = 0);
            singleKitTotalPrice += unitTotalPrice;
        }
        var managementFeeRate = $('input[name="managementFeeRate"]').val() * 1;
        var profitsRate = $('input[name="profitsRate"]').val() * 1;
        var packingFeeRate = $('input[name="packingFeeRate"]').val() * 1;
        var transportFeeRate = $('input[name="transportFeeRate"]').val() * 1;
        var compositionFeeRate = $('input[name="compositionFeeRate"]').val() * 1;
        var designFeeRate = $('input[name="designFeeRate"]').val() * 1;
        var kitFee = $('input[name="kitFee"]').val() * 1;

        //1+（管理费率+利润率+包装费率+运输费率+合成费率+设计费率）/100）+配套件费*（1+管理费率/100）；
        var rate = 1 + (managementFeeRate + profitsRate + packingFeeRate + transportFeeRate + compositionFeeRate + designFeeRate) / 100 + kitFee * (1 + managementFeeRate / 100)
        var singleKitTotalPrice=singleKitTotalPrice * rate;
        $('input[name="singleKitTotalPrice"]').val(singleKitTotalPrice.toFixed(4));
        $('input[name="totalPrice"]').val(singleKitTotalPrice.toFixed(4) *1);//套件数量未，目前给定值 1

    });

}