/**
 * 物料相关
 */
var totalPrice = 0,pagePriceSum = 0,currentTax = 6;
var itemDetailList = {}; //非标报价明细
var rfqRequestItemId;//询单物料ID
var rfqIndex;//物料行索引
$(document).ready(function () {
	if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0"){
		var $moudle = $('#data-moudle');
		var module = $moudle.val();
		var $id = $('#data-id');
		var id = $id.val();
	}else {
		var $body = $('body');
		var module = $body.data('moudle');
		var id = $body.data('id');
	}
	var requestId = $('#requestId').val();


	//小数位校验
	var checkScale = {unitPrice:2,availableCount:4,tax:0};
	//预算总价计算
	var $totalPrice = $('#totalPrice');
	var $requestItemTable = $('#datatable-requestItemData');
	totalPrice = (+$totalPrice.html())||0;
	$requestItemTable.on('input propertychange', '.js-change', function (e, noCal) {
		var $this = $(this);
		var name = $this.data('name');
		RFQ.scaleCheck(this,checkScale[name]);
		var $tr = $this.closest('tr');
		var availableCount = +$tr.find('input[data-name$="availableCount"]').val();
		var unitPrice = +$tr.find('input[data-name$="unitPrice"]').val();
		var tax = +$tr.find('input[data-name$="tax"]').val();
		var $unitPriceTaxed = $tr.find('input[data-name$="unitPriceTaxed"]');
		var $subtotal = $tr.find('input[data-name$="subtotal"]');
		var $subtotalTaxed = $tr.find('input[data-name$="subtotalTaxed"]');
		!availableCount && (availableCount = 0);
		!unitPrice && (unitPrice = 0);
		!tax && (tax = 0);
		var unitPriceTaxed = ((1 + tax / 100) * unitPrice).toFixed(4);
		$unitPriceTaxed.val(unitPriceTaxed);
		$subtotalTaxed.val((availableCount * unitPriceTaxed).toFixed(4));
		$subtotal.val((availableCount * unitPrice).toFixed(2));
		if (!noCal) {
			setPriceVal(countTotalPrice());
		}
	});

	//税率点击
	$requestItemTable.on('click', '#tax a', function () {
		$(this).parent().parent().prev('.btn').html('<span class="v">'+$(this).html()+'</span>'+'<span class="caret"></span>');
		var tax = $(this).data('value');
		var rfqDtable = $requestItemTable.data('_rfqDTable');
		$requestItemTable.find('input[data-name$="tax"]').val(tax).trigger('input', true);
		setPriceVal(countTotalPrice());
		currentTax = tax;
		//保存当页数据
		var extras = rfqDtable.fetchChanges('rfqQuotationItemTempListBo');
		RFQ.post(ctx+'/rfqQuotationItemTemp/saveRfqQuotationItemTemp?requestId='+requestId+'&validateFlag=0',extras, function (msg) {
			RFQ.post(ctx+'/rfqQuotation/updateTax',{requestId:requestId,tax:tax}, function (data) {
				if(data.obj){
					totalPrice = +data.obj.SUBTOTAL_TAXED;
					$totalPrice.html(totalPrice.toFixed(2));
					rfqDtable.load();
				}
			});
		});
	});


	//计算总价
	function countTotalPrice() {
		var sum = 0;
		$requestItemTable.find("input[data-name='subtotalTaxed']").each(function () {
			var subtotal = +this.value;
			!subtotal && (subtotal = 0);
			sum += subtotal;
		});
		return sum;
	}


	//物料分页加载
	//var $zuiDataTable = $('#requestItemTable');
	(function () {
		var clsMapping = {availableCount:'js-change',unitPrice:'js-change',tax:'js-change',availableDeliveryDate:'form-date',timeOfWarranty:'form-date'};
		var readMapping = {unitPriceTaxed:'readonly',subtotal:'readonly',subtotalTaxed:'readonly'};
		var _mappingNames =  ['items','requestAmount','requestItemId','availableCount','unitPrice','tax','unitPriceTaxed','subtotal','subtotalTaxed','availableDeliveryDate','substitutableSpec','manufactory','productPlace','upstreamSuppliers','transType','deliveryLocation','material','singleWeight','timeOfWarranty','memo'];
		var _mappingLimits = {availableCount:15,unitPrice:12,tax:5,availableDeliveryDate:20,substitutableSpec:200,manufactory:100,productPlace:100,upstreamSuppliers:100,transType:100,deliveryLocation:100,material:50,singleWeight:25,timeOfWarranty:20,memo:2000};
		var inputTpl = '<input type="text" class="form-control input-sm {cls}" {read} data-name="{name}" value="{value}" maxlength="{limit}">';//输入框
		var options = {
			datatype: 'json',
			renderDel:false,
			url:ctx+'/rfqQuotationItemTemp/rfqQuotationItemTempList?requestId='+requestId,
			mappingNames:_mappingNames,
			cellFormat: function (name,val,row,index,i) {
				if(name == 'items'){
					var html='代码：{materialNo} 物料名称：{materialName} 规格型号：{character}<a href="javascript:;" class="zkBtn icon icon-chevron-down"></a><div class="hidden"> 计量单位：{unit} 生产厂家(品牌)：{producer} 交货期：{requestDeliveryDate} 备注：{requestMemo}</div>'.format(row);
					if(row.isItemDetail=='1'){
						html+='<a href="#" onclick="getDetail('+row.materialId+','+row.quotationItemId+','+i+')" class="btn btn-mini hjBtn btn-warning" data-toggle="modal" data-target="#myModafei">非标报</a>';
					}
					return html;
				}else if(name == 'requestAmount'){
					return val||'';
				}else if(name == 'requestItemId'){
					return (val=$("#currentStatus").val()==2?'已报价':'未报价') +  '<input class="form-control" data-name="id" value="'+(row.id||'')+'" type="hidden"><input class="form-control" data-name="requestAmount" value="'+row.requestAmount+'" type="hidden"><input class="form-control" data-name="requestId" value="'+requestId+'" type="hidden"><input class="form-control" data-name="requestItemId" value="'+row.materialId+'" type="hidden">';
				}else if (name == 'tax' && val != '') {
					val = val + "%";
				}else if(name == 'availableDeliveryDate'){
					if(val == '' ){
						val = '{requestDeliveryDate}'.format(row);
					}
				}
				return inputTpl.format({name:name,value:val||'',cls:clsMapping[name]||'',read:readMapping[name]||'',limit:_mappingLimits[name]||100});
			},
			afterLoadData: function () {
				this.visibleObj.find('.form-date').datetimepicker({
					weekStart: 1,
					todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					format: "yyyy-mm-dd"
				});
				pagePriceSum = countTotalPrice();
				$requestItemTable.find('#dropdownMenu1').html(currentTax+'%<span class="caret"></span>');
			},
			beforePagination: function () {
				//分页前保存数据
				var that = this;
				var extras = that.fetchChanges('rfqQuotationItemTempListBo');
				RFQ.d(extras);
				var canPass = false;
				RFQ.post(ctx+'/rfqQuotationItemTemp/saveRfqQuotationItemTemp?requestId='+requestId,extras, function (msg) {
					//数据置空
					canPass = true;
					that.deleteIds = [];
				},false);
				return canPass;
			},
			searchForm:'#searchForm2'
		};

		var rfqDTable = RFQ.dataTable('#requestItemData').init(options);

		$requestItemTable.data('_rfqDTable',rfqDTable);

		rfqDTable.load();


		//关键字搜索
		$('#searchKey').on('click', function () {
			rfqDTable.load();
		});

	})();
});
//excel模板导出
function exportExcelModel(){
	location.href=ctx+'/rfqQuotationItemTemp/exportTemplate';
}

//excel导出
function exportExcel(){
	$.ajax({
		type: "POST",
		url: ctx+"/rfqQuotationItemTemp/exportRecord",
		data: {
			requestId: $('#requestId').val()
		},
		success: function(data) {
			if ("success"==data.status) {
				//预算总价
				var budgetPrice = data.budgetPrice;
				//console.log("budgetPrice:"+budgetPrice);
				location.href=data.message;
			} else {
				RFQ.error('sorry,出错了！');
			}
		}
	});
}

//总价设置
function  setPriceVal(sum){
	totalPrice = totalPrice + sum - pagePriceSum;
	pagePriceSum = sum;
	$('#totalPrice').html(totalPrice.toFixed(2));
	$('#subtotalTaxed').val(totalPrice.toFixed(2));
}

function saveExcel(){
	//校验是否有输入文件
	var $file = $('#file');
	if(!$file[0].value){
		RFQ.error('您还未选择文件');
		return;
	}
	$('#excelUpload').ajaxSubmit({
		success: function (data) {
            var data;
            try
            {
                data = eval('('+data+')');
            }
            catch (e)
            {
                data = jQuery.parseJSON(jQuery(data).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
            }
			if (data.status == 'success') {
				//总价
				var budgetPrice = +data.budgetPrice;
				RFQ.info(data.message);
				$('#cancelPrice33').modal('hide');
				totalPrice = budgetPrice;
				pagePriceSum = 0;
				setPriceVal(0);
				//临时表加载数据
				$('#datatable-requestItemData').data('_rfqDTable').load();
			} else {
				RFQ.error(data.message);
			}
		}
	});
}
function getDetail(id,quotationItemId,index){
	rfqRequestItemId = id;
	rfqIndex = index;
	$.ajax({
		url:ctx+"/rfqQuotation/getQuotationItemDetail",
		type:"POST",
		async : false, //设置成为 同步的
		data:{
			requestItemId:id,
			quotationItemId:quotationItemId
		},
		success: function(data){
			var htmlTex= '<div class="modal-body"><div class="content">'+
				'<div class="so-form">'+
				'<span class="so-form-20">编号：'+data.seriNo+'</span>'+
				'<span class="so-form-20">金额单位：不含增值税元</span>'+
				'</div>'+
				'<div class="clearfix"></div>'+
				'<div class="so-form">'+
				'<span class="so-form-20">中文品名：高速矫机剪切模</span>'+
				'<span class="so-form-20">物品代码：'+data.materialNo+'</span>'+
				'<span class="so-form-15">申请单行编号：'+data.singleLineNo+'</span>'+
				'<span class="so-form-15">图号：'+data.picNo+'</span>'+
				'<span class="so-form-15">要求交货期：2010/12/15</span>'+
				'<span class="so-form-15">数量：5(套）</span>'+
				'</div>'+
				'<table class="table table-bordered align-md change_1" id="datatable-requestItemdetailTable">'+
				'<thead>'+
				'<tr>'+
				'<th rowspan="2">序号</th>'+
				'<th rowspan="2">零件名称</th>'+
				'<th rowspan="2" width="90"></th>'+
				'<th rowspan="2">数量</th>'+
				'<th colspan="4" width="">报价单位</th>'+
				'<th colspan="2">'+data.processName11+'</th>'+
				'<th colspan="2">'+data.processName12+'</th>'+
				'<th colspan="2">'+data.processName13+'</th>'+
				'<th colspan="2">'+data.processName14+'</th>'+
				'<th colspan="2">'+data.processName15+'</th>'+
				'<th colspan="2">'+data.processName16+'</th>'+
				'<th colspan="2">'+data.processName17+'</th>'+
				'<th colspan="2">'+data.processName18+'</th>'+
				'<th colspan="2">'+data.processName19+'</th>'+
				'<th colspan="2">'+data.processName110+'</th>'+
				'<th colspan="2">'+data.processName111+'</th>'+
				'<th rowspan="2" width="90">加工费合计</th>'+
				'<th rowspan="2" width="90">单价合计</th>'+
				'</tr>'+
				'<tr>'+
				'<th width="70">净重</th>'+
				'<th width="70">毛重</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">材料费</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'<th width="70">单件工时</th>'+
				'<th width="70">单价</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>';
			for(var i=1;i<12;i++){
				htmlTex+= '<tr>'+
					'<td rowspan="3">'+i+'</td>'+
					'<td rowspan="3">'+data["partName"+i]+'</td>'+
					'<td rowspan="2">供应商报价</td>'+
					'<td rowspan="2">'+data["quantity"+i]+'</td>'+
					'<td colspan="4" width="">9Cr18MoV</td>'+
					'<td colspan="2" width="">锻造</td>'+
					'<td colspan="2" width="">车床加工</td>'+
					'<td colspan="2" width="">热处理</td>'+
					'<td colspan="2" width="">线切割孔</td>'+
					'<td colspan="2" width="">磨床加工</td>'+
					'<td colspan="2" width="">锻造</td>'+
					'<td colspan="2" width="">车床加工</td>'+
					'<td colspan="2" width="">热处理</td>'+
					'<td colspan="2" width="">线切割孔</td>'+
					'<td colspan="2" width="">磨床加工</td>'+
					'<td colspan="2" width="">其它加工</td>'+
					'<td width="">&nbsp;</td>'+
					'<td width="">&nbsp;</td>'+
					'</tr>'+
					'<tr>'+
					'<td><input type="text" class="form-control input-sm" name="netWeight'+i+'" value="'+data['netWeight'+i]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="grossWeight'+i+'" value="'+data['grossWeight'+i]+'" ></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="materialUnitPrice'+i+'" value="'+data['materialUnitPrice'+i]+'"></td>'+
					'<td><input type="text" class="form-control input-sm " readonly="readonly" name="materialFee'+i+'" value="'+data['materialFee'+i]+'"></td>';
				if(i==11){
					htmlTex+= '<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton11_1" value="'+data.timeOfSingleton11_1+'"></td>'+
						'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton11_1" value="'+data.netWeight11_1+'"></td>';
				}else {
					htmlTex+= '<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+1+'" value="'+data['timeOfSingleton'+i+1]+'"></td>'+
						'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+1+'" value="'+data['unitPriceOfSingleton'+i+1]+'"></td>';
				}
				htmlTex+= '<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+2+'" value="'+data['timeOfSingleton'+i+2]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+2+'" value="'+data['unitPriceOfSingleton'+i+2]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+3+'" value="'+data['timeOfSingleton'+i+3]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+3+'" value="'+data['unitPriceOfSingleton'+i+3]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+4+'" value="'+data['timeOfSingleton'+i+4]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+4+'" value="'+data['unitPriceOfSingleton'+i+4]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+5+'" value="'+data['timeOfSingleton'+i+5]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+5+'" value="'+data['unitPriceOfSingleton'+i+5]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+6+'" value="'+data['timeOfSingleton'+i+6]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+6+'" value="'+data['unitPriceOfSingleton'+i+6]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+7+'" value="'+data['timeOfSingleton'+i+7]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+7+'" value="'+data['unitPriceOfSingleton'+i+7]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+8+'" value="'+data['timeOfSingleton'+i+8]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+8+'" value="'+data['unitPriceOfSingleton'+i+8]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+9+'" value="'+data['timeOfSingleton'+i+9]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+9+'" value="'+data['unitPriceOfSingleton'+i+9]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+10+'" value="'+data['timeOfSingleton'+i+10]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+10+'" value="'+data['unitPriceOfSingleton'+i+10]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="timeOfSingleton'+i+11+'" value="'+data['timeOfSingleton'+i+11]+'"></td>'+
					'<td><input type="text" class="form-control input-sm input-change" name="unitPriceOfSingleton'+i+11+'" value="'+data['unitPriceOfSingleton'+i+11]+'"></td>'+
					'<td><input type="text" class="form-control input-sm" readonly="readonly" name="processTotalFee'+i+'" value="'+data['processTotalFee'+i]+'"></td>'+
					'<td><input type="text" class="form-control input-sm" readonly="readonly" name="unitTotalPrice'+i+'" value="'+data['unitTotalPrice'+i]+'"></td>'+
					'</tr>'+
					'<tr>'+
					'<td>业务员核价</td>'+
					'<td>1</td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'<td><input type="text" class="form-control input-sm"></td>'+
					'</tr>';
			}

			htmlTex+='<tr>'+
				'<td rowspan="3" colspan="2">其它费用</td>'+
				'<td rowspan="2">供应商报价</td>'+
				'<td colspan="3">管理费</td>'+
				'<td colspan="2">利润</td>'+
				'<td colspan="2">包装费</td>'+
				'<td colspan="2">运输费</td>'+
				'<td colspan="2">合成费</td>'+
				'<td colspan="2">设计费</td>'+
				'<td colspan="3">配套件</td>'+
				'<td colspan="4">单套(件)总价</td>'+
				'<td colspan="4">总价</td>'+
				'<td colspan="5"></td>'+
				'</tr>'+
				'<tr>'+
				'<td colspan="2"><input type="text" class="form-control input-sm input-change" name="managementFeeRate" value="'+data.managementFeeRate+'"></td>'+
				'<td>%</td>'+
				'<td><input type="text" class="form-control input-sm input-change" name="profitsRate" value="'+data.profitsRate+'"></td>'+
				'<td>%</td>'+
				'<td><input type="text" class="form-control input-sm input-change" name="packingFeeRate" value="'+data.packingFeeRate+'"></td>'+
				'<td>%</td>'+
				'<td><input type="text" class="form-control input-sm input-change" name="transportFeeRate" value="'+data.transportFeeRate+'"></td>'+
				'<td>%</td>'+
				'<td><input type="text" class="form-control input-sm input-change" name="compositionFeeRate" value="'+data.compositionFeeRate+'"></td>'+
				'<td>%</td>'+
				'<td><input type="text" class="form-control input-sm input-change" name="designFeeRate" value="'+data.designFeeRate+'"></td>'+
				'<td>%</td>'+
				'<td colspan="2"><input type="text" class="form-control input-sm input-change" name="kitFee" value="'+data.kitFee+'"></td>'+
				'<td>元</td>'+
				'<td colspan="3"><input type="text" class="form-control input-sm" readonly="readonly" name="singleKitTotalPrice" value="'+data.singleKitTotalPrice+'"></td>'+
				'<td>元</td>'+
				'<td colspan="3"><input type="text" class="form-control input-sm" readonly="readonly" name="totalPrice" value="'+data.totalPrice+'"></td>'+
				'<td>元</td>'+
				'<td colspan="2">承诺交货期</td>'+
				'<td colspan="3">2010/12/22</td>'+
				'</tr>'+
				'<tr>'+
				'<td>业务员核价</td>'+
				'<td colspan="2"><input type="text" class="form-control input-sm"></td><td>%</td>'+
				'<td><input type="text" class="form-control input-sm"></td><td>%</td>'+
				'<td><input type="text" class="form-control input-sm"></td><td>%</td>'+
				'<td><input type="text" class="form-control input-sm"></td><td>%</td>'+
				'<td><input type="text" class="form-control input-sm"></td><td>%</td>'+
				'<td><input type="text" class="form-control input-sm"></td><td>%</td>'+
				'<td colspan="2"><input type="text" class="form-control input-sm"></td><td>元</td>'+
				'<td colspan="3"><input type="text" class="form-control input-sm"></td><td>元</td>'+
				'<td colspan="3"><input type="text" class="form-control input-sm"></td><td>元</td>'+
				'<td colspan="5">工装模具用此单另报</td>'+
				'</tr>'+
				'</tbody>'+
				'</table>'+
				'<div class="panel mt12">'+
				'<div class="panel-heading clearfix">'+
				'<span class="mg-margin-right">报价说明：</span>'+
				'</div>'+
				'<div class="panel-body">'+
				'<div class="row">'+
				'<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">'+
				'报价说明：每套剪切模由剪切上模和剪切下模各一件组成，孔由线切割完成，确保精度。<br/>'+
				'报价人：张 ××     日期：2010年12月*日    联系电话： <br/>'+
				'报价单位：<br/>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="panel">'+
				'<div class="panel-heading clearfix">'+
				'<span class="mg-margin-right">核价说明：</span>'+
				'</div>'+
				'<div class="panel-body">'+
				'<div class="row">'+
				'<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">'+
				'核价说明原因描述描述描述描述描述描述描述描述描述描述。<br/>'+
				'采购员：'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="panel">'+
				'<div class="panel-heading clearfix">'+
				'<span class="mg-margin-right">备注：</span>'+
				'</div>'+
				'<div class="panel-body">'+
				'<div class="row">'+
				'<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">'+
				'1、计量单位：费用：元；重量：公斤；工时：小时。<br/>'+
				'2、本核价表由供应商按此格式填写，有底纹的格子由采购员填写。<br/>'+
				'3、当某种零件加工工序超过10道时，列出主要的10道工序分别报价，其余工序合并在第11道工序内报价。<br/>'+
				'4、组件的零件超过10种时，列出主要的10种零件并允许简化报价，工序名称、单件工时、工序单价省略，其它内容必须填写；其余零件归在第11种（其它零件）内；当采用简化报价时，加工费合计一栏的程序计算公式不起作用，其报价、核价必须手工录入。'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="panel">'+
				'<div class="panel-heading clearfix">'+
				'<span class="mg-margin-right">核价方式说明：</span>'+
				'</div>'+
				'<div class="panel-body">'+
				'<div class="row">'+
				'<div class="re line-32" style="margin-left:35px; color:#666; font-size:12px;">'+
				'1、全部认可：全部认可供应商报价。 <br/>'+
				'2、局部修改：个别数据认为有误，手工输入数据修改（该单元格计算公式就删除了）。  <br/>'+
				'3、总体下浮：材料费、工时费按输入的比例总体下浮。   <br/>'+
				'4、局部下浮：可多选或单选下浮全部材料重量、下浮全部材料单价、下浮全部工时小时、下浮全部工时单价，输入下浮比例。 <br/>'+
				'5、总价修改：仅对总价手工输入修改。 <br/>'+
				'6、其它费用的核价，数据量小，手工输入管理费率、利润率、包装费率、运输费率等。'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>';
			$('#detailForm').html(htmlTex);
			calculate();
		}
	});
}
/*$(function() {
 //集体调用
 $("#detailForm input").each(function(){
 $(this).setDefauleValue();
 });
 //单个调用
 //$("#key").setDefauleValue();
 })*/
function saveDetail(){
	$("#detailForm input").each(function(){
		var value=$(this).val();
		if(value.length==0){
			$(this).val("0.00");
		}
	});
	var tmp = {};
	$('#datatable-requestItemdetailTable').find('tr td input').each(function () {
		var $this = $(this);
		var name = $this.attr('name');
		if(!name) return true;
		tmp[name] = $this.val();
	});
	itemDetailList[rfqRequestItemId] = tmp;
	$('input[data-name="unitPrice"]').eq(rfqIndex).val($('input[name="totalPrice"]').val());
	// $('#detailForm').ajaxSubmit({
	// 	type:"POST",
	// 	dataType:"json/application",
	// 	url:ctx+"/rfqQuotationItemDetail/save",
	// 	success:function(data){
	// 		alert(data);
	// 	}
	// })
}



