$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

	//附件
	var types=[".jpg",".jpeg",".gif",".png",".doc",".docx",".xls",".xlsx",".pdf"];
	var maxSize=104857600;//100M，单位:B

    if(navigator.appName == "Microsoft Internet Explorer"){
        $("#myModa2").on('show.zui.modal', function () {
            //初始化flash插件
            initPlupload2(maxSize,types);
            //关闭时清理页面
            initEvent();
        });
    }else {
        //初始化flash插件
        initPlupload(maxSize,types);
        //关闭时清理页面
        initEvent();
    }

	var requestId= $('#requestId').val();
	var supplierNum= $('#supplierCode').val();
	$('#myHistory').click(function(){

    RFQ.jqGrid('#jqGrid').init({
        url: ctx+'/rfqLaunchedSeveralPrice/history',
        colModel: [
			{
				label: '询价单号',
				name: 'unifiedRfqNum',
				align:'center',
				hidden:true,
				width: 80
			},
            {
                label: '询价单号',
                name: 'ouRfqNum',
                align:'center',
                width: 80
            },
			{
				label: '报价单号',
				name: 'id',
				hidden: true,
				align:'center',
				width: 20
			},
			{
				label: '含税总价',
				name: 'subtotalTaxed',
				align:'center',
				width: 60
			},
			{
				label: '报价有效期',
				name: 'quotationValidDate',
				align:'center',
				width: 80
			},
			{
				label: '报价人',
				name: 'quotationInputUsername',
				align:'center',
				width: 60
			},
			{
				label: '报价时间',
				name: 'quotationDate',
				align:'center',
				width: 110
			},
			{
				label: '操作',
				name: 'id',
				align: 'center',
				width: 50,
				index: 'ID',
				formatter: actFormatter
			}

		],
        rowNum: 20,//每页显示记录数
        rowList: [20, 30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
		postData:{"requestId":requestId},
        width: 980,
        sortname: 'QUOTATION_DATE',
        sortorder: 'DESC',
        pager: "#jqGridPager"
    });
		$("#jqGrid").jqGrid('resizeGrid');
	})

	//$('#searchKey').click(function () {
	//	var key=$('#soText').val();
    //
	//	$.post(ctx+"/rfqQuotationHisotry/searchRfqQuotationItem",{requestId:'826417'}, function(data) {
	//		var data= eval("("+data+")");
	//		var htmlText='<div class="row">'+
	//		'<table class="table datatable table-bordered align-md" id="requestItemData">'+
	//			'<thead>'+
	//			'<tr>'+
	//			'<th data-width="80">序号</th>'+
	//			'<th>物料信息 <a class="btn btn-danger btn-mini" id="all-zk" href="javascript:allZk();">全部展开</a></th>'+
	//			'<th data-width="80">采购数量</th>'+
	//			'<th class="flex-col" data-width="80">状态</th>'+
	//			'<th class="flex-col" data-width="80">可供量</th>'+
	//			'<th class="flex-col" data-width="120">*未税单价(元)</th>'+
	//			'<th class="flex-col" data-width="120">'+
	//			'<div>*税率</div>'+
	//			'<div class="btn-group">'+
	//			'<button class="btn btn-hui dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">'+
	//			'6%<span class="caret"></span>'+
	//			'</button>'+
	//			'<ul class="dropdown-menu" id="tax" name="tax" role="menu" aria-labelledby="dropdownMenu1">'+
	//			'<li><a data-value="6">6%</a></li>'+
	//			'<li><a data-value="7">7%</a></li>'+
	//			'<li><a data-value="10">10%</a></li>'+
	//			'<li><a data-value="11">11%</a></li>'+
	//			'<li><a data-value="13">13%</a></li>'+
	//			'<li><a data-value="17">17%</a></li>'+
	//			'</ul>'+
	//			'</div>'+
	//			'</th>'+
	//			'<th class="flex-col" data-width="100">含税单价</th>'+
	//			'<th class="flex-col" data-width="100">未税总价</th>'+
	//			'<th class="flex-col" data-width="100">含税总价</th>'+
	//			'<th class="flex-col" data-width="100">*承诺交货期</th>'+
	//			'<th class="flex-col" data-width="100">替代型规</th>'+
	//			'<th class="flex-col" data-width="100">*制造商</th>'+
	//			'<th class="flex-col" data-width="100">*产地</th>'+
	//			'<th class="flex-col" data-width="120">*上一级供应渠道</th>'+
	//			'<th class="flex-col" data-width="100">运输方式</th>'+
	//			'<th class="flex-col" data-width="100">发货地点</th>'+
	//			'<th class="flex-col" data-width="100">材质</th>'+
	//			'<th class="flex-col" data-width="100">单重</th>'+
	//			'<th class="flex-col" data-width="100">质保期</th>'+
	//			'<th class="flex-col" data-width="100">报价备注</th>'+
	//			'</tr>'+
	//			'</thead>'+
	//			'<tbody>"';
    //
    //
	//		$.each(data, function(index, item) {
	//			htmlText+='<tr>'+
	//			'<td>111</td>'+
	//			'<td> 代码：111 物料名称：111 规格型号：111'+
	//			'<a href="javascript:;" class="zkBtn icon icon-chevron-down"></a> <div class="hidden"> 描述：111 计量单位：111 生产厂家(品牌)：111'+
	//			'</div>'+
	//			'</td>'+
	//			'<td>111'+
	//			'<input class="form-control" data-name="rfqQuotationItemList[1].requestAmount" value="111" type="hidden" placeholder="">'+
	//				'</td>'+
	//				'<td>未报价:已报价'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].requestId" value="111" type="hidden" placeholder="">'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].requestItemId" value="${itemVo.requestItemIdOrg}" type="hidden" placeholder="">'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].seq" value="111" type="hidden" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control js-change" data-name="rfqQuotationItemList[1].availableCount" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control js-change" data-name="rfqQuotationItemList[1].unitPrice" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control js-change" data-name="rfqQuotationItemList[1].tax" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].unitPriceTaxed" value="111" readonly="readonly" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].subtotal" value="${itemVo.subtotal}" type="text" readonly="readonly" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].subtotalTaxed" value="111" type="text" readonly="readonly" placeholder="" onkeyup="countTotalPrice()">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].availableDeliveryDate" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].substitutableSpec" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].manufactory" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].productPlace" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].upstreamSuppliers" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].transType" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].deliveryLocation" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].material" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].singleWeight" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].timeOfWarranty" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'<td>'+
	//				'<input class="form-control" data-name="rfqQuotationItemList[1].memo" value="111" type="text" placeholder="">'+
	//				'</td>'+
	//				'</tr>';
	//		});
    //
	//		htmlText+='</tbody>'+
	//		'</table>'+
	//		'</div>';
	//	});
    //
	//})


	$('#payCash').click(function(){
		$('#payCashForm').ajaxSubmit({
			success: function (data) {
				RFQ.info(data.rspmsg);
				location.reload();
			}
		})
	})

    //放弃报价
    $('#giveUpQuo').on('click','.give-up',function(){
        $('#giveUpQuo').ajaxSubmit({
            type: 'post', // 提交方式 get/post
            url: ctx+'/rfqQuotation/giveUpQuotation', // 需要提交的 url
            dataType: 'json',
            success: function (data) {
                if(data.rspcod != 200){
                    RFQ.error(data.rspmsg);
                    return;
                }else{
                    location.href=ctx+"/rfqQuotationHisotry/getRfqQuotationHistory?requestId="+data.rspmsg;
                }
            },
            error : function(XmlHttpRequest, textStatus, errorThrown){
                RFQ.error('出错了！');
            }
        });
    });
});

function resetValues(){
	$("#giveUpReason option").eq(0).attr("selected",true);
	$("#giveUpMemo").val("");
}
function actFormatter(cellvalue, options, rawObject) {
	if (rawObject.id!=undefined && rawObject.id != "") {
		var examinationBtn = '<span class="btn btn-sm btn-warning" onclick="viewDetails(\'' + rawObject.id + '\' , \'' + rawObject.type + '\')"><i class="icon icon-check-sign"></i>查看</span>';
	} else {
		return "";
	}
	return examinationBtn;
};
function viewDetails(id,type) {
	var aa = window.open();
	var url = ctx+"/rfqQuotation/getQuotationDetail?id=" + id + "&type=" +type;
	aa.location.href = url ; 
}
