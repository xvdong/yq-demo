$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var $jqGrid = $("#jqGrid");
    RFQ.jqGrid("#jqGrid").init({
        url: ctx+'/rfqRequest/queryRfqQuotationItemList',
		postData:{rfqMethod:$("#rfqMethod").val()},
        colModel: [
                {
                    label: '供应商',
                    name: 'supplierId',
                    hidden:true
                },
				{
					label: '询单序号',
					name: 'id',
					index: 'id',
					width: 80,
					key: true,
					hidden:true
				},
				{
					label: '物料代码',
					name: 'materialNo',
					index: 'MATERIAL_NO',
					align:'center',
					width: 80
				},
				{
					label: '物料名称',
					name: 'materialName',
					index: 'MATERIAL_NAME',
					align:'center',
					width: 80
				},
				{
					label: '询价单号',
					name: 'unifiedRfqNum',
					index: 'UNIFIED_RFQ_NUM',
					align:'center',
					hidden:true,
					width: 80,
					formatter:selectFormatter
				},
            {
                label: '询价单号',
                name: 'ouRfqNum',
                index: 'OU_RFQ_NUM',
                align:'center',
                width: 80,
                formatter:selectFormatter
            },
				{
					label: '询单标题',
					name: 'title',
					index: 'TITLE',
					align:'center',
					width: 80
				},
				{
					label: '采购商',
					name: 'ouName',
					index: 'OU_NAME',
					align:'center',
					width: 80
				},
				{
					label: '发布人',
					name: 'issueUsername',
					index: 'ISSUE_USERNAME',
					align:'center',
					width: 60
				},
				{
					label: '发布日期',
					name: 'issueDate',
					index: 'ISSUE_DATE',
					align:'center',
					width: 80
				},
				{
					label: '报价起止时间',
					name: 'startDate',
					index: 'START_DATE',
					align:'center',
					width: 90,
					formatter:startEndTimeFormatter
				},
				{
					label: '状态',
					name: 'status',
					index: 'TYPE',
					align:'center',
					width: 60,
					formatter:formatRfqItemStatus
				},
				{
					width: 80,
					label: '操作',
					name: 'id',
					index: 'id',
					sortable:false,
					formatter: actFormatter
				}
        ],
		sortname: 'MATERIAL_NO',
        sortorder: 'desc',
        pager: "#jqGridPager",
    });


	function formatRfqItemStatus(cellvalue, options, rawObject){
		var rval = '';
		switch (cellvalue) {
			case '0':
				if('PSCS'==$("#rfqMethod").val()){
					rval = '未报价';
				}else if('DAC'==$("#rfqMethod").val()){
					rval = '待竞价';
				}
				break;
			case '1':
				if('PSCS'==$("#rfqMethod").val()){
					rval = '已报价';
				}else if('DAC'==$("#rfqMethod").val()){
					rval = '已竞价';
				}
				break;
			default:
				if('PSCS'==$("#rfqMethod").val()){
					rval = '未报价';
				}else if('DAC'==$("#rfqMethod").val()){
					rval = '待竞价';
				}
				break;
		}
		return rval;
	}

    function actFormatter(cellvalue, options, rawObject) {
        var rtype=rawObject.status;
		var rfqMethod=$("#rfqMethod").val();
		var detailBtn;
        // 报价页面
		if('PSCS'==rfqMethod){
			detailBtn = '<span class="ml5"><a class="btn btn-sm btn-warning js-check re"  href="javascript:rfqRequestQuotation(\'' + rawObject.id + '\',\'' + rawObject.supplierId+ '\')" ><i class="icon icon-yen"></i>报价</a></span>';
		}else if('DAC'==rfqMethod) {
			detailBtn = '<span class="ml5"><a class="btn btn-sm btn-warning js-check re"  href="javascript:rfqRequestQuotationTry(\'' + rawObject.id+ '\')" ><i class="icon icon-yen"></i>竞价</a></span>';
		}
		if(rtype=="1"&& 'PSCS'==rfqMethod){ //报价详情
            detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info  re" href="javascript:rfqRequestHistory(\'' + rawObject.id + '\',\'' + rawObject.supplierId+ '\')"><i class="icon icon-zoom-in"></i> 查看</a></span>';
        }
		if(rtype=="1"&& 'DAC'==rfqMethod){ //报价详情
			detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info  re" href="javascript:rfqRequestDetail(\'' + rawObject.id + '\')"><i class="icon icon-yen"></i> 竞价</a></span>';
		}
        return detailBtn;
    };

	var $myTab = $('#myTab');
	//搜索按钮点击
	$('button.btnSearch').on('click',function(){
		jqGridReload();
	});

	function jqGridReload(){

		var formId= 'searchFormTab';
		var params = {};
		var postData = $.extend(form2Json(formId),params);
		$jqGrid.jqGrid('setGridParam',{postData: postData,page: 1}).trigger("reloadGrid");
	}

});

function rfqMethodType(){
	if('PSCS'==$("#rfqMethod").val()){
		$("#jqGrid").jqGrid('setLabel', 5,"询价单号");
		$("#jqGrid").jqGrid('setLabel', 6,"询价标题");
		$("#jqGrid").jqGrid('setLabel', 10,"询价起止时间");
	}else if('DAC'==$("#rfqMethod").val()){
		$("#jqGrid").jqGrid('setLabel', 5,"竞价单号");
		$("#jqGrid").jqGrid('setLabel', 6,"竞价标题");
		$("#jqGrid").jqGrid('setLabel', 10,"竞价起止时间");
	}
}

/**
 * 起止时间格式化
 */
function startEndTimeFormatter(cellvalue, options, rawObject) {
	var endDate = rawObject.quotationEndDate;
	return cellvalue +'~\n'+ endDate;
}

/**
 * 询单编号链接跳转
 */
function selectFormatter(cellvalue, options, rawObject) {
	var id = rawObject.id;
	return "<a class='blue' href='"+ctx+"/rfqRequest/rfqRequestDetail?id="+id+"' target='_blank' >"+cellvalue+"</a>";
}
/**
 * 报价明细
 */
function rfqRequestHistory (id,supplierId){
    location.href = ctx+"/rfqQuotationHisotry/getRfqQuotationHistory?requestId=" + id+"&supplierId="+supplierId;
}
/**
 * 报价
 */
function rfqRequestQuotation(id,supplierId) {
		location.href = ctx+"/rfqQuotation/getRfqQuotation?requestId=" + id+"&supplierId="+supplierId+"&rmethod=PSCS";

}

function rfqRequestQuotationTry(id){
	window.sessionStorage.designateResult_tax="NoStart";
	location.href = ctx+"/dacReverseAuction/reverseAuctionDetail?id=" + id;
}



function rfqRequestDetail(id){
	/*location.href = ctx+"/dacReverseAuction/reverseAuctionDetail?id=" + id;*/
	$.ajax({
		url:ctx+"/rfqRequest/queryRules",
		data:{requestId:id,},
		type:'POST',
		success:function(data){
			var rule=data.rules;
			if('1'==rule){
				location.href = ctx+"/dacQuotationTotal/init?requestId="+id;
			}else if('0'==rule){
				location.href = ctx+"/dacQuotationSingle/init?requestId="+id;
			}else{
				$.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
			}
		}
	});
}