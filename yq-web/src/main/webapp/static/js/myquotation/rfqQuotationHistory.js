$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var requestId= $('#requestId').val();
	var supplierNum =$('#supplierNum').val();
    RFQ.jqGrid('#jqGrid').init({
        url: ctx+'/rfqQuotation/rfqQuotationHistoryItemList',
        colModel: [
				{
					label: '供应商U代码',
					name: 'supplierNum',
					index: 'SUPPLIER_NUM',
					width: 100
				},
				{
					label: '供应商名称',
					name: 'supplierName',
					index: 'SUPPLIER_NAME',
					width: 80
				},
				{
					label: '保证金',
					name: 'assureFlag',
					index: 'ASSURE_FLAG',
					width: 80,
					formatter: function(cellValue){
						if( cellValue > 0 ){
							return '已缴纳' ;
						}else {
							return '未缴纳' ;
						}
					}
				},
				{
					label: '报价时间',
					name: 'quotationDate',
					index: 'QUOTATION_DATE',
					width: 120
				},
				{
					label: '报价情况',
					name: 'quotationMemo',
					index: 'QUOTATION_MEMO',
					width: 80,
					formatter: function(cellValue){
						if( 2 == cellValue || 3 == cellValue){
							return '已报价' ;
						}else {
							return '未报价' ;
						}
					}
				},
				{
					label: '报价金额(元)',
					name: 'subtotalTaxed',
					index: 'SUBTOTAL_TAXED',
					width: 80
				},
				{
					label: 'IP地址',
					name: 'ip',
					index: 'IP',
					width: 80
				},
				{
					label: '放弃原因',
					name: 'quotationGiveUpMemo',
					index: 'QUOTATION_GIVE_UP_MEMO',
					width: 80
				},
				{
					width: 80,
					label: '操作',
					name: 'id',
					index: 'id',
					sortable:false,
					formatter: actFormatter
				}
        ],
		postData:{"requestId":requestId,"supplierNum":supplierNum},
        width: 980,
        sortname: 'SUPPLIER_NUM',
        sortorder: 'asc',
        pager: "#jqGridPager"
    });

	function actFormatter(cellvalue, options, rawObject) {
		var detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info re js-check" data-action="rfqRequestDetail(\'' + requestId + '\')" href="javascript:;"><i class="icon icon-zoom-in"></i> 查看</a></span>';
		return detailBtn;
	};

	function rfqRequestDetail(id) {
		location.href = ctx+"/rfqQuotation/rfqQuotationDetail?id=" + id;
	}
});

