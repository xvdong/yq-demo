/**
 * Created by Jhony Zhang on 2016-05-10.
 */
/**
 * 将表单数据转为json
 * @param id from表单id
 * @param addParam 还需额外添加的参数（json格式）
 */
function form2Json(id, addParam) {
    // 不止为何直接通过from的id序列化返回为空，暂+class处理
    var arr = $("#" + id+" .form-control").not(".phcolor").serializeArray();
    var jsonStr = "";
    jsonStr += '{';
    if (typeof(addParam) != 'undefined' && addParam != '') {
        jsonStr += addParam;
    }
    for (var i = 0; i < arr.length; i++) {
        if ($.trim(arr[i].value) != null && $.trim(arr[i].value) != '') {
            jsonStr += '"' + arr[i].name + '":"' + $.trim(arr[i].value) + '",';
        } else {
            jsonStr += '"' + arr[i].name + '":"",';
        }
    }
    if (jsonStr.length > 1) {
        jsonStr = jsonStr.substring(0, (jsonStr.length - 1));
    }
    jsonStr += '}';
    var json = JSON.parse(jsonStr);
    return json;
}



/**
 * 字段显示超长处理，若超长使用bootstrap popover显示
 * @param value
 * @param cutLength
 * @returns {string}
 */
function lengthFormatter(value, cutLength) {
    var content = "";
    var abValue = value;
    if (value != null && value != '') {
        if (value.length > cutLength) {
            abValue = abValue.substr(0, cutLength);
            abValue = abValue + "..";
            content = "<div onmouseover=\"$(this).popover('show');\" onmouseout=\"$(this).popover('hide');\" data-toggle='popover' data-trigger='click|hover' data-container='body' data-content='" + value + "' data-placement='top'>" + abValue + "</div>";
        } else {
            content = value;
        }
    } else {
        content = "";
    }
    return content;
}

/**
 * ajax提交表单数据
 */
function saveForm(formId) {
    $('#' + formId).ajaxSubmit({
        success: function (data) {
            if (data.status == 'success') {
                $('#modalSuccess').modal('show');
            } else {
                $.zui.messager.show('操作失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    });
    return false; //不自动提交表单
}

//兼容IE8的trim->正则表达式
function trimPro(s){
    String.prototype.trim = function()
    {
        return this.replace(/(^\s*)|(\s*$)/g, "");
    }
    return s.trim();
}