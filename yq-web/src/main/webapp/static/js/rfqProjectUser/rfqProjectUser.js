$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	$('#prouser').click(function(){
		getAttData("#jqGridphone","#jqGridPagerphone");

	});

	function getAttData(grid,page){
		$(grid).jqGrid({
			url: '/rfqProjectUserNew/rfqProjectUserListNew',
			datatype: 'json',
			mtype: 'POST',
			colModel: [
				{
					label: '序号',
					name: 'puId',
					index: 'PU_ID',
					width: 20,
					key: true,
				},
				{
					label: '工号',
					name: 'jobCode',
					index: 'JOB_CODE',
					width: 20,
				},
				{
					label: '姓名',
					name: 'userName',
					index: 'USER_NAME',
					width: 20,
				},
				{
					label: '所属科室',
					name: 'userOffice',
					index: 'USER_OFFICE',
					width: 20,
				},
				{
					label: '职位',
					name: 'userTitle',
					index: 'USER_TITLE',
					width: 20,
				}
			],
			jsonReader: { //映射server端返回的字段
				root: "list",
				rows: "list",
				page: "pageNum",
				total: "pages",
				records: "total"
			},
			prmNames: {
				id: 'puId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
			},
			viewrecords: true, // show the current page, data rang and total records on the toolbar
			multiselect: true,
			width: 810,
			height: "100%",
			rowNum: 10,
			rowList: [10, 20, 30],
			sortname: 'PU_ID',
			sortorder: 'asc',
			pager: page,
			//caption: "询报价_小组成员列表"
		});
	}


    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqProjectUserDetail(\'' + rawObject.puId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqProjectUser(\'' + rawObject.puId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqProjectUser(\'' + rawObject.puId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqProjectUser").click(function () {
        location.href = "editRfqProjectUser?oper=add"
    });
});

function rfqProjectUserDetail(puId) {
    location.href = "rfqProjectUserDetail?puId=" + puId;
}

function editRfqProjectUser(puId) {
    location.href = "editRfqProjectUser?oper=edit&puId=" + puId;
}

function deleteRfqProjectUser(puId) {
    location.href = "delRfqProjectUser?puId=" + puId;
}