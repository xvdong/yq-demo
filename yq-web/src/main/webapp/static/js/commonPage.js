function totalPageFun(totalCount, recPerPage) {
    return (totalCount % recPerPage == 0) ? (Math
        .floor(totalCount / recPerPage)) : (Math
        .floor((totalCount / recPerPage)) + 1);
}

function showPageFun(pageNo, pageSize, totalCount, showFunName, divId) {
    var pageNoL = parseInt(pageNo);
    var totalCountL = parseInt(totalCount);
    var totalPage = totalPageFun(totalCountL, pageSize);

    var pageHtml = "";
    if (pageNoL > totalPage)
        pageNoL = totalPage;
    if (pageNoL < 1)
        pageNoL = 1;
    if (totalPage < 1)
        totalPage = 1;
    var pageUrl = "javascript:" + showFunName;
    pageHtml = "<div class=\"fenye\">\n";
    if (pageNoL > 1 && pageNoL <= totalPage) {
        pageHtml = pageHtml
            + "<span><a href=\"" + pageUrl + "("
            + (pageNoL - 1)
            + ");\" title=\"上一页\" class=\"fy-2\">&lt;</a></span>\n";
    } else {
        pageHtml = pageHtml
            + "<span><a href=\"#\" title=\"上一页\" class=\"fy-1\">&lt;</a></span>\n";
    }
    var beginPage = 1;
    var endPage = totalPage;
    if (totalPage == 0) {
        endPage = 1;
    }
    if (totalPage > 1 && pageNoL < totalPage) {
        pageHtml = pageHtml + "<span><a href=\"" + pageUrl + "("
            + (pageNoL + 1) + ");\" title=\"下一页\">&gt;</a></span>";
    } else {
        pageHtml = pageHtml + "<span><a href=\"#\" title=\"下一页\" class=\"fy-1\">&gt;</a></span>";
    }
    pageHtml = pageHtml + "</div>";
    $("#" + divId).append(pageHtml);
}
