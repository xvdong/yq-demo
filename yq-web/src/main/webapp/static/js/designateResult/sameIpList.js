$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    $("#jqGrid1").jqGrid({
        url: 'querySupplierNotQuotedPriceDetail',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        postData : {unifiedRfqNum:$("#unifiedRfqNum").val()},
        colModel: [
            {
                label: '询价单号',
                name: 'unifiedRfqNum',
                index:'UNIFIED_RFQ_NUM',
                align:'center',
                hidden:true,
                width: 80,
                formatter: function(cellvalue){
                    return '<a href="javascript:void(\'' + 0 + '\')" class="blue" target="_blank" >'+ cellvalue +'</a>';
                }
            },
            {
                label: '询价单号',
                name: 'ouRfqNum',
                index:'OU_RFQ_NUM',
                align:'center',
                width: 80,
                formatter: function(cellvalue){
                    return '<a href="javascript:void(\'' + 0 + '\')" class="blue" target="_blank" >'+ cellvalue +'</a>';
                }
            },
            {
                label: '询单标题',
                name: 'title',
                index: 'TITLE',
                align:'center',
                width: 80
            },
            {
                label: '供应商U代码',
                name: 'supplierCode',
                index: 'SUPPLIER_CODE',
                align:'center',
                width: 60
            },
            {
                label: '供应商名称',
                name: 'supplierName',
                index: 'SUPPLIER_NAME',
                align:'center',
                width: 80
            },
            {
                label: '联系人',
                name: 'linkmanName',
                index: 'LINKMAN_NAME',
                align:'center',
                width: 80
            },
            {
                label: '联系电话',
                name: 'linkmanTelphone',
                index: 'LINKMAN_TELPHONE',
                align:'center',
                width: 80
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 980,
        height: "100%",
        rowNum: 10,
        sortname: 'id',
        sortorder: 'asc'
    });

    $("#jqGrid2").jqGrid({
        url: 'queryIPIdenticalDetail',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        postData : {unifiedRfqNum:$("#unifiedRfqNum").val()},
        colModel: [
            {
                label: '供应商U代码',
                name: 'supplierCode',
                index:'SUPPLIER_CODE',
                align:'center',
                width: 55,
                formatter: function(cellvalue){
                    return '<a href="javascript:void(\'' + 0 + '\')" class="blue" target="_blank" >'+ cellvalue +'</a>';
                }
            },
            {
                label: '供应商名称',
                name: 'supplierName',
                index: 'SUPPLIER_NAME',
                align:'center',
                width: 70
            },
            {
                label: '保证金',
                name: 'assureFlag',
                index: 'ASSURE_FLAG',
                align:'center',
                width: 30,
                formatter: function(cellValue){
                    if( cellValue > 0 ){
                        return '已缴纳' ;
                    }else {
                        return '未缴纳' ;
                    }
                }
            },
            {
                label: '报价时间',
                name: 'quotationDate',
                index: 'QUOTATION_DATE',
                align:'center',
                width: 75,
                formatter: actFormatter1 //将内容改变成自己的格式,见下面
            },
            {
                label: '报价情况',
                name: 'status',
                index: 'STATUS',
                align:'center',
                width: 35,
                formatter: function(cellValue){
                    if( 2 == cellValue || 3 == cellValue){
                        return '已报价' ;
                    }else if (4 == cellValue) {
                        return '已放弃' ;
                    } else {
                        return '未报价' ;
                    }
                }
            },
            {
                label: '报价金额(元)',
                name: 'subtotalTaxed',
                index: 'SUBTOTAL_TAXED',
                align:'center',
                width: 46,
                formatter: actFormatter2 //将内容改变成自己的格式,见下面
            },
            {
                label: 'IP地址',
                name: 'ip',
                index: 'IP',
                align:'center',
                width: 50
            }
            //{
            //    label: '放弃原因',
            //    name: 'quotationGiveUpMemo',
            //    index: 'QUOTATION_GIVE_UP_MEMO',
            //    align:'center',
            //    width: 85
            //},
            //{
            //    label: '操作',
            //    name: 'supplierCode',
            //    index: 'SUPPLIER_CODE',
            //    align:'center',
            //    width: 40,
            //    formatter: actFormatter
            //},
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 980,
        height: "100%",
        rowNum: 10,
        sortname: 'id',
        sortorder: 'asc'
    });


    $("#jqGrid1").jqGrid('setLabel', 0,"序号");
    $("#jqGrid2").jqGrid('setLabel', 0,"序号");

    //function actFormatter(cellvalue, options, rawObject) {
    //    var examinationBtn = '<a class="btn btn-sm btn-info" href="javascript:viewDetails(\'' + rawObject.supplierCode + '\')"><i class="icon icon-zoom-in"></i> 查看</a></span></span>';
    //    return examinationBtn;
    //};

    ////TODO 查看供应商详情，页面未出，等国庆后在完成
    //function viewDetails(supplierCode) {
    //    location.href = ctx+"/rfqRequest/delRfqRequest?id=" + supplierCode;
    //}

});
function actFormatter1(cellvalue, options, rawObject) {
   if (rawObject.status!= 2 && rawObject.status!= 3 && rawObject.status!= 4){//状态为未保价的 报价时间为空
       return "";
   }else {
       return cellvalue;
   }
};
function actFormatter2(cellvalue, options, rawObject) {
    if (rawObject.status!= 2 && rawObject.status!= 3 && rawObject.status!= 4){//状态为未保价的 报价金额为空
        return "";
    }else {
        return cellvalue;
    }
};
