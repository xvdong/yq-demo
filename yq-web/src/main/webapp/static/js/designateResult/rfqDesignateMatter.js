/**
 * Created by Administrator on 2016/11/11.
 */
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

function createGys(){
    var requestId = $('#requestId').val();
    var htmlText="";
    var htmlSupplier="";
    var htmlDetail="";
    var zhongbiaoMoney=0.0;
    var totalMoney=0.0;
    $.post(ctx+"/rfqQuotationResult/testGYSQuotation",{id:requestId}, function(data) {
        var data= eval("("+data+")");

        $.each(data, function(index, item) {
            htmlSupplier += '<li class="';
            if (index == 0) {
                htmlSupplier += 'active';
            }
            htmlSupplier += '"><a href="#tabgs' + index + '" data-toggle="tab" title="' + item.supplierName + '">' + item.supplierName + '</a></li>';
        });

        $.each(data, function(index, item) {
            htmlDetail += '<div class="tab-pane example';
            if(index==0){
                htmlDetail+=' active';
            }
            htmlDetail+='" id="tabgs'+index+'">' +
                '<input type="hidden" name="rfqQuotationNoticeList['+index+'].supplierCode" value="'+item.supplierNum+'" />'+
                '<textarea id="contentSimple'+(index+2)+'" name="rfqQuotationNoticeList['+index+'].memoDesc"'+
                'class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">'+item.supplierName+',恭喜您中标！</textarea></div>';
        });

        $('#myTabZB').html(htmlSupplier).tab();
        $('#tabContent').html(htmlDetail);

        $('textarea.kindeditorSimple').each(function () {
            KindEditor.create('#'+this.id, {
                resizeType: 1, allowPreviewEmoticons: false, allowImageUpload: false, items: [] ,afterBlur: function(){this.sync();}
            });
        });

        $.each(data, function(index, item) {
            $('#zhongbiaogys').html(data.length);
            $('#totalMoney').html(item.totalMoney.toFixed(2));
            htmlText +='<table class="table table-bordered align-md">'+
                '<thead>'+
                '<div class="thead-info">'+
                '<div class="pull-left company">'+
                '<h4 class="pull-left"><a href="javascript:;" class="blue" target="_blank">'+item.supplierName+
                '</a>&nbsp;&nbsp;</h4>' +
                '<a class="c999 pull-left line-32" href="'+ctx+'/rfqQuotationHisotry/getRfqQuotationHistory?requestId='+requestId+'&supplierNum='+item.supplierNum+'" target="_blank">查看报价详情</a>'+
                '</div>'+
                '<div class="pull-right price">'+
                '中标条数：<span class="red2">'+item.quotationResult.length+
                '</span>条&nbsp;&nbsp;中标金额：<span class="red2">'+item.bidderMoney.toFixed(2)+'</span>元'+
                '</div>'+
                '</div>'+
                '</thead>'+
                '<tbody>'+
                '<tr>'+
                '<th>物料代码</th>'+
                '<th>物料名称</th>'+
                '<th>品牌</th>'+
                '<th>型规</th>'+
                '<th>采购数量</th>'+
                '<th>中标数量</th>'+
                '<th>单位</th>'+
                '<th>单价</th>'+
                '<th>含税总价</th>'+
                '<th>竞价状况</th>'+
                '</tr>';

            $.each(item.quotationResult, function(index, itemTwo) {

                if(itemTwo.confirmedPriceTaxed==undefined){
                    itemTwo.confirmedPriceTaxed="";
                }
                if(itemTwo.subtotalTaxed==undefined){
                    itemTwo.subtotalTaxed="";
                }
                htmlText += '<tr>'+
                    '<td>'+itemTwo.materialNo+'</td>'+
                    '<td>'+itemTwo.materialName+'</td>'+
                    '<td>'+itemTwo.producer+'</td>'+
                    '<td>'+itemTwo.character+'</td>'+
                    '<td>'+itemTwo.requestAmount+'</td>'+
                    '<td>'+itemTwo.confirmedAmout+'</td>'+
                    '<td>'+itemTwo.unit+'</td>'+
                    '<td>'+itemTwo.confirmedPriceTaxed+'</td>'+
                    '<td>'+(+(itemTwo.subtotalTaxed)).toFixed(4)+'</td>'+
                    '<td><a class="blue" herf="javascript:void(0);" onclick="showMap(\'' +itemTwo.id+'\'' +'\,'+'\''+itemTwo.materialName+ '\')">查看</a></td>'+
                    '</tr>';
            });

            htmlText += '</tbody> </table>';
            $('#tab1').html(htmlText);
        });
    });
}

function completeRelease(){
    $('#completeRelease').submit();
}

$(document).ready(function () {

    createGys();

    $('#myTab .supplier_type').click(function(){
        createGys();
    });
    $('#myTab .item_type').click(function(){
        var htmlText2="";
        var requestId1 = $('#requestId').val();
        $.post(ctx+"/rfqQuotationResult/testWLQuotation",{id:requestId1}, function(data) {
            $.each(data.list, function(index, item) {
                htmlText2 += '<table class="table table-bordered align-md info-y-1">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>物料代码</th>' +
                    '<th>物料名称</th>' +
                    '<th>型规</th>' +
                    '<th>品牌</th>' +
                    '<th>备注</th>' +
                    '<th>采购数量</th>' +
                    '<th>单位</th>' +
                    '<th>参考价</th>' +
                    '<th>竞价情况</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '<tr>' +
                    '<td>'+item.materialNo+'</td>'+
                    '<td>'+item.materialName+' </td>'+
                    '<td>'+item.character+'</td>'+
                    '<td>'+item.producer+'</td>'+
                    '<td>'+item.materialNo+'</td>'+
                    '<td>'+item.requestAmount+'</td>'+
                    '<td>'+item.unit+'</td>'+
                    '<td>'+item.salePrice+'</td>'+
                    '<td><a class="blue" herf="javascript:void(0);" onclick="showMap(\'' +item.requestItemId+'\'' +'\,'+'\''+item.materialName+ '\')">查看</a></td>'+
                    '</tr>' +
                    '<tr>' +
                    '<td colspan="9">' +
                    '<table class="table table-bordered align-md" style="margin-bottom:0;">' +
                    '<thead>' +
                    '<tr>' +
                    '<th class="warning-y-1">中标供应商</th>' +
                    '<th>中标数量</th>' +
                    '<th>单价</th>' +
                    '<th>含税总价</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>';

                $.each(item.gysResult, function(index, item2) {
                    if(item2.confirmedPriceTaxed==undefined){
                        item2.confirmedPriceTaxed="";
                    }
                    if(item2.subtotalTaxed==undefined){
                        item2.subtotalTaxed="";
                    }
                    htmlText2 += '<tr>'+
                        '<td><a href="javascript:;" class="blue" target="_blank">'+item2.supplierName+'</a></td>'+
                        '<td>'+item2.confirmedAmout+'</td>'+
                        '<td>'+item2.confirmedPriceTaxed+'</td>'+
                        '<td>'+(+(item2.subtotalTaxed)).toFixed(4)+'</td>'+
                        '</tr>';
                });
                htmlText2 += '</tbody>'+
                    '</table>'+
                    '</td>'+
                    '</tr>'+
                    '</tbody>'+
                    '</table>';

            });
            $('#tab2').html(htmlText2);
        });
    })
})



showMap=function (requestItemId,name){
    $.ajax({
        url:"WLQuotation",
        type:"POST",
        data:{
            requestId:$("#requestId").val(),
            requestItemId:requestItemId
        },
        datatype:'json',
        success: function (data) {
            var dataJson=eval("("+data+")");
            $("#WLtitle").text(name);
            $("#myModal19").modal('show');
            setEchat(dataJson.moneyList);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
};
