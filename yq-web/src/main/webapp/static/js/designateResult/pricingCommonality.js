/**
 * Created by Administrator on 2016/11/10.
 */

var optionTemptale={
    //提示框
    tooltip: {
        //触发类型，默认数据触发，见下图，可选为：’item’ | ‘axis’
        trigger: 'axis'//表示直角坐标系的一个坐标轴
    },
    //	辅助工具箱，辅助功能
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    //	图例
    legend: {
        data:['公司1','公司2','公司3','公司4','公司5']
    },
    //	直角坐标系中除坐标轴外的绘图网格，用于定义直角系整体布局
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    tooltiplbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['1次','2次','3次','4次','5次','6次','7次']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'公司1',
            type:'line',
            data:[120, 132, 101, 134, 90, 230, 210]
        },
        {
            name:'公司2',
            type:'line',
            data:[220, 182, 191, 234, 290, 330, 310]
        },
        {
            name:'公司3',
            type:'line',
            data:[150, 232, 201, 154, 190, 330, 410]
        },
        {
            name:'公司4',
            type:'line',
            data:[320, 332, 301, 334, 390, 630, 920]
        },
        {
            name:'公司5',
            type:'line',
            data:[820, 932, 901, 934, 1290, 1330, 1320]
        }
    ]
};
/*去除数据中的逗号*/
function convertNumbers(strs){
    var numbers=[];
    $.each(strs,function(i,d){
        numbers.push(Number((d+"").replace(/,/g,"")));
    });
    return numbers;
}

function convertData(dataList,option){
    option.legend.data=[];//清空公司名称
    option.series=[];//清空显示数据
    var maxQuotationMoney=0;
    $.each(dataList,function(i,d){
        option.legend.data.push(d.supplierName);//添加公司名称
        var xAxisTemplate={};
        xAxisTemplate.name=d.supplierName;
        xAxisTemplate.data=convertNumbers(d.quotationMoney);
        xAxisTemplate.type='line';
        option.series.push(xAxisTemplate);//设置显示数据

        if(maxQuotationMoney<xAxisTemplate.data.length){
            maxQuotationMoney=xAxisTemplate.data.length;
        }

    });
    option.xAxis.data=[];//清空次数
    for(var i=0;i<=maxQuotationMoney;i++){
        option.xAxis.data.push((i+1)+"次");
    }
    return option;
}



function setEchat(moneyList){
    //console.log(moneyLis;
    // 基于准备好的dom，初始化echarts图表
    var myChart = echarts.init(document.getElementById('mainChart'));
    var option=optionTemptale;
    // 为echarts对象加载数据
    myChart.setOption(convertData(moneyList,option));
}

//撤销竞价单
function revoke(){
    if( $("#pubEndMemo").val().length == 0){
        $.zui.messager.show('sorry，请填写撤销原因！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return;
    }
    if($("#pubEndMemo").val().length > 2000){
        $.zui.messager.show('撤销原因不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return;
    }
    $.ajax({
        type: "POST",
        url:ctx + '/rfqRequestState/revocationOfRequest',
        data: {
            extendField4:$("#extendField4  option:selected").val(),
            pubEndMemo:$("#pubEndMemo").val(),
            unifiedRfqNum:$("#unifiedRfqNum").val()
        },
        success: function (data) {
            if (data.status == 'success') {
                location.href = $("#contextPath").val() + "/rfqRequest/init";
            } else {
                $.zui.messager.show('sorry,系统出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    });
};

//取富文本编辑值
var editor;
KindEditor.ready(function (K) {
    editor = K.create('textarea#contentSimple', {
        resizeType: 1,
        allowPreviewEmoticons: false,
        allowImageUpload: false,
        items: []
    });
});
//点击提交按钮
submit_modal_func=function(){
    if(editor.text()==""||editor.text()==undefined){
        $.zui.messager.show('请录入授标说明！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return false;
    }
    var str=editor.text();
    var newStr =  str.split('&lt;').join('<').split('&gt;').join('>').replace(/\s+/g, "");
    if(newStr.length>2000){
        $.zui.messager.show('授标说明不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return false;
    }
    $("#myModa6").modal('show');
};

/**
 * 授标提交
 */
submit_func=function(){
    var selAudit = $('#approvalCode').val();
    var procDefType = selAudit.split(",")[1];
    if(selAudit != 0) {   //选择审批
        if (procDefType == 2) {   //灵活流程
            $('#myModa6').modal('hide');
            $('#myRole').modal('show');
            return;
        } else {
            var $confirmBtn = $('#myModa6').find('.modal-footer .btn-primary');
            $confirmBtn.attr('disabled', true);
            postforData([]);
        }
    }else {
        var $confirmBtn = $('#myModa6').find('.modal-footer .btn-primary');
        $confirmBtn.attr('disabled', true);
        postforData([]);
    }
};

//ajax提交方法
function postforData(rowDataLists){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/grantResult",
        dataType: "text",
        type:"POST",
        async:false,
        data:{
            requestId:$("#requestId").val(),
            resultMemo:editor.html(),
            approvalCode:$("#approvalCode").val(),
            unifiedRfqNum:$("#unifiedRfqNum").val(),
            rowDataLists: JSON.stringify(rowDataLists)
        },
        success: function(data){
            if("true"==data){
                window.location.href=$("#contextPath").val()+"/rfqRequest/init";
            }else{
                $.zui.messager.show('授标失败，请检查结果审批流！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    });
}

function completeRelease(){

    //加入确认提示
    RFQ.confirm('发布确认','确定要发布结果吗？', function () {
        $('#completeRelease').ajaxSubmit({
            type: 'post', // 提交方式 get/post
            url: ctx+'/rfqRequest/completeRelease', // 需要提交的 url
            dataType: 'json',
            success: function (data) {
                if(data.rspcod != 200){
                    RFQ.error(data.rspmsg);
                    return;
                }else{
                    location.href=ctx+"/rfqRequest/init";
                }
            },
            error : function(XmlHttpRequest, textStatus, errorThrown){
                RFQ.error('出错了！');
            }
        });
    });
};