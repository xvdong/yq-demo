/*信息节点*/
var oTbodyTr='<tr><td><span class="availableCount">0</span><div class="col-xs-12"><input type="hidden" class="form-control quotationItemId" value="";><input type="text" onkeyup="checkPrice(3, this)" onafterpaste="checkPrice(3, this)" onblur="checkPrice(3, this),compare(this)" class="form-control confirmedAmout remove";><input  type="hidden" class="form-control oldAmout remove";></div></td><td width="135"><span class="unitPrice"><div class="unitPriceA">未报价</div><div class="unitPriceB">未报价</div></span><div class="col-xs-12"><input type="text" onkeyup="checkPrice(2, this)" onafterpaste="checkPrice(2, this)" onblur="checkPrice(2, this)" class="form-control confirmedUnitPrice remove";><input type="hidden" class="form-control oldUnitPrice remove";><input type="text" onkeyup="checkPrice(1, this)" onafterpaste="checkPrice(1, this)" onblur="checkPrice(1, this)" class="form-control confirmedPriceTaxed remove";><input type="hidden" class="form-control oldUnitPriceTaxed remove";></div></td><td width="80"><span class="taxrate"></span></td></tr>';

var oTbody='<table class="table table-bordered table-striped align-md"><tbody></tbody></table>';

/*行*/
var wlTr='<tr><td width="60" class="rownum">1</td><td><div class="zkDiv"><span class="materialName"></span><a href="#" class="btn btn-mini hjBtn btn-warning" data-toggle="modal" onclick="setItemWindow(this)"  data-target="#myModa3">按行核价</a><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a><div class="hidden moreInfo">更多说明更多说明更多说明更多说明更多说明更多说明更多说明</div></div></td><td width="80" class="requestAmount">0个</td><td width="100" class="minPrice"><div class="minPriceA"></div><div class="minPriceB"></div></td></tr>';

$(function () {
    $.ajax({
        url:$("#contextPath").val()+ "/designateResult/designate",
        data: {requestId: $("#requestId").val(), page: $("#pageNum").html(), size: $("#pageSize").val()},
        type: 'POST',
        dataType: "json",
        async:false,
        cache: false,
        success: function (data) {
            showTable(data);
            calcResult();//初始化拟签小计
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    })
});
var select = document.getElementById('pageSize');
select.onchange = function(){
    toPage($("#pageNum").html(), $("#pageSize").val());
}
$("#toPage").bind("click",function(){
    toPage($("#pageNo").val(), $("#pageSize").val());
});
$("#launched").bind("click",function(){

    //核价页面导出时页面获取核价方式---start
    var priceType=$("#dropdownMenu1").text();//获取含税未税的值
    var priceTypeNum = "1";
    //将文字转换成数字
    if(priceType !== undefined && priceType !== null){
        if( priceType.indexOf("未税") > -1){
            priceTypeNum = "0";//
        }else{
            priceTypeNum = "1";
        }
    }
    //核价页面导出时页面获取核价方式---start

    if(checkdata_fun("launched")){
        return false;
    };
    if($("strong[class=red]").html()=="0.00"){
        $.zui.messager.show('请输入核价信息', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        return false;
    };
    $.ajax({
        url: $("#contextPath").val()+"/designateResult/result",
        data: {requestId: $("#requestId").val(),
            quotationResult: returnStr(),
            priceType: priceTypeNum
        },
        type:"POST",
        success:function(data){
            window.sessionStorage.designateResult_tax=$("#dropdownMenu1").text().indexOf("含税")>-1;
            window.location.href=$("#contextPath").val()+"/designateResult/grantInit?unifiedRfqNum="+$("#unifiedRfqNum").val();
        }
    });
});

$("#resultSave").bind("click",function(){

    //核价页面导出时页面获取核价方式---start
    var priceType=$("#dropdownMenu1").text();//获取含税未税的值
    var priceTypeNum = "1";
    //将文字转换成数字
    if(priceType !== undefined && priceType !== null){
        if( priceType.indexOf("未税") > -1){
            priceTypeNum = "0";//
        }else{
            priceTypeNum = "1";
        }
    }
    //核价页面导出时页面获取核价方式---start

    if(checkdata_fun()){
        return false;
    };
    $.ajax({
        url: $("#contextPath").val()+"/designateResult/result",
        data: {requestId: $("#requestId").val(),
            quotationResult: returnStr(),
            priceType: priceTypeNum
        },
        type:"POST",
        success:function(data){
            $.zui.messager.show('保存成功!', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
        },
        error: function () {
            $.zui.messager.show('保存失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
});

function toPage(pageNum, pageSize) {

    if(Number(pageNum)>Number($("#pages").text())
        ||
        Number(pageNum)<0){
        return false;
    }

    if(checkdata_fun()){
        return false;
    }
    //获取含税未税的值
    var priceType=$("#dropdownMenu1").text();
    var priceTypeNum = "1";
    //将文字转换成数字
    if(priceType !== undefined && priceType !== null){
        if( priceType.indexOf("未税") > -1){
            priceTypeNum = "0";//
        }else{
            priceTypeNum = "1";
        }
    }
    //console.log("========= + " + priceType);
    $.ajax({
        url: $("#contextPath").val()+"/designateResult/result",
        type:"POST",
        data: {requestId: $("#requestId").val(), quotationResult: returnStr(),priceType: priceTypeNum }
    });

    /**清除数据叠加*/
    $('#div3 .scrollDiv>div').remove();
    $('#div2 #scrollDiv>table').remove();
    $('.hjmx-tbody .hjmx-tbody-gd table tbody>tr').remove();

    $.ajax({
        url: $("#contextPath").val()+"/designateResult/designate",
        data: {requestId: $("#requestId").val(), page: pageNum, size: pageSize},
        dataType: "json",
        async:false,
        type:"POST",
        success: function (data) {
            showTable(data);
            calcResult();//初始化拟签小计
        },
        error: function () {
            $.zui.messager.show('请输入正确页面信息！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });

}
var scrollK = 0;
/** 表格动态展开*/
function showTable(data) {
    /** 分页信息*/
    var pageNum = data.pageInfo.pageNum;
    var pages = data.pageInfo.pages;
    var pageSize = data.pageInfo.pageSize;
    $('#total').html(data.pageInfo.total);
    $('#pageNum').html(pageNum);
    $('#pages').html(pages);
    $('#pageSize').val(pageSize);
    if (pages > 1 && pageNum < pages) {
        $("#nextPage").replaceWith('<li id="nextPage"><a href="javascript:toPage('+(Number(pageNum)+1)+', '+pageSize+')">下一页</a></li>');
        $("#endPage").replaceWith('<li id="endPage"><a href="javascript:toPage('+pages+', '+pageSize+')">尾页</a></li>');
    } else {
        $("#nextPage").replaceWith('<li id="nextPage"><a class="gray">下一页</a></li>');
        $("#endPage").replaceWith('<li id="endPage"><a class="gray">尾页</a></li>');
    }
    if (pageNum > 1) {
        $("#homePage").replaceWith('<li id="homePage"><a href="javascript:toPage(1, '+pageSize+')">首页</a></li>');
        $("#lastPage").replaceWith('<li id="lastPage"><a href="javascript:toPage('+(Number(pageNum)-1)+', '+pageSize+')">上一页</a></li>');
    } else {
        $("#homePage").replaceWith('<li id="homePage"><a class="gray">首页</a></li>');
        $("#lastPage").replaceWith('<li id="lastPage"><a class="gray">上一页</a></li>');
    }
    /** 供应商表头信息*/
    $.each(data.quotationList, function (i, key) {

        /*公司节点*/
        var oT='<div><div class="t"><h4><a id=\'' + i + '\'  class="blue" target="_blank"></a></h4><a class="group-bjxq" target="myquotationView" href="myquotationView.htma claspan class="label label-warning">报价详情</span>&nbsp;<span class="red weiShui" style="display: none">0.00</span><span class="red jine" style="display: inline">0.00</span> 元</a><span class="green attachment">附</span> <span class="red remark">注</span><span class="ml12">拟签小计：<strong class="red"><a class="red totalA">0.00</a><a class="red totalB">0.00</a></strong>元</span></div><table class="table table-bordered align-md"><tr><th width="">可供数量<br>拟签数量</th><th class="changeTax" width="135">含税报价单价(元)<br>含税拟签单价(元)</th><th width="80">税率</th></tr></table></div>';
        $('#div3 .scrollDiv').append(oT);
        $('#div3 .scrollDiv>div').find('h4 a').eq(i).html(key.supplierName);
        $('#div3 .scrollDiv>div').find('.jine').eq(i).html(key.subtotalTaxed);
        $('#div3 .scrollDiv>div').find('.weiShui').eq(i).html(key.subtotal);
        $('#div3 .scrollDiv>div').find('.group-bjxq').eq(i).attr("href", $("#contextPath").val() + "/rfqQuotation/getQuotationDetail?id=" + key.id + "&type=1");
        // $('#div3 .scrollDiv>div').find('.quotationhref').eq(i).attr("href",$("#contextPath").val()+"/rfqQuotation/getQuotationDetail?id="+key.id+"&type=1");
        $("#"+i).attr("href","http://eps.baosteel.net.cn/eps_eis//overview/BusinessResume.do?method=resume&companyCode="+key.supplierNum+"&companyId="+key.companyId);
        if (key.isRemark != "1") {
            $('#div3 .scrollDiv>div .remark').eq(i).html('');
        }
        if (key.isAttachment != "1") {
            $('#div3 .scrollDiv>div .attachment').eq(i).html('');
        }
    });


    //-----------加载后根据核价方式刷新(价格筛选工具需要)
    //核价页面导出时页面获取核价方式---start
    var priceType=$("#dropdownMenu1").text();//获取含税未税的值
    var priceTypeNum = "1";
    //将文字转换成数字
    if(priceType !== undefined && priceType !== null){
        if( priceType.indexOf("未税") > -1){
            priceTypeNum = "0";//
        }else{
            priceTypeNum = "1";
        }
    }
    //核价页面导出时页面获取核价方式---end
    if (priceTypeNum == "0"){
        $('#div3 .scrollDiv>div').find('.jine').hide();
        $('#div3 .scrollDiv>div').find('.weiShui').show();
    }else {
        $('#div3 .scrollDiv>div').find('.jine').show();
        $('#div3 .scrollDiv>div').find('.weiShui').hide();
    }
    //-----------加载后根据核价方式刷新(价格筛选工具需要)



    /** 供应商报价、核价信息*/
    $.each(data.requestItemList[0].quotationItemList, function (i, key) {
        $('#div2 #scrollDiv').append(oTbody);
    });
    var changeVar = $('#changeZt').find('.v').html();
    /** 物料基本信息*/
    $.each(data.requestItemList, function (i, key) {
        if(i<data.requestItemList.length){
            var info;
            $('.hjmx-tbody .hjmx-tbody-gd table tbody').append(wlTr);
            $('#div2 #scrollDiv table tbody').append(oTbodyTr);
            var cgxxTr = $('.hjmx-tbody .hjmx-tbody-gd table tbody tr');
            cgxxTr.eq(i).find('.materialName').html(key.baseInfo);
            if($("#dataSource").val()=='JFE' || $("#dataSource").val()=='NBBX'){
                var drawing=key.drawingNo==null?'':key.drawingNo;
                var more;
                if($("#dataSource").val()=='JFE'){
                    more="申购单号:"+key.purchaseNo+"<br/>"+"申购人:"+key.purchasePerson+"<br/>"+"图号:"+drawing;
                }else{
                    more="材质/图号:"+drawing;
                }
                var k=key.moreInfo.split('<br/>');
                k.splice(5,0,more);
                info=k.toString().split(',').join('<br/>');
            }else{
                info=key.moreInfo;
            }
            cgxxTr.eq(i).find('.moreInfo').html(info);
            cgxxTr.eq(i).find('.requestAmount').html(key.requestAmount);
            if (key.minTaxedPrice == null || key.minTaxedPrice ==''){//未报价则空
                cgxxTr.eq(i).find('.minPriceA').html("");
            }else {
                cgxxTr.eq(i).find('.minPriceA').html(key.minTaxedPrice+'<br>'+" "+key.salePrice);//中间的空格是截取标记
            }
            if (key.minTaxedPrice == null || key.minTaxedPrice ==''){
                cgxxTr.eq(i).find('.minPriceB').html("");
            }else {
                cgxxTr.eq(i).find('.minPriceB').html(key.minPrice+'<br>'+" "+key.salePrice);
            }
            cgxxTr.eq(i).find('.rownum').html(key.rownum);
            if (changeVar == '含税核价') {
                cgxxTr.eq(i).find('.minPriceA').show();
                cgxxTr.eq(i).find('.minPriceB').hide();
            } else {
                cgxxTr.eq(i).find('.minPriceA').hide();
                cgxxTr.eq(i).find('.minPriceB').show();
            }
            //changeBar();
        }
    });
    /** 供应商报价、核价详细信息*/
    $.each(data.requestItemList, function (i, key) {
        var minPriceA = key.minTaxedPrice;
        var minPriceB = key.minPrice;
        $.each(key.quotationItemList, function (j, val) {
            var oTableTr = $('#div2 #scrollDiv table').eq(j).find('tr').eq(i);
            if (changeVar == '含税核价') {
                $('.changeTax').replaceWith('<th class="changeTax" width="135">含税报价单价(元)<br>含税拟签单价(元)</th>');
                oTableTr.find('.unitPriceA').show();
                oTableTr.find('.confirmedPriceTaxed').show();
                oTableTr.find('.unitPriceB').hide();
                oTableTr.find('.confirmedUnitPrice').hide();
            } else {
                $('.changeTax').replaceWith('<th class="changeTax" width="135">未税报价单价(元)<br>未税拟签单价(元)</th>');
                oTableTr.find('.unitPriceA').hide();
                oTableTr.find('.confirmedPriceTaxed').hide();
                oTableTr.find('.confirmedUnitPrice').show();
                oTableTr.find('.unitPriceB').show();
            }
            if (val && val.id != null) {
                oTableTr.find('.taxrate').html(val.tax + '%');
                oTableTr.find('.availableCount').html(val.availableCount);
                oTableTr.find('.quotationItemId').val(val.id);
                oTableTr.find('.unitPriceA').html(val.unitPriceTaxed);
                oTableTr.find('.unitPriceB').html(val.unitPrice);
                oTableTr.find('.confirmedAmout').val(val.confirmedAmout);
                oTableTr.find('.oldAmout').val(val.confirmedAmout);
                oTableTr.find('.confirmedPriceTaxed').val(val.confirmedPriceTaxed);
                oTableTr.find('.oldUnitPriceTaxed').val(val.confirmedPriceTaxed);
                oTableTr.find('.confirmedUnitPrice').val(val.confirmedUnitPrice);
                oTableTr.find('.oldUnitPrice').val(val.confirmedUnitPrice);
                if (minPriceA != null && minPriceA != '' && minPriceA == val.unitPriceTaxed) {
                    oTableTr.find('.unitPriceA').css("color","red");
                }
                if (minPriceB != null && minPriceB != '' && minPriceB == val.unitPrice) {
                    oTableTr.find('.unitPriceB').css("color","red");
                }
            }
            $("input").each(function(i,d){$(d).val($(d).val().replace(/,/g,''));});
        });
        $("input").bind("change",inputChangeCalcTotal_func);
    });
    changeBar();
    /** 表格自定义滚动展现*/
    var listSize = data.quotationList.length;
    if (listSize <= 1) {
        scrollK = 1;
        $('#div3').attr("style","left: 0;");
        $('#scrollDiv').attr("style","left: 0;");
    } else {
        scrollK = 0;
    }
    scroll(scrollK);
    /**禁止对未报价物料进行核价*/
    $(".unitPrice:contains(未报价)").each(function(){
        var v_tr=$(this).parent().parent();
        $(v_tr).find("input").attr("disabled","disabled");

    });

    $('.zkBtn').rotateArrow();
    $('.zkBtn').on("click", function () {
        var This = $(this);
        $(this).next().toggleClass('hidden');
        changeBar();
        var num = 0;
        $('.zkBtn').each(function(){
            if($(this).next().hasClass('hidden')){
                num +=1;
            }
        })

        if(num==0){
            $('#all-zk').html("全部收缩");
        }else if(num== $('.zkBtn').length){
            $('#all-zk').html("全部展开");
        }
        num = 0;
    });
}
/**含(未)税切换*/
var aaaa=$('#changeZt').find('.v').html();
$('#changeZt').find('li>a').click(function(){
    if($(this).html()!=aaaa){
        aaaa=$(this).html();
        if (aaaa=='含税核价') {
            $('#changeZt').find('.v').html("含税核价");
            $('#div3 .scrollDiv>div').find('.jine').show();
            $('#div3 .scrollDiv>div').find('.weiShui').hide();
            var total = 0;
            $('#div3 .scrollDiv').find('strong.red').each(function(){
                $(this).find('.totalA').show();
                $(this).find('.totalB').hide();
                total=total+Number($(this).find('.totalA').html().replace(/,/g,""));
            });
            $(".hjmx-th-gd strong.red").text(total.toFixed(2));
            $('.changeTax').replaceWith('<th class="changeTax" width="135">含税报价单价(元)<br>含税拟签单价(元)</th>');
            $(".scrollDiv>table").find("tr").each(function(){
                var v_oldUnitPrice = $(this).find('.oldUnitPrice').val();
                var v_confirmedUnitPrice = $(this).find('.confirmedUnitPrice').val();
                if (v_oldUnitPrice != v_confirmedUnitPrice) {
                    if (v_confirmedUnitPrice == '') {
                        $(this).find('.confirmedPriceTaxed').val('');
                    } else {
                        var v_tax = $(this).find('.taxrate').html().replace('%','');
                        $(this).find('.confirmedPriceTaxed').val((v_confirmedUnitPrice * (v_tax/100 + 1)).toFixed(4));
                    }
                }
                $(this).find('.unitPriceA').show();
                $(this).find('.confirmedPriceTaxed').show();
                $(this).find('.unitPriceB').hide();
                $(this).find('.confirmedUnitPrice').hide();
            });

            $('.hjmx-tbody .hjmx-tbody-gd table tbody tr').each(function(){
                $(this).find('.minPriceA').show();
                $(this).find('.minPriceB').hide();
            });
        } else {
            $('#changeZt').find('.v').html("未税核价");
            $('#div3 .scrollDiv>div').find('.jine').hide();
            $('#div3 .scrollDiv>div').find('.weiShui').show();
            var total = 0;
            $('#div3 .scrollDiv').find('strong.red').each(function(){
                $(this).find('.totalB').show();
                $(this).find('.totalA').hide();
                total=total+Number($(this).find('.totalB').html().replace(/,/g,""));
            });
            $(".hjmx-th-gd strong.red").text(total.toFixed(2));
            $('.changeTax').replaceWith('<th class="changeTax" width="135">未税报价单价(元)<br>未税拟签单价(元)</th>');
            $(".scrollDiv>table").find("tr").each(function(){
                var v_oldUnitPriceTaxed = $(this).find('.oldUnitPriceTaxed').val();
                var v_confirmedPriceTaxed = $(this).find('.confirmedPriceTaxed').val();
                if (v_oldUnitPriceTaxed != v_confirmedPriceTaxed) {
                    if (v_confirmedPriceTaxed == '') {
                        $(this).find('.confirmedUnitPrice').val('');
                    }else {
                        var v_tax = $(this).find('.taxrate').html().replace('%','');
                        $(this).find('.confirmedUnitPrice').val((v_confirmedPriceTaxed / (v_tax/100 + 1)).toFixed(2));
                    }
                }
                $(this).find('.unitPriceA').hide();
                $(this).find('.confirmedPriceTaxed').hide();
                $(this).find('.unitPriceB').show();
                $(this).find('.confirmedUnitPrice').show();
            });
            $('.hjmx-tbody .hjmx-tbody-gd table tbody tr').each(function(){
                $(this).find('.minPriceA').hide();
                $(this).find('.minPriceB').show();
            });
        }
    }
    inputChangeCalcTotal_func();
});
function checkPrice(type, obj){
    if (isNaN(obj.value)) {
        obj.value = "";
    }
    if (obj != null) {
        if (type == '1') {
            if (obj.value.toString().split(".").length > 1 && obj.value.toString().split(".")[1].length > 4) {
                $.zui.messager.show('含税拟签价格最多四位小数', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                $(obj).focus();
            }
        } else if (type == '2') {
            if (obj.value.toString().split(".").length > 1 && obj.value.toString().split(".")[1].length > 2) {
                $.zui.messager.show('未税拟签价格最多两位小数', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                $(obj).focus();
            }
        } else if (type == '3') {
            if (obj.value.toString().split(".").length > 1 && obj.value.toString().split(".")[1].length > 4) {
                $.zui.messager.show('拟签数量最多四位小数', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                $(obj).focus();
            }
        }
    }
}
/**组装返回给后台有变动的核价结果*/
function returnStr() {
    var arr1=[];
    var arr2=[];
    $(".scrollDiv>table").find("tr").each(function(){
        var quotationItemId = $(this).find(".quotationItemId").val();
        var v_confirmedAmout = $(this).find(".confirmedAmout").val();
        var v_oldAmout = $(this).find(".oldAmout").val();
        var v_confirmedPriceTaxed = $(this).find(".confirmedPriceTaxed").val();
        var v_oldUnitPriceTaxed = $(this).find(".oldUnitPriceTaxed").val();
        var v_confirmedUnitPrice = $(this).find(".confirmedUnitPrice").val();
        var v_oldUnitPrice = $(this).find(".oldUnitPrice").val();
        if(v_confirmedAmout != v_oldAmout || v_confirmedPriceTaxed != v_oldUnitPriceTaxed || v_confirmedUnitPrice != v_oldUnitPrice){
            var v_tax = $(this).find('.taxrate').html().replace('%','');
            arr1.push(quotationItemId);
            arr1.push(v_confirmedAmout);
            //if (v_confirmedPriceTaxed != v_oldUnitPriceTaxed && v_confirmedUnitPrice == v_oldUnitPrice) {
            if ($('#changeZt').find('.v').text().indexOf("含税核价")>-1) {
                arr1.push(v_confirmedPriceTaxed);
                arr1.push((v_confirmedPriceTaxed / (v_tax/100 + 1)).toFixed(2));
                // } else if (v_confirmedUnitPrice != v_oldUnitPrice && v_confirmedPriceTaxed == v_oldUnitPriceTaxed) {
            } else {
                arr1.push((v_confirmedUnitPrice * (v_tax/100 + 1)).toFixed(4));
                arr1.push(v_confirmedUnitPrice);
            } /*else {
             arr1.push(v_confirmedPriceTaxed);
             arr1.push(v_confirmedUnitPrice);
             }*/
        }
    });
    if (arr1.length == 0) {
        return null;
    }
    for(var i=0,len=arr1.length;i<len;i+=4){
        arr2.push(arr1.slice(i,i+4));
    }
    return JSON.stringify(arr2);
}
/**
 * 供应商筛选列表查询
 */
querySupplierList_func=function(){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/querySupplierList",
        dataType: "json",
        data:{requestId: $("#requestId").val(),vPar:new Date()},
        type:"POST",
        success: function(data){
            $("#supplierList>tbody").html("");//清空供应商筛选表格
            $.each(data,function(i,d){
                var template="<tr> <th><input type='checkbox' name='supplierSel' ${isSelected} value='${supplierCode}'></th><td>${supplierName}</td><td><a class='red' target='myquotationView' href='${quotationUrl}'>${subtotalTaxed}</a></td><td>${quotationCount}</td><td>${minQuotationCount}</td><td>${isAttachment}</td><td>${isRemark}</td></tr>";
                var v_Y="<a class='icon icon-check green'></a>";
                var v_N="<a class='icon icon-times red2'></a>";
                template=template.replace("${supplierCode}", d.quotateId);
                template=template.replace("${supplierName}", d.supplierName);
                template=template.replace("${subtotalTaxed}", d.subtotalTaxed);
                template=template.replace("${quotationCount}", d.itemCount);
                template=template.replace("${minQuotationCount}", d.itemCountMin==null?'0':d.itemCountMin);
                template=template.replace("${isSelected}", d.selected?"checked='checked'":"");
                template=template.replace("${isAttachment}", d.isAttachment=="1"?v_Y:v_N);
                template=template.replace("${isRemark}", d.isRemark=="1"?v_Y:v_N);
                template=template.replace("${quotationUrl}",$("#contextPath").val()+"/rfqQuotation/getQuotationDetail?id="+d.quotateId+"&type=1");
                $("#supplierList>tbody").append(template);
            });
            var supplierList=$("#supplierList");
            $(".mg-text-yelloy").text("("+$(supplierList).find("input[name=supplierSel]:checked").length+"/"+$(supplierList).find("input[name=supplierSel]").length+")");
            $("#supplierSelTh").bind("click",supplierSelThClick);
        }});
}

/**
 * 供应商筛选列表查询-事件
 */
$("#gyssx").bind("click",querySupplierList_func);
/**
 * 供应商筛选全选与反选
 */
supplierSelThClick=function(){
    $("#supplierList :checkbox[name=supplierSel]").prop("checked",$("#supplierSelTh").is(":checked"));
}

/**
 *供应商筛选保存
 */
supplierSubmitFunc=function(){
    var supplierSelStr="";
    var supplierList=$("#supplierList");
    $(supplierList).find("input[name=supplierSel]:checked").each(function(){
        supplierSelStr+=$(this).val()+",";
    });
    if(supplierSelStr.length>0){
        supplierSelStr=supplierSelStr.substr(0,supplierSelStr.length-1);
    }
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/setSupplierList",
        dataType: "text",
        data:{requestId: $("#requestId").val(),supplierIds:supplierSelStr},
        type:"POST",
        success: function(data){
            $(".mg-text-yelloy").text("("+$(supplierList).find("input[name=supplierSel]:checked").length+"/"+$(supplierList).find("input[name=supplierSel]").length+")");
            $('#gyssx').trigger('click');
            toPage($("#pageNo").val(), $("#pageSize").val());
        }});
}
/**
 * 重新核价
 */
$("#designateResultAgain").bind("click",function(){
    $('.remove').val("");//清空列表中所有input的值
    calcResult();
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/designateResultAgain",
        dataType: "text",
        data:{requestId: $("#requestId").val()},
        type: 'POST',
        success: function(data){
            if("true"==data){
                //window.location.reload();
                toPage($("#pageNo").val(), $("#pageSize").val());
            }
        }});
});
/**
 * 设置价格筛选工具1、2
 * 依据类型(p_type)区分1、2
 */
setPriceTools=function(p_type,p_targetSupplierCode){

    //----核价方式获取 start
    var priceType=$("#dropdownMenu1").text();//获取含税未税的值
    var priceTypeNum = "1";
    //将文字转换成数字
    if(priceType !== undefined && priceType !== null){
        if( priceType.indexOf("未税") > -1){
            priceTypeNum = "0";//
        }else{
            priceTypeNum = "1";
        }
    }
    //----核价方式后期 end

    $('.remove').val("");//清空列表中所有input的值
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/setPriceTools",
        dataType: "text",
        data:{
            requestId: $("#requestId").val(),
            type:p_type,
            targetSupplierCode:p_targetSupplierCode,
            priceType:priceTypeNum
        },
        type: 'POST',
        async:false,
        success: function(data){
            if("true"==data){
                //alert("计算成功！");
                $('#jgsx').trigger('click');
                toPage($("#pageNo").val(), $("#pageSize").val());
            }else{
                $.zui.messager.show('工具计算错误！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }});
}

//$("#supplierSubmit").bind("click",);

/**
 * 设置价格筛选工具3
 */
setPriceToolsByTargetSupplier=function(){
    if( $("#targetSupplier").val() === null ||  $("#targetSupplier").val() === ""){
        $.zui.messager.show('请选择授标供应商', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return;
    }
    setPriceTools('1',$("#targetSupplier").val());
    $("#targetSupplier").val("");
    $(".close").click();
};
/**
 * 设置指定供应商筛选工具
 */
setPriceToolsByChooseSupplier=function(){
    if( $("#chooseSupplier").val() === null ||  $("#chooseSupplier").val() === ""){
        $.zui.messager.show('请选择供应商', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return;
    }
    setPriceTools('3',$("#chooseSupplier").val());
    $("#chooseSupplier").val("");
    $(".close").click();
};
/**
 * 设置价格筛选工具的目标供应商
 */
setSelSupplier=function(num){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/querySupplierList",
        dataType: "json",
        type:"POST",
        data:{requestId: $("#requestId").val(),vPar:new Date()},
        success: function(data){
            $("#targetSupplier").html("");
            if(num==3){
                $("#chooseSupplier").html("");
            }
            $.each(data,function(i,d){
                if(d.selected){
                    if(num==3){
                        $("#chooseSupplier").append("<option value='"+ d.supplierCode+"'>"+ d.supplierName+"</option>");
                    }
                    $("#targetSupplier").append("<option value='"+ d.supplierCode+"'>"+ d.supplierName+"</option>");
                }
            });
            if(num==3){
                $("#chooseSupplier").val("");
            }
            $("#targetSupplier").val("");
        }});
}
var v_global_index=null;

/**
 *拟签数量校验
 */
compare=function(obj,type){
    var returnFlag=false;
    var v_tr=$(obj).parent().parent().parent();
    v_global_index=$(v_tr).index();
    var v_data={};
    var v_amout_sum=0;
    var requestAmount=$(".hjmx-tbody-gd table tbody tr").eq(v_global_index).find("td:eq(2)").text();//采购数量
    if(requestAmount!=undefined){
        requestAmount=requestAmount.substr(0,requestAmount.indexOf(" "));
    }
    $(".scrollDiv>table").each(function(i,d){
        var v_quotation_tr=$(d).find("tbody>tr:eq("+v_global_index+")");
        var confirmedAmout=$(v_quotation_tr).find("td:eq(0)").find('.confirmedAmout').val();//拟签数量
        v_amout_sum+=Number(confirmedAmout);
    });
    if(Number(v_amout_sum)>Number(requestAmount)){
        if(type==="submit"){
            // returnFlag=true;
        }else{
            $.zui.messager.show('请注意：您的拟签数量已经超过了采购数量', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        }
        //  $(obj).focus();
    }
    if(Number(v_amout_sum)<Number(requestAmount)){
        if(type==="submit"){
            // returnFlag=true;
        }else{
            $.zui.messager.show('请注意：您的拟签数量少于采购数量', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        }
        //  $(obj).focus();
    }
    return returnFlag;
};
/**
 * 按行核价拟签数量校验
 */
compareByRow = function(obj,type){
    var requestAmount = $("#requestAmount").text();//采购数量
    var v_amout_sum=0;
    if(requestAmount!=undefined){
        requestAmount=requestAmount.substr(0,requestAmount.indexOf(" "));
    }
    $("#itemTable>tbody tr").each(function(i,d){
        //获取每一行的拟签数量
        var confirmedAmout=$(d).find('td').find('input[name="confirmedAmoutByRow"]').val();
        v_amout_sum+=Number(confirmedAmout);
    });
    if(Number(v_amout_sum)>Number(requestAmount)){
        $.zui.messager.show('请注意：您的拟签数量已经超过了采购数量。', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
    }
    if(Number(v_amout_sum)<Number(requestAmount)){
        $.zui.messager.show('请注意：您的拟签数量少于采购数量。', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
    }
};

/**
 *显示按行核价页面
 */
setItemWindow=function(obj){
    var num=$("#dropdownMenu1").text();
    $("span[id=hejia]").text(num);
    var v_tr=$(obj).parent().parent().parent();
    v_global_index=$(v_tr).index();
    var v_data={};

    // $(v_tr).find(".zkDiv").find("a").remove();
    var flag = false;
    if($(v_tr).find(".zkDiv").find("div").hasClass("hidden")){ //判断moreInfo是否有hidden
        flag = true;
        $(v_tr).find(".zkDiv").find("div").removeClass("hidden");//移除moreInfo中的hidden
    }
    $(v_tr).find(".zkDiv").find("a").addClass("hidden");//将物料信息中的两个a标签(向下箭头和按行核价)隐藏
    v_data.itemInfo=$(v_tr).find(".zkDiv").html();//物料信息    (通过html获取物料信息带格式)
    if(flag){
        $(v_tr).find(".zkDiv").find("div").addClass("hidden");//添加moreInfo中的hidden
    }
    $(v_tr).find(".zkDiv").find("a").removeClass("hidden");//显示物料信息中的两个a标签(向下箭头和按行核价)

    v_data.requestAmount=$(v_tr).find("td:eq(2)").text();//采购数量
    v_data.quotationInfos=[];//供应商报价数据
    $(".hjmx-th-fd").find(".scrollDiv>div").each(function(){
        var v_quotationInfo={};
        v_quotationInfo.supplierName=$(this).find(".blue").text();
        if(num.replace(/(^\s*)|(\s*$)/g, "")=="含税核价"){
            v_quotationInfo.subtotalTaxed=$(this).find("a.jine").text();
        }else{
            v_quotationInfo.subtotalTaxed=$(this).find("a.weiShui").text();
        }

        v_data.quotationInfos.push(v_quotationInfo);//供应商名称
    });
    $.each(v_data.quotationInfos,function(i,d){
        var v_quotationInfo=d;
        var v_table=$(".scrollDiv>table:eq("+i+")");
        var v_quotation_tr=$(v_table).find("tbody>tr:eq("+v_global_index+")");
        v_quotationInfo.availableCount=$(v_quotation_tr).find("td:eq(0)").text();//可供量
        if($("#dropdownMenu1").text().indexOf("未税")>-1){
            v_quotationInfo.unitPriceTaxed=$(v_quotation_tr).find("td:eq(1)").find(".unitPriceB").text();//报价单价
            v_quotationInfo.confirmedPriceTaxed=$(v_quotation_tr).find("td:eq(1)").find('.confirmedUnitPrice').val();//拟签含税价
        }else{
            v_quotationInfo.unitPriceTaxed=$(v_quotation_tr).find("td:eq(1)").find(".unitPriceA").text();//报价单价
            v_quotationInfo.confirmedPriceTaxed=$(v_quotation_tr).find("td:eq(1)").find('.confirmedPriceTaxed').val();//拟签含税价
        }
        v_quotationInfo.unitPriceTaxed=v_quotationInfo.unitPriceTaxed.replace(/,/g,"");
        v_quotationInfo.confirmedPriceTaxed=v_quotationInfo.confirmedPriceTaxed.replace(/,/g,"");
        v_quotationInfo.tax=$(v_quotation_tr).find("td:eq(2)").text();//税率
        v_quotationInfo.confirmedAmout=$(v_quotation_tr).find("td:eq(0)").find('.confirmedAmout').val();//拟签数量
        v_data.quotationInfos[i]=v_quotationInfo;

        if(!typeof(v_quotationInfo.confirmedUnitPrice) == "undefined"){
            v_quotationInfo.confirmedUnitPrice=v_quotationInfo.confirmedUnitPrice.replace(/,/g,"");
        }
        if(!typeof(v_quotationInfo.confirmedUnitPrice) == "undefined"){
            v_quotationInfo.confirmedSubtotal=v_quotationInfo.confirmedSubtotal.replace(/,/g,"");
        }

    });
    //Table
    var targetTable=$("#itemTable");
    $(targetTable).find("tbody").html("");

    //求出所有价格中的最低价
    var minPrice="999999999999999999.0000";
    try
    {
        for (var a=0;a<v_data.quotationInfos.length;a++){
            if (parseFloat(v_data.quotationInfos[a].unitPriceTaxed)<parseFloat(minPrice)){
                minPrice=v_data.quotationInfos[a].unitPriceTaxed;
            }else {}
        }
    }
    catch (e) {}


    $.each(v_data.quotationInfos,function(i,d){
        var v_template="";
        if(i==0){
            if (d.unitPriceTaxed == minPrice){//如果当前行是 最低价的行,则背景颜色突出
                v_template="<tr dataIdx='${dataIdx}'><td rowspan='${rowspan}' style='vertical-align:top'><div class='text-left'>${itemInfo}</div></td>"
                    +"<td rowspan='${rowspan}' id='requestAmount'>${requestAmount}</td>"
                    +"<td style='background-color:#CCCCCC'>${supplierName}</td>"
                    +"<td style='background-color:#CCCCCC'>${availableCount}</td>"
                    +"<td style='background-color:#CCCCCC'>${unitPriceTaxed}</td>"
                    +"<td style='background-color:#CCCCCC'>${tax}</td>"
                    +"<td style='background-color:#CCCCCC'>${subtotalTaxed}</td>"
                    +"<td style='background-color:#CCCCCC'><input class='form-control input-sm' name='confirmedAmoutByRow' ${disabled} type='text' placeholder='' onkeyup='checkPrice(3, this)' onafterpaste='checkPrice(3, this)' onblur='checkPrice(3,this);compareByRow(this)' value='${confirmedAmout}'></td>"
                    +"<td style='background-color:#CCCCCC'><input class='form-control input-sm confirmedPriceTaxed' name='confirmedUnitPriceByRow' ${disabled} type='text' placeholder='' value='${confirmedPriceTaxed}'></td></tr>";
            }else {
                v_template="<tr dataIdx='${dataIdx}'><td rowspan='${rowspan}' style='vertical-align:top'><div class='text-left'>${itemInfo}</div></td>"
                    +"<td rowspan='${rowspan}' id='requestAmount'>${requestAmount}</td>"
                    +"<td>${supplierName}</td>"
                    +"<td>${availableCount}</td>"
                    +"<td>${unitPriceTaxed}</td>"
                    +"<td>${tax}</td>"
                    +"<td>${subtotalTaxed}</td>"
                    +"<td><input class='form-control input-sm' name='confirmedAmoutByRow' ${disabled} type='text' placeholder='' onkeyup='checkPrice(3, this)' onafterpaste='checkPrice(3, this)' onblur='checkPrice(3,this);compareByRow(this)' value='${confirmedAmout}'></td>"
                    +"<td><input class='form-control input-sm confirmedPriceTaxed' name='confirmedUnitPriceByRow' ${disabled} type='text' placeholder='' value='${confirmedPriceTaxed}'></td></tr>";
            }
        }else{
            if (d.unitPriceTaxed == minPrice) {//如果当前行是 最低价的行,则背景颜色突出
                v_template="<tr style='background-color:red' dataIdx='${dataIdx}'><td style='background-color:#CCCCCC'>${supplierName}</td>"
                    +"<td style='background-color:#CCCCCC'>${availableCount}</td>"
                    +"<td style='background-color:#CCCCCC'>${unitPriceTaxed}</td>"
                    +"<td style='background-color:#CCCCCC'>${tax}</td>"
                    +"<td style='background-color:#CCCCCC'>${subtotalTaxed}</td>"
                    +"<td style='background-color:#CCCCCC'><input class='form-control input-sm' name='confirmedAmoutByRow' ${disabled} type='text' placeholder='' onkeyup='checkPrice(3, this)' onafterpaste='checkPrice(3, this)' onblur='checkPrice(3,this); compareByRow(this)' value='${confirmedAmout}'></td>"
                    +"<td style='background-color:#CCCCCC'><input class='form-control input-sm confirmedPriceTaxed' name='confirmedUnitPriceByRow' ${disabled} type='text' placeholder='' value='${confirmedPriceTaxed}'></td></tr>";
            }else {
                v_template="<tr style='background-color:red' dataIdx='${dataIdx}'><td>${supplierName}</td>"
                    +"<td>${availableCount}</td>"
                    +"<td>${unitPriceTaxed}</td>"
                    +"<td>${tax}</td>"
                    +"<td>${subtotalTaxed}</td>"
                    +"<td><input class='form-control input-sm' name='confirmedAmoutByRow' ${disabled} type='text' placeholder='' onkeyup='checkPrice(3, this)' onafterpaste='checkPrice(3, this)' onblur='checkPrice(3,this); compareByRow(this)' value='${confirmedAmout}'></td>"
                    +"<td><input class='form-control input-sm confirmedPriceTaxed' name='confirmedUnitPriceByRow' ${disabled} type='text' placeholder='' value='${confirmedPriceTaxed}'></td></tr>";
            }
        }
        v_template=v_template.replace(/\$\{rowspan\}/g,v_data.quotationInfos.length);
        v_template=v_template.replace("${itemInfo}",v_data.itemInfo);
        v_template=v_template.replace("${requestAmount}",v_data.requestAmount);
        v_template=v_template.replace("${supplierName}",d.supplierName);
        v_template=v_template.replace("${availableCount}",d.availableCount);
        v_template=v_template.replace("${unitPriceTaxed}",d.unitPriceTaxed);
        v_template=v_template.replace("${tax}",d.tax);
        v_template=v_template.replace("${subtotalTaxed}",d.subtotalTaxed);
        v_template=v_template.replace("${confirmedAmout}",d.confirmedAmout);
        v_template=v_template.replace("${confirmedPriceTaxed}",d.confirmedPriceTaxed);
        v_template=v_template.replace("${dataIdx}",i);
        $("#myModa3_v_unit").text(v_data.requestAmount.replace(/(\d{1,}[.]?\d{0,})/g,''));
        if(isNaN(d.unitPriceTaxed)){
            v_template=v_template.replace(/\$\{disabled\}/g,"disabled");
        }else{
            v_template=v_template.replace(/\$\{disabled\}/g,"");
        }
        $(targetTable).find("tbody").append(v_template);
        $("input[name=confirmedAmoutByRow]").bind("change",func_caleRowItem);
        $("input[name=confirmedUnitPriceByRow]").bind("change",func_caleRowItem);
    });
    func_caleRowItem();
}

/**
 *保存按行核价信息
 */
func_saveItemWindow=function(){
    var requestAmount=$("#requestAmount").text();
    if(requestAmount!=undefined){
        requestAmount=requestAmount.substr(0,requestAmount.indexOf(" "));
    }
    var v_amout_sum=$("#myModa3_v_amout_sum").html();
    //if(Number(v_amout_sum)>Number(requestAmount)){
    //    $.zui.messager.show('拟签数量不能大于采购数量', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
    //    return false;
    //};
    $("tr[dataidx]").each(function(i,d){
        var v_target=$(".scrollDiv>table:eq("+i+")").find("tr:eq("+v_global_index+")");
        $(v_target).find("td:eq(0)").find('.confirmedAmout').val($(d).find("input[name=confirmedAmoutByRow]").val());
        if ($(d).find("input[name=confirmedUnitPriceByRow]").val() != '') {
            var v_tax = $(v_target).find('.taxrate').html().replace('%', '') / 100 + 1;
            if ($("#dropdownMenu1").text().indexOf("未税") > -1) {
                $(v_target).find("td:eq(1)").find('.confirmedPriceTaxed').val(($(d).find("input[name=confirmedUnitPriceByRow]").val() * v_tax).toFixed(4));
                $(v_target).find("td:eq(1)").find('.confirmedUnitPrice').val($(d).find("input[name=confirmedUnitPriceByRow]").val());
            } else {
                $(v_target).find("td:eq(1)").find('.confirmedPriceTaxed').val($(d).find("input[name=confirmedUnitPriceByRow]").val());
                $(v_target).find("td:eq(1)").find('.confirmedUnitPrice').val(($(d).find("input[name=confirmedUnitPriceByRow]").val() / v_tax).toFixed(2));
            }
        }else{
            $(v_target).find("td:eq(1)").find('.confirmedPriceTaxed').val('');
            $(v_target).find("td:eq(1)").find('.confirmedUnitPrice').val('');
        }
    });
    $(".close").click();
    inputChangeCalcTotal_func();
}

func_caleRowItem=function(){
    if($("#dropdownMenu1").text().indexOf("未税")>-1){
        $(".confirmedPriceTaxed").attr("onkeyup","checkPrice(2, this)");
        $(".confirmedPriceTaxed").attr("onafterpaste","checkPrice(2, this)");
        $(".confirmedPriceTaxed").attr("onblur","checkPrice(2, this)");
    } else {
        $(".confirmedPriceTaxed").attr("onkeyup","checkPrice(1, this)");
        $(".confirmedPriceTaxed").attr("onafterpaste","checkPrice(1, this)");
        $(".confirmedPriceTaxed").attr("onblur","checkPrice(1, this)");
    }
    var v_unit_price_sum=0;
    var v_amout_sum=0;
    var v_avg=0;
    $("input[name=confirmedAmoutByRow]").each(function(i,d){
        var v_amout=$(d).val();
        if(v_amout!=""){
            v_amout_sum=Number(v_amout_sum)+Number(v_amout);
            var v_unitPrice=$(d).parent().parent().find("input[name=confirmedUnitPriceByRow]").val();
            if(v_unitPrice!=""){
                v_unit_price_sum+=Number(v_unitPrice)*Number(v_amout);
            }
        }
    });
    v_avg=v_unit_price_sum/v_amout_sum;
    $("#myModa3_unit_price_sum").html(v_unit_price_sum.toFixed(2));
    $("#myModa3_v_amout_sum").html(v_amout_sum);
    if(isNaN(v_avg)){
        $("#myModa3_v_amout_avg").html("0.00");
    }else{
        $("#myModa3_v_amout_avg").html(v_avg.toFixed(2));
    }
}
var confirmedTotalA={};//含税
var confirmedTotalB={};//未税
var confirmedTotalPage={};//未税
var confirmedTotalPageTax={};//含税
/**
 * 输入框绑定事件
 */
inputChangeCalcTotal_func=function(){
    $("#scrollDiv table").each(function(i,p_table){
        var page_total_tax=0;
        var page_total=0;
        $(p_table).find("tr").each(function(j,p_tr){
            var v_confirmedAmout=$(p_tr).find(".confirmedAmout").val().replace(/,/g,'');//拟签数量
            var v_confirmedPriceTaxed=$(p_tr).find(".confirmedPriceTaxed").val().replace(/,/g,'');//拟签单价
            var v_confirmedUnitPrice=$(p_tr).find(".confirmedUnitPrice").val().replace(/,/g,'');//拟签单价
            if(!isNaN(v_confirmedAmout)&&!isNaN(v_confirmedPriceTaxed)){
                page_total_tax+=Number(v_confirmedAmout)*(v_confirmedPriceTaxed);
            }
            if(!isNaN(v_confirmedAmout)&&!isNaN(v_confirmedUnitPrice)){
                page_total+=Number(v_confirmedAmout)*(v_confirmedUnitPrice);
            }
        });
        var total=Number(eval("confirmedTotalB.q"+i))+(page_total-Number(eval("confirmedTotalPage.p"+i)))
        $("#div3 .scrollDiv").find("strong.red .totalB").eq(i).text(total.toFixed(2));
        var total_tax=Number(eval("confirmedTotalA.q"+i))+(page_total_tax-Number(eval("confirmedTotalPageTax.p"+i)))
        $("#div3 .scrollDiv").find("strong.red .totalA").eq(i).text(total_tax.toFixed(4));
    });
    setTimeout(calcConfirmedTotal_func,100);
}
/**
 * 计算含税总价
 */
calcConfirmedTotal_func=function(){
    var total=0;
    if($("#dropdownMenu1").text().indexOf("未税")>-1){
        $("#div3 .scrollDiv").find("strong.red .totalB").each(function(i,d){
            total+=Number($(d).text().replace(/,/g,''));
        });
        $(".hjmx-th-gd strong.red").text(total.toFixed(2));
    }else{
        $("#div3 .scrollDiv").find("strong.red .totalA").each(function(i,d){
            total+=Number($(d).text().replace(/,/g,''));
        });
        $(".hjmx-th-gd strong.red").text(total.toFixed(2));
    }
}

/**
 * 拟签合计
 */
calcResult=function(){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/calcResult",
        dataType: "json",
        data:{requestId: $("#requestId").val()},
        type:"POST",
        success: function(data){
            //var total=0;
            $.each(data.resaultDetail,function(i,d){
                var v_confirmedTotal=Number(d.confirmedTotal.replace(/,/g,'')).toFixed(2);
                var v_confirmedTotalTaxed=Number(d.confirmedTotalTaxed.replace(/,/g,'')).toFixed(4);
                $("a:contains("+d.supplierName+")").parent().parent().find('strong.red .totalB').html(v_confirmedTotal);
                $("a:contains("+d.supplierName+")").parent().parent().find('strong.red .totalA').html(v_confirmedTotalTaxed);
                eval("confirmedTotalA.q"+ i+"="+v_confirmedTotalTaxed);
                eval("confirmedTotalB.q"+ i+"="+v_confirmedTotal);
                if($("#dropdownMenu1").text().indexOf("未税")>-1){
                    $("a:contains("+d.supplierName+")").parent().parent().find('strong.red .totalA').hide();
                    $("a:contains("+d.supplierName+")").parent().parent().find('strong.red .totalB').show();
                }else{
                    $("a:contains("+d.supplierName+")").parent().parent().find('strong.red .totalA').show();
                    $("a:contains("+d.supplierName+")").parent().parent().find('strong.red .totalB').hide();
                }
            });
            // 统计当前也数据
            $("#scrollDiv table").each(function(i,p_table){
                var page_total_tax=0;
                var page_total=0;
                $(p_table).find("tr").each(function(j,p_tr){
                    var v_confirmedAmout=$(p_tr).find(".confirmedAmout").val().replace(/,/g,'');//拟签数量
                    var v_confirmedPriceTaxed=$(p_tr).find(".confirmedPriceTaxed").val().replace(/,/g,'');//拟签单价
                    var v_confirmedUnitPrice=$(p_tr).find(".confirmedUnitPrice").val().replace(/,/g,'');//拟签单价
                    if(!isNaN(v_confirmedAmout)&&!isNaN(v_confirmedPriceTaxed)){
                        page_total_tax+=Number(v_confirmedAmout)*(v_confirmedPriceTaxed);
                    }
                    if(!isNaN(v_confirmedAmout)&&!isNaN(v_confirmedUnitPrice)){
                        page_total+=Number(v_confirmedAmout)*(v_confirmedUnitPrice);
                    }
                });
                eval("confirmedTotalPageTax.p"+i+"="+page_total_tax);
                eval("confirmedTotalPage.p"+i+"="+page_total);
            });
            calcConfirmedTotal_func();
        }});
}
/**
 * 检查录入数据
 * @returns {boolean}
 */
checkdata_fun=function(type){
    $(".scrollDiv").find("input:visible").css("background","#FFFFFF")
    var v_flag=false;
    var v_error_color="#AAAAAA";
    $(".scrollDiv>table").find("tr").each(function(){
        var v_a=$(this).find("td:eq(0)").find("input:visible");
        var v_b=$(this).find("td:eq(1)").find("input:visible");
        if($(v_a).val()!==''||$(v_b).val()!==''){
            if(compare(v_a,"submit")){
                $(v_a).css("background",v_error_color);
                v_flag=true;
            }
            if($(v_a).val()==''){
                $(v_b).css("background",v_error_color);
                v_flag=true;
            }
            if($(v_b).val()==''){
                $(v_b).css("background",v_error_color);
                v_flag=true;
            }

            if(isNaN($(v_a).val())){
                $(v_a).css("background",v_error_color);
                v_flag=true;
            }

            if(isNaN($(v_b).val())){
                $(v_b).css("background",v_error_color);
                v_flag=true;
            }
        }
    });
    if(v_flag){
        $.zui.messager.show('请检查拟签清单！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        return v_flag;
    }
    var v_count=0;
    $(".scrollDiv").find(".confirmedAmout:visible").each(function(i,d){
        v_count+=Number($(d).val());
    });
    if("launched"===type && Number(v_count)<=0){
        $.ajax({
            url: $("#contextPath").val()+"/designateResultUtils/checkData",
            data: {requestId: $("#requestId").val()},
            async:false,
            dataType: "json",
            type:"POST",
            success: function (data) {
                if (data.status == 'failed') {
                    $.zui.messager.show('sorry,核价内容为空，请进行核价！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                    v_flag=true;
                }
            },
            error: function () {
                $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                v_flag=true;
            }
        })
    }
    return v_flag;
}
/**
 * Excel模版下载
 */
exportFunc=function(){
    if($(".checkList .active").index()<0){
        $.zui.messager.show('请选择需要导出的模版类型！', {placement: 'center', type: 'info', icon: 'icon-exclamation-sign'});
        return false;
    }
    $.ajax({
        url: $("#contextPath").val()+"/designateResultExport/exportTemplate",
        data: {requestId: $("#requestId").val(), tax: $("#dropdownMenu1").text().indexOf("含税")>-1 , type: $(".checkList .active").index()+1},
        dataType: "json",
        type:"POST",
        success: function (data) {
            var url=data.filename;
            if($("#dowloadFrame").length<1){
                $("body").append("<iframe id='dowloadFrame' style='display:none'/>");
            }
            $("#dowloadFrame").attr("src",url);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
    $('#mbhj').trigger('click');
}

/**
 * 页面初始化
 */
$(document).ready(function(){
    querySupplierList_func();//统计已选供应商
});
