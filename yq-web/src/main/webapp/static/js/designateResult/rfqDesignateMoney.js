/**
 * Created by Administrator on 2016/11/9.
 */
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    var colModel=[
            {
                label: '物料代码',
                name: 'materialNo',
                index: 'materialNo',
                align:'center',
                width: 105,
                key: true,
            },
            {
                label: '物料名称',
                name: 'materialName',
                index: 'materialName',
                width: 103,
                align:'center',
                editable: true,
                editrules: {required: true},
            },
            {
                label: '型规',
                name: 'character',
                index: 'character',
                align:'center',
                width: 142,
                editable: true,
                editrules: {required: true}
            },
            {
                label: '品牌',
                name: 'producer',
                index: 'producer',
                align:'center',
                width: 120,
                editable: true,
                editrules: {required: true}
            },
            {
                width: 142,
                label: '数量',
                name: 'requestAmount',
                index: 'requestAmount',
                align:'center',
                sortable:false,
            },
            {
                width: 58,
                label: '单位',
                name: 'unit',
                index: 'unit',
                align:'center',
                sortable:false,
            },
            {
                width: 120,
                label: '自定义字段1',
                name: 'extendItemData',
                align:'center',
                sortable:false,
                hidden:true,
                /*formatter:designate*/
            },
            {
                width: 180,
                label: '要求交货期',
                name: 'requestDeliveryDate',
                index: 'requestDeliveryDate',
                align:'center',
                sortable:false,
            }
        ];
        $.post(ctx+'/rfqRequest/getCustomItemHeader',{}, function (data) {
        $.each(data.obj,function(i,item){
            var res={};
            res.label=item.itemName;
            res.name="itemValue_"+(i+1);
            res.width=80;
            colModel.push(res);
        });
        //console.log(colModel);
        RFQ.jqGrid('#jqGrid').init({
            url: ctx+'/designateContract/quateSupplie',
            colModel: colModel,
            postData: {
                requestId: $("#requestId").val()
            },
            jsonReader: { //映射server端返回的字段
                root: "list",
                rows: "list",
                page: "pageNum",
                total: "pages",
                records: "total"
            },
            prmNames: {
                id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
            },
            viewrecords: true, // show the current page, data rang and total records on the toolbar

            width: 1036,
            height: "100%",
            rowNum: 10,
            rowList: [10, 20, 30],
            sortname: 'REQUEST_DELIVERY_DATE',
            sortorder: 'asc',
            pager: "#jqGridPager",
            rownumbers:true
        });
    },'json');

    $("#jqGrid").jqGrid('setLabel', 0,"序号");
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    var rfqMethod = $("#rfqMethod").val();
    if(rfqMethod =='DAC'){
        $.ajax({
            type:"POST",
            url:"quateOffer",
            data:{requestId:$("#requestId").val(),},
            datatype: 'json',
            success: function (data) {
                var dataJson=eval("("+data+")");
                setEchat(dataJson.moneyList);
            },
            error: function () {
                $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        });
    }
});


   var htmlSupplier = '<li class="active"><a href="#tabgs0" data-toggle="tab">'+$("#supplierName").val()+'</a></li>';

   var htmlDetail = '<div class="tab-pane example active" id="tabgs0">' +
        '<input type="hidden" name="rfqQuotationNoticeList[0].supplierCode" value="'+$("#supplierNum").val()+'" />'+
        '<textarea id="contentSimple2" name="rfqQuotationNoticeList[0].memoDesc"'+
        'class= "form-control kindeditorSimple col-md-10" style=" width:100%;height:150px;color:#666;">'+$("#supplierName").val()+',恭喜您中标！</textarea></div>';

$('#myTabZB').html(htmlSupplier).tab();
$('#tabContent').html(htmlDetail);

$('textarea.kindeditorSimple').each(function () {
    KindEditor.create('#'+this.id, {
        resizeType: 1, allowPreviewEmoticons: false, allowImageUpload: false, items: [] ,afterBlur: function(){this.sync();}
    });
});