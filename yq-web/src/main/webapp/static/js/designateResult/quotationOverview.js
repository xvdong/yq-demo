
/*信息节点*/
//var oTbodyTr='<tr><td><span class="availableCount">0</span><div class="col-xs-12"><input type="hidden" class="form-control quotationItemId" value="";><input type="text" onkeyup="checkPrice(3, this)" onafterpaste="checkPrice(3, this)" onblur="checkPrice(3, this),compare(this)" class="form-control confirmedAmout remove";><input  type="hidden" class="form-control oldAmout remove";></div></td><td width="135"><span class="unitPrice"><div class="unitPriceA">未报价</div><div class="unitPriceB">未报价</div></span><div class="col-xs-12"><input type="text" onkeyup="checkPrice(2, this)" onafterpaste="checkPrice(2, this)" onblur="checkPrice(2, this)" class="form-control confirmedUnitPrice remove";><input type="hidden" class="form-control oldUnitPrice remove";><input type="text" onkeyup="checkPrice(1, this)" onafterpaste="checkPrice(1, this)" onblur="checkPrice(1, this)" class="form-control confirmedPriceTaxed remove";><input type="hidden" class="form-control oldUnitPriceTaxed remove";></div></td><td width="80"><span class="taxrate"></span></td></tr>';
var oTbodyTr='<tr><td class = "baojiaxiangqing"></td></tr>';


var oTbody='<table class="table table-bordered table-striped align-md bjzl-w3"><tbody></tbody></table>';

/*行*/
var wlTr='<tr><td width="80" class="rownum">1</td><td class="materialNo"></td><td width="80"  class="Material_name"></td><td width="100" class="Character"></td><td width="100" class="Request_amount"></td><td width="80"  class="Pack_manner"></td><td width="90"  class="Request_delivery_date"></td></tr>';
//var wlTr='<tr><td width="60" class="rownum">1</td><td width="80" class="materialNo"></td><td width="80"  class="Material_name"></td><td width="80"  class="Character"></td><td width="80"  class="Request_amount"></td><td width="80"  class="Pack_manner"></td><td width="80"  class="Unit"></td><td width="100"  class="Request_delivery_date"></td></tr>';

$(function () {
    $.ajax({
        url:$("#contextPath").val()+ "/designateResult/designate",
        data: {requestId: $("#requestId").val(), page: $("#pageNum").html(), size: $("#pageSize").val()},
        type: 'POST',
        dataType: "json",
        async:false,
        cache: false,
        success: function (data) {
            showTable(data);
            //console.log(data);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    })
});
var select = document.getElementById('pageSize');
select.onchange = function(){
    toPage($("#pageNum").html(), $("#pageSize").val());
}
$("#toPage").bind("click",function(){
    toPage($("#pageNo").val(), $("#pageSize").val());
});

function toPage(pageNum, pageSize) {

    if(Number(pageNum)>Number($("#pages").text())
        ||
        Number(pageNum)<0){
        return false;
    }

    /**清除数据叠加*/
    $('#div3 .scrollDiv>div').remove();
    $('#div2 #scrollDiv>table').remove();
    $('.hjmx-tbody .hjmx-tbody-gd table tbody>tr').remove();

$.ajax({
    url: $("#contextPath").val()+"/designateResult/designate",
    data: {requestId: $("#requestId").val(), page: pageNum, size: pageSize},
    dataType: "json",
    async:false,
    type:"POST",
    success: function (data) {
        showTable(data);
    },
    error: function () {
        $.zui.messager.show('请输入正确页面信息！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
    }
});
}



var scrollK = 0;
/** 表格动态展开*/
function showTable(data) {
    /** 分页信息*/
    var pageNum = data.pageInfo.pageNum;
    var pages = data.pageInfo.pages;
    var pageSize = data.pageInfo.pageSize;
    $('#total').html(data.pageInfo.total);
    $('#pageNum').html(pageNum);
    $('#pages').html(pages);
    $('#pageSize').val(pageSize);
    if (pages > 1 && pageNum < pages) {
        $("#nextPage").replaceWith('<li id="nextPage"><a href="javascript:toPage('+(Number(pageNum)+1)+', '+pageSize+')">下一页</a></li>');
        $("#endPage").replaceWith('<li id="endPage"><a href="javascript:toPage('+pages+', '+pageSize+')">尾页</a></li>');
    } else {
        $("#nextPage").replaceWith('<li id="nextPage"><a class="gray">下一页</a></li>');
        $("#endPage").replaceWith('<li id="endPage"><a class="gray">尾页</a></li>');
    }
    if (pageNum > 1) {
        $("#homePage").replaceWith('<li id="homePage"><a href="javascript:toPage(1, '+pageSize+')">首页</a></li>');
        $("#lastPage").replaceWith('<li id="lastPage"><a href="javascript:toPage('+(Number(pageNum)-1)+', '+pageSize+')">上一页</a></li>');
    } else {
        $("#homePage").replaceWith('<li id="homePage"><a class="gray">首页</a></li>');
        $("#lastPage").replaceWith('<li id="lastPage"><a class="gray">上一页</a></li>');
    }
    /**报价明细*/
    $('.hj-tb-xd .panel>div').find('.mg-sand a').html(data.quotationList.length);
    /** 供应商表头信息*/
    $.each(data.quotationList, function (i, key) {

        /*公司节点*/
        var oT='<div class="bjzl-h bjzl-w2"><div class="t"><h4><a id=\'' + i + '\'  class="blue" target="_blank"></a></h4><a class="group-bjxq" target="myquotationView" href="myquotationView.html" ><span class="label label-warning">报价详情</span> <span class="red weiShui" style="display: inline">0.00</span>元</div></div>';
        //var oT='<div><div class="t"><h4><a id=\'' + i + '\'  class="blue" target="_blank"></a></h4><a class="group-bjxq" target="myquotationView" href="myquotationView.html" ><span class="label label-warning">报价详情</span>&nbsp;<span class="red weiShui" style="display: inline">0.00</span>元</table></div>';
        $('#div3 .scrollDiv').append(oT);
        $('#div3 .scrollDiv>div').find('h4 a').eq(i).html(key.supplierName);
        $("#"+i).attr("href","http://eps.baosteel.net.cn/eps_eis//overview/BusinessResume.do?method=resume&companyCode="+key.supplierNum+"&companyId="+key.companyId);
        /*$('#div3 .scrollDiv>div').find('.jine').eq(i).html(key.subtotalTaxed);*/
        if(price_Type == "1"){
            //$('.hjmx-th-gd>div').find('.weiShui').eq(i).html(key.subtotalTaxed);
            $('#div3 .scrollDiv>div').find('.weiShui').eq(i).html(key.subtotalTaxed);
        }else {
            $('#div3 .scrollDiv>div').find('.weiShui').eq(i).html(key.subtotal);
        }
        $('#div3 .scrollDiv>div').find('.group-bjxq').eq(i).attr("href", $("#contextPath").val() + "/rfqQuotation/getQuotationDetail?id=" + key.id + "&type=1");
        /*if (key.isRemark != "0") {
            $('#div3 .scrollDiv>div .remark').eq(i).html('');
        }
        if (key.isAttachment != "1") {
            $('#div3 .scrollDiv>div .attachment').eq(i).html('');
        }*/
        //changeBar();
    });
    /** 供应商报价、核价信息*/
    $.each(data.requestItemList[0].quotationItemList, function (i, key) {
        $('#div2 #scrollDiv').append(oTbody);
    });
    /** 物料基本信息*/
    $.each(data.requestItemList, function (i, key) {
        if(i<data.requestItemList.length){
            $('.hjmx-tbody .hjmx-tbody-gd table tbody').append(wlTr);
            $('#div2 #scrollDiv table tbody').append(oTbodyTr);
            var cgxxTr = $('.hjmx-tbody .hjmx-tbody-gd table tbody tr');
            /*cgxxTr.eq(i).find('.materialName').html(key.baseInfo);*/
            cgxxTr.eq(i).find('.materialNo').html(key.materialNo);
            cgxxTr.eq(i).find('.Material_name').html(key.materialName);
            cgxxTr.eq(i).find('.Character').html(key.character);
            cgxxTr.eq(i).find('.Request_amount').html(key.requestAmount);
            cgxxTr.eq(i).find('.Pack_manner').html(key.salePrice);
            cgxxTr.eq(i).find('.Unit').html(key.unit);
            cgxxTr.eq(i).find('.Request_delivery_date').html(key.requestDeliveryDate.substring(0,10));
            cgxxTr.eq(i).find('.rownum').html(key.rownum);
            changeBar(key.minTaxedPrice);
        }
    });


    /** 供应商报价*/
    $.each(data.requestItemList, function (i, key) {
        var minPriceA = key.minTaxedPrice;
        var minPriceB = key.minPrice;
        $.each(key.quotationItemList, function (j, val) {
            var oTableTr = $('#div2 #scrollDiv table').eq(j).find('tr').eq(i);
            if(price_Type == "1"){
                oTableTr.find('.baojiaxiangqing').html(val.unitPriceTaxed);
                if (minPriceA != null && minPriceA != '' && minPriceA == val.unitPriceTaxed) {
                    oTableTr.find('.baojiaxiangqing').css("color","red");
                }
                //changeBar(key.minTaxedPrice);
            }else {
                oTableTr.find('.baojiaxiangqing').html(val.unitPrice);
                if (minPriceB != null && minPriceB != '' && minPriceB == val.unitPrice) {
                    oTableTr.find('.baojiaxiangqing').css("color","red");
                }
                //changeBar(key.minPrice);
            }
            //changeBar();
        });
        changeBar();
        /** 表格自定义滚动展现*/
        var listSize = data.quotationList.length;
        //console.log(listSize);
        if (listSize <= 1) {
            scrollK = 1;
            $('#div3').attr("style","left: 0;");
            $('#scrollDiv').attr("style","left: 0;");
        } else {
            scrollK = 0;
        }
        scroll(scrollK);
    });

}

function changeBar(){
    $('.hjmx-th-fd .scrollDiv>div:odd').css('background','#fff1d5');
    $('.hjmx-th-fd .scrollDiv>div:even').css('background','#ddf3f5');
    $('.scrollDiv').css('width',$('.scrollDiv>div').length*($('.scrollDiv>div').width()));
    $('.hjmx-tbody .hjmx-tbody-fd').height($('.hjmx-tbody-gd').height());

    $('.hj-tb').height($('.hjmx-tbody-gd').height()+10);
    $('#bar-row').css({
        'top':$('.hjmx-tbody-gd').height(),
        'width':$('.hjmx-tbody-fd').width()
    });
    $('.hjmx-th').width($('.hjmx-tbody').width()-2);
}

function scroll(k){
    myWeb.scrollLeft("bar-row","div2","div3",k);
}
