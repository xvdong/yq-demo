/*信息节点*/
var oTbodyTr='<tr><td><span class="availableCount">0</span><div class="col-xs-12"><input type="hidden" class="form-control quotationItemId" value="";><input type="text" onkeyup="checkPrice(3, this)" onafterpaste="checkPrice(3, this)" onblur="checkPrice(3, this),compare(this)" class="form-control confirmedAmout remove";><input  type="hidden" class="form-control oldAmout remove";></div></td><td width="135"><span class="unitPrice"><a class="unitPriceA">未报价</a><a class="unitPriceB">未报价</a></span><div class="col-xs-12"><input type="text" onkeyup="checkPrice(2, this)" onafterpaste="checkPrice(2, this)" onblur="checkPrice(2, this)" class="form-control confirmedUnitPrice remove";><input type="hidden" class="form-control oldUnitPrice remove";><input type="text" onkeyup="checkPrice(1, this)" onafterpaste="checkPrice(1, this)" onblur="checkPrice(1, this)" class="form-control confirmedPriceTaxed remove";><input type="hidden" class="form-control oldUnitPriceTaxed remove";></div></td><td width="80"><span class="taxrate"></span></td></tr>';

var oTbody='<table class="table table-bordered table-striped align-md"><tbody></tbody></table>';

/*行*/
var wlTr='<tr><td width="60" class="rownum">1</td><td><div class="zkDiv"><span class="materialName"></span><a href="javascript:;" class="zkBtn icon icon-chevron-down"></a><div class="hidden moreInfo">更多说明更多说明更多说明更多说明更多说明更多说明更多说明</div></div></td><td width="80" class="requestAmount">0个</td><td width="100" class="minPrice"><a class="minPriceA"></a><a class="minPriceB"></a></td></tr>';
var changeVar="含税核价";
$(function () {
    if("true"===window.sessionStorage.designateResult_tax){
        changeVar="含税核价";
    }else{
        changeVar="未税核价";
    }

    $.ajax({
        url:$("#contextPath").val()+ "/designateResult/designate",
        data: {requestId: $("#requestId").val(), page: $("#pageNum").html(), size: $("#pageSize").val()},
        type: 'POST',
        dataType: "json",
        success: function (data) {
            showTable(data);
            calcResult();//初始化拟签小计
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    })
    //querySupplierList_func();//统计已选供应商
});
var select = document.getElementById('pageSize');
select.onchange = function(){
    toPage($("#pageNum").html(), $("#pageSize").val());
}
$("#toPage").bind("click",function(){
    toPage($("#pageNo").val(), $("#pageSize").val());
});

function toPage(pageNum, pageSize) {
    if(Number(pageNum)>Number($("#pages").text())
        ||
        Number(pageNum)<1){
        return false;
    }
    /**清除数据叠加*/
    $('#div3 .scrollDiv>div').remove();
    $('#div2 #scrollDiv>table').remove();
    $('.hjmx-tbody .hjmx-tbody-gd table tbody>tr').remove();

    $.ajax({
        url: $("#contextPath").val()+"/designateResult/designate",
        data: {requestId: $("#requestId").val(), page: pageNum, size: pageSize},
        type: 'POST',
        dataType: "json",
        success: function (data) {
            showTable(data);
            calcResult();//初始化拟签小计
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });

}
var scrollK = 0;
/** 表格动态展开*/
function showTable(data) {
    /** 分页信息*/
    /** 分页信息*/
    var pageNum = data.pageInfo.pageNum;
    var pages = data.pageInfo.pages;
    var pageSize = data.pageInfo.pageSize;
    $('#total').html(data.pageInfo.total);
    $('#pageNum').html(pageNum);
    $('#pages').html(pages);
    $('#pageSize').val(pageSize);
    if (pages > 1 && pageNum < pages) {
        $("#nextPage").replaceWith('<li id="nextPage"><a href="javascript:toPage('+(Number(pageNum)+1)+', '+pageSize+')">下一页</a></li>');
        $("#endPage").replaceWith('<li id="endPage"><a href="javascript:toPage('+pages+', '+pageSize+')">尾页</a></li>');
    } else {
        $("#nextPage").replaceWith('<li id="nextPage"><a class="gray">下一页</a></li>');
        $("#endPage").replaceWith('<li id="endPage"><a class="gray">尾页</a></li>');
    }
    if (pageNum > 1) {
        $("#homePage").replaceWith('<li id="homePage"><a href="javascript:toPage(1, '+pageSize+')">首页</a></li>');
        $("#lastPage").replaceWith('<li id="lastPage"><a href="javascript:toPage('+(Number(pageNum)-1)+', '+pageSize+')">上一页</a></li>');
    } else {
        $("#homePage").replaceWith('<li id="homePage"><a class="gray">首页</a></li>');
        $("#lastPage").replaceWith('<li id="lastPage"><a class="gray">上一页</a></li>');
    }
    /** 供应商表头信息*/
    $.each(data.quotationList, function (i, key) {

        /*公司节点*/
        var oT='<div><div class="t"><h4><a id=\'' + i + '\'  class="blue" target="_blank"></a></h4><a class="group-bjxq" target="myquotationView" href="myquotationView.html" ><span class="label label-warning">报价详情</span>&nbsp;<span class="red weiShui" style="display: none">0.00</span><span class="red jine" style="display: inline">0.00</span> 元</a><span class="green attachment">附</span> <span class="red remark">注</span><span class="ml12">拟签小计：<strong class="red"><a class="red totalA">0.00</a><a class="red totalB">0.00</a></strong>元</span></div><table class="table table-bordered align-md"><tr><th width="">可供数量<br>拟签数量</th><th class="changeTax" width="135">含税报价单价(元)<br>含税拟签单价(元)</th><th width="80">税率</th></tr></table></div>';
        $('#div3 .scrollDiv').append(oT);
        $('#div3 .scrollDiv>div').find('h4 a').eq(i).html(key.supplierName);
        $('#div3 .scrollDiv>div').find('.jine').eq(i).html(key.subtotalTaxed);
        $('#div3 .scrollDiv>div').find('.weiShui').eq(i).html(key.subtotal);
        $('#div3 .scrollDiv>div').find('.group-bjxq').eq(i).attr("href", $("#contextPath").val() + "/rfqQuotation/getQuotationDetail?id=" + key.id + "&type=1");
        // $('#div3 .scrollDiv>div').find('.jine').eq(i).attr("href",$("#contextPath").val()+"/rfqQuotation/getQuotationDetail?id="+key.id+"&type=1");
        // $('#div3 .scrollDiv>div').find('.weiShui').eq(i).attr("href",$("#contextPath").val()+"/rfqQuotation/getQuotationDetail?id="+key.id+"&type=1");
        $("#"+i).attr("href","http://eps.baosteel.net.cn/eps_eis//overview/BusinessResume.do?method=resume&companyCode="+key.supplierNum+"&companyId="+key.companyId);
        if (key.isRemark != "1") {
            $('#div3 .scrollDiv>div .remark').eq(i).html('');
        }
        if (key.isAttachment != "1") {
            $('#div3 .scrollDiv>div .attachment').eq(i).html('');
        }
        if (changeVar == '含税核价') {
            $('#div3 .scrollDiv>div').find('.jine').eq(i).show();
            $('#div3 .scrollDiv>div').find('.weiShui').eq(i).hide();
        }else{
            $('#div3 .scrollDiv>div').find('.jine').eq(i).hide();
            $('#div3 .scrollDiv>div').find('.weiShui').eq(i).show();
        }

    });
    /** 供应商报价、核价信息*/
    $.each(data.requestItemList[0].quotationItemList, function (i, key) {
        $('#div2 #scrollDiv').append(oTbody);
    });
    /** 物料基本信息*/
    $.each(data.requestItemList, function (i, key) {
        if(i<data.requestItemList.length){
            var info;
            $('.hjmx-tbody .hjmx-tbody-gd table tbody').append(wlTr);
            $('#div2 #scrollDiv table tbody').append(oTbodyTr);

            var cgxxTr = $('.hjmx-tbody .hjmx-tbody-gd table tbody tr');
            cgxxTr.eq(i).find('.materialName').html(key.baseInfo);
            if($("#dataSource").val()=='JFE' || $("#dataSource").val()=='NBBX'){
                var drawing=key.drawingNo==null?'':key.drawingNo;
                var more;
                if($("#dataSource").val()=='JFE'){
                    more="申购单号:"+key.purchaseNo+"<br/>"+"申购人:"+key.purchasePerson+"<br/>"+"图号:"+drawing;
                }else{
                    more="材质/图号:"+drawing;
                }
                var k=key.moreInfo.split('<br/>');
                k.splice(5,0,more);
                info=k.toString().split(',').join('<br/>');
            }else{
                info=key.moreInfo;
            }
            cgxxTr.eq(i).find('.moreInfo').html(info);
            cgxxTr.eq(i).find('.requestAmount').html(key.requestAmount);

            if (key.minTaxedPrice == null || key.minTaxedPrice ==''){//未报价则空
                cgxxTr.eq(i).find('.minPriceA').html("");
            }else {
                cgxxTr.eq(i).find('.minPriceA').html(key.minTaxedPrice+'<br>'+" "+key.salePrice);//中间的空格是截取标记
            }
            if (key.minTaxedPrice == null || key.minTaxedPrice ==''){
                cgxxTr.eq(i).find('.minPriceB').html("");
            }else {
                cgxxTr.eq(i).find('.minPriceB').html(key.minPrice+'<br>'+" "+key.salePrice);
            }

            cgxxTr.eq(i).find('.rownum').html(key.rownum);
            if (changeVar == '含税核价') {
                cgxxTr.eq(i).find('.minPriceA').show();
                cgxxTr.eq(i).find('.minPriceB').hide();
            } else {
                cgxxTr.eq(i).find('.minPriceA').hide();
                cgxxTr.eq(i).find('.minPriceB').show();
            }
        }
    });
    /** 供应商报价、核价详细信息*/
    $.each(data.requestItemList, function (i, key) {
        $.each(key.quotationItemList, function (j, val) {
            var oTableTr = $('#div2 #scrollDiv table').eq(j).find('tr').eq(i);
            if (changeVar == '含税核价') {
                $('.changeTax').replaceWith('<th class="changeTax" width="135">含税报价单价(元)<br>含税拟签单价(元)</th>');
                oTableTr.find('.unitPriceA').show();
                oTableTr.find('.confirmedPriceTaxed').show();
                oTableTr.find('.unitPriceB').hide();
                oTableTr.find('.confirmedUnitPrice').hide();
            } else {
                $('.changeTax').replaceWith('<th class="changeTax" width="135">未税报价单价(元)<br>未税拟签单价(元)</th>');
                oTableTr.find('.unitPriceA').hide();
                oTableTr.find('.confirmedPriceTaxed').hide();
                oTableTr.find('.confirmedUnitPrice').show();
                oTableTr.find('.unitPriceB').show();
            }
            if (val && val.id != null) {
                oTableTr.find('.taxrate').html(val.tax + '%');
                oTableTr.find('.availableCount').html(val.availableCount);
                oTableTr.find('.quotationItemId').val(val.id);
                oTableTr.find('.unitPriceA').html(val.unitPriceTaxed);
                oTableTr.find('.unitPriceB').html(val.unitPrice);
                oTableTr.find('.confirmedAmout').val(val.confirmedAmout);
                oTableTr.find('.oldAmout').val(val.confirmedAmout);
                oTableTr.find('.confirmedPriceTaxed').val(val.confirmedPriceTaxed);
                oTableTr.find('.oldUnitPriceTaxed').val(val.confirmedPriceTaxed);
                oTableTr.find('.confirmedUnitPrice').val(val.confirmedUnitPrice);
                oTableTr.find('.oldUnitPrice').val(val.confirmedUnitPrice);
                if (minPriceA != null && minPriceA != '' && minPriceA == val.unitPriceTaxed) {
                    oTableTr.find('.unitPriceA').css("color","red");
                }
                if (minPriceB != null && minPriceB != '' && minPriceB == val.unitPrice) {
                    oTableTr.find('.unitPriceB').css("color","red");
                }
            }
        });
    });
    changeBar();
    /** 表格自定义滚动展现*/
    var listSize = data.quotationList.length;
    if (listSize <= 1) {
        scrollK = 1;
        $('#div3').attr("style","left: 0;");
        $('#scrollDiv').attr("style","left: 0;");
    } else {
        scrollK = 0;
    }
    scroll(scrollK);
    $("input[type=text]").attr("readonly","readonly");//授标页面独有
    $('#pageNo').attr("readonly",false);//允许页面跳转输入
    $("#myRole").find(':text').attr("readonly",false);//允许myRole页面输入

    $('.zkBtn').rotateArrow();
    $('.zkBtn').on("click", function () {
        var This = $(this);
        $(this).next().toggleClass('hidden');
        changeBar();
    });
}
/*/!**含(未)税切换*!/
var aaaa=$('#changeZt').find('.v').html();
$('#changeZt').find('li>a').click(function(){
    if($(this).html()!=aaaa){
        aaaa=$(this).html();
        if (aaaa=='含税核价') {
            $('.changeTax').replaceWith('<th class="changeTax" width="135">含税报价单价(元)<br>含税拟签单价(元)</th>');
            $(".scrollDiv>table").find("tr").each(function(){
                $(this).find('.unitPriceA').show();
                $(this).find('.confirmedPriceTaxed').show();
                $(this).find('.unitPriceB').hide();
                $(this).find('.confirmedUnitPrice').hide();
            });
            $('.hjmx-tbody .hjmx-tbody-gd table tbody tr').each(function(){
                $(this).find('.minPriceA').show();
                $(this).find('.minPriceB').hide();
            });
        } else {
            $('.changeTax').replaceWith('<th class="changeTax" width="135">未税报价单价(元)<br>未税拟签单价(元)</th>');
            $(".scrollDiv>table").find("tr").each(function(){
                $(this).find('.unitPriceA').hide();
                $(this).find('.confirmedPriceTaxed').hide();
                $(this).find('.unitPriceB').show();
                $(this).find('.confirmedUnitPrice').show();
            });
            $('.hjmx-tbody .hjmx-tbody-gd table tbody tr').each(function(){
                $(this).find('.minPriceA').hide();
                $(this).find('.minPriceB').show();
            });
        }
    }
});*/

/**
 * 供应商筛选列表查询
 */
/*querySupplierList_func=function(){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/querySupplierList",
        dataType: "json",
        data:{requestId:$("#requestId").val()},
        success: function(data){
            $("#supplierList>tbody").html("");//清空供应商筛选表格
            $.each(data,function(i,d){
                var template="<tr> <th><input type='checkbox' name='supplierSel' ${isSelected} value='${supplierCode}'></th><td>${supplierName}</td><td><a class='red' target='myquotationView' href='${quotationUrl}'>${subtotalTaxed}</a></td><td>${quotationCount}</td><td>${minQuotationCount}</td><td>${isAttachment}</td><td>${isRemark}</td></tr>";
                var v_Y="<a class='icon icon-check green'></a>";
                var v_N="<a class='icon icon-times red2'></a>";
                template=template.replace("${supplierCode}", d.quotateId);
                template=template.replace("${supplierName}", d.supplierName);
                template=template.replace("${subtotalTaxed}", d.subtotalTaxed);
                template=template.replace("${quotationCount}", d.itemCount);
                template=template.replace("${minQuotationCount}", d.itemCountMin==null?'0':d.itemCountMin);
                template=template.replace("${isSelected}", d.selected?"checked='checked'":"");
                template=template.replace("${isAttachment}", d.isAttachment=="1"?v_Y:v_N);
                template=template.replace("${isRemark}", d.isRemark=="0"?v_Y:v_N);
                template=template.replace("${quotationUrl}",$("#contextPath").val()+"/rfqQuotation/getQuotationDetail?id="+d.quotateId+"&type=0");
                $("#supplierList>tbody").append(template);
            });
            var supplierList=$("#supplierList");
            $(".mg-text-yelloy").text("("+$(supplierList).find("input[name=supplierSel]:checked").length+"/"+$(supplierList).find("input[name=supplierSel]").length+")");
            $("#supplierSelTh").bind("click",supplierSelThClick);
        }});
}*/

/**
 * 供应商筛选列表查询-事件
 */
//$("#gyssx").bind("click",querySupplierList_func);
/**
 * 供应商筛选全选与反选
 */
/*supplierSelThClick=function(){
    $("#supplierList :checkbox[name=supplierSel]").prop("checked",$("#supplierSelTh").is(":checked"));
}*/

/**
 *供应商筛选保存
 */
/*supplierSubmitFunc=function(){
    var supplierSelStr="";
    var supplierList=$("#supplierList");
    $(supplierList).find("input[name=supplierSel]:checked").each(function(){
        supplierSelStr+=$(this).val()+",";
    });
    if(supplierSelStr.length>0){
        supplierSelStr=supplierSelStr.substr(0,supplierSelStr.length-1);
    }
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/setSupplierList",
        dataType: "text",
        data:{requestId:$("#requestId").val(),supplierIds:supplierSelStr},
        success: function(data){
            $(".mg-text-yelloy").text("("+$(supplierList).find("input[name=supplierSel]:checked").length+"/"+$(supplierList).find("input[name=supplierSel]").length+")");
            $('#gyssx').trigger('click');
            toPage($("#pageNo").val(), $("#pageSize").val());
        }});
}*/
/**
 * 重新核价
 */
/*$("#designateResultAgain").bind("click",function(){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/designateResultAgain",
        dataType: "text",
        data:{requestId:$("#requestId").val()},
        success: function(data){
            if("true"==data){
                //window.location.reload();
                toPage($("#pageNo").val(), $("#pageSize").val());
            }else{
                $.zui.messager.show('请先保存一次核价信息！', {placement: 'center', type: 'info', icon: 'icon-exclamation-sign'});
            }
        }});
});*/
/**
 * 设置价格筛选工具
 */
/*setPriceTools=function(p_type,p_targetSupplierCode){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/setPriceTools",
        dataType: "text",
        data:{requestId:$("#requestId").val(),type:p_type,targetSupplierCode:p_targetSupplierCode},
        success: function(data){
            if("true"==data){
                //alert("计算成功！");
                toPage($("#pageNo").val(), $("#pageSize").val());
            }else{
                $.zui.messager.show('工具计算错误！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }});
}*/

//$("#supplierSubmit").bind("click",);

/**
 * 设置价格筛选工具1-2
 */
/*setPriceToolsByTargetSupplier=function(){
    setPriceTools('1',$("#targetSupplier").val());
    $("#targetSupplier").val("")
    $(".close").click();
}*/
/**
 * 设置价格筛选工具的目标供应商
 */
/*setSelSupplier=function(){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/querySupplierList",
        dataType: "json",
        data:{requestId:$("#requestId").val()},
        success: function(data){
            $("#targetSupplier").html("");
            $.each(data,function(i,d){
                if(d.selected){
                    $("#targetSupplier").append("<option value='"+ d.supplierCode+"'>"+ d.supplierName+"</option>");
                }
            });
            $("#targetSupplier").val("");
        }});
}*/

/*func_caleRowItem=function(){
    var v_unit_price_sum=0;
    var v_amout_sum=0;
    $("input[name=confirmedAmoutByRow]").each(function(i,d){
        var v_amout=$(d).val();
        if(v_amout!=""){
            v_amout_sum=v_amout_sum+Number(v_amout);
            var v_unitPrice=$(d).parent().parent().find("input[name=confirmedUnitPriceByRow]").val();
            if(v_unitPrice!=""){
                v_unit_price_sum+=Number(v_unitPrice)*Number(v_amout);
            }
        }
    });
    $("#myModa3_unit_price_sum").html(v_unit_price_sum);
    $("#myModa3_v_amout_sum").html(v_amout_sum);
}*/
/**
 * 拟签合计
 */
calcResult=function(){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/calcResult",
        dataType: "json",
        data:{requestId:$("#requestId").val()},
        type:"POST",
        success: function(data){
            $.each(data.resaultDetail,function(i,d){
                if (changeVar == '含税核价') {
                    $("a:contains("+d.supplierName+")").parent().parent().find('strong.red').text(d.confirmedTotalTaxed);
                } else {
                    $("a:contains("+d.supplierName+")").parent().parent().find('strong.red').text(d.confirmedTotal);
                }

            });
            //求和
            var total=0;
            $(".scrollDiv strong.red").each(function(i,d){
                total=total+Number($(d).text().replace(/,/g,"")-0);
            });
            $(".hjmx-th-gd strong.red").text(total.toFixed(2));
            $("#confirmedPrice").text(total.toFixed(2));
        }});

}

//**********************************************************************************************************************
//**********************************************************************************************************************
// 授标部分JS
//---------------------------------------------------------------------------------------------------------------------
submit_modal_func=function(){
    if($("#resultMemo").val()==""){
        $.zui.messager.show('请录入核价说明！', {placement: 'center', type: 'info', icon: 'icon-exclamation-sign'});
        return false;
    }
    var memo=$("#resultMemo").val();
    if(memo.length>2000){
        $.zui.messager.show('核价说明不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return false;
    }
    $("#myModa6").modal('show');
}

/**
 * 授标提交
 */
submit_func=function(){
    if($("#resultMemo").val()==""){
        $.zui.messager.show('请录入核价说明！', {placement: 'center', type: 'info', icon: 'icon-exclamation-sign'});
        return false;
    }
    /* var memo=$("#resultMemo").val();
     if(memo.replace(/[^\x00-/xff]/g,"aa").length>2000){
     $.zui.messager.show('核价说明不可超过2000个字符！', {placement: 'center', type: 'info', icon: 'icon-exclamation-sign'});
     return false;
     }*/
    var selAudit = $('#approvalCode').val();
    var procDefType = selAudit.split(",")[1];
    if(selAudit != 0) {   //选择审批
        if (procDefType == 2) {   //灵活流程
            $('#myModa6').modal('hide');
            $('#myRole').modal('show');
            return;
        } else {
            var $confirmBtn = $('#myModa6').find('.modal-footer .btn-primary');
            $confirmBtn.attr('disabled', true);
            postforData([]);
        }
    }else {
        var $confirmBtn = $('#myModa6').find('.modal-footer .btn-primary');
        $confirmBtn.attr('disabled', true);
        postforData([]);
    }
}

//ajax提交方法
function postforData(rowDataLists){
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/grantResult",
        dataType: "text",
        type:"POST",
        async:false,
        data:{
            requestId:$("#requestId").val(),
            resultMemo:$("#resultMemo").val(),
            approvalCode:$("#approvalCode").val(),
            unifiedRfqNum:$("#unifiedRfqNum").val(),
            priceType:window.sessionStorage.designateResult_tax === "true" ? "1" : "0",
            rowDataLists: JSON.stringify(rowDataLists)
        },
        success: function(data){
            if("true"==data){
                window.location.href=$("#contextPath").val()+"/rfqRequest/init";
            }else{
                $.zui.messager.show('授标失败，请检查结果审批流！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    });
}

$("#resultMemoSave").bind("click",function(){
    if($("#resultMemo").val()==""){
        $.zui.messager.show('请录入核价说明！', {placement: 'center', type: 'info', icon: 'icon-exclamation-sign'});
        return false;
    }
    var memo=$("#resultMemo").val();
    if(memo.length>2000){
        $.zui.messager.show('核价说明不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
        return false;
    }
    $.ajax({
        url: $("#contextPath").val()+"/designateResultUtils/saveGrantResult",
        type:"POST",
        async:false,
        data:{
            requestId:$("#requestId").val(),
            resultMemo:$("#resultMemo").val()
        },
        success:function(data){
            $.zui.messager.show('保存成功!', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
        },
        error: function () {
            $.zui.messager.show('保存失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });


});