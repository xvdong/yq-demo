$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

        $("#jqGrid1").jqGrid({
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialApprovalDetail",
            datatype: 'json',
            mtype: 'POST',
            rownumbers:true,
            rownumWidth:50,
            colModel: [
                {
                    label: '主键id',
                    name: 'id',
                    index: 'ID',
                    width: 30,
                    hidden:true
                },
                {
                    label: '供应商U代码',
                    name: 'supplierCode',
                    index: 'SUPPLIER_CODE',
                    width: 30
                },
                {
                    label: '供应商名称',
                    name: 'supplierName',
                    index: 'SUPPLIER_NAME',
                    width: 40,
                    formatter: function(cellvalue, options,rawObject){
                        if( 0 == rawObject.isSupplier  ){
                            return rawObject.supplierName + '&nbsp;<a class="label label-warning " href="javascript:;">待考察</a>';
                        }else{
                            return rawObject.supplierName  + '&nbsp;<a class="label label-success " href="javascript:;">合格</a>';
                        }
                    }
                },
                {
                    label: '缴纳时间',
                    name: 'assureOrTenderPayDate',
                    index: 'assureOrTenderPayDate',
                    align:'center',
                    width: 50
                },
                {
                    label: '缴纳金额',
                    name: 'assureOrTenderMoney',
                    index: 'assureOrTenderMoney',
                    align:'center',
                    width: 20
                },
                {
                    label: '财务状态',
                    name: 'rfqFinancialStatus',
                    index: 'rfqFinancialStatus',
                    align:'center',
                    width: 20,
                   formatter: formatRfqFinancialStatus
                },
                {
                    width: 20,
                    label: '操作',
                    name: 'requestId',
                    index: 'REQUEST_ID',
                   formatter: formatOptionStatus
                }
            ],
            jsonReader: { //映射server端返回的字段
                root: "list",//
                rows: "list",
                page: "pageNum",
                total: "pages",
                records: "total"
            },
            prmNames: {
                id: 'ruleId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
            },
            postData:{
                unifiedRfqNum:$("#unifiedRfqNum").val(),
                moneyOrFlagType:$("#moneyOrFlagType").val(),
                requestId:$("#requestId").val()
            },
            viewrecords: true, // show the current page, data rang and total records on the toolbar
            width: 980,
            height: "100%",
            rowNum: 10,
            rowList: [10, 20, 30],
            sortname: 'requestId',
            sortorder: 'asc',
            pager: "#jqGridPager1"

        });
        $("#jqGrid1").jqGrid('setLabel', 0,"序号");


    //设置询单号的格式
    function formatunifiedRfqNum(cellvalue, options, rawObject){

        var unifiedRfqNum = rawObject.unifiedRfqNum;
        var id = rawObject.requestId;
        var rval = '<a class="blue" href="javascript:rfqRequestDetail(\'' + id + '\')" target="10-ggmk-xunjiadanxiangqing">'+unifiedRfqNum+'</a>';
        return rval;
    }
    //判断财务状态
    function formatRfqFinancialStatus(cellvalue, options, rawObject){
        var rval = '';
        var type =$("#type").val();

            //待报价-未缴纳--->未缴纳
            if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&"0"===cellvalue){
                rval = '未缴纳';
            //待报价--已缴纳 -->已缴纳
            }else if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&"1"===cellvalue){
                rval = "已缴纳";
            //已结束--未缴纳--->已退回
            }else if ("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&"0"===cellvalue){
                rval = "已退回";
            //已结束--已缴纳--->已缴纳
            }else if ("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&"1"===cellvalue){
                rval = "已缴纳";
            }
        return rval;
    }
    //设置操作的格式
    function formatOptionStatus(cellvalue, options, rawObject){
        var rval = '';
        var id = rawObject.id;//获取供应商id
        var moneyOrFlagType = $("#moneyOrFlagType").val();
        var financialStatus = rawObject.rfqFinancialStatus;//获取财务状态值
        //获取询单状态值
        var type =$("#type").val();
        //待报价--已缴纳--->空
        if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&'1'===financialStatus){
            rval = '';
        //待报价--未缴纳--->缴纳
        }else if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&'0'===financialStatus){
            rval = '<span class="ml5"><a class="btn btn-sm btn-warning" onclick="updateAssureMoneyOrTenderStatus1(\'' + id+'\'' +'\,'+'\''+ moneyOrFlagType+'\''+'\,'+'\''+ type+'\''+'\,' +'\''+financialStatus+ '\')"><i class="icon icon-check-sign"></i> 缴纳</a></span>';
        //已结束--已缴纳-->退回
        }else if("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&'1'===financialStatus){
            rval = '<span class="ml5"><a class="btn btn-sm btn-warning" onclick="updateAssureMoneyOrTenderStatus(\'' + id+'\'' +'\,'+'\''+ moneyOrFlagType+'\''+'\,'+'\''+ type+'\''+'\,' +'\''+financialStatus+ '\''+'\,'+'\''+cellvalue+'\')"><i class="icon icon-reply"></i> 退回</a></span>';
        //已结束--已退回-->查看
        }else if("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&'0'===financialStatus){
            rval = '';
        }
        return rval;
    }
    //设置标题--截取标题
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    }
//判断财务状态
    function formatFfqMethod(cellvalue, options, rawObject){
        var rval = '';
        switch (cellvalue) {
            case 'RAQ': rval = '询比价'; break;
            case 'DAC': rval = "招投标"; break;
            case '反向竞拍': rval = "反向竞拍"; break;
            default:
                break;
        }
        return rval;
    }
});

function updateAssureMoneyOrTenderStatus1(id,moneyOrFlagType,type,financialStatus){
    var url = ctx+"/rfqFinancialApprovalList/updateAssureMoneyOrTenderStatus?id=" + id+"&moneyOrFlagType="+moneyOrFlagType+"&type="+type+"&financialStatus="+financialStatus;
    //location.href=url;
    $.post(url,function(data){
        $("#jqGrid1").jqGrid().trigger("reloadGrid");
    });
}
function updateAssureMoneyOrTenderStatus(id,moneyOrFlagType,type,financialStatus,requestId){
    $.ajax({
        url: ctx + "/rfqFinancialApprovalList/dffuReturnData?supplierId="+id,
        data: {requestId: requestId},
        type: 'POST',
        success: function (data) {
            var data = eval('('+data+')');
            if(data.payStatus == "000000"){
                RFQ.info("保证金释放成功！");
                var url2 = ctx+"/rfqFinancialApprovalList/updateAssureMoneyOrTenderStatus?id=" + id+"&moneyOrFlagType="+moneyOrFlagType+"&type="+type+"&financialStatus="+financialStatus;
                $.post(url2,function(data){
                    $("#jqGrid1").jqGrid().trigger("reloadGrid");
                });
            }else{
                RFQ.error("操作失败！"+data.payMessage);
            }
        }
    });
}

