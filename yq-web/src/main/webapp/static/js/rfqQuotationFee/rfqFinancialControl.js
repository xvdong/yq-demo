$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $.ajaxSetup({complete:function(XHR, TS){
        //全部状态的总数
        var ids1 = $("#jqGrid1").jqGrid('getDataIDs');
        if(ids1.length<1){
            $("#t1").html("("+0+")");
        }else {
            $("#t1").html("("+($("#jqGrid1").getCell(ids1[0],'count'))+")");
        }
        //未锁定的总数
        var ids2 = $("#jqGrid2").jqGrid('getDataIDs');
        if(ids2.length<1){
            $("#t2").html("("+0+")");
        }else {
            $("#t2").html("("+($("#jqGrid2").getCell(ids2[0],'count'))+")");
        }
        //已锁定的总数
        var ids3 = $("#jqGrid3").jqGrid('getDataIDs');
        if(ids3.length<1){
            $("#t3").html("("+0+")");
        }else {
            $("#t3").html("("+($("#jqGrid3").getCell(ids3[0],'count'))+")");
        }
        //已释放的总数
        var ids4 = $("#jqGrid4").jqGrid('getDataIDs');
        if(ids4.length<1){
            $("#t4").html("("+0+")");
        }else {
            $("#t4").html("("+($("#jqGrid4").getCell(ids4[0],'count'))+")");
        }
    }});

    var tableId1 = "jqGrid1";
    var tableId2 = "jqGrid2";
    var tableId3 = "jqGrid3";
    var tableId4 = "jqGrid4";
    var url1 = ctx+"/rfqFinancialControlList/queryRfqFinancialAssureMoneyList";
    var url2 = ctx+"/rfqFinancialControlList/queryAssureMoneyNotPaidList";
    var url3 = ctx+"/rfqFinancialControlList/queryAssureMoneyAlreadyPaidList";
    var url4 = ctx+"/rfqFinancialControlList/queryAssureMoneyAlreadyBackList";
    var pagerId1 = "jqGridPager1";
    var pagerId2 = "jqGridPager2";
    var pagerId3 = "jqGridPager3";
    var pagerId4 = "jqGridPager4";
    loadData(tableId1,url1,pagerId1);//加载全部状态表
    loadData(tableId2,url2,pagerId2);//加载未缴纳列表
    loadData(tableId3,url3,pagerId3);//加载已缴纳列表
    loadData(tableId4,url4,pagerId4);//加载已退回列表

    //点击全部状态,隐藏未缴纳和已缴纳和已退回,显示全部状态
    $("#myTabl").click(function(){

        $("#dataTable2").hide();//隐藏未缴纳列表
        $("#dataTable3").hide();//隐藏已缴纳列表
        $("#dataTable4").hide();//隐藏已退回列表

        $("#btnSearch2").hide();//隐藏表2搜索按钮
        $("#btnSearch3").hide();//隐藏表3搜索按钮
        $("#btnSearch4").hide();//隐藏表4搜索按钮
        $("#btnSearch1").show();//显示表1搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid1").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable1").show();//显示全部状态的列表
    });
    //点击未缴纳,隐藏全部状态和已缴纳和已退回,显示未缴纳
    $("#myTab2").click(function(){

        $("#dataTable1").hide();//隐藏全部状态列表
        $("#dataTable3").hide();//隐藏已缴纳列表
        $("#dataTable4").hide();//隐藏已退回列表

        $("#btnSearch1").hide();//隐藏表1搜索按钮
        $("#btnSearch3").hide();//隐藏表3搜索按钮
        $("#btnSearch4").hide();//隐藏表4搜索按钮
        $("#btnSearch2").show();//显示表2搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid2").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable2").show();//显示未缴纳列表
    });
    //点击已缴纳,隐藏全部状态和未缴纳和已退回,显示已缴纳
    $("#myTab3").click(function(){

        $("#dataTable1").hide();//隐藏全部状态的列表
        $("#dataTable2").hide();//隐藏未缴纳列表
        $("#dataTable4").hide();//隐藏已退回列表

        $("#btnSearch1").hide();//隐藏表1搜索按钮
        $("#btnSearch2").hide();//隐藏表2搜索按钮
        $("#btnSearch4").hide();//隐藏表4搜索按钮
        $("#btnSearch3").show();//显示表3搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid3").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable3").show();//显示已缴纳列表
    });
    //点击已退回,隐藏全部状态和未缴纳和已缴纳,显示已退回
    $("#myTab4").click(function(){

        $("#dataTable1").hide();//隐藏全部状态的列表
        $("#dataTable2").hide();//隐藏未缴纳列表
        $("#dataTable3").hide();//隐藏已缴纳列表

        $("#btnSearch1").hide();//隐藏表1搜索按钮
        $("#btnSearch2").hide();//隐藏表2搜索按钮
        $("#btnSearch3").hide();//隐藏表3搜索按钮
        $("#btnSearch4").show();//显示表4搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid4").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable4").show();//显示已退回列表
    });

    function loadData(tableId,url,pagerId){
        $("#"+tableId).jqGrid({
            url:url,
            datatype: 'json',
            mtype: 'POST',
            rownumbers:true,
            colModel: [
                {
                    label: '询单序号',
                    name: 'id',
                    index: 'id',
                    width: 80,
                    key: true,
                    hidden:true
                },
                {
                    label: '供应商',
                    name: 'supplierId',
                    hidden:true
                },
                {
                    label: '询价单号',
                    name: 'unifiedRfqNum',
                    index: 'UNIFIED_RFQ_NUM',
                    align:'center',
                    hidden:true,
                    width: 80,
                    formatter: formatunifiedRfqNum
                }, {
                    label: '询价单号',
                    name: 'ouRfqNum',
                    index: 'OU_RFQ_NUM',
                    align:'center',
                    width: 80,
                    formatter: formatunifiedRfqNum
                },
                {
                    label: '询价标题',
                    name: 'title',
                    index: 'TITLE',
                    align:'center',
                    width: 100,
                    formatter: formatTitle
                },
                {
                    label: '模式',
                    name: 'rfqMethod',
                    index: 'RFQ_METHOD',
                    align:'center',
                    width: 40,
                    formatter: formatFfqMethod
                },
                {
                    label: '创建人',
                    name: 'recCreatorUsername',
                    index: 'REC_CREATOR_USERNAME',
                    align:'center',
                    width: 60
                },
                {
                    label: '创建时间',
                    name: 'recCreateTime',
                    index: 'REC_CREATE_TIME',
                    align:'center',
                    width: 100
                },
                {
                    label: '询单状态',
                    name: 'type',
                    index: 'TYPE',
                    align:'center',
                    width: 50,
                    formatter: formatRfqRequestType
                },
                {
                    label: '保证金锁定详情',
                    name: 'rfqFinancialStatus',
                    index: 'rfqFinancialStatus',
                    align:'center',
                    width: 60,
                    formatter: formatRfqFinancialStatus
                },
                {
                    width: 50,
                    label: '操作',
                    name: 'requestId',
                    index: 'REQUEST_ID',
                    formatter: formatOptionStatus
                },
                {
                    width: 60,
                    label: '总数',
                    name: 'count',
                    index: 'count',
                    hidden:true
                },
                {
                    label: '中标数',
                    name: 'checkedNum',
                    index: 'checkedNum',
                    hidden:true
                }
            ],
            jsonReader: { //映射server端返回的字段
                root: "list",//
                rows: "list",
                page: "pageNum",
                total: "pages",
                records: "total"
            },
            prmNames: {
                id: 'ruleId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
            },
            viewrecords: true, // show the current page, data rang and total records on the toolbar
            width: 1038,
            height: "100%",
            rowNum: 20,//每页显示记录数
            rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
            sortname: 'requestId',
            sortorder: 'asc',
            pager: "#"+pagerId

        });
        $("#"+tableId).jqGrid('setLabel', 0,"序号");
    }


    //保证金的列表
    $("#financialApprovalAllBtn").bind("click",function(){
        $("#resetForm").click();//清空表单数据
        $("#jqGrid1").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialControlList/queryRfqFinancialAssureMoneyList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable1").show();

        $("#jqGrid2").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialControlList/queryAssureMoneyNotPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable2").hide();

        $("#jqGrid3").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialControlList/queryAssureMoneyAlreadyPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable3").hide();

        $("#jqGrid4").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialControlList/queryAssureMoneyAlreadyBackList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable4").hide();
    });

    //设置询单号的格式
    function formatunifiedRfqNum(cellvalue, options, rawObject){
        var unifiedRfqNum = rawObject.unifiedRfqNum;
        var id = rawObject.requestId;
        var rval = '<span class="blue" onclick="rfqRequestDetail(\'' + id + '\')" >'+unifiedRfqNum+'</span>';
        return rval;
    }
    //判断财务状态
    function formatRfqFinancialStatus(cellvalue, options, rawObject){
        var rval = '';
        switch (cellvalue) {
            case '0': rval = '未锁定'; break;
            case '1': rval = "已锁定"; break;
            case '2':rval = "已释放保证金"; break;
            default:
                break;
        }
        return rval;
    }
    //设置操作的格式
    function formatOptionStatus(cellvalue, options, rawObject){
        var rval = '';
        //获取财务状态值
        var financialStatus = rawObject.rfqFinancialStatus;
        //获取询单状态值
        var type = rawObject.type;

        //待报价--已锁定--->查看
        if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&'1'===financialStatus){
            rval = '<span class="ml5"><a class="btn btn-sm btn-info" data-toggle="modal"  data-target="#myModa2" onclick ="financialControlDetail(\''+rawObject.id+ '\''+'\,' +'\''+rawObject.rfqMethod+ '\''+'\,' +'\''+financialStatus+ '\''+'\,' +'\''+type+ '\')"><i class="icon icon-reply"></i>查看</a></span>';
            //待报价--未锁定--->锁定
        }else if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&'0'===financialStatus){
            rval = '<span class="ml5"><a class="btn btn-sm btn-warning" data-toggle="modal"  data-target="#myModa2" onclick="financialControlDetail(\''+ rawObject.id+'\''+'\,' +'\''+rawObject.rfqMethod+ '\''+'\,' +'\''+financialStatus+ '\''+'\,' +'\''+type+ '\')"><i class="icon icon-check-sign"></i> 锁定</a></span>';
            //已结束--已锁定-->查看
        }else if("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&'1'===financialStatus){
            rval = '<span class="ml5"><a class="btn btn-sm btn-info" data-toggle="modal"  data-target="#myModa2"  onclick="financialControlDetail(\''+ rawObject.id+'\''+'\,' +'\''+rawObject.rfqMethod+ '\''+'\,' +'\''+financialStatus+ '\''+'\,' +'\''+type+ '\')"><i class="icon icon-reply"></i> 查看</a></span>';
            //已结束--已释放
        }/*else if("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&'2'==rfqFinancialStatus){
            rval = '<span class="ml5"><a class="btn btn-sm btn-info" data-toggle="modal"  data-target="#myModa2"  onclick="financialControlDetail(\''+ rawObject.id+'\''+'\,' +'\''+rawObject.rfqMethod+ '\''+'\,' +'\''+financialStatus+ '\''+'\,' +'\''+type+ '\')"><i class="icon icon-zoom-in"></i></a></span>';
        }*/
        return rval;
    }
    //设置标题--截取标题
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0,50) + "...";
    }
//判断财务状态
    function formatFfqMethod(cellvalue, options, rawObject){
        var rval = '';
        switch (cellvalue) {
            case 'RAQ': rval = '询比价'; break;
            case 'DAC': rval = "反向竞价"; break;
            case '反向竞拍': rval = "反向竞拍"; break;
            default:
                break;
        }
        return rval;
    }

        $("#btnSearch1").click(function () {
            $("#jqGrid1").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

        $("#btnSearch2").click(function () {
            $("#jqGrid2").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

        $("#btnSearch3").click(function () {
            $("#jqGrid3").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

        $("#btnSearch4").click(function () {
            $("#jqGrid4").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

});

function financialControlDetail(id,rmethod,rfqFinancialStatus,type) {
    var assureMoney = null;
    var currency = null;
    if("DAC" == rmethod){
        $("#unifiedRfqNumLock").prev().html("竞价单号 : ");
        $("#titleLock").prev().html("竞价标题 : ");
    }else if("RAQ" == rmethod ){
        $("#unifiedRfqNumLock").prev().html("询价单号 : ");
        $("#titleLock").prev().html("询价标题 : ");
    }

    $.ajax({
        url:ctx+"/rfqFinancialControlList/queryRfqFinancialAssureMoneyListById",
        data:{requestId:id},
        type:'POST',
        success:function(data){
            var data = eval('('+data+')');
            assureMoney = data.assureMoney;
            currency = data.currency;
            if(data != null){
                $("#unifiedRfqNumLock").html(data.unifiedRfqNum);//询价单号
                $("#titleLock").html(data.title);//询单标题
                $("#recCreateTimeLock").html(data.recCreateTime);//创建时间
                $("#recCreatorUsernameLock").html(data.recCreatorUsername);//创建人
                if("DAC" == rmethod){
                    $("#recMethodLock").html("反向竞价");
                }else if("RAQ" == rmethod){
                    $("#recMethodLock").html("询比价");
                }else if("反向竞拍" == rmethod){
                    $("#recMethodLock").html("反向竞拍");
                }

                var rval = "";
                switch (type) {
                    case '100': rval = '全部'; break;
                    case '0': rval = '草稿'; break;
                    case '1': rval = "创建待审批"; break;
                    case '2': rval = "待发布"; break;
                    case '3':
                        if('RAQ'==rmethod){
                            rval = "待报价";
                        }else if('DAC'==rmethod){
                            rval = "待竞价";
                        }
                        break;
                    case '4': rval = "待报名"; break;
                    case '5': rval = "报名中"; break;
                    case '6':
                        if('RAQ'==rmethod){
                            rval = "报价中";
                        }else if('DAC'==rmethod){
                            rval = "竞价中";
                        }
                        break;
                    case '7': rval = "待开标"; break;
                    case '8':
                        if('RAQ'==rmethod){
                            rval = "待核价";
                        }else if('DAC'==rmethod){
                            rval = "待授标";
                        }
                        break;
                    case '9': rval = "结果待审批"; break;
                    case '10': rval = "已结束"; break;
                    case '11': rval = "已作废"; break;
                    case '12': rval = "已流标"; break;
                    case '13': rval = "结果待发布"; break;
                    case '14': rval = "创建待审批"; break;
                    case '15': rval = "待发布"; break;
                    default:
                        break;
                }
                $("#typeLock").html(rval);

            }else{
                $("#unifiedRfqNumLock").html("");//询价单号
                $("#titleLock").html("");//询单标题
                $("#recCreateTimeLock").html("");//创建时间
                $("#recCreatorUsernameLock").html("");//创建人
                $("#recMethodLock").html("");
                $("#typeLock").html("");
            }
            lookLock(id,rfqFinancialStatus,type,assureMoney,currency);
        }
    });
}

function rfqRequestDetail(id) {
    var aa = window.open();
    var url = ctx+"/rfqRequest/rfqRequestDetail?id=" + id;
    aa.location.href = url ;
}

function lookLock(id,rfqFinancialStatus,type,assureMoney,currency){
        assureMoney = parseFloat(assureMoney).toFixed(2);
        var ret = "";
        if(currency == ""||currency == "CNY"){
            ret = "元";
        }else if(currency == "USD"){
            ret = "美元";
        }else if(currency = "GBP"){
            ret = "英镑";
        }else if(currency = "JPY"){
            ret = "日元";
        }else if(currency = "EUR"){
            ret = "欧元";
        }else if(currency = "HKD"){
            ret = "港币";
        }else if(currency = "CHF"){
            ret = "瑞士法郎";
        }
    $("#lockJqGrid").jqGrid({
        url:ctx+"/rfqFinancialControlList/queryRfqFinancialAssureMoneyListById2",
        datatype: 'json',
        mtype: 'POST',
        rownumbers:true,
        width:868,
        height: "100%",
        colModel: [
            {
                label: '询单序号',
                name: 'id',
                index: 'id',
                width: 60,
                key: true,
                hidden:true
            },
            {
                label:'供应商代码',
                name:'supplierCode',
                index:'SUPPLIER_CODE',
                width:60
            },
            {
                label:'供应商名称',
                name:'supplierName',
                index:'SUPPLIER_NAME',
                width:100
            },
            {
                label:'是否中标',
                name:'checkedNum',
                index:'checkedNum',
                width:60,
                formatter:zhongBiaoStatus
            },
            {
                label: '是否锁定保证金额（保证金额：<font color="red">'+assureMoney+'</font>'+ret+'）',
                name:'isAssureMoney',
                index:'isAssureMoney',
                width:110,
                formatter:assureMoneyLockStatus
            }
        ],
        postData:{'requestId':id},
        jsonReader: { //映射server端返回的字段
            root: "list",//
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },

        viewrecords: true, // show the current page, data rang and total records on the toolbar
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'id',
        sortorder: 'asc',
        pager: "#lockJqGridPager"
    });
    $("#lockJqGrid").jqGrid('setLabel', 0,"序号");

    function assureMoneyLockStatus(cellvalue, options, rawObject){
        var rval = '';
        if(cellvalue == undefined && rfqFinancialStatus == '1'){
            rval = "已锁定";
        }else if(cellvalue ==undefined && rfqFinancialStatus == '0'){
            rval = '<span class="ml5"><a class="btn btn-sm btn-info" onclick="changeStatus(\''+ rawObject.id+'\''+'\,' +'\''+rfqFinancialStatus + '\''+'\,' +'\''+type+'\''+'\,' +'\''+assureMoney+'\''+'\,' +'\''+currency+'\')"><i class="icon icon-check-sign"></i> 锁定</a></span>';
        }else{
            rval = "已锁定";
        }
        return rval;
    }
    function zhongBiaoStatus(cellvalue, options, rawObject){
        var rval = '';
            if(cellvalue <= 0){
                rval = "否"
            }else if(cellvalue > 0){
                rval = "是"
            }
        return rval;
    }
}

function changeStatus(id){
    $.ajax({
        url:ctx+"/rfqFinancialControlList/dffuReturnData",
        data:{requestId:id},
        type:'POST',
        async:false,
        success:function(data){
            var data = eval('('+data+')');
            if(data.payStatus == "000000"){
                $.post(ctx+"/rfqFinancialControlList/updateAssureMoneyStatus",{requestId:id},function(data){
                        RFQ.info("保证金锁定成功！");
                        $("#lockJqGrid").jqGrid("setCell",id,"isAssureMoney","1");
                });
            }else{
               /* $.post(ctx+"/rfqFinancialControlList/deleteRecordPay?payId="+data.payID,function(data){*/
                RFQ.error("操作失败！"+data.payMessage);
            }
        }
    })
}