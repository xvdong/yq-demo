$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $.ajaxSetup({complete:function(XHR, TS){
        //全部状态的总数
        var ids1 = $("#jqGrid1").jqGrid('getDataIDs');
        if(ids1.length<1){
            $("#t1").html("("+0+")");
        }else {
            $("#t1").html("("+($("#jqGrid1").getCell(ids1[0],'count'))+")");
        }
        //未缴纳的总数
        var ids2 = $("#jqGrid2").jqGrid('getDataIDs');
        if(ids2.length<1){
            $("#t2").html("("+0+")");
        }else {
            $("#t2").html("("+($("#jqGrid2").getCell(ids2[0],'count'))+")");
        }
        //已缴纳的总数
        var ids3 = $("#jqGrid3").jqGrid('getDataIDs');
        if(ids3.length<1){
            $("#t3").html("("+0+")");
        }else {
            $("#t3").html("("+($("#jqGrid3").getCell(ids3[0],'count'))+")");
        }
        //已退回的总数
        var ids4 = $("#jqGrid4").jqGrid('getDataIDs');
        if(ids4.length<1){
            $("#t4").html("("+0+")");
        }else {
            $("#t4").html("("+($("#jqGrid4").getCell(ids4[0],'count'))+")");
        }
    }});

    var tableId1 = "jqGrid1";
    var tableId2 = "jqGrid2";
    var tableId3 = "jqGrid3";
    var tableId4 = "jqGrid4";
    var url1 = ctx+"/rfqFinancialApprovalList/queryRfqFinancialApprovalList";
    var url2 = ctx+"/rfqFinancialApprovalList/queryRfqFinancialNotPaidList";
    var url3 = ctx+"/rfqFinancialApprovalList/queryRfqFinancialAlreadyPaidList";
    var url4 = ctx+"/rfqFinancialApprovalList/queryRfqFinancialAlreadyBackList";
    var pagerId1 = "jqGridPager1";
    var pagerId2 = "jqGridPager2";
    var pagerId3 = "jqGridPager3";
    var pagerId4 = "jqGridPager4";
    loadData(tableId1,url1,pagerId1);//加载全部状态表
    loadData(tableId2,url2,pagerId2);//加载未缴纳列表
    loadData(tableId3,url3,pagerId3);//加载已缴纳列表
    loadData(tableId4,url4,pagerId4);//加载已退回列表

    //点击全部状态,隐藏未缴纳和已缴纳和已退回,显示全部状态
    $("#myTabl").click(function(){

        $("#dataTable2").hide();//隐藏未缴纳列表
        $("#dataTable3").hide();//隐藏已缴纳列表
        $("#dataTable4").hide();//隐藏已退回列表

        $("#btnSearch2").hide();//隐藏表2搜索按钮
        $("#btnSearch3").hide();//隐藏表3搜索按钮
        $("#btnSearch4").hide();//隐藏表4搜索按钮
        $("#btnSearch1").show();//显示表1搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid1").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable1").show();//显示全部状态的列表
    });
    //点击未缴纳,隐藏全部状态和已缴纳和已退回,显示未缴纳
    $("#myTab2").click(function(){

        $("#dataTable1").hide();//隐藏全部状态列表
        $("#dataTable3").hide();//隐藏已缴纳列表
        $("#dataTable4").hide();//隐藏已退回列表

        $("#btnSearch1").hide();//隐藏表1搜索按钮
        $("#btnSearch3").hide();//隐藏表3搜索按钮
        $("#btnSearch4").hide();//隐藏表4搜索按钮
        $("#btnSearch2").show();//显示表2搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid2").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable2").show();//显示未缴纳列表
    });
    //点击已缴纳,隐藏全部状态和未缴纳和已退回,显示已缴纳
    $("#myTab3").click(function(){

        $("#dataTable1").hide();//隐藏全部状态的列表
        $("#dataTable2").hide();//隐藏未缴纳列表
        $("#dataTable4").hide();//隐藏已退回列表

        $("#btnSearch1").hide();//隐藏表1搜索按钮
        $("#btnSearch2").hide();//隐藏表2搜索按钮
        $("#btnSearch4").hide();//隐藏表4搜索按钮
        $("#btnSearch3").show();//显示表3搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid3").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable3").show();//显示已缴纳列表
    });
    //点击已退回,隐藏全部状态和未缴纳和已缴纳,显示已退回
    $("#myTab4").click(function(){

        $("#dataTable1").hide();//隐藏全部状态的列表
        $("#dataTable2").hide();//隐藏未缴纳列表
        $("#dataTable3").hide();//隐藏已缴纳列表

        $("#btnSearch1").hide();//隐藏表1搜索按钮
        $("#btnSearch2").hide();//隐藏表2搜索按钮
        $("#btnSearch3").hide();//隐藏表3搜索按钮
        $("#btnSearch4").show();//显示表4搜索按钮

        $("#resetForm").click();//清空表单数据
        $("#jqGrid4").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable4").show();//显示已退回列表
    });

    function loadData(tableId,url,pagerId){

        $("#"+tableId).jqGrid({
            url:url,
            datatype: 'json',
            mtype: 'POST',
            rownumbers:true,
            colModel: [
                {
                    label: '类型',
                    name: 'moneyOrFlagType',
                    index: 'moneyOrFlagType',
                    width: 50,
                    formatter: formatmoneyOrFlagType
                },
                {
                    label: '询价单号',
                    name: 'unifiedRfqNum',
                    index: 'UNIFIED_RFQ_NUM',
                    align:'center',
                    hidden:true,
                    width: 80,
                    formatter: formatunifiedRfqNum
                },
                {
                    label: '询价单号',
                    name: 'ouRfqNum',
                    index: 'OU_RFQ_NUM',
                    align:'center',
                    width: 80,
                    formatter: formatunifiedRfqNum
                },
                {
                    label: '询价标题',
                    name: 'title',
                    index: 'TITLE',
                    align:'center',
                    width: 100,
                    formatter: formatTitle
                },
                {
                    label: '模式',
                    name: 'rfqMethod',
                    index: 'RFQ_METHOD',
                    align:'center',
                    width: 40,
                    formatter: formatFfqMethod
                },
                {
                    label: '创建人',
                    name: 'recCreatorUsername',
                    index: 'REC_CREATOR_USERNAME',
                    align:'center',
                    width: 60
                },
                {
                    label: '发布时间',
                    name: 'issueDate',
                    index: 'ISSUE_DATE',
                    align:'center',
                    width: 100
                },
                {
                    label: '询单状态',
                    name: 'type',
                    index: 'TYPE',
                    align:'center',
                    width: 50,
                    formatter: formatRfqRequestType
                },
                {
                    label: '财务状态',
                    name: 'rfqFinancialStatus',
                    index: 'rfqFinancialStatus',
                    align:'center',
                    width: 50,
                    formatter: formatRfqFinancialStatus
                },
                {
                    width: 60,
                    label: '操作',
                    name: 'requestId',
                    index: 'REQUEST_ID',
                    formatter: formatOptionStatus
                },
                {
                    width: 60,
                    label: '总数',
                    name: 'count',
                    index: 'count',
                    hidden:true
                }
            ],
            jsonReader: { //映射server端返回的字段
                root: "list",//
                rows: "list",
                page: "pageNum",
                total: "pages",
                records: "total"
            },
            prmNames: {
                id: 'ruleId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
            },
            viewrecords: true, // show the current page, data rang and total records on the toolbar
            width: 1038,
            height: "100%",
            rowNum: 20,//每页显示记录数
            rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
            sortname: 'requestId',
            sortorder: 'asc',
            pager: "#"+pagerId

        });
        $("#"+tableId).jqGrid('setLabel', 0,"序号");
    }


    //全部状态的列表
    $("#financialApprovalAllBtn").bind("click",function(){
        $("#resetForm").click();//清空表单数据
        $("#jqGrid1").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialApprovalList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable1").show();

        $("#jqGrid2").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialNotPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable2").hide();

        $("#jqGrid3").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialAlreadyPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable3").hide();

        $("#jqGrid4").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialAlreadyBackList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable4").hide();
    });
    //保证金的列表
    $("#financialAssureMoneyBtn").bind("click",function(){
        $("#resetForm").click();//清空表单数据
        $("#jqGrid1").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialAssureMoneyList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable1").show();

        $("#jqGrid2").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryAssureMoneyNotPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable2").hide();

        $("#jqGrid3").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryAssureMoneyAlreadyPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable3").hide();

        $("#jqGrid4").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryAssureMoneyAlreadyBackList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable4").hide();
    });
    //标书费的列表
    $("#financialTenderFlagBtn").bind("click",function(){
        $("#resetForm").click();//清空表单数据
        $("#jqGrid1").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryRfqFinancialTenderFlagList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable1").show();

        $("#jqGrid2").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryTenderFlagNotPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable2").hide();

        $("#jqGrid3").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryTenderFlagAlreadyPaidList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable3").hide();

        $("#jqGrid4").jqGrid('setGridParam', {
            url:ctx+"/rfqFinancialApprovalList/queryTenderFlagAlreadyBackList",
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
        $("#dataTable4").hide();
    });
    //设置标书费或保证金
    function formatmoneyOrFlagType(cellvalue, options, rawObject){
        var rval = '';
        switch (cellvalue) {
            case '0': rval = '<span class="label label-warning">标书费</span>'; break;
            case '1': rval = '<span class="label label-primary">保证金</span>'; break;
            default:
                break;
        }
        return rval;
    }
    //设置询单号的格式
    function formatunifiedRfqNum(cellvalue, options, rawObject){

        var unifiedRfqNum = rawObject.unifiedRfqNum;
        var id = rawObject.requestId;
        var rval = '<span class="blue" onclick="rfqRequestDetail(\'' + id + '\')" >'+unifiedRfqNum+'</span>';
        return rval;
    }
    //判断财务状态
    function formatRfqFinancialStatus(cellvalue, options, rawObject){
        var rval = '';
        switch (cellvalue) {
            case '0': rval = '未缴纳'; break;
            case '1': rval = "已缴纳"; break;
            case '2': rval = "已退回"; break;
            default:
                break;
        }
        return rval;
    }
    //设置操作的格式
    function formatOptionStatus(cellvalue, options, rawObject){
        var rval = '';
        //获取财务状态值
        var financialStatus = rawObject.rfqFinancialStatus;
        //获取询单状态值
        var type = rawObject.type;
        //待报价--已缴纳--->查看
        if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&'1'===financialStatus){
            rval = '<span class="ml5"><span class="btn btn-sm btn-info" onclick="financialApprovalDetail(\'' + rawObject.moneyOrFlagType+'\'' +'\,'+'\''+ rawObject.unifiedRfqNum+'\''+'\,' +'\''+rawObject.requestId+ '\')"><i class="icon icon-zoom-in"></i> 查看</span></span>';
        //待报价--未缴纳--->缴纳
        }else if("-3-4-5-6-14-15-".indexOf("-"+type+"-")>-1&&'0'===financialStatus){
            rval = '<span class="ml5"><span class="btn btn-sm btn-warning" onclick="financialApprovalDetail(\'' + rawObject.moneyOrFlagType+'\'' +'\,'+'\''+ rawObject.unifiedRfqNum+'\''+'\,' +'\''+rawObject.requestId+ '\')"><i class="icon icon-check-sign"></i> 缴纳</span></span>';
        //已结束--已缴纳-->退回
        }else if("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&'1'===financialStatus){
            rval = '<span class="ml5"><span class="btn btn-sm btn-warning" onclick="financialApprovalDetail(\'' + rawObject.moneyOrFlagType+'\'' +'\,'+'\''+ rawObject.unifiedRfqNum+'\''+'\,' +'\''+rawObject.requestId+ '\')"><i class="icon icon-reply"></i> 退回</span></span>';
        //已结束--已退回-->查看
        }else if("-7-8-9-10-11-12-13-".indexOf("-"+type+"-")>-1&&'2'==financialStatus){
            rval = '<span class="ml5"><span class="btn btn-sm btn-info" onclick="financialApprovalDetail(\'' + rawObject.moneyOrFlagType+'\'' +'\,'+'\''+ rawObject.unifiedRfqNum+'\''+'\,' +'\''+rawObject.requestId+ '\')"><i class="icon icon-zoom-in"></i> 查看</span></span>';

        }
        return rval;
    }
    //设置标题--截取标题
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    }
//判断财务状态
    function formatFfqMethod(cellvalue, options, rawObject){
        var rval = '';
        switch (cellvalue) {
            case 'RAQ': rval = '询比价'; break;
            case 'DAC': rval = "反向竞价"; break;
            case '反向竞拍': rval = "反向竞拍"; break;
            default:
                break;
        }
        return rval;
    }

        $("#btnSearch1").click(function () {
            $("#jqGrid1").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

        $("#btnSearch2").click(function () {
            $("#jqGrid2").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

        $("#btnSearch3").click(function () {
            $("#jqGrid3").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });

        $("#btnSearch4").click(function () {
            $("#jqGrid4").jqGrid('setGridParam', {
                postData: form2Json("searchForm"),
                page: 1
            }).trigger("reloadGrid");
        });


    function financialNotPaid() {
        var url = ctx+"/rfqFinancialApproval/queryRfqFinancialNotPaidList";
        var tableId1 = "jqGrid1";
        var pagerId1 = "jqGridPager1";
        loadData(tableId1,url1,pagerId1);
    }
    function financialAlreadyBack() {
        var url = ctx+"/rfqFinancialApproval/queryRfqFinancialAlreadyBackList";
        var tableId1 = "jqGrid1";
        var pagerId1 = "jqGridPager1";
        loadData(tableId1,url1,pagerId1);
    }


});
function financialApprovalDetail(moneyOrFlagType,unifiedRfqNum,requestId) {
    // location.href =ctx+"/rfqPreauditSupplier/auditInit?id=" + pasId;
    var aa=window.open();
    var url = ctx+"/rfqFinancialApprovalList/rfqFinancialApprovalDetail?moneyOrFlagType=" + moneyOrFlagType+"&unifiedRfqNum="+unifiedRfqNum+"&requestId="+requestId;
    aa.location.href = url;
    //window.open(url);

}
function rfqRequestDetail(id) {
    var aa = window.open();
    var url = ctx+"/rfqRequest/rfqRequestDetail?id=" + id;
    aa.location.href = url ;

}
//function financialAlreadyPaid() {
//    alert("123");
//    var url = ctx+"/rfqFinancialApproval/queryRfqFinancialAlreadyPaidList";
//    var tableId1 = "jqGrid1";
//    var pagerId1 = "jqGridPager1";
//    $.loadData(tableId1,url,pagerId1);
//}


