var uploader;//flash插件
var maxSize;
var types;

var uploader2;//excel flash插件

var i=0;//进度条,win7 IE8要用
var t1;//定时器,win7 IE8要用
//初始化flash插件
function initPlupload(maxSize, types) {
    maxSize = maxSize;
    types = types;

    //实例化一个plupload上传对象
    uploader = new plupload.Uploader({
        browse_button: 'upload', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/common/uploadFile", //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });
        $("#fileName").val(files[0].name);

        if (testFile(files[0], maxSize, types) == 0) {
            $("#msg").css('color', 'red').text("大小超过100M");
        } else if (testFile(files[0], maxSize, types) == 1) {
            $("#msg").css('color', 'red').text("附件类型不支持exe格式");
        } else {
            uploader.start();
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader.bind('BeforeUpload', function (uploader, file) {
        $("#upload").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader.bind('UploadProgress', function (uploader, file) {
        $("#msg").css('color', 'black').text(file.percent + "%")
    });
    //4.上传成功
    uploader.bind('FileUploaded', function (uploader, file, responseObject) {
        $("#msg").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = JSON.parse(b);                  //解析response
        }
        catch (e)
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
        }

        var result = JSON.parse(data.attachmentsVo);       //解析attachmentsVo
        $("#myModa2").data('_result', result);
        $("#sure").attr("disabled", false);
    });
    //5.上传失败
    uploader.bind('Error', function (uploader, errObject) {
        $("#msg").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#upload").attr("disabled", false);
    });
    //6.上传完成
    uploader.bind('UploadComplete', function () {
        $("#upload").attr("disabled", false);
    });
}

//初始化flash插件,IE8模式下使用,结果解析不同
function initPlupload2(maxSize, types) {
    maxSize = maxSize;
    types = types;

    //实例化一个plupload上传对象
    uploader = new plupload.Uploader({
        browse_button: 'upload', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/common/uploadFile", //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });
        $("#fileName").val(files[0].name);

        if (testFile(files[0], maxSize, types) == 0) {
            $("#msg").css('color', 'red').text("大小超过100M");
        } else if (testFile(files[0], maxSize, types) == 1) {
            $("#msg").css('color', 'red').text("附件类型不支持exe格式");
        } else {

            //win7 IE8处理
            var ua = navigator.userAgent;
            if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
                uploader.start();
            } else {//win7系统
                if ((navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0")){
                    uploader.start();
                    t1 = window.setInterval(processBar,1000);
                }else {
                    uploader.start();
                }
            }
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader.bind('BeforeUpload', function (uploader, file) {
        $("#upload").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader.bind('UploadProgress', function (uploader, file) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
            $("#msg").css('color', 'black').text(file.percent + "%")
        } else {//win7系统
        }
    });
    //4.上传成功
    uploader.bind('FileUploaded', function (uploader, file, responseObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
        } else {//win7系统
            window.clearInterval(t1);
        }

        $("#msg").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
             data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式
        }
        catch (e)
        {
             data = JSON.parse(b);                            //如果发生异常说明返回的不带PRE,部分IE8是不带PRE的
        }

        // var result = JSON.parse(data.attachmentsVo);       //解析attachmentsVo
        var result = eval("("+data.attachmentsVo+")")       //解析attachmentsVo
        $("#myModa2").data('_result', result);
        $("#sure").attr("disabled", false);
    });
    //5.上传失败
    uploader.bind('Error', function (uploader, errObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
        } else {//win7系统
            window.clearInterval(t1);
        }
        $("#msg").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#upload").attr("disabled", false);
    });
    //6.上传完成
    uploader.bind('UploadComplete', function () {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
        } else {//win7系统
            window.clearInterval(t1);
        }

        $("#upload").attr("disabled", false);
    });
}

//关闭后清理页面
function initEvent() {
    $("#myModa2").on('hide.zui.modal', function () {
        uploader.stop();
        $("#fileName").val("");
        $("#msg").css('color', 'black').text("0%");
        $("#sure").attr("disabled", true);
        $("#fileDeclaration").val("");
        $("#upload").attr("disabled", false);
    });
}

//-----------------------------------------------询价单 start-----------------
//初始化excel flash插件
function initPluploadss(maxSize, types) {
    maxSize = maxSize;
    types = types;

    //实例化一个plupload上传对象
    uploader2 = new plupload.Uploader({
        browse_button: 'excelFile', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/rfqRequestItemTmp/importRecord", //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader2.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader2.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });

         $("#fileNamess").val(files[0].name);

        if (testFiless(files[0], maxSize, types) == 0) {
            $("#msgss").css('color', 'red').text("大小超过100M");
            $("#suress").attr("disabled", true);
        } else if (testFiless(files[0], maxSize, types) == 1) {
            $("#msgss").css('color', 'red').text("附件类型只支持excel");
            $("#suress").attr("disabled", true);
        } else {
            $("#suress").attr("disabled", false);
            $("#msgss").text("");
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader2.bind('BeforeUpload', function (uploader, file) {
        $("#file").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader2.bind('UploadProgress', function (uploader, file) {
      //  $("#msgss").css('color', 'black').text(file.percent + "%")
    });
    //4.上传成功
    uploader2.bind('FileUploaded', function (uploader, file, responseObject) {
        //$("#msgss").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = JSON.parse(b);                  //解析response
        }
        catch (e)
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
        }

        //var result = JSON.parse(data.attachmentsVo);       //解析attachmentsVo
      //  $("#cancelPrice33").data('_result', result);
        $("#suress").attr("disabled", false);

        if (data.status == 'success') {
            var messageList = data.messageList;
            //console.log('messageList'+messageList);
            //预算总价
            var budgetPrice = +data.budgetPrice;
            if(messageList!="" && messageList!=null){
                RFQ.warn(data.message+"["+messageList+"]");
            }else{
                RFQ.info(data.message);
            }
            $('#cancelPrice33').modal('hide');
            //临时表加载数据
            $('#datatable-requestItemTable').data('_rfqDTable').load();
            //预算价
            totalPrice = budgetPrice;
            $('#totalPrice').html(totalPrice.toFixed(2));
        } else {
            RFQ.error(data.message);
        }
    });
    //5.上传失败
    uploader2.bind('Error', function (uploader, errObject) {
        $("#msgss").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#file").attr("disabled", false);
    });
    //6.上传完成
    uploader2.bind('UploadComplete', function () {
        $("#file").attr("disabled", false);
    });
}

//初始化excel flash插件,IE8模式下使用,结果解析不同
function initPlupload2ss(maxSize, types) {
    maxSize = maxSize;
    types = types;

    //实例化一个plupload上传对象
    uploader2 = new plupload.Uploader({
        browse_button: 'excelFile', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/rfqRequestItemTmp/importRecord", //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader2.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader2.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });
        $("#fileNamess").val(files[0].name);

        if (testFiless(files[0], maxSize, types) == 0) {
            $("#msgss").css('color', 'red').text("大小超过100M");
            $("#suress").attr("disabled", true);
        } else if (testFiless(files[0], maxSize, types) == 1) {
           $("#msgss").css('color', 'red').text("附件类型只支持excel");
            $("#suress").attr("disabled", true);
        } else {
            $("#suress").attr("disabled", false);
            //win7 IE8处理
            var ua = navigator.userAgent;
            if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
                //uploader.start();
                $("#msgss").text("");
            } else {//win7系统
               // t1 = window.setInterval(processBarss,1000);
                $("#msgss").text("");
            }
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader2.bind('BeforeUpload', function (uploader, file) {
        $("#file").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader2.bind('UploadProgress', function (uploader, file) {
        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
           // $("#msgss").css('color', 'black').text(file.percent + "%")
        } else {//win7系统
        }
    });
    //4.上传成功
    uploader2.bind('FileUploaded', function (uploader, file, responseObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
        } else {//win7系统
            window.clearInterval(t1);
        }

       //$("#msgss").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式
        }
        catch (e)
        {
            data = JSON.parse(b);                            //如果发生异常说明返回的不带PRE,部分IE8是不带PRE的
        }

        // var result = JSON.parse(data.attachmentsVo);       //解析attachmentsVo
        //var result = eval("("+data.attachmentsVo+")")       //解析attachmentsVo
        //$("#cancelPrice33").data('_result', result);
        //$("#suress").attr("disabled", false);
        if (data.status == 'success') {
            var messageList = data.messageList;
            //console.log('messageList'+messageList);
            //预算总价
            var budgetPrice = +data.budgetPrice;
            if(messageList!="" && messageList!=null){
                RFQ.warn(data.message+"["+messageList+"]");
            }else{
                RFQ.info(data.message);
            }
            $('#cancelPrice33').modal('hide');
            //临时表加载数据
            $('#datatable-requestItemTable').data('_rfqDTable').load();
            //预算价
            totalPrice = budgetPrice;
            $('#totalPrice').html(totalPrice.toFixed(2));
        } else {
            RFQ.error(data.message);
        }

    });
    //5.上传失败
    uploader2.bind('Error', function (uploader, errObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8

        } else {//win7系统
            window.clearInterval(t1);
        }
        $("#msgss").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#file").attr("disabled", false);
    });
    //6.上传完成
    uploader2.bind('UploadComplete', function () {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8

        } else {//win7系统
            window.clearInterval(t1);
        }

        $("#file").attr("disabled", false);
    });
}

//关闭后清理页面
function initEventss() {
    $("#cancelPrice33").on('hide.zui.modal', function () {
        //uploader2.stop();
        $("#fileNamess").val("请上传,附件大小≤100M");
        $("#msgss").text("");
        $("#suress").attr("disabled", true);
        //$("#file").attr("disabled", false);
    });
}
//maxSize单位是字节,types是字符串集合".xxx",包含 .
function testFiless(file, maxSize, types) {
    var extStart = file.name.lastIndexOf(".");
    var fileType = file.name.substr(extStart, file.name.length);
    if (fileType != '.xls' && fileType !='.xlsx') {
        return 1;
    } else if (file.size > maxSize) {
        return 0;
    } else {
        return 2;
    }
}
function upstart(){
    uploader2.start();
}

//--------------------------------------询价单 end-------------------------

//maxSize单位是字节,types是字符串集合".xxx",包含 .
function testFile(file, maxSize, types) {
    var extStart = file.name.lastIndexOf(".");
    var fileType = file.name.substr(extStart, file.name.length);
    if (fileType == '.exe') {
        return 1;
    } else if (file.size > maxSize) {
        return 0;
    } else {
        return 2;
    }
}

//清空file按钮
function excelClicke() {
    //$('#excelUpload')[0].reset();
    var file = document.getElementById("file");
    if (file.outerHTML) {
        file.outerHTML = file.outerHTML;
    } else {
        file.value = "";
    }
}

//win7 IE8下假进度条
function processBar(){
    if (i<90){
        $("#msg").css('color', 'black').text(i + "%")
        i=i+5;
    }else {
    }
}


//报价单 flash上传
//-----------------------------------------------报价单 start-----------------
//初始化excel flash插件
function initPluploadse(maxSize, types,requestId) {
    maxSize = maxSize;
    types = types;
    requestId=requestId;

    //实例化一个plupload上传对象
    uploader2 = new plupload.Uploader({
        browse_button: 'excelFile', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/rfqQuotationItemTemp/importRecord?requestId="+requestId, //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader2.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader2.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });

        $("#fileNamess").val(files[0].name);

        if (testFiless(files[0], maxSize, types) == 0) {
            $("#msgss").css('color', 'red').text("大小超过100M");
            $("#suress").attr("disabled", true);
        } else if (testFiless(files[0], maxSize, types) == 1) {
            $("#msgss").css('color', 'red').text("附件类型只支持excel");
            $("#suress").attr("disabled", true);
        } else {
            $("#suress").attr("disabled", false);
            $("#msgss").text("");
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader2.bind('BeforeUpload', function (uploader, file) {
        $("#file").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader2.bind('UploadProgress', function (uploader, file) {
        //  $("#msgss").css('color', 'black').text(file.percent + "%")
    });
    //4.上传成功
    uploader2.bind('FileUploaded', function (uploader, file, responseObject) {
        //$("#msgss").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = JSON.parse(b);                  //解析response
        }
        catch (e)
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
        }

        $("#suress").attr("disabled", false);

        if (data.status == 'success') {
            //总价
            /*console.log(data);
            alert(data.budgetPrice);*/
            var budgetPrice = +data.budgetPrice;
            RFQ.info(data.message);
            $('#cancelPrice33').modal('hide');
            totalPrice = budgetPrice;
            pagePriceSum = 0;
            setPriceVal(0);
            //临时表加载数据
            $('#datatable-requestItemData').data('_rfqDTable').load();
        } else {
            RFQ.error(data.message);
        }
    });
    //5.上传失败
    uploader2.bind('Error', function (uploader, errObject) {
        $("#msgss").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#file").attr("disabled", false);
    });
    //6.上传完成
    uploader2.bind('UploadComplete', function () {
        $("#file").attr("disabled", false);
    });
}

//初始化excel flash插件,IE8模式下使用,结果解析不同
function initPlupload2se(maxSize, types,requestId) {
    maxSize = maxSize;
    types = types;
    requestId=requestId;

    //实例化一个plupload上传对象
    uploader2 = new plupload.Uploader({
        browse_button: 'excelFile', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/rfqQuotationItemTemp/importRecord?requestId="+requestId, //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader2.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader2.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });
        $("#fileNamess").val(files[0].name);

        if (testFiless(files[0], maxSize, types) == 0) {
            $("#msgss").css('color', 'red').text("大小超过100M");
            $("#suress").attr("disabled", true);
        } else if (testFiless(files[0], maxSize, types) == 1) {
            $("#msgss").css('color', 'red').text("附件类型只支持excel");
            $("#suress").attr("disabled", true);
        } else {

            $("#suress").attr("disabled", false);
            //win7 IE8处理
            var ua = navigator.userAgent;
            if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
                //uploader.start();
                $("#msgss").text("");
            } else {//win7系统
               // t1 = window.setInterval(processBarss,1000);
                $("#msgss").text("");
            }
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader2.bind('BeforeUpload', function (uploader, file) {
        $("#file").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader2.bind('UploadProgress', function (uploader, file) {
        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
            // $("#msgss").css('color', 'black').text(file.percent + "%")
        } else {//win7系统
        }
    });
    //4.上传成功
    uploader2.bind('FileUploaded', function (uploader, file, responseObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
        } else {//win7系统
            window.clearInterval(t1);
        }

       // $("#msgss").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式
        }
        catch (e)
        {
            data = JSON.parse(b);                            //如果发生异常说明返回的不带PRE,部分IE8是不带PRE的
        }

        if (data.status == 'success') {
            //总价
            var budgetPrice = +data.budgetPrice;
            RFQ.info(data.message);
            $('#cancelPrice33').modal('hide');
            totalPrice = budgetPrice;
            pagePriceSum = 0;
            setPriceVal(0);
            //临时表加载数据
            $('#datatable-requestItemData').data('_rfqDTable').load();
        } else {
            RFQ.error(data.message);
        }

    });
    //5.上传失败
    uploader2.bind('Error', function (uploader, errObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8

        } else {//win7系统
            window.clearInterval(t1);
        }
        $("#msgss").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#file").attr("disabled", false);
    });
    //6.上传完成
    uploader2.bind('UploadComplete', function () {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8

        } else {//win7系统
            window.clearInterval(t1);
        }

        $("#file").attr("disabled", false);
    });
}

//关闭后清理页面
function initEventse() {
    $("#cancelPrice33").on('hide.zui.modal', function () {
        //uploader2.stop();
        $("#fileNamess").val("请上传,附件大小≤100M");
        $("#msgss").text("");
        $("#suress").attr("disabled", true);
        //$("#file").attr("disabled", false);
    });
}
//总价设置
function  setPriceVal(sum){
    /*totalPrice = totalPrice + sum - pagePriceSum;*/
    defaultPrice = $("#totalPrice").text();
    totalPrice =defaultPrice - pagePriceSum +sum;
    pagePriceSum = sum;
    $('#totalPrice').html(totalPrice.toFixed(2));
    $('#subtotalTaxed').val(totalPrice.toFixed(2));
}

//--------------------------------------报价单 end-------------------------


//明细清单 flash上传
//-----------------------------------------------明细清单 start-----------------
//初始化excel flash插件
function initPluploadmx(maxSize, types,requestId,money) {
    maxSize = maxSize;
    types = types;
    requestId=requestId;
    //实例化一个plupload上传对象
    uploader2 = new plupload.Uploader({
        browse_button: 'excelFile', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/rfqRequestItemTmp/importRecord2?requestId="+requestId+"&money="+money, //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader2.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader2.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });

        $("#fileNamess").val(files[0].name);

        if (testFiless(files[0], maxSize, types) == 0) {
            $("#msgss").css('color', 'red').text("大小超过100M");
            $("#suress").attr("disabled", true);
        } else if (testFiless(files[0], maxSize, types) == 1) {
            $("#msgss").css('color', 'red').text("附件类型只支持excel");
            $("#suress").attr("disabled", true);
        } else {
            $("#suress").attr("disabled", false);
            $("#msgss").text("");
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader2.bind('BeforeUpload', function (uploader, file) {
        $("#file").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader2.bind('UploadProgress', function (uploader, file) {
        //  $("#msgss").css('color', 'black').text(file.percent + "%")
    });
    //4.上传成功
    uploader2.bind('FileUploaded', function (uploader, file, responseObject) {
        //$("#msgss").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = JSON.parse(b);                  //解析response
        }
        catch (e)
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式,如果发生异常说明带PRE,部分IE9是带PRE的
        }

        $("#suress").attr("disabled", false);

        if (data.status == 'success') {
            //console.log(data);
            //alert(data);
            RFQ.info(data.message);
            $('#cancelPrice33').modal('hide');

            //临时表加载数据
           $("#jqGrid").trigger("reloadGrid");
        } else {
            RFQ.error(data.message);
        }
    });
    //5.上传失败
    uploader2.bind('Error', function (uploader, errObject) {
        $("#msgss").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#file").attr("disabled", false);
    });
    //6.上传完成
    uploader2.bind('UploadComplete', function () {
        $("#file").attr("disabled", false);
    });
}

//初始化excel flash插件,IE8模式下使用,结果解析不同
function initPlupload2mx(maxSize, types,requestId,money) {
    maxSize = maxSize;
    types = types;
    requestId=requestId;

    //实例化一个plupload上传对象
    uploader2 = new plupload.Uploader({
        browse_button: 'excelFile', //触发文件选择对话框的按钮(即上传按钮)，为那个元素id
        url: ctx + "/rfqRequestItemTmp/importRecord2?requestId="+requestId+"&money="+money, //服务器端的上传页面地址
        multi_selection: false,
        flash_swf_url: '/lib/plupload-2.1.9/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '/lib/plupload-2.1.9/js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        headers: {Authorization: ""}
    });
    uploader2.init();
    //1.添加文件之后的事件,先删除队列里所有文件,保证单文件上传,再验证文件合法性,合法后就直接自动上传
    uploader2.bind('FilesAdded', function (uploader, files) {
        $.each(uploader.files, function (i, file) {
            if (uploader.files.length <= 1) {
                return;
            }
            uploader.removeFile(file);
        });
        $("#fileNamess").val(files[0].name);

        if (testFiless(files[0], maxSize, types) == 0) {
            $("#msgss").css('color', 'red').text("大小超过100M");
            $("#suress").attr("disabled", true);
        } else if (testFiless(files[0], maxSize, types) == 1) {
            $("#msgss").css('color', 'red').text("附件类型只支持excel");
            $("#suress").attr("disabled", true);
        } else {

            $("#suress").attr("disabled", false);
            //win7 IE8处理
            var ua = navigator.userAgent;
            if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
                //uploader.start();
                $("#msgss").text("");
            } else {//win7系统
                // t1 = window.setInterval(processBarss,1000);
                $("#msgss").text("");
            }
        }
    });
    //2.上传前的事件,将上传按钮失效
    uploader2.bind('BeforeUpload', function (uploader, file) {
        $("#file").attr("disabled", true);
    });
    //3.上传中,进度条显示
    uploader2.bind('UploadProgress', function (uploader, file) {
        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
            // $("#msgss").css('color', 'black').text(file.percent + "%")
        } else {//win7系统
        }
    });
    //4.上传成功
    uploader2.bind('FileUploaded', function (uploader, file, responseObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8
        } else {//win7系统
            window.clearInterval(t1);
        }

        // $("#msgss").css('color', 'black').text("文件已选中");
        var b = responseObject.response;                     //response是个json字符串,要先解析一遍
        var data;
        try
        {
            data = jQuery.parseJSON(jQuery(b).text());          //兼容ie8不返回PRE的解析方式
        }
        catch (e)
        {
            data = JSON.parse(b);                            //如果发生异常说明返回的不带PRE,部分IE8是不带PRE的
        }

        if (data.status == 'success') {
            //总价
            var budgetPrice = +data.budgetPrice;
            RFQ.info(data.message);
            $('#cancelPrice33').modal('hide');
            totalPrice = budgetPrice;
            pagePriceSum = 0;
            setPriceVal(0);
            //临时表加载数据
            $('#datatable-requestItemData').data('_rfqDTable').load();
        } else {
            RFQ.error(data.message);
        }

    });
    //5.上传失败
    uploader2.bind('Error', function (uploader, errObject) {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8

        } else {//win7系统
            window.clearInterval(t1);
        }
        $("#msgss").css('color', 'red').text("上传失败:未知错误,请保存数据后刷新页面重试");
        $("#file").attr("disabled", false);
    });
    //6.上传完成
    uploader2.bind('UploadComplete', function () {

        //win7 IE8处理
        var ua = navigator.userAgent;
        if(ua.indexOf("Windows NT 5")!=-1) {//xp系统,xpIE8

        } else {//win7系统
            window.clearInterval(t1);
        }

        $("#file").attr("disabled", false);
    });
}

//关闭后清理页面
function initEventse() {
    $("#cancelPrice33").on('hide.zui.modal', function () {
        //uploader2.stop();
        $("#fileNamess").val("请上传,附件大小≤100M");
        $("#msgss").text("");
        $("#suress").attr("disabled", true);
        //$("#file").attr("disabled", false);
    });
}
//总价设置
function  setPriceVal(sum){
    /*totalPrice = totalPrice + sum - pagePriceSum;*/
    defaultPrice = $("#totalPrice").text();
    totalPrice =defaultPrice - pagePriceSum +sum;
    pagePriceSum = sum;
    $('#totalPrice').html(totalPrice.toFixed(2));
    $('#subtotalTaxed').val(totalPrice.toFixed(2));
}

//--------------------------------------明细清单 end-------------------------
