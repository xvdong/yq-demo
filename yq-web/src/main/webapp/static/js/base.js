;(function($){
	$.fn.extend({
		setScroll:function(opt){
			if($(this).length<1 || !opt.startTop){return;}
			Timmer= null,
			$this = $(this),$win = $(window),
			startTop = opt.startTop;
			if(opt.defaultTop){$this.css('top',opt.defaultTop);}
			$win.bind('scroll',function(){
				if(Timmer){clearTimeout(Timmer);}
					Timmer = setTimeout(function(){
						if($win.scrollTop()>=startTop+100){
						$this.css({'position':'fixed','top':0,'z-index':2});
					}else{
						$this.css({'position':'absolute','top':opt.startTop});
					}
					//console.log($win.scrollTop());
				},15);	
			});
		},
		
		/*删除行*/
		delBtn:function(){
			$(this).unbind("click").click(function(){
				var ss = $(this).parent().parent("tr").remove();
			});	
		},
		
		checkeInfo:function(){
			$(this).find('label').unbind("click").click(function(){
				if($(this).parent().index()==0){
					$('.fb-zz').show(300);	
				}else{
					$('.fb-zz').hide(300);	
				}
			});	
		},
		
		cachet:function(){
			var This=$(this);
			setTimeout(function(){
				This.addClass('zt-gz');
			},500);

		},
		
		/*下拉菜单*/
		selectLisr:function(){
			$(this).find('li a').click(function(){
				$(this).parent().parent().prev('.btn').html('<span class="v">'+$(this).html()+'</span>'+'<span class="caret"></span>');	
			});
		},	
		
		rotateArrow:function(){
			$(this).unbind('click').bind('click',function(){
					$(this).toggleClass('cur');		
			});	
		},
		hoverRow:function(){
			var This=$(this);
			This.find('.hjmx-tbody td').hover(function(){
				var That=$(this);
				This.find('table').each(function(){
					$(this).find('tr').	eq(That.parent().index()).find('td').addClass('bge8e8e8');
				});
	
			},function(){
				var That=$(this);
				This.find('table').each(function(){
					$(this).find('tr').	eq(That.parent().index()).find('td').removeClass('bge8e8e8');
				});
			});
		},
		soTable:function(obj,info){
			$(this).keyup(function(){
				var soText =$(this).val();
				$(obj).find("td").removeClass('fli');
				if(soText!=''){ 
					$(info).hide().parent().removeClass('has-error');	;
					setTimeout(function(){
						$(obj).find("td:contains("+soText+")").addClass('fli');
						if($(obj).find("td:contains("+soText+")").size()==0){
							$(info).show().parent().addClass('has-error');	
						};
					},300);
				}else{
					$(info).show().parent().addClass('has-error');
				}
			});			
		},
		navbarNav:function(){
			var This=$(this);
			$(this).find("li a").unbind('click').bind('click',function(){
				This.find('li a').css("background","none");
				$(this).css("background","#f1f1f1");
			});
		},


	});
})(jQuery);

;(function(){
	$.extend($,{
		BCacheLoadPic:function(options){//缓存图片
			var defaults={
				src:null,
				param:null,
				callback:function(){}
			};
			var options  = $.extend(true,defaults,options);
			var img = new Image();
			$(img).load(function() {
				options.callback(options.src,this.width,this.height,options.param);
			}).attr("src",options.src);	
		},
		scrollTop:function(options){//返回顶部
			var defaults={
				right:10,
				bottom:10,
				path:'../images/',
				src:'return_top.gif',
				callback:function(){}
			};
			var options  = $.extend(true,defaults,options); 	
			if($('#scroll-top').length<=0){
				$('body').append('<a href="javascript:;" style="height:50px;display:none;width:50px;z-index:996;position:fixed;background:url('+options.path+options.src+') no-repeat" title="返回顶部" id="scroll-top"></a>');
				$top=$('#scroll-top');
				$top.css({
					'opacity':0.5,
					'right':options.right,
					'bottom':options.bottom	
				}).bind({
					'click':a,
					'mouseover':b,
					'mouseleave':c
				});
			};
			$(window).scroll(function(){
				$(window).scrollTop()>0?$top.fadeIn(500):$top.fadeOut(500);	
			});
			function a(){
				$('html,body').animate({'scrollTop':0},300);		
			};
			function b(){
				$(this).fadeTo(300,1);
			};
			function c(){
				$(this).fadeTo(300,0.5);
			};
		},
		placeholder:function(){//返回顶部
				supportPlaceholder='placeholder'in document.createElement('input'),
				placeholder=function(input){
					var text = input.attr('placeholder'),
					defaultValue = input.val();
					if(!defaultValue){
						input.val(text).addClass("phcolor");
					}
					input.focus(function(){
						if(input.val() == text){
							$(this).val("");
						}
					});
					input.blur(function(){
						if(input.val() == ""){
							$(this).val(text).addClass("phcolor");
						}
					});
					input.keydown(function(){
						$(this).removeClass("phcolor");
					});
					input.change(function(){
						$(this).removeClass("phcolor");
					});
				};

			if(!supportPlaceholder){
				$('input').each(function(){
					text = $(this).attr("placeholder");
					if($(this).attr("type") == "text"){
						placeholder($(this));
					}
				});
			}
		},
		reset:function () {
			$(":reset").on("click", function () {
				setTimeout(function () {
                    $.placeholder();
                },50)

			});
        },
        setposition:function () {
            if (document.body.clientWidth < 1200) {
                if ($('#sideRightBar').length > 0) {
                    $('.dangzhu2').css('marginRight', 443 - (1210 - document.body.clientWidth) / 2 + 'px');
                    $('.dangzhu1').css('marginLeft', 595 - (1210 - document.body.clientWidth) / 2 - 88 + 'px');
                    $(".wstyle,#topBarInfo2,.bcontrol").css("width", document.body.clientWidth - 260 + "px");
                    $('#sideRightBar').css("marginLeft", $(".page-content").width() / 2 + 50 + "px");
                } else {
                    $('.dangzhu2').css('marginRight', 443 - (1210 - document.body.clientWidth) / 2 + 'px');
                    $('.dangzhu1').css('marginLeft', 595 - (1210 - document.body.clientWidth) / 2 + 'px');
                }
            } else if (document.body.clientWidth >= 1200) {
                $('.dangzhu2').css('marginRight', 443 + 'px');
                $('.dangzhu1').css('marginLeft', 595 + 'px');
                if (1200 <= document.body.clientWidth < 1350) {
                    if ($('#sideRightBar').length > 0) {
                        var otmp = document.body.clientWidth - $('.container_main').width();
                        $('.dangzhu2').css('marginRight', 443 - (1210 - document.body.clientWidth) / 2 + 'px');
                        $('.dangzhu1').css('marginLeft', $(".page-content").width() / 2 + 32 + 'px');
                        $(".wstyle,#topBarInfo2,.bcontrol").css("width", document.body.clientWidth - 260 - otmp + "px");
                        $('#sideRightBar').css("marginLeft", $(".page-content").width() / 2 + 50 + "px");
                    }
                }
                if (document.body.clientWidth >= 1350) {
                    if ($('#sideRightBar').length > 0) {
                        $('.dangzhu2').css('marginRight', 443 + 'px');
                        $('.dangzhu1').css('marginLeft', 595 + 'px');
                        $('#sideRightBar').css("marginLeft", "595px");
                        $('.wstyle').css('width', '100%');
                        $('#topBarInfo2,.bcontrol').css('width', "1036px");
                    }
                }
            }
        },
        getExplorerInfo:function(){
        var explorer = window.navigator.userAgent.toLowerCase();
        //ie
        if (explorer.indexOf("msie") >= 0) {
            var ver = explorer.match(/msie ([\d.]+)/)[1];
            if (ver == 8.0) {
                return true;
            }
        } else {
            return false;
        }
    }

});

	if (!Array.prototype.filter)
	{
		Array.prototype.filter = function(fun /*, thisArg */)
		{
			"use strict";

			if (this === void 0 || this === null)
				throw new TypeError();

			var t = Object(this);
			var len = t.length >>> 0;
			if (typeof fun !== "function")
				throw new TypeError();

			var res = [];
			var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
			for (var i = 0; i < len; i++)
			{
				if (i in t)
				{
					var val = t[i];

					// NOTE: Technically this should Object.defineProperty at
					//       the next index, as push can be affected by
					//       properties on Object.prototype and Array.prototype.
					//       But that method's new, and collisions should be
					//       rare, so use the more-compatible alternative.
					if (fun.call(thisArg, val, i, t))
						res.push(val);
				}
			}

			return res;
		};
	}
})(jQuery);


var panel={
	init:function(){
		$('.zt-ico-w').cachet();
		$('.btn-group').selectLisr();
		$('.zkBtn').rotateArrow();
		$('.hj-tb').hoverRow();
		$.scrollTop();	
		$.placeholder();
		$.reset();
		$('.navbar-nav').navbarNav();
	},
};

$(function () {
	var s={
		init:function(){
			panel.init();
		},
	};
	s.init();
});
/*重写js四舍五入*/
Number.prototype.toFixed=function (d) {
	var s=this+"";
	if(!d)d=0;
	if(s.indexOf(".")==-1)s+=".";
	s+=new Array(d+1).join("0");
	if(new RegExp("^(-|\\+)?(\\d+(\\.\\d{0,"+(d+1)+"})?)\\d*$").test(s)){
		var s="0"+RegExp.$2,pm=RegExp.$1,a=RegExp.$3.length,b=true;
		if(a==d+2){
			a=s.match(/\d/g);
			if(parseInt(a[a.length-1])>4){
				for(var i=a.length-2;i>=0;i--){
					a[i]=parseInt(a[i])+1;
					if(a[i]==10){
						a[i]=0;
						b=i!=1;
					}else break;
				}
			}
			s=a.join("").replace(new RegExp("(\\d+)(\\d{"+d+"})\\d$"),"$1.$2");

		}if(b)s=s.substr(1);
		return (pm+s).replace(/\.$/,"");
	}return this+"";

};

//(function(){
//
//})();
