$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

var requestId = $("#requestId").val(); //询单id
$(document).ready(function () {
    refreshLocal();
    //setInterval(refreshLocal(), 1000 * 60); //每分钟刷新
});

//局部刷新
function refreshLocal() {
    var type = $("#biddingMethod").val();
    var supplierNumUrl = ctx + "/biddingHall/querySupplierNum";  //获取参与供应商数
    var minPrice2LineUrl = ctx + "/biddingHall/queryMinPirceByLine";  //获取当前最低价（按物料行）
    var minPrice2TotalUrl = ctx + "/biddingHall/queryMinPirceByTotal";  //获取当前最低价（按总价）
    var totalBid2LineUrl = ctx + "/biddingHall/queryTotalBidByLine";  //获取供应商数出价总次数（按物料行）
    var totalBid2TotalUrl = ctx + "/biddingHall/queryTotalBidByTotal";  //获取供应商数出价总次数（按总价）

    var item2TotalUrl = ctx + "/biddingHall/queryItemByTotal";  //获取物料列表（按总价）
    var item2LineUrl = ctx + "/biddingHall/queryItemByLine";  //获取物料列表（按物料行）

    switch (type) {
        case '0': // 按物料行
            outlineAjaxFunc(supplierNumUrl, requestId, "supplierNum");
            outlineAjaxFunc(totalBid2LineUrl, requestId, "totalBid");
            outlineAjaxFunc(minPrice2LineUrl, requestId, "minPrice", type);
            item2Line(item2LineUrl, requestId);
            break;
        case '1': // 按总价
            outlineAjaxFunc(supplierNumUrl, requestId, "supplierNum");
            outlineAjaxFunc(totalBid2TotalUrl, requestId, "totalBid");
            outlineAjaxFunc(minPrice2TotalUrl, requestId, "minPrice", type);
            item2Total(item2TotalUrl, requestId);
            break;
        default:
            break;
    }
    supplier2Pre(1);
    supplier2Quo(1);
}

// 物料列表 -按物料行竞价
function item2Line(url, requestId) {
    var colModel = [{
        label: '物料代码',
        name: 'materialNo',
        index: 'MATERIAL_NO',
        width: 80,
    },
        {
            label: '物料名称',
            name: 'materialName',
            index: 'MATERIAL_NAME',
            width: 80,
        },
        {
            label: '型规',
            name: 'character',
            index: 'CHARACTER',
            width: 80
        },
        {
            label: '数量',
            name: 'requestAmount',
            index: 'REQUEST_AMOUNT',
            width: 80
        },
        {
            label: '单位',
            name: 'unit',
            index: 'UNIT',
            width: 80
        },
        {
            label: '竞价供应商数',
            name: 'supplierNum',
            index: 'SUPPLIER_NUM',
            width: 80,
            sortable: false
        },
        {
            label: '供应商出价总次数',
            name: 'totalBid',
            index: 'TOTAL_BID',
            width: 80,
            sortable: false
        },
        {
            label: '当前最低价',
            name: 'minPrice',
            index: 'MIN_PRICE',
            width: 80,
            sortable: false
        }
    ];

    RFQ.jqGrid('#jqGrid').init({
        url: url,
        colModel: colModel,
        postData: {"requestId": requestId},
        //width: 500,
        sortname: 'MATERIAL_NO',
        sortorder: 'asc',
        pager: "#jqGridPager"
    });
}

// 物料列表 -按总价竞价
function item2Total(url, requestId) {
    var colModel = [{
        label: '物料代码',
        name: 'materialNo',
        index: 'MATERIAL_NO',
        width: 80,
    },
        {
            label: '物料名称',
            name: 'materialName',
            index: 'MATERIAL_NAME',
            width: 80,
        },
        {
            label: '型规',
            name: 'character',
            index: 'CHARACTER',
            width: 80
        },
        {
            label: '品牌',
            name: 'producer',
            index: 'PRODUCER',
            width: 80,
        },
        {
            label: '数量',
            name: 'requestAmount',
            index: 'REQUEST_AMOUNT',
            width: 80
        },
        {
            label: '单位',
            name: 'unit',
            index: 'UNIT',
            width: 80
        },
        {
            label: '交货期',
            name: 'requestDeliveryDate',
            index: 'REQUEST_DELIVERY_DATE',
            width: 80,
            formatter: 'date',
            formatoptions: {srcformat: 'Y-m-d H:i:s', newformat: 'Y-m-d'}
        }
    ];

    RFQ.jqGrid('#jqGrid').init({
        url: url,
        colModel: colModel,
        postData: {"requestId": requestId},
        //width: 500,
        sortname: 'MATERIAL_NO',
        sortorder: 'asc',
        pager: "#jqGridPager"
    });
}

// 供应商列表 -未审核
function supplier2Pre(page) {
    var pageSize = 6;
    $.ajax({
        type: "POST",
        url: ctx + "/biddingHall/queryPreSupplier",
        dataType: "json",
        cache: false,
        data: {
            page: page,
            rows: pageSize,
            requestId: requestId
        },
        success: function (data) {
            if (data.total == 0) {
                $("#tabgs2").empty();
            } else {
                createGrid(data.list, "tabgs2");
                if(data.total > 6){
                    showPageFun(data.pageNum, pageSize, data.total, "supplier2Pre", "tabgs2");
                }
            }
        }
    });
}

// 供应商列表 -报价
function supplier2Quo(page) {
    var pageSize = 6;
    $.ajax({
        type: "POST",
        url: ctx + "/biddingHall/queryQuoSupplier",
        dataType: "json",
        cache: false,
        data: {
            page: page,
            rows: pageSize,
            requestId: requestId
        },
        success: function (data) {
            if (data.total == 0) {
                $("#tabgs1").empty();
            } else {
                createGrid(data.list, "tabgs1");
                if(data.total > 6){
                    showPageFun(data.pageNum, pageSize, data.total, "supplier2Quo", "tabgs1");
                }
            }
        }
    });
}

function createGrid(json, divId) {
    var target = $("#" + divId);
    if (target) {
        target.empty();
        for (var i in json) {
            var row = [];
            row.push("<div class=\"bj-list\">");
            row.push("<dl>");
            row.push("<dt><i class=\"icon icon-user head-noon\"></i></dt>");
            row.push("<dd><span class=\"mr5 c333\">" + json[i].linkmanName + "</span><span class=\"noon font12\">【离线】</span></dd>");
            row.push("<dd>" + json[i].supplierName + "</dd>");
            row.push("</dl>");
            row.push("</div>");
            target.append(row.join(""));
        }
    }
}

// 公共ajax 根据不同url、参数获取数据赋值给不同对象
function outlineAjaxFunc(url, requestId, objId, type) {
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        data: {
            requestId: requestId
        },
        success: function (data) {
            if (objId == 'minPrice' && type == '0' && typeof(data.minPrice) != "undefined") {
                $("#" + objId).html(data.minPrice);
                if (data.material != null && typeof(data.material) != "undefined") {
                    $("#minMaterial").append("<dd>物料代码:" + data.material.materialNo + "</dd>");
                    $("#minMaterial").append("<dd>物料名称:" + data.material.materialName + "</dd>");
                    $("#minMaterial").append("<dd>型规:" + data.material.character + "</dd>");
                    $("#minMaterial").append("<dd>数量:" + data.material.requestAmount + "</dd>");
                    $("#minMaterial").append("<dd>单位:" + data.material.unit + "</dd>");
                    $("#minMaterial").append("<dd>竞价供应商数:" + data.material.supplierNum + "</dd>");
                    $("#minMaterial").append("<dd>供应商出价总次数:" + data.material.totalBid + "</dd>");
                    $("#minMaterial").append("<dd>当前最低价:" + data.material.minPrice + "</dd>");
                } else {
                    $(".nav-pripop").hide();
                }
            } else {
                if (data == '') {
                    $("#" + objId).html("暂无");
                    if("supplierNum"==objId){
                        $("#supplier").hide();
                    }
                    if("totalBid"==objId){
                        $("#total").hide();
                    }
                    if("minPrice"==objId){
                        $("#Price").hide();
                    }
                } else {
                    $("#" + objId).html(data);
                }
                $(".nav-pripop").hide();
            }
        },
        error: function () {
            $.zui.messager.show('请求失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
}

if('0'==$("#biddingMethod").val()){
    $(".nav-pri").mouseover(function(){$(".nav-pripop").show();});
    $(".nav-pri").mouseout(function(){$(".nav-pripop").hide();});
}
