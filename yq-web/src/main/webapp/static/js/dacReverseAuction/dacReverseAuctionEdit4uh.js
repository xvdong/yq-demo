/**
 * Created by lxu on 2016/12/9.
 */

//切换样式
var state = null;
$.ajax({
    url: ctx+"/rfqOperateHabits/getUserHabits?operateType=0",
    type:'post',
    dataType: "json",
    async: false,
    success: function(data) {
        if(data.rspcod == '1'){
            if(data.obj){
                state = data.obj.operateValue||0;
            }
        }
    }
});
page();

$('.view-option-single').click(function(){
    state =0;
    page();
    $.ajax({url: ctx+"/rfqOperateHabits/saveUserHabits?operateType=0&operateValue=0", type: 'post', dataType: "json", success: function(data) {
            if(data.rspcod == '1'){
                //静默保存，暂不处理，不影响用户使用
            }
        }
    });
});

$('.view-option-double').click(function(){
    state =1;
    page();
    $.ajax({url: ctx+"/rfqOperateHabits/saveUserHabits?operateType=0&operateValue=1", type: 'post',dataType: "json", success: function(data) {
        if(data.rspcod == '1'){
            //静默保存，暂不处理，不影响用户使用
        }
    }
    });
})

//切换显示样式
function page(){
    if(state==null){
        $('#changeViewModal').modal('toggle', 'center');
    }else if(state==0){
        $('#stepBar').removeClass('ke-toolbar');
        $('.bcontrol').removeClass('ke-toolbar');
        $('.panel-block').addClass('ke-toolbar');
        $('#btn-r-b').addClass('ke-toolbar');
        $('#sideRightBar').addClass('ke-toolbar');
        $('.panel-block').eq(0).removeClass('ke-toolbar');
        $('.panel-block').eq(1).removeClass('ke-toolbar');
        step();
        toSite();
    }
    if(state==1){
        $('.panel-block').removeClass('ke-toolbar');
        $('#btn-r-b').removeClass('ke-toolbar');
        $('#sideRightBar').removeClass('ke-toolbar');
        $('#stepBar').addClass('ke-toolbar');
        $('.bcontrol').addClass('ke-toolbar');
        $(".page-content .ke-container").css("width","100%");
    }
}

//滚动监听
$(window).bind('scroll', scrollevent);
function scrollevent() {
    if (state == 1) {
        if ($(window).scrollTop() >= $('#topBarInfo').offset().top) {
            if ($(window).scrollTop() + $(window).height() >= $('#btn-r-b').offset().top) {
                $('#topBarInfo2').hide()
            } else {
                $('#topBarInfo2').show()
            }
        } else {
            $('#topBarInfo2').hide()
        }
        $('.page-content .panel-block').each(function () {
            if ($(window).scrollTop() >= $(this).offset().top) {
                if ($(this).index() == 1 || $(this).index() == 2) {
                    $('.ui-rightbar-list').removeClass('active')
                    $('.ui-rightbar-list').eq(1).addClass('active');
                }
                if ($(this).index() == 3 || $(this).index() == 4 || $(this).index() == 5) {
                    $('.ui-rightbar-list').removeClass('active')
                    $('.ui-rightbar-list').eq($(this).index() - 1).addClass('active');
                }
                if ($(this).index() == 6) {
                    $('.ui-rightbar-list').removeClass('active')
                    $('.ui-rightbar-list').eq($(this).index() - 1).addClass('active');
                }
            } else if ($(window).scrollTop() < $('.page-content .panel-block').eq(0).offset().top) {
                $('.ui-rightbar-list').removeClass('active')
                $('.ui-rightbar-list').eq(0).addClass('active');
            }

        });
    }
}
scrollevent();

$('.ui-rightbar-list').click(function(){
    $(window).off('scroll', scrollevent);
    $('.ui-rightbar-list').removeClass('active');
    $(this).addClass('active');
    if($(this).index()==1){
        $('html,body').animate({scrollTop:  0}, 300, function(){$(window).on('scroll', scrollevent)});
    }else{
        $('html,body').animate({scrollTop:  $('.page-content').children('.panel-block').eq($(this).index()).offset().top-70}, 300, function(){$(window).on('scroll', scrollevent)});
    }

});


//分布显示效果
var prow;
var pagearr;
prow = 0;
pagearr = [[1,2],[3],[4],[5,6],[7],[8],[9],[10]];
function step(){
    stepBar.init("stepBar", {
        step : 1,
        change : true,
        animation : true
    });
}

$(".ui-stepInfo").click(function(){
    prow= $(this).index();
    stepBar.init("stepBar", {
        step : prow+1,
        change : true,
        animation : true
    });
    nextrow();
    toSite();
})
$(".pagebtn .bottom").click(function(){
    if(prow<7){
        prow++;
    }
    stepBar.init("stepBar", {step : prow+1,change : true,animation : true,});
    nextrow();
    toSite();
})
$(".pagebtn .top").click(function(){
    if(prow>0){
        prow--;
    }
    stepBar.init("stepBar", {step : prow+1,change : true,animation : true,});
    nextrow();
    toSite();
})

function nextrow(){
    $(".page-content .ke-container").css("width","100%");
    $(".page-content .panel-block").addClass('ke-toolbar');
    for(i in $(".page-content .panel-block")){
        for(k in pagearr){
            if(prow == k){
                for(m in pagearr[k]){
                    $(".page-content .panel-block").eq(pagearr[k][m]-1).removeClass('ke-toolbar');
                }
            }
        }
    }
    if(prow==0){
        $(".page-content .top").addClass('ke-toolbar');
        $(".page-content .bottom").removeClass('ke-toolbar');
    }else if(prow==$(".page-content .panel-block").length-3){
        $(".page-content .top").removeClass('ke-toolbar');
        $(".page-content .bottom").addClass('ke-toolbar');
    }else{
        $(".top").removeClass('ke-toolbar');
        $(".bottom").removeClass('ke-toolbar');
    }
    $("#supplierListJqGrid").jqGrid('resizeGrid');
}

function toSite(){
    for(var i=0; i<prow; i++){
        $(".ui-stepInfo a").eq(i).removeClass();
        $(".ui-stepInfo a").eq(i).addClass('ui-stepSequence judge-stepSequence-pre judge-stepSequence-pre-change')
    }
}

//物料表格超出
$.setposition();
stepBar.init("stepBar", {step: prow + 1, change: true, animation: true,});

$(window).resize(function () {
    $.setposition();
    stepBar.init("stepBar", {step: prow + 1, change: true, animation: true,});
    toSite();
})