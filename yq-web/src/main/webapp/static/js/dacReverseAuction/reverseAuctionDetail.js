$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    var requestId = $('#requestId').val();
    var gys_login = $('#gys_login').val();

    var colModel = [{
        label: '物料代码',
        name: 'materialNo',
        index: 'MATERIAL_NO',
        width: 80,
    },
        {
            label: '物料名称',
            name: 'materialName',
            index: 'MATERIAL_NAME',
            width: 80,
        },
        {
            label: '数量',
            name: 'requestAmount',
            index: 'REQUEST_AMOUNT',
            width: 80
        },
        {
            label: '单位',
            name: 'unit',
            index: 'UNIT',
            width: 80
        },
        {
            label: '型规',
            name: 'character',
            index: 'CHARACTER',
            width: 80
        },
        {
            label: '品牌',
            name: 'producer',
            index: 'PRODUCER',
            width: 80,
        },
        {
            label: '参考价',
            name: 'salePrice',
            index: 'SALE_PRICE',
            width: 80,
        },
        {
            label: '交货期',
            name: 'requestDeliveryDate',
            index: 'REQUEST_DELIVERY_DATE',
            width: 80,
            formatter: 'date',
            formatoptions: {srcformat: 'Y-m-d H:i:s', newformat: 'Y-m-d'}
        },
        {
            label: '备注',
            name: 'memo',
            index: 'MEMO',
            width: 80
        }
    ];
    var biddingMethod = $("#biddingMethod").val();

    if (gys_login !== "1") {
        colModel.splice(6, 1);
    }
    $.post(ctx + '/rfqRequest/getCustomItemHeader', {}, function (data) {
        $.each(data.obj, function (i, item) {
            var res = {};
            res.label = item.itemName;
            res.name = "itemValue_" + (i + 1);
            res.width = 80;
            colModel.push(res);
        });
        if (biddingMethod == '0') {
            var col1 = {};
            col1.label = "起拍价";
            col1.width = 82;
            col1.name = "startPrice";
            col1.index = "START_PRICE";
            colModel.push(col1);

            var col2 = {};
            col2.label = "梯度";
            col2.name = "priceGrad";
            col2.index = 'PRICE_GRAD';
            col2.width = 82;
            colModel.push(col2);
        }
        RFQ.jqGrid('#jqGrid').init({
            url: ctx + '/rfqRequest/rfqRequestItemList',
            colModel: colModel,
            postData: {"id": requestId},
            width: 500,
            sortname: 'SEQ',
            sortorder: 'ASC',
            pager: "#jqGridPager",
            rowNum: 20,//每页显示记录数
            rowList: [20, 30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        });
    }, 'json');

    $('#payCash').click(function () {
        $('#payCashForm').ajaxSubmit({
            success: function (data) {
                RFQ.info(data.rspmsg);
                location.reload();
            }
        })
    });
    //放弃报价
    $('#giveUpQuo').on('click', '.give-up', function () {
        $('#giveUpQuo').ajaxSubmit({
            type: 'post', // 提交方式 get/post
            url: ctx + '/rfqQuotation/giveUpQuotation', // 需要提交的 url
            dataType: 'json',
            success: function (data) {
                if (data.rspcod != 200) {
                    RFQ.error(data.rspmsg);
                    return;
                } else {
                    location.href = ctx + "/rfqQuotationHisotry/getRfqQuotationHistory?requestId=" + data.rspmsg;
                }
            },
            error: function (XmlHttpRequest, textStatus, errorThrown) {
                RFQ.error('出错了！');
            }
        });
    });

    if ('NoStart' != window.sessionStorage.designateResult_tax) {
        //$("#hidd").hide();
        $(".costom").hide();
    } else {
        window.sessionStorage.designateResult_tax = null;
    }


    //供应商信息
    var isSupplierRequirement = $("#isSupplierRequirement").val();
    if (isSupplierRequirement == '0') {
        $("#supplierListJqGrid").jqGrid({
            datatype: 'local',
            rownumbers: true,
            rownumWidth: 50,
            colModel: [
                {
                    label: 'id',
                    name: 'id',
                    index: 'id',
                    key: true,
                    hidden: true
                },
                {
                    label: '合格供应商代码',
                    name: 'supplierCode',
                    index: 'supplierCode',
                    align: 'center'
                },
                {
                    label: '特邀供应商名称',
                    name: 'supplierName',
                    index: 'supplierName',
                    align: 'center'
                },
                {
                    label: '联系人',
                    name: 'linkmanName',
                    index: 'linkmanName',
                    align: 'center'
                },
                {
                    label: '联系电话',
                    name: 'linkmanTelphone',
                    index: 'linkmanTelphone',
                    align: 'center'
                    // formatter: lockChangeFormatter
                },
                {
                    label: '微信状态',
                    name: 'isBindWechat',
                    index: 'isBindWechat',
                    align: 'center'
                    // formatter: lockChangeFormatter
                }
            ],
            viewrecords: true,
            //sortname: 'userLoginNo',
            //sortorder: 'asc',
            width: "970",
            height: "100%",
            pager: "#supplierListjqGridPager",
            rowNum: 20,//每页显示记录数
            rowList: [20, 30, 50, 100]//用于改变显示行数的下拉列表框的元素数组。
        });
    } else if (isSupplierRequirement == '1') {
        $("#supplierListJqGrid").jqGrid({
            datatype: 'local',
            rownumbers: true,
            rownumWidth: 50,
            colModel: [
                {
                    label: 'id',
                    name: 'id',
                    index: 'id',
                    key: true,
                    hidden: true
                },
                {
                    label: '合格供应商代码',
                    name: 'supplierCode',
                    index: 'supplierCode',
                    align: 'center'
                },
                {
                    label: '特邀供应商名称',
                    name: 'supplierName',
                    index: 'supplierName',
                    align: 'center'
                },
                {
                    label: '联系人',
                    name: 'linkmanName',
                    index: 'linkmanName',
                    align: 'center'
                },
                {
                    label: '联系电话',
                    name: 'linkmanTelphone',
                    index: 'linkmanTelphone',
                    align: 'center'
                    // formatter: lockChangeFormatter
                },
                {
                    label: '微信状态',
                    name: 'isBindWechat',
                    index: 'isBindWechat',
                    align: 'center'
                    // formatter: lockChangeFormatter
                }
            ],
            viewrecords: true,
            //sortname: 'userLoginNo',
            //sortorder: 'asc',
            width: "970",
            height: "100%",
            pager: "#supplierListjqGridPager",
            rowNum: 20,//每页显示记录数
            rowList: [20, 30, 50, 100]//用于改变显示行数的下拉列表框的元素数组。
        });
    }
    $("#supplierListJqGrid").jqGrid('setLabel', 0, "序号");
    if (isSupplierRequirement == '0') {

        var supplierList =  $("#supplierList").val() == "" ? null : JSON.parse($("#supplierList").val());
        if (supplierList != undefined && supplierList != null) {

            $("#supplierListJqGrid").jqGrid('setGridParam', {datatype: 'local', data: supplierList}).trigger('reloadGrid');
        }
    } else if (isSupplierRequirement == '1') {


        var preauditSupplierList = $("#preauditSupplierList").val() ? null : JSON.parse($("#preauditSupplierList").val());
        if (preauditSupplierList != undefined && preauditSupplierList != null) {

            $("#supplierListJqGrid").jqGrid('setGridParam', {datatype: 'local', data: preauditSupplierList}).trigger('reloadGrid');
        }
    }


    });



/*查看是否缴纳保证金*/
function startPriceformat() {
    $.ajax({
        url: "money",
        data: {requestId: $("#requestId").val()},
        type: "POST",
        datatype: "json",
        success: function (data) {
            if ('0' == data.flag && '1' == $("#needAssureMoney").val() && '0' == $("#assureFlag").val()) {
                $.zui.messager.show('请缴纳保证金！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                return false;
            } else if ('1' == data.flag || '0' == data.flag) {
                if ('0' == $("#rules").val()) {
                    window.sessionStorage.type = "";
                    var aa=window.open();
                    aa.location.href = ctx + "/dacQuotationSingle/init?requestId=" + $("#requestId").val();
                }
                if ('1' == $("#rules").val()) {
                    var aa=window.open();
                    aa.location.href = ctx + "/dacQuotationTotal/init?requestId=" + $("#requestId").val();
                }
            }
        }
    });
}

/**
 * 审核记录
 */
function auditHistory(){
    var requestId = $('#requestId').val();
    location.href = ctx+"/rfqRequest/listActivityHistory?requestId="+requestId;
};

function freshQuotation(requestId,type) {
    window.sessionStorage.type="";
    var aa = window.open();
    var url = '/dacQuotationTotal/tryBidding?requestId='+requestId+"&type="+type;
    aa.location.href = url;
};