$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    //全局缓存
    var supplierList = {}; //邀请供应商
    var projectUserList =  {};//角色

    new RFQ.Init();

    //从历史询单创建
    (function(){
        //初始化jqGrid 默认不加载数据
        var jqGrid = null;

        function initJqGrid(){
            return RFQ.jqGrid('#jqGridHis').init({
                url:ctx+ '/rfqRequest/rfqRequestList',
                autoLoad:false,
                beforeSelectRow: beforeSelectRow,//js方法
                colModel: [
                    {
                        label: '主键',
                        name: 'id',
                        index: 'id',
                        width: 20,
                        key: true,
                        hidden: true
                    },
                    {
                        label: '询价单号',
                        name: 'unifiedRfqNum',
                        index: 'UNIFIED_RFQ_NUM',
                        width: 20,
                        formatter:selectFormatter
                    },
                    {
                        label: '询单标题',
                        name: 'title',
                        index: 'TITLE',
                        width: 20,
                    },
                    {
                        label: '创建人',
                        name: 'recCreatorUsername',
                        index: 'REC_CREATOR_USERNAME',
                        width: 20
                    },
                    {
                        label: '创建时间',
                        name: 'recCreateTime',
                        index: 'REC_CREATE_TIME',
                        width: 20
                    },
                    {
                        label: '询单状态',
                        name: 'type',
                        index: 'TYPE',
                        width: 20
                    }
                ],
                pager: "#jqGridHisPager"
            });
        }

        $("#btnSearchHis").click(function () {
            jqGrid.reload(form2Json("searchFormHis"));
        });
        //弹窗显示加载数据
        $('#myModa1').on('shown.zui.modal', function() {
            if(!jqGrid){
                jqGrid = initJqGrid();
                jqGrid.reload();
            }
            jqGrid.resize();
        });

        /**
         * 实现复选框单选
         */
        function beforeSelectRow() {
            $("#jqGridHis").jqGrid('resetSelection');
            return(true);
        }

        /*function formatTitle(cellValue, options, rowObject) {
         return cellValue.substring(0, 50) + "...";
         };*/
        /**
         * 竞价单编号链接跳转
         */
        function selectFormatter(cellvalue, options, rawObject) {
            var id = rawObject.id;
            return "<a class='blue' href='"+ctx+"/rfqReverseAuction/rfqRequestDetail?id="+id+"' target='_blank' >"+cellvalue+"</a>";
        }

        window.addHis = function(){
            var ids = $('#jqGridHis').jqGrid('getGridParam', 'selarrrow');
            console.log('ids:'+ids);
            if(ids.length==0){
                $('#myModa1').modal('hide');
                return;
            }/* else if(ids.length > 1) {
             $.zui.messager.show('选择的数据超出范围！', {placement: 'center', type: 'warning', icon: 'icon-warning-sign'});
             return;
             } */else {
                $("#rid").val(ids);
                //console.log('rid:'+$("#rid").val());
                $.ajax({
                    type: "POST",
                    url: ctx+"/rfqRequest/addRequestByHis",
                    data: {
                        rid: $("#rid").val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            $('#myModa1').modal('hide');
                            //成功后跳转到编辑页
                            location.href = ctx+"/rfqRequest/toUpdateRfqRequest?id="+data.requestId;
                        } else {
                            $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                        }
                    }
                });
            }
        }
    })();
    //END

    //邀请供应商
    (function(){

        var jqGrid = null;

        $('#superlist').click(function(){
            getAttData("#jqGridsup","#jqGridPagersup");

        });

        function getAttData(grid,page){
            jqGrid = RFQ.jqGrid(grid).init({
                url:ctx+ '/rfqSupplierListNew/rfqSupplierListListNew',
                colModel: [
                    {
                        name: 'id',
                        index: 'id',
                        key: true,
                        hidden: true
                    },
                    {
                        name: 'companyId',
                        index: 'companyId',
                        hidden: true
                    },
                    {
                        label: '供应商U代码',
                        name: 'supplierCode',
                        width: 20,
                    },
                    {
                        label: '供应商名称',
                        name: 'supplierName',
                        width: 20,
                    },
                    {
                        label: '所属类别',
                        name: 'type',
                        width: 20,
                    },
                    {
                        label: '供应商等级',
                        name: 'deviceDealerGrade',
                        width: 20,
                    },
                    {
                        label: '联系人',
                        name: 'linkmanName',
                        width: 20,
                    },
                    {
                        label: '联系电话',
                        name: 'linkmanTelphone',
                        width: 20
                    }
                ],
                pager: page
            });
        }

        /*function formatTitle(cellValue, options, rowObject) {
         return cellValue.substring(0, 50) + "...";
         };*/

        /*function actFormatter(cellvalue, options, rawObject) {

         var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqProjectUserDetail(\'' + rawObject.puId + '\')"><i class="icon icon-search"></i></a>';

         var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqProjectUser(\'' + rawObject.puId + '\')"><i class="icon icon-edit"></i></a>';

         var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqProjectUser(\'' + rawObject.puId + '\')"><i class="icon icon-times"></i></a>';

         return detailBtn + editBtn + deleteBtn;
         };*/

        $("#btnSearch").click(function () {
            jqGrid.reload( form2Json("searchForm"));
        });

        //$("#addRfqProjectUser").click(function () {
        //	location.href = "editRfqProjectUser?oper=add"
        //});

        //DOM操作
        var rowTmp = '<tr><td>{index}</td> <td>{supplierCode}</td> <td>{supplierName}</td> <td>{linkmanName}</td><td>{linkmanTelphone}</td> <td>未绑定</td> <td><a class="order-del-btn" data-id="{id}"><i class="red icon icon-trash"></i>&nbsp; 删除</a></td> </tr>';
        var rowIndex = 0;
        var rows = [{supplierCode:'2001',supplierName:'测试1',linkmanName:'jake',linkmanTelphone:'18600000000',id:'1'},
            {supplierCode:'2002',supplierName:'测试2',linkmanName:'jake',linkmanTelphone:'18600000000',id:'2'}];
        //$('#supplist').on('click','.js-confirm', function () {
        //	var rows = $('#jqGridsup').jqGrid("getGridParam", "selarrrow");
        //	console.log(rows);
        var renderHTML = [];
        $.each(rows,function(i,rowData){
            //if(supplierList[r]) return true;
            //var rowData = $('#jqGridsup').jqGrid('getRowData',r);//获取选中行的数据（单行）
            rowData.index = rowIndex+1;
            renderHTML.push(rowTmp.format(rowData));
            rowIndex ++;
            //数据加入缓存
            supplierList[rowData.id] = rowData;
        });
        $('#supplierListTable tbody').append(renderHTML.join(''));
        $('#supplist').modal('hide');
        //});

        //删除
        $('#supplierListTable').on('click','.order-del-btn',function(){
            var id = $(this).data("id");
            $(this).parent().parent("tr").remove();
            //从缓存里面删除
            delete supplierList[id];
            rowIndex > 0 && rowIndex--;
        });

    })();
    //END


    //角色设置
    (function(){
        var jqGrid = null;
        $('#prouser').click(function(){
            getAttData("#jqGridphone","#jqGridPagerphone");

        });

        function getAttData(grid,page){
            jqGrid = RFQ.jqGrid(grid).init({
                url: ctx+'/rfqProjectUserNew/rfqProjectUserListNew',
                colModel: [
                    {
                        label: '主键',
                        name: 'puId',
                        index: 'puId',
                        width: 20,
                        key: true,
                        hidden: true
                    },
                    {
                        label: '工号',
                        name: 'jobCode',
                        index: 'JOB_CODE',
                        width: 20,
                    },
                    {
                        label: '姓名',
                        name: 'userName',
                        index: 'USER_NAME',
                        width: 20,
                    },
                    {
                        label: '所属科室',
                        name: 'userOffice',
                        index: 'USER_OFFICE',
                        width: 20,
                    },
                    {
                        label: '职位',
                        name: 'userTitle',
                        index: 'USER_TITLE',
                        width: 20
                    }
                ],
                prmNames: {
                    id: 'puId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
                },
                sortname: 'PU_ID',
                pager: page,
            });
        }

        /*function formatTitle(cellValue, options, rowObject) {
         return cellValue.substring(0, 50) + "...";
         };

         function actFormatter(cellvalue, options, rawObject) {

         var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqProjectUserDetail(\'' + rawObject.puId + '\')"><i class="icon icon-search"></i></a>';

         var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqProjectUser(\'' + rawObject.puId + '\')"><i class="icon icon-edit"></i></a>';

         var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqProjectUser(\'' + rawObject.puId + '\')"><i class="icon icon-times"></i></a>';

         return detailBtn + editBtn + deleteBtn;
         };*/

        $("#btnSearch").click(function () {
            jqGrid.reload(form2Json("searchForm"));
        });

        /*$("#addRfqProjectUser").click(function () {
         location.href = "editRfqProjectUser?oper=add"
         });

         function rfqProjectUserDetail(puId) {
         location.href = "rfqProjectUserDetail?puId=" + puId;
         }

         function editRfqProjectUser(puId) {
         location.href = "editRfqProjectUser?oper=edit&puId=" + puId;
         }

         function deleteRfqProjectUser(puId) {
         location.href = "delRfqProjectUser?puId=" + puId;
         }*/

        //DOM操作
        var rowIndex = 0;
        var rowTmp = '<tr id="puser_{puId}"> <td>{index}</td> <td>{jobCode}</td> <td>{userName}</td> <td>{userOffice}</td> <td>{userTitle}</td> <td><input name="" type="checkbox"></td> <td><input name="" type="checkbox" checked></td> <td><input name="" type="checkbox" checked></td> <td><a class="order-del-btn" data-id="{puId}" ><i class="icon icon-trash red"></i>&nbsp; 删除</a></td> </tr>';
        //$('#myModaphone').on('click','.js-confirm', function () {
        //	var rows = $('#jqGridphone').jqGrid("getGridParam", "selarrrow");
        //	console.log(rows);
        var rows = [{jobCode:'J0010',userName:'测试1',userOffice:'部门测试1',userTitle:'经理',puId:'1'},
            {jobCode:'J0011',userName:'测试2',userOffice:'部门测试2',userTitle:'副经理',puId:'2'}];
        var renderHTML = [];
        $.each(rows,function(i,rowData){
            //if(projectUserList[r]) return true;
            //var rowData = $('#jqGridphone').jqGrid('getRowData',r);//获取选中行的数据（单行）
            rowData.index = rowIndex + 1;
            renderHTML.push(rowTmp.format(rowData));
            rowIndex ++;
            projectUserList[rowData.puId] = rowData;
        });
        $('#roleTable tbody').append(renderHTML.join(''));
        $('#myModaphone').modal('hide');
        //});

        //删除
        $('#roleTable').on('click','.order-del-btn',function(){
            var id = $(this).data("id");
            $(this).parent().parent("tr").remove();
            delete projectUserList[id];
            console.log(projectUserList);
            rowIndex >0 && rowIndex--;
        });
    })();
    //END

    var editor,editor2,editor3;
    //商务与技术条款
    (function(){

        <!--富文本JS-->
        KindEditor.ready(function(K) {
            editor = K.create('textarea#contentSimple', {
                resizeType : 1,
                allowPreviewEmoticons : false,
                allowImageUpload : false,
                items : []
            });
        });
        KindEditor.ready(function(K) {
            editor2 = K.create('textarea#contentSimple2', {
                resizeType : 1,
                allowPreviewEmoticons : false,
                allowImageUpload : false,
                items : []
            });
        });
        KindEditor.ready(function(K) {
            editor3 = K.create('textarea#contentSimple3', {
                resizeType : 1,
                allowPreviewEmoticons : false,
                allowImageUpload : false,
                items : []
            });
        });

        var jqGrid = null;
        var jqGrid2 = null;

        $('#attachment').click(function(){
            jqGrid = getAttData("BL","#jqGrid","#jqGridPager");
        })
        $('#attachment2').click(function(){
            jqGrid2 = getAttData("TL","#jqGrid1","#jqGridPager1");
        })
        function getAttData(type,grid,page){
            return RFQ.jqGrid(grid).init({
                url: ctx+'/rfqLibTerms/rfqLibTermsList',
                colModel: [
                    {
                        label: '序号',
                        name: 'termId',
                        index: 'TERM_ID',
                        width: 40,
                        key: true,
                        hidden:true
                    },
                    {
                        label: '条款名称',
                        name: 'title',
                        index: 'TITLE',
                        width: 80
                    },
                    {
                        label: '条款内容',
                        name: 'content',
                        index: 'CONTENT',
                        width: 110
                    },
                    {
                        label: '备注',
                        name: 'remark',
                        index: 'REMARK',
                        width: 110
                    }
                ],
                postData:{type:type},
                prmNames: {
                    id: 'termId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
                },
                sortname: 'TERM_ID',
                sortorder: 'asc',
                pager:page
            });
        }

        /*function formatTitle(cellValue, options, rowObject) {
         return cellValue.substring(0, 50) + "...";
         };

         function actFormatter(cellvalue, options, rawObject) {

         var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqLibTermsDetail(\'' + rawObject.termId + '\')"><i class="icon icon-search"></i></a>';

         var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqLibTerms(\'' + rawObject.termId + '\')"><i class="icon icon-edit"></i></a>';

         var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqLibTerms(\'' + rawObject.termId + '\')"><i class="icon icon-times"></i></a>';

         return detailBtn + editBtn + deleteBtn;
         };

         $("#addRfqLibTerms").click(function () {
         location.href = "editRfqLibTerms?oper=add"
         });

         function rfqLibTermsDetail(termId) {
         location.href = "rfqLibTermsDetail?termId=" + termId;
         }

         function editRfqLibTerms(termId) {
         location.href = "editRfqLibTerms?oper=edit&termId=" + termId;
         }

         function deleteRfqLibTerms(termId) {
         location.href = "delRfqLibTerms?termId=" + termId;
         }*/

        $('#myModaBL').on('click','.js-confirm', function () {
            var rows = $('#jqGrid').jqGrid("getGridParam", "selarrrow");
            var renderHTML = [];
            $.each(rows,function(i,r){
                var rowData = $('#jqGrid').jqGrid('getRowData',r);//获取选中行的数据（单行）
                renderHTML.push(rowData.content);
            });
            editor3.html(renderHTML.join('\n'));
            $('#myModaBL').modal('hide');
        });

        $('#myModaTL').on('click','.js-confirm', function () {
            var rows = $('#jqGrid1').jqGrid("getGridParam", "selarrrow");
            var renderHTML = [];
            $.each(rows,function(i,r){
                var rowData = $('#jqGrid1').jqGrid('getRowData',r);//获取选中行的数据（单行）
                renderHTML.push(rowData.content);
            });
            editor2.html(renderHTML.join('\n'));
            $('#myModaTL').modal('hide');
        });
    })();
    //END

    //省市区域选择
    /*(function(){*/
    //显示省市区
    showProvince('level','1');
    //选择省市chang事件
    $('#deliveryProvince').bind('change', selProvince);
    $('#deliveryCity').bind('change', selCity);
    /**
     * 显示初始省市区信息
     */
    function showProvince(key1,value1) {
        //console.log('key1:'+key1)
        //key值获取变量的方法
        var data = {},key=key1;
        data[key]=value1;
        $.ajax({
            type: "POST",
            url: ctx+"/rfqRequest/refreshArea",
            data: data,
            cache: false,
            success: function (data) {
                if (null != data && "" != data) {
                    $.each(data, function (i, val) {
                        $("#deliveryProvince").append("<option value=" + val.code+','+val.name + ">" + val.name + "</option>");
                    });
                    /*for (var i = 0; i < data.length; i++) {
                     $("#deliveryProvince").append("<option value=" + data[i].code + ">" + data[i].name + "</option>");
                     }*/
                    selProvince();
                } else {
                    $.zui.messager.show('sorry,区域信息刷新出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    }

    /**
     * 选择省
     */
    function selProvince(){
        var code = $('#deliveryProvince').val().substring(0,6);
        //console.log('province:'+code)
        $.ajax({
            type: "POST",
            url: ctx+"/rfqRequest/refreshArea",
            data: {
                pCode: code
            },
            cache:false,
            success: function (data) {
                if (null!=data && ""!=data) {
                    //清空市区select
                    $("#deliveryCity").empty();
                    $("#deliveryArea").empty();
                    $.each(data, function(i,val){
                        $("#deliveryCity").append("<option value=" + val.code+','+val.name + ">" + val.name + "</option>");
                    });
                    selCity();
                } else {
                    $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                };
            }
        });
    }

    /**
     * 选择市
     */
    function selCity(){
        var code = $('#deliveryCity').val().substring(0,6);
        //console.log('city:'+code)
        $.ajax({
            type: "POST",
            url: ctx+"/rfqRequest/refreshArea",
            data: {
                pCode: code
            },
            cache:false,
            success: function (data) {
                if (null!=data && ""!=data) {
                    //清空区select
                    $("#deliveryArea").empty();
                    $.each(data, function(i,val){
                        $("#deliveryArea").append("<option value=" + val.code+','+val.name + ">" + val.name + "</option>");
                    });
                } else {
                    $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    }
    /*})();*/
    //END

    //常用地址设置
    (function(){
        var jqGrid = null;
        /*$('#prouser').click(function(){
         getAttData("#jqGridphone","#jqGridPagerphone");

         });*/
        $('#myModa8').on('shown.zui.modal', function() {
            if(!jqGrid){
                jqGrid = initAddreJqGrid();
                jqGrid.reload();
            }
            jqGrid.resize();
        });

        function initAddreJqGrid(grid,page){
            return RFQ.jqGrid('#AddreJqGrid').init({
                url:ctx+ '/rfqRequestContacts/rfqRequestContactsList',
                autoLoad:false,
                beforeSelectRow: beforeSelectRow,//js方法
                colModel: [
                    {
                        label: '主键',
                        name: 'id',
                        index: 'ID',
                        key: true,
                        hidden: true
                    },
                    {
                        label: '省id',
                        name: 'deliveryProvinceCode',
                        index: 'DELIVERY_PROVINCE_CODE',
                        hidden: true
                    },
                    {
                        label: '省',
                        name: 'deliveryProvince',
                        index: 'DELIVERY_PROVINCE',
                    },
                    {
                        label: '市id',
                        name: 'deliveryCityCode',
                        index: 'DELIVERY_CITY_CODE',
                        hidden: true
                    },
                    {
                        label: '市',
                        name: 'deliveryCity',
                        index: 'DELIVERY_CITY',
                    },
                    {
                        label: '区id',
                        name: 'deliveryAreaCode',
                        index: 'DELIVERY_AREA_CODE',
                        hidden: true
                    },
                    {
                        label: '区',
                        name: 'deliveryArea',
                        index: 'DELIVERY_AREA',
                    },
                    {
                        label: '具体地址',
                        name: 'deliveryAddress',
                        index: 'DELIVERY_ADDRESS',
                    }/*,
                     {
                     width: 80,
                     label: '操作',
                     name: 'id',
                     index: 'id',
                     sortable:false,
                     /!*formatter: actFormatter*!/
                     }*/
                ],
                sortname: 'ID',
                sortorder: 'desc',
                pager: '#AddreJqGridPager',
            });
        }

        /**
         * 实现复选框单选
         */
        function beforeSelectRow() {
            $("#AddreJqGrid").jqGrid('resetSelection');
            return(true);
        }

        window.addAddress = function(){
            var ids = $('#AddreJqGrid').jqGrid('getGridParam', 'selarrrow');
            //console.log('ids:'+ids);
            if(ids.length==0){
                $('#myModa8').modal('hide');
                return;
            }else {
                var rowData = $('#AddreJqGrid').jqGrid('getRowData',ids);//获取选中行的数据（单行）
                //赋值
                //console.log('val：'+rowData.deliveryCityCode+','+rowData.deliveryCity);
                $("#deliveryProvince").val(rowData.deliveryProvinceCode+','+rowData.deliveryProvince);
                selProvince();
                $("#deliveryCity").val(rowData.deliveryCityCode+','+rowData.deliveryCity);
                $("#deliveryArea").val(rowData.deliveryAreaCode+','+rowData.deliveryArea);
                $("#deliveryAddress").val(rowData.deliveryAddress);
                $('#myModa8').modal('hide');
            }
        }
    })();
    //END

    //表单提交
    (function(){
        var $myModa9 = $('#myModa9');
        var $myModa6 = $('#myModa6');
        //表单操作按钮
        $('#dataform').on('click','.js-form-btn',function(){
            var $this = $(this);
            var type = $this.data('type');
            if(type==1 || type== 3 || type==4){ //1 保存  4 发布 3 预览 统一提交表单
                var extras = fecthExtraParams();
                var url = 'saveRfqReverseAuction/add';
                if(type == 3){
                    url = 'rfqReverseAuctionPreview';
                }
                $('#dataform').ajaxSubmit({
                    type: 'post', // 提交方式 get/post
                    url: ctx+'/rfqReverseAuction/'+url, // 需要提交的 url
                    data:extras,
                    dataType: 'json',
                    success: function (data) {
                        if(data.rspcod != 200){
                            RFQ.error(data.rspmsg);
                            return;
                        }
                        if(type == 1){ //保存成功
                            RFQ.renderText('#myModa9',data.obj);
                            $myModa9.modal('show');
                        }
                        if(type == 4){ //保存成功并发布
                            RFQ.renderText('#myModa6',data.obj);
                            $myModa6.data('id',data.obj.requestId);
                            $myModa6.modal('show');
                        }
                        if(type == 3){
                            location.href=ctx+'/rfqReverseAuction/toRfqReverseAuctionPreview';
                        }
                    },
                    error : function(XmlHttpRequest, textStatus, errorThrown){
                        RFQ.error('出错了！');
                    }
                });
            }else{ //删除
                location.href=ctx+'/rfqReverseAuction/toRfqReverseAuctionPreview';
            }
        });

        //发布摸态框按钮事件绑定
        $myModa6.on('click','.modal-footer a',function(){
            var type = $(this).data('type');
            var id = $myModa6.data('id');
            if(type == 1){ //确认提交
                RFQ.post(ctx+'/rfqRequest/saveRfqRequest/publish',{id:id},function(data){
                    location.href=ctx+'/rfqRequest/init';
                });
            }
        });

        //抓取额外参数
        function fecthExtraParams(){
            //组装额外参数 选取的供应商以及角色
            var extras = {};
            if(!$.isEmptyObject(supplierList)){
                var index = 0,tmp;
                for(var key in supplierList){
                    tmp = supplierList[key];
                    extras['supplierList['+index+'].companyId'] = tmp.companyId;
                    extras['supplierList['+index+'].companyNum'] = tmp.supplierCode;
                    extras['supplierList['+index+'].supplierName'] = tmp.supplierName;
                    extras['supplierList['+index+'].linkmanName'] = tmp.linkmanName;
                    extras['supplierList['+index+'].linkmanTelphone'] = tmp.linkmanTelphone;
                    index++;
                }
            }
            if(!$.isEmptyObject(projectUserList)){
                var index = 0,tmp;
                for(var key in projectUserList){
                    tmp = projectUserList[key];
                    extras['projectUserList['+index+'].userOffice'] = tmp.userOffice;
                    extras['projectUserList['+index+'].jobCode'] = tmp.jobCode;
                    extras['projectUserList['+index+'].userName'] = tmp.userName;
                    extras['projectUserList['+index+'].userTitle'] = tmp.userTitle;
                    //获取选中的权限
                    var $checks = $('#puser_' + key).find(':checkbox');
                    extras['projectUserList['+index+'].viewQx'] = $checks.eq(0).prop('checked')?1:0;
                    extras['projectUserList['+index+'].optionQx'] = $checks.eq(1).prop('checked')?1:0;
                    extras['projectUserList['+index+'].caiwuQx'] =  $checks.eq(2).prop('checked')?1:0;
                    index++;
                }
            }

            //富文本e
            extras['requestBo.requestBusiTerms'] = editor3.html();
            extras['requestBo.requestTecTerms'] = editor2.html();
            extras['preauditBo.requirementDesc'] = editor.html();

            //组装物料行信息
            $('#requestItemData').find('.datatable:visible input').each(function(){
                extras[$(this).data('name')] = this.value;
            });
            return extras;
        }

    })();
    //END

});
