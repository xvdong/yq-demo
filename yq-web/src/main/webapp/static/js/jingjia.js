//输入判断-只能输入数字
$.fn.onlyNum = function() {
  $(this).keypress(function(event) {
    var eventObj = event || e;
    var keyCode = eventObj.keyCode || eventObj.which;
    if ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 37 || keyCode == 39) {
      if ($('#stepn').val()) {
        return true;
      }
    } else
      return false;
  }).focus(function() {
    //禁用输入法
    this.style.imeMode = 'disabled';
  }).bind("paste", function() {
    //获取剪切板的内容
    var clipboard = window.clipboardData.getData("Text");
    if (/^\d+$/.test(clipboard))
      return true;
    else
      return false;
  });
};

//数字大写转换
function jqNumToMoney(n) {
  var fraction = ['角', '分'];
  var digit = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
  var unit = [
    ['元', '万', '亿'],
    ['', '拾', '百', '千']
  ];
  var head = n < 0 ? '欠' : '';
  n = Math.abs(n);
  var s = '';
  for (var i = 0; i < fraction.length; i++) {
    s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
  }
  s = s || '';
  n = Math.floor(n);
  for (var i = 0; i < unit[0].length && n > 0; i++) {
    var p = '';
    for (var j = 0; j < unit[1].length && n > 0; j++) {
      p = digit[n % 10] + unit[1][j] + p;
      n = Math.floor(n / 10);
    }
    s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
  }
  return head + s.replace(/(零.)*零元/, '元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整');
}
