$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    var unifiedRfqNum=$("#unifiedRfqNum").val();
    $("#jqGrid").jqGrid({
        url: 'sameIpList',
        datatype: 'json',
        mtype: 'POST',
        postData : {unifiedRfqNum:unifiedRfqNum},
        autowidth:true,
        rownumbers: true,

        colModel: [

            {
                label: '询价单号',
                name: 'unifiedRfqNum',
                index:unifiedRfqNum ,
                align:'center',
                width: 80,
            },
            {
                label: '询单标题',
                name: 'title',
                index: 'TITLE',
                align:'center',
                width: 80
            },
            {
                label: '供应商U代码',
                name: 'supplierNum',
                index: 'SUPPLIER_NUM',
                align:'center',
                width: 60
            },
            {
                label: '供应商名称',
                name: 'supplierName',
                index: 'SUPPLIER_NAME',
                align:'center',
                width: 80
            },
            {
                label: '是否交纳保证金',
                name: 'assureFlag',
                index: 'ASSURE_FLAG',
                align:'center',
                width: 80
            },
            {
                label: '报价时间',
                name: 'quotationDate',
                index: 'QUOTATION_DATE',
                align:'center',
                width: 80
            },
            {
                label: '报价人',
                name: 'quotationInputUsername',
                index: 'QUOTATION_INPUT_USERNAME',
                align:'center',
                width: 80
            },
            {
                label: '报价金额',
                name: 'subtotalTaxed',
                index: 'SUBTOTAL_TAXED',
                align:'center',
                width: 80
            },
            {
                label: 'IP地址',
                name: 'ip',
                index: 'IP',
                align:'center',
                width: 80
            },
            {
                label: '放弃原因',
                name: 'quotationGiveUpMemo',
                index: 'QUOTATION_GIVE_UP_MEMO',
                align:'center',
                width: 80
            },
            {
                width: 80,
                label: '操作',
                name: 'id',
                index: 'id',
                sortable:false,
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'id',
        sortorder: 'asc',
        pager: "#jqGridPager",
        /*caption: "询报价_询单主表列表"*/
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {
        var rtype = rawObject.type;
        var detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info re" href="javascript:rfqRequestDetail(\'' + rawObject.id + '\')"><i class="icon icon-zoom-in"></i>查看</a></span>';
        if(rtype == '0'){//草稿
            detailBtn =  '<span class="ml5"><a class="btn btn-sm btn-success" href="javascript:editRfqRequest(\'' + rawObject.id + '\')"><i class="icon icon-edit-sign"></i>编辑</a></span>';
        }else if(rtype == '5'){//报名中
            detailBtn =  '<span class="ml5"><a class="btn btn-sm btn-warning" href="javascript:;"><i class="icon icon-check-sign"></i>审批</a></span>';
        }else if(rtype == '7'){//待开标
            detailBtn =  '<span class="ml5"><a class="btn btn-sm btn-danger" href="javascript:;"><i class="icon icon-folder-open-alt"></i>开标</a></span>';
        }else if(rtype == '8'){//待核价
            detailBtn =  '<span class="ml5"><a class="btn btn-sm btn-success" href="javascript:;"><i class="icon icon-yen"></i>核价</a></span>';
        }
        //TODO 缺少公布按钮
        return detailBtn;
    };

    var $myTab = $('#myTab');
    //搜索按钮点击
    $('button.btnSearch').on('click',function(){
        jqGridReload();
    });

    //标签页切换
    $myTab.find('[data-toggle]').on('shown.zui.tab', function(e) {
        var $activeTab = $(e.target);
        //表单重置
        var $activeContent = $($activeTab.attr('href'));
        $activeContent.find('.searchFormTab')[0].reset();
        $.placeholder();
        //状态重置
        var $lis = $activeContent.find('.navbar-nav li');
        $lis.removeClass('active');
        //数据重新加载
        jqGridReload();
        var rfqMethod = $activeTab.data('method')||'';
        //TODO 数据统计
        $.get('fingByRfqType',{rfqMethod:rfqMethod},function(data){
            var staticDatas = {};
            $.each(data,function(i,d){
                staticDatas[d.type] = d.count;
            });
            $lis.each(function(){
                var $a = $(this).find('a');
                $a.find('strong').text('('+(staticDatas[$a.data('type')]||0)+')');
            });
        },'json');
    });

    //状态页切换
    $('div.tab-content').on('click','.navbar-nav a',function(){
        var $this = $(this);
        $this.parent().addClass('active').siblings().removeClass('active');
        jqGridReload();
    });

    function jqGridReload(params){
        //选择的模式
        var $activeTab = $myTab.find('.active a');
        var rfqMethod = $activeTab.data('method')||'';
        //选择的状态以及表单
        var $activeContent = $($activeTab.attr('href'));
        var type = $activeContent.find('.navbar-nav .active a').data('type')||'';
        //表单id
        var formId= $activeContent.find('.searchFormTab')[0].id;
        var params = {rfqMethod:rfqMethod,type:type};
        var postData = $.extend(form2Json(formId),params);
        $jqGrid.jqGrid('setGridParam', {
            postData: postData,
            page: 1
        }).trigger("reloadGrid");
    }

    $("#addRfqRequest").click(function () {
        location.href = "editRfqRequest?oper=add"
    });
});

function rfqRequestDetail(id) {
    location.href = "rfqRequestDetail?id=" + id;
}

function editRfqRequest(id) {
    location.href = "editRfqRequest?oper=edit&id=" + id;
}

function deleteRfqRequest(id) {
    location.href = "delRfqRequest?id=" + id;
}


