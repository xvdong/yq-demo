$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

 function findSupplier(unifiedRfqNo) {
     var supplierName=$("#supplierName").val();
     var supplierCode=$("#supplierCode").val();
     var linkmanName = $("#linkmanName").val();
     var sdata = [];
     //var jqGrid =RFQ.jqGrid("#jqGrid").init
     var jqGrid =null;
     jqGrid = RFQ.jqGrid("#jqGrid").init({
         datatype:"local",
         multiselect: true,
         colModel: [
             {
                 name: 'id',
                 index: 'id',
                 key: true,
                 hidden: true
             },
             {
                 label: 'quotationRound',
                 name: 'quotationRound',
                 hidden:true
             },
             {
                 label: '供应商U代码',
                 name: 'supplierCode',
                 width: 130
             },
             {
                 label: '供应商名称',
                 name: 'supplierName',
                 width: 150
             },
             {
                 label: '所属类别',
                 name: 'type',
                 width: 100
             },
             {
                 label: '供应商等级',
                 name: 'deviceDealerGrade',
                 width: 100,
                 formatter: formatDeviceDealerGrade
             },
             {
                 label: '联系人',
                 name: 'linkmanName',
                 width: 120
             },
             {
                 label: '联系电话',
                 name: 'linkmanTelphone',
                 width: 120
             }
         ],
         pager: "#jqGridPager",
         rowNum: 20,//每页显示记录数
         rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
     });

     if (supplierName == $("#supplierName").attr("placeholder")) {
         supplierName = "";
     }
     if (supplierCode == $("#supplierCode").attr("placeholder")) {
         supplierCode = "";
     }
     if (linkmanName == $("#linkmanName").attr("placeholder")) {
         linkmanName = "";
     }

     $.post(ctx+'/rfqLaunchedSeveralPrice/rfqLaunchedSeveralPriceList',{unifiedRfqNum:unifiedRfqNo,supplierName:supplierName,supplierCode:supplierCode,linkmanName:linkmanName}, function (data) {
         sdata = data.list;
         jqGrid.loadLocal(sdata);
     },'json');
    function formatDeviceDealerGrade(cellValue, options, rowObject) {
        return "已认证";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:fqLaunchedSeveralPriceDetail(\'' + rawObject.id + '\')"><i class="icon icon-search"></i></a>';

/*
        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editfqLaunchedSeveralPrice(\'' + rawObject.id + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deletefqLaunchedSeveralPrice(\'' + rawObject.id + '\')"><i class="icon icon-times"></i></a>';
*/
        return detailBtn
        /* + editBtn + deleteBtn;*/
    };

 /*   $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm")
        }).trigger("reloadGrid");
    });*/

    $("#addfqLaunchedSeveralPrice").click(function () {
        location.href = "editfqLaunchedSeveralPrice?oper=add"
    });
};

function fqLaunchedSeveralPriceDetail(id) {
    location.href = "fqLaunchedSeveralPriceDetail?id=" + id;
}

function editfqLaunchedSeveralPrice(id) {
    location.href = "editfqLaunchedSeveralPrice?oper=edit&id=" + id;
}

function deletefqLaunchedSeveralPrice(id) {
    location.href = "delfqLaunchedSeveralPrice?id=" + id;
}