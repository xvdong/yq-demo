$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    $("#jqGrid1").jqGrid({
        url: ctx + '/rfqLaunchedSeveralPrice/querySupplierList',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        colModel: [
            {
                label: '主键',
                name: 'id',
                width: 18,
                hidden: true
            },
            {
                label: '类型',
                name: 'type',
                width: 18,
                hidden: true
            },
            {
                label: '供应商U代码',
                name: 'supplierCode',
                width: 18
            },
            {
                label: '供应商名称',
                name: 'supplierName',
                width: 50
            },
            {
                label: '保证金',
                name: 'assureFlag',
                width: 10

            },
            {
                label: '报价时间',
                name: 'quotationDate',
                index: 'QUOTATION_DATE',
                width: 17
            },
            {
                label: '报价情况',
                name: 'recCreatorUsername',
                width: 10

            },
            {
                label: '报价金额(元)',
                name: 'subtotalTaxed',
                width: 25
            },
            {
                label: 'IP地址',
                name: 'ip',
                index: 'IP',
                width: 12
            },
            {
                label: '放弃原因',
                name: 'giveUpMemo',
                width: 18
            },
            {
                width: 15,
                label: '操作',
                name: 'id',
                index: 'ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        postData: {
            unifiedRfqNum: $("#unifiedRfqNum").val(),
            quotationRound: $("#quotationRound").val(),
            id: $("#requestId").val()
        },
        //viewrecords: true, // show the current page, data rang and total records on the toolbar
        //multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 100,
        rowList: [10, 20, 30],
        sortname: 'SUPPLIER_CODE',
        sortorder: 'asc',
        sortable: true
    });
    $("#jqGrid1").jqGrid('setLabel', 0,"序号");
    function actFormatter(cellvalue, options, rawObject) {
        if(!isNaN(rawObject.id) && rawObject.id !="") {
            var examinationBtn = '<span class="btn btn-sm btn-warning" onclick="viewDetails(\'' + rawObject.id +  '\' , \'' + rawObject.type + '\')"><i class="icon icon-check-sign"></i>查看</span>';
        }else{
            return "";
        }
        return examinationBtn;
    };
});

function viewDetails(id,type) {
    var aa = window.open();
    var url = ctx+"/rfqQuotation/getQuotationDetail?id=" + id + "&type=" +type;
    aa.location.href = url ;
}
