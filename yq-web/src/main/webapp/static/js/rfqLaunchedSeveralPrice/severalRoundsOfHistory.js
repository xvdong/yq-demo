$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    var unifiedRfqNum=$("#unifiedRfqNum").val();
    $("#jqGrid").jqGrid({
        url:ctx+ '/rfqLaunchedSeveralPrice/history',
        datatype: 'json',
        mtype: 'POST',
        /*postData : {unifiedRfqNum:unifiedRfqNum},*/
        autowidth:true,
        rownumbers: true,

        colModel: [
            {
                label: '询价单号',
                name: 'unifiedRfqNum',
                align:'center',
                hidden:true,
                width:50
            }, {
                label: '询价单号',
                name: 'ouRfqNum',
                align:'center',
                width:50
            },
            {
                label: '询单标题',
                name: 'title',
                align:'center',
                width: 30
            },
            {
                label: '报价单号',
                name: 'id',
                hidden: true,
                align:'center',
                width: 20
            },
            {
                label: '供应商名称',
                name: 'supplierName',
                align: 'center',
                width: 40
            },
            {
                label: '报价时间',
                name: 'quotationDate',
                index: 'QUOTATION_DATE',
                align: 'center',
                width: 50
            },
            {
                label: '报价金额(元)',
                name: 'subtotalTaxed',
                align: 'center',
                width: 35
            },
            {
                label: 'IP地址',
                name: 'ip',
                index: 'IP',
                align: 'center',
                width: 40
            },
            {
                label: '放弃原因',
                name: 'giveUpMemo',
                align: 'center',
                width: 30
            },
            {
                label: '操作',
                name: 'id',
                align: 'center',
                width: 30,
                index: 'ID',
                formatter: actFormatter
            }

        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        postData: {
            ouRfqNum: $("#unifiedRfqNum").val(),
            quotationRound: $("#quotationRound").val(),
            requestId: $("#requestId").val()
        },
        //viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 980,
        height: "100%",
        sortname: 'quotationDate',
        sortorder: 'DESC',
        pager: "#jqGridPager",
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        /*caption: "询报价_询单主表列表"*/
    });
    function unifiedRfqNumFormatter(cellValue, options, rowObject) {
        if(rowObject.quotationRound===0){
            return cellValue.split("-")[0];
        }else{
            return cellValue;
        }
    }
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    }

    /*function actFormatter(cellvalue, options, rawObject) {
        var detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info re" href="javascript:rfqQuotationHistory(\'' + rawObject.unifiedRfqNum+'\',\''+ rawObject.requestId+'\''+'\,'+'\'' +rawObject.quotationRound + '\')"><i class="icon icon-zoom-in"></i>查看</a></span>';
        return detailBtn;
    }*/
    function actFormatter(cellvalue, options, rawObject) {
        if (rawObject.id!=undefined && rawObject.id != "") {
            var examinationBtn = '<span class="btn btn-sm btn-warning" onclick="viewDetails(\'' + rawObject.id + '\' , \'' + rawObject.type + '\')"><i class="icon icon-check-sign"></i>查看</span>';
        } else {
            return "";
        }
        return examinationBtn;
    };

    var $myTab = $('#myTab');
    //搜索按钮点击
    $('button.btnSearch').on('click',function(){
        jqGridReload();
    });

    //标签页切换
    $myTab.find('[data-toggle]').on('shown.zui.tab', function(e) {
        var $activeTab = $(e.target);
        //表单重置
        var $activeContent = $($activeTab.attr('href'));
        $activeContent.find('.searchFormTab')[0].reset();
        $.placeholder();
        //状态重置
        var $lis = $activeContent.find('.navbar-nav li');
        $lis.removeClass('active');
        //数据重新加载
        jqGridReload();
        var rfqMethod = $activeTab.data('method')||'';
        //TODO 数据统计
        $.get('fingByRfqType',{rfqMethod:rfqMethod},function(data){
            var staticDatas = {};
            $.each(data,function(i,d){
                staticDatas[d.type] = d.count;
            });
            $lis.each(function(){
                var $a = $(this).find('a');
                $a.find('strong').text('('+(staticDatas[$a.data('type')]||0)+')');
            });
        },'json');
    });

    //状态页切换
    $('div.tab-content').on('click','.navbar-nav a',function(){
        var $this = $(this);
        $this.parent().addClass('active').siblings().removeClass('active');
        jqGridReload();
    });

    function jqGridReload(params){
        //选择的模式
        var $activeTab = $myTab.find('.active a');
        var rfqMethod = $activeTab.data('method')||'';
        //选择的状态以及表单
        var $activeContent = $($activeTab.attr('href'));
        var type = $activeContent.find('.navbar-nav .active a').data('type')||'';
        //表单id
        var formId= $activeContent.find('.searchFormTab')[0].id;
        var params = {rfqMethod:rfqMethod,type:type};
        var postData = $.extend(form2Json(formId),params);
        $jqGrid.jqGrid('setGridParam', {
            postData: postData,
            page: 1
        }).trigger("reloadGrid");
    }

    $("#addRfqRequest").click(function () {
        location.href = "editRfqRequest?oper=add"
    });
});

function rfqRequestDetail(id) {
    location.href = "rfqRequestDetail?id=" + id;
}
function rfqQuotationHistory(unifiedRfqNum ,id, quotationRound) {
    location.href = ctx + "/rfqLaunchedSeveralPrice/quotationHistory?unifiedRfqNum="+unifiedRfqNum+"&id="+id+"&quotationRound="+quotationRound;
}

function editRfqRequest(id) {
    location.href = "editRfqRequest?oper=edit&id=" + id;
}

function deleteRfqRequest(id) {
    location.href = "delRfqRequest?id=" + id;
}
function viewDetails(id, type) {
    var aa = window.open();
    var url = ctx + "/rfqQuotation/getQuotationDetail?id=" + id + "&qId=" + type;
    aa.location.href = url ;
}


