$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqApprovalStepsList',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        colModel: [
				{
					label: 'ID',
					name: 'id',
					index: 'ID',
					width: 20,
					key: true,
                    hidden:true
				},
				{
					label: '审批流编号',
					name: 'approvalCode',
					index: 'APPROVAL_CODE',
					width: 20,
                    hidden:true
				},
				{
					label: '审批步骤编号',
					name: 'approvalStepCode',
					index: 'APPROVAL_STEP_CODE',
					width: 50,
				},
				{
					label: '审批步骤名称',
					name: 'approvalStepName',
					index: 'APPROVAL_STEP_NAME',
					width: 50,
				},
				{
					label: '创建人',
					name: 'founder',
					index: 'FOUNDER',
					width: 40,
				},
				{
					label: '创建时间',
					name: 'createTime',
					index: 'CREATE_TIME',
					width: 50,
				},
				{
					label: '是否有效',
					name: 'isValid',
					index: 'IS_VALID',
					width: 40,
                    formatter:statusFormatter
				},

            {
                width: 50,
                label: '操作',
                name: 'id',
                index: 'ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        postData:{
            approvalCode:$('#approvalCode').val()
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        //multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        //caption: "审批步骤表列表"
        caption: "<a href=\"javascript:;\" id=\"addRfqApprovalSteps\"><i class=\"icon icon-2x icon-plus\"></i></a>"
    });

    $("#RolejqGrid").jqGrid({
        //url: '/sysRole/sysRoleList',
        url: 'roleList',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        colModel: [
            {
                label: 'id',
                name: 'rolId',
                index: 'rol_id',
                width: 50,
                key: true,
                hidden:true
            },
            {
                label: '角色Code',
                name: 'rolCode',
                index: 'rol_code',
                width: 130
            },
            {
                label: '角色名称',
                name: 'rolTitle',
                index: 'rol_title',
                width: 160,
                formatter: function (cellValue, options, rowObject) {
                    return lengthFormatter(cellValue, 20);
                }
            },
            {
                label: '新增人',
                name: 'rolCreatedBy',
                index: 'rol_created_By',
                width: 120
            },
            {
                label: '新增时间',
                name: 'rolCreatedAt',
                index: 'rol_created_at',
                width: 160
            },
            {
                label: '角色描述',
                name: 'rolDescription',
                index: 'rol_description',
                sortable: false,
                width: 200,
                formatter: function (cellValue, options, rowObject) {
                    return lengthFormatter(cellValue, 40);
                }
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'rolId'   //请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 800,
        height: "50%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'rol_id',
        sortorder: 'asc',
        pager: "#RolejqGridPager",
        caption: "角色列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a href="javascript:rfqApprovalStepsDetail(\'' + rawObject.id + '\')" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="详情"><i class="icon-info-sign"></i></a>';

        var editBtn = '<a href="javascript:editRfqApprovalSteps(\'' + rawObject.id + '\')" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="编辑"><i class="icon-edit"></i></a>';

        var deleteBtn = '<a href="javascript:deleteRfqApprovalSteps(\'' + rawObject.id + '\',\''+ rawObject.approvalStepCode+'\')" class="table-text-danger" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="删除" onclick= "return confirm( \'确定删除该审批记录吗? \')"><i class="icon-trash"></i></a>';

        var setOper = '<a href="javascript:addPeople(\'' + rawObject.id + '\')" class="text-success" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="设置审批角色"><i class="icon-cogs"></i></a>';

        return detailBtn + "&nbsp;&nbsp;&nbsp;" + editBtn + "&nbsp;&nbsp;&nbsp;" + deleteBtn + "&nbsp;&nbsp;&nbsp;" + setOper;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqApprovalSteps").click(function () {
        location.href = "editRfqApprovalSteps?oper=add&approvalCode=" + $("#approvalCode").val();
    });

    $("#save").click(function () {
        var $confirmBtn = $('#saveApprovalStep').find('.form-group .btn-primary');
        $confirmBtn.attr('disabled',true);
        $('#saveApprovalStep').ajaxSubmit({
            success: function (data) {
                if (data.status == 'success') {
                    $.zui.messager.show(data.message, {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                    location.href = ctx+"/rfqApprovalSteps/init?approvalCode="+$('#approvalCode').val();
                } else {
                    $.zui.messager.show(data.message, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    });
});

function rfqApprovalStepsDetail(id) {
    location.href = "rfqApprovalStepsDetail?id=" + id;
}

function editRfqApprovalSteps(id) {
    location.href = "editRfqApprovalSteps?oper=edit&id=" + id;
}

function deleteRfqApprovalSteps(id,approvalStepCode) {
    $.ajax({
        type: "POST",
        url: "delRfqApprovalStep",
        data: {
            id : id,
            approvalCode : $('#approvalCode').val(),
            approvalStepCode : approvalStepCode
        },
        success: function (data) {
            if (data.status == 'success') {
                location.href = "init?approvalCode="+$('#approvalCode').val();
            }else{
                $.zui.messager.show(data.message, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    })
}

//点击设置审批角色
function addPeople(id) {
    $("#RolejqGrid").trigger("reloadGrid");  //刷新
    //$("#RolejqGrid").resetSelection();
    $.ajax({
        type: "POST",
        url: "selectApprovalStepRole",
        data: {
            approvalStepCode : id
        },
        //async:false,   //同步(ajax默认异步)
        success: function (data) {
            if (data.status == 'success') {
                $.each(data.stepOperList,function(i){
                    var roleId = data.stepOperList[i].approverCode;
                    //console.log('roleId'+(i+1)+'='+roleId);
                    //$("#RolejqGrid").jqGrid('setSelection',roleId);
                    $("#RolejqGrid").setSelection(roleId);
                });
            } else {
                $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    });
    $("#stepId").val(id);
    $("#myModal-shr").modal('show');
}

//添加审核角色
function addRole() {
    var ids = $('#RolejqGrid').jqGrid('getGridParam', 'selarrrow');
    //console.log('ids:'+ids);
    if (ids.length == 0) {
        $("#roleids").val();
        $.zui.messager.show('请至少选择一条数据！', {placement: 'center', type: 'warning', icon: 'icon-warning-sign'});
        return;
    } else {
        $("#roleids").val(ids);
        //console.log(ids);
        $('#shrForm').ajaxSubmit({
            success: function (data) {
                if (data.status == 'success') {
                    $('#myModal-shr').modal('hide');
                    $("#jqGrid").trigger("reloadGrid");
                    $.zui.messager.show('设置审核人成功！', {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                } else {
                    $.zui.messager.show('设置审核人失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    }
}

//是否有效状态格式化
function statusFormatter(cellvalue, options, rawObject) {
    var result;
    if(cellvalue=='0'){
        result='<span style="color:red">无效</span>';
    }else if(cellvalue=='1'){
        result='<span style="color:green">有效</span>';
    }else{
        result="未知";
    }
    return result;
}