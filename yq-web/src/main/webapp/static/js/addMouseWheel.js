function addMouseWheel(obj,fn){

	//1添加事件
	if(window.navigator.userAgent.toLowerCase().indexOf("firefox") != -1){
		//ff
		obj.addEventListener("DOMMouseScroll",fnWheel,false);
	} else {
		//ie chrome
		obj.onmousewheel = fnWheel;
	}
	function fnWheel(ev){
		var oEvent = ev || event;
		var bDown = true;//下 为正

		if(oEvent.wheelDelta){
			bDown = oEvent.wheelDelta > 0 ? false : true;
		} else {
			bDown = oEvent.detail > 0 ? true : false;
		}

		typeof fn == "function" && fn(bDown);
		oEvent.preventDefault && oEvent.preventDefault();
		return false;
	}
}
myWeb.scrollLeft=function(bar,s1,s2,k){
	var oDiv = document.getElementById(bar);
	var oDiv2 = document.getElementById(s1);
	var oWrap = document.getElementById("scrollDiv");
	var oBar = oDiv.getElementsByTagName('div')[0];
	var oWrap2 = document.getElementById(s2);

	addMouseWheel(oDiv2,function(bDown){
		if(k!=1){
			var l = oBar.offsetLeft;
			if(bDown){
				l += 10;
			} else {
				l -= 10;
			}

			//限定范围
			if(l < 0){
				l = 0;
			} else if(l > oDiv.offsetWidth - oBar.offsetWidth){
				l = oDiv.offsetWidth - oBar.offsetWidth;
			}

			var scale = l/(oDiv.offsetWidth - oBar.offsetWidth);

			var oLeft = -(oWrap.scrollWidth - oDiv2.offsetWidth)*scale + "px";

			oWrap.style.left =oLeft;
			oWrap2.style.left =oLeft;
			oBar.style.left = l + "px";
		}
	});

	var nn=oDiv.offsetWidth/oWrap.offsetWidth;
	if(nn>=1){
		oDiv.style.display='none';
	}else{
		oDiv.style.display='block';
		oBar.style.width=oDiv.offsetWidth*nn+'px';
	}
	oBar.onmousedown = function(ev){
		if(k!=1){
			oBar.style.background='green';
			var oEvent = ev || event;
			var disX = oEvent.clientX - oBar.offsetLeft;
			document.onmousemove = function(ev){
				var oEvent = ev || event;
				var l = oEvent.clientX - disX;
				if(l < 0){
					l = 0;
				} else if(l > oDiv.offsetWidth - oBar.offsetWidth) {
					l = oDiv.offsetWidth - oBar.offsetWidth;
				}
				oBar.style.left = l + "px";
				var scale = l/(oDiv.offsetWidth - oBar.offsetWidth);
				var oLeft = -(oWrap.scrollWidth - oDiv2.offsetWidth)*scale + "px";
				oWrap.style.left =oLeft;
				oWrap2.style.left =oLeft;
			};
			document.onmouseup = function(){
				oBar.style.background='#e96153';
				document.onmousemove = null;
				document.onmouseup = null;
				oBar.releaseCapture && oBar.releaseCapture();
			};
			oBar.setCapture && oBar.setCapture();
			return false;
		}
	};
}
