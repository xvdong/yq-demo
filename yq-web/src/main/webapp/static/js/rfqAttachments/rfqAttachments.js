$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqAttachmentsList',
        datatype: 'json',
        mtype: 'POST',
        colModel: [
				{
					label: '系统主键(SEQ_RFQ_ATTACHMENT_ID.NEXTVAL)',
					name: 'attachmentId',
					index: 'ATTACHMENT_ID',
					width: 20,
					key: true
				},
				{
					label: '对应的询单ID(BUSINESS FK)',
					name: 'requestId',
					index: 'REQUEST_ID',
					width: 20,
				},
				{
					label: '附件类型(值集:A22) 资格预审通知附件/询价单附件/报价单附件',
					name: 'type',
					index: 'TYPE',
					width: 20,
				},
				{
					label: '对应的资格预审单ID(BUSINESS FK)',
					name: 'preauditId',
					index: 'PREAUDIT_ID',
					width: 20,
				},
				{
					label: '对应的报价单ID(BUSINESS FK)',
					name: 'quotationId',
					index: 'QUOTATION_ID',
					width: 20,
				},
				{
					label: '下载标题',
					name: 'title',
					index: 'TITLE',
					width: 20,
				},
				{
					label: '原始上传文件名',
					name: 'originalFilename',
					index: 'ORIGINAL_FILENAME',
					width: 20,
				},
				{
					label: '下载文件名',
					name: 'downloadFilename',
					index: 'DOWNLOAD_FILENAME',
					width: 20,
				},
				{
					label: '完整下载URL',
					name: 'downloadUrl',
					index: 'DOWNLOAD_URL',
					width: 20,
				},
				{
					label: '参考URL',
					name: 'refUrl',
					index: 'REF_URL',
					width: 20,
				},
				{
					label: '上传个人ID',
					name: 'userId',
					index: 'USER_ID',
					width: 20,
				},
				{
					label: '上传公司ID',
					name: 'companyId',
					index: 'COMPANY_ID',
					width: 20,
				},
				{
					label: '删除标志',
					name: 'isDeletedFlag',
					index: 'IS_DELETED_FLAG',
					width: 20,
				},
				{
					label: '归档标志',
					name: 'isArchivedFlag',
					index: 'IS_ARCHIVED_FLAG',
					width: 20,
				},
				{
					label: '状态',
					name: 'status',
					index: 'STATUS',
					width: 20,
				},
				{
					label: '大小',
					name: 'fileSize',
					index: 'FILE_SIZE',
					width: 20,
				},
				{
					label: '序号',
					name: 'seq',
					index: 'SEQ',
					width: 20,
				},
				{
					label: '对应资格预审供应商ID',
					name: 'preauditSupplierId',
					index: 'PREAUDIT_SUPPLIER_ID',
					width: 20,
				},
				{
					label: '附件是否要返回的标识',
					name: 'originalFileType',
					index: 'ORIGINAL_FILE_TYPE',
					width: 20,
				},

            {
                width: 100,
                label: '操作',
                name: 'attachmentId',
                index: 'ATTACHMENT_ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'attachmentId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'ATTACHMENT_ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_附件记录列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqAttachmentsDetail(\'' + rawObject.attachmentId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqAttachments(\'' + rawObject.attachmentId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqAttachments(\'' + rawObject.attachmentId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqAttachments").click(function () {
        location.href = "editRfqAttachments?oper=add"
    });
});

function rfqAttachmentsDetail(attachmentId) {
    location.href = "rfqAttachmentsDetail?attachmentId=" + attachmentId;
}

function editRfqAttachments(attachmentId) {
    location.href = "editRfqAttachments?oper=edit&attachmentId=" + attachmentId;
}

function deleteRfqAttachments(attachmentId) {
    location.href = "delRfqAttachments?attachmentId=" + attachmentId;
}