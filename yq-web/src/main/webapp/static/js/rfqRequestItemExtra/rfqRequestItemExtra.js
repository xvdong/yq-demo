$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqRequestItemExtraList',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        colModel: [
				{
					label: '序号',
					name: 'id',
					index: 'ID',
					width: 20,
					key: true
				},
                {
                    label: '采购组织名称',
                    name: 'ouName',
                    index: 'OU_NAME',
                    width: 40,
                },
				{
					label: '编码',
					name: 'itemCode',
					index: 'ITEM_CODE',
					width: 30,
				},
            {
                label: '名称',
                name: 'itemName',
                index: 'ITEM_NAME',
                width: 40,
            },
            {
                width: 50,
                label: '操作',
                name: 'id',
                index: 'ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        //multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'id',
        sortorder: 'asc',
        pager: "#jqGridPager",
        //caption: "自定义物料增加列表"
        caption: "<a href=\"javascript:;\" id=\"addRequestItemExtra\"><i class=\"icon icon-2x icon-plus\"></i></a>"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {


        var editBtn = '<a href="javascript:editRfqRequestItemExtra(\'' + rawObject.id + '\')" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="编辑"><i class="icon-edit"></i></a>';

        var deleteBtn = '<a href="javascript:delRfqRequestItemExtra(\'' + rawObject.id + '\')" class="table-text-danger" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="删除" onclick= "return confirm( \'确定删除该物料信息吗? \')"><i class="icon-trash"></i></a>';


        return editBtn + "&nbsp;&nbsp;&nbsp;" + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRequestItemExtra").click(function () {
        location.href = "editRfqRequestItemExtra?oper=add"
    });

    $("#save").click(function () {
        $('#saveItem').ajaxSubmit({
            success: function (data) {
                if (data.status == 'success') {
                    $.zui.messager.show(data.message, {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                    location.href = ctx+"/rfqRequestItemExtra/init";
                } else {
                    $.zui.messager.show(data.message, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    });
});




function editRfqRequestItemExtra(id) {
    location.href = "editRfqRequestItemExtra?oper=edit&id=" + id;
}

function delRfqRequestItemExtra(id) {
    location.href = "delRfqRequestItemExtra?id=" + id;
}

//是否有效状态格式化
function validFormatter(cellvalue, options, rawObject) {
    var result;
    var isStep = rawObject.isStep;
    if(parseInt(isStep)>0){
        if(parseInt(cellvalue)>0){
            result='<span style="color:red">无效</span>';
        }else if(parseInt(cellvalue)==0){
            result='<span style="color:green">有效</span>';
        }else{
            result="未知";
        }
    }else{
        result='<span style="color:red">无效</span>';
    }
    return result;
}

