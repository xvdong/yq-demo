;(function($){
	$.fn.extend({
		setScroll:function(opt){
			if($(this).length<1 || !opt.startTop){return;}
			Timmer= null,
			$this = $(this),$win = $(window),
			startTop = opt.startTop;
			if(opt.defaultTop){$this.css('top',opt.defaultTop);}
			$win.bind('scroll',function(){
				if(Timmer){clearTimeout(Timmer);}
					Timmer = setTimeout(function(){
						if($win.scrollTop()>=startTop+100){
						$this.css({'position':'fixed','top':0,'z-index':2});
					}else{
						$this.css({'position':'absolute','top':opt.startTop});
					}
					//console.log($win.scrollTop());
				},15);	
			});
		},
		
		/*删除行*/
		delBtn:function(){
			$(this).unbind("click").click(function(){
				var ss = $(this).parent().parent("tr").remove();
			});	
		},
		
		checkeInfo:function(){
			$(this).find('label').unbind("click").click(function(){
				$(this).parent().index()==0?$('.fb-zz').show(300):$('.fb-zz').hide(300);	
			});	
		},
		
		cachet:function(){
			var This=$(this);
			setTimeout(function(){
				This.addClass('zt-gz');
			},1000);

		},
		
		/*下拉菜单*/
		selectLisr:function(){
			$(this).find('li a').click(function(){
				$(this).parent().parent().prev('.btn').html('<span class="v">'+$(this).html()+'</span>'+'<span class="caret"></span>');	
			});
		},	
		
		rotateArrow:function(){
			$(this).unbind('click').bind('click',function(){
					$(this).toggleClass('cur');		
			});	
		},
		hoverRow:function(){
			var This=$(this);
			This.find('.hjmx-tbody td').hover(function(){
				var That=$(this);
				This.find('table').each(function(){
					$(this).find('tr').	eq(That.parent().index()).find('td').addClass('bge8e8e8');
				});
	
			},function(){
				var That=$(this);
				This.find('table').each(function(){
					$(this).find('tr').	eq(That.parent().index()).find('td').removeClass('bge8e8e8');
				});
			});
		},
		soTable:function(obj,info){
			$(this).keyup(function(){
				var soText =$(this).val();
				$(obj).find("td").removeClass('fli');
				if(soText!=''){ 
					$(info).hide().parent().removeClass('has-error');	;
					setTimeout(function(){
						$(obj).find("td:contains("+soText+")").addClass('fli');
						if($(obj).find("td:contains("+soText+")").size()==0){
							$(info).show().parent().addClass('has-error');	
						};
					},300);
				}else{
					$(info).show().parent().addClass('has-error');
				}
			});			
		},
		
			
	});
})(jQuery);

;(function(){
	$.extend($,{
		BCacheLoadPic:function(options){//缓存图片
			var defaults={
				src:null,
				param:null,
				callback:function(){}
			};
			var options  = $.extend(true,defaults,options);
			var img = new Image();
			$(img).load(function() {
				options.callback(options.src,this.width,this.height,options.param);
			}).attr("src",options.src);	
		},
		scrollTop:function(options){//返回顶部
			var defaults={
				right:10,
				bottom:10,
				path:'../images/',
				src:'return_top.gif',
				callback:function(){}
			};
			var options  = $.extend(true,defaults,options); 	
			if($('#scroll-top').length<=0){
				$('body').append('<a href="javascript:;" style="height:50px;display:none;width:50px;z-index:996;position:fixed;background:url('+options.path+options.src+') no-repeat" title="返回顶部" id="scroll-top"></a>');
				$top=$('#scroll-top');
				$top.css({
					'opacity':0.5,
					'right':options.right,
					'bottom':options.bottom	
				}).bind({
					'click':a,
					'mouseover':b,
					'mouseleave':c
				});
			};
			$(window).scroll(function(){
				$(window).scrollTop()>0?$top.fadeIn(500):$top.fadeOut(500);	
			});
			function a(){
				$('html,body').animate({'scrollTop':0},300);		
			};
			function b(){
				$(this).fadeTo(300,1);
			};
			function c(){
				$(this).fadeTo(300,0.5);
			};
		},
		placeholder:function(){
				supportPlaceholder='placeholder'in document.createElement('input'),
				placeholder=function(input){
					var text = input.attr('placeholder'),
					defaultValue = input.defaultValue;
					if(!defaultValue){
						input.val(text).addClass("phcolor");
					}
					input.focus(function(){
						if(input.val() == text){
							$(this).val("");
						}
					});
					input.blur(function(){
						if(input.val() == ""){
							$(this).val(text).addClass("phcolor");
						}
					});
					input.keydown(function(){
						$(this).removeClass("phcolor");
					});
					input.change(function(){
						$(this).removeClass("phcolor");
					});
				};
			
			if(!supportPlaceholder){
				$('input').each(function(){
					text = $(this).attr("placeholder");
					if($(this).attr("type") == "text"){
						placeholder($(this));
					}
				});
			}
		}			
	});	
})(jQuery);


var panel={
	init:function(){
		$('.zt-ico-w').cachet();
		$('.btn-group').selectLisr();
		$('.zkBtn').rotateArrow();
		$('.hj-tb').hoverRow();
		$.scrollTop();	
		$.placeholder();
	},
};

$(function(){
	var s={
		init:function(){
			panel.init();	
		},
	};
	s.init();	
});
