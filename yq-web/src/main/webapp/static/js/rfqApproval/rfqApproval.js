$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqApprovalList',
        datatype: 'json',
        mtype: 'POST',
        rownumbers: true,
        colModel: [
				{
					label: '审批流编号',
					name: 'approvalCode',
					index: 'APPROVAL_CODE',
					width: 20,
					key: true,
                    hidden:true
				},
				{
					label: '审批流名称',
					name: 'approvalName',
					index: 'APPROVAL_NAME',
					width: 40,
				},
                {
                    label: '审批流类型',
                    name: 'approvalType',
                    index: 'APPROVAL_TYPE',
                    width: 30,
                    formatter:typeFormatter
                },
				/*{
					label: '组织',
					name: 'organization',
					index: 'ORGANIZATION',
					width: 20,
				},*/
				{
					label: '审批流条件',
					name: 'approvalCondition',
					index: 'APPROVAL_CONDITION',
					width: 30,
				},
				{
					label: '创建人',
					name: 'founder',
					index: 'FOUNDER',
					width: 40,
				},
				{
					label: '创建日期',
					name: 'createTime',
					index: 'CREATE_TIME',
					width: 40,
				},
				{
					label: '修改人',
					name: 'updateFounder',
					index: 'UPDATE_FOUNDER',
					width: 40,
				},
				{
					label: '修改时间',
					name: 'updateTime',
					index: 'UPDATE_TIME',
					width: 40,
				},
                {
                    label: '是否有效',
                    name: 'isValid',
                    index: 'isValid',
                    width: 30,
                    formatter:validFormatter,
                },
                {
                    label: '是否有步骤',
                    name: 'isStep',
                    index: 'isStep',
                    hidden: true,
                },
            {
                width: 50,
                label: '操作',
                name: 'approvalCode',
                index: 'APPROVAL_CODE',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'approvalCode'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        //multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'APPROVAL_CODE',
        sortorder: 'asc',
        pager: "#jqGridPager",
        //caption: "审批流设置表列表"
        caption: "<a href=\"javascript:;\" id=\"addRfqApproval\"><i class=\"icon icon-2x icon-plus\"></i></a>"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a href="javascript:rfqApprovalDetail(\'' + rawObject.approvalCode + '\')" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="详情"><i class="icon-info-sign"></i></a>'

        var editBtn = '<a href="javascript:editRfqApproval(\'' + rawObject.approvalCode + '\')" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="编辑"><i class="icon-edit"></i></a>';

        var deleteBtn = '<a href="javascript:deleteRfqApproval(\'' + rawObject.approvalCode + '\')" class="table-text-danger" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="删除" onclick= "return confirm( \'确定删除该审批流吗? \')"><i class="icon-trash"></i></a>';

        var setStep = '<a href="javascript:setSteps(\'' + rawObject.approvalCode + '\')" class="text-success" onmouseover="$(this).tooltip(\'show\');" onmouseout="$(this).tooltip(\'hide\');" data-toggle="tooltip" data-container="body" title="设置步骤"><i class="icon-cogs"></i></a>';

        return detailBtn + "&nbsp;&nbsp;&nbsp;" + editBtn + "&nbsp;&nbsp;&nbsp;" + deleteBtn + "&nbsp;&nbsp;&nbsp;" + setStep;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqApproval").click(function () {
        location.href = "editRfqApproval?oper=add"
    });

    $("#save").click(function () {
        var $confirmBtn = $('#saveApproval').find('.form-group .btn-primary');
        $confirmBtn.attr('disabled',true);
        $('#saveApproval').ajaxSubmit({
            success: function (data) {
                if (data.status == 'success') {
                    $.zui.messager.show(data.message, {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
                    location.href = ctx+"/rfqApproval/init";
                } else {
                    $.zui.messager.show(data.message, {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
                }
            }
        });
    });
});

function rfqApprovalDetail(approvalCode) {
    location.href = "rfqApprovalDetail?approvalCode=" + approvalCode;
}

function editRfqApproval(approvalCode) {
    location.href = "editRfqApproval?oper=edit&approvalCode=" + approvalCode;
}

function deleteRfqApproval(approvalCode) {
    location.href = "delRfqApproval?approvalCode=" + approvalCode;
}

function setSteps(approvalCode) {
    location.href = ctx+"/rfqApprovalSteps/init?approvalCode=" + approvalCode;
}

//是否有效状态格式化
function validFormatter(cellvalue, options, rawObject) {
    var result;
    var isStep = rawObject.isStep;
    if(parseInt(isStep)>0){
        if(parseInt(cellvalue)>0){
            result='<span style="color:red">无效</span>';
        }else if(parseInt(cellvalue)==0){
            result='<span style="color:green">有效</span>';
        }else{
            result="未知";
        }
    }else{
        result='<span style="color:red">无效</span>';
    }
    return result;
}

//审批流类型格式化
function typeFormatter(cellvalue, options, rawObject) {
    var result;
    if(cellvalue=='1'){
        result = '<span class="label label-warning">创建审批</span>';
    }else if(cellvalue=='2'){
        result = '<span class="label label-primary">多轮报价审批</span>';
    }else if(cellvalue=='3'){
        result = '<span class="label label-primary">结果审批</span>';
    }else{
        result = '<span class="label label-danger">未知</span>';
    }
    return result;
}