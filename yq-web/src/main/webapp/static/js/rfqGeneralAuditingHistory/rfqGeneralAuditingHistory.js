$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var $jqGrid = $("#jqGrid");
	$jqGrid.jqGrid({
		url: ctx+'/rfqGeneralAuditingHistory/rfqRequestGeneralAuditingHistoryList',
        datatype: 'json',
        mtype: 'GET',
		autowidth:true,
		rownumbers: true,
        colModel: [
				{
					label: 'id',
					name: 'id',
					index: 'ID',
					width: 60,
					key: true,
					hidden: true
				},
				{
				label: '询价单号',
				name: 'unifiedRfqNum',
				index: 'UNIFIED_RFQ_NUM',
				align:'center',
                hidden:true,
				width: 130,
				formatter:selectFormatter
				},
            {
                label: '询价单号',
                name: 'ouRfqNum',
                index: 'OU_RFQ_NUM',
                align:'center',
                width: 130,
                formatter:selectFormatter
            },
			{
				label: '询单标题',
				name: 'title',
				index: 'TITLE',
				align:'center',
				width: 120
			},
			{
				label: '发起人',
				name: 'applicantUsername',
				index: 'APPLICANT_USERNAME',
				align:'center',
				width: 90
			},
			{
				label: '提交时间',
				name: 'applicantTime',
				index: 'APPLICANT_TIME',
				align:'center',
				width: 150
			},
			{
				label: '审批人信息',
				name: 'auditorUsername',
				index: 'AUDITOR_USERNAME',
				align:'center',
				width: 90
			},
			{
				label: '审批类型',
				name: 'auditType',
				index: 'AUDIT_TYPE',
				align:'center',
				width: 80,
				formatter: formatRfqAuditType
				},
			{
				label: '审批时间',
				name: 'auditingTime',
				index: 'AUDITING_TIME',
				align:'center',
				width: 150
			},
			{
				label: '状态',
				name: 'status',
				index: 'STATUS',
				align:'center',
				width: 80,
				formatter: formatStatus
			},{
				label: '下一审批级别',
				name: 'signCode',
				index: 'SIGN_CODE',
				align:'center',
				width: 100,
				formatter: formatSignCode
			},{
				label: '审批理由',
				name: 'auditingDesc',
				index: 'AUDITING_DESC',
				align:'center',
				width: 80,
				formatter: formatCSS
			}
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
		postData: {
			objectBillId: $('#objectBillId').val()
		},
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 1100,
        height: "100%",
		rowNum: 20,//每页显示记录数
		rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'AUDITING_TIME',
        sortorder: 'desc',
        pager: "#jqGridPager",
        //caption: "审批记录列表"
    });

});

/**
 * 询单编号链接跳转
 */
function selectFormatter(cellvalue, options, rawObject) {
	var id = rawObject.requestId;
	return "<a class='blue' href='"+ctx+"/rfqRequest/rfqRequestDetail?id="+id+"' target='_blank' >"+cellvalue+"</a>";
}

/**
 * 状态格式化
 */
function formatStatus(cellvalue, options, rawObject) {
	var result = '';
	if('1'==cellvalue){
		result = '同意';
	}else if('0'==cellvalue){
		result = '驳回';
	}else if('2'==cellvalue){
		result = '审批失败';
	}else if('c'==cellvalue){
		result = '发起';
	}else{
		result = '<span class="label label-danger">未知</span>';
	}
	return result;
}

/**
 * 审批级别格式化
 */
function formatSignCode(cellvalue, options, rawObject) {
	var result = '';
	if('-1'==cellvalue){
		result = '审核退回';
	}else if('j'==cellvalue){
		result = '审核已结束';
	}else{
		cellvalue = parseInt(cellvalue)+1;
		result = '第'+cellvalue+'步审批';
	}
	return result;
}