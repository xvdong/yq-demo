$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var $jqGrid = $("#jqGrid");
	$jqGrid.jqGrid({
		url: ctx+'/rfqGeneralAuditing/rfqGeneralAuditingListNew',
        datatype: 'json',
        mtype: 'GET',
		autowidth:true,
		rownumbers: true,
        colModel: [
				{
					label: '序号',
					name: 'gaId',
					index: 'GA_ID',
					width: 80,
					key: true,
					hidden:true
				},
				{
				label: '类型',
				name: 'rfqMethod',
				index: 'RFQ_METHOD',
				width: 50,
				align:'center',
				formatter:rfqMethodFormatter
				},
				{
				label: '询价单号',
				name: 'unifiedRfqNum',
				index: 'UNIFIED_RFQ_NUM',
				align:'center',
					hidden:true,
				width: 120,
				formatter:selectFormatter
				},
            {
                label: '询价单号',
                name: 'ouRfqNum',
                index: 'OU_RFQ_NUM',
                align:'center',
                width: 120,
                formatter:selectFormatter
            },
			{
				label: '询单标题',
				name: 'title',
				index: 'TITLE',
				align:'center',
				width: 80
			},
			{
				label: '提交人',
				name: 'applicantUsername',
				index: 'APPLICANT_USERNAME',
				align:'center',
				width: 60
			},
			{
				label: '提交时间',
				name: 'applicantTime',
				index: 'APPLICANT_TIME',
				align:'center',
				width: 120
			},
			{
				label: '报价截止时间',
				name: 'quotationEndDate',
				index: 'QUOTATION_END_DATE',
				align:'center',
				width: 120
			},
			{
				label: '审批类型',
				name: 'auditType',
				index: 'AUDIT_TYPE',
				align:'center',
				width: 80,
				formatter: formatRfqAuditType
				},
			{
				label: '状态',
				name: 'status',
				index: 'STATUS',
				align:'center',
				width: 60,
				formatter: formatRfqAuditType

			},
            {
                width: 100,
				label: '操作',
                name: 'gaId',
                index: 'GA_ID',
				sortable:false,
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'gaId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 980,
        height: "100%",
        rowNum: 20,
        rowList: [20, 30, 50, 100],
        sortname: 'GA_ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_审批记录表列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

	function actFormatter(cellvalue, options, rawObject) {
		var rtype = rawObject.status;
		var detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info re" href="javascript:rfqRequestDetail(\'' + rawObject.id + '\')"><i class="icon icon-zoom-in"></i>查看</a></span>';
		if(rtype == '20'||rtype == '30'){//已通过或者已驳回 显示查看按钮
			detailBtn ;
		}else if(rtype == '10'){//未审核
			detailBtn =  '<span class="ml5"><a class="btn btn-sm btn-warning" href="javascript:rfqRequestState(\'' + rawObject.unifiedRfqNum + '\')"><i class="icon icon-check-sign"></i>审核</a></span>';
		}
		return detailBtn;
	};
	/**
	 * 起止时间格式化
	 */
	function startEndTimeFormatter(cellvalue, options, rawObject) {
		var endDate = rawObject.quotationEndDate;
		return cellvalue +'~\n'+ endDate;
	}
	/**
	 * 类型格式化
	 */
	function rfqMethodFormatter(cellvalue, options, rawObject) {
		var result = '';
		if('RAQ'==cellvalue){
			result = '<span class="label label-warning">询价</span>';
		}else if('DAC'==cellvalue){
			result = '<span class="label label-primary">反向</span>';
		}else{
			result = '<span class="label label-danger">未知</span>';
		}
		return result;
	}

	/**
	 * 询单编号链接跳转
	 */
	function selectFormatter(cellvalue, options, rawObject) {
		var id = rawObject.id;
		return "<a class='blue' href='"+ctx+"/rfqRequest/rfqRequestDetail?id="+id+"' target='_blank'>"+cellvalue+"</a>";
	}
	var $myTab = $('#myTab');
	//搜索按钮点击
	$('button.btnSearch').on('click',function(){
		jqGridReload();
	});
	function jqGridReload(params){
		//选择的模式
		var $activeTab = $myTab.find('.active a');
		var rfqMethod = $activeTab.data('method')||'';
		//选择的状态以及表单
		var $activeContent = $($activeTab.attr('href'));
		var type = $activeContent.find('.navbar-nav .active a').attr('data-type')||'';
		//表单id
		var formId= $activeContent.find('.searchFormTab')[0].gaId;
		var params = {rfqMethod:rfqMethod,status:status};
		var postData = $.extend(form2Json(formId),params);
		$jqGrid.jqGrid('setGridParam', {
			postData: postData,
			page: 1
		}).trigger("reloadGrid");
	}
    $("#addRfqGeneralAuditing").click(function () {
        location.href = ctx+"/rfqGeneralAuditing/editRfqGeneralAuditing?oper=add"
    });
});

function rfqGeneralAuditingDetail(gaId) {
    location.href = ctx+"/rfqGeneralAuditing/rfqGeneralAuditingDetail?gaId=" + gaId;
}

function editRfqGeneralAuditing(gaId) {
    location.href = ctx+"/rfqGeneralAuditing/editRfqGeneralAuditing?oper=edit&gaId=" + gaId;
}

function deleteRfqGeneralAuditing(gaId) {
    location.href = ctx+"/rfqGeneralAuditing/delRfqGeneralAuditing?gaId=" + gaId;
}