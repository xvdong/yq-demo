$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqPreauditList',
        datatype: 'json',
        mtype: 'POST',
        colModel: [
				{
					label: '系统主键',
					name: 'paId',
					index: 'PA_ID',
					width: 20,
					key: true
				},
				{
					label: '询单ID(FK)',
					name: 'requestId',
					index: 'REQUEST_ID',
					width: 20,
				},
				{
					label: '采购组织ID(从RFQ_REQUEST上冗余过来)',
					name: 'ouId',
					index: 'OU_ID',
					width: 20,
				},
				{
					label: '报名要求',
					name: 'requirementDesc',
					index: 'REQUIREMENT_DESC',
					width: 20,
				},
				{
					label: '删除标志',
					name: 'isDeteled',
					index: 'IS_DETELED',
					width: 20,
				},
				{
					label: '报名通知已发布0－发布/1－未发布',
					name: 'status',
					index: 'STATUS',
					width: 20,
				},
				{
					label: '发布时间',
					name: 'recCreateTime',
					index: 'REC_CREATE_TIME',
					width: 20,
				},
				{
					label: '采购组织名称',
					name: 'ouName',
					index: 'OU_NAME',
					width: 20,
				},
				{
					label: '报名开始时间',
					name: 'registrationStartTime',
					index: 'REGISTRATION_START_TIME',
					width: 20,
				},
				{
					label: '报名截止时间',
					name: 'registrationEndTime',
					index: 'REGISTRATION_END_TIME',
					width: 20,
				},
				{
					label: '资质要求',
					name: 'qualifications',
					index: 'QUALIFICATIONS',
					width: 20,
				},
				{
					label: '厂商要求',
					name: 'requirementFactory',
					index: 'REQUIREMENT_FACTORY',
					width: 20,
				},
				{
					label: '是否为直接生产厂家(0：否 1：是)',
					name: 'isDirectmanuFacturer',
					index: 'IS_DIRECTMANU_FACTURER',
					width: 20,
				},
				{
					label: '是否必须通过ISO9001认证',
					name: 'isCertification',
					index: 'IS_CERTIFICATION',
					width: 20,
				},
				{
					label: '节点名称（大类/中类/小类）',
					name: 'nodename',
					index: 'NODENAME',
					width: 20,
				},
				{
					label: '联系人名称',
					name: 'linkname',
					index: 'LINKNAME',
					width: 20,
				},
				{
					label: '联系电话',
					name: 'linktel',
					index: 'LINKTEL',
					width: 20,
				},
				{
					label: '送货地点',
					name: 'placeOf',
					index: 'PLACE_OF',
					width: 20,
				},
				{
					label: '节点名称代码',
					name: 'nodeNameCode',
					index: 'NODE_NAME_CODE',
					width: 20,
				},
				{
					label: '销售额需求',
					name: 'valueOfSales',
					index: 'VALUE_OF_SALES',
					width: 20,
				},
				{
					label: '注册资本',
					name: 'regcapital',
					index: 'REGCAPITAL',
					width: 20,
				},

            {
                width: 100,
                label: '操作',
                name: 'paId',
                index: 'PA_ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'paId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'PA_ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_资格预审主表记录列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqPreauditDetail(\'' + rawObject.paId + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqPreaudit(\'' + rawObject.paId + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqPreaudit(\'' + rawObject.paId + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqPreaudit").click(function () {
        location.href = "editRfqPreaudit?oper=add"
    });
});

function rfqPreauditDetail(paId) {
    location.href = "rfqPreauditDetail?paId=" + paId;
}

function editRfqPreaudit(paId) {
    location.href = "editRfqPreaudit?oper=edit&paId=" + paId;
}

function deleteRfqPreaudit(paId) {
    location.href = "delRfqPreaudit?paId=" + paId;
}