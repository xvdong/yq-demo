$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: ctx+'/rfqPreauditSupplier/auditHistoryList?unifiedRfqNum='+$("#unifiedRfqNum").val()+'&supplierCode='+$("#supplierCode").val(),
        datatype: 'json',
        mtype: 'POST',
        autowidth:true,
        rownumbers: true,
        colModel: [

            {
                label: '供应商U代码',
                name: 'supplierCode',
                index: 'supplierCode',
                width: 120
            },
            {
                label: '供应商名称',
                name: 'supplierName',
                index: 'supplierName',
                width: 133,
                formatter: function(cellvalue, options,rawObject){
                    if( 0 == rawObject.isSupplier  ){
                        return rawObject.supplierName + '&nbsp;<a class="label label-warning " href="javascript:;">待考察</a>';
                    }else{
                        return rawObject.supplierName  + '&nbsp;<a class="label label-success " href="javascript:;">合格</a>';
                    }
                }
            },
            {
                label: '联系人',
                name: 'linkmanName',
                index: 'linkmanName',
                width: 62
            },
            {
                label: '电话',
                name: 'linkmanTelphone',
                index: 'linkmanTelphone',
                width: 112
            },
            {
                label: '报名时间',
                name: 'answerTime',
                index: 'answerTime',
                width: 141
            },
            {
                label: '审批时间',
                name: 'approvalTime',
                index: 'approvalTime',
                width: 142
            },

            {
                label: '状态',
                name: 'status',
                index: 'status',
                width: 63,
                formatter: statusFormatter
            },

            {
                label: '操作',
                name: 'act',
                index: 'act',
                width: 100,
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 980,
        height: "100%",
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'approvalTime',
        sortorder: 'desc',
        pager: "#jqGridPager",

    });

    $("#jqGrid").jqGrid('setLabel', 0,"序号");
});
/*
状态格式化
 */
function statusFormatter(cellvalue, options, rawObject) {
    if (cellvalue == "") {
        return "未报名"
    } else if (cellvalue == "0") {
        return "未审核";
    } else if (cellvalue == "1") {
        return "已通过";
    } else if (cellvalue == "2") {
        return "已退回";
    }
}
/*
操作按钮格式化
 */
function actFormatter(cellvalue, options, rowObject) {

    var detailBtn = ' <span class="ml5"><span class="btn btn-sm btn-info" onclick="rfqPreauditSupplierDetail(\'' + rowObject['pasId'] + '\')"><i class="icon icon-zoom-in"></i> 查看</span></span>';


        return detailBtn;


}

function rfqPreauditSupplierDetail(pasId) {
    var aa = window.open();
    var url = ctx+"/rfqPreauditSupplier/auditHistoryDetail?pasId="+pasId;
    aa.location.href = url ;
}