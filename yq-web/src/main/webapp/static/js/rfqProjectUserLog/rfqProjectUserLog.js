$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {

    $("#jqGrid").jqGrid({
        url: 'rfqProjectUserLogList',
        datatype: 'json',
        mtype: 'POST',
        colModel: [
				{
					label: '主键',
					name: 'id',
					index: 'ID',
					width: 20,
					key: true,
                },
				{
					label: '询单ID',
					name: 'requestId',
					index: 'REQUEST_ID',
					width: 20,
                },
				{
					label: '修改备注',
					name: 'remarks',
					index: 'REMARKS',
					width: 20,
                },
				{
					label: '创建时间',
					name: 'ctime',
					index: 'CTIME',
					width: 20,
                },
				{
					label: '操作内容',
					name: 'content',
					index: 'CONTENT',
					width: 20,
                },
				{
					label: '性质:0-人工抽取/1-人工指定',
					name: 'nature',
					index: 'NATURE',
					width: 20,
                },
				{
					label: '页面排序号',
					name: 'seq',
					index: 'SEQ',
					width: 20,
                },
				{
					label: '修改人员',
					name: 'xusername',
					index: 'XUSERNAME',
					width: 20,
                },

            {
                width: 100,
                label: '操作',
                name: 'id',
                index: 'ID',
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        multiselect: true,
        width: 980,
        height: "100%",
        rowNum: 10,
        rowList: [10, 20, 30],
        sortname: 'ID',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "询报价_小组成员日志列表"
    });

    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };

    function actFormatter(cellvalue, options, rawObject) {

        var detailBtn = '<a class="btn btn-sm btn-primary" href="javascript:rfqProjectUserLogDetail(\'' + rawObject.id + '\')"><i class="icon icon-search"></i></a>';

        var editBtn = '<a class="btn btn-sm btn-primary" href="javascript:editRfqProjectUserLog(\'' + rawObject.id + '\')"><i class="icon icon-edit"></i></a>';

        var deleteBtn = '<a class="btn btn-sm btn-primary" href="javascript:deleteRfqProjectUserLog(\'' + rawObject.id + '\')"><i class="icon icon-times"></i></a>';

        return detailBtn + editBtn + deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchForm"),
            page: 1
        }).trigger("reloadGrid");
    });

    $("#addRfqProjectUserLog").click(function () {
        location.href = "editRfqProjectUserLog?oper=add"
    });
});

function rfqProjectUserLogDetail(id) {
    location.href = "rfqProjectUserLogDetail?id=" + id;
}

function editRfqProjectUserLog(id) {
    location.href = "editRfqProjectUserLog?oper=edit&id=" + id;
}

function deleteRfqProjectUserLog(id) {
    location.href = "delRfqProjectUserLog?id=" + id;
}