/**
 * Created by Administrator on 2016/10/9.
 */
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    $("#jqGrid").jqGrid({
        url: 'termsList',
        datatype: 'json',
        mtype: 'POST',
        editurl: 'termsList',
        userdata: form2Json("searchFormTab1"),
        multiselect: true,
        colModel: [
            {
                label: '序号',
                name: 'termId',
                index: 'termId',
                width: 140,
                key: true,
                hidden:true
            },
            {
                label: '条款类型',
                name: 'type',
                index: 'type',
                width: 147,
                align:'center',
                editable: true,
                editrules: {required: true},
                formatter: termsType
            },
            {
                label: '条款名称',
                name: 'title',
                index: 'title',
                align:'center',
                width: 153,
                editable: true,
                editrules: {required: true}
            },
            {
                label: '条款内容',
                name: 'content',
                index: 'content',
                align:'center',
                width: 400,
                editable: true,
                editrules: {required: true}
            },
            {
                width: 147,
                label: '操作',
                name: 'termId',
                index: 'termId',
                align:'center',
                sortable:false,
                formatter: actFormatter
            }
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'termId'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar

        width: 1036,
        height: "100%",
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'term_Id',
        sortorder: 'asc',
        pager: "#jqGridPager",
        caption: "条款库列表",
        rownumbers:true
    });
    $("#jqGrid").jqGrid('setLabel', 0,"序号");
    $("#jqGrid").jqGrid('setLabel', 1,"选择");
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };
    function actFormatter(cellvalue, options, rawObject) {

        var editBtn = '<a class="btn btn-sm btn-primary change" onclick="rfqTremsListEdit(this)" termId='+rawObject.termId+' data-toggle="modal" data-target="#myModa2"><i class="icon icon-edit"></i>&nbsp;修改&nbsp;</a>';
        var deleteBtn = '<a class="btn btn-danger btn-sm ml5 order-del-btn" onclick="deleteData(\'' + rawObject.termId + '\')" href="javascript:;"><i class="icon icon-trash"></i>&nbsp;删除&nbsp;</a>';

        return editBtn+deleteBtn;
    };

    $("#btnSearch").click(function () {
        $("#jqGrid").jqGrid('setGridParam', {
            postData: form2Json("searchFormTab1"),
            page:1
        }).trigger("reloadGrid");
    });

    $('#myModa2').on('hidden.bs.modal', function () {
       window.location.reload();});

    $("#enter").click(function(){
        var content = $("#contentSimple").val();/*editor.text()*/
        var termsTitle = $("#termsTitle").val();
        var termId = $("#termId").val();
        var type = $("#type-test").val();
        var remark = $("#contentRemark").val();
        var param = {title: termsTitle, termId: termId, type: type, content: content, remark: remark};
        $.ajax({
            type:"POST",
            url:"editTerms",
            data:param,
            datatype: 'json',
            success: function (data) {

                var ms=eval(data);
                addTerms(ms.rspmsg);
                if(ms.rspmsg=="条款新增成功"||ms.rspmsg=="条款修改成功") {
                    $.zui.messager.show(ms.rspmsg, {
                        placement: 'center',
                        type: 'success',
                        icon: 'icon-exclamation-sign'
                    });
                }else{$.zui.messager.show(ms.rspmsg, {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'}); }
            },
            error: function () {
                $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        });
    });
});

function addTerms(rspmsg){
    if(rspmsg=="条款新增成功"||rspmsg=="条款修改成功"){
        window.location.href = ctx+"/rfqTermsController/init";
    }
};


/*编辑前查询*/
function rfqTremsListEdit(obj) {
    $.ajax({
        type: "POST",
        url:"queryTermsDetail",
        cache:false,
        async:false,
        datatype: 'json',
        data: {
            termId:$(obj).attr("termId"),
        },
        success: function (data) {
            /*console.log(data.content);*/
            $("input[name=title][placeholder=条款名称]").val(data.title);
           /* editor.html(data.content);*/
            $("#contentSimple").val(data.content);
            $("input[name=termId]").val(data.termId);
            $("#type-test").find("option[value="+data.type+"]").attr("selected",true);
            $("#fromTitle").text("条款编辑");
            $("#contentRemark").val(data.remark);
        },
        error: function () {
            $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
        }
    });
}

function add(){
    $("#fromTitle").text("条款新增");
    $("#type-test").attr("请选择");
    $("input[name=title][placeholder=条款名称]").val("");
    $("#contentSimple").val("");
}
/*条款类型判断*/
function termsType(cellvalue, options, rawObject){
    var result = '';
    if('BL'==cellvalue){
        result = '<span>商务条款</span>';
    }else if('TL'==cellvalue){
        result = '<span>技术条款</span>';
    }else{
        result = '<span>未知</span>';
    }
    return result;
}
// 批量删除数据和单个删除,单个删除传递id参数,批量删除id参数为undefined
function deleteData(id) {

    var ids = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');

    if (typeof(id) == "undefined") { //批量删除时参数id为undefined
        if(ids.length==0){
            $.zui.messager.show("请选择需要删除的条款", {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
            return false;
        }else{
            $("#hid").val(ids);
            var termId = $("#hid").val();
        }
    }else {
        var termId = id ;
    }

    $.ajax({
        type: "POST",
        url: ctx+"/rfqTermsController/deleteTerms",
        data: {
            id:termId
        },

        success: function (data) {
            if (data.status == 'success') {

                /*window.location.reload();*/
                window.location.href =ctx+"/rfqTermsController/init";
            } else {
                $.zui.messager.show('链接失败！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            }
        }
    });

}




