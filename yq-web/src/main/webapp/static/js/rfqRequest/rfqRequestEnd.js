$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

var suminfo;//按物料查看一共有多少条,分页时要用
var allResponse;//按物料查后台获取数据,放到这个变量上

$(document).ready(function () {
    $('#myTab .supplier_type').click(function(){//点击按供应商查看
        creatSuppler();
    });
    $('#myTab .item_type').click(function(){//点击按物料查看
        creatThings()
    });
    createGys();//初始化按供应商查看jqgrid,刚进页面就是供应商查看,点击按供应商和按物料查看分别进行不同操作
});
//----------------------------按供应商查看初始化
function creatSuppler(){
    $("#buttom").hide();
    createGys();

}
//----------------------------按物料查看初始化
function creatThings(){
    $("#buttom").show();
    getaQutationList();
}

function createGys(){
        $("#buttom").hide();
        var requestId = $('#requestId').val();
        var htmlText="";
        var htmlSupplier="";
        var htmlDetail="";
        var zhongbiaoMoney=0.0;
        var totalMoney=0.0;
        var mycars=new Array();
        var suppNum=new Array();
        var finalData;
        $.post(ctx+"/rfqQuotationResult/testGYSQuotation",{id:requestId}, function(data) {
            var data= eval("("+data+")");
            finalData=data;
            $('textarea.kindeditorSimple').each(function () {
                KindEditor.create('#'+this.id, {
                    resizeType: 1, allowPreviewEmoticons: false, allowImageUpload: false, items: [] ,afterBlur: function(){this.sync();}
                });
            });

            $.each(data, function(index, item) {
                $('#zhongbiaogys').html(data.length);
                $('#totalMoney').html(item.totalMoney.toFixed(2));
                var id="jqGrid"+index;
                var page="page"+index;
                suppNum[index]=item.supplierNum;
                var priceType = "";
                if ("1" == item.priceType) {
                    priceType = "中标含税总金额：";
                } else {
                    priceType = "中标未税总金额：";
                }
                htmlText +='<table class="table table-bordered align-md">'+
                    '<thead>'+
                    '<div class="thead-info">'+
                    '<div class="pull-left company">'+
                    '<h4 class="pull-left"><a id=\'' + index+"a" + '\' class="blue" target="_blank">'+item.supplierName+
                    '</a>&nbsp;&nbsp;</h4>' +
                    '<a class="c999 pull-left line-32" href="'+ctx+'/rfqQuotationHisotry/getRfqQuotationHistory?requestId='+requestId+'&supplierNum='+item.supplierNum+'" target="_blank">查看报价详情</a>'+
                    '</div>'+
                    '<div class="pull-right price">'+
                    '中标条数：<span class="red2">'+item.quotationResult.length+
                    '</span>条&nbsp;&nbsp;'+priceType+'<span><span class="red2">'+item.bidderMoney.toFixed(2)+'</span>元'+
                    '</div>'+
                    '</div>'+
                    '</thead>'+
                    '<tbody>'+
                    '<div class="jqGridPage">'+
                    '<table id=\'' + index + '\'>'+
                    '</table>'+
                    /*'</tbody>'+*/
                    '<div id=\'' + page + '\' style="width: 980px">'+
                    '</div>'+
                    // '</div>'+
                    '</tbody>'+
                    '</table>'+
                    '<br>';
                $('#tab1').html(htmlText);
                //initMyColmodel(myColmodel);
                mycars[index]=[{
                    label: '物料代码',
                    name: 'materialNo',//物料代码
                    index: 'materialNo',
                    align: 'center',
                    editable: false,
                    width:6,

                },
                    {

                        label: '物料名称',
                        name: 'materialName',//物料名称
                        index: 'materialName',
                        align: 'center',
                        editable: false,  //为true才可以编辑

                        width:6,

                    },
                    {
                        label: '规格型号',
                        name: 'character',
                        index: 'character',
                        align: 'center',
                        editable: false,
                        width:6,

                    },{
                        label: '生产厂家(品牌)',
                        name: 'producer',
                        index: 'producer',
                        align: 'center',
                        editable: false,
                        width:5,

                    },{
                        label: '采购数量',
                        name: 'requestAmount',
                        index: 'requestAmount',
                        align: 'center',
                        editable: false,
                        width:8,

                    },{
                        label: '计量单位',
                        name: 'unit',
                        index: 'unit',
                        align: 'center',
                        editable: false,
                        width:6,

                    },{
                        label: '参考单价(元)',
                        name: 'salePrice',
                        index: 'salePrice',
                        align: 'center',
                        editable: false,
                        width:10,
                        formatter: actFormatter2
                    },{
                        label: '交货期',
                        name: 'requestDeliveryDate',
                        index: 'requestDeliveryDate',
                        align: 'center',
                        editable: false,
                        width:10,

                    },{
                        label: '备注',
                        name: 'memo',
                        index: 'memo',
                        align: 'center',
                        editable: false,
                        width:5,

                    },
                    {
                        label: '中标数量',
                        name: 'confirmedAmout',
                        index: 'confirmedAmout',
                        align: 'center',
                        editable: false,
                        width:8,
                        formatter: actFormatter2
                    },{
                        label: '未税单价',
                        name: 'confirmedUnitPrice',
                        index: 'confirmedUnitPrice',
                        align: 'center',
                        editable: false,
                        width:10,
                        formatter: actFormatter2

                    },{
                        label: '未税总价',
                        name: 'confirmedSubtotal',
                        index: 'confirmedSubtotal',
                        align: 'center',
                        editable: false,
                        width:10,

                    },{
                        label: '税率',
                        name: 'tax',
                        index: 'tax',
                        align: 'center',
                        editable: false,
                        width:5,

                    },{
                        label: '含税单价',
                        name: 'confirmedPriceTaxed',
                        index: 'confirmedPriceTaxed',
                        align: 'center',
                        editable: false,
                        width:10,

                    },{
                        label: '含税总价',
                        name: 'subtotalTaxed',
                        index: 'subtotalTaxed',
                        align: 'center',
                        editable: false,
                        width:10,

                    },

                ];

                if($("#dataSource").val() == 'JFE'){
                    var purchaseNo={
                        label: '申购单号',
                        name: 'purchaseNo',
                        index: 'purchaseNo',
                        align: 'center',
                        editable: false,
                        width:10,
                    }
                    mycars[index].splice(8,0,purchaseNo);
                    var purchaseNo={
                        label: '申购人',
                        name: 'purchasePerson',
                        index: 'purchasePerson',
                        align: 'center',
                        editable: false,
                        width:10,
                    }
                    mycars[index].splice(9,0,purchaseNo);
                    var purchaseNo={
                        label: '图号',
                        name: 'drawingNo',
                        index: 'drawingNo',
                        align: 'center',
                        editable: false,
                        width:10,
                    }
                    mycars[index].splice(10,0,purchaseNo);
                };
                if($("#dataSource").val() == 'NBBX'){
                    var purchaseNo={
                        label: '材质/图号',
                        name: 'drawingNo',
                        index: 'drawingNo',
                        align: 'center',
                        editable: false,
                        width:10,
                    }
                    mycars[index].splice(8,0,purchaseNo);
                }
            });
            /*--------初始化超链接---------*/
            for (var k=0;k<mycars.length;k++){
                $("#"+k+"a").attr("href","http://eps.baosteel.net.cn/eps_eis//overview/BusinessResume.do?method=resume&companyCode="+finalData[k].supplierNum+"&companyId="+finalData[k].companyId);
            }
            initMGrid(suppNum,mycars.length,mycars)
        });
    }
    function  initMGrid(suppNum,k,mycars){
        for (var i=0;i<k;i++)
        {
            initGrid(suppNum[i],i,mycars[i])
        }
    }

    function initGrid(supplierNum,index,mycars) {
        var pagee="page"+index;
        $("#"+index).jqGrid({
            url: ctx + "/rfqQuotationResult/getdataById",          //获取数据路径
            //cellurl: $("#contextPath").val() + "/win/edit",      //编辑后后台路径
            datatype: 'json', //数据来源，本地数据（local，json,jsonp,xml等）,本地测试时不要忘记了改成local
            height: "auto",//高度，表格高度。可为数值、百分比或'auto'
            colModel: mycars,//自己的列,要去初始化,这样写可以方便配置
            mtype: 'POST',
            postData: {'requestId': $('#requestId').val(),'supplierNum':supplierNum},
            jsonReader: { //映射server端返回的字段,这是封装的pageInfo对应的,在用pageInfo时,一定要传bo,bo继承base,base中含有页面等信息
                root: "list",
                rows: "list",
                page: "pageNum",
                total: "pages",
                records: "total"
            },
            viewrecords: true,//是否在浏览导航栏显示记录总数
            rowNum: 20,//每页显示记录数，base类里面通常已经写死成10....
            rowList: [20, 30, 50,100],//用于改变显示行数的下拉列表框的元素数组。
            pager: '#'+pagee,//分页、按钮所在的浏览导航栏
            rownumbers: true,
            //cellEdit: true,
            //cellsubmit: 'remote',
            //toppager: true,//是否在上面显示浏览导航栏
            //sortname:'SUBTOTAL',//默认的排序列名,通过后台数据库排序
            // sortorder: 'asc',//默认的排序方式（asc升序，desc降序）
            sortname: 'seqOfRequestItem',
            sortorder: 'asc',
//            caption: "采购退货单列表",//表名
            autowidth: true,//自动宽


        });
        $("#"+index).jqGrid('setLabel', 0, "序号");//保证第一列是自动的序号
    }

/*-------------------按物料查看初始化-------------------------------*/
function getaQutationList(){
    var htmlText2="";
    var requestId1 = $('#requestId').val();
    $.post(ctx+"/rfqQuotationResult/testWLQuotation",{id:requestId1}, function(data) {
       allResponse=data;;
       suminfo= allResponse.list.length;//一共多少条
        $("#pageSize").val();//每页显示多少条
        getNumber($("#pageSize").val())//获取一共多少页
        //alert($("#pageSize").val())
        dynaInitList(0,$("#pageSize").val(),htmlText2,data);//一开始一定是第一页,所以链表从0开始,第二个参数是20

      $("#total").text(allResponse.list.length);//一共多少条
      $("#pageNum").text(1);//当前页
      $("#pages").text(getNumber($("#pageSize").val()));//共有多少页,$("#pageSize").val()开始是20,自己可以改
      initPageEvent(htmlText2,allResponse);//初始化所有翻页事件
      testCondition();
    });


}
/*--------------获取按物料查看的页数------------------*/
function getNumber(rows){//获取一共多少页
    if(suminfo==0){//没有物料不可能发布成功
    }else {
        return  Math.ceil(suminfo/rows)//获取一共多少页
    }
}
/*
 * 给tab2拼接一个"",即清空tab2
 * */
function clearTable(htmlText2){
    htmlText2="";
    $('#tab2').html(htmlText2);
}
/*
 * 参数:
 * 1.链表开始下标
 * 2.链表结束下标
 * 3.拼接table的html
 * 4.传入的后台含链表的参数
 * */
function dynaInitList(start,end,htmlText2,data){//动态初始化
    clearTable(htmlText2);//先清空原来的table
    //data来源:ctx+"/rfqQuotationResult/testWLQuotation",它返回的pageinfo大小1000(max),没有分页
    var num="9";
    if($("#dataSource").val()=='JFE'){//判断采购组织是否为JFE
        num="12";
    }
    $.each(data.list, function(index, item) {
        item.purchaseNo =item.purchaseNo ==null ? "":item.purchaseNo;
        item.purchasePerson =item.purchasePerson ==null ? "":item.purchasePerson;
        item.drawingNo =item.drawingNo ==null ? "":item.drawingNo;
        if (index<end && index>=start){
            htmlText2 += '<table class="table table-bordered align-md info-y-1">' +
                '<thead>' +
                '<tr>' +
                '<th>物料代码</th>' +
                '<th>物料名称</th>' +
                '<th>型号规格</th>' +
                '<th>生产厂家(品牌)</th>' +
                '<th>采购数量</th>' +
                '<th>计量单位</th>' +
                '<th>参考单价</th>' +
                '<th>交货期</th>' +
                '<th class="purchaseNo">申购单号</th>' +
                '<th class="purchasePerson">申购人</th>' +
                '<th class="drawingNo">图号</th>' +
                '<th class="drawingNo1">材质/图号</th>' +
                '<th>备注</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>' + item.materialNo + '</td>' +
                '<td>' + item.materialName + ' </td>' +
                '<td>' + item.character + '</td>' +
                '<td>' + item.producer + '</td>' +
                '<td>' + item.requestAmount + '</td>' +
                '<td>' + item.unit + '</td>' +
                '<td style="font-weight:bold; color: red">' + item.salePrice + '</td>' +
                '<td>' + item.requestDeliveryDate + '</td>' +
                '<td class="purchaseNo">' + item.purchaseNo + '</td>' +
                '<td class="purchasePerson">' + item.purchasePerson + '</td>' +
                '<td class="drawingNo">' + item.drawingNo + '</td>' +
                '<td class="drawingNo1">' + item.drawingNo + '</td>' +
                '<td>' + item.memo + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td colspan='+num+'>' +
                '<table class="table table-bordered align-md" style="margin-bottom:0;">' +
                '<thead>' +
                '<tr>' +
                '<th>中标供应商</th>' +
                '<th>中标数量</th>' +
                '<th>未税单价</th>' +
                '<th>未税总价</th>' +
                '<th>税率</th>' +
                '<th>含税单价</th>' +
                '<th>含税总价</th>' +
                '<th>中标数对比</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>';
            $.each(item.gysResult, function(index, item2) {
                if(item2.confirmedPriceTaxed==undefined){
                    item2.confirmedPriceTaxed="";
                }
                if(item2.subtotalTaxed==undefined){
                    item2.subtotalTaxed="";
                }
                htmlText2 += '<tr>'+
                    '<td><a href= "javascript:toCompany(\'' + item2.supplierNum+ '\',\'' + item2.companyId+ '\')"  class="blue" target="_blank">'+item2.supplierName+'</a></td>'+
                    '<td style="font-weight:bold; color: red">'+item2.confirmedAmout+'</td>'+
                    '<td style="font-weight:bold; color: red">'+item2.confirmedUnitPrice+'</td>'+
                    '<td>'+(+(item2.confirmedSubtotal)).toFixed(4)+'</td>'+
                    '<td>'+ RFQ.formatTax(item2.tax)+'</td>'+
                    '<td>'+item2.confirmedPriceTaxed+'</td>'+
                    '<td>'+(+(item2.subtotalTaxed)).toFixed(4)+'</td>'+
                    '<td>'+RFQ.formatPercent(item2.confirmedAmout,item.requestAmount)+'</td>'+
                    '</tr>';
                //$("#"+index+"a2").attr("href","https://www.baidu.com/");
            });
            htmlText2 += '</tbody>'+
                '</table>'+
                '</td>'+
                '</tr>'+
                '</tbody>'+
                '</table>';
        }else {
        }
        $('#tab2').html(htmlText2);
    });
    if($("#dataSource").val()!='JFE' && $("#dataSource").val()!='NBBX'){
        $(".purchaseNo").css('display','none');
        $(".purchasePerson").css('display','none');
        $(".drawingNo").css('display','none');
        $(".drawingNo1").css('display','none');
    }
    if($("#dataSource").val() =='JFE'){//判断采购组织是否为JFE
        $(".drawingNo1").css('display','none');
    }
    if($("#dataSource").val() =='NBBX'){//判断采购组织是否为宁波宝新
        $(".purchaseNo").css('display','none');
        $(".purchasePerson").css('display','none');
        $(".drawingNo").css('display','none');
    }
    testCondition();
}

/*------------------初始化所有翻页事件-----------------*/
function initPageEvent(htmlText2,allResponse){

    /*选择每页条数后跳回第一页*/
    $("#pageSize").change(function(){//选择每页条数后的触发事件
        var p1=$(this).children('option:selected').val();//获得自己的选择值,即每页多少条数据
        $("#pageNum").text(1);//设当前页为1
        $("#pages").text(getNumber(p1))//设置一共多少页,只有选择每页条数才会改变总页数
        $("#pageNo").val("")//选择多少页置0
        dynaInitList(0,p1,htmlText2,allResponse);//从0开始遍历到选择的每页条数,就是首页
    });

    $("#homePage").click(function(){//首页
        $("#pageNum").text(1);//设当前页为1
        /*---------------首页操作-------------------*/
        dynaInitList(0,$("#pageSize").val(),htmlText2,allResponse);//从0开始遍历到选择的每页条数,就是首页
    });
    $("#lastPage").click(function(){//上一页
        //不进行parseInt无法进行计算
        if (parseInt($("#pageNum").text()) - 1 <1) {
            $("#pageNum").text(1);
            /////////////页面大小减///////////////////////////
            //小于1页面大小就什么不做
        } else {
            $("#pageNum").text(parseInt($("#pageNum").text())-1);
            /////////////页面大小减///////////////////////////
            /*----------------上一页操作--------------------------*/
            var number=parseInt($("#pageNum").text());//跳到了第几页,,这里不用再加1或者减1,已经text了
            var rows=parseInt(($("#pageSize").val()));//每页多少条
            var start=(number-1)*rows;//从第几条开始
            var end=start+rows;//到第几条结束

            dynaInitList(start,end,htmlText2,allResponse)
        }
    });
    $("#nextPage").click(function(){//下一页
        if (parseInt($("#pageNum").text()) + 1 > parseInt(getNumber($("#pageSize").val()))) {
            $("#pageNum").text(getNumber($("#pageSize").val()));
            /////////////页面大小增///////////////////////////
            //超过页面大小就什么不做
        } else {
            $("#pageNum").text(parseInt($("#pageNum").text()) + 1);
            /////////////页面大小增///////////////////////////
            /*----------------下一页操作--------------------------*/
            var number=parseInt($("#pageNum").text());//跳到了第几页,这里不用再加1或者减1,已经text了
            var rows=parseInt(($("#pageSize").val()));//每页多少条
            var start=(number-1)*rows;//从第几条开始
            var end=start+rows;//到第几条结束

            dynaInitList(start,end,htmlText2,allResponse)
        }
    });
    $("#endPage").click(function(){//尾页
        $("#pageNum").text(getNumber($("#pageSize").val()));//一共多少页就跳到多少页
        /*----------------尾操作--------------------------*/
        var number=parseInt($("#pageNum").text());//跳到了第几页
        var rows=parseInt(($("#pageSize").val()));//每页多少条
        var start=(number-1)*rows;//从第几条开始
        var end=start+rows;//到第几条结束


        dynaInitList(start,end,htmlText2,allResponse);

    });
    $("#toPage").click(function(){//确定按钮

        if ($("#pageNo").val()==''){
            $("#pageNo").val('1')
        }
        var number=parseInt($("#pageNo").val());
        var rows=parseInt(($("#pageSize").val()));//每页多少条
        var start=(number-1)*rows;//从第几条开始
        var end=start+rows;//到第几条结束
        if (number > parseInt(getNumber($("#pageSize").val()))) {
            //
            $.zui.messager.show('所选页数不能大于总页数', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pageNo").val("")//选择多少页置0
        } else if (number <= 0) {
            //
            $.zui.messager.show('所选页数不能为0', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pageNo").val("")//选择多少页置0
        } else if ( 0<number && number<=parseInt(getNumber($("#pageSize").val())) ){
            $("#pageNum").text(parseInt($("#pageNo").val()));
            $("#pageNo").val(parseInt($("#pageNo").val()));
            dynaInitList(start, end, htmlText2, allResponse);
        }else {
            $.zui.messager.show('无效输入', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            $("#pageNo").val("")//选择多少页置0
        }
    });
}


/*-----------------上一页和下一页后的判断,动态拼接链表后会调用----------------------*/
function testCondition(){
    var totalPages=getNumber($("#pageSize").val());//一共多少页
    var currentPage=parseInt($("#pageNum").text());//当前是第几页
    if (currentPage == totalPages) {
        //当前页等于总页数
        $("#anextPage").attr("disabled",true);
        //$("#aendPage").attr("disabled",true);//尾页判断
    } else {
        $("#anextPage").attr("disabled",false);
        //$("#aendPage").attr("disabled",false);
    }
    if (currentPage == 1) {
        //当前页等于1
        $("#alastPage").attr("disabled",true)
        //$("#ahomePage").attr("disabled",true)//首页判断
    } else {
        $("#alastPage").attr("disabled",false);
        //$("#ahomePage").attr("disabled",false);
    }

}

function toCompany(supplierNum,companyId){
    //window.open( "http://eps.baosteel.net.cn/eps_eis//overview/BusinessResume.do?method=resume&companyCode=U00126&companyId="+companyId,'_blank','');
    window.open ( "http://eps.baosteel.net.cn/eps_eis//overview/BusinessResume.do?method=resume&companyCode="+supplierNum+"&companyId="+companyId);
}
//未知
function completeRelease(){
    $('#completeRelease').submit();
}

//excel导出
function exportExcel(){
    var supplierNum =form2Json("modalFrom").supplier;
    $.ajax({
        type: "POST",
        /*url: ctx+"/rfqRequestItem/exportRecord",*/
        url: ctx+"/rfqQuotationItem/exportRecord",
        data: {
            requestId: $("#requestId").val(),
            qtype:"2",
            supplierNum:supplierNum
        },
        success: function(data) {
            if ("success"==data.status) {
                location.href=data.message;
            } else {
                $.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
            };
        }
    });
}

function actFormatter2(cellvalue, options, rawObject) {
    return '<span style="font-weight:bold; color: red">' + cellvalue + '</span>'
}