/**
 * 选择审核状态_审核方法
 */
function subAudit(status){
	var auditingDesc = $('#auditingDesc').val();
	if(''==auditingDesc){
		RFQ.warn('请输入审批意见!');
		return;
	}
	if(auditingDesc.length>2000){
		$.zui.messager.show('审批意见不可超过2000个字！', {placement: 'center', type: 'warning', icon: 'icon-exclamation-sign'});
		return;
	}
	var $confirmBtn = $('#signAuditForm').find('.text-right .btn-md');
	$confirmBtn.attr('disabled',true);
	$('#signAuditForm').ajaxSubmit({
		data:{//url:rfqRequest/signAudit
			status:status,
			auditingDesc:$("#auditingDesc").val()//审批意见
		},
		success: function (data) {
			if (data.status == 'success') {
				$.zui.messager.show(data.message, {placement: 'center', type: 'success', icon: 'icon-exclamation-sign'});
				setTimeout(function () {
					location.href = ctx+"/rfqRequest/auditInit";
				}, 1000);
			} else {
				RFQ.error(data.message);
			}
		}
	});
}

$(document).ready(function () {
	//人员列表
	(function () {
		var jqGrid = null;
		var roleData = [];
		var $myRole = $('#myRole');
		var $form = $myRole.find('form');
		$myRole.on('shown.zui.modal', function () {
			$form.find('input[type="text"]').not('.phcolor').val('');
			$.placeholder();
			getAttData("#jqGridRole", "#jqGridPagerRole");
			jqGrid.resize();
		});

		function getAttData(grid, page) {
			jqGrid = RFQ.jqGrid(grid).init({
				datatype: "local",
				multiselect: true,
				colModel: [
					{
						label: '主键id',
						name: 'id',
						width: 20,
						key: true,
						hidden: true
					},
					{
						label: '员工U代码',
						name: 'userLoginNo',
						width: 15,
					},
					{
						label: '员工名称',
						name: 'userName',
						width: 20,
					},
					{
						label: '公司名称',
						name: 'chineseFullname',
						width: 30,
					}
				],
				pager: page,
				rowNum: 10,//每页显示记录数
				rowList: [10,20, 30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
			});
			$.post(ctx + '/rfqRequest/rfqRequestAuditUserList', {}, function (data) {
				roleData = data.list;
				$form[0].reset();
                $.placeholder();
				jqGrid.loadLocal(roleData);
			}, 'json');
		}

		//模糊搜索
		$("#userSearch").click(function () {
			var validJson = {};
			$form.find(':text,select').each(function () {
				this.value && (validJson[this.name] = this.value);
			});
			var searchData = roleData.filter(function (item) {
				var isValid = true;
				for(var key in validJson){
					if(!isValid) return false;
					isValid = item[key] && ~item[key].indexOf(validJson[key]);//模糊匹配
				}
				return isValid;
			});
			jqGrid.loadLocal(searchData);
		});
	})();
	//END

	//灵活流程选择人员
	$('#myRole').on('click','.modal-footer .js-confirm',function(){
		var rows = $('#jqGridRole').jqGrid("getGridParam", "selarrrow");
		if(rows.length<1){
			RFQ.warn('请选择审批人员!');
			return;
		}
		var rowDataLists = new Array();
		$.each(rows,function(i,r) {
			var rowData = $('#jqGridRole').jqGrid('getRowData', r);//获取选中行的数据（单行）
			rowDataLists.push(rowData);
		});
		var $confirmBtn = $('#myRole').find('.modal-footer .js-confirm');
		$confirmBtn.attr('disabled',true);
		postforData(rowDataLists);
	});

	/**
	 * 流转角色
	 */
	$('#myRole').on('click','#transfer', function () {
        var auditingDesc;
        var type = $("#myRole").data('_type');
        if (type == 'S'){//S时是创建审批点击流转
            auditingDesc = $('#contentSimple').val();
		}else {
            auditingDesc = $('#auditingDesc').val();
		}
		if(''==auditingDesc){
			RFQ.warn('请输入审批意见!');
			return;
		}
		if(auditingDesc.length>2000){
			RFQ.warn('审批意见不可超过2000个字！');
			return;
		}
		var rows = $('#jqGridRole').jqGrid("getGridParam", "selarrrow");
		if(rows.length>1){
			RFQ.warn('选择人员超限!');
			return;
		}
		if(rows.length<1){
			RFQ.warn('请选择审批人员!');
			return;
		}
		var rowData = $('#jqGridRole').jqGrid('getRowData',rows);//获取选中行的数据（单行）
		addUserlist(rowData);
	});
});


function addUserlist (d){
	d.procinstId=$("#procinstId").val();
	$.ajax({
		type: "POST",
		url:ctx + '/rfqRequest/transfer',
		async:false,
		data:d,
		success: function (data) {
			if (data.status == 'success') {
				RFQ.info(data.message);
				setTimeout(function () {
					location.href = ctx+"/rfqRequest/auditInit";
				}, 1000);
			} else {
				RFQ.error(data.message);
			}
		}
	});
};
