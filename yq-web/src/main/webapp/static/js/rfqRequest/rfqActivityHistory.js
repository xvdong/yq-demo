$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var $jqGrid = $("#jqGrid");
	$jqGrid.jqGrid({
		url: ctx+'/rfqRequest/rfqAuditingList',
        datatype: 'json',
        mtype: 'GET',
		autowidth:true,
		rownumbers: true,
        colModel: [
				{
					label: 'id',
					name: 'id',
					index: 'ID',
					width: 60,
					key: true,
					hidden: true
				},
				{
				label: '询价单号',
				name: 'unifiedRfqNum',
				index: 'UNIFIED_RFQ_NUM',
				hidden:true,
				align:'center',
				width: 130,
				formatter:selectFormatter
				},
            {
                label: '询价单号',
                name: 'ouRfqNum',
                index: 'OU_RFQ_NUM',
                align:'center',
                width: 130,
                formatter:selectFormatter
            },
			{
				label: '询单标题',
				name: 'title',
				index: 'TITLE',
				align:'center',
				width: 120
			},
			// {
			// 	label: '发起人',
			// 	name: 'applicantUsername',
			// 	index: 'APPLICANT_USERNAME',
			// 	align:'center',
			// 	width: 90
			// },
			// {
			// 	label: '提交时间',
			// 	name: 'applicantTime',
			// 	index: 'APPLICANT_TIME',
			// 	align:'center',
			// 	width: 150
			// },
			{
				label: '审批人信息',
				name: 'auditorName',
				index: 'AUDITOR_USERNAME',
				align:'center',
				width: 90
			},
			{
				label: '审批类型',
				name: 'billTypeCode',
				index: 'AUDIT_TYPE',
				align:'center',
				width: 80,
				formatter: formatRfqAuditType
			},
			{
				label: '事件编号',
				name: 'eventNumber',
				//index: 'STATUS',
				align:'center',
				width: 60,
				hidden:true
			},
			{
				label: '审批时间',
				name: 'auditTime',
				index: 'AUDITING_TIME',
				align:'center',
				width: 150,
                formatter: timeFormatter
			},
			{
				label: '状态',
				name: 'status',
				index: 'STATUS',
				align:'center',
				width: 80,
				formatter: formatRfqAuditStatus
			},
			{
				label: '下级审批人',
				name: 'approves',
				index: 'approves',
				align:'center',
				width: 80
			},
			{
				label: '审批理由',
				name: 'auditAdvice',
				index: 'AUDITING_DESC',
				align:'center',
				width: 80,
				formatter: formatCSS
			},
			{
				width: 100,
				label: '操作',
				name: 'operat',
				sortable:false,
				formatter: actFormatter
			}
        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
		postData: {
			requestId: $('#requestId').val()
		},
        viewrecords: true, // show the current page, data rang and total records on the toolbar
        width: 1100,
        height: "100%",
		rowNum: 20,//每页显示记录数
		rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'AUDITING_TIME',
        sortorder: 'desc',
        pager: "#jqGridPager",
        //caption: "审批记录列表"
    });

	$("#jqNodeViewGrid").jqGrid({
		url: ctx+'/rfqRequest/rfqAuditingView',
		datatype: 'json',
		mtype: 'GET',
		autowidth:true,
		rownumbers: true,
		colModel: [
			{
				label: 'id',
				name: 'id',
				index: 'ID',
				key: true,
				hidden: true
			},
			{
				label: '审批节点',
				name: 'name',
				align:'center'
			},
			{
				label: '审批人（岗位、角色）',
				name: 'person',
				align:'center'
			},
			{
				label: '审批意见',
				name: 'advice',
				align:'center'
			},
			{
				label: '审批时间',
				name: 'auditTime',
				align:'center'
			}
		],
		jsonReader: { //映射server端返回的字段
			root: "list",
			rows: "list",
			page: "pageNum",
			total: "pages",
			records: "total"
		},
		viewrecords: true, // show the current page, data rang and total records on the toolbar
		height: "100%",
		/*rowNum: 20,//每页显示记录数
		rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
		pager: "#jqNodeViewGridPager",*/
		//caption: "审批记录列表"
	});

});

function actFormatter(cellvalue, options, rawObject) {
	var procinstId = rawObject.procinstId;  //流程id
	var detailBtn = '<span class="ml5"><a class="btn btn-sm btn-info re" onclick="ResultDetail(\'' + procinstId +'\')"><i class="icon icon-zoom-in"></i>详情</a></span>';
	return detailBtn;
};

/**
 * 详情操作按钮
 * @param procinstId   流程实例id
 */
function ResultDetail(procinstId) {
	$("#jqNodeViewGrid").jqGrid('setGridParam', {
		postData: {
			procinstId: procinstId
		},
		page: 1
	}).trigger("reloadGrid");
	$('#nodeView').modal('show');
	/*$.ajax({
		type: "POST",
		url:ctx + '/rfqRequest/rfqAuditingView',
		async:false,
		data:{
			procinstId : procinstId
		},
		success: function (data) {
			if (null != data && "" != data) {
				$('#nodeView').modal('show');
				var d ="<thead>"
				/!*for(var i=0;i<data.length;i++){
					d+="<td>"+i+"</td>";//主要是每隔10个列（表格）换一行 每个表格宽度10%
				}*!/
				d+="<tr>"+
					"<th>"+"审批节点"+"</th>"+
					"<th>"+"审批人（岗位、角色）"+"</th>"+
					"<th>"+"审批意见"+"</th>"+
					"<th>"+"审批时间"+"</th>"+
				"</tr>";
				$.each(data, function (i, val) {
					d+="<td>"+val.name+"</td>"+"<td>"+val.person+"</td>"+"<td>"+val.advice+"</td>"+"<td>"+val.auditTime+"</td>";//主要是每隔10个列（表格）换一行 每个表格宽度10%
				});
				d+="</thead>"
				$("#nodeViewTable").html(d);
			} else {
				$.zui.messager.show('sorry,出错了！', {placement: 'center', type: 'danger', icon: 'icon-exclamation-sign'});
			}
		}
	});*/
}

/**
 * 询单编号链接跳转
 */
function selectFormatter(cellvalue, options, rawObject) {
	var id = rawObject.requestId;
	return "<a class='blue' href='"+ctx+"/rfqRequest/rfqRequestDetail?id="+id+"' target='_blank' >"+cellvalue+"</a>";
}

function timeFormatter(cellvalue, options, rawObject) {
    //补0操作
    function getzf(num){
        if(parseInt(num) < 10){
            num = '0'+num;
        }
        return num;
    }

    function getMyDate(str){
        var oDate = new Date(str),
            oYear = oDate.getFullYear(),
            oMonth = oDate.getMonth()+1,
            oDay = oDate.getDate(),
            oHour = oDate.getHours(),
            oMin = oDate.getMinutes(),
            oSen = oDate.getSeconds(),
            oTime = oYear +'-'+ getzf(oMonth) +'-'+ getzf(oDay) +' '+ getzf(oHour) +':'+ getzf(oMin) +':'+getzf(oSen);//最后拼接时间
        return oTime;
    };
    if (rawObject.status == 20 ){
    	return "";
	}else {
        return getMyDate(cellvalue);
	}
}