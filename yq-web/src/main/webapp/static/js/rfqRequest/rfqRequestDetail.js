$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
	var requestId= $('#requestId').val();
    var gys_login =$('#gys_login').val();

    var colModel = [{
            label: '物料代码',
            name: 'materialNo',
            index: 'MATERIAL_NO',
            width: 80,
        },
        {
            label: '物料名称',
            name: 'materialName',
            index: 'MATERIAL_NAME',
            width: 80,
        },
        {
            label: '型号规格',
            name: 'character',
            index: 'CHARACTER',
            width: 80
        },
        {
            label: '生产厂家（品牌）',
            name: 'producer',
            index: 'PRODUCER',
            width: 80,
        },
        {
            label: '采购数量',
            name: 'requestAmount',
            index: 'REQUEST_AMOUNT',
            width: 80
        },
        {
            label: '计量单位',
            name: 'unit',
            index: 'UNIT',
            width: 80
        },
        //{
        //    label: '物料描述',
        //    name: 'specification',
        //    index: 'SPECIFICATION',
        //    width: 80,
        //},
        {
            label: '参考单价（元）',
            name: 'salePrice',
            index: 'SALE_PRICE',
            width: 80,
        },
        {
            label: '交货期',
            name: 'requestDeliveryDate',
            index: 'REQUEST_DELIVERY_DATE',
            width: 80,
            formatter:'date',
            formatoptions:{srcformat: 'Y-m-d H:i:s', newformat: 'Y-m-d'}
        },
        {
            label: '备注',
            name: 'memo',
            index: 'MEMO',
            width: 80
        }
    ];
    if(gys_login !== "1"){
        colModel.splice(6,1);
    }
    $.post(ctx+'/rfqRequest/getCustomItemHeader',{"requestId":requestId}, function (data) {
            $.each(data.obj,function(i,item){
            var res={};
            res.label=item.itemName;
            res.name="itemValue_"+(i+1);
            res.width=80;
            colModel.push(res);
        });
        if('DAC'==$("#rfqMethod").val()&&'0'==$("#biddingMethod").val()){
            var price= {
                label: '起拍价',
                name: 'startPrice',
                index: 'START_PRICE',
                width: 80
            }
            colModel.push(price);
            var grad={
                label: '梯度',
                name: 'priceGrad',
                index: 'PRICE_GRAD',
                width: 80
            }
            colModel.push(grad);
        } ;
        if('JFE'==$("#dataSource").val()){
            var no= {
                label: '申购单号',
                name: 'purchaseNo',
                index: 'PURCHASE_NO',
                width: 150,
                align:'center'
            }
            colModel.splice(colModel.length-1,0,no);
            var person={
                label: '申购人',
                name: 'purchasePerson',
                index: 'PURCHASE_PERSON',
                width: 100,
                align:'center'
            }
            colModel.splice(colModel.length-1,0,person);
            var drawing={
                label: '图号',
                name: 'drawingNo',
                index: 'DRAWING_NO',
                width: 80,
                align:'center'
            }
            colModel.splice(colModel.length-1,0,drawing);
        };
        if('NBBX'==$("#dataSource").val()){
            var drawing={
                label: '材质/图号',
                name: 'drawingNo',
                index: 'DRAWING_NO',
                width: 80,
                align:'center'
            }
            colModel.splice(colModel.length-1,0,drawing);
        };
        if('PSCS'==$("#dataSource").val()){
            var dummyLineId={
                label: '拟选单行号',
                name: 'dummyLineId',
                index: 'DUMMY_LINE_ID',
                width: 80,
                align:'center'
            }
            colModel.splice(0,0,dummyLineId);
            var mrNo={
                label: 'MR号',
                name: 'mrNo',
                index: 'MR_NO',
                width: 80,
                align:'center'
            }
            colModel.splice(1,0,mrNo);
            /*var dummyLineId={
                label: '拟选单行号',
                name: 'dummyLineId',
                index: 'DUMMY_LINE_ID',
                width: 80,
                align:'center'
            }
            colModel.splice(5,0,dummyLineId);*/
            var packManner={
                label: '签约单位名称',
                name: 'packManner',
                index: 'PACK_MANNER',
                width: 80,
                align:'center'
            }
            colModel.splice(8,0,packManner);
        }
        RFQ.jqGrid('#jqGrid').init({
            url: ctx+'/rfqRequest/rfqRequestItemList',
            colModel: colModel,
            postData:{"id":requestId},
            width: 500,
            sortname: 'SEQ',
            sortorder: 'ASC',
            pager: "#jqGridPager",
            rowNum: 20,//每页显示记录数
            rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        });
    },'json');
});

/**
 * 审核记录
 */
function auditHistory(){
	var requestId = $('#requestId').val();
	location.href = ctx+"/rfqRequest/listActivityHistory?requestId="+requestId;
}

$(document).ready(function () {
    var isSupplierRequirement = $("#isSupplierRequirement").val();
    var quotataionRound = $('#quotataionRound').val();
    if ((quotataionRound !== '' && +quotataionRound > 0) || isSupplierRequirement == '0'){
        $("#supplierListJqGrid").jqGrid({
            datatype: 'local',
            rownumbers:true,
            rownumWidth:50,
            colModel: [
                {
                    label: 'id',
                    name: 'id',
                    index: 'id',
                    key: true,
                    hidden: true
                },
                {
                    label: '供应商U代码',
                    name: 'supplierCode',
                    index: 'supplierCode',
                    align: 'center'
                },
                {
                    label: '特邀供应商名称',
                    name: 'supplierName',
                    index: 'supplierName',
                    align: 'center'
                },
                {
                    label: '联系人',
                    name: 'linkmanName',
                    index: 'linkmanName',
                    align: 'center'
                },
                {
                    label: '联系电话',
                    name: 'linkmanTelphone',
                    index: 'linkmanTelphone',
                    align: 'center'
                    // formatter: lockChangeFormatter
                },
                {
                    label: '微信状态',
                    name: 'isBindWechat',
                    index: 'isBindWechat',
                    align: 'center'
                    // formatter: lockChangeFormatter
                }
            ],
        viewrecords: true,
        //sortname: 'userLoginNo',
        //sortorder: 'asc',
        width: "970",
        height: "100%",
        pager: "#supplierListjqGridPager",
        rowNum: 20,//每页显示记录数
        rowList: [20, 30, 50, 100]//用于改变显示行数的下拉列表框的元素数组。
    });
}else if (isSupplierRequirement == '1') {
        $("#supplierListJqGrid").jqGrid({
            datatype: 'local',
            rownumbers: true,
            rownumWidth: 50,
            colModel: [
                {
                    label: 'id',
                    name: 'id',
                    index: 'id',
                    key: true,
                    hidden: true
                },
                {
                    label: '供应商U代码',
                    name: 'supplierCode',
                    index: 'supplierCode',
                    align: 'center'
                },
                {
                    label: '特邀供应商名称',
                    name: 'supplierName',
                    index: 'supplierName',
                    align: 'center'
                },
                {
                    label: '联系人',
                    name: 'linkmanName',
                    index: 'linkmanName',
                    align: 'center'
                },
                {
                    label: '联系电话',
                    name: 'linkmanTelphone',
                    index: 'linkmanTelphone',
                    align: 'center'
                    // formatter: lockChangeFormatter
                },
                {
                    label: '微信状态',
                    name: 'isBindWechat',
                    index: 'isBindWechat',
                    align: 'center'
                    // formatter: lockChangeFormatter
                }
            ],
            viewrecords: true,
            //sortname: 'userLoginNo',
            //sortorder: 'asc',
            width: "970",
            height: "100%",
            pager: "#supplierListjqGridPager",
            rowNum: 20,//每页显示记录数
            rowList: [20, 30, 50, 100]//用于改变显示行数的下拉列表框的元素数组。
        });
    }
     $("#supplierListJqGrid").jqGrid('setLabel', 0,"序号");
    if(isSupplierRequirement == '0'){

        var supplierList =JSON.parse($("#supplierList").val());
        if(supplierList != undefined && supplierList !=null){

         $("#supplierListJqGrid").jqGrid('setGridParam',{datatype:'local',data:supplierList}).trigger('reloadGrid');
        }
        //debugger;
    }else if(isSupplierRequirement == '1'){


        var preauditSupplierList =JSON.parse($("#preauditSupplierList").val());
        if (preauditSupplierList != undefined && preauditSupplierList != null) {

        $("#supplierListJqGrid").jqGrid('setGridParam',{datatype:'local',data:preauditSupplierList}).trigger('reloadGrid');
        }
    }

});