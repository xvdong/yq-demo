/**
 * Created by Administrator on 2016/10/13.
 */
$.jgrid.defaults.responsive = true;
$.jgrid.defaults.styleUI = 'Bootstrap';

$(document).ready(function () {
    $("#jqGrid").jqGrid({
        url: 'rfqRequestLogList',
        datatype: 'json',
        mtype: 'POST',
        editurl: 'rfqRequestLogList',
        postData:{
            title:$("#title").val()
        },
        colModel: [
            {
                label: '操作对象编号/标题',
                name: 'title',
                index: 'title',
                width: 120,
                key: true,
                formatter: actFormatter

            },
            {
                label: '操作动作',
                name: 'opAction',
                index: 'opAction',
                width: 100,
                align:'center',
                editable: true,
                editrules: {required: true}
            },
            {
                label: '公司',
                name: 'opCompany',
                index: 'opCompany',
                align:'center',
                width: 80,
                editable: true,
                editrules: {required: true}
            },
            {
                label: '操作人代码',
                name: 'createBy',
                index: 'createBy',
                width: 120,
                editable: true,
                editrules: {required: true}
            },
            {
                label: '姓名',
                name: 'opName',
                index: 'opName',
                width: 90,
                editable: true,
                editrules: {required: true}
            },
            {
                label: '操作备注',
                name: 'opDesc',
                index: 'opDesc',
                width: 150,
                editable: true,
                editrules: {required: true}
            },
            {
                label: '操作时间',
                name: 'createDate',
                index: 'createDate',
                width: 150,
                editable: true,
                editrules: {required: true}
            },

        ],
        jsonReader: { //映射server端返回的字段
            root: "list",
            rows: "list",
            page: "pageNum",
            total: "pages",
            records: "total"
        },
        prmNames: {
            id: 'id'//请求参数映射，如默认参数为id，实际参数为myId则可在此映射
        },
        viewrecords: true, // show the current page, data rang and total records on the toolbar

        width: 1100,
        height: "100%",
        rowNum: 20,//每页显示记录数
        rowList: [20,30, 50, 100],//用于改变显示行数的下拉列表框的元素数组。
        sortname: 'create_date',
        sortorder: 'desc',
        pager: "#jqGridPager",
        caption: "询单操作日志列表",
        rownumbers:true
    });
    $("#jqGrid").jqGrid('setLabel', 0,"序号");
    function formatTitle(cellValue, options, rowObject) {
        return cellValue.substring(0, 50) + "...";
    };


    function actFormatter(cellvalue, options, rawObject){
        var strs= new Array();
             strs=  cellvalue.split("-");
        return strs[0];
    };
});
