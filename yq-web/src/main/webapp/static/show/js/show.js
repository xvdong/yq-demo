 $(function() {
   var timer;
   timer = setInterval(function() {
     $(".pane").toggle()
   }, 600);

   $(".page").click(function() {
     if ($(this).hasClass('to800')) {
       $(this).hide();
       $(this).next().show(500, function() {
         $('html, body').animate({
           scrollTop: 500
         }, 100);
       })
     }
     if ($(this).hasClass('last')) {
       return false;
     } else {
       $(this).hide();
       $(this).next().show(500)
     }
   })
 })
