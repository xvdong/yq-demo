package com.yq.domain;

import com.yq.common.BaseBo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/10/9.
 */
public class RptReportLookupVo extends BaseBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;              //主键
    private String reportId;        //报表ID
    private String organizationId;  //可查阅组织ID
    private String creatorUserName; //创建人
    private String creatorUserId;   //创建人ID
    private Date createTime;        //创建时间
    private String revisorUserName; //修改人
    private String revisorUserId;   //修改人ID
    private Date reviseTime;        //修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getCreatorUserName() {
        return creatorUserName;
    }

    public void setCreatorUserName(String creatorUserName) {
        this.creatorUserName = creatorUserName;
    }

    public String getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRevisorUserName() {
        return revisorUserName;
    }

    public void setRevisorUserName(String revisorUserName) {
        this.revisorUserName = revisorUserName;
    }

    public String getRevisorUserId() {
        return revisorUserId;
    }

    public void setRevisorUserId(String revisorUserId) {
        this.revisorUserId = revisorUserId;
    }

    public Date getReviseTime() {
        return reviseTime;
    }

    public void setReviseTime(Date reviseTime) {
        this.reviseTime = reviseTime;
    }
}
