package com.yq.domain;

import com.yq.common.BaseBo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/10/9.
 */
public class RptReportParameterVo extends BaseBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;              //主键
    private String reportId;        //报表ID
    private String displayName;     //显示名称
    private String parameterName;   //参数名
    private String parameterType;   //参数类型 0.文本/1.数值/2.日期
    private String parameterLength; //参数长度
    private String defaultValue;    //默认
    private String optionalValue;   //可选值
    private String creatorUserName; //创建人
    private String creatorUserId;   //创建人ID
    private Date createTime;        //创建时间
    private String revisorUserName; //修改人
    private String revisorUserId;   //修改人ID
    private Date reviseTime;        //修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getParameterLength() {
        return parameterLength;
    }

    public void setParameterLength(String parameterLength) {
        this.parameterLength = parameterLength;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getOptionalValue() {
        return optionalValue;
    }

    public void setOptionalValue(String optionalValue) {
        this.optionalValue = optionalValue;
    }

    public String getCreatorUserName() {
        return creatorUserName;
    }

    public void setCreatorUserName(String creatorUserName) {
        this.creatorUserName = creatorUserName;
    }

    public String getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRevisorUserName() {
        return revisorUserName;
    }

    public void setRevisorUserName(String revisorUserName) {
        this.revisorUserName = revisorUserName;
    }

    public String getRevisorUserId() {
        return revisorUserId;
    }

    public void setRevisorUserId(String revisorUserId) {
        this.revisorUserId = revisorUserId;
    }

    public Date getReviseTime() {
        return reviseTime;
    }

    public void setReviseTime(Date reviseTime) {
        this.reviseTime = reviseTime;
    }
}
