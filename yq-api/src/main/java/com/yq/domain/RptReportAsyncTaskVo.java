package com.yq.domain;

import com.yq.common.BaseBo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/10/9.
 */
public class RptReportAsyncTaskVo extends BaseBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;              //主键
    private String reportId;        //报表ID
    private String queryParameter;  //查询参数
    private String submitName;      //提交人
    private String submitId;        //提交人ID
    private String status;          //执行状态
    private Date startTime;         //任务开始时间
    private Date endTime;           //任务结束时间
    private String creatorUserName; //创建人
    private String creatorUserId;   //创建人ID
    private Date createTime;        //创建时间
    private String revisorUserName; //修改人
    private String revisorUserId;   //修改人ID
    private Date reviseTime;        //修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getQueryParameter() {
        return queryParameter;
    }

    public void setQueryParameter(String queryParameter) {
        this.queryParameter = queryParameter;
    }

    public String getSubmitName() {
        return submitName;
    }

    public void setSubmitName(String submitName) {
        this.submitName = submitName;
    }

    public String getSubmitId() {
        return submitId;
    }

    public void setSubmitId(String submitId) {
        this.submitId = submitId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getCreatorUserName() {
        return creatorUserName;
    }

    public void setCreatorUserName(String creatorUserName) {
        this.creatorUserName = creatorUserName;
    }

    public String getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRevisorUserName() {
        return revisorUserName;
    }

    public void setRevisorUserName(String revisorUserName) {
        this.revisorUserName = revisorUserName;
    }

    public String getRevisorUserId() {
        return revisorUserId;
    }

    public void setRevisorUserId(String revisorUserId) {
        this.revisorUserId = revisorUserId;
    }

    public Date getReviseTime() {
        return reviseTime;
    }

    public void setReviseTime(Date reviseTime) {
        this.reviseTime = reviseTime;
    }
}
