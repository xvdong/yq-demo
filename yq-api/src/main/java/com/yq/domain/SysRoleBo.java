package com.yq.domain;

import com.yq.common.BaseBo;

import java.io.Serializable;
import java.util.Date;

/**
 * Class Name : SysRoleBo.
 * Description : 角色查询实体类.
 * Created by Auto on 2016-05-06.
 */
public class SysRoleBo extends BaseBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long              rolId;             // 角色id
    private String            rolCode;           // 角色code
    private String            rolTitle;          // 角色名称
    private String            rolDescription;    // 角色描述
    private Long              rolCreatedBy;      // 创建人
    private Date              rolCreatedAt;      // 创建时间
    private Long              rolUpdatedBy;      // 最后更新人
    private Date              rolUpdatedAt;      // 最后更新时间
    private String            permission;        // 权限字符串

    public String getRolCode() {
        return rolCode;
    }

    public void setRolCode(String rolCode) {
        this.rolCode = rolCode;
    }

    public Date getRolCreatedAt() {
        return rolCreatedAt;
    }

    public void setRolCreatedAt(Date rolCreatedAt) {
        this.rolCreatedAt = rolCreatedAt;
    }

    public Long getRolCreatedBy() {
        return rolCreatedBy;
    }

    public void setRolCreatedBy(Long rolCreatedBy) {
        this.rolCreatedBy = rolCreatedBy;
    }

    public String getRolDescription() {
        return rolDescription;
    }

    public void setRolDescription(String rolDescription) {
        this.rolDescription = rolDescription;
    }

    public Long getRolId() {
        return rolId;
    }

    public void setRolId(Long rolId) {
        this.rolId = rolId;
    }

    public String getRolTitle() {
        return rolTitle;
    }

    public void setRolTitle(String rolTitle) {
        this.rolTitle = rolTitle;
    }

    public Date getRolUpdatedAt() {
        return rolUpdatedAt;
    }

    public void setRolUpdatedAt(Date rolUpdatedAt) {
        this.rolUpdatedAt = rolUpdatedAt;
    }

    public Long getRolUpdatedBy() {
        return rolUpdatedBy;
    }

    public void setRolUpdatedBy(Long rolUpdatedBy) {
        this.rolUpdatedBy = rolUpdatedBy;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
