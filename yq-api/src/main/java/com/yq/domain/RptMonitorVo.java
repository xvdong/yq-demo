package com.yq.domain;

import com.yq.common.BaseBo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/10/9.
 */
public class RptMonitorVo extends BaseBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;              //主键
    private String title;           //报表标题
    private String monitorSql;            //采购组织ID
    private String planTime;      //供应商ID
    private String mails;      //供应商ID
    private String creatorUserName; //创建人
    private String creatorUserId;   //创建人ID
    private Date createTime;        //创建时间
    private String revisorUserName; //修改人
    private String revisorUserId;   //修改人ID
    private Date reviseTime;        //修改时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMonitorSql() {
        return monitorSql;
    }

    public void setMonitorSql(String monitorSql) {
        this.monitorSql = monitorSql;
    }

    public String getPlanTime() {
        return planTime;
    }

    public void setPlanTime(String planTime) {
        this.planTime = planTime;
    }

    public String getCreatorUserName() {
        return creatorUserName;
    }

    public void setCreatorUserName(String creatorUserName) {
        this.creatorUserName = creatorUserName;
    }

    public String getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRevisorUserName() {
        return revisorUserName;
    }

    public void setRevisorUserName(String revisorUserName) {
        this.revisorUserName = revisorUserName;
    }

    public String getRevisorUserId() {
        return revisorUserId;
    }

    public void setRevisorUserId(String revisorUserId) {
        this.revisorUserId = revisorUserId;
    }

    public Date getReviseTime() {
        return reviseTime;
    }

    public void setReviseTime(Date reviseTime) {
        this.reviseTime = reviseTime;
    }

    public String getMails() {
        return mails;
    }

    public void setMails(String mails) {
        this.mails = mails;
    }
}
