package com.yq.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/10/9.
 */
public class RfqTermsPo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String termId;//编号
    private String nodeId;//采购目录节点
    private String title;//条款名称
    private String type;//条款类型
    private String content;//条款内容
    private String ownerOuId;//拥有采购组织ID
    private String recCreatorUserId;//创建人ID
    private String recCreatorUserName;//创建人名称
    private Date recCreateTime;//创建时间
    private String recRevisorUserId;//修改人id
    private String recRevisorUserName;//修改人名称
    private Date recReviseTime;//修改时间
    private String isDeleted;//删除标志
    private String keyWords;//关键字
    private String remark;//备注

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getRecCreatorUserId() {
        return recCreatorUserId;
    }

    public void setRecCreatorUserId(String recCreatorUserId) {
        this.recCreatorUserId = recCreatorUserId;
    }

    public String getRecRevisorUserId() {
        return recRevisorUserId;
    }

    public void setRecRevisorUserId(String recRevisorUserId) {
        this.recRevisorUserId = recRevisorUserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOwnerOuId() {
        return ownerOuId;
    }

    public void setOwnerOuId(String ownerOuId) {
        this.ownerOuId = ownerOuId;
    }

    public String getRecCreatorUserName() {
        return recCreatorUserName;
    }

    public void setRecCreatorUserName(String recCreatorUserName) {
        this.recCreatorUserName = recCreatorUserName;
    }

    public Date getRecCreateTime() {
        return recCreateTime;
    }

    public void setRecCreateTime(Date recCreateTime) {
        this.recCreateTime = recCreateTime;
    }

    public String getRecRevisorUserName() {
        return recRevisorUserName;
    }

    public void setRecRevisorUserName(String recRevisorUserName) {
        this.recRevisorUserName = recRevisorUserName;
    }

    public Date getRecReviseTime() {
        return recReviseTime;
    }

    public void setRecReviseTime(Date recReviseTime) {
        this.recReviseTime = recReviseTime;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
