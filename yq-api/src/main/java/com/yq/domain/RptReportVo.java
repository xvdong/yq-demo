package com.yq.domain;

import com.yq.common.BaseBo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/10/9.
 */
public class RptReportVo extends BaseBo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;              //主键
    private String title;           //报表标题
    private String ouId;            //采购组织ID
    private String supplierId;      //供应商ID
    private String type;            //报表类型 0.标准/1.非标
    private String reportSql;       //报表SQL标准报表 (长文本)
    private String asyncTaskName;   //异步任务名 非标报表
    private String accessUrl;       //访问URL
    private String creatorUserName; //创建人
    private String creatorUserId;   //创建人ID
    private Date createTime;        //创建时间
    private String revisorUserName; //修改人
    private String revisorUserId;   //修改人ID
    private Date reviseTime;        //修改时间
    private String remark;

    private List<RptReportParameterVo> parameter =new ArrayList<RptReportParameterVo>();
    private List<Map> parameter1 =new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOuId() {
        return ouId;
    }

    public void setOuId(String ouId) {
        this.ouId = ouId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReportSql() {
        return reportSql;
    }

    public void setReportSql(String reportSql) {
        this.reportSql = reportSql;
    }

    public String getAsyncTaskName() {
        return asyncTaskName;
    }

    public void setAsyncTaskName(String asyncTaskName) {
        this.asyncTaskName = asyncTaskName;
    }

    public String getAccessUrl() {
        return accessUrl;
    }

    public void setAccessUrl(String accessUrl) {
        this.accessUrl = accessUrl;
    }

    public String getCreatorUserName() {
        return creatorUserName;
    }

    public void setCreatorUserName(String creatorUserName) {
        this.creatorUserName = creatorUserName;
    }

    public String getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRevisorUserName(String revisorUserName) {
        return revisorUserName;
    }

    public void setRevisorUserName(String revisorUserName) {
        this.revisorUserName = revisorUserName;
    }

    public String getRevisorUserId() {
        return revisorUserId;
    }

    public void setRevisorUserId(String revisorUserId) {
        this.revisorUserId = revisorUserId;
    }

    public Date getReviseTime() {
        return reviseTime;
    }

    public void setReviseTime(Date reviseTime) {
        this.reviseTime = reviseTime;
    }

    public List<RptReportParameterVo> getParameter() {
        return parameter;
    }

    public void setParameter(List<RptReportParameterVo> parameter) {
        this.parameter = parameter;
    }

    public List<Map> getParameter1() {
        return parameter1;
    }

    public void setParameter1(List<Map> parameter1) {
        this.parameter1 = parameter1;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
