package com.yq.common;

public class Constants {
    // 每页条数
    public static final Long REC_PER_PAGE = 20L;
    // 当前页
    public static final Long PAGE_NO = 1L;
    // 当前页
    public static final  String REQUEST_COMPLEX_VO = "rfqRequestComplexVo";
    public static final  String DAC_REQUEST_COMPLEX_VO = "DACRfqRequestComplexVo";
}