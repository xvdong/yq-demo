package com.yq.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application-dev.properties")
@ImportResource({ "classpath:META-INF/spring/applicationContext.xml" })
public class DubboConfig {

}