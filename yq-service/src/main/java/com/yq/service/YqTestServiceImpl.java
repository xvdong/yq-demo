package com.yq.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.yq.mapper.YqtestDemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by xvdong on 2017-10-17.
 */
@Service
@Transactional
public class YqTestServiceImpl implements YqTestService {

    @Autowired
    private YqtestDemoMapper mapper;

    @Override
    public String test() {
        return "测试dubbo";
    }

    @Override
    public List<Map> test2(String sql) {
        return mapper.query(sql);
    }
}
