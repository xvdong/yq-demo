package com.yq.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by xudong on 2017-10-17.
 */
@Mapper
public interface YqtestDemoMapper {
    /**
     * 执行动态查询SQL
     * @param sql
     * @return
     */
    public List<Map> query(String sql);
}
